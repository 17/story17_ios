//
//  UserSuggestionViewController.h
//  Story17
//
//  Created by POPO on 5/11/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendSuggestionCell.h"
#import "ConnectionHeaderView.h"
#import "CustomNavbarView.h"

@interface UserSuggestionViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,SuggestionCellDelegate,ConnectionHeaderDelegate>


@property (nonatomic,strong) CustomNavbarView* navBar;
@property (nonatomic,strong) UIImageView* bgImageView;

@property (nonatomic,strong) UIScrollView* mainScrollView;
@property (nonatomic,strong) UICollectionView* mainCollectionView;
@property (nonatomic,strong) ConnectionHeaderView* headerView;
@property (nonatomic,strong) UIImageView* noDataImageView;

@property (nonatomic,strong) NSMutableArray* suggestionArray;
@property (nonatomic,strong) NSMutableArray* followedUserIDs;
@property (nonatomic,strong) UIActivityIndicatorView* progressView;

@property (nonatomic,strong) NSMutableArray* privacyUserIDs;  // followed users
@property (nonatomic,strong) NSMutableArray* privacyfollowedUserIDs;  // followed users

@property (nonatomic, assign) BOOL useCustomNavBar;


@end
