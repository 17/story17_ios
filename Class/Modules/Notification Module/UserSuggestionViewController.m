//
//  UserSuggestionViewController.m
//  Story17
//
//  Created by POPO on 5/11/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "UserSuggestionViewController.h"
#import "Constant.h"
#import "SVPullToRefresh.h"
#import "UIImage+Helper.h"

@interface UserSuggestionViewController ()

@end

@implementation UserSuggestionViewController

-(id)init
{
    self = [super init];
    if(self){
        _useCustomNavBar = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.title = LOCALIZE(@"User_Suggestion");
    
    [self configureViewForIOS7];
    
    [self.view setBackgroundColor:LIGHT_BG_COLOR];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:LOCALIZE(@"Done") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.view setBackgroundColor:WHITE_COLOR];
    
    _suggestionArray = CREATE_MUTABLE_ARRAY;
    _followedUserIDs = CREATE_MUTABLE_ARRAY;
    _privacyfollowedUserIDs = CREATE_MUTABLE_ARRAY;
    _privacyUserIDs = CREATE_MUTABLE_ARRAY;
    
    [self setup];
    [self reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [self.navigationController setNavigationBarHidden:NO];
}

/* Disable panGesute back */
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}


-(void) setup
{

    int nextY = 0;
    
    if(_useCustomNavBar){
        nextY = STATUS_BAR_HEIGHT;
        
        _navBar = [[CustomNavbarView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, NAVI_BAR_HEIGHT)];
        [_navBar.titleLabel setTextColor:BLACK_COLOR];
        [_navBar.backBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        [_navBar.nextBtn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
        _navBar.backBtn.hidden = YES;
        _navBar.titleLabel = LOCALIZE(@"User_Suggestion");
        [_navBar.nextBtn setTitle:LOCALIZE(@"Next") forState:UIControlStateNormal];

        [_navBar setBackgroundColor:WHITE_COLOR];
        nextY += _navBar.frame.size.height;
        
        //    UIImage* screenImg = [UIImage imageByApplyingBlurToImage:[UIImage imageNamed:@"bg5"] withRadius:SCREEN_WIDTH/32 tintColor:[UIColor colorWithRed:255.f green:255.f blue:255.f alpha:0.2f] saturationDeltaFactor:1.f maskImage:nil];
        //    _bgImageView = [[UIImageView alloc]initWithImage:screenImg];
        _bgImageView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, SCREEN_HEIGHT-nextY);
        _bgImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        [self.view addSubview:_bgImageView];
        [self.view addSubview:_navBar];    
    }
    
    float headerHeight = [ConnectionHeaderView getDefaultHeaderHeight];
    _headerView = [[ConnectionHeaderView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, headerHeight)];
    _headerView.delegate = self;
    [_headerView setBackgroundColor:WHITE_COLOR];
    [_headerView.titleLabel setText:LOCALIZE(@"User_Suggestion")];
    [self.view addSubview:_headerView];
    
    nextY += headerHeight;

    _mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, SCREEN_HEIGHT-nextY)];
    [self scrollViewSettings:_mainScrollView];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    _mainCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, _mainScrollView.frame.size.width, _mainScrollView.frame.size.height) collectionViewLayout:flowLayout];
    
    [self collectionViewSettings:_mainCollectionView];
    [_mainCollectionView registerClass:[FriendSuggestionCell class] forCellWithReuseIdentifier:@"friendSuggestionCell"];
    [_mainCollectionView setBackgroundColor:LIGHT_BG_COLOR];
    [_mainCollectionView setBackgroundColor:[UIColor clearColor]];
    
    [_mainScrollView addSubview:_mainCollectionView];
    [self.view addSubview:_mainScrollView];
    
    _progressView = [UIActivityIndicatorView new];
    _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView setColor:MAIN_COLOR];
    [self.view addSubview:_progressView];
    
    _noDataImageView = [ThemeManager getNoDataImageView];
    [self.view addSubview:_noDataImageView];
    _noDataImageView.hidden = YES;


}


-(void) reloadData
{
    [_progressView startAnimating];
    [API_MANAGER getSuggestedUsers:0 count:100 completion:^(BOOL success, NSArray *suggestedUser) {
        [_progressView stopAnimating];
        if(success){
                        
            if([suggestedUser count]==0){
                _noDataImageView.hidden = NO;
            }else{
                _noDataImageView.hidden = YES;
            }
            
            for(SuggestionObject* suggestion in suggestedUser){
                [_suggestionArray addObject:suggestion];
                
                if(suggestion.user.isFollowing){
                    [_followedUserIDs addObject:suggestion.user.userID];
                }
                if([suggestion.user isPrivacyMode]){
                    [_privacyUserIDs addObject:suggestion.user.userID];
                }
            }
            [_mainCollectionView reloadData];
        }
    }];
}


#pragma mark - UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_suggestionArray count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell* cell;
    
    FriendSuggestionCell* customCell = (FriendSuggestionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"friendSuggestionCell" forIndexPath:indexPath];
    customCell.delegate = self;
    cell = customCell;
    
    SuggestionObject* sugObj = (SuggestionObject*)_suggestionArray[indexPath.row];
    
    if([_followedUserIDs containsObject:sugObj.user.userID]){
        sugObj.user.isFollowing = 1;
    }else{
        sugObj.user.isFollowing = 0;
        
        if([_privacyfollowedUserIDs containsObject:sugObj.user.userID]){
            sugObj.user.followRequestTime = CURRENT_TIMESTAMP;
        }else{
            sugObj.user.followRequestTime = 0;
        }
    }
    
    [(FriendSuggestionCell*)cell reloadCell:(SuggestionObject*)_suggestionArray[indexPath.row]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray* users = CREATE_MUTABLE_ARRAY;
    users = _suggestionArray;
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 10, 0 );
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

# pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(SCREEN_WIDTH,180);
}


#pragma mark - UI Setting
-(void) collectionViewSettings:(UICollectionView*)collectionview
{
    collectionview.delegate = self;
    collectionview.dataSource = self;
    collectionview.scrollsToTop = YES;
    collectionview.alwaysBounceVertical = YES;
    [collectionview setBackgroundColor:LIGHT_BG_COLOR];
}

-(void) scrollViewSettings:(UIScrollView*)scrollView
{
    scrollView.delegate = self;
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = YES;
    scrollView.delaysContentTouches = YES;
    scrollView.canCancelContentTouches = NO;
    scrollView.scrollsToTop = NO;
}

-(void)doneAction:(id)sender
{
    if([_followedUserIDs count]==0){
        
        if([SINGLETON mainTabBarViewController]==NULL || [UIApplication sharedApplication].keyWindow.rootViewController!= [SINGLETON mainTabBarViewController]){
            [SINGLETON setupMainTabBarViewController];
            return;
        }
        
        [NOTIFICATION_CENTER postNotificationName:REFRESH_FEEDS object:self userInfo:nil];
        [self dismissAction];
        return;
    }
    
    [DIALOG_MANAGER showLoadingView];
    [API_MANAGER followAllAction:TO_JSON(_followedUserIDs) withCompletion:^(BOOL success) {
        [DIALOG_MANAGER hideLoadingView];
        if(success){
            if([SINGLETON mainTabBarViewController]==NULL || [UIApplication sharedApplication].keyWindow.rootViewController!= [SINGLETON mainTabBarViewController]){
                [API_MANAGER getSelfInfo:^(BOOL success) {
                    [SINGLETON setupMainTabBarViewController];
                }];
                return;
            }
            
            [NOTIFICATION_CENTER postNotificationName:REFRESH_FEEDS object:self userInfo:nil];
            [self dismissAction];
        }
    }];
    for(NSString* userID in _privacyfollowedUserIDs){
        [API_MANAGER sendFollowRequest:userID withCompletion:^(BOOL success) {}];
    }
    
}

-(void)dismissAction
{
    if([SINGLETON mainTabBarViewController]==NULL || [UIApplication sharedApplication].keyWindow.rootViewController!= [SINGLETON mainTabBarViewController]){
        [SINGLETON setupMainTabBarViewController];
    }else{
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - SuggestionDelegate
-(void)didClickFollow:(NSString*)userID
{
    [EVENT_HANDLER addEventTracking:@"FollowSuggestUser" withDict:@{@"targetUserID":userID}];
    if(![_followedUserIDs containsObject:userID]){
        
        if([_privacyUserIDs containsObject:userID]){
            [_privacyfollowedUserIDs addObject:userID];
        }else{
            [_followedUserIDs addObject:userID];
        }
    }else{
        [_followedUserIDs removeObject:userID];
    }
}

#pragma mark - ConnectionHeaderDelegate
-(void)didClickFollowAll
{
    
    for(SuggestionObject* sug in _suggestionArray){
        
        if(![_followedUserIDs containsObject:sug.user.userID]){
            
            if([sug.user isPrivacyMode]){
                [_privacyfollowedUserIDs addObject:sug.user.userID];
            }else{
                [_followedUserIDs addObject:sug.user.userID];
            }
            
        }
    }
    [EVENT_HANDLER addEventTracking:@"FollowAllSuggestUser" withDict:nil];
    [_mainCollectionView reloadData];
}



@end
