//
//  ListTableViewController.h
//  story17
//
//  Created by POPO Chen on 5/29/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListTableViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) UITableView* listTableView;
@property (nonatomic,strong) NSArray* dataArray;
@property (nonatomic,strong) NSString* selectedItem;
@property (copy)void (^selectionCallback)(NSString* name);

@end
