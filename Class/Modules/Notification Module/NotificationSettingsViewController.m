//
//  NotifcationSettingsViewController.m
//  Story17
//
//  Created by POPO on 5/12/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "NotificationSettingsViewController.h"

@implementation NotificationSettingsViewController

#define verticalMargin 5

// off, followerOnly, everyone

-(void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addCustomNavigationBackButton];
    [self configureViewForIOS7];

    self.title = LOCALIZE(@"NOTIFICATION_SETTINGS");
    
    _mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
    [_mainScrollView setBackgroundColor:LIGHT_BG_COLOR];
    
    int nextY = 0;
    _likeTitleView = [[TitleLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 30)];
    _likeTitleView.userInteractionEnabled = YES;
    [_likeTitleView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushLikeAction:)]];
    [_likeTitleView.titleLabel setText:LOCALIZE(@"Likes")];
    [_mainScrollView addSubview:_likeTitleView];
    nextY += _likeTitleView.frame.size.height;
    
    _likeOffView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _likeOffView.userInteractionEnabled = YES;
    [_likeOffView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushLikeAction:)]];
    [_likeOffView styleSettingsWithMode:WITH_NONE];
    [_likeOffView.titleLabel setText:LOCALIZE(@"Off")];
    [_mainScrollView addSubview:_likeOffView];
    nextY += _likeOffView.frame.size.height+verticalMargin;
    
    _likeFriendView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _likeFriendView.userInteractionEnabled = YES;
    [_likeFriendView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushLikeAction:)]];
    [_likeFriendView styleSettingsWithMode:WITH_NONE];
    [_likeFriendView.titleLabel setText:LOCALIZE(@"From people I liked")];
    [_mainScrollView addSubview:_likeFriendView];
    nextY += _likeFriendView.frame.size.height+verticalMargin;

    _likeAllView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_likeAllView styleSettingsWithMode:WITH_NONE];
    [_likeAllView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushLikeAction:)]];
    [_likeAllView.titleLabel setText:LOCALIZE(@"Like all")];
    [_mainScrollView addSubview:_likeAllView];
    nextY += _likeAllView.frame.size.height+verticalMargin+20;
    
    _commentTitleView = [[TitleLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 30)];
    [_mainScrollView addSubview:_commentTitleView];
    [_commentTitleView.titleLabel setText:LOCALIZE(@"Comments")];
    nextY += _commentTitleView.frame.size.height;

    _commentOffView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _commentOffView.userInteractionEnabled = YES;
    [_commentOffView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushCommentAction:)]];
    [_commentOffView styleSettingsWithMode:WITH_NONE];
    [_commentOffView.titleLabel setText:LOCALIZE(@"Off")];
    [_mainScrollView addSubview:_commentOffView];
    nextY += _commentOffView.frame.size.height+verticalMargin;
    
    _commentFriendView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _commentFriendView.userInteractionEnabled = YES;
    [_commentFriendView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushCommentAction:)]];
    [_commentFriendView styleSettingsWithMode:WITH_NONE];
    [_commentFriendView.titleLabel setText:LOCALIZE(@"From people I liked")];
    [_mainScrollView addSubview:_commentFriendView];
    nextY += _commentFriendView.frame.size.height+verticalMargin;
    
    _commentAllView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _commentAllView.userInteractionEnabled = YES;
    [_commentAllView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushCommentAction:)]];
    [_commentAllView styleSettingsWithMode:WITH_NONE];
    [_commentAllView.titleLabel setText:LOCALIZE(@"Like all")];
    [_mainScrollView addSubview:_commentAllView];
    nextY += _commentAllView.frame.size.height+verticalMargin+20;
    
    _followTitleView = [[TitleLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 30)];
    [_mainScrollView addSubview:_followTitleView];
    [_followTitleView.titleLabel setText:LOCALIZE(@"Follow")];
    nextY += _followTitleView.frame.size.height;
    
    _followOffView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _followOffView.userInteractionEnabled = YES;
    [_followOffView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushFollowAction:)]];
    [_followOffView styleSettingsWithMode:WITH_NONE];
    [_followOffView.titleLabel setText:LOCALIZE(@"Off")];
    [_mainScrollView addSubview:_followOffView];
    nextY += _followOffView.frame.size.height+verticalMargin;
    
    _followAllView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _followAllView.userInteractionEnabled = YES;
    [_followAllView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushFollowAction:)]];
    [_followAllView styleSettingsWithMode:WITH_NONE];
    [_followAllView.titleLabel setText:LOCALIZE(@"Like all")];
    [_mainScrollView addSubview:_followAllView];
    nextY += _followAllView.frame.size.height+verticalMargin+20;
    
    
    _tagTitleView = [[TitleLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 30)];
    [_mainScrollView addSubview:_tagTitleView];
    [_tagTitleView.titleLabel setText:LOCALIZE(@"Tag")];
    nextY += _tagTitleView.frame.size.height;
    
    _tagOffView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _tagOffView.userInteractionEnabled = YES;
    [_tagOffView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushTagAction:)]];
    [_tagOffView styleSettingsWithMode:WITH_NONE];
    [_tagOffView.titleLabel setText:LOCALIZE(@"Off")];
    [_mainScrollView addSubview:_tagOffView];
    nextY += _tagOffView.frame.size.height+verticalMargin;
    
    _tagFriendView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _tagFriendView.userInteractionEnabled = YES;
    [_tagFriendView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushTagAction:)]];
    [_tagFriendView styleSettingsWithMode:WITH_NONE];
    [_tagFriendView.titleLabel setText:LOCALIZE(@"From people I liked")];
    [_mainScrollView addSubview:_tagFriendView];
    nextY += _tagFriendView.frame.size.height+verticalMargin;
    
    _tagAllView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _tagAllView.userInteractionEnabled = YES;
    [_tagAllView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushTagAction:)]];
    [_tagAllView styleSettingsWithMode:WITH_NONE];
    [_tagAllView.titleLabel setText:LOCALIZE(@"Like all")];
    [_mainScrollView addSubview:_tagAllView];
    nextY += _tagAllView.frame.size.height+verticalMargin+20;
    
    
    /* Friend Related Push Notification */
    
    _friendJoinTitleView = [[TitleLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 30)];
    [_mainScrollView addSubview:_friendJoinTitleView];
    [_friendJoinTitleView.titleLabel setText:LOCALIZE(@"Friend_Join")];
    nextY += _friendJoinTitleView.frame.size.height;
    
    _friendJoinOffView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _friendJoinOffView.userInteractionEnabled = YES;
    [_friendJoinOffView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushFriendJoinAction:)]];
    [_friendJoinOffView styleSettingsWithMode:WITH_NONE];
    [_friendJoinOffView.titleLabel setText:LOCALIZE(@"Off")];
    [_mainScrollView addSubview:_friendJoinOffView];
    nextY += _friendJoinOffView.frame.size.height+verticalMargin;
    
    _friendJoinAllView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _friendJoinAllView.userInteractionEnabled = YES;
    [_friendJoinAllView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushFriendJoinAction:)]];
    [_friendJoinAllView styleSettingsWithMode:WITH_NONE];
    [_friendJoinAllView.titleLabel setText:LOCALIZE(@"Like all")];
    [_mainScrollView addSubview:_friendJoinAllView];
    nextY += _friendJoinAllView.frame.size.height+verticalMargin+20;
    
    
    /* Friend Request Push Notification */
    
    _followRequestTitleView = [[TitleLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 30)];
    [_mainScrollView addSubview:_followRequestTitleView];
    [_followRequestTitleView.titleLabel setText:LOCALIZE(@"Friend_Request")];
    nextY += _followRequestTitleView.frame.size.height;
    
    _followRequestOffView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _followRequestOffView.userInteractionEnabled = YES;
    [_followRequestOffView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushFriendRequestAction:)]];
    [_followRequestOffView styleSettingsWithMode:WITH_NONE];
    [_followRequestOffView.titleLabel setText:LOCALIZE(@"Off")];
    [_mainScrollView addSubview:_followRequestOffView];
    nextY += _followRequestOffView.frame.size.height+verticalMargin;
    
    _followRequestAllView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _followRequestAllView.userInteractionEnabled = YES;
    [_followRequestAllView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushFriendRequestAction:)]];
    [_followRequestAllView styleSettingsWithMode:WITH_NONE];
    [_followRequestAllView.titleLabel setText:LOCALIZE(@"Like all")];
    [_mainScrollView addSubview:_followRequestAllView];
    nextY += _followRequestAllView.frame.size.height+verticalMargin+20;
    
    /* Systemo Push Notification */
    
    _sysNotifTitleView = [[TitleLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 30)];
    [_mainScrollView addSubview:_sysNotifTitleView];
    [_sysNotifTitleView.titleLabel setText:LOCALIZE(@"Sys_notif")];
    nextY += _sysNotifTitleView.frame.size.height;
    
    _sysNotifOffView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _sysNotifOffView.userInteractionEnabled = YES;
    [_sysNotifOffView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushSysNotifAction:)]];
    [_sysNotifOffView styleSettingsWithMode:WITH_NONE];
    [_sysNotifOffView.titleLabel setText:LOCALIZE(@"Off")];
    [_mainScrollView addSubview:_sysNotifOffView];
    nextY += _sysNotifOffView.frame.size.height+verticalMargin;
    
    _sysNotifAllView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _sysNotifAllView.userInteractionEnabled = YES;
    [_sysNotifAllView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushSysNotifAction:)]];
    [_sysNotifAllView styleSettingsWithMode:WITH_NONE];
    [_sysNotifAllView.titleLabel setText:LOCALIZE(@"Like all")];
    [_mainScrollView addSubview:_sysNotifAllView];
    nextY += _sysNotifAllView.frame.size.height+verticalMargin+20;
    
    
    _livestreamTitleView = [[TitleLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 30)];
    [_mainScrollView addSubview:_livestreamTitleView];
    [_livestreamTitleView.titleLabel setText:LOCALIZE(@"Livestream_notif")];
    nextY += _livestreamTitleView.frame.size.height;
    
    _livestreamOffView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _livestreamOffView.userInteractionEnabled = YES;
    [_livestreamOffView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushLivestreamNotifAction:)]];
    [_livestreamOffView styleSettingsWithMode:WITH_NONE];
    [_livestreamOffView.titleLabel setText:LOCALIZE(@"Off")];
    [_mainScrollView addSubview:_livestreamOffView];
    nextY += _livestreamOffView.frame.size.height+verticalMargin;
    
    _livestreamAllView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _livestreamAllView.userInteractionEnabled = YES;
    [_livestreamAllView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushLivestreamNotifAction:)]];
    [_livestreamAllView styleSettingsWithMode:WITH_NONE];
    [_livestreamAllView.titleLabel setText:LOCALIZE(@"Like all")];
    [_mainScrollView addSubview:_livestreamAllView];
    nextY += _livestreamAllView.frame.size.height+verticalMargin+20;
    
    
    _restreamTitleView = [[TitleLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 30)];
    [_mainScrollView addSubview:_restreamTitleView];
    [_restreamTitleView.titleLabel setText:LOCALIZE(@"Restream_notif")];
    nextY += _restreamTitleView.frame.size.height;
    
    _retreamOffView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _retreamOffView.userInteractionEnabled = YES;
    [_retreamOffView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushRestreamNotifAction:)]];
    [_retreamOffView styleSettingsWithMode:WITH_NONE];
    [_retreamOffView.titleLabel setText:LOCALIZE(@"Off")];
    [_mainScrollView addSubview:_retreamOffView];
    nextY += _retreamOffView.frame.size.height+verticalMargin;
    
    _retreamAllView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _retreamAllView.userInteractionEnabled = YES;
    [_retreamAllView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushRestreamNotifAction:)]];
    [_retreamAllView styleSettingsWithMode:WITH_NONE];
    [_retreamAllView.titleLabel setText:LOCALIZE(@"Like all")];
    [_mainScrollView addSubview:_retreamAllView];
    nextY += _retreamAllView.frame.size.height+verticalMargin+20;
    
    
    _scheduleTitleView = [[TitleLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 30)];
//    [_mainScrollView addSubview:_scheduleTitleView];
    [_scheduleTitleView.titleLabel setText:LOCALIZE(@"Schedule_notif")];
    nextY += _scheduleTitleView.frame.size.height;

    
    _scheduleOffView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _scheduleOffView.userInteractionEnabled = YES;
    [_scheduleOffView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushRestreamNotifAction:)]];
    [_scheduleOffView styleSettingsWithMode:WITH_NONE];
    [_scheduleOffView.titleLabel setText:LOCALIZE(@"Off")];
//    [_mainScrollView addSubview:_scheduleOffView];
//    nextY += _scheduleOffView.frame.size.height+verticalMargin;
    
    _scheduleAllView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    _scheduleAllView.userInteractionEnabled = YES;
    [_scheduleAllView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePushRestreamNotifAction:)]];
    [_scheduleAllView styleSettingsWithMode:WITH_NONE];
    [_scheduleAllView.titleLabel setText:LOCALIZE(@"Like all")];
//    [_mainScrollView addSubview:_scheduleAllView];
//    nextY += _scheduleAllView.frame.size.height+verticalMargin+20;
    
    
    [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, nextY)];
    
    [self.view addSubview:_mainScrollView];
    
    [self reloadPushLike];
    [self reloadPushComment];
    [self reloadPushFollow];
    [self reloadPushFriendJoin];
    [self reloadPushFriendFirstPost];
    [self reloadPushTag];
    [self reloadPushSysNotif];
    [self reloadPushLiveNotif];
    [self reloadPushRestreamNotif];
    [self reloadPushFriendRequest];
    
//    [self reloadPushSchedule];

}


#pragma mark - reload push status
-(void)reloadPushLike
{
    NSString* mode = GET_DEFAULT(PUSH_LIKE);
    
    if([mode isEqualToString:@"off"]){
        [_likeOffView styleSettingsWithMode:IS_SELECTED];
        [_likeFriendView styleSettingsWithMode:WITH_NONE];
        [_likeAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:@"followerOnly"]){
        [_likeOffView styleSettingsWithMode:WITH_NONE];
        [_likeFriendView styleSettingsWithMode:IS_SELECTED];
        [_likeAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:@"everyone"]){
        [_likeOffView styleSettingsWithMode:WITH_NONE];
        [_likeFriendView styleSettingsWithMode:WITH_NONE];
        [_likeAllView styleSettingsWithMode:IS_SELECTED];
    }
}

-(void)reloadPushComment
{
    NSString* mode = GET_DEFAULT(PUSH_COMMENT);
    if([mode isEqualToString:PUSH_OFF]){
        [_commentOffView styleSettingsWithMode:IS_SELECTED];
        [_commentFriendView styleSettingsWithMode:WITH_NONE];
        [_commentAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:PUSH_FOLLOWER_ONLT]){
        [_commentOffView styleSettingsWithMode:WITH_NONE];
        [_commentFriendView styleSettingsWithMode:IS_SELECTED];
        [_commentAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:PUSH_EVERYONE]){
        [_commentOffView styleSettingsWithMode:WITH_NONE];
        [_commentFriendView styleSettingsWithMode:WITH_NONE];
        [_commentAllView styleSettingsWithMode:IS_SELECTED];
    }
}

-(void)reloadPushFollow
{
    NSString* mode = [GET_DEFAULT(PUSH_FOLLOW) stringValue];
    if([mode isEqualToString:@"0"]){
        [_followOffView styleSettingsWithMode:IS_SELECTED];
        [_followAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:@"1"]){
        [_followOffView styleSettingsWithMode:WITH_NONE];
        [_followAllView styleSettingsWithMode:IS_SELECTED];
    }
}

-(void)reloadPushTag
{
    NSString* mode = GET_DEFAULT(PUSH_TAG);
    if([mode isEqualToString:PUSH_OFF]){
        [_tagOffView styleSettingsWithMode:IS_SELECTED];
        [_tagFriendView styleSettingsWithMode:WITH_NONE];
        [_tagAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:PUSH_FOLLOWER_ONLT]){
        [_tagOffView styleSettingsWithMode:WITH_NONE];
        [_tagFriendView styleSettingsWithMode:IS_SELECTED];
        [_tagAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:PUSH_EVERYONE]){
        [_tagOffView styleSettingsWithMode:WITH_NONE];
        [_tagFriendView styleSettingsWithMode:WITH_NONE];
        [_tagAllView styleSettingsWithMode:IS_SELECTED];
    }
}

-(void)reloadPushFriendRequest
{
    NSString* mode = [GET_DEFAULT(PUSH_FRIEND_REQUEST) stringValue];
    if([mode isEqualToString:@"0"]){
        [_followRequestOffView styleSettingsWithMode:IS_SELECTED];
        [_followRequestAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:@"1"]){
        [_followRequestOffView styleSettingsWithMode:WITH_NONE];
        [_followRequestAllView styleSettingsWithMode:IS_SELECTED];
    }
}

-(void)reloadPushFriendJoin
{
    NSString* mode = [GET_DEFAULT(PUSH_FRIEND_JOIN) stringValue];
    if([mode isEqualToString:@"0"]){
        [_friendJoinOffView styleSettingsWithMode:IS_SELECTED];
        [_friendJoinAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:@"1"]){
        [_friendJoinOffView styleSettingsWithMode:WITH_NONE];
        [_friendJoinAllView styleSettingsWithMode:IS_SELECTED];
    }
}

-(void)reloadPushFriendFirstPost
{
    NSString* mode = GET_DEFAULT(PUSH_FRIEND_FIRST_POST);
    if([mode isEqualToString:PUSH_OFF]){
        [_friendFirstPostOffView styleSettingsWithMode:IS_SELECTED];
        [_friendFirstPostFriendView styleSettingsWithMode:WITH_NONE];
        [_friendFirstPostAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:PUSH_FOLLOWER_ONLT]){
        [_friendFirstPostOffView styleSettingsWithMode:WITH_NONE];
        [_friendFirstPostFriendView styleSettingsWithMode:IS_SELECTED];
        [_friendFirstPostAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:PUSH_EVERYONE]){
        [_friendFirstPostOffView styleSettingsWithMode:WITH_NONE];
        [_friendFirstPostFriendView styleSettingsWithMode:WITH_NONE];
        [_friendFirstPostAllView styleSettingsWithMode:IS_SELECTED];
    }
}

-(void)reloadPushSysNotif
{
    NSString* mode = [GET_DEFAULT(PUSH_SYSTEM_NOTIF) stringValue];
    if([mode isEqualToString:@"0"]){
        [_sysNotifOffView styleSettingsWithMode:IS_SELECTED];
        [_sysNotifAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:@"1"]){
        [_sysNotifOffView styleSettingsWithMode:WITH_NONE];
        [_sysNotifAllView styleSettingsWithMode:IS_SELECTED];
    }
}

-(void)reloadPushLiveNotif
{
    NSString* mode = [GET_DEFAULT(PUSH_LIVESTREAM_NOTIF) stringValue];
    if([mode isEqualToString:@"0"]){
        [_livestreamOffView styleSettingsWithMode:IS_SELECTED];
        [_livestreamAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:@"1"]){
        [_livestreamOffView styleSettingsWithMode:WITH_NONE];
        [_livestreamAllView styleSettingsWithMode:IS_SELECTED];
    }
}

-(void)reloadPushRestreamNotif
{
    NSString* mode = [GET_DEFAULT(PUSH_RESTREAM_NOTIF) stringValue];
    if([mode isEqualToString:@"0"]){
        [_retreamOffView styleSettingsWithMode:IS_SELECTED];
        [_retreamAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:@"1"]){
        [_retreamOffView styleSettingsWithMode:WITH_NONE];
        [_retreamAllView styleSettingsWithMode:IS_SELECTED];
    }
}

-(void)reloadPushSchedule
{
    NSString* mode = [GET_DEFAULT(PUSH_SCHEDULE_NOTIF) stringValue];
    if([mode isEqualToString:@"0"]){
        [_scheduleOffView styleSettingsWithMode:IS_SELECTED];
        [_scheduleAllView styleSettingsWithMode:WITH_NONE];
    }else if([mode isEqualToString:@"1"]){
        [_scheduleOffView styleSettingsWithMode:WITH_NONE];
        [_scheduleAllView styleSettingsWithMode:IS_SELECTED];
    }
}

#pragma mark - update push settings
-(void)changePushLikeAction:(id)sender
{
    UITapGestureRecognizer* tapGesture = sender;
    NSString* mode = @"everyone" ;
    if([tapGesture.view isEqual:_likeOffView]){
        mode = PUSH_OFF;
    }else if([tapGesture.view isEqual:_likeFriendView]){
         mode = PUSH_FOLLOWER_ONLT;
    }else if([tapGesture.view isEqual:_likeAllView]){
         mode = PUSH_EVERYONE;
    }
    
    [DEFAULTS setObject:mode forKey:PUSH_LIKE];
    [DEFAULTS synchronize];
    [self reloadPushLike];

    [API_MANAGER updateUserInfo:@{@"pushLike":mode} fetchSelfInfo:NO completion:^(BOOL success) {
        if(success){
        }
    }];
}

-(void)changePushCommentAction:(id)sender
{
    UITapGestureRecognizer* tapGesture = sender;
    NSString* mode = @"everyone" ;
    if([tapGesture.view isEqual:_commentOffView]){
        mode = PUSH_OFF;
    }else if([tapGesture.view isEqual:_commentFriendView]){
        mode = PUSH_FOLLOWER_ONLT;
    }else if([tapGesture.view isEqual:_commentAllView]){
        mode = PUSH_EVERYONE;
    }
    
    [DEFAULTS setObject:mode forKey:PUSH_COMMENT];
    [DEFAULTS synchronize];
    [self reloadPushComment];

    [API_MANAGER updateUserInfo:@{@"pushComment":mode} fetchSelfInfo:NO completion:^(BOOL success) {
        if(success){
        }
    }];
}

-(void)changePushFollowAction:(id)sender
{
    UITapGestureRecognizer* tapGesture = sender;
    NSString* mode = @"1" ;
    if([tapGesture.view isEqual:_followOffView]){
        mode = @"0";
        [DEFAULTS setObject:@0 forKey:PUSH_FOLLOW];
    }else if([tapGesture.view isEqual:_followAllView]){
        mode = @"1";
        [DEFAULTS setObject:@1 forKey:PUSH_FOLLOW];
    }
    [DEFAULTS synchronize];
    [self reloadPushFollow];

    [API_MANAGER updateUserInfo:@{@"pushFollow":mode} fetchSelfInfo:NO  completion:^(BOOL success) {
        if(success){
        }
    }];
}

-(void)changePushTagAction:(id)sender
{
    UITapGestureRecognizer* tapGesture = sender;
    NSString* mode = @"everyone" ;
    if([tapGesture.view isEqual:_tagOffView]){
        mode = PUSH_OFF;
    }else if([tapGesture.view isEqual:_tagFriendView]){
        mode = PUSH_FOLLOWER_ONLT;
    }else if([tapGesture.view isEqual:_tagAllView]){
        mode = PUSH_EVERYONE;
    }
    
    [DEFAULTS setObject:mode forKey:PUSH_TAG];
    [DEFAULTS synchronize];
    [self reloadPushTag];
    
    [API_MANAGER updateUserInfo:@{@"pushTag":mode} fetchSelfInfo:NO  completion:^(BOOL success) {
        if(success){
        }
    }];
}


-(void)changePushFriendJoinAction:(id)sender
{
    UITapGestureRecognizer* tapGesture = sender;
    NSString* mode = @"1" ;
    if([tapGesture.view isEqual:_friendJoinOffView]){
        mode = @"0";
        [DEFAULTS setObject:@0 forKey:PUSH_FRIEND_JOIN];
    }else if([tapGesture.view isEqual:_friendJoinAllView]){
        mode = @"1";
        [DEFAULTS setObject:@1 forKey:PUSH_FRIEND_JOIN];
    }
    
    [DEFAULTS synchronize];
    [self reloadPushFriendJoin];
    
    [API_MANAGER updateUserInfo:@{@"pushFriendJoin":mode} fetchSelfInfo:NO  completion:^(BOOL success) {
        if(success){
        }
    }];
}

-(void)changePushFriendRequestAction:(id)sender
{
    UITapGestureRecognizer* tapGesture = sender;
    NSString* mode = @"1" ;
    if([tapGesture.view isEqual:_followRequestOffView]){
        mode = @"0";
        [DEFAULTS setObject:@0 forKey:PUSH_FRIEND_REQUEST];
    }else if([tapGesture.view isEqual:_followRequestAllView]){
        mode = @"1";
        [DEFAULTS setObject:@1 forKey:PUSH_FRIEND_REQUEST];
    }
    
    [DEFAULTS synchronize];
    [self reloadPushFriendRequest];
    [API_MANAGER updateUserInfo:@{@"pushAcceptFollowRequest":mode} fetchSelfInfo:NO  completion:^(BOOL success) {
        if(success){
        }
    }];
}


-(void)changePushFriendPostAction:(id)sender
{
    UITapGestureRecognizer* tapGesture = sender;
    NSString* mode = @"everyone" ;
    if([tapGesture.view isEqual:_friendFirstPostOffView]){
        mode = PUSH_OFF;
    }else if([tapGesture.view isEqual:_friendFirstPostFriendView]){
        mode = PUSH_FOLLOWER_ONLT;
    }else if([tapGesture.view isEqual:_friendFirstPostAllView]){
        mode = PUSH_EVERYONE;
    }
    
    [DEFAULTS setObject:mode forKey:PUSH_FRIEND_FIRST_POST];
    [DEFAULTS synchronize];
    [self reloadPushFriendFirstPost];
    
    [API_MANAGER updateUserInfo:@{@"pushFriendFirstPost":mode} fetchSelfInfo:NO  completion:^(BOOL success) {
        if(success){
        }
    }];
}

-(void)changePushSysNotifAction:(id)sender
{
    UITapGestureRecognizer* tapGesture = sender;
    NSString* mode = @"1";
    if([tapGesture.view isEqual:_sysNotifOffView]){
        mode = @"0";
        [DEFAULTS setObject:@0 forKey:PUSH_SYSTEM_NOTIF];
    }else if([tapGesture.view isEqual:_sysNotifAllView]){
        mode = @"1";
        [DEFAULTS setObject:@1 forKey:PUSH_SYSTEM_NOTIF];
    }

    [DEFAULTS synchronize];
    [self reloadPushSysNotif];

    [API_MANAGER updateUserInfo:@{@"pushSystemNotif":mode} fetchSelfInfo:NO  completion:^(BOOL success) {
        if(success){
        }
    }];
}

-(void)changePushLivestreamNotifAction:(id)sender
{
    UITapGestureRecognizer* tapGesture = sender;
    NSString* mode = @"1";
    if([tapGesture.view isEqual:_livestreamOffView]){
        mode = @"0";
        [DEFAULTS setObject:@0 forKey:PUSH_LIVESTREAM_NOTIF];
    }else if([tapGesture.view isEqual:_livestreamAllView]){
        mode = @"1";
        [DEFAULTS setObject:@1 forKey:PUSH_LIVESTREAM_NOTIF];
    }
    
    [DEFAULTS synchronize];
    [self reloadPushLiveNotif];
    
    [API_MANAGER updateUserInfo:@{@"pushLiveStream":mode} fetchSelfInfo:NO  completion:^(BOOL success) {
        if(success){
        }
    }];
}

-(void)changePushRestreamNotifAction:(id)sender
{
    UITapGestureRecognizer* tapGesture = sender;
    NSString* mode = @"1";
    if([tapGesture.view isEqual:_retreamOffView]){
        mode = @"0";
        [DEFAULTS setObject:@0 forKey:PUSH_RESTREAM_NOTIF];
    }else if([tapGesture.view isEqual:_retreamAllView]){
        mode = @"1";
        [DEFAULTS setObject:@1 forKey:PUSH_RESTREAM_NOTIF];
    }
    
    [DEFAULTS synchronize];
    [self reloadPushRestreamNotif];
    
    [API_MANAGER updateUserInfo:@{@"pushLiveRestream":mode} fetchSelfInfo:NO  completion:^(BOOL success) {
        if(success){
        }
    }];
}

-(void)changeScheduleNotifAction:(id)sender
{
    UITapGestureRecognizer* tapGesture = sender;
    NSString* mode = @"1";
    if([tapGesture.view isEqual:_scheduleOffView]){
        mode = @"0";
        [DEFAULTS setObject:@0 forKey:PUSH_SCHEDULE_NOTIF];
    }else if([tapGesture.view isEqual:_scheduleAllView]){
        mode = @"1";
        [DEFAULTS setObject:@1 forKey:PUSH_SCHEDULE_NOTIF];
    }
    
    [DEFAULTS synchronize];
    [self reloadPushSchedule];
    
    [API_MANAGER updateUserInfo:@{@"pushScheduleLiveStream":mode} fetchSelfInfo:NO  completion:^(BOOL success) {
        if(success){
        }
    }];
}

@end
