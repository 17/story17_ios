//
//  NotificationViewController.m
//  story17
//
//  Created by POPO Chen on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "NotificationViewController.h"
#import "FindFriendViewController.h"
#import "Constant.h"
#import "SVPullToRefresh.h"
#import "NotifCell.h"
#import "RequestCell.h"
#import "SystemNotifCell.h"
#import "UserListViewController.h"

@interface NotificationViewController()

@property BOOL needUpdate;
@property (nonatomic,strong) NSMutableArray* updatedArray;

@property int enterFollowPageTimeStamp;
@property int enterSelfPageTimeStamp;
@property int enterSysPageTimeStamp;
@property int visiblefollowCount;
@property int visibleSelfCount;
@property int visibleSysCount;
@property int followBackCount;

@end

@implementation NotificationViewController

#define btnWidth SCREEN_WIDTH/2
#define scrollViewHeight_Self_Following SCREEN_HEIGHT-TAB_BAR_HEIGHT-2*NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT
#define fetchNum 20
#define sectionHeight 25
#define scrollViewHeight_Sys SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT

-(id)init
{
    self = [super init];
    if(self){
        _enterFollowPageTimeStamp=0;
        _enterSelfPageTimeStamp=0;
        _enterSysPageTimeStamp=0;
        _visiblefollowCount=0;
        _visibleSelfCount=0;
        _visibleSysCount=0;

    }
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self configureViewForIOS7];
    if (_enterMode==ENTER_MODE_SYS) {
        self.title = LOCALIZE(@"Sys_notif");
    }else{
        self.title = LOCALIZE(@"Notification");
    }
    self.navigationController.title = @"";
    
    _followNotifArray = CREATE_MUTABLE_ARRAY;
    _selfNotifArray = CREATE_MUTABLE_ARRAY;
    _systemNotifArray = CREATE_MUTABLE_ARRAY;
    _requestArray = CREATE_MUTABLE_ARRAY;
    
    _updatedArray = CREATE_MUTABLE_ARRAY;
    _needUpdate = YES;
    
    _isFirstFetching = true;

    [self setup];
    
    if (_enterMode==ENTER_MODE_SELF_FOLLOWING) {
        [self fetchData:true withMode:-1];
    }else{
        [self fetchData:true withMode:2];
        [self switchToMode:2 animated:YES];
        
    }

    [NOTIFICATION_CENTER addObserver:self selector:@selector(fetchNotif:) name:NEW_FRIEND_REQUEST object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(fetchNotif:) name:NEW_FOLLOW object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(fetchNotif:) name:NEW_LIKE object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(fetchNotif:) name:NEW_COMMENT object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(fetchNotif:) name:NEW_POST_TAG object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(fetchNotif:) name:NEW_COMMENT_TAG object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(fetchNotif:) name:NEW_FRIEND_JOIN_FROM_CONTACTS object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(fetchNotif:) name:NEW_FRIEND_JOIN_FROM_FB object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(fetchNotif:) name:NEW_SYSTEM_NOTIF object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(followUser) name:FOLLOW_IN_NOTIFYCELL object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    
//    [API_MANAGER sendSysNotif:GET_DEFAULT(USER_ID) withMessage:@"YAAAAAAAAAAA" withCompletion:^(BOOL success) {
//        
//        if (success) {
//            DLog(@"sajodsajodasjodasjod");
//        }
//    }];
    
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    _visibleSysCount=0;
    _visibleSelfCount=0;
    _visiblefollowCount=0;
    _followBackCount=0;
    
    [SINGLETON reloadAllBadge];
    if (_enterMode==ENTER_MODE_SYS) {
        [self addCustomNavigationBackButton];
        [DEFAULTS setObject:@0 forKey:SYSTEM_NOTIF_BADGE];
        [DEFAULTS synchronize];
        [EVENT_HANDLER addEventTracking:@"EnterSysPage" withDict:nil];
        _enterSysPageTimeStamp=CURRENT_TIMESTAMP;
        [API_MANAGER updateUserInfo:@{@"systemNotifBadge":@"0"}  fetchSelfInfo:YES  completion:^(BOOL success) {
        }];

    }else{
        [DEFAULTS setObject:@0 forKey:NOTIF_BADGE];
        [DEFAULTS setObject:@0 forKey:MESSGAE_BADGE];
        [DEFAULTS setObject:@0 forKey:LIKE_BADGE];
        [DEFAULTS setObject:@0 forKey:COMMENT_BADGE];
        [DEFAULTS synchronize];
        [EVENT_HANDLER addEventTracking:@"EnterSelfFollowPage" withDict:nil];
        _enterSelfPageTimeStamp=CURRENT_TIMESTAMP;
        [API_MANAGER updateUserInfo:@{@"notifBadge":@"0",@"likeBadge":@"0",@"commentBadge":@"0",@"messageBadge":@"0"}  fetchSelfInfo:YES  completion:^(BOOL success) {
        }];

    }
    
    if([GET_DEFAULT(UPDATE_NOTIF_TIME) intValue] < CURRENT_TIMESTAMP-UPDATE_INTERVAL){
        
        _needUpdate = YES;
        [_updatedArray removeAllObjects];
        
        /* reload current mode */
        [self fetchData:true withMode:-1];
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    /* Level 5 crash fixed */
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (_enterMode==ENTER_MODE_SYS) {
        [EVENT_HANDLER addEventTracking:@"LeaveSysPage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterSysPageTimeStamp),@"cellCount":INT_TO_STRING(_visibleSysCount)}];
    }else{
        if (_viewMode==NotificationViewModeSelf) {
            [EVENT_HANDLER addEventTracking:@"LeaveSelfFollowPage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterFollowPageTimeStamp),@"followBackCount":INT_TO_STRING(_followBackCount),@"cellCount":INT_TO_STRING(_visibleSelfCount)}];
        }else{
            [EVENT_HANDLER addEventTracking:@"LeaveFollowPage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterFollowPageTimeStamp),@"cellCount":INT_TO_STRING(_visiblefollowCount)}];

        }
    }
    
}

-(void)dealloc
{
    [NOTIFICATION_CENTER removeObserver:self];
}

-(void)fetchNotif:(id)sender
{
    NSNotification* s = sender;
    NSString* pushType = s.name;
    
    if([pushType isEqualToString:NEW_FOLLOW]){
        [self fetchData:YES withMode:0];
        [self fetchData:YES withMode:1];
    }else if( [pushType isEqualToString:NEW_COMMENT]){
        [self fetchData:YES withMode:0];
        [self fetchData:YES withMode:1];
    }else if( [pushType isEqualToString:NEW_POST_TAG]){
        [self fetchData:YES withMode:0];
        [self fetchData:YES withMode:1];
    }else if( [pushType isEqualToString:NEW_COMMENT_TAG]){
        [self fetchData:YES withMode:0];
        [self fetchData:YES withMode:1];
    }else if( [pushType isEqualToString:NEW_FRIEND_JOIN_FROM_CONTACTS]){
        [self fetchData:YES withMode:0];
        [self fetchData:YES withMode:1];
    }else if( [pushType isEqualToString:NEW_FRIEND_JOIN_FROM_FB]){
        [self fetchData:YES withMode:0];
        [self fetchData:YES withMode:1];
    }else if( [pushType isEqualToString:NEW_SYSTEM_NOTIF]){
        [self fetchData:YES withMode:2];
    }
}


-(void)setup
{
    _viewMode = NotificationViewModeSelf;



    UIButton* findFriendBtn = [ThemeManager findFriendBtn];
    [findFriendBtn addTarget:self action:@selector(didClickFindFriend:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:findFriendBtn];
    if (_enterMode==ENTER_MODE_SELF_FOLLOWING) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:findFriendBtn];
        
    }
    _mainView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
    
    _selfBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, btnWidth, NAVI_BAR_HEIGHT)];
    _selfBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [_selfBtn setTitle:LOCALIZE(@"Self") forState:UIControlStateNormal];
    [_selfBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_selfBtn setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
    [_selfBtn addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];
    [_selfBtn setSelected:YES];
    
//    _followBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/3, 0, btnWidth, NAVI_BAR_HEIGHT)];
    _followBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2, 0, btnWidth, NAVI_BAR_HEIGHT)];

    _followBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [_followBtn setTitle:LOCALIZE(@"Following") forState:UIControlStateNormal];
    [_followBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_followBtn setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
    [_followBtn addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];
    
//    _sysNotifBtn = [[UIButton alloc] initWithFrame:CGRectMake(2*SCREEN_WIDTH/3, 0, btnWidth, NAVI_BAR_HEIGHT)];
    _sysNotifBtn = [[UIButton alloc] initWithFrame:CGRectMake(2*SCREEN_WIDTH/3, 0, btnWidth, NAVI_BAR_HEIGHT)];

    _sysNotifBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [_sysNotifBtn setTitle:LOCALIZE(@"SysNotif") forState:UIControlStateNormal];
    [_sysNotifBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_sysNotifBtn setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
    [_sysNotifBtn addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];

    _btnArray = @[_selfBtn,_followBtn,_sysNotifBtn];
 
    
    _modeSelectorContainer = [ThemeManager maintabBackgroundImageView];
    _modeSelectorContainer.frame = CGRectMake(0, 0, SCREEN_WIDTH, NAVI_BAR_HEIGHT);
    _modeSelectorContainer.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
    [_modeSelectorContainer setBackgroundColor:[UIColor clearColor]];
    
    _modeIndicatorImageView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"bar")];
//    _modeIndicatorImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH/3, 3);
    _modeIndicatorImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH/2, 3);

    _modeIndicatorImageView.layer.masksToBounds = YES;
    _modeIndicatorImageView.center = CGPointMake(SCREEN_WIDTH/4, NAVI_BAR_HEIGHT-3);
//    _modeIndicatorImageView.center = CGPointMake(SCREEN_WIDTH/6, NAVI_BAR_HEIGHT-3);
    [_modeSelectorContainer addSubview:_modeIndicatorImageView];
    [_modeSelectorContainer addSubview:_followBtn];
    [_modeSelectorContainer addSubview:_selfBtn];
    [_modeSelectorContainer addSubview:_sysNotifBtn];
    _sysNotifBtn.hidden=YES;

    if (_enterMode==ENTER_MODE_SYS) {
        _followBtn.hidden=YES;
        _selfBtn.hidden=YES;
        _modeSelectorContainer.hidden=YES;
    }
    
//    [_modeSelectorContainer addSubview:_sysNotifBtn];
    
    _mainScrollView = [ThemeManager getBouncedScrollView];
    if (_enterMode==ENTER_MODE_SELF_FOLLOWING) {
        _mainScrollView.frame = CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, scrollViewHeight_Self_Following);
    }else{
        _mainScrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH, scrollViewHeight_Sys);
    }
    _mainScrollView.delegate = self;

    if (_enterMode==ENTER_MODE_SELF_FOLLOWING) {
        [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH*2, _mainScrollView.frame.size.height)];

    }else{
    
        [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, _mainScrollView.frame.size.height)];
    }

    /* CollectionView Setting */
    
    __weak NotificationViewController *weakSelf = self;
        _followTableView = [[UITableView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, scrollViewHeight_Self_Following) style:UITableViewStylePlain];

    _followTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    [_followTableView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData:TRUE withMode:-1];
    }];
    
    
        _selfTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, scrollViewHeight_Self_Following)];
    
    
    _selfTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_selfTableView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData:TRUE withMode:-1];
    }];
    
    _systemTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, scrollViewHeight_Sys) style:UITableViewStylePlain];
    _systemTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_systemTableView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData:TRUE withMode:-1];
    }];
    [self tableViewSettings:_followTableView];
    [self tableViewSettings:_selfTableView];
    [self tableViewSettings:_systemTableView];
    
    _tableViewArray = @[_selfTableView,_followTableView,_systemTableView];
    _notifArray = @[_selfNotifArray,_followNotifArray,_systemNotifArray];
    
    
    /* no data view & progress view settings */
    
    _noDataImageView = [ThemeManager getNoDataImageView];
    _noDataImageView2 = [ThemeManager getNoDataImageView];
    _noDataImageView3 = [ThemeManager getNoDataImageView];

    CGRect rect = _noDataImageView.frame;
    _noDataImageView.frame = rect;
    rect.origin.x += SCREEN_WIDTH;
    _noDataImageView2.frame = rect;
    rect.origin.x += SCREEN_WIDTH;
    _noDataImageView3.frame = rect;

    _noDataArray = @[_noDataImageView,_noDataImageView2,_noDataImageView3];
    
    _progressView = [UIActivityIndicatorView new];
    _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView setColor:MAIN_COLOR];
    
    _progressView2 = [UIActivityIndicatorView new];
    _progressView2.frame = CGRectMake(SCREEN_WIDTH/2-15+SCREEN_WIDTH, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView2 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView2 setColor:MAIN_COLOR];
    
    _progressView3 = [UIActivityIndicatorView new];
    _progressView3.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView3 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView3 setColor:MAIN_COLOR];
    
    _progressArray = @[_progressView,_progressView2,_progressView3];
    
    if (_enterMode==ENTER_MODE_SYS) {
        [_mainScrollView addSubview:_systemTableView];
    }else{
        [_mainScrollView addSubview:_followTableView];
        [_mainScrollView addSubview:_selfTableView];
    }
    [_mainScrollView addSubview:_noDataImageView];
    [_mainScrollView addSubview:_noDataImageView2];
    [_mainScrollView addSubview:_noDataImageView3];

    [_mainScrollView addSubview:_progressView];
    [_mainScrollView addSubview:_progressView2];
    if (_enterMode==ENTER_MODE_SYS) {
        [_mainScrollView addSubview:_progressView3];
    }

    [_mainView addSubview:_mainScrollView];
    [_mainView addSubview:_modeSelectorContainer];
    [self.view addSubview:_mainView];
    
}

-(void)fetchData:(BOOL)refresh withMode:(int)viewMode
{
    if(refresh){
        _noMoreData = false;
    }
    if(_isFetching || _noMoreData) return;
    
    int modeChange = viewMode==-1?_viewMode:viewMode;
    
    /* current view */
    NSMutableArray* notifArray = _notifArray[modeChange];
    UILabel* noDataView = _noDataArray[modeChange];
    
    if(_enterMode == ENTER_MODE_SYS){
        noDataView = _noDataArray[0];
    }
    
    UITableView* tableView = _tableViewArray[modeChange];
    UIActivityIndicatorView* progressView = _progressArray[modeChange];
    int currentMode = modeChange;
    
    /* Handle updated */
    if(refresh){
        if(![_updatedArray containsObject:INT_TO_STRING(currentMode)]){
           [_updatedArray addObject:INT_TO_STRING(currentMode)];
        }
        
        if(((int)[_updatedArray count]==1 && _enterMode == ENTER_MODE_SYS) || (int)[_updatedArray count]==2){
            [_updatedArray removeAllObjects];
            _needUpdate = NO;
        }
        
        if([tableView isEqual:_selfTableView]){
            [API_MANAGER getFollowRequests:20 beforeTime:CURRENT_TIMESTAMP withCompletion:^(BOOL success , NSArray* users) {
                [_requestArray removeAllObjects];
                if(success){
                    [_requestArray addObjectsFromArray:users];
                }else{
                }
                [tableView reloadData];
            }];
        }
    }

    
    PostObject* lastPostObject = NULL;
    int beforeTime = INT32_MAX;
    int offset = 0;
    
    if([notifArray count]>0){
        lastPostObject = notifArray[[notifArray count]-1];
        if(!refresh){
            offset = (int)[notifArray count];
        }
    }
    
    if(lastPostObject==NULL || refresh){
        beforeTime = INT32_MAX;
    }else{
        beforeTime = lastPostObject.timestamp;
    }
    
    _isFetching = true;
    
    if((viewMode==-1 || refresh) && !_isFirstFetching){
        [tableView.pullToRefreshView startAnimating];
    }
    
    if(_isFirstFetching){
        [progressView startAnimating];
        _isFirstFetching = false;
    }
    
    [self callApiWithMode:currentMode andBeforeTime:beforeTime withCompletion:^(BOOL success, NSArray *dict) {

        
        [progressView stopAnimating];
        [tableView.pullToRefreshView stopAnimating];
        _isFetching = false;
        
        if([dict count]==0 && [notifArray count]==0){
            noDataView.hidden = NO;
        }else{
            noDataView.hidden = YES;
        }
        
        if(success){
            DLog(@"Stop");
            
            if(refresh){
                [DEFAULTS setObject:INT_TO_NUMBER(CURRENT_TIMESTAMP) forKey:UPDATE_NOTIF_TIME];
                [DEFAULTS synchronize];
                [notifArray removeAllObjects];
            }
            
            [notifArray addObjectsFromArray:dict];
            
            if([dict count]<fetchNum){
                _noMoreData = true;
            }
            
            // 如果跑到一半換 mode 本來這個 collectionview reload就變成另一個 to avoid wrong reoload
            if(currentMode==modeChange){
                DLog(@"reload");
                [tableView reloadData];
            }
            
        }

    }];
}


#pragma mark - UITableViewDelegagte
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if([tableView isEqual:_systemTableView]){
        NotificationObject* notifObj = _systemNotifArray[indexPath.row];
        return [SystemNotifCell getCellHeigthWithNotif:notifObj];
    }else if([tableView isEqual:_followTableView]){
        NotificationObject* notifObj = _followNotifArray[indexPath.row];
        return [NotifCell getCellHeigthWithNotif:notifObj];
    }else if([tableView isEqual:_selfTableView]){
        if(indexPath.section==0 && [_requestArray count]!=0){
            if([_requestArray count]!=0){
                return [RequestCell getCellHeigt];
            }
        }else{
            NotificationObject* notifObj = _selfNotifArray[indexPath.row];
            return [NotifCell getCellHeigthWithNotif:notifObj];
        }
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView isEqual:_selfTableView] && indexPath.section==0 && [_requestArray count]!=0){
        UserListViewController* ulVC  = [UserListViewController new];
        ulVC.mode = REQUEST_MODE;
        ulVC.users = [_requestArray mutableCopy];
        ulVC.title = LOCALIZE(@"FriendRequest");
        ulVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:ulVC animated:YES];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([tableView isEqual:_followTableView]){
        DLog(@"%d %lu %lu",_viewMode, (unsigned long)[_followNotifArray count],(unsigned long)[(NSArray*)_notifArray[_viewMode] count]);
        return [_followNotifArray count];
    }else if([tableView isEqual:_selfTableView]){
        DLog(@"%d  %lu %lu",_viewMode, (unsigned long)[_selfNotifArray count],(unsigned long)[(NSArray*)_notifArray[_viewMode] count]);
        if(section==0 && [_requestArray count]!=0){
            return 1;
        }
        return [_selfNotifArray count];
    }else{
        DLog(@"%d  %lu %lu",_viewMode, (unsigned long)[_systemNotifArray count],(unsigned long)[(NSArray*)_notifArray[_viewMode] count]);
        return [_systemNotifArray count];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([tableView isEqual:_selfTableView] &&  [_requestArray count]!=0){
        return 2;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView==_selfTableView && section==1 && [_requestArray count]!=0){
        return 0;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, sectionHeight)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-20, sectionHeight)];
    [label setTextColor:DARK_GRAY_COLOR];
    [label setFont: BOLD_FONT_WITH_SIZE(14) ];
    label.textAlignment = NSTextAlignmentLeft;
    [view setBackgroundColor:LIGHT_GRAY_COLOR];
    [label setText:@"message"];
    
    [view addSubview:label];
    [view setAlpha:0.8];
    
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView isEqual:_followTableView]){
        
        NotifCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotifCell"];
        
        if(cell==nil){
            cell = [[NotifCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NotifCell"];
        }
        
        NotificationObject* notif = _followNotifArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.navCtrl = self.navigationController;
        [cell reloadCell:notif];
        
        if(indexPath.row>[_followNotifArray count]-fetchNum/3){
            [self fetchData:false withMode:1];
        }
        _visiblefollowCount++;
        return cell;
        
    }else if( [tableView isEqual:_selfTableView]){
        
        if(indexPath.section==0 && [_requestArray count]!=0){
            
            /* Friend Request Cell */
            RequestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RequestCell"];
            
            if(cell==nil){
                cell = [[RequestCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RequestCell"];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell reloadCell:_requestArray];
            return cell;

        }else{
            NotifCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FriendNotifCell"];
            
            if(cell==nil){
                cell = [[NotifCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FriendNotifCell"];
            }
            
            NotificationObject* notif = _selfNotifArray[indexPath.row];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.navCtrl = self.navigationController;
            [cell reloadCell:notif];
            [cell isReadSettings];
            
            if(indexPath.row>[_selfNotifArray count]-fetchNum/3){
                [self fetchData:false withMode:0];
            }
            _visibleSelfCount++;
            return cell;
        
        }
        
    }else{
        
        SystemNotifCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SystemNotifCell"];
        if(cell==nil){
            cell = [[SystemNotifCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SystemNotifCell"];
        }
        NotificationObject* notif = _systemNotifArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.navCtrl = self.navigationController;
        [cell reloadCell:notif];
        
        if(indexPath.row>[_systemNotifArray count]-fetchNum/3){
            [self fetchData:false withMode:2];
        }
        _visibleSysCount++;
        return cell;
    }
}

#pragma mark - API Action with callback
-(void)callApiWithMode:(int)mode andBeforeTime:(int)beforeTime withCompletion:(void(^)(BOOL success,NSArray* dict))callback
{
    switch (mode) {
        case NotificationViewModeFollower:
        {
            [API_MANAGER getFriendNotif:beforeTime andCount:fetchNum withCompletion:^(BOOL success, NSArray *dict) {
                callback(success,dict);
            }];
            break;
        }
        case NotificationViewModeSelf:
        {
            [API_MANAGER getNotif:beforeTime andCount:fetchNum withCompletion:^(BOOL success, NSArray *dict) {
                callback(success,dict);
            }];
            break;
        }
        case NotificationViewModeSystem:
        {
            [API_MANAGER getSystemNotif:beforeTime andCount:fetchNum withCompletion:^(BOOL success, NSArray *dict) {
                callback(success,dict);
                [API_MANAGER readSystemNotifWithCompletion:^(BOOL success) {}];
            }];
            break;
        }
            
        default:
            DLog(@"Err: callApiWithMode unknown %d", mode);
            break;
    }
}

-(void)changeModeAction:(id)sender
{
    if([sender isEqual:_followBtn]){
        [self switchToMode:NotificationViewModeFollower animated:YES];
    }else if ([sender isEqual:_selfBtn]){
        [self switchToMode:NotificationViewModeSelf animated:YES];
        [API_MANAGER readNotif:^(BOOL success) {}];
    }else if([sender isEqual:_sysNotifBtn]){
        [self switchToMode:NotificationViewModeSystem animated:YES];
    }
}

-(void) switchToMode:(int)mode animated:(BOOL) animated
{
    if(_viewMode==mode) {
        return;
    }
    _viewMode = mode;
    NSMutableArray* notifs = _notifArray[mode];

    if(_needUpdate){
        if((int)[notifs count]==0){
            _isFirstFetching = true;
        }
        _isFetching = false;
        [self fetchData:true withMode:_viewMode];
    }
    switch (_viewMode) {
        case NotificationViewModeSelf:
        {
            [EVENT_HANDLER addEventTracking:@"LeaveFollowPage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterFollowPageTimeStamp),@"cellCount":INT_TO_STRING(_visiblefollowCount)}];
            
            [EVENT_HANDLER addEventTracking:@"EnterSelfFollowPage" withDict:nil];

            _enterSelfPageTimeStamp=CURRENT_TIMESTAMP;
            [_selfBtn setSelected:YES];
            [_followBtn setSelected:NO];
            [_sysNotifBtn setSelected:NO];
            break;
        }
        case NotificationViewModeFollower:
        {
            [EVENT_HANDLER addEventTracking:@"LeaveSelfFollowPage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterFollowPageTimeStamp),@"followBackCount":INT_TO_STRING(_followBackCount),@"cellCount":INT_TO_STRING(_visibleSelfCount)}];
            [EVENT_HANDLER addEventTracking:@"EnterFollowPage" withDict:nil];
            _followBackCount=0;
            _enterFollowPageTimeStamp=CURRENT_TIMESTAMP;
            
            [_selfBtn setSelected:NO];
            [_followBtn setSelected:YES];
            [_sysNotifBtn setSelected:NO];
            break;
        }
        case NotificationViewModeSystem: //not use now
        {

            [_selfBtn setSelected:NO];
            [_followBtn setSelected:NO];
            [_sysNotifBtn setSelected:YES];
            break;
        }
            
        default:
            break;
    }
    
    [self reloadUIAnimated:animated];
}

-(void) reloadUIAnimated:(BOOL)animated
{
    for (int mode=0; mode < 3; mode++) {
        UIButton* modeButton = _btnArray[mode];
        UIScrollView *tableView = _tableViewArray[mode];
        
        if(mode == _viewMode) {

            tableView.scrollsToTop = YES;
            modeButton.userInteractionEnabled = NO;
            
            [_mainScrollView scrollRectToVisible:CGRectMake(SCREEN_WIDTH*mode, 0, SCREEN_WIDTH, _mainScrollView.frame.size.height) animated:YES];
            
            if(animated) {
                [UIView animateWithDuration:0.2 animations:^{
                    _modeIndicatorImageView.center = CGPointMake((SCREEN_WIDTH/4)+(SCREEN_WIDTH/2)*mode, NAVI_BAR_HEIGHT-1);
//                    _modeIndicatorImageView.center = CGPointMake((SCREEN_WIDTH/6)+(SCREEN_WIDTH/3)*mode, NAVI_BAR_HEIGHT-1);

                }];
            } else {
//                _modeIndicatorImageView.center = CGPointMake((SCREEN_WIDTH/6)+(SCREEN_WIDTH/3)*mode, NAVI_BAR_HEIGHT-1);
                _modeIndicatorImageView.center = CGPointMake((SCREEN_WIDTH/4)+(SCREEN_WIDTH/2)*mode, NAVI_BAR_HEIGHT-1);

            }
            
        }else {
            
            tableView.scrollsToTop = NO;
            modeButton.userInteractionEnabled = YES;
        }
    }
}


#pragma mark UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // to avoid refresh action go this way
    if([_tableViewArray containsObject:scrollView]) return;
    
    if(scrollView.contentOffset.x==0) {
        [self switchToMode:0 animated:YES];
    }else if(scrollView.contentOffset.x==SCREEN_WIDTH) {
        [self switchToMode:1 animated:YES];
    }else if(scrollView.contentOffset.x==SCREEN_WIDTH*2) {
        [self switchToMode:2 animated:YES];
    }
}


/* UI Settings */
-(void) tableViewSettings:(UITableView*)tableView
{
    tableView.delegate = self;
    tableView.dataSource = self;
    
    if([tableView isEqual:_selfTableView]){
        
        [tableView registerClass:[NotifCell class] forCellReuseIdentifier:@"FriendNotifCell"];
        [tableView registerClass:[RequestCell class] forCellReuseIdentifier:@"RequestCell"];
    
    } else if([tableView isEqual:_followTableView]){
        
        [tableView registerClass:[NotifCell class] forCellReuseIdentifier:@"NotifCell"];
    
    }else{
        [tableView registerClass:[SystemNotifCell class] forCellReuseIdentifier:@"SystemNotifCell"];
    }
}

-(void)didClickFindFriend:(id)sender
{
    FindFriendViewController* findFriendVC = [FindFriendViewController new];
    findFriendVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:findFriendVC animated:YES];
}

-(void)followUser
{
    _followBackCount++;
}

@end
