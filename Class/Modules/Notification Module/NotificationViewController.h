//
//  NotificationViewController.h
//  story17
//
//  Created by POPO Chen on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#define ENTER_MODE_SYS 0
#define ENTER_MODE_SELF_FOLLOWING 1
@interface NotificationViewController : UIViewController <UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) UIView* mainView;

/* mode indicator */
@property(nonatomic,strong) UIView* modeSelectorContainer;
@property(nonatomic,strong) UIImageView* modeIndicatorImageView;

/* mode btn array */
@property(nonatomic,strong) NSArray* btnArray;
@property(nonatomic,strong) UIButton* followBtn;
@property(nonatomic,strong) UIButton* selfBtn;
@property(nonatomic,strong) UIButton* sysNotifBtn;

/* main scrollview */
@property(nonatomic,strong) UIScrollView* mainScrollView;
@property(nonatomic,strong) NSArray* tableViewArray;
@property(nonatomic,strong) UITableView* followTableView;
@property(nonatomic,strong) UITableView* selfTableView;
@property(nonatomic,strong) UITableView* systemTableView;

/* data array */
@property (nonatomic,strong) NSArray* notifArray;
@property (nonatomic,strong) NSMutableArray* followNotifArray;
@property (nonatomic,strong) NSMutableArray* requestArray;  /* Only on selfNotif */
@property (nonatomic,strong) NSMutableArray* selfNotifArray;
@property (nonatomic,strong) NSMutableArray* systemNotifArray;

/* progress view */
@property (nonatomic,strong) NSArray* progressArray;
@property (nonatomic,strong) UIActivityIndicatorView* progressView;
@property (nonatomic,strong) UIActivityIndicatorView* progressView2;
@property (nonatomic,strong) UIActivityIndicatorView* progressView3;

/* no data view */
@property (nonatomic,strong) NSArray* noDataArray;
@property (nonatomic,strong) UIImageView* noDataImageView;
@property (nonatomic,strong) UIImageView* noDataImageView2;
@property (nonatomic,strong) UIImageView* noDataImageView3;


@property  int viewMode;    // PHOTO_LEFT PHOTO_RIGHT

@property BOOL isFirstFetching;
@property BOOL isFetching;
@property BOOL noMoreData;
@property BOOL enterMode;

@end
