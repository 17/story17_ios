#import "NotificationObject.h"
#import "PostObject.h"
#import "UserObject.h"

@implementation NotificationObject

@synthesize title;
@synthesize message;
@synthesize timestamp;
@synthesize isRead;
@synthesize targetUserID;
@synthesize targetUserPicture;

+(NotificationObject*) getNotifWithDict:(NSDictionary*)notifInfoDict
{
 
    NotificationObject* notifObject = [NotificationObject new];
    
    notifObject.title = notifInfoDict[@"title"];
    notifObject.message = notifInfoDict[@"message"];
    notifObject.commentID = notifInfoDict[@"commentID"];
    notifObject.postID = notifInfoDict[@"postID"];
    notifObject.post = [PostObject getPostWithDict:notifInfoDict[@"postInfo"]];
    
    notifObject.type = notifInfoDict[@"type"];
    notifObject.friendUserID = notifInfoDict[@"friendUserID"];
    notifObject.user = [UserObject getUserWithDict:notifInfoDict[@"friendUserInfo"]];   // friend User Info
    notifObject.timestamp = [notifInfoDict[@"timestamp"] intValue];
    notifObject.isRead = [notifInfoDict[@"isRead"] intValue];
    
    if([notifObject.postID isEqualToString:@""]){
        
        notifObject.targetUserPicture = notifInfoDict[@"targetUserInfo"][@"picture"];
        notifObject.targetUserID = notifInfoDict[@"targetUserID"];
    
    }
    
    return notifObject;
}

@end
