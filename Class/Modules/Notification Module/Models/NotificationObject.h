#import <Foundation/Foundation.h>

@class PostObject;
@class UserObject;

@interface NotificationObject : NSObject

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* message;
@property (nonatomic, strong) NSString* commentID;
@property (nonatomic, strong) PostObject* post;
@property (nonatomic, strong) NSString* postID;
@property (nonatomic, strong) NSString* postPicture;

@property (nonatomic, strong) NSString* type;
@property (nonatomic, strong) NSString* friendUserID;
@property (nonatomic, strong) UserObject* user;

@property (nonatomic, strong) NSString* targetUserID; // openID
@property (nonatomic, strong) NSString* targetUserPicture;

@property int timestamp;
@property int isRead;

+(NotificationObject*) getNotifWithDict:(NSDictionary*)notifInfoDict;

@end
