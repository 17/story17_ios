//
//  RequestUserCell.m
//  story17
//
//  Created by POPO Chen on 8/26/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "RequestUserCell.h"
#import "UIImageView+AFNetworking.h"

@implementation RequestUserCell

#define cellHeight 55
#define imageWidth 38


+ (float)getCellHeigt
{
    return cellHeight;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20+2.5*imageWidth, 0, 0, 0)];
        [_titleLabel setTextColor:DARK_GRAY_COLOR];
        _titleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        
        _userImageView = [self getCircleImageView];
        _userImageView.frame = CGRectMake(10, (cellHeight-imageWidth)/2, imageWidth, imageWidth);

        _rejectButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-imageWidth-10, cellHeight/2-imageWidth/2, imageWidth, imageWidth)];
        [_rejectButton setImage:IMAGE_FROM_BUNDLE(@"no") forState:UIControlStateNormal];
        [_rejectButton setImage:IMAGE_FROM_BUNDLE(@"no") forState:UIControlStateHighlighted];

        _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-2*imageWidth-20, cellHeight/2-imageWidth/2, imageWidth, imageWidth)];
        [_acceptButton setImage:IMAGE_FROM_BUNDLE(@"yes") forState:UIControlStateNormal];
        [_acceptButton setImage:IMAGE_FROM_BUNDLE(@"yes") forState:UIControlStateHighlighted];
        
        [_acceptButton addTarget:self action:@selector(didclickButton:) forControlEvents:UIControlEventTouchUpInside];
        [_rejectButton addTarget:self action:@selector(didclickButton:) forControlEvents:UIControlEventTouchUpInside];
        
        UIView* linveView = [ThemeManager separaterLine];
        linveView.frame = CGRectMake(0, cellHeight-0.5, SCREEN_WIDTH, 0.5);
        
        [self.contentView addSubview:_titleLabel];
        [self.contentView addSubview:_userImageView];
        [self.contentView addSubview:linveView];
        [self.contentView addSubview:_acceptButton];
        [self.contentView addSubview:_rejectButton];
        
    }
    return self;
}

-(void)reloadCell:(UserObject*)user;
{
    
    _user = user;
    
    [_titleLabel setText:user.openID];
    [_titleLabel sizeToFit];
    _titleLabel.center = CGPointMake(20+imageWidth+_titleLabel.frame.size.width/2, cellHeight/2);
    [_userImageView setImageWithURL:S3_THUMB_IMAGE_URL(user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    
}

-(UIImageView*) getCircleImageView
{
    UIImageView* imageView =  [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imageWidth, imageWidth)];
    imageView.layer.cornerRadius = imageWidth/2;
    imageView.layer.masksToBounds = YES;
    imageView.layer.borderWidth = 2;
    imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    return imageView;
}

-(void)didclickButton:(UIButton*)btn
{
    if([btn isEqual:_acceptButton]){
        [_delegate didClickAcceptRequestButton:self userID:[_user.userID copy]];
    }else if([btn isEqual:_rejectButton]){
        [_delegate didClickRejectRequestButton:self userID:[_user.userID copy]];
    }
}

@end
