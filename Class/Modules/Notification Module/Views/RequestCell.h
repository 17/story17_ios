//
//  RequestCell.h
//  story17
//
//  Created by POPO Chen on 8/26/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestCell : UITableViewCell


@property (nonatomic,strong) UIButton* badgeButton;
@property (nonatomic,strong) UIImageView* coverPhoto1;
@property (nonatomic,strong) UIImageView* coverPhoto2;
@property (nonatomic,strong) UIImageView* coverPhoto3;

@property (nonatomic,strong) UIImageView* arrowImageView;
@property (nonatomic,strong) UILabel* titleLabel;
@property (nonatomic,strong) UILabel* subtitleLabel;

-(void)reloadCell:(NSArray*)requestUsers;
+ (float)getCellHeigt;

@end
