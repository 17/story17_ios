//
//  NotifCell.m
//  Story17
//
//  Created by POPO on 5/8/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "NotifCell.h"
#import "NotificationObject.h"
#import "Constant.h"
#import "UIImageView+AFNetworking.h"
#import "UserProfileViewController.h"
#import "SinglePostViewController.h"

@implementation NotifCell

#define UserTitleImageSize 45
#define titleTextSize 14
#define titleDefaultHeight 30
#define subTitleDefaultHeight 15

@synthesize navCtrl;


+ (float)getCellHeigthWithNotif:(NotificationObject*)notifObj
{
    
    NSString* titleString = @"";
    NSString* userOpenID = notifObj.user.openID;
    
    if([notifObj.type isEqualToString:POST_LIKE]){
        if([notifObj.post.user.userID isEqualToString:MY_USER_ID]){
            titleString = [NSString stringWithFormat:LOCALIZE(@"post_like_your_notification"),userOpenID];
        }else{
            if(notifObj.post.user!=nil){
                titleString = [NSString stringWithFormat:LOCALIZE(@"post_like_notification"),userOpenID,notifObj.post.user.openID];
            }else{
                titleString = [NSString stringWithFormat:LOCALIZE(@"post_like_notification"),userOpenID,@""];
            }
        }
    }else if([notifObj.type isEqualToString:POST_COMMENT]){
        titleString = [NSString stringWithFormat:LOCALIZE(@"post_comment_notification"),userOpenID];
    }else if([notifObj.type isEqualToString:POST_TAG]){
        titleString = [NSString stringWithFormat:LOCALIZE(@"post_tag_notification"),userOpenID];
    }else if([notifObj.type isEqualToString:COMMENT_TAG]){
        titleString = [NSString stringWithFormat:LOCALIZE(@"comment_tag_notification"),userOpenID];
    }else if([notifObj.type isEqualToString:NEW_FRIEND_JOIN_CONTACT]){
        
        titleString = [NSString stringWithFormat:LOCALIZE(@"new_contact_friend_joined"),userOpenID];
    
    }else if([notifObj.type isEqualToString:NEW_FRIEND_JOIN_FB]){
        titleString = [NSString stringWithFormat:LOCALIZE(@"new_fb_friend_joined"),userOpenID];
    }else if([notifObj.type isEqualToString:NEW_FRIEND_FIRST_POST]){
        titleString = [NSString stringWithFormat:LOCALIZE(@"new_friend_first_post"),userOpenID];
    }else if([notifObj.type isEqualToString:NEW_FOLLOW_TYPE]){
        if(notifObj.targetUserID!=nil && ![notifObj.targetUserID isKindOfClass:[NSNull class]]){
           
            if([notifObj.targetUserID isEqualToString:MY_USER_ID]){
                titleString = [NSString stringWithFormat:LOCALIZE(@"self_follow_notification"),userOpenID];
            }else{
                titleString = [NSString stringWithFormat:LOCALIZE(@"follow_notification"),userOpenID,notifObj.targetUserID];
            }
        }else{
            titleString = [NSString stringWithFormat:LOCALIZE(@"self_follow_notification"),userOpenID,GET_DEFAULT(USER_OPEN_ID)];
        }
    }

    //use singleton to avoid lag
    STTweetLabel* titleLabel = [SINGLETON stTitleLabel];
    [titleLabel setText:titleString];
    CGSize stringSize = VIEW_SIZE([titleLabel suggestedFrameSizeToFitEntireStringConstrainedToWidth:(SCREEN_WIDTH-20-2*UserTitleImageSize)]);

    float height = (15+subTitleDefaultHeight+stringSize.height)<55? 55:18+subTitleDefaultHeight+stringSize.height;


    return height ;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self awakeFromNib];
        
    }
    return self;
}

- (void)awakeFromNib {
    
    _userImageView = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, UserTitleImageSize, UserTitleImageSize)];
    _userImageView.userInteractionEnabled = YES;
    [_userImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickUser:)]];

    _maskImage = [[UIImageView alloc] init];
    [self addSubview:_maskImage];
    [_maskImage setImage:[UIImage imageNamed:@"head_mask"]];
    _maskImage.frame = CGRectMake(5, 5, UserTitleImageSize, UserTitleImageSize);
    
    _titleLabel = [[STTweetLabel alloc]initWithFrame:CGRectMake(UserTitleImageSize+10, 5, SCREEN_WIDTH-2*UserTitleImageSize-20, titleDefaultHeight)];
    _titleLabel.numberOfLines = 0;
    [_titleLabel setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                 NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(titleTextSize)}
                       hotWord:STTweetHandle];
    [_titleLabel setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                 NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(titleTextSize)}
                       hotWord:STTweetHashtag];
    [_titleLabel setAttributes:@{NSForegroundColorAttributeName: DARK_GRAY_COLOR,
                                 NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(titleTextSize)}];
    
    _subTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.origin.y+_titleLabel.frame.size.height, _titleLabel.frame.size.width, subTitleDefaultHeight)];
    _subTitleLabel.font = SYSTEM_FONT_WITH_SIZE(12);
    [_subTitleLabel setTextColor:GRAY_COLOR];
    
    _postImageView = [UIImageView new];
    [_postImageView setFrame:CGRectMake(SCREEN_WIDTH-UserTitleImageSize-5, 5, UserTitleImageSize, UserTitleImageSize)];
    _postImageView.userInteractionEnabled = YES;
    [_postImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickPost:)]];
    [_postImageView setBackgroundColor:[UIColor clearColor]];
    
    _followBtn = [UIButton new];
    [_followBtn setFrame:CGRectMake(SCREEN_WIDTH-50, 10, 45 , 35 )];

    [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow") forState:UIControlStateNormal];
    [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow_down") forState:UIControlStateSelected];
    [_followBtn addTarget:self action:@selector(followAction:) forControlEvents:UIControlEventTouchUpInside];
    [_followBtn setBackgroundColor:[UIColor clearColor]];

    _bottomLine = [ThemeManager separaterLine];
    _bottomLine.frame = CGRectMake(10, 54.5, SCREEN_WIDTH-20, 0.3);

    
    [self.contentView addSubview:_userImageView];
    [self.contentView addSubview:_titleLabel];
    [self.contentView addSubview:_subTitleLabel];
    [self.contentView addSubview:_followBtn];
    [self.contentView addSubview:_postImageView];
    [self.contentView addSubview:_bottomLine];
    
    _videoImageView = [[UIImageView alloc] init];
    [_videoImageView setImage:IMAGE_FROM_BUNDLE(@"video_1")];
    [self addSubview:_videoImageView];
    _videoImageView.hidden = YES;
    
    CGRect postFrame = _postImageView.frame;
    postFrame.origin.x += _postImageView.frame.size.width-15;
    postFrame.size.width = 15;
    postFrame.size.height = 15;
    _videoImageView.frame = postFrame;

}


-(void)reloadCell:(NotificationObject*)notifObj
{
    
    _notifObj = notifObj;
    _subTitleLabel.text = [SINGLETON getElaspsedTimeString:notifObj.timestamp];
    
    if(notifObj.user!=nil){
        [_userImageView setImageWithURL:S3_THUMB_IMAGE_URL(notifObj.user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
//        [_userImageView my_setImageWithURL:(notifObj.user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s") isThumbnail:YES];
    }else{
        [_userImageView setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }
    
    NSString* titleString = [self getTitleString:_notifObj];
    
    if(![_notifObj.post.picture isEqualToString:@""] && _notifObj.post.picture!=NULL){
        
        if(![_notifObj.post.picture isEqualToString:@""]){
            [_postImageView setImageWithURL:S3_THUMB_IMAGE_URL(_notifObj.post.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
//            [_postImageView my_setImageWithURL:(_notifObj.post.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s") isThumbnail:YES];
        }else{
            [_postImageView setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
        }
        
        _postImageView.hidden = NO;
    
        if([_notifObj.post.type isEqualToString:@"video"]){
            _videoImageView.hidden = NO;
        }else if([_notifObj.post.type isEqualToString:@"image"]){
            _videoImageView.hidden = YES;
        }

    }else if(_notifObj.targetUserPicture!=NULL && ![_notifObj.targetUserPicture isKindOfClass:[NSNull class]]){
        
        if(![_notifObj.targetUserPicture isEqualToString:@""]){
            [_postImageView setImageWithURL:S3_THUMB_IMAGE_URL(_notifObj.targetUserPicture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
//            [_postImageView my_setImageWithURL:(_notifObj.targetUserPicture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s") isThumbnail:YES];
        }else{
            [_postImageView setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
        }
        
        _postImageView.hidden = NO;
        _videoImageView.hidden = YES;

    }else{
        
        _videoImageView.hidden = YES;
        _postImageView.hidden = YES;
    }
    
    if([_notifObj.type isEqualToString:NEW_FRIEND_JOIN_CONTACT] || [_notifObj.type isEqualToString:NEW_FRIEND_JOIN_FB] ||  [_notifObj.type isEqualToString:NEW_FOLLOW_TYPE]){
        _followBtn.hidden = NO;
        
        if(_notifObj.user.isFollowing==1){
            [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow_down") forState:UIControlStateNormal];
            [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow_down") forState:UIControlStateSelected];
        }else{
            if(_notifObj.user.followRequestTime==0){
                [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow") forState:UIControlStateNormal];
                [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow") forState:UIControlStateSelected];
            }else{
                [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow_wait") forState:UIControlStateNormal];
                [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow_wait") forState:UIControlStateSelected];
            }
        }
        
    }else{
        _followBtn.hidden = YES;
    }

    __weak UINavigationController* nav = navCtrl;
    __weak NotifCell* cell = self;

    [_titleLabel setDetectionBlock:^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range) {
        
        NSString* identifier = [string substringToIndex:1];
        NSString* userOpenID = [string substringFromIndex:1];

        if([userOpenID isEqualToString:GET_DEFAULT(USER_OPEN_ID)]){
            [cell shake:5 withDelta:4 speed:0.05 shakeDirection:ShakeDirectionHorizontal];
            return;
        }
        
        if([identifier isEqualToString:@"@"]){

            UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
            [userCtrl getUserWithOpenID:userOpenID];
            userCtrl.hidesBottomBarWhenPushed = YES;
            [nav pushViewController:userCtrl animated:YES];
            
        }else if([[string substringToIndex:1] isEqualToString:@"#"]){
            
        }
    }];
    
    [_titleLabel setText:titleString];

    CGSize stringSize = VIEW_SIZE([_titleLabel suggestedFrameSizeToFitEntireStringConstrainedToWidth:(SCREEN_WIDTH-20-2*UserTitleImageSize)]);
    _titleLabel.frame = CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.origin.y, stringSize.width, stringSize.height);
    _subTitleLabel.frame = CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.origin.y+_titleLabel.frame.size.height+5, _titleLabel.frame.size.width, subTitleDefaultHeight);
    
//    [_titleLabel setBackgroundColor:LIGHT_BG_COLOR];
    
    if(_subTitleLabel.frame.origin.y+_subTitleLabel.frame.size.height<=UserTitleImageSize+10){
        _bottomLine.frame = CGRectMake(10, 54.5, SCREEN_WIDTH-20, 0.5);
    }else{
        _bottomLine.frame = CGRectMake(10, _subTitleLabel.frame.origin.y+_subTitleLabel.frame.size.height+5, SCREEN_WIDTH-20, 0.5);
        [self.contentView addSubview:_bottomLine];
    }
    
}

-(void)isReadSettings // followersNotif do not run time
{
    if(_notifObj.isRead==1){
        [self setBackgroundColor:[UIColor clearColor]];
    }else{
        [self setBackgroundColor:LIGHT_MAIN_COLOR];
    }
}

-(NSString*)getTitleString:(NotificationObject*)notifObj
{
    _notifObj = notifObj;
    
    NSString* titleString = @"";
    NSString* userOpenID = notifObj.user.openID;
    
    // go to single post
    if([notifObj.type isEqualToString:POST_LIKE]){
        
        if([notifObj.post.user.userID isEqualToString:MY_USER_ID]){
            titleString = [NSString stringWithFormat:LOCALIZE(@"post_like_your_notification"),userOpenID];
        }else{
            if(notifObj.post.user!=nil){
                titleString = [NSString stringWithFormat:LOCALIZE(@"post_like_notification"),userOpenID,notifObj.post.user.openID];
            }else{
                titleString = [NSString stringWithFormat:LOCALIZE(@"post_like_notification"),@"",@""];
            }
        }
    
    }else if([notifObj.type isEqualToString:POST_COMMENT]){
    
        titleString = [NSString stringWithFormat:LOCALIZE(@"post_comment_notification"),userOpenID];
    
    }else if([notifObj.type isEqualToString:POST_TAG]){
        
        titleString = [NSString stringWithFormat:LOCALIZE(@"post_tag_notification"),userOpenID];
    
    }else if([notifObj.type isEqualToString:COMMENT_TAG]){
        
        titleString = [NSString stringWithFormat:LOCALIZE(@"comment_tag_notification"),userOpenID];
     
    // go to user profile
    }else if([notifObj.type isEqualToString:NEW_FRIEND_JOIN_CONTACT]){
        
        titleString = [NSString stringWithFormat:LOCALIZE(@"new_contact_friend_joined"),userOpenID];
    
    }else if([notifObj.type isEqualToString:NEW_FRIEND_JOIN_FB]){
        
        titleString = [NSString stringWithFormat:LOCALIZE(@"new_fb_friend_joined"),userOpenID];
        
    }else if([notifObj.type isEqualToString:NEW_FOLLOW_TYPE]){
        
        if(_notifObj.targetUserID!=nil && ![_notifObj.targetUserID isKindOfClass:[NSNull class]]){
            if([_notifObj.targetUserID isEqualToString:GET_DEFAULT(USER_OPEN_ID)]){
                titleString = [NSString stringWithFormat:LOCALIZE(@"self_follow_notification"),userOpenID];
            }else{
                titleString = [NSString stringWithFormat:LOCALIZE(@"follow_notification"),userOpenID,_notifObj.targetUserID];
            }
        }else{
            titleString = [NSString stringWithFormat:LOCALIZE(@"self_follow_notification"),userOpenID,GET_DEFAULT(USER_OPEN_ID)];
        }
    }

    return titleString;
}


-(void)followAction:(id)sender
{
    [_followBtn playBounceAnimation];
    
    if(_notifObj.user.isFollowing==1){
        _notifObj.user.isFollowing=0;
        [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow") forState:UIControlStateNormal];
        [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow") forState:UIControlStateSelected];
        [API_MANAGER unfollowUserAction:_notifObj.user.userID withCompletion:^(BOOL success) {}];

    }else{
        
        if([_notifObj.user isPrivacyMode]){
            if(_notifObj.user.followRequestTime!=0){
                _notifObj.user.followRequestTime=0;
                [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow") forState:UIControlStateNormal];
                [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow") forState:UIControlStateSelected];
                [API_MANAGER cancelFollowRequests:_notifObj.user.userID withCompletion:^(BOOL success) {}];
            }else{
                
                if([GET_DEFAULT(FOLLOWING_COUNT) intValue] >= MAX_FOLLOWER_COUNT){
                    [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"cannot_follow_user") message:[NSString stringWithFormat:LOCALIZE(@"max_followcount"),MAX_FOLLOWER_COUNT] buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                        if(okClicked){
                            
                        }
                    }];
                }else{
                    _notifObj.user.followRequestTime=CURRENT_TIMESTAMP;
                    [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow_wait") forState:UIControlStateNormal];
                    [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow_wait") forState:UIControlStateSelected];
                    [API_MANAGER sendFollowRequest:_notifObj.user.userID withCompletion:^(BOOL success) {
                        if (success) {
                            [NOTIFICATION_CENTER postNotificationName:FOLLOW_IN_NOTIFYCELL object:nil];
                        }
                    }];
                }
                
            }
        }else{
            
            if([GET_DEFAULT(FOLLOWING_COUNT) intValue] >= MAX_FOLLOWER_COUNT){
                [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"cannot_follow_user") message:[NSString stringWithFormat:LOCALIZE(@"max_followcount"),MAX_FOLLOWER_COUNT] buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                    if(okClicked){
                        
                    }
                }];
            }else{
                _notifObj.user.isFollowing=1;
                [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow_down") forState:UIControlStateNormal];
                [_followBtn setImage:IMAGE_FROM_BUNDLE(@"follow_down") forState:UIControlStateSelected];
                _notifObj.user.followRequestTime=0;
                [API_MANAGER followUserAction:_notifObj.user.userID withCompletion:^(BOOL success) {
                }];
            }
        }
    }

    [SINGLETON reloadFollowStatus];
}

-(void)didClickPost:(id)sender
{
    if([_notifObj.type isEqualToString:NEW_FRIEND_JOIN_CONTACT] || [_notifObj.type isEqualToString:NEW_FRIEND_JOIN_FB]
       || [_notifObj.type isEqualToString:NEW_FOLLOW_TYPE]){
        
        if([_notifObj.targetUserID isEqualToString:GET_DEFAULT(USER_OPEN_ID)]){
            [self.postImageView shake:5 withDelta:4 speed:0.05 shakeDirection:ShakeDirectionHorizontal];
            return;
        }
        
        UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
        [userCtrl getUserWithOpenID:_notifObj.targetUserID];
        userCtrl.hidesBottomBarWhenPushed = YES;
        [navCtrl pushViewController:userCtrl animated:YES];
        
    }else{
        SinglePostViewController* sVC = [SinglePostViewController new];
        sVC.enterMode=EnterFromFollowerLike;
        sVC.hidesBottomBarWhenPushed = YES;
        sVC.post = _notifObj.post;
        [navCtrl pushViewController:sVC animated:YES];
    }
}

-(void)didClickUser:(id)sender
{
    if([_notifObj.user.userID isEqualToString:MY_USER_ID]){
        [self shake:5 withDelta:4 speed:0.05 shakeDirection:ShakeDirectionHorizontal];
        return;
    }
    
    UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
    [userCtrl getUserWithOpenID:_notifObj.user.openID];
    userCtrl.hidesBottomBarWhenPushed = YES;
    [navCtrl pushViewController:userCtrl animated:YES];
}

@end
