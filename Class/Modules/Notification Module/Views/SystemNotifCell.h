//
//  SystemNotifCell.h
//  Story17
//
//  Created by POPO on 5/8/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationObject.h"

@interface SystemNotifCell : UITableViewCell

@property (nonatomic,strong) UILabel* messageLabel;
@property (nonatomic,strong) UILabel* timestampLabel;
@property(nonatomic, weak) UINavigationController* navCtrl;
@property (nonatomic,strong) UIView* bottomLine;

-(void)reloadCell:(NotificationObject*)notifObj;
+ (float)getCellHeigthWithNotif:(NotificationObject*)notifObj;

@end
