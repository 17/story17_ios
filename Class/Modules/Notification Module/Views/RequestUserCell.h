//
//  RequestUserCell.h
//  story17
//
//  Created by POPO Chen on 8/26/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserObject.h"

@class RequestUserCell;

@protocol RequestUserCellDelegate <NSObject>
-(void) didClickAcceptRequestButton:(RequestUserCell*)cell userID:(NSString*)userID;
-(void) didClickRejectRequestButton:(RequestUserCell*)cell userID:(NSString*)userID;
@end


@interface RequestUserCell : UITableViewCell

@property (nonatomic,weak) UserObject* user;
@property (nonatomic,strong) UIImageView* userImageView;
@property (nonatomic,strong) UILabel* titleLabel;

@property (nonatomic,strong) UIButton* acceptButton;
@property (nonatomic,strong) UIButton* rejectButton;

@property(nonatomic, weak) id<RequestUserCellDelegate> delegate;

-(void)reloadCell:(UserObject*)user;
+ (float)getCellHeigt;

@end
