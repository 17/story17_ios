//
//  RequestCell.m
//  story17
//
//  Created by POPO Chen on 8/26/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "RequestCell.h"
#import "Constant.h"
#import "UIImageView+AFNetworking.h"

@implementation RequestCell

#define cellHeight 55
#define imageWidth 38


+ (float)getCellHeigt
{
    return cellHeight;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20+2.5*imageWidth, 0, 0, 0)];
        [_titleLabel setTextColor:DARK_GRAY_COLOR];
        _titleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        
        _subtitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(_titleLabel.frame.origin.x, 0, 0, 0)];
        [_subtitleLabel setTextColor:GRAY_COLOR];
        _subtitleLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        
        _coverPhoto1 = [self getCircleImageView];
        _coverPhoto2 = [self getCircleImageView];
        _coverPhoto3 = [self getCircleImageView];
        
        [_coverPhoto1 setBackgroundColor:WHITE_COLOR];
        _coverPhoto1.frame = CGRectMake(10, (cellHeight-imageWidth)/2, imageWidth, imageWidth);
        _coverPhoto2.frame = CGRectMake(10+0.75*imageWidth, (cellHeight-imageWidth)/2, imageWidth, imageWidth);
        _coverPhoto3.frame = CGRectMake(10+1.5*imageWidth,  (cellHeight-imageWidth)/2, imageWidth, imageWidth);

        _badgeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _badgeButton.frame = CGRectMake(_coverPhoto1.frame.origin.x+_coverPhoto1.frame.size.width-8, _coverPhoto1.frame.origin.y, 15, 15);
        _badgeButton.titleLabel.font = SYSTEM_FONT_WITH_SIZE(8);

//        [_badgeButton setBackgroundImage:[[UIImage imageNamed:@"big_badge"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
//        [_badgeButton setBackgroundImage:[[UIImage imageNamed:@"big_badge"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5) resizingMode:UIImageResizingModeStretch] forState:UIControlStateHighlighted];
        
        [_badgeButton setBackgroundImage:[UIImage imageNamed:@"big_badge"] forState:UIControlStateNormal];
        [_badgeButton setBackgroundImage:[UIImage imageNamed:@"big_badge"] forState:UIControlStateHighlighted];
        [_badgeButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];

        [_badgeButton.layer setCornerRadius:_badgeButton.frame.size.width/2];
        _badgeButton.layer.masksToBounds = YES;
        
        _arrowImageView = [[UIImageView alloc]initWithImage:IMAGE_FROM_BUNDLE(@"nav_arrow_gray_next")];
        _arrowImageView.frame = CGRectMake(0, 0, 18, 12);
        _arrowImageView.center = CGPointMake(SCREEN_WIDTH-15, cellHeight/2);
        
        UIView* linveView = [ThemeManager separaterLine];
        linveView.frame = CGRectMake(0, cellHeight-0.5, SCREEN_WIDTH, 0.5);
        
        [self.contentView addSubview:_titleLabel];
        [self.contentView addSubview:_subtitleLabel];
        
//        [self.contentView addSubview:_coverPhoto3];
//        [self.contentView addSubview:_coverPhoto2];
        
        [self.contentView addSubview:_coverPhoto1];
        [self.contentView addSubview:_arrowImageView];
//        [self.contentView addSubview:_badgeButton];
        
        [self.contentView addSubview:linveView];
        
        self.backgroundColor = [UIColor colorWithRed:239.f/255.f green:239.f/255.f blue:239.f/255.f alpha:1];
    }
    return self;
}

-(void)reloadCell:(NSArray*)requestUsers
{
    [_titleLabel setText:LOCALIZE(@"Follow_request")];
    [_titleLabel sizeToFit];
    _titleLabel.center = CGPointMake(20+imageWidth+_titleLabel.frame.size.width/2, 10+_titleLabel.frame.size.height/2);
    
    if([GET_DEFAULT(REQUEST_BADGE)  isEqual:@0]){
        _badgeButton.hidden = YES;
    }else{
        _badgeButton.hidden = NO;
        if([GET_DEFAULT(REQUEST_BADGE) intValue] >=100){
            [_badgeButton setTitle:@"99" forState:UIControlStateNormal];
        }else{
            [_badgeButton setTitle:[GET_DEFAULT(REQUEST_BADGE) stringValue] forState:UIControlStateNormal];
        }
    }
    
    [_subtitleLabel setText:LOCALIZE(@"Follow_accept_request")];
    [_subtitleLabel sizeToFit];
    _subtitleLabel.center = CGPointMake(20+imageWidth+_subtitleLabel.frame.size.width/2, _titleLabel.frame.origin.y+_titleLabel.frame.size.height+_subtitleLabel.frame.size.height/2);
    
    [_coverPhoto1 setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    
//    [_coverPhoto2 setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
//    [_coverPhoto3 setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    
    NSArray* coverphoto = @[_coverPhoto1,_coverPhoto2,_coverPhoto3];
    
    for(int i=0;i<[requestUsers count];i++){
        
        if(i>2){
            return;
        }
        
        UserObject* user = requestUsers[i];
        UIImageView* coverImage = coverphoto[i];
        [coverImage setImageWithURL:S3_THUMB_IMAGE_URL(user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }

}

-(UIImageView*) getCircleImageView
{
    UIImageView* imageView =  [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imageWidth, imageWidth)];
    imageView.layer.cornerRadius = imageWidth/2;
    imageView.layer.masksToBounds = YES;
    imageView.layer.borderWidth = 2;
    imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    return imageView;
}

@end
