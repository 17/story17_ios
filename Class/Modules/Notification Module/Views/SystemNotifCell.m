//
//  SystemNotifCell.m
//  Story17
//
//  Created by POPO on 5/8/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "SystemNotifCell.h"
#import "Constant.h"

@implementation SystemNotifCell

@synthesize navCtrl;

#define messageTextSize 14
#define messageDefaultHeight 30
#define timeStampDefaultHeight 20

+ (float)getCellHeigthWithNotif:(NotificationObject*)notifObj
{
    CGSize stringSize = [notifObj.message getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-20,300) withFont:SYSTEM_FONT_WITH_SIZE(messageTextSize)];

    return (15+timeStampDefaultHeight+stringSize.height)<55? 55:15+timeStampDefaultHeight+stringSize.height ;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self awakeFromNib];
        
    }
    return self;
}

- (void)awakeFromNib {
    
    _messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, SCREEN_WIDTH-20, messageDefaultHeight)];
    _timestampLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, _messageLabel.frame.origin.y+_messageLabel.frame.size.height, _messageLabel.frame.size.width, timeStampDefaultHeight)];

    _messageLabel.font = SYSTEM_FONT_WITH_SIZE(messageTextSize);
    _messageLabel.numberOfLines = 0;
    _timestampLabel.font = SYSTEM_FONT_WITH_SIZE(12);
    
    _bottomLine = [ThemeManager separaterLine];
    _bottomLine.frame = CGRectMake(10, 54.5, SCREEN_WIDTH-20, 0.3);
    
    [self.contentView addSubview:_messageLabel];
    [self.contentView addSubview:_timestampLabel];
    [self.contentView addSubview:_bottomLine];

}


-(void)reloadCell:(NotificationObject*)notifObj
{
    _messageLabel.text = notifObj.message;
    _timestampLabel.text = [SINGLETON getElaspsedTimeString:notifObj.timestamp];
    
    CGSize stringSize = [notifObj.message getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-20,300) withFont:SYSTEM_FONT_WITH_SIZE(messageTextSize)];

    _messageLabel.frame = CGRectMake(10, 5, stringSize.width, stringSize.height);
    _timestampLabel.frame = CGRectMake(10, _messageLabel.frame.origin.y+_messageLabel.frame.size.height+5, SCREEN_WIDTH, timeStampDefaultHeight);
    
    if((15+timeStampDefaultHeight+stringSize.height)<55){
        _bottomLine.frame = CGRectMake(10, 54.5, SCREEN_WIDTH-20, 0.5);
    }else{
        _bottomLine.frame = CGRectMake(10, _timestampLabel.frame.origin.y+_timestampLabel.frame.size.height+4.5, SCREEN_WIDTH-20, 0.5);
    }
    
}




@end
