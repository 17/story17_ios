//
//  NotifCell.h
//  Story17
//
//  Created by POPO on 5/8/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserTitleView.h"
#import "NotificationObject.h"
#import "STTweetLabel.h"

@interface NotifCell : UITableViewCell

@property (nonatomic,strong) UIImageView* userImageView;
@property (nonatomic,strong) UIImageView* maskImage;
@property (nonatomic,strong) UIImageView* videoImageView;

@property (nonatomic,strong) STTweetLabel* titleLabel;
@property (nonatomic,strong) UILabel* subTitleLabel;

@property (nonatomic,strong) UIImageView* postImageView;
@property (nonatomic,strong) UIButton* followBtn;
@property (nonatomic,weak) NotificationObject* notifObj;
@property (nonatomic,strong) UIView* bottomLine;

@property(nonatomic, weak)UINavigationController* navCtrl;

-(void)reloadCell:(NotificationObject*)notifObj;
+ (float)getCellHeigthWithNotif:(NotificationObject*)notifObj;
-(void)isReadSettings; // followersNotif do not run time

@end
