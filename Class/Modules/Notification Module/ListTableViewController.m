//
//  ListTableViewController.m
//  story17
//
//  Created by POPO Chen on 5/29/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "ListTableViewController.h"
#import "ReasonCell.h"
#import "Constant.h"

@implementation ListTableViewController

@synthesize selectionCallback;

#define listCellIdentity @"listCellIdentity"

-(id)init
{
    self = [super init];
    
    if(self){
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    
    _listTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
    [_listTableView registerClass:[ReasonCell class] forCellReuseIdentifier:listCellIdentity];
    _listTableView.delegate = self;
    _listTableView.dataSource = self;
    
    [self.view addSubview:_listTableView];
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    ReasonCell* cell = [_listTableView dequeueReusableCellWithIdentifier:listCellIdentity];
    if(!cell){
        cell = [[ReasonCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:listCellIdentity];
    }

    if([_dataArray isEqualToArray:ADMIN_DELETE_POST_REASON]){
        [cell reloadCell:LOCALIZE(_dataArray[indexPath.row])];
    }else{
        [cell reloadCell:_dataArray[indexPath.row]];
    }
    
    if([cell.mainTextLabel.text isEqualToString:_selectedItem]){        
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [ReasonCell getHeightWithText:_dataArray[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    selectionCallback(_dataArray[indexPath.row]);
    [self.navigationController popViewControllerAnimated:YES];
    
  }


@end
