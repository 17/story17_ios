//
//  NotifcationSettingsViewController.h
//  Story17
//
//  Created by POPO on 5/12/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TitleLabelView.h"
#import "ArrowSwitchLabelView.h"

@interface NotificationSettingsViewController : UIViewController

@property (nonatomic,strong) UIScrollView* mainScrollView;

@property (nonatomic,strong) TitleLabelView* likeTitleView;
@property (nonatomic,strong) ArrowSwitchLabelView* likeOffView;
@property (nonatomic,strong) ArrowSwitchLabelView* likeFriendView;
@property (nonatomic,strong) ArrowSwitchLabelView* likeAllView;

@property (nonatomic,strong) TitleLabelView* commentTitleView;
@property (nonatomic,strong) ArrowSwitchLabelView* commentOffView;
@property (nonatomic,strong) ArrowSwitchLabelView* commentFriendView;
@property (nonatomic,strong) ArrowSwitchLabelView* commentAllView;

@property (nonatomic,strong) TitleLabelView* followTitleView;
@property (nonatomic,strong) ArrowSwitchLabelView* followOffView;
@property (nonatomic,strong) ArrowSwitchLabelView* followAllView;

@property (nonatomic,strong) TitleLabelView* tagTitleView;
@property (nonatomic,strong) ArrowSwitchLabelView* tagOffView;
@property (nonatomic,strong) ArrowSwitchLabelView* tagFriendView;
@property (nonatomic,strong) ArrowSwitchLabelView* tagAllView;

@property (nonatomic,strong) TitleLabelView* friendJoinTitleView;
@property (nonatomic,strong) ArrowSwitchLabelView* friendJoinOffView;
@property (nonatomic,strong) ArrowSwitchLabelView* friendJoinAllView;

@property (nonatomic,strong) TitleLabelView* followRequestTitleView;
@property (nonatomic,strong) ArrowSwitchLabelView* followRequestOffView;
@property (nonatomic,strong) ArrowSwitchLabelView* followRequestAllView;

@property (nonatomic,strong) TitleLabelView* friendFirstPostTitleView;
@property (nonatomic,strong) ArrowSwitchLabelView* friendFirstPostOffView;
@property (nonatomic,strong) ArrowSwitchLabelView* friendFirstPostFriendView;
@property (nonatomic,strong) ArrowSwitchLabelView* friendFirstPostAllView;

@property (nonatomic,strong) TitleLabelView* sysNotifTitleView;
@property (nonatomic,strong) ArrowSwitchLabelView* sysNotifOffView;
@property (nonatomic,strong) ArrowSwitchLabelView* sysNotifAllView;

@property (nonatomic,strong) TitleLabelView* livestreamTitleView;
@property (nonatomic,strong) ArrowSwitchLabelView* livestreamOffView;
@property (nonatomic,strong) ArrowSwitchLabelView* livestreamAllView;

@property (nonatomic,strong) TitleLabelView* restreamTitleView;
@property (nonatomic,strong) ArrowSwitchLabelView* retreamOffView;
@property (nonatomic,strong) ArrowSwitchLabelView* retreamAllView;

@property (nonatomic,strong) TitleLabelView* scheduleTitleView;
@property (nonatomic,strong) ArrowSwitchLabelView* scheduleOffView;
@property (nonatomic,strong) ArrowSwitchLabelView* scheduleAllView;

@end
