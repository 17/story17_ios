//
//  ExploreUserViewController.m
//  story17
//
//  Created by POPO Chen on 7/23/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "ExploreUserViewController.h"
#import "SVPullToRefresh.h"
#import "Constant.h"
#import "FriendSuggestionCell.h"
#import "UserTitleView.h"

@interface ExploreUserViewController ()

@property (assign, nonatomic) BOOL isFetching;
@property (assign, nonatomic) BOOL noMoreData;
@property (assign, nonatomic) BOOL isFirstFetching;
@property (assign, nonatomic) int enterPageTimeStamp;
@property (assign, nonatomic) int refreshCount;
@property (assign, nonatomic) int visibleCount;

@end

@implementation ExploreUserViewController

#define fetchNum 20

#pragma mark - Initialize

- (id)init
{
    self = [super init];
    if (self) {
        _suggestionArray = CREATE_MUTABLE_ARRAY;
    }
    return self;
}

#pragma mark - Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = LOCALIZE(@"User_Explorer");
    
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];

    [self.view setBackgroundColor:LIGHT_BG_COLOR];
    
    _isFirstFetching = YES;
    _isFetching = NO;
    _noMoreData = NO;
    
    [self setup];
    
    if (_suggestionArray.count <= 0) {
        [self fetchData:YES];
    }
    
    [_mainCollectionView reloadData];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [EVENT_HANDLER addEventTracking:@"EnterDiscoverUser" withDict:nil];
    _enterPageTimeStamp = CURRENT_TIMESTAMP;
    _visibleCount = 0;
    _refreshCount = 0;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [EVENT_HANDLER addEventTracking:@"LeaveDiscoverUser" withDict:@{@"cellCount": INT_TO_STRING(_visibleCount),
                                                                    @"duration": INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp),
                                                                    @"refreshCount": INT_TO_STRING(_refreshCount)}];
}

- (void)setup
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    _mainCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT) collectionViewLayout:flowLayout];
    
    [self collectionViewSettings:_mainCollectionView];
    [_mainCollectionView registerClass:[FriendSuggestionCell class] forCellWithReuseIdentifier:@"friendSuggestionCell"];
    [_mainCollectionView setBackgroundColor:LIGHT_BG_COLOR];
    
    __weak ExploreUserViewController *weakSelf = self;
    
    [_mainCollectionView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData:YES];
    }];
    
    [self.view addSubview:_mainCollectionView];
    
    _progressView = [UIActivityIndicatorView new];
    _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView setColor:MAIN_COLOR];
    
    [self.view addSubview:_progressView];
    
}

- (void)fetchData:(BOOL)refresh
{
    if (refresh) {
        _noMoreData = NO;
        _refreshCount++;
    }
    
    if (_noMoreData || _isFetching) {
        return;
    }
    
    if (_isFirstFetching) {
        [_progressView startAnimating];
        _isFirstFetching = NO;
    }
    
    int beforeTime = INT32_MAX;
    if ([_suggestionArray count] != 0 && !refresh) {
        SuggestionObject *lastUser = _suggestionArray[[_suggestionArray count]-1];
        beforeTime = lastUser.user.lastLogin;
    }
    
    DLog(@"%d",beforeTime);
    _isFetching = YES;
    [API_MANAGER getExploreSuggestedUsers:beforeTime count:fetchNum completion:^(BOOL success, NSArray *suggestedUser) {
        [_progressView stopAnimating];
        [_mainCollectionView.pullToRefreshView stopAnimating];
        _isFetching = NO;
        if (success) {
            
            if (refresh) {
                [_suggestionArray removeAllObjects];
            }
            
            [_suggestionArray addObjectsFromArray:suggestedUser];
            [_mainCollectionView reloadData];

            if ([suggestedUser count] < fetchNum) {
                _noMoreData = YES;
            }
        }
        
    }];
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_suggestionArray count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    FriendSuggestionCell* customCell = (FriendSuggestionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"friendSuggestionCell" forIndexPath:indexPath];
    
    if (!customCell) {
        CGRect rect = CGRectMake(0, 0, self.view.bounds.size.width, 170);
        customCell = [[FriendSuggestionCell alloc] initWithFrame:rect];
    }

    customCell.navCtrl = self.navigationController;
    customCell.userTitleView.userInteractionEnabled = YES;
    
    [(FriendSuggestionCell*)customCell reloadCell:(SuggestionObject*)_suggestionArray[indexPath.row]];
    _visibleCount++;
    return customCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *users = CREATE_MUTABLE_ARRAY;
    users = _suggestionArray;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 10, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

# pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(SCREEN_WIDTH,180);
}

#pragma mark - UI Setting
- (void)collectionViewSettings:(UICollectionView*)collectionview
{
    collectionview.delegate = self;
    collectionview.dataSource = self;
    collectionview.scrollsToTop = YES;
    collectionview.alwaysBounceVertical = YES;
    [collectionview setBackgroundColor:LIGHT_BG_COLOR];
}

- (void)cancelAction:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end
