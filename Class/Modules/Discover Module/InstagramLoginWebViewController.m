//
//  InstagramLoginWebViewController.m
//  Story17
//
//  Created by James17 on 1/15/16.
//  Copyright © 2016 POPO_INNOVATION. All rights reserved.
//

#import "InstagramLoginWebViewController.h"
#import "Constant.h"
#import "InstagramEngine.h"
#import "InstagramUser.h"

@interface InstagramLoginWebViewController ()

@property (nonatomic,strong) NSDictionary *dict;
@property (nonatomic,strong) NSString *token;
@property (nonatomic,strong)  NSMutableData *datas;
@property (nonatomic,strong) NSMutableArray *InstagramFollowerArray;
@property (nonatomic,strong) NSString *InstagramName;
@end

@implementation InstagramLoginWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
    UIWebView *loginWebview = [[UIWebView alloc]initWithFrame:CGRectMake(0,STATUS_BAR_HEIGHT , SCREEN_WIDTH, SCREEN_HEIGHT - STATUS_BAR_HEIGHT)];
    [self.view addSubview:loginWebview];
    NSURL *authURL = [[InstagramEngine sharedEngine] authorizationURL];
    loginWebview.delegate = self;

    [loginWebview loadRequest:[NSURLRequest requestWithURL:authURL]];
    
    _datas = [[NSMutableData alloc]init];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{

    NSError *error;
    
    if ([[InstagramEngine sharedEngine] receivedValidAccessTokenFromURL:request.URL error:&error])
    {
        _token = [InstagramEngine sharedEngine] .accessToken;
        NSLog(@"Instagram 的 Access Token: %@ ",_token);
        
        [self updateUserInfoAPI];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    return YES;
}

#pragma mark - Update Instagram User Info

-(void)updateUserInfoAPI
{
    
    [API_MANAGER updateUserInfo:@{@"instagramID":[InstagramEngine sharedEngine].appClientID,@"instagramAccessToken":_token} fetchSelfInfo:NO completion:^(BOOL success) {
        if (success)
        {
            NSLog(@"成功上傳 Instagram UserInfo");
            [self getInstagramUserName];
        }
    }];
}

#pragma mark - Instagram

-(void)getInstagramUserName
{
    [[InstagramEngine sharedEngine] getSelfUserDetailsWithSuccess:^(InstagramUser * _Nonnull user) {
        _InstagramName = user.username;
        [self getInstagramFollowerList];
    } failure:^(NSError * _Nonnull error, NSInteger serverStatusCode) {
        NSLog(@"取得 Instagram user name 失敗，原因: %@ ",error);
    }];
}


-(void)getInstagramFollowerList
{
    NSString *string = @"https://api.instagram.com/v1/users/self/follows?access_token=";
    NSString *stringWithToken = [string stringByAppendingString:_token];
    
    NSURL *url = [NSURL URLWithString:stringWithToken];
    
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:url];
    [NSURLConnection connectionWithRequest:requestURL delegate:self];
}



#pragma mark - NSURLConnectionDataDelegate

//當取得網站回應的時候，把NSMutableData裡面的資料清空，從頭開始以保證資料完整性。
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [_datas setLength:0];
}

//取得資料，加入NSMutableData之中。
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"取得 Instagram 資料: %@",data);
    [_datas appendData:data];
}

//如果遇到失敗或錯誤，把資料先清空。
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"發生錯誤！");
    [_datas setLength:0];
}

//下載完成，拿這個資料來使用。
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    _dict = [NSJSONSerialization JSONObjectWithData:_datas options:NSJSONReadingAllowFragments error:nil];
    
    _InstagramFollowerArray = [[NSMutableArray alloc]init];
    for (NSDictionary *dic in [_dict objectForKey:@"data"])
    {
        NSString *username = dic[@"username"];
        NSString *userID = dic[@"id"];

        NSDictionary *tempDic = @{@"name":username , @"id":userID};
        [_InstagramFollowerArray addObject:tempDic];
    }
    
    [self uploadInstagramFollowerAPI];
}


#pragma mark - Upload Instagram Followers

-(void)uploadInstagramFollowerAPI
{
    [API_MANAGER uploadInstagramFriends:_InstagramName instagramIDs:TO_JSON(_InstagramFollowerArray) withCompletion:^(BOOL success, NSString *message) {
        if (success)
        {
            NSLog(@"成功上傳 Instagram Friends ");
        }

    }];

}
@end
