//
//  SearchViewController.h
//  story17
//
//  Created by POPO Chen on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchUserCell.h"
#import "HotLiveV2ViewController.h"
#import "BannerObject.h"
#import <SafariServices/SafariServices.h>
#import "BannerCell.h"

@interface SearchViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,UITextViewDelegate,UIScrollViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,SearchUserDelegate,SFSafariViewControllerDelegate,BannerDelegate>


#pragma mark - Global variable
/* SearchBar */
@property(nonatomic,strong) UIView* statusView;

@property(nonatomic,strong) UISearchBar* searchBar;

@property(nonatomic,strong) UIView* topView;

#pragma mark - TableView
/* mode indicator */
@property(nonatomic,strong) UIView* tableModeSelectorContainer;
@property(nonatomic,strong) NSArray* searchButtonArray;
@property(nonatomic,strong) UIImageView* searchIndicatorImageView;

/* mode btn array */
@property(nonatomic,strong) NSArray* searchBtnArray;
@property(nonatomic,strong) UIButton* searchUserBtn;
@property(nonatomic,strong) UIButton* searchTagBtn;

/* main scrollview */
@property(nonatomic,strong) UIScrollView* searchScrollView;

@property(nonatomic,strong) NSArray* tableViewArray;
@property(nonatomic,strong) UITableView* userTableView;
@property(nonatomic,strong) UITableView* tagTableView;
@property(nonatomic,strong) UIImageView* noUserLabel;
@property(nonatomic,strong) UIImageView* noTagLabel;

/* data array */
@property (nonatomic,strong) NSArray* searchArray;
@property (nonatomic,strong) NSMutableArray* searchUserArray;
@property (nonatomic,strong) NSMutableArray* searchTagArray;

#pragma mark - CollectionView
@property (nonatomic,strong) UIScrollView* mainScrollView;
@property (nonatomic,strong) UICollectionView* photoCollectionView;
@property (nonatomic,strong) NSMutableArray* photoArray;
@property (nonatomic,strong) UIActivityIndicatorView* progressView;
@property (nonatomic,strong) UIImageView* noDataImageView;


@property  int currentMode;

@property BOOL isFirstFetching;
@property BOOL isFetching;
@property BOOL noMoreData;

@end
