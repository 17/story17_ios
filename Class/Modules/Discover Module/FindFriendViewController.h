//
//  FindFriendViewController.h
//  Story17
//
//  Created by POPO on 5/13/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchUserCell.h"

typedef enum
{
    FindFriendTabSearch = 0,
    FindFriendTabContact = 1,
    FindFriendTabFacebook
} FindFriendTabMode;

@interface FindFriendViewController : UIViewController




@end
