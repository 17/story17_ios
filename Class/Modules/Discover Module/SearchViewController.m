//
//  SearchViewController.m
//  story17
//
//  Created by POPO Chen on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "SearchViewController.h"
#import "FindFriendViewController.h"
#import "Constant.h"
#import "SVPullToRefresh.h"
#import "PostGridCell.h"
#import "SearchUserCell.h"
#import "TagViewCell.h"
#import "SinglePostViewController.h"
#import "UserProfileViewController.h"
#import "BrowsePostsViewController.h"
#import "UserSuggestionViewController.h"

#import "ExplorerCell.h"
#import "ExploreUserViewController.h"
#import "BannerCell.h"
#import "UIImageView+AFNetworking.h"
#import "LiveStreamObject.h"

static NSString * const kReuseCellIdentifier = @"PostGridCell";
static float const kBannerHeight = 100.0f;

@interface SearchViewController()

@property BOOL haveExplorerUser;
@property BOOL haveExplorerLive;
@property (nonatomic,strong) NSMutableArray* bannerArray;
@property (nonatomic,strong) NSMutableArray* explorerUser;
@property (nonatomic,strong) NSMutableArray* explorerLive;
@property (nonatomic,strong) NSMutableArray* explorerLiveShow;
@property int flurryUserCount;
@property int flurryHashCount;
@property int visibleCellCount;
@property int enterPageTimeStamp;
@property int enterSearchUserPageTimeStamp;

@property int refreshCount;
@property float touchX;
@property float touchY;

@end

@implementation SearchViewController

#define horizontalMargin 5
#define searchUserIdentity @"searchUserIdentity"
#define searchTagIdentity @"searchTagIdentity"
#define searchUserCellHeight 55;
#define searchTagCellHeight 55;

-(id)init
{
    self = [super init];
    if(self){
        
        self.currentMode = SEARCH_LEFT;

        _photoArray = CREATE_MUTABLE_ARRAY;
        
        _searchUserArray = CREATE_MUTABLE_ARRAY;
        _searchTagArray = CREATE_MUTABLE_ARRAY;
        
        
        _bannerArray = CREATE_MUTABLE_ARRAY;

        
        _isFirstFetching = YES;
        _noMoreData = NO;
        _isFetching = NO;
        
        _haveExplorerUser = NO;
        _haveExplorerLive = NO;
        
        _explorerUser = CREATE_MUTABLE_ARRAY;
        _explorerLive = CREATE_MUTABLE_ARRAY;
        _explorerLiveShow = CREATE_MUTABLE_ARRAY;
        UITapGestureRecognizer *oneFingerTwoTaps =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneFingerTwoTaps)] ;
        oneFingerTwoTaps.delegate=self;
        // Set required taps and number of touches
        [oneFingerTwoTaps setNumberOfTapsRequired:1];
        [oneFingerTwoTaps setNumberOfTouchesRequired:1];
        [[self view] addGestureRecognizer:oneFingerTwoTaps];
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self configureViewForIOS7];
    
    [self.view setBackgroundColor:LIGHT_BG_COLOR];
    
    self.title = LOCALIZE(@"Search");
    self.navigationController.title = @"";
    
    [self setup];
    
    [self fetchData:YES];
    

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    /* Level 5 crash fixed */
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [EVENT_HANDLER addEventTracking:@"EnterDiscoverPage" withDict:nil];
    _enterPageTimeStamp=CURRENT_TIMESTAMP;
    _enterSearchUserPageTimeStamp=0;
    _refreshCount=0;
    _visibleCellCount=0;
    if([GET_DEFAULT(UPDATE_SEARCH_TIME) intValue] < CURRENT_TIMESTAMP-UPDATE_INTERVAL){
        [self fetchData:YES];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [EVENT_HANDLER addEventTracking:@"LeaveDiscoverPage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp),@"refreshCount":INT_TO_STRING(_refreshCount),@"cellCount":INT_TO_STRING(_visibleCellCount)}];
}

-(void)setup
{

    /* Search Bar */
    
    UIButton* findFriendBtn = [ThemeManager findFriendBtn];
    [findFriendBtn addTarget:self action:@selector(didClickFindFriend:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:findFriendBtn];
    
    _statusView = [[UIView alloc]initWithFrame:CGRectMake(0, -STATUS_BAR_HEIGHT, SCREEN_WIDTH, STATUS_BAR_HEIGHT)];
    [_statusView setBackgroundColor:WHITE_COLOR];
    
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0 , SCREEN_WIDTH, NAVI_BAR_HEIGHT)];
    _searchBar.delegate = self;
    _searchBar.showsCancelButton = NO;
    _searchBar.placeholder = LOCALIZE(@"enter_query");
    [_searchBar setTintColor:MAIN_COLOR];
    _searchBar .autocapitalizationType = UITextAutocapitalizationTypeNone;    
    _searchBar.keyboardType = UIKeyboardTypeTwitter;
//    _searchBar.barTintColor = WHITE_COLOR;

    if([GET_DEFAULT(IS_ADMIN) intValue]>=1){
        [self.view addSubview:_searchBar];
    }

    /* Search Btn Container Mode */
    
    _topView = [[UIView alloc]initWithFrame:CGRectMake(0, NAVI_BAR_HEIGHT+STATUS_BAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
    _topView.alpha = 0;
    _topView.hidden = YES;
    [_topView setBackgroundColor:WHITE_COLOR];
    
    _searchUserBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, NAVI_BAR_HEIGHT)];
    _searchTagBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2, NAVI_BAR_HEIGHT)];
    
    [_searchUserBtn setSelected:YES];
    _searchUserBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [_searchUserBtn setTitle:LOCALIZE(@"User") forState:UIControlStateNormal];
    [_searchUserBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_searchUserBtn setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
    [_searchUserBtn addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];
    _searchTagBtn.titleLabel.font =BOLD_FONT_WITH_SIZE(14);
    [_searchTagBtn setTitle:LOCALIZE(@"Tag") forState:UIControlStateNormal];
    [_searchTagBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_searchTagBtn setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
    [_searchTagBtn addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _searchBtnArray = @[_searchTagBtn,_searchTagBtn];
    
    _tableModeSelectorContainer = [ThemeManager maintabBackgroundImageView];
    _tableModeSelectorContainer.frame = CGRectMake(0, 0, SCREEN_WIDTH, NAVI_BAR_HEIGHT);
    _tableModeSelectorContainer.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
    [_tableModeSelectorContainer setBackgroundColor:LIGHT_BG_COLOR];
    
    _searchIndicatorImageView = [[UIImageView alloc] initWithImage:[IMAGE_FROM_BUNDLE(@"bar") resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)]];
    _searchIndicatorImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH/2, 3);
    _searchIndicatorImageView.center = CGPointMake(SCREEN_WIDTH/4, NAVI_BAR_HEIGHT-3);
    [_tableModeSelectorContainer addSubview:_searchIndicatorImageView];
    
    [_tableModeSelectorContainer addSubview:_searchUserBtn];
    [_tableModeSelectorContainer addSubview:_searchTagBtn];
    
    _searchScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT-TAB_BAR_HEIGHT-STATUS_BAR_HEIGHT-KEYBOARD_HEIGHT-NAVI_BAR_HEIGHT+4)];
    [_searchScrollView setContentSize:CGSizeMake(2*SCREEN_WIDTH, _searchScrollView.frame.size.height)];
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resigbSearchBar:)];
    [_searchScrollView addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
    _searchScrollView.userInteractionEnabled = YES;
    
    [self scrollViewSettings:_searchScrollView];
    
    _userTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH, _searchScrollView.frame.size.height)];
    _tagTableView = [[UITableView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, _searchScrollView.frame.size.height)];
    [self tableViewSettings:_userTableView];
    [self tableViewSettings:_tagTableView];
    
    _userTableView.hidden = YES;
    _tagTableView.hidden = YES;
    
    [_searchScrollView addSubview:_userTableView];
    [_searchScrollView addSubview:_tagTableView];
    
    
    _noUserLabel = [ThemeManager getNoDataImageView];
    CGRect rect = _noUserLabel.frame;
    rect.origin.y = 0;
    _noUserLabel.frame = rect;
    _noTagLabel = [ThemeManager getNoDataImageView];
    rect.origin.x += SCREEN_WIDTH;
    _noTagLabel.frame = rect;
    _noUserLabel.hidden = NO;
    _noTagLabel.hidden = NO;
    
    [_searchScrollView addSubview:_noUserLabel];
    [_searchScrollView addSubview:_noTagLabel];

    
    [_userTableView registerClass:[SearchUserCell class] forCellReuseIdentifier:searchUserIdentity];
    _userTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    [_tagTableView registerClass:[TagViewCell class] forCellReuseIdentifier:searchTagIdentity];
    _tagTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    _tableViewArray = @[_userTableView,_tagTableView];
    _searchArray = @[_searchUserArray,_searchTagArray];
    
    /* CollectionView Setting */
    _mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-TAB_BAR_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
    
    if([GET_DEFAULT(IS_ADMIN) intValue]>=1){
        _mainScrollView.frame = CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT-TAB_BAR_HEIGHT-2*NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT);
    }

    [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, _mainScrollView.frame.size.height)];
    [self scrollViewSettings:_mainScrollView];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    _photoCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, -4, SCREEN_WIDTH, SCREEN_HEIGHT-TAB_BAR_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT) collectionViewLayout:flowLayout ];
    [self collectionViewSettings:_photoCollectionView];

    __weak SearchViewController *weakSelf = self;
    
    [_photoCollectionView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData:YES];
    }];
    
    /* no data view & progress view settings */
    
    _noDataImageView = [ThemeManager getNoDataImageView];
    
    _progressView = [UIActivityIndicatorView new];
    _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView setColor:MAIN_COLOR];
    
    [_mainScrollView addSubview:_statusView];
    

    [_photoCollectionView addSubview:_noDataImageView];
    [_mainScrollView addSubview:_photoCollectionView];
    [_mainScrollView addSubview:_progressView];

    [_topView addSubview:_searchScrollView];
    [_topView addSubview:_tableModeSelectorContainer];

    [self.view addSubview:_mainScrollView];
    [self.view addSubview:_topView];
    
}

-(void)fetchData:(BOOL)refresh
{
    
    PostObject* lastPostObject = NULL;
    int beforeTime = INT32_MAX;

    
    if(refresh){
        _refreshCount++;
        _noMoreData = false;
        [self fetchExplorer:^(BOOL success) {
            [_photoCollectionView reloadData];
        }];
    }else{
        if([_photoArray count]>0){
            
            lastPostObject = _photoArray[[_photoArray count]-1];
            if(lastPostObject!=NULL && !refresh){
                beforeTime = lastPostObject.timestamp;
            }
            
        }
    }
    
    if(_isFetching || _noMoreData){
        return;
    }

    _isFetching = YES;
    
    if(refresh && !_isFirstFetching){
        [_photoCollectionView.pullToRefreshView startAnimating];
    }
 
    if(_isFirstFetching){
        [_progressView startAnimating];
        _isFirstFetching = NO;
    }
    
    [API_MANAGER getHotPost:beforeTime count:18 withCompletion:^(BOOL success, NSArray *posts) {

        DLog(@"get Hot Post");
        
        [_progressView stopAnimating];
        [_photoCollectionView.pullToRefreshView stopAnimating];
        _isFetching = NO;
        
        if([posts count]==0 && [_photoArray count]==0){
            _noDataImageView.hidden = NO;
        }else{
            _noDataImageView.hidden = YES;
        }
        
        if(success){
            
            if(refresh){
                [_photoArray removeAllObjects];
                [DEFAULTS setObject:INT_TO_NUMBER(CURRENT_TIMESTAMP) forKey:UPDATE_SEARCH_TIME];
                [DEFAULTS synchronize];
            }
            
            /* filter reported user post */
            for(PostObject* post in posts){
                if(![[[DatabaseManager sharedInstance] getReportRecord:@"userID"] containsObject:post.user.userID]){
                    [_photoArray addObject:post];
                }
            }
            
//            [_photoArray addObjectsFromArray:posts];

            if([posts count]<18){
                _noMoreData = YES;
            }
            
            [_photoCollectionView reloadData];
            
//            [_photoCollectionView reloadSections:[NSIndexSet indexSetWithIndex:3]];
            
        }else{
            
        }

    }];
    
}

-(void)fetchSearchTagWithMode:(int)mode andSearchText:(NSString*)searchText
{
    if(_currentMode== SEARCH_LEFT){
        
        [API_MANAGER getSearchUsers:searchText followingOnly:0 offset:0 count:100 completion:^(BOOL success, NSArray *userObjects) {
            
            if(![searchText isEqualToString:_searchBar.text])
                return ;
            
            if(success){
                
                if(![_searchBar.text isEqualToString:searchText]){
                    return;
                }
                
                if([userObjects count]==0){
                    _userTableView.hidden = YES;
                    _noUserLabel.hidden = NO;
                }else{
                    _userTableView.hidden = NO;
                    _noUserLabel.hidden = YES;
                }
                
                if(_searchBar.text.length==0){
                    [_searchUserArray removeAllObjects];
                    _noUserLabel.hidden = NO;
                }else{
                    [_searchUserArray removeAllObjects];
                    [_searchUserArray addObjectsFromArray:userObjects];
                }
                [_userTableView reloadData];
            }
        }];
        
    }else if(_currentMode ==SEARCH_RIGHT){
        
        [API_MANAGER searchHashTag:searchText offset:0 count:100 withCompletion:^(BOOL success, NSArray *tags) {
            
            if(![searchText isEqualToString:_searchBar.text])
                return ;
            
            if(success){
                if([tags count]==0){
                    _tagTableView.hidden = YES;
                    _noTagLabel.hidden = NO;
                }else{
                    _tagTableView.hidden = NO;
                    _noTagLabel.hidden = YES;
                }
                
                [_searchTagArray removeAllObjects];
                for(TagObject* tag in tags){
                    [_searchTagArray addObject:tag];
                }
                [_tagTableView reloadData];
            }
        }];
    }
}

-(void)clearSearchData
{
    [_searchUserArray removeAllObjects];
    [_searchTagArray removeAllObjects];
    [_tagTableView reloadData];
    [_userTableView reloadData];
    _tagTableView.hidden = YES;
    _userTableView.hidden = YES;
    
}

-(void)changeModeAction:(id)sender
{
    if([sender isEqual:_searchUserBtn]){
        [self switchToMode:_searchScrollView toMode:SEARCH_LEFT animated:YES];
    }else if ([sender isEqual:_searchTagBtn]){
        [self switchToMode:_searchScrollView toMode:SEARCH_RIGHT animated:YES];
    }
}


#pragma mark SearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    _topView.alpha = 1;
    _topView.hidden = NO;
    if (_enterSearchUserPageTimeStamp!=0) {
        _enterSearchUserPageTimeStamp=CURRENT_TIMESTAMP;
    }

    _photoCollectionView.alpha = 0;
    _searchBar.showsCancelButton = YES;
   
    [UIView animateWithDuration:0.3 animations:^{
        _statusView.frame = CGRectMake(0, STATUS_BAR_HEIGHT, SCREEN_WIDTH, STATUS_BAR_HEIGHT);
        _searchBar.frame = CGRectMake(0, STATUS_BAR_HEIGHT, SCREEN_WIDTH, NAVI_BAR_HEIGHT);
        _photoCollectionView.frame = CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT-TAB_BAR_HEIGHT-2*NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT);
    }];
    if (_currentMode==SEARCH_LEFT) {
        if (_flurryUserCount!=1) {
            _flurryUserCount=1;
        }
    }else{
        if (_flurryHashCount!=1) {
            _flurryHashCount=1;
        }
    }
 
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    if (_currentMode==SEARCH_LEFT) {


    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self resigbSearchBar:searchBar];
    [EVENT_HANDLER addEventTracking:@"SearchPageDuration" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterSearchUserPageTimeStamp)}];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    // fetch Search query
    if(searchText.length==0){
        
        _noUserLabel.hidden = NO;
        _noTagLabel.hidden = NO;
        if(_currentMode==SEARCH_LEFT){
            [_searchUserArray removeAllObjects];
            [_userTableView reloadData];
        }else{
            [_searchTagArray removeAllObjects];
            [_tagTableView reloadData];
        }
        return;
    }
    [self fetchSearchTagWithMode:_currentMode andSearchText:searchText];
}


#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([tableView isEqual:_userTableView]){
        return [_searchUserArray count];
    }else{ // if([tableView isEqual:_tagTableView]){
        return [_searchTagArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView isEqual:_userTableView]){
        
        SearchUserCell *cell = [tableView dequeueReusableCellWithIdentifier:searchUserIdentity];
        cell.delegate = self;
        
        if(cell==nil){
            cell = [[SearchUserCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchUserIdentity];
        }
        
        UserObject* user = _searchUserArray[indexPath.row];
        [cell reloadCell:user];
        return cell;
        
    }else if([tableView isEqual:_tagTableView]){
        
        TagViewCell *cell = [tableView dequeueReusableCellWithIdentifier:searchTagIdentity];

        if(cell==nil){
            cell = [[TagViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchTagIdentity];
        }
        
        TagObject* tag = _searchTagArray[indexPath.row];
        [cell reloadCell:tag];
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView isEqual:_userTableView]){
        return searchUserCellHeight;
    }else{
        return searchTagCellHeight;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [EVENT_HANDLER addEventTracking:@"SearchPageDuration" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterSearchUserPageTimeStamp)}];

    if([tableView isEqual:_userTableView]){

        
        
        UserObject* user = _searchUserArray[indexPath.row];
           [EVENT_HANDLER addEventTracking:@"SearchUserAndClick" withDict:@{@"targetUserID":user.userID,@"query":_searchBar.text}];
        UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
        userCtrl.user = user;
        [userCtrl getUserWithUserID:user.userID];
        userCtrl.hidesBottomBarWhenPushed = YES;
        userCtrl.dismissNavbar = YES;
        [self.navigationController pushViewController:userCtrl animated:YES];
        
    }else{

        if([GET_DEFAULT(IN_SANDBOX) isEqualToString:@"1"]){
            return;
        }
        
        
        TagObject* tag = _searchTagArray[indexPath.row];
        [EVENT_HANDLER addEventTracking:@"SearchHashTagAndClick" withDict:@{@"tag":tag.hashTag,@"query":_searchBar.text}];

        BrowsePostsViewController* broVC =[BrowsePostsViewController new];
        broVC.tagQuery = tag.hashTag;
        broVC.title = [NSString stringWithFormat:@"#%@",tag.hashTag];
        broVC.hidesBottomBarWhenPushed = YES;
        broVC.dismissNavbar = YES;
        [self.navigationController pushViewController:broVC animated:YES];
        
    }
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 4;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == 1 || section ==2) {
        if ((_haveExplorerLive && section == 1) || (_haveExplorerUser && section == 2)) {
            return 1;
        }
        return 0;
    }else if(section==0){
        return 1;
    }else {
        return [_photoArray count];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 || indexPath.section == 2) {
        
        ExplorerCell* exploreCell = (ExplorerCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"explorerCell" forIndexPath:indexPath];
        if (exploreCell == nil){
            exploreCell = [ExplorerCell new];
        }
        
        switch (indexPath.section) {
            case 1:
                if(_haveExplorerLive){
                    exploreCell.type = LIVE_MODE;
                    [exploreCell reloadCell:_explorerLive withText:LOCALIZE(@"explorer_live") andSubTitle:LOCALIZE(@"sub_explorer_live")];
                }
                break;
            case 2:
                if(_haveExplorerUser){
                    exploreCell.type = USER_MODE;
                    [exploreCell reloadCell:_explorerUser withText:LOCALIZE(@"explorer_user") andSubTitle:LOCALIZE(@"sub_explorer_user")];
                }
                break;
                
            default:
                break;
        }
        return exploreCell;
        
    }else if (indexPath.section == 0){
        BannerCell* bannerCell=(BannerCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"bannerCell" forIndexPath:indexPath];
        bannerCell.delegate=self;
        if (bannerCell == nil) {
            bannerCell = [BannerCell new];
        }
        return bannerCell;
        
    }else {
        
        PostGridCell *cell = (PostGridCell*)[collectionView dequeueReusableCellWithReuseIdentifier:kReuseCellIdentifier forIndexPath:indexPath];
        
        cell.post = _photoArray[indexPath.row];
        
        if (indexPath.row >= [_photoArray count] - 15 && !_isFetching) {
            [self fetchData:NO];
        }
        
        _visibleCellCount++;
        
        return cell;

    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            

            break;
        }
        case 1:
        {
            
            HotLiveV2ViewController *hotVC = [HotLiveV2ViewController new];
            hotVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:hotVC animated:YES];
            break;
        }
        case 2:
        {
            ExploreUserViewController *usVC = [ExploreUserViewController new];
            usVC.suggestionArray = [_explorerUser mutableCopy];
            usVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:usVC animated:YES];
            break;
        }
        default:
        {
            if (indexPath.row >= [_photoArray count]) {
                return;
            }
            
            //event tracking
            for (UICollectionViewCell *cell in [self.photoCollectionView visibleCells]) {
                NSIndexPath *indexPath = [self.photoCollectionView indexPathForCell:cell];
                if (indexPath.section == 0 || indexPath.section == 1) {
                    _touchY = _touchY - [ExplorerCell getCellHeight];
                };
            }

            int x = (int)(_touchX / (SCREEN_WIDTH / 3));
            int y = (int)(_touchY / ((SCREEN_HEIGHT - TAB_BAR_HEIGHT - NAVI_BAR_HEIGHT - STATUS_BAR_HEIGHT - NAVI_BAR_HEIGHT) / 4));
            [EVENT_HANDLER addEventTracking:@"ClickDiscoverPictures"
                                   withDict:@{@"position": INT_TO_STRING((y - 1) * 3 + x),
                                              @"number":[NSString stringWithFormat:@"%ld", indexPath.row]}];

            SinglePostViewController *ctrl = nil;
            if (ctrl == nil) {
                ctrl = [[SinglePostViewController alloc] init];
                ctrl.hidesBottomBarWhenPushed = YES;
            }
            ctrl.enterMode = EnterFromDiscover;
            ctrl.post = [_photoArray objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:ctrl animated:YES];
            break;
        }
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if(section == 1 || section == 2){
        return UIEdgeInsetsMake(0,0,0,0);
    }else{
        return UIEdgeInsetsMake(2,2,2,2);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

# pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 || indexPath.section == 2) {
        if ((indexPath.section == 1 && _haveExplorerLive) || (indexPath.section == 2 && _haveExplorerUser)) {
            return CGSizeMake(SCREEN_WIDTH, [ExplorerCell getCellHeight]);
        }
        else {
            return CGSizeMake(SCREEN_WIDTH, 1);
        }
    }else if (indexPath.section == 0){
        return CGSizeMake(SCREEN_WIDTH, kBannerHeight);
    }else {
        return CGSizeMake(SCREEN_WIDTH / 3 - 2, SCREEN_WIDTH / 3 - 2);
    }
}

#pragma mark UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if([_tableViewArray containsObject:scrollView] || [scrollView isEqual:_mainScrollView]|| [scrollView isEqual:_photoCollectionView]) return;
    
    if(scrollView.contentOffset.x==0) {
        [self switchToMode:scrollView toMode:0 animated:YES];
    }
    else if(scrollView.contentOffset.x==SCREEN_WIDTH) {
        [self switchToMode:scrollView toMode:1 animated:YES];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [LIKE_HANDLER likeBatchUpload];
}

-(void) switchToMode:(UIScrollView*)scrollView toMode:(int)mode animated:(BOOL) animated
{
    DLog(@"swich mode: %d",mode);
    
    if(_currentMode==mode) {
            return;
        }
    _currentMode = mode;
    
    if(mode==SEARCH_LEFT){
        [_searchUserBtn setSelected:YES];
        [_searchTagBtn setSelected:NO];
    }else if(mode==SEARCH_RIGHT){
        [_searchUserBtn setSelected:NO];
        [_searchTagBtn setSelected:YES];
    }
    
    [self reloadUIAnimated:scrollView withAnimation:animated];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint pointOnScreen = [touch locationInView:nil];
    _touchX=pointOnScreen.x;
    _touchY=pointOnScreen.y;
    if([touch.view isEqual:_searchScrollView]){
        return YES;
    }
    return NO;
}


#pragma mark - UI Setting
-(void) collectionViewSettings:(UICollectionView*)collectionview
{
    collectionview.delegate = self;
    collectionview.dataSource = self;
    collectionview.scrollsToTop = YES;
    collectionview.alwaysBounceVertical = YES;
    [collectionview setBackgroundColor:LIGHT_BG_COLOR];
    [collectionview registerNib:[UINib nibWithNibName:kReuseCellIdentifier bundle:nil] forCellWithReuseIdentifier:kReuseCellIdentifier];
    [collectionview registerClass:[ExplorerCell class] forCellWithReuseIdentifier:@"explorerCell"];
    [collectionview registerClass:[BannerCell class] forCellWithReuseIdentifier:@"bannerCell"];

}

-(void) scrollViewSettings:(UIScrollView*)scrollView
{
    scrollView.delegate = self;
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = YES;
    scrollView.delaysContentTouches = YES;
    scrollView.canCancelContentTouches = NO;
    scrollView.scrollsToTop = NO;
}

-(void) tableViewSettings:(UITableView*)tableView
{
    tableView.delegate = self;
    tableView.dataSource = self;
    
}

-(void) reloadUIAnimated:(UIScrollView*)scrollView withAnimation:(BOOL)animated
{
    NSArray* modeButtonArray;
    NSArray* scrollViewArray;
    UIScrollView* currentScrollView;
    UIImageView* indecatorView;
    int viewMode;
    
    modeButtonArray = _searchButtonArray;
    scrollViewArray =  _tableViewArray;
    viewMode = _currentMode;
    currentScrollView = _searchScrollView;
    indecatorView = _searchIndicatorImageView;
    
    
    for (int mode=0; mode < 2; mode++) {
        
        UIButton* modeButton = modeButtonArray[mode];
        UIScrollView *tableView = scrollViewArray[mode];
        
        if(mode == viewMode) {
            
            [modeButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
            [modeButton setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
            [modeButton setTitleColor:MAIN_COLOR forState:UIControlStateHighlighted];
            tableView.scrollsToTop = YES;
            modeButton.userInteractionEnabled = NO;
            
            [currentScrollView scrollRectToVisible:CGRectMake(SCREEN_WIDTH*mode, 0, SCREEN_WIDTH, _mainScrollView.frame.size.height) animated:YES];
            
            if(animated) {
                [UIView animateWithDuration:0.3 animations:^{
                    indecatorView.center = CGPointMake((SCREEN_WIDTH/4)+(SCREEN_WIDTH/2)*mode, NAVI_BAR_HEIGHT-1);
                }];
            } else {
                indecatorView.center = CGPointMake((SCREEN_WIDTH/4)+(SCREEN_WIDTH/2)*mode, NAVI_BAR_HEIGHT-1);
            }
            
        }else {
            
            [modeButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
            [modeButton setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
            
            tableView.scrollsToTop = NO;
            modeButton.userInteractionEnabled = YES;
            
        }
    }
    
    // reload search view
    if(_searchBar.text.length!=0){
        [self fetchSearchTagWithMode:_currentMode andSearchText:_searchBar.text];
    }
}

-(void)didClickFindFriend:(id)sender
{

//    [API_MANAGER getSuggestedUsers:0 count:10 completion:^(BOOL success, NSArray *suggestedUser) {
//        if(success){
//            [DIALOG_MANAGER showCompleteToast];
//        }else{
//            [DIALOG_MANAGER showNetworkFailToast];
//        }
//    }];
    
    
//    [API_MANAGER getHotUsers:0 count:10 completion:^(BOOL success, NSArray *suggestedUser) {
//        if(success){
//            [DIALOG_MANAGER showCompleteToast];
//        }else{
//            [DIALOG_MANAGER showNetworkFailToast];
//        }
//    }];


//    [API_MANAGER getSelfInfo:^(BOOL success) {
//        if(success){
//            [DIALOG_MANAGER showCompleteToast];
//        }else{
//            [DIALOG_MANAGER showNetworkFailToast];
//        }
//    }];
    
    
    FindFriendViewController* findFriendVC = [FindFriendViewController new];
    findFriendVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:findFriendVC animated:YES];

}


#pragma mark Explorer
-(void) fetchExplorer:(void(^)(BOOL success))callback
{
    
    __block int index = 0;
    
    [API_MANAGER getExploreSuggestedUsers:INT32_MAX count:50 completion:^(BOOL success, NSArray *suggestedUser) {
        
        index++;
        
        if(success){
            
            if([suggestedUser count]==0){
                _haveExplorerUser = NO;
            }else{
                _haveExplorerUser = YES;
            }
            _explorerUser = [suggestedUser mutableCopy];
        }else{
            [_explorerUser removeAllObjects];
        }
        if(index==2){
            callback(YES);
        }
    }];

    
    NSString* country = @"local";
    if([GET_DEFAULT(IN_SANDBOX) isEqualToString:@"1"]){
        country = @"TW";
    }
    
    [API_MANAGER getSuggestedLiveStreams:country count:50 withCompletion:^(BOOL success, NSArray *liveSteamObjects) {

        index++;
        if(success){
            if([liveSteamObjects count]==0){
                _haveExplorerLive = NO;
            }else{
                _haveExplorerLive = YES;
            }
            _explorerLive = [liveSteamObjects mutableCopy];
        }else{
            [_explorerLive removeAllObjects];
        }
        
        if(index==2){
            callback(YES);
        }

    }];
}

-(void)resigbSearchBar:(id)sender
{
    [_searchBar resignFirstResponder];
    _searchBar.showsCancelButton = NO;
    _searchBar.text = @"";
    
    
    _noUserLabel.hidden = NO;
    _noTagLabel.hidden = NO;
    
    _topView.alpha = 0;
    _topView.hidden = YES;
    _photoCollectionView.alpha = 1;
    [self clearSearchData];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        _statusView.frame = CGRectMake(0, -STATUS_BAR_HEIGHT, SCREEN_WIDTH, STATUS_BAR_HEIGHT);
        _searchBar.frame = CGRectMake(0, 0, SCREEN_WIDTH, NAVI_BAR_HEIGHT);
        _photoCollectionView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-TAB_BAR_HEIGHT-2*NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT);
        
    }];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];

}


#pragma mark SearchUserDelegate
-(void)shoudResignSearchbar
{
    _topView.hidden = YES;
    _searchBar.text = @"";
    [self resigbSearchBar:_searchBar];
}
-(void)shoudBecomeFirstResponse
{
    
}

-(void)didFollowUser:(UserObject*)user
{
}

-(void)dealloc
{
    
}

-(void)didClickBanner:(int)contentIndex
{
    NSLog(@"%d",contentIndex);
//    BannerObject* banner = [_bannerArray objectAtIndex:0];
//    switch (banner.type) {
//        case BannerPost:
//        {
//            SinglePostViewController *postVC = [SinglePostViewController new];
//            postVC.post = banner.post;
//            [self.navigationController pushViewController:postVC animated:YES];
//        }
//            break;
//        case BannerUser:
//        {
//            UserProfileViewController* userVC = [UserProfileViewController new];
//            userVC.user=banner.user;
//            userVC.hidesBottomBarWhenPushed = YES;
//            
//            [self.navigationController pushViewController:userVC animated:YES];
//        }
//            break;
//        case BannerLiveStream:
//        {
//            
//            LiveStreamViewController* vc = [SINGLETON liveStreamSharedManager];
//            if (vc.canPresentView) {
//                CustomTabbarController *tabVC = [SINGLETON mainTabBarViewController];
//                
//                vc.liveStreamMode = LiveStreamModeWatch;
//                vc.liveStream = banner.livestream;
//                vc.delegate = tabVC;
//                vc.transitioningDelegate = tabVC;
//                vc.modalTransitionStyle = UIModalPresentationCustom;
//                vc.modalPresentationStyle = UIModalPresentationFullScreen;
//                if (tabVC.presentedViewController==nil) {
//                    [tabVC presentViewController:vc animated:YES completion:nil];
//                }
//            }
//        }
//            break;
//        case BannerUrl:
//        {
//            [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"open_on_safari") message:banner.url buttonText:LOCALIZE(@"OK") cancelable:YES withCompletion:^(BOOL okClicked) {
//                NSString *myURL;
//                myURL = [banner.url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//                
//                if(okClicked){
//                    if (![banner.url.lowercaseString hasPrefix:@"http://"]&&![banner.url.lowercaseString hasPrefix:@"https://"]) {
//                        
//                        myURL = [NSString stringWithFormat:@"http://%@",banner.url];
//                    }else{
//                        //                        myURL=string;
//                    }
//                    
//                    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")){
//                        SFSafariViewController *safariViewController = [[SFSafariViewController alloc]
//                                                                        initWithURL:[NSURL URLWithString:myURL]
//                                                                        entersReaderIfAvailable:YES];
//                        
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            
//                            [self.navigationController presentViewController:safariViewController animated:YES completion:nil];
//                            
//                        });
//                    }else{
//                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:banner.url]];
//                    }
//                }
//            }];
//        }
//            break;
//        default:
//            break;
//    }
}
@end
