//
//  BannerObject.m
//  Story17
//
//  Created by LiRex on 2016/2/23.
//  Copyright © 2016年 POPO_INNOVATION. All rights reserved.
//

#import "BannerObject.h"

@implementation BannerObject

+ (BannerObject*)getBannerWithDict:(NSDictionary*)bannerInfoDict
{
    BannerObject* banner = [BannerObject new];
    if ([bannerInfoDict[@"type"]isEqualToString:@"post"]) {
        banner.type = BannerPost;
    }else if ([bannerInfoDict[@"type"]isEqualToString:@"user"]){
        banner.type = BannerUser;
    }else if ([bannerInfoDict[@"type"]isEqualToString:@"livestream"]){
        banner.type = BannerLiveStream;
    }else if ([bannerInfoDict[@"type"]isEqualToString:@"url"]){
        banner.type = BannerUrl;
    }
    switch (banner.type) {
        case BannerPost:
            banner.post = [PostObject getPostWithDict:bannerInfoDict[@"postInfo"]];
            break;
        case BannerUser:
            banner.user = [UserObject getUserWithDict:bannerInfoDict[@"userInfo"]];
            break;
        case BannerLiveStream:
            banner.livestream = [LiveStreamObject getLiveStreamWithDict:bannerInfoDict[@"liveStreamInfo"]];
            break;
        case BannerUrl:
            banner.url = bannerInfoDict[@"url"];
            break;
        default:
            break;
    }
    
    return banner;  
}

@end