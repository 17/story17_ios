//
//  ContactsData.m
//  Dare
//
//  Created by POPO on 11/26/14.
//  Copyright (c) 2014 POPO_INNOVATION. All rights reserved.
//

#import "ContactsData.h"

@implementation ContactsData

@synthesize numbers;
@synthesize image;
@synthesize emails;
@synthesize firstNames;
@synthesize lastNames;
@synthesize fullName;

@end
