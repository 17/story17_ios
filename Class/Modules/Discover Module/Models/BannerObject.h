//
//  BannerObject.h
//  Story17
//
//  Created by LiRex on 2016/2/23.
//  Copyright © 2016年 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "LiveStreamObject.h"
#import "PostObject.h"
#import "UserObject.h"

typedef NS_ENUM(NSUInteger, BannerType) {
    BannerPost,
    BannerLiveStream,
    BannerUser,
    BannerUrl
};

@interface  BannerObject : NSObject


@property (nonatomic , strong) UserObject* user;
@property (nonatomic , strong) LiveStreamObject* livestream;
@property (nonatomic , strong) PostObject* post;
@property (nonatomic , strong) NSString* url;
@property (nonatomic , assign) BannerType type;
@property (nonatomic , strong) NSString* coverPicture;
@end