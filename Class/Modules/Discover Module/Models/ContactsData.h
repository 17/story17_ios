//
//  ContactsData.h
//  Dare
//
//  Created by POPO on 11/26/14.
//  Copyright (c) 2014 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactsData : NSObject
@property (strong, nonatomic) NSMutableArray* numbers;
@property (strong, nonatomic) NSMutableArray *emails;
@property (strong, nonatomic) UIImage* image;
@property (strong, nonatomic) NSString *firstNames;
@property (strong, nonatomic) NSString *lastNames;
@property (strong, nonatomic) NSString *fullName;

@end
