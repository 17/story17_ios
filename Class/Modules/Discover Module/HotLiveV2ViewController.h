//
//  NotificationViewController.h
//  story17
//
//  Created by POPO Chen on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LiveNationViewController.h"
#import "LivestreamCell.h"

typedef enum {
    HOT_LIVESTREAM_MODE,
    LATEST_LIVESTREAM_MODE,
    SUGGEST_LIVESTREAM_MODE
} HOTLIVE_MODE;

@interface HotLiveV2ViewController : UIViewController <UIScrollViewDelegate,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,LiveNationTableDelegate,LivestreamCellDelegate>


@property HOTLIVE_MODE viewMode;

@property(nonatomic,strong) UIView* mainView;

/* mode indicator */
@property(nonatomic,strong) UIView* modeSelectorContainer;
@property(nonatomic,strong) UIImageView* modeIndicatorImageView;

/* mode btn array */
@property(nonatomic,strong) NSArray* btnArray;
@property(nonatomic,strong) UIButton* hotBtn;
@property(nonatomic,strong) UIButton* sugBtn;
@property(nonatomic,strong) UIButton* latestBtn;

/* main scrollview */
@property(nonatomic,strong) UIScrollView* mainScrollView;
@property(nonatomic,strong) NSArray* collectionViewArray;
@property(nonatomic,strong) UICollectionView* hotCollectionView;
@property(nonatomic,strong) UICollectionView* suggestionCollectionView;
@property(nonatomic,strong) UICollectionView* latestCollectionView;

/* data array */
@property (nonatomic,strong) NSArray* liveArray;
@property (nonatomic,strong) NSMutableArray* hotLiveArray;
@property (nonatomic,strong) NSMutableArray* suggestLiveArray;
@property (nonatomic,strong) NSMutableArray* latestLiveArray;

/* progress view */
@property (nonatomic,strong) NSArray* progressArray;
@property (nonatomic,strong) UIActivityIndicatorView* progressView;
@property (nonatomic,strong) UIActivityIndicatorView* progressView2;
@property (nonatomic,strong) UIActivityIndicatorView* progressView3;

/* no data view */
@property (nonatomic,strong) NSArray* noDataArray;
@property (nonatomic,strong) UIImageView* noDataImageView;
@property (nonatomic,strong) UIImageView* noDataImageView2;
@property (nonatomic,strong) UIImageView* noDataImageView3;


@property (nonatomic,strong) NSString* currentCountry;
@property (nonatomic,strong) UIButton* flagButton;

@property BOOL isLocal;


@property BOOL isFirstFetching;
@property BOOL isFetching;
@property int enterTrendingTimeStamp;
@property int enterLatestTimeStamp;
@property int visibleTrendingCount;
@property int visibleLatestCount;
@end
