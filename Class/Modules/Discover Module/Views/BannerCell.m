//
//  BannerCell.m
//  Story17
//
//  Created by LiRex on 2016/2/23.
//  Copyright © 2016年 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BannerCell.h"

@implementation BannerCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        
        _bannerImageView=[[UIImageView alloc]initWithFrame:frame];
        [_bannerImageView setBackgroundColor:RED_COLOR];
        _bannerScrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 100)];
        
        
        CGFloat imageH = 100;
        CGFloat imageY = 0;
        NSInteger totalCount = 5;
        for (int i = 0; i < totalCount; i++) {
            UIImageView *imageView = [[UIImageView alloc] init];
            UILabel *test=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 100)];
            [test setTextColor:BLACK_COLOR];
            [test setFont:[UIFont fontWithName:@"Helvetica" size:20]];

            CGFloat imageX = i * SCREEN_WIDTH;
            imageView.frame = CGRectMake(imageX, imageY, SCREEN_WIDTH, imageH);
            test.frame = CGRectMake(imageX, imageY, SCREEN_WIDTH, imageH);

            if (i==0) {
                NSString *name = [NSString stringWithFormat:@"giftpoint_3"];
                imageView.image = [UIImage imageNamed:name];
                [test setText:@"3"];
                
            }else if (i==4){
                NSString *name = [NSString stringWithFormat:@"giftpoint_1"];
                imageView.image = [UIImage imageNamed:name];
                [test setText:@"1"];
            }else{
                NSString *name = [NSString stringWithFormat:@"giftpoint_%d", i + 1];
                imageView.image = [UIImage imageNamed:name];
                [test setText:INT_TO_STRING(i)];

            }
            [test setTextColor:BLACK_COLOR];
            [test setTextAlignment:NSTextAlignmentCenter];
//            [self.bannerScrollView addSubview:imageView];
            [self.bannerScrollView addSubview:test];
        }
        self.bannerScrollView.showsHorizontalScrollIndicator = NO;
        self.bannerScrollView.contentSize = CGSizeMake(totalCount *SCREEN_WIDTH, 100);
        self.bannerScrollView.pagingEnabled = YES;
        self.bannerScrollView.delegate = self;
        [self addTimer];
        [self.contentView addSubview:_bannerScrollView];
        [self.bannerScrollView scrollRectToVisible:CGRectMake(SCREEN_WIDTH, 0, _bannerScrollView.frame.size.width, _bannerScrollView.frame.size.height) animated:NO];
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
        [_bannerScrollView addGestureRecognizer:singleTap];
    }
    
    return self;

}



- (void)nextImage
{
    
             _nowPage++;
  
    
         CGFloat x = _nowPage * self.bannerScrollView.frame.size.width;
    [UIView animateWithDuration:1 animations:^{
        self.bannerScrollView.contentOffset = CGPointMake(x, 0);

    }];
    if(_nowPage == 4) {
        _nowPage=0;
        [self.bannerScrollView scrollRectToVisible:CGRectMake(SCREEN_WIDTH, 0, _bannerScrollView.frame.size.width, _bannerScrollView.frame.size.height) animated:NO];
    }
}

- (int)currentPage
{
    return _nowPage;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
         CGFloat scrollviewW =  _bannerScrollView.frame.size.width;
         CGFloat x = _bannerScrollView.contentOffset.x;
         int page = (x + scrollviewW / 2) /  scrollviewW;
         _nowPage = page;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
         [self removeTimer];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
         [self addTimer];
}

- (void)addTimer
{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(nextImage) userInfo:nil repeats:YES];
}

- (void)removeTimer
{
    [self.timer invalidate];
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (self.bannerScrollView.contentOffset.x < SCREEN_WIDTH/2) {
        [self.bannerScrollView scrollRectToVisible:CGRectMake(3*SCREEN_WIDTH, 0, _bannerScrollView.frame.size.width, _bannerScrollView.frame.size.height) animated:NO];
    }
    else if(self.bannerScrollView.contentOffset.x > 3*SCREEN_WIDTH+SCREEN_WIDTH/2) {
        [self.bannerScrollView scrollRectToVisible:CGRectMake(SCREEN_WIDTH, 0, _bannerScrollView.frame.size.width, _bannerScrollView.frame.size.height) animated:NO];
    }
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    int contentIndex=0;
    if (_nowPage==0) {
        contentIndex=3;
    }else if (_nowPage==4){
        contentIndex=0;
    }else{
        contentIndex=_nowPage;
    }
    [_delegate didClickBanner:contentIndex];
}
@end
