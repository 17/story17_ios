//
//  NotifCell.m
//  Story17
//
//  Created by POPO on 5/8/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "ContactCell.h"
#import "Constant.h"




@implementation ContactCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self awakeFromNib];
        
    }
    return self;
}

- (void)awakeFromNib {
    
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _type = ContactStatusInviting;
    
    _userTitleView = [UserTitleView new];
    [_userTitleView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 55)];
    
    _followBtn = [UIButton new];
    [_followBtn setFrame:CGRectMake(10, 55/2-FOLLOW_BTN_HEIGHT/2, FOLLOW_BTN_WIDTH, FOLLOW_BTN_HEIGHT)];
    _followBtn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(12);

    UIView* followBgView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - FOLLOW_BTN_WIDTH-20,0, FOLLOW_BTN_WIDTH+20, 55)];
    followBgView.userInteractionEnabled = YES;
    [followBgView addSubview:_followBtn];
    
    __weak ContactCell* weakself = self;
    [followBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [weakself didClickFollow:sender];
    }]];
    
    _selfImageView = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 45, 45)];
    _bottomLine = [ThemeManager separaterLine];
    _bottomLine.frame = CGRectMake(10, 54.5, SCREEN_WIDTH-20, 0.3);
    
    [self.contentView addSubview:_userTitleView];
    [self.contentView addSubview:_selfImageView];
    [self.contentView addSubview:_bottomLine];
    [self.contentView addSubview:followBgView];

}


-(void)reloadCell:(UserObject*)user
{
    _user = user;
    
    if(_user.isVerified==1){
        [_userTitleView verified:YES];
    }else{
        [_userTitleView verified:NO];
    }
    
    if(_user.isChoice==1){
        [_userTitleView choiced:YES];
    }else{
        [_userTitleView choiced:NO];
    }
    
    if(_user.crossGreatWall==1){
        [_userTitleView chinaFlag:YES];
    }else{
        [_userTitleView chinaFlag:NO];
    }
    
    if(_user.isInternational==1){
        [_userTitleView internationalFlag:YES];
    }else{
        [_userTitleView internationalFlag:NO];
    }
    
    if(_type == ContactStatusFollowing){
        
        _userTitleView.title = user.openID;
        _userTitleView.subTitle = user.name;
        _userTitleView.image = user.picture;
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        [_followBtn setTitle:LOCALIZE(@"Following") forState:UIControlStateNormal];
        [_followBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];

        [_followBtn removeTarget:self action:@selector(invitedFriend:) forControlEvents:UIControlEventTouchUpInside];
        [_followBtn addTarget:self action:@selector(didClickFollow:) forControlEvents:UIControlEventTouchUpInside];
        
    }else if (_type == ContactStatusUsing){
        
        _userTitleView.title = user.openID;
        _userTitleView.subTitle = user.name;
        _userTitleView.image = user.picture;
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        [_followBtn setTitle:LOCALIZE(@"+Follow") forState:UIControlStateNormal];
        [_followBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
        
        [_followBtn removeTarget:self action:@selector(invitedFriend:) forControlEvents:UIControlEventTouchUpInside];
        [_followBtn addTarget:self action:@selector(didClickFollow:) forControlEvents:UIControlEventTouchUpInside];

    }else if(_type == ContactStatusInviting){
        
        _userTitleView.title = user.name;
        _userTitleView.subTitle = user.openID;
        _userTitleView.image = @"placehold_s";

        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        [_followBtn setTitle:LOCALIZE(@"invite") forState:UIControlStateNormal];
        [_followBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
       
        [_followBtn removeTarget:self action:@selector(didClickFollow:) forControlEvents:UIControlEventTouchUpInside];
        [_followBtn addTarget:self action:@selector(invitedFriend:) forControlEvents:UIControlEventTouchUpInside];
    }else{
    
        DLog(@"!!!!!!!!!!!!!");
    }
}

-(void)didClickFollow:(id)sender
{
    if(_type == ContactStatusInviting)
        return;
    
    if(_delegate!=nil)
        [_delegate didFollowUser:_user];
    
    [_followBtn playBounceAnimation];
    
    if(_user.isFollowing==1){
        _user.isFollowing = 0;
        _type = ContactStatusUsing;
        [self reloadCell:_user];
        [API_MANAGER unfollowUserAction:_user.userID withCompletion:^(BOOL success) {
        }];
    }else{
        [EVENT_HANDLER addEventTracking:@"FollowContactFrined" withDict:nil];
        _user.isFollowing = 1;
        _type = ContactStatusFollowing;
        [self reloadCell:_user];
        
        if([GET_DEFAULT(FOLLOWING_COUNT) intValue] >= MAX_FOLLOWER_COUNT){
            [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"cannot_follow_user") message:[NSString stringWithFormat:LOCALIZE(@"max_followcount"),MAX_FOLLOWER_COUNT] buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                if(okClicked){
                    
                }
            }];
        }else{
            [API_MANAGER followUserAction:_user.userID withCompletion:^(BOOL success) {
            }];
        }
    }
    [SINGLETON reloadFollowStatus];
}

-(void) invitedFriend:(id)sender
{
    [_followBtn playBounceAnimation];

    NSString* shareText = [NSString stringWithFormat:LOCALIZE(@"SHARE_BODY"), GET_DEFAULT(USER_OPEN_ID), APP_SHARE_URL];

    // LOCALIZE(@"Invite your friend")
    [DIALOG_MANAGER showActionSheetDialogTitle:@"" options:@[LOCALIZE(@"SMS"), LOCALIZE(@"Email"), LOCALIZE(@"Line"), LOCALIZE(@"WhatsApp")] destructiveIndexes:@[@"cancel"] cancelable:YES withCompletion:^(int selectedOption) {
        
        DLog(@"%d",selectedOption);
        if(selectedOption==0){
            MFMessageComposeViewController*messageComposeViewController = [[MFMessageComposeViewController alloc] init];
            messageComposeViewController.navigationBar.tintColor = DARK_GRAY_COLOR;
            if([MFMessageComposeViewController canSendText]) {
                messageComposeViewController.body = shareText;
                messageComposeViewController.messageComposeDelegate = self;
                [self.navCtrl presentViewController:messageComposeViewController animated:YES completion:nil];
            } else {
                [DIALOG_MANAGER showActionSheetOKDialogTitle:@"" message:LOCALIZE(@"No SMS Service!") buttonText:LOCALIZE(@"OK") cancelable:nil withCompletion:nil];
            }
        }else if(selectedOption==1){
            NSString* subject = LOCALIZE(@"Share");
            NSString* emailBody = shareText;
            
            BOOL gmailInstalled = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"googlegmail:///co?to=&subject=%@&body=%@", [subject urlEncode], [emailBody urlEncode]]]];
            
            if(gmailInstalled) {
                return;
            }
            
            if ([MFMailComposeViewController canSendMail]) {
                // Email
                MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
                controller.navigationBar.tintColor = DARK_GRAY_COLOR;
                controller.mailComposeDelegate = self;
                
                [controller setSubject:subject];
                
                [controller setMessageBody:emailBody isHTML:NO];
                
                [controller becomeFirstResponder];
                [self.navCtrl presentViewController:controller animated:YES completion:nil];
            } else {
                [DIALOG_MANAGER showActionSheetOKDialogTitle:@"" message:LOCALIZE(@"no available Email") buttonText:LOCALIZE(@"OK") cancelable:nil withCompletion:nil];
            }
            
        }else if(selectedOption==2){
            // line
            if([self isLineInstalled]==YES){
                [self shareToLine: shareText];
            } else {
                [DIALOG_MANAGER showActionSheetOKDialogTitle:@"" message:LOCALIZE(@"please install Line!") buttonText:LOCALIZE(@"OK") cancelable:nil withCompletion:nil];
            }
        }else if(selectedOption==3){
            if(![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"whatsapp://"]]) {
                [DIALOG_MANAGER showActionSheetOKDialogTitle:@"" message:LOCALIZE(@"please install Whatsapp!") buttonText:LOCALIZE(@"OK") cancelable:nil withCompletion:nil];
                return;
            }
            
            NSURL *whatsappURL = [NSURL URLWithString:[NSString stringWithFormat:@"whatsapp://send?text=%@", [shareText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
            if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
                [[UIApplication sharedApplication] openURL: whatsappURL];
            }
        }
        
    }];
    

}

#pragma mark - Share To Line
- (BOOL)isLineInstalled {
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"line://"]];
}

- (BOOL)shareToLine:(NSString *)text {
    return [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"line://msg/text/%@", [text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self.navCtrl dismissViewControllerAnimated:YES completion:nil];
}

# pragma mark - MFMessageComposeViewControllerDelegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self.navCtrl dismissViewControllerAnimated:YES completion:nil];
}

@end
