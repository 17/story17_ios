//
//  NotifCell.h
//  Story17
//
//  Created by POPO on 5/8/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserTitleView.h"
#import "UserObject.h"

@class SearchUserCell;

@protocol SearchUserDelegate <NSObject>

@optional
-(void)didFollowUser:(UserObject*)user;
-(void)shoudResignSearchbar;
-(void)shoudBecomeFirstResponse;
-(void)didClickUnblock:(SearchUserCell*)cell;

@end

@interface SearchUserCell : UITableViewCell

@property (nonatomic,weak) UserObject* user;
@property (nonatomic,strong) UserTitleView* userTitleView;
@property (nonatomic,strong) UIButton* followBtn;
@property (nonatomic,strong) UIView* followBgView;
@property (nonatomic,strong) UIView* bottomLine;
@property (nonatomic,strong) UIImageView* heartImageView;
@property (nonatomic,weak) id<SearchUserDelegate> delegate;
@property UserListViewMode mode;

-(void)reloadCell:(UserObject*)user;
-(void)reloadFollowStatus:(UserObject*)user;
@property BOOL cellForFbMode;
@end
