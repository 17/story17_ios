//
//  BannerCell.h
//  story17
//
//  Created by POPO Chen on 7/23/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
@protocol BannerDelegate <NSObject>
-(void)didClickBanner:(int)contentIndex;

@end

@interface BannerCell : UICollectionViewCell <UIScrollViewDelegate>



@property (nonatomic,strong) UIImageView* bannerImageView;
@property (nonatomic,strong) UIScrollView* bannerScrollView;
@property (nonatomic,assign) int nowPage;
@property (nonatomic,strong) NSTimer* timer;
@property (nonatomic, weak) id<BannerDelegate> delegate;

-(int)currentPage;
@end
