//
//  ContactCell.h
//  Story17
//
//  Created by POPO on 5/15/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserTitleView.h"
#import "UserObject.h"

typedef enum
{
    ContactStatusFollowing = 0,
    ContactStatusUsing = 1,
    ContactStatusInviting
} ContactFriendStatus;

@protocol ContactCellDelegate <NSObject>
-(void)didFollowUser:(UserObject*)user;
@end

@interface ContactCell : UITableViewCell <MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>

@property (nonatomic,weak) UserObject* user;
@property (nonatomic,strong) UserTitleView* userTitleView;
@property (nonatomic,strong) UIButton* followBtn;
@property (nonatomic,strong) UIImageView* selfImageView;
@property (nonatomic,strong) UIView* bottomLine;

@property (nonatomic) ContactFriendStatus type;
@property (nonatomic,weak) id<ContactCellDelegate> delegate;

-(void)reloadCell:(UserObject*)user;
@property(nonatomic, weak)UINavigationController* navCtrl;

// reload with image & name & status


@end
