//
//  FriendSuggestionCell.h
//  story17
//
//  Created by POPO Chen on 5/7/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GridWithTypeView;
@class UserTitleView;
@class UserObject;
@class PostObject;
@class SuggestionObject;

@protocol SuggestionCellDelegate <NSObject>
- (void)didClickFollow:(NSString*)userID;

@end

@interface FriendSuggestionCell : UICollectionViewCell
@property (strong, nonatomic) UserTitleView *userTitleView;
@property (strong, nonatomic) GridWithTypeView *postImageView1;
@property (strong, nonatomic) GridWithTypeView *postImageView2;
@property (strong, nonatomic) GridWithTypeView *postImageView3;

@property (strong, nonatomic) UserObject *user;
@property (strong, nonatomic) PostObject *post1;
@property (strong, nonatomic) PostObject *post2;
@property (strong, nonatomic) PostObject *post3;

@property (strong, nonatomic) UIButton* followBtn;
@property (weak, nonatomic) id<SuggestionCellDelegate> delegate;
@property (weak, nonatomic) UINavigationController *navCtrl;

- (void)reloadCell:(SuggestionObject *)suggestionObj;

@end
