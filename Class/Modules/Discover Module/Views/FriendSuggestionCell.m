//
//  FriendSuggestionCell.m
//  story17
//
//  Created by POPO Chen on 5/7/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "FriendSuggestionCell.h"
#import "UserProfileViewController.h"
#import "LoginHandler.h"
#import "GridWithTypeView.h"

@implementation FriendSuggestionCell

+ (float)getCellHeight
{
    return [UserTitleView getDefaultHeight] + SCREEN_WIDTH / 3;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self setBackgroundColor:WHITE_COLOR];
        
        _userTitleView = [UserTitleView new];
        [_userTitleView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, [UserTitleView getDefaultHeight])];
        [self.contentView addSubview:_userTitleView];

        _postImageView1 = [[GridWithTypeView alloc] initWithFrame:CGRectMake(GridHMargin, _userTitleView.frame.size.height, GridImageWidth, GridImageWidth)];
        _postImageView2 = [[GridWithTypeView alloc] initWithFrame:CGRectMake(GridImageWidth+2*GridHMargin, _userTitleView.frame.size.height,GridImageWidth, GridImageWidth)];
        _postImageView3 = [[GridWithTypeView alloc] initWithFrame:CGRectMake(2*GridImageWidth+3*GridHMargin, _userTitleView.frame.size.height, GridImageWidth, GridImageWidth)];
        
        _followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _followBtn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_followBtn addTarget:self action:@selector(didClickFollow:) forControlEvents:UIControlEventTouchUpInside];
        
        CGRect rect;
        rect.origin.x = SCREEN_WIDTH - FOLLOW_BTN_WIDTH - 10;
        rect.origin.y = [UserTitleView getDefaultHeight] / 2 - FOLLOW_BTN_HEIGHT / 2;
        rect.size.width = FOLLOW_BTN_WIDTH;
        rect.size.height = FOLLOW_BTN_HEIGHT;
        [_followBtn setFrame:rect];
        [self.contentView addSubview:_postImageView1];
        [self.contentView addSubview:_postImageView2];
        [self.contentView addSubview:_postImageView3];
        [self.contentView addSubview:_followBtn];
        
    }
    return self;
}

- (void)reloadCell:(SuggestionObject*)suggestionObj
{
    _user = suggestionObj.user;
    
    _userTitleView.title = _user.openID;
    _userTitleView.subTitle = _user.name;
    _userTitleView.image = _user.picture;

//    DLog(@"UserID:%@",_user.openID);
    
    [ _userTitleView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [self gotoUserProfile];
    }]];
    
    [_postImageView1 reloadCellWithType:suggestionObj.post1];
    [_postImageView2 reloadCellWithType:suggestionObj.post2];
    [_postImageView3 reloadCellWithType:suggestionObj.post3];

    [ _postImageView1 addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [self gotoUserProfile];
    }]];

    [ _postImageView2 addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [self gotoUserProfile];
    }]];

    [ _postImageView3 addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [self gotoUserProfile];
    }]];

    
    
    
    if (_user.isFollowing) {
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        [_followBtn setTitle:LOCALIZE(@"Following") forState:UIControlStateNormal];
        [_followBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    } else {
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        [_followBtn setTitle:LOCALIZE(@"+Follow") forState:UIControlStateNormal];
        [_followBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
        
    }
    
}

- (void)gotoUserProfile
{
    if (!self.navCtrl) {
        return;
    }
    
    if ([_user.openID isEqualToString:GET_DEFAULT(USER_OPEN_ID)]) {
        [_userTitleView shake:5 withDelta:4 speed:0.05 shakeDirection:ShakeDirectionHorizontal];
        return;
    }
    
    [EVENT_HANDLER addEventTracking:@"ClickDiscoverUser" withDict:@{@"targetUserID": _user.userID}];
    
    UserProfileViewController *userCtrl = [[UserProfileViewController alloc] init];
    userCtrl.user = _user;
    [userCtrl getUserWithUserID:userCtrl.user.userID];
    userCtrl.hidesBottomBarWhenPushed = YES;
    [_navCtrl pushViewController:userCtrl animated:YES];

}

- (void)reloadButton
{
    if (_user.isFollowing) {
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        [_followBtn setTitle:LOCALIZE(@"Following") forState:UIControlStateNormal];
        [_followBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    } else {
        if (_user.followRequestTime != 0) {
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"]  resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"]  resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
            [_followBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
            [_followBtn setTitle:LOCALIZE(@"user_profile_request") forState:UIControlStateNormal];
        } else {
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
            [_followBtn setTitle:LOCALIZE(@"+Follow") forState:UIControlStateNormal];
            [_followBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
        }
    }
}

- (void)didClickFollow:(id)sender
{
    if ([LoginHandler isGuest]) {
        [LoginHandler handleUserLogin];
        return;
    }
    
    [_followBtn playBounceAnimation];

    if (_user.isFollowing == 1) {
        _user.isFollowing = 0;
        
        [API_MANAGER unfollowUserAction:_user.userID withCompletion:^(BOOL success) {
            
        }];
        [SINGLETON reloadFollowStatus];
    } else {
        if ([GET_DEFAULT(FOLLOWING_COUNT) intValue] >= MAX_FOLLOWER_COUNT) {
            [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"cannot_follow_user") message:[NSString stringWithFormat:LOCALIZE(@"max_followcount"),MAX_FOLLOWER_COUNT] buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                if (okClicked) {
                    
                }
            }];
        } else {
            
            [EVENT_HANDLER addEventTracking:@"FollowDiscoverUser" withDict:@{@"targetUserID":_user.userID}];

            if (![_user isPrivacyMode]) {
                _user.isFollowing = 1;
                [API_MANAGER followUserAction:_user.userID withCompletion:^(BOOL success) {
                    
                }];
            } else {
                _user.followRequestTime = CURRENT_TIMESTAMP;
                [self setUser:_user];
                [API_MANAGER sendFollowRequest:_user.userID withCompletion:^(BOOL success) {
                    if (success) {
                    
                    }
                }];
            }
        }
        [SINGLETON reloadFollowStatus];
    }
    [self reloadButton];
    
    if ([self.delegate respondsToSelector:@selector(didClickFollow:)]) {
        [self.delegate didClickFollow:_user.userID];
    }
}

@end
