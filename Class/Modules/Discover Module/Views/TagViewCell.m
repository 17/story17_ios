//
//  TagViewCell.m
//  Story17
//
//  Created by POPO on 5/11/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "TagViewCell.h"
#import "Constant.h"

@implementation TagViewCell

#define horizontalMargin 10
#define topPaddingMargin 10
#define arrowImageWidth 25

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self awakeFromNib];
        self.selectionStyle = UITableViewCellSelectionStyleNone;

    }
    return self;
}

- (void)awakeFromNib {
    
    _tagImageView = [[UILabel alloc]initWithFrame:CGRectMake(25, 5, 40, 40)];
    [_tagImageView setText:@"#"];
    _tagImageView.font = BOLD_FONT_WITH_SIZE(20);
    [_tagImageView setTextColor:GRAY_COLOR];
    
    _tagTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(horizontalMargin+50, topPaddingMargin, SCREEN_WIDTH/2, 30)];
    _tagTitleLabel.textAlignment = NSTextAlignmentLeft;
    _tagTitleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    
    _tagCountLabel = [[UILabel alloc]initWithFrame:CGRectMake(_tagTitleLabel.frame.origin.x+_tagTitleLabel.frame.size.width, _tagTitleLabel.frame.origin.y, SCREEN_WIDTH/2-2*horizontalMargin-arrowImageWidth, _tagTitleLabel.frame.size.height)];
    _tagCountLabel.textAlignment = NSTextAlignmentCenter;
    _tagCountLabel.font = SYSTEM_FONT_WITH_SIZE(12);

    _bottomLine = [ThemeManager separaterLine];
    _bottomLine.frame = CGRectMake(10, 54.5, SCREEN_WIDTH-20, 0.3);
    

    
    [self.contentView addSubview:_tagImageView];
    [self.contentView addSubview:_tagTitleLabel];
    [self.contentView addSubview:_tagCountLabel];
    [self.contentView addSubview:_bottomLine];
    
}


-(void)reloadCell:(TagObject*)tag;
{
    if(tag==nil)
        return;
    [_tagTitleLabel setText:[NSString stringWithFormat:@"#%@",tag.hashTag]];
    [_tagCountLabel setText:[NSString stringWithFormat:@"%d",tag.postCount]];
    
}


@end
