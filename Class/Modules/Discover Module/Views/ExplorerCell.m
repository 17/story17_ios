//
//  ExplorerCell.m
//  story17
//
//  Created by POPO Chen on 7/23/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "ExplorerCell.h"
#import "Constant.h"
#import "UIImageView+AFNetworking.h"

@implementation ExplorerCell

#define imageWidth 38
#define cellHeight 55

+(float)getCellHeight{
    return cellHeight;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if(self)
    {
        _type = USER_MODE;
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [_titleLabel setTextColor:DARK_GRAY_COLOR];
        _titleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        
        _subtitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
        [_subtitleLabel setTextColor:GRAY_COLOR];
        _subtitleLabel.font = SYSTEM_FONT_WITH_SIZE(12);

        _coverPhoto1 = [self getCircleImageView];
        _coverPhoto2 = [self getCircleImageView];
        _coverPhoto3 = [self getCircleImageView];

        _coverPhoto1.frame = CGRectMake(SCREEN_WIDTH-3.0*imageWidth, (cellHeight-imageWidth)/2, imageWidth, imageWidth);
        _coverPhoto2.frame = CGRectMake(SCREEN_WIDTH-2.25*imageWidth, (cellHeight-imageWidth)/2, imageWidth, imageWidth);
        _coverPhoto3.frame = CGRectMake(SCREEN_WIDTH-1.5*imageWidth,  (cellHeight-imageWidth)/2, imageWidth, imageWidth);

        _arrowImageView = [[UIImageView alloc]initWithImage:IMAGE_FROM_BUNDLE(@"nav_arrow_gray_next")];
        _arrowImageView.frame = CGRectMake(0, 0, 12, 8);
        _arrowImageView.center = CGPointMake(SCREEN_WIDTH-10, cellHeight/2);
        
        UIView* linveView = [ThemeManager separaterLine];
        linveView.frame = CGRectMake(0, cellHeight-0.5, SCREEN_WIDTH, 0.5);
        
        _topLineView = [ThemeManager separaterLine];
        _topLineView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 0.5);
        _topLineView.hidden = YES;

        [_coverPhoto1 setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
        [_coverPhoto2 setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
        [_coverPhoto3 setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
        
        [self.contentView addSubview:_topLineView];

        [self.contentView addSubview:_titleLabel];
        [self.contentView addSubview:_subtitleLabel];

        [self.contentView addSubview:_coverPhoto3];
        [self.contentView addSubview:_coverPhoto2];
        [self.contentView addSubview:_coverPhoto1];
        [self.contentView addSubview:_arrowImageView];

        [self.contentView addSubview:linveView];
    }
    return self;
}

-(void)reloadCell:(NSArray*)suggestObjs withText:(NSString*)inputText andSubTitle:(NSString*)subTitle
{
    [_titleLabel setText:inputText];
    [_titleLabel sizeToFit];
    _titleLabel.center = CGPointMake(10+_titleLabel.frame.size.width/2, 10+_titleLabel.frame.size.height/2);
    
    [_subtitleLabel setText:subTitle];
    [_subtitleLabel sizeToFit];
    _subtitleLabel.center = CGPointMake(10+_subtitleLabel.frame.size.width/2, _titleLabel.frame.origin.y+_titleLabel.frame.size.height+_subtitleLabel.frame.size.height/2);
    
    [_coverPhoto1 setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    [_coverPhoto2 setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    [_coverPhoto3 setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    
    NSArray* coverphoto = @[_coverPhoto1,_coverPhoto2,_coverPhoto3];
    _topLineView.hidden = YES;

    switch (_type) {
        case USER_MODE:
            _topLineView.hidden = NO;
            
            for(int i=0;i<[suggestObjs count];i++){
                if(i>2){
                    return;
                }
                
                SuggestionObject* sug = suggestObjs[i];
                UIImageView* coverImage = coverphoto[i];
                [coverImage setImageWithURL:S3_THUMB_IMAGE_URL(sug.user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];

//                [coverImage my_serImageWithURL:S3_THUMB_IMAGE_URL(sug.user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s") withAnimation:UIViewAnimationOptionTransitionFlipFromTop];
                
            }
            break;
        case LIVE_MODE:
            for(int i=0;i<[suggestObjs count];i++){
                if(i>2){
                    return;
                }
                LiveStreamObject* live = suggestObjs[i];
                UIImageView* coverImage = coverphoto[i];
                [coverImage setImageWithURL:S3_THUMB_IMAGE_URL(live.user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];

//                [coverImage my_serImageWithURL:S3_THUMB_IMAGE_URL(live.user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s") withAnimation:UIViewAnimationOptionTransitionFlipFromTop];
//                [coverImage my_setImageWithURL:(live.user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s") isThumbnail:YES];
            }
        case LIVE_SHOW_MODE:
            for(int i=0;i<[suggestObjs count];i++){
                if(i>2){
                    return;
                }
                LiveStreamObject* live = suggestObjs[i];
                UIImageView* coverImage = coverphoto[i];
                [coverImage setImageWithURL:S3_THUMB_IMAGE_URL(live.user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
//                [coverImage my_serImageWithURL:S3_THUMB_IMAGE_URL(live.user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s") withAnimation:UIViewAnimationOptionTransitionFlipFromTop];
            }
            break;
            
        default:
            break;
    }
}

-(UIImageView*) getCircleImageView
{
    UIImageView* imageView =  [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imageWidth, imageWidth)];
    imageView.layer.cornerRadius = imageWidth/2;
    imageView.layer.masksToBounds = YES;
    imageView.layer.borderWidth = 2;
    imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    [imageView setBackgroundColor:WHITE_COLOR];
    return imageView;
}


@end
