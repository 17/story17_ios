//
//  TagViewCell.h
//  Story17
//
//  Created by POPO on 5/11/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagObject.h"

@interface TagViewCell : UITableViewCell

@property (nonatomic,strong) UILabel* tagImageView;

@property (nonatomic,strong) UILabel* tagTitleLabel;
@property (nonatomic,strong) UILabel* tagCountLabel;
@property (nonatomic,strong) UIImageView* arrowImageView ;
@property (nonatomic,strong) UIView* bottomLine;

-(void)reloadCell:(TagObject*)tag;


@end
