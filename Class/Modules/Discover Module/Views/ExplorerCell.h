//
//  ExplorerCell.h
//  story17
//
//  Created by POPO Chen on 7/23/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum {
    USER_MODE,
    LIVE_MODE,
    LIVE_SHOW_MODE
} EXPLORER_MODE;


@interface ExplorerCell : UICollectionViewCell
@property (nonatomic,strong) UILabel* titleLabel;
@property (nonatomic,strong) UILabel* subtitleLabel;
@property (nonatomic,strong) UIView* topLineView;

@property (nonatomic,strong) UIImageView* coverPhoto1;
@property (nonatomic,strong) UIImageView* coverPhoto2;
@property (nonatomic,strong) UIImageView* coverPhoto3;
@property (nonatomic,strong) UIImageView* arrowImageView;

@property EXPLORER_MODE type;

-(void)reloadCell:(NSArray*)suggestObjs withText:(NSString*)inputText andSubTitle:(NSString*)subTitle;
+(float)getCellHeight;

@end
