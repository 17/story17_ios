//
//  NotifCell.m
//  Story17
//
//  Created by POPO on 5/8/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "SearchUserCell.h"
#import "Constant.h"

@implementation SearchUserCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _userTitleView = [UserTitleView new];
        [_userTitleView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, [UserTitleView getDefaultHeight])];
        
        _followBtn = [UIButton new];
        [_followBtn setFrame:CGRectMake(10, [UserTitleView getDefaultHeight]/2-FOLLOW_BTN_HEIGHT/2, FOLLOW_BTN_WIDTH, FOLLOW_BTN_HEIGHT)];
        _followBtn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        
        _followBgView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - FOLLOW_BTN_WIDTH-20,0, FOLLOW_BTN_WIDTH+20, [UserTitleView getDefaultHeight])];
        [_followBgView addSubview:_followBtn];
        _followBgView.userInteractionEnabled = YES;
        [_followBgView addSubview:_followBtn];
        [_userTitleView addSubview:_followBgView];
        
        [_followBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            [self didClickFollow:sender];
        }]];
        
        _bottomLine = [ThemeManager separaterLine];
        _bottomLine.frame = CGRectMake(10, [UserTitleView getDefaultHeight]-0.5, SCREEN_WIDTH-20, 0.3);
        
        _heartImageView = [ThemeManager randomHeartImage];
        _heartImageView.hidden = YES;
        
        [self.contentView addSubview:_userTitleView];
        [self.contentView addSubview:_bottomLine];
        [self.contentView addSubview:_heartImageView];

        
    }
    return self;
}

-(void)reloadCell:(UserObject*)user;
{
    _user = user;
    _userTitleView.title = _user.openID;
    _userTitleView.subTitle = _user.name;
    _userTitleView.image = _user.picture;
    _cellForFbMode=NO;
    switch (_mode) {
        case UserListViewModeBlockedUser:
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
            [_followBtn setTitle:LOCALIZE(@"unblock") forState:UIControlStateNormal];
            [_followBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
            [_followBtn removeTarget:self action:@selector(didClickFollow:) forControlEvents:UIControlEventTouchUpInside];
            [_followBtn removeTarget:self action:@selector(clickUnblock) forControlEvents:UIControlEventTouchUpInside];
            [_followBtn addTarget:self action:@selector(clickUnblock) forControlEvents:UIControlEventTouchUpInside];
            break;
            
        default:
            if([_user.userID isEqualToString:MY_USER_ID]){
                _followBtn.hidden = YES;
            }else{
                _followBtn.hidden = NO;
            }
            
            if(_user.isVerified==1){
                [_userTitleView verified:YES];
            }else{
                [_userTitleView verified:NO];
            }
            
            
            if(_user.isChoice==1){
                [_userTitleView choiced:YES];
            }else{
                [_userTitleView choiced:NO];
            }
            
            if(_user.crossGreatWall==1){
                [_userTitleView chinaFlag:YES];
            }else{
                [_userTitleView chinaFlag:NO];
            }
            
            if(_user.isInternational==1){
                [_userTitleView internationalFlag:YES];
            }else{
                [_userTitleView internationalFlag:NO];
            }
            
            [self reloadFollowStatus:_user];
            break;
    }
}

-(void)reloadFollowStatus:(UserObject*)user
{
    
    _user = user;
    if(_user.isFollowing==1){
        
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        [_followBtn setTitle:LOCALIZE(@"Following") forState:UIControlStateNormal];
        [_followBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
        
        [_followBtn removeTarget:self action:@selector(didClickFollow:) forControlEvents:UIControlEventTouchUpInside];
        [_followBtn removeTarget:self action:@selector(clickUnblock) forControlEvents:UIControlEventTouchUpInside];
        [_followBtn addTarget:self action:@selector(didClickFollow:) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        if(_user.followRequestTime!=0){
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
            [_followBtn setTitle:LOCALIZE(@"Sended_request") forState:UIControlStateNormal];
            [_followBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];

        }else{
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
            [_followBtn setTitle:LOCALIZE(@"+Follow") forState:UIControlStateNormal];
            [_followBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
            
        }
        [_followBtn removeTarget:self action:@selector(didClickFollow:) forControlEvents:UIControlEventTouchUpInside];
        [_followBtn removeTarget:self action:@selector(clickUnblock) forControlEvents:UIControlEventTouchUpInside];
        [_followBtn addTarget:self action:@selector(didClickFollow:) forControlEvents:UIControlEventTouchUpInside];
        
    }
}

-(void)clickUnblock
{
    [DIALOG_MANAGER showLoadingView];
    [API_MANAGER unblockUserAction:_user.userID withCompletion:^(BOOL success) {
        
        [DIALOG_MANAGER hideLoadingView];
        if (success) {
            [_delegate didClickUnblock:self];
        } else {
            DLog(@"ERR: unblockUserAction");
        }
    }];
}

-(void)didClickFollow:(id)sender
{
    if(_delegate!=nil)
       [_delegate didFollowUser:_user];
    
    [_followBtn playBounceAnimation];
    
    if(_user.isFollowing==1){
        _user.isFollowing=0;
        _user.followRequestTime=0;
        [self reloadFollowStatus:_user];
        [API_MANAGER unfollowUserAction:_user.userID withCompletion:^(BOOL success) {
        }];
    }else{
        if([_user isPrivacyMode]){
            if(_user.followRequestTime!=0){
                _user.followRequestTime=0;
                [self reloadFollowStatus:_user];
                [API_MANAGER cancelFollowRequests:_user.userID withCompletion:^(BOOL success) {}];
            }else{
                
                if([GET_DEFAULT(FOLLOWING_COUNT) intValue] >= MAX_FOLLOWER_COUNT){
                    
                    if(_delegate!=nil)
                        [_delegate shoudResignSearchbar];
                    
                    [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"cannot_follow_user") message:[NSString stringWithFormat:LOCALIZE(@"max_followcount"),MAX_FOLLOWER_COUNT] buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                        if(okClicked){
                            if(_delegate!=nil)
                                [_delegate shoudBecomeFirstResponse];
                        }
                    }];
                }else{
                    
                    if([GET_DEFAULT(FOLLOWING_COUNT) intValue] >= MAX_FOLLOWER_COUNT){
                        [self cannotFollowUserAlert];
                    }else{
                        _user.followRequestTime=CURRENT_TIMESTAMP;
                        [self reloadFollowStatus:_user];
                        [API_MANAGER sendFollowRequest:_user.userID withCompletion:^(BOOL success) {}];
                    }
                }
            }
        }else{
            if([GET_DEFAULT(FOLLOWING_COUNT) intValue] >= MAX_FOLLOWER_COUNT){
                
                [self cannotFollowUserAlert];
            }else{
                _user.isFollowing=1;
                _user.followRequestTime=0;
                [self reloadFollowStatus:_user];
                [API_MANAGER followUserAction:_user.userID withCompletion:^(BOOL success) {
                    if (success) {
                        if (_cellForFbMode) {
                            [EVENT_HANDLER addEventTracking:@"FollowFBFriend" withDict:nil];
                        }
                    }
                }];
            }
        }
    }
    [SINGLETON reloadFollowStatus];
}

-(void)cannotFollowUserAlert
{
    if(_delegate!=nil)
        [_delegate shoudResignSearchbar];
    
    [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"cannot_follow_user") message:[NSString stringWithFormat:LOCALIZE(@"max_followcount"),MAX_FOLLOWER_COUNT] buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
        if(okClicked){
            [_delegate shoudBecomeFirstResponse];
        }
    }];
}

@end
