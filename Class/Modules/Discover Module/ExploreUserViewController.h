//
//  ExploreUserViewController.h
//  story17
//
//  Created by POPO Chen on 7/23/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreUserViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *mainCollectionView;
@property (strong, nonatomic) NSMutableArray *suggestionArray;
@property (strong, nonatomic) UIActivityIndicatorView *progressView;

@end
