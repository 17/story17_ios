//
//  NationTableViewController.h
//  Dare
//
//  Created by POPO on 11/20/14.
//  Copyright (c) 2014 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LiveNationTableDelegate
-(void) didSelectNations:(NSString*)nation;
@end

@interface LiveNationViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>


@property (weak) id<LiveNationTableDelegate> delegate;
//@property (nonatomic,strong) UISearchDisplayController* searchDisplay;

@property (nonatomic,strong) UITableView* tableView;
@property (nonatomic,strong) NSArray* countryArray;
@property (nonatomic,strong) UIActivityIndicatorView* progressView;

@property int mode;
@property (nonatomic,strong) NSMutableArray* searchArray;
@property (nonatomic,strong) NSString* selectedCountry;


@end
