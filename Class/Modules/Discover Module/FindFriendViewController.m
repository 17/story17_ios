//
//  FindFriendViewController.m
//  Story17
//
//  Created by POPO on 5/13/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "FindFriendViewController.h"
#import "UserProfileViewController.h"
#import <AddressBook/AddressBook.h>
#import "Constant.h"
#import "ContactsData.h"
#import "SearchUserCell.h"
#import "ContactCell.h"
#import "StatusBarColorApplyingActivityViewController.h"
#import "FacebookManager.h"
#import "InstagramLoginWebViewController.h"


@interface FindFriendViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UIScrollViewDelegate, SearchUserDelegate, FacebookManagerDelegate>
@property (strong, nonatomic) UISearchBar *searchBar;

@property (strong, nonatomic) UIView *btnContainer;
@property (strong, nonatomic) UIButton *searchBtn;
@property (strong, nonatomic) UIButton *contactBtn;
@property (strong, nonatomic) UIButton *fbBtn;
@property (strong, nonatomic) UITableView *mainTableView;
@property (strong, nonatomic) UIImageView *noDataImageView;

@property (strong, nonatomic) NSMutableArray *suggestionUser;
@property (strong, nonatomic) NSMutableArray *searchUsers;
@property (strong, nonatomic) NSMutableArray *contactUsers;
@property (strong, nonatomic) NSMutableArray *fbUsers;
@property (assign, nonatomic)FindFriendTabMode type;

/* Contact book related */
@property (strong, nonatomic) NSMutableArray *fbFriendIDArray;
@property (strong, nonatomic) NSMutableArray *fbUploadData;
@property (strong, nonatomic) NSMutableArray *uploadedKey;

/* Contact book related */
@property (strong, nonatomic) NSMutableArray *contactsArray;
@property (strong, nonatomic) NSMutableArray *fullNameArray;
@property (strong, nonatomic) NSMutableArray *phoneNumerArray;
@property (strong, nonatomic) NSMutableDictionary *numberMapNameDict;
@property (strong, nonatomic) NSMutableDictionary *nameMapNumberDict;
@property (strong, nonatomic) NSMutableDictionary *getImageFromFullName;
@property (strong, nonatomic) NSMutableArray *contactUploadArray;

@property (strong, nonatomic) NSMutableArray *addedFriends;
@property (strong, nonatomic) NSMutableArray *unAddedFriends;
@property (strong, nonatomic) NSMutableArray *unPlayedFriends;

@property (assign, nonatomic) BOOL isFirstIn;
@property (assign, nonatomic) BOOL isFirstInFB;
@property (assign, nonatomic) BOOL isFirstInContact;

@property (strong, nonatomic) UIActivityIndicatorView *progressView;
@property (strong, nonatomic) NSString *facebookName;
@property (assign, nonatomic) int flurryCount;
@end

@implementation FindFriendViewController

#define sectionHeight 25.0f

#ifdef INTERNATIONAL_VERSION
    #define btnWidth SCREEN_WIDTH/3
#else
    #define btnWidth SCREEN_WIDTH/2
#endif

#define searchUserCellIdentity @"searchUserCellIdentity"
#define contactCellIdentity @"contactCellIdentity"
#define inviteCellIdentity @"inviteCellIdentity"
#define middleHeight SCREEN_HEIGHT-2*NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-KEYBOARD_HEIGHT // height when keyboard showed
#define tableViewHeight SCREEN_HEIGHT-2*NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT

-(id)init
{
    self = [super init];
    if(self){
//        DLog(@" init Retain count is %ld", CFGetRetainCount((__bridge CFTypeRef)self));
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    DLog(@" viewDidLoad init Retain count is %ld", CFGetRetainCount((__bridge CFTypeRef)self));

    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];

    self.title = LOCALIZE(@"find_friend");
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:LOCALIZE(@"invite") style:UIBarButtonItemStyleDone target:self action:@selector(inviteAction)];
    
    _suggestionUser = CREATE_MUTABLE_ARRAY;
    _searchUsers = CREATE_MUTABLE_ARRAY;
    _fbUsers = CREATE_MUTABLE_ARRAY;
    _contactUsers = CREATE_MUTABLE_ARRAY;
    _contactUploadArray = CREATE_MUTABLE_ARRAY;
    
    /* Facebook Friend Related */
    _fbFriendIDArray = CREATE_MUTABLE_ARRAY;
    _fbUploadData = CREATE_MUTABLE_ARRAY;
    _uploadedKey = CREATE_MUTABLE_ARRAY;
    
    /* Contact Status Related */
    _contactsArray = CREATE_MUTABLE_ARRAY;
    _fullNameArray = CREATE_MUTABLE_ARRAY;
    _phoneNumerArray = CREATE_MUTABLE_ARRAY;
    _numberMapNameDict = CREATE_MUTABLE_DICTIONARY;
    _nameMapNumberDict = CREATE_MUTABLE_DICTIONARY;
    _getImageFromFullName = CREATE_MUTABLE_DICTIONARY;
    
    /* Contact Detail Related */
    _addedFriends = CREATE_MUTABLE_ARRAY;
    _unAddedFriends = CREATE_MUTABLE_ARRAY;
    _unPlayedFriends = CREATE_MUTABLE_ARRAY;
    
    [self setup];
//    [self twitterLoginAPI];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [EVENT_HANDLER addEventTracking:@"LeaveFindFriendPage" withDict:nil];
//    DLog(@" viewDidDisappear Retain count is %ld", CFGetRetainCount((__bridge CFTypeRef)self));

}



-(void)setup
{
    _type = FindFriendTabSearch;
    
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(5, 5, SCREEN_WIDTH, 50)];
    _searchBar.delegate = self;
    _searchBar.showsCancelButton = NO;
    
    _searchBar.placeholder = LOCALIZE(@"enter_query");
    [_searchBar setTintColor:MAIN_COLOR];
    
    _mainTableView.tableHeaderView = _searchBar;
    
    _mainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, tableViewHeight)];
    [_mainTableView registerClass:[SearchUserCell class] forCellReuseIdentifier:searchUserCellIdentity];
    [_mainTableView registerClass:[ContactCell class] forCellReuseIdentifier:contactCellIdentity];
    _mainTableView.delegate = self;
    _mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _mainTableView.dataSource = self;
    [_mainTableView setBackgroundColor:LIGHT_BG_COLOR];
    
    _btnContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NAVI_BAR_HEIGHT)];
    [_btnContainer setBackgroundColor:WHITE_COLOR];
    _searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, btnWidth, NAVI_BAR_HEIGHT)];
    _contactBtn = [[UIButton alloc]initWithFrame:CGRectMake(btnWidth, 0, btnWidth, NAVI_BAR_HEIGHT)];
    _fbBtn = [[UIButton alloc]initWithFrame:CGRectMake(2*btnWidth, 0, btnWidth, NAVI_BAR_HEIGHT)];
    
    [_btnContainer addSubview:_searchBtn];
    [_btnContainer addSubview:_contactBtn];
    [_btnContainer addSubview:_fbBtn];
 
    UIView* separaterLine = [ThemeManager separaterLine];
    separaterLine.frame = CGRectMake(0, _btnContainer.frame.size.height-0.5, SCREEN_WIDTH, 0.5);

    UIView* verticalLine = [ThemeManager separaterLine];
    verticalLine.frame = CGRectMake(btnWidth, 10, 0.5,NAVI_BAR_HEIGHT-20);
    UIView* verticalLine2 = [ThemeManager separaterLine];
    verticalLine2.frame = CGRectMake(2*btnWidth, 10, 0.5, NAVI_BAR_HEIGHT-20);

    [_btnContainer addSubview:separaterLine];
    [_btnContainer addSubview:verticalLine];
    [_btnContainer addSubview:verticalLine2];
    
    [_searchBtn setSelected:YES];
    [_searchBtn addTarget:self action:@selector(changeMode:) forControlEvents:UIControlEventTouchUpInside];
    [_contactBtn addTarget:self action:@selector(changeMode:) forControlEvents:UIControlEventTouchUpInside];
    [_fbBtn addTarget:self action:@selector(changeMode:) forControlEvents:UIControlEventTouchUpInside];
    
    [_searchBtn setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    [_searchBtn setImage:[UIImage imageNamed:@"search_active"] forState:UIControlStateSelected];
    [_contactBtn setImage:[UIImage imageNamed:@"contact"] forState:UIControlStateNormal];
    [_contactBtn setImage:[UIImage imageNamed:@"contact_active"] forState:UIControlStateSelected];
    [_fbBtn setImage:[UIImage imageNamed:@"facebook"] forState:UIControlStateNormal];
    [_fbBtn setImage:[UIImage imageNamed:@"facebook_active"] forState:UIControlStateSelected];
    
    _noDataImageView = [ThemeManager getNoDataImageView];
   
    _progressView = [UIActivityIndicatorView new];
    _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView setColor:MAIN_COLOR];
    
    [self.view addSubview:_btnContainer];
    [self.view addSubview:_mainTableView];
    [self.view addSubview:_noDataImageView];
    [self.view addSubview:_progressView];
    
    _isFirstIn = true;
    _isFirstInFB = true;
    _isFirstInContact = true;
    
    [NOTIFICATION_CENTER addObserver:self selector:@selector(reloadData:) name:FOLLOW_USER_NOTIFICATION object:nil];
    [_progressView startAnimating];

    [self reloadUI];
}

-(void)reloadData:(id)sender
{
    [_mainTableView reloadData];
}

-(void)dealloc
{
    [NOTIFICATION_CENTER removeObserver:self];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [NOTIFICATION_CENTER postNotificationName:RELOAD_SELF_INFO object:nil];
 
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  
    [EVENT_HANDLER addEventTracking:@"EnterFindFriend" withDict:nil];
    if([GET_DEFAULT(COUNTRY_CODE) isEqualToString:@""]){
        
//        [SINGLETON detectIpGeoLocation:^(BOOL success, NSDictionary *jsonDict) {
//            if(success){
//                NSString* phoneCode = [SINGLETON countryCode2PhoneCode][jsonDict[@"countryCode"]];
//                
//                // in 17app, countryCode this means phoneCode
//                [DEFAULTS setObject:phoneCode forKey:COUNTRY_CODE];
//                [DEFAULTS synchronize];
//            }
//        }];
        
//        [API_MANAGER getCountry:^(BOOL success, NSString *country) {
//            if(success){
//                NSString* countryCode = [SINGLETON countryName2countryCode][country];
//                [DEFAULTS setObject:countryCode forKey:COUNTRY_CODE];
//                [DEFAULTS synchronize];
//            }
//        }];
    }
}

-(void)refresh:(id)sender
{
    if(_type == FindFriendTabContact)
    {
//        [self showPermissionAlert];
        
    }else if(_type == FindFriendTabFacebook)
    {
        [self showFBPermissionAlert];
    
    }else if(_type == FindFriendTabSearch)
    {
        [self reloadUI];
    }
}

-(void)reloadUI
{
    if(_type==FindFriendTabSearch){
        _mainTableView.tableHeaderView = _searchBar;
    }else{
        _mainTableView.tableHeaderView = nil;
        _mainTableView.hidden = YES;
    }
    
    [_mainTableView reloadData];

    if([_suggestionUser count]!=0)
        return;
    
    [API_MANAGER getHotUsers:0 count:100 completion:^(BOOL success, NSArray *suggestedUser) {
        
        [_progressView stopAnimating];
        
        if(success){
            if([_suggestionUser count]!=0){
                [_suggestionUser removeAllObjects];
            }
            
            for(SuggestionObject* suggestion in suggestedUser){
                [_suggestionUser addObject:suggestion];
                
            }
            [_mainTableView reloadData];
        }
    }];
}

-(void)changeMode:(id)sender
{
    _mainTableView.frame = CGRectMake(_mainTableView.frame.origin.x, _mainTableView.frame.origin.y, SCREEN_WIDTH, tableViewHeight);
    if([sender isEqual:_searchBtn]){
        
        _type = FindFriendTabSearch;
        [self reloadUI];

        [_searchBtn setSelected:YES];
        [_contactBtn setSelected:NO];
        [_fbBtn setSelected:NO];
        
        if([_suggestionUser count]==0){
            _noDataImageView.hidden=NO;
        }else{
            _noDataImageView.hidden = YES;
        }
        
    }else if([sender isEqual:_contactBtn]){

        _type = FindFriendTabContact;
        [self reloadUI];

        [_searchBtn setSelected:NO];
        [_contactBtn setSelected:YES];
        [_fbBtn setSelected:NO];
        
        if([_contactUsers count]==0){

            [_progressView startAnimating];
            if(!_isFirstInContact){
                _noDataImageView.hidden=NO;
            }
            [self showPermissionAlert];
            
        }else{
            _noDataImageView.hidden = YES;
            _mainTableView.hidden = NO;
        }
        
    }else if([sender isEqual:_fbBtn]){
        
        _type = FindFriendTabFacebook;
        [self reloadUI];

        [_searchBtn setSelected:NO];
        [_contactBtn setSelected:NO];
        [_fbBtn setSelected:YES];
        
        if([_fbUsers count]==0){
            
            [_progressView startAnimating];
            
            if(!_isFirstInFB){
                _noDataImageView.hidden=NO;
            }
            [self showFBPermissionAlert];
            
        }else{
            _noDataImageView.hidden = YES;
            _mainTableView.hidden = NO;
        }
        
    }
}



#pragma mark - Contact Book Related
-(void) showPermissionAlert
{
    _isFirstInContact = false;
    
    ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
        if (!granted){
            [DIALOG_MANAGER showActionSheetOKDialogTitle:@"Cannot Add Contact" message:@"You must give the app permission to add the contact first." buttonText:@"OK" cancelable:NO withCompletion:^(BOOL okClicked) {
            }];
            return;
        }
        [self loadingContact];
        return;
    });
}

- (void)showFBPermissionAlert
{
    if (![FACEBOOK_MANAGER isLoginned]) {
        [FACEBOOK_MANAGER setDelegate:self];
        [FACEBOOK_MANAGER loginAction:self];
        
    }
    else {
        [self loginSuccessHandler];
    }
}

-(void)loginSuccessHandler
{
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         
         if (error) {
             NSLog(@"Error %@",error);
             [DIALOG_MANAGER showNetworkFailToast];
             return ;
         }
         
         NSString* facebookID = result[@"id"];
         NSString* fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
         [API_MANAGER updateUserInfo:@{@"facebookID":facebookID,@"facebookAccessToken":fbAccessToken} fetchSelfInfo:NO completion:^(BOOL success) {
         }];
         
         [self loadFBFriendWithOffset:@""];
     }];
}

-(void)loginFailHandler
{
    
}

-(void)loadFBFriendWithOffset:(NSString*)urlString
{
    
    NSString* url;
    
    if([urlString isEqualToString:@""]){
        [_progressView startAnimating];
        url = @"me/friends";
        
    }else{
        url = urlString;
        url = [url substringFromIndex:32]; //remove the 'https://graph.facebook.com/v2.3/' for using FBSDKGraphRequest
 
    }
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:url parameters:nil HTTPMethod:@"GET"]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (error) {
             [DIALOG_MANAGER showNetworkFailToast];
             return ;
         }
         
         NSArray* fbFriends = result[@"data"];
         NSString* nextPage = result[@"paging"][@"next"];
         
         for(NSDictionary* dict in fbFriends){
             [_fbUploadData addObject:dict];
             [_fbFriendIDArray addObject:dict[@"id"]];
         }
         
         if(nextPage !=nil){
             
             [self loadFBFriendWithOffset:nextPage];
             
         }else{
     
             [API_MANAGER findFriends:TO_JSON(_fbFriendIDArray) method:@"facebook" completion:^(BOOL success, NSArray *userObjects) {
                 [_fbUsers removeAllObjects];
                 [_progressView stopAnimating];
                 _mainTableView.hidden = NO;
                 if(success){
                     if([_fbUsers count]!=0){
                         [_fbUsers removeAllObjects];
                     }
     
                     if([userObjects count]==0){
                         _noDataImageView.hidden = NO;
                     }else{
                         _noDataImageView.hidden = YES;
                     }
     
                     [_fbUsers addObjectsFromArray:userObjects];
     
                     [_mainTableView reloadData];
                 }
             }];
     
     
             if([GET_DEFAULT(FACEBOOK_LAST_UPDATE) intValue]>CURRENT_TIMESTAMP-86400){
                 return;
             }
             
             _facebookName =[NSString stringWithFormat:@"%@ %@",result[@"first_name"],result[@"last_name"]];
             [API_MANAGER uploadFacebookFriends:_facebookName facebookIDs:TO_JSON(_fbUploadData) withCompletion:^(BOOL success, NSString *message) {
                 
                 if(success){
                     [DEFAULTS setObject:INT_TO_NUMBER(CURRENT_TIMESTAMP) forKey:FACEBOOK_LAST_UPDATE];
                     [DEFAULTS synchronize];
                 }
             }];

//             [API_MANAGER findFriends:TO_JSON(_fbFriendIDArray) method:@"facebook" completion:^(BOOL success, NSArray *userObjects) {
//                 [_fbUsers removeAllObjects];
//                 [_progressView stopAnimating];
//                 _mainTableView.hidden = NO;
//                 if(success){
//                     if([_fbUsers count]!=0){
//                         [_fbUsers removeAllObjects];
//                     }
//                     
//                     if([userObjects count]==0){
//                         _noDataImageView.hidden = NO;
//                     }else{
//                         _noDataImageView.hidden = YES;
//                     }
//                     
//                     [_fbUsers addObjectsFromArray:userObjects];
//                     
//                     [_mainTableView reloadData];
//                 }
//             }];
//             
//             if([GET_DEFAULT(FACEBOOK_LAST_UPDATE) intValue]>CURRENT_TIMESTAMP-86400){
//                 return;
//             }
//             
//             [API_MANAGER uploadFacebookFriends:_facebookName facebookIDs:TO_JSON(_fbUploadData) withCompletion:^(BOOL success, NSString *message) {
//                 
//                 if(success){
//                     [DEFAULTS setObject:INT_TO_NUMBER(CURRENT_TIMESTAMP) forKey:FACEBOOK_LAST_UPDATE];
//                     [DEFAULTS synchronize];
//                 }
//             }];
             
         }
     }];
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(_type == FindFriendTabSearch){
        return 1;
    }else if(_type == FindFriendTabContact){
        return 1;
    }else if(_type == FindFriendTabFacebook){
        return 1;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(_type==FindFriendTabContact){
        return sectionHeight;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(_type == FindFriendTabContact){
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, sectionHeight)];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, sectionHeight)];
        [label setTextColor:[UIColor whiteColor]];
        [label setFont: BOLD_FONT_WITH_SIZE(14) ];
        
        label.textAlignment = NSTextAlignmentCenter;
        [view setBackgroundColor:MAIN_COLOR];
        switch (section)
        {
            case 0:
                [label setText:@"17 Players"];
                break;
            case 1:
                [label setText:@"Invite"];
                break;
            default:
                [label setText:@""];
                break;
        }
        
        [view addSubview:label];
        [view setAlpha:0.8];
        
        return view;
    
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(_type == FindFriendTabSearch){
        
        if(![_searchBar.text isEqualToString:@""] && [_searchUsers count]!=0){
            return [_searchUsers count];
        }
        
        return [_suggestionUser count];
        
    }else if(_type == FindFriendTabContact){
        if(section==0){
            return [_addedFriends count]+[_unAddedFriends count];
        } else if(section==1){
            return  [_nameMapNumberDict count]-[_unPlayedFriends count];
        }
    }else if(_type == FindFriendTabFacebook){
        
        return [_fbUsers count];
    }    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_type == FindFriendTabSearch){
        [EVENT_HANDLER setNowStatus:@"FindFriendTab"];

        if(![_searchBar.text isEqualToString:@""] && [_searchUsers count]!=0){
            SearchUserCell* cell = [tableView dequeueReusableCellWithIdentifier:searchUserCellIdentity];
            if (cell == nil){
                cell = [[SearchUserCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchUserCellIdentity];
            }
            
            cell.delegate = self;
            [cell reloadCell:_searchUsers[indexPath.row]];
            return cell;
        }
        
        SearchUserCell* cell = [tableView dequeueReusableCellWithIdentifier:searchUserCellIdentity];
        if (cell == nil){
            cell = [[SearchUserCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchUserCellIdentity];
        }
        cell.delegate = self;
        SuggestionObject* suggestion = _suggestionUser[indexPath.row];
        [cell reloadCell:suggestion.user];
        return cell;
        
    }else if(_type == FindFriendTabContact){

        if(indexPath.section==0){
            ContactCell* cell = [tableView dequeueReusableCellWithIdentifier:contactCellIdentity];
            if (cell == nil){
                cell = [[ContactCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:contactCellIdentity];
            }
            
            cell.navCtrl = self.navigationController;
            
            UserObject* user;
            
            if(indexPath.row<[_addedFriends count]){
                user = [_addedFriends objectAtIndex:indexPath.row];
                cell.type = ContactStatusFollowing;
            } else{
                user = [_unAddedFriends objectAtIndex:indexPath.row-[_addedFriends count]];
                cell.type = ContactStatusUsing;
            }
            
            NSString* phone = user.phoneNumber;
            NSString* realName = [_numberMapNameDict objectForKey:phone];
            user.name = realName;
           
            [cell reloadCell:user];
            
            [_fullNameArray removeObject:realName];
            [EVENT_HANDLER setNowStatus:@"FindFriendTabContact"];

            return cell;

        } else {
            
            ContactCell* cell = [tableView dequeueReusableCellWithIdentifier:inviteCellIdentity];
            if (cell == nil){
                cell = [[ContactCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:inviteCellIdentity];
            }
            
            cell.navCtrl = self.navigationController;
            
            UserObject* user = [UserObject new];
            NSString* realName = [_fullNameArray objectAtIndex:indexPath.row -[_unPlayedFriends count] ];
            NSString* fullNumber = [_nameMapNumberDict objectForKey:realName][0];
            user.name = realName;
            user.openID = fullNumber;
            cell.type = ContactStatusInviting;

            [cell reloadCell:user];
            
            if(_getImageFromFullName[realName]==nil){
                [cell.selfImageView setImage:[UIImage imageNamed:@"placehold_profile_s"]];
            }else{
                [cell.selfImageView setImage:_getImageFromFullName[realName]];
            }
            [EVENT_HANDLER setNowStatus:@"FindFriendTabContact"];

            return cell;
        }
        
    }else if(_type == FindFriendTabFacebook){
        [EVENT_HANDLER setNowStatus:@"FindFriendTabContactFB"];

        SearchUserCell* cell = [tableView dequeueReusableCellWithIdentifier:searchUserCellIdentity];
        if (cell == nil){
            cell = [[SearchUserCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchUserCellIdentity];
        }
        cell.delegate = self;
        [cell reloadCell:_fbUsers[indexPath.row]];
        cell.cellForFbMode=YES;
        return cell;
    
    }
    
    UITableViewCell* cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchUserCellIdentity];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_type == FindFriendTabSearch){
        // go to see the user
        if(![_searchBar.text isEqualToString:@""] &&  [_searchUsers count]!=0){
            UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
            if(indexPath.row > [_searchUsers count]-1)
                return;
            userCtrl.user = _searchUsers[indexPath.row];
            [userCtrl getUserWithUserID:userCtrl.user.userID];
            
            if (_flurryCount==1) {
                [EVENT_HANDLER addEventTracking:@"SearchUserAndClick" withDict:@{@"targetUserID":userCtrl.user.userID,@"query":_searchBar.text}];
            }

            [self.navigationController pushViewController:userCtrl animated:YES];
        }else{
            UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
            if(indexPath.row > [_suggestionUser count]-1)
                return;
            SuggestionObject* suggestion = _suggestionUser[indexPath.row];
            
            userCtrl.user = suggestion.user;
            [userCtrl getUserWithUserID:userCtrl.user.userID];
            if (_flurryCount==1) {
                [EVENT_HANDLER addEventTracking:@"SearchUserAndClick" withDict:@{@"targetUserID":userCtrl.user.userID,@"query":_searchBar.text}];
            }
            [self.navigationController pushViewController:userCtrl animated:YES];
        }
        
    }else if(_type == FindFriendTabContact){
        
        if(indexPath.section==0){
            
            UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
            UserObject* user;
            
            if(indexPath.row<[_addedFriends count]){
                user = [_addedFriends objectAtIndex:indexPath.row];
            } else{
                user = [_unAddedFriends objectAtIndex:indexPath.row-[_addedFriends count]];
            }
            
            if(user==nil)
                return;
            [EVENT_HANDLER addEventTracking:@"SearchContactUserAndClick" withDict:@{@"targetUserID":user.userID}];
            
            userCtrl.user = user;
            [userCtrl getUserWithUserID:userCtrl.user.userID];
            [self.navigationController pushViewController:userCtrl animated:YES];
        }
        
    }else if(_type == FindFriendTabFacebook){
        // go to see the user
        UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
        userCtrl.user = _fbUsers[indexPath.row];
        [userCtrl getUserWithUserID:userCtrl.user.userID];
        [EVENT_HANDLER addEventTracking:@"SearchFBUserAndClick" withDict:@{@"targetUserID":userCtrl.user.userID}];

        [self.navigationController pushViewController:userCtrl animated:YES];
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [UserTitleView getDefaultHeight];
}

-(void) refreshFriend
{
    
    // API_GET FRINED BY PHONE  SECRET UPLOAD Friend Contact
    if([_contactUsers count]==0){
        [_progressView startAnimating];
    }
    
    [API_MANAGER findFriends:TO_JSON(_phoneNumerArray) method:@"contacts" completion:^(BOOL success, NSArray *userObjects) {
        [_progressView stopAnimating];
        _mainTableView.hidden = NO;
        
        if(success){
            
            [_contactUsers removeAllObjects];
            [_addedFriends removeAllObjects];
            [_unAddedFriends removeAllObjects];

            
            if([userObjects count]==0){
                _noDataImageView.hidden = NO;
            }else{
                _noDataImageView.hidden = YES;
            }
            
            for(UserObject* user in userObjects){
                    
                if(user.isFollowing==1){
//                    DLog(@"username : %@",user.name);
                    [_addedFriends addObject:user];
                } else{
                    [_unAddedFriends addObject:user];
                }
            }
            
            
            for(UserObject* user in userObjects){
                [_contactUsers addObject:user];
            }
            [_mainTableView reloadData];
        }
    }];
    
    if([GET_DEFAULT(CONTACT_LAST_UPDATE) intValue]>CURRENT_TIMESTAMP-86400){
        return;
    }

    [API_MANAGER uploadPhoneNumbers:GET_DEFAULT(PHONE_NUM) phoneNumbers:TO_JSON(_contactUploadArray) withCompletion:^(BOOL success, NSString *message) {
        if(success){
            [DEFAULTS setObject:INT_TO_NUMBER(CURRENT_TIMESTAMP) forKey:CONTACT_LAST_UPDATE];
            [DEFAULTS synchronize];
        }
    
    }];
}



#pragma mark SearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
//    if([_searchUsers count]==0){
//        _topMaskView.hidden = NO;
//    }else{
//        _topMaskView.hidden = YES;
//    }
    if(_flurryCount!=1){
        _flurryCount=1;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _mainTableView.frame = CGRectMake(_mainTableView.frame.origin.x, _mainTableView.frame.origin.y, SCREEN_WIDTH, middleHeight);
    });
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{

}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    // fetch Search query
    if(searchText.length==0){
        [_searchUsers removeAllObjects];
        [_mainTableView reloadData];
//        _topMaskView.hidden = NO;
        return;
    }
    [API_MANAGER getSearchUsers:searchText followingOnly:0 offset:0 count:100 completion:^(BOOL success, NSArray *userObjects) {
        if(success){
            
            if(![_searchBar.text isEqualToString:searchText]){
                return;
            }
            
//            if([userObjects count]!=0){
//                _topMaskView.hidden = YES;
//            }else{
//                _topMaskView.hidden = NO;
//            }
//            
            [_searchUsers removeAllObjects];
            for(UserObject* user in userObjects){
                [_searchUsers addObject:user];
            }
            [_mainTableView reloadData];
        }
    }];

}

-(void)hideSearchbar:(id)sender
{
    [_searchBar resignFirstResponder];
    _mainTableView.frame = CGRectMake(_mainTableView.frame.origin.x, _mainTableView.frame.origin.y, SCREEN_WIDTH, tableViewHeight);
//    _topMaskView.hidden = YES;
}

-(void)loadingContact
{
    DLog(@"loadingContact");
    [_progressView startAnimating];
    
    dispatch_async(GLOBAL_QUEUE, ^{
        NSMutableArray* allContacts = CREATE_MUTABLE_ARRAY;
        allContacts = [[self getAllContacts] mutableCopy];
        DLog(@"finished");

        for(ContactsData* contact in allContacts){
            
            NSString* fullName = [NSString stringWithFormat:@"%@%@",contact.firstNames,contact.lastNames ];
            fullName = [fullName stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSMutableArray* phones = CREATE_MUTABLE_ARRAY;
            if(contact.image!=nil)
                _getImageFromFullName[fullName] = contact.image;
            
            if([self validName:fullName]){
                
                phones = [[self validPhoneNumber:contact.numbers] copy];
                
                for(NSString* valied in phones){
                    
                    [_contactsArray addObject:valied];
                    NSString* countryCode = GET_DEFAULT(COUNTRY_CODE);
                    
                    _numberMapNameDict[[NSString stringWithFormat:@"%@%@",countryCode,valied]] = fullName;
                    _numberMapNameDict[valied] = fullName;
                    if(![_phoneNumerArray containsObject:valied]){
                        [_phoneNumerArray addObject:valied];
                    }
//                    DLog(@"_phoneNumerArray:%@",_phoneNumerArray);
//                    DLog(@"_phoneNumerCount:%lu",(unsigned long)[_phoneNumerArray count]);
                    

                    NSMutableDictionary* dict = CREATE_MUTABLE_DICTIONARY;
                    dict[@"name"] = fullName;
                    dict[@"phone"] = valied;
                    NSString* key1 = [NSString stringWithFormat:@"%@-%@",dict[@"name"],dict[@"phone"]];
                    
                    NSMutableDictionary* dict2 = CREATE_MUTABLE_DICTIONARY;
                    dict2[@"name"] = fullName;
                    dict2[@"phone"] = [NSString stringWithFormat:@"%@%@",countryCode,valied ];
                    NSString* key2 = [NSString stringWithFormat:@"%@-%@",dict2[@"name"],dict2[@"phone"]];

                    if(![_uploadedKey containsObject:key1]){
                        [_contactUploadArray addObject:dict];
                    }
                    if(![_uploadedKey containsObject:key2]){
                        [_contactUploadArray addObject:dict2];
                    }
                    
                }
                
                if([_nameMapNumberDict objectForKey:fullName]==nil){
                   
                    [_fullNameArray addObject:fullName];
                    
                    if([phones count]!=0 && fullName.length!=0){
                        _nameMapNumberDict[fullName] = phones;
                    }
                    
                    
                } else{
                    // to handle different person but same name
                    //[_numberMapNumberDict[fullName] addObject:contact.numbers];
                }
                
            }
        }
        
        dispatch_async(MAIN_QUEUE, ^{
            [self refreshFriend];
        });
    });
}

#pragma mark - AddressBook
-(NSArray *)getAllContacts
{
    NSMutableArray* contactList = [[NSMutableArray alloc] init];
    CFErrorRef *error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);

    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    
    for (int i=0;i < nPeople;i++) {
        ContactsData *contacts = [ContactsData new];

        ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);

        //get First Name
        CFStringRef firstName = (CFStringRef)ABRecordCopyValue(person,kABPersonFirstNameProperty);
        contacts.firstNames = [(__bridge NSString*)firstName copy];

        if (firstName != NULL) {
            CFRelease(firstName);
        }


        //get Last Name
        CFStringRef lastName = (CFStringRef)ABRecordCopyValue(person,kABPersonLastNameProperty);
        contacts.lastNames = [(__bridge NSString*)lastName copy];

        if (lastName != NULL) {
            CFRelease(lastName);
        }


        if (!contacts.firstNames) {
            contacts.firstNames = @"";
        }

        if (!contacts.lastNames) {
            contacts.lastNames = @"";
        }

        contacts.fullName = [NSString stringWithFormat:@"%@ %@", contacts.firstNames, contacts.lastNames];

        // get contacts picture, if pic doesn't exists, show standart one
        CFDataRef imgData = ABPersonCopyImageData(person);
        NSData *imageData = (__bridge NSData *)imgData;
        contacts.image = [UIImage imageWithData:imageData];

        if (imgData != NULL) {
            CFRelease(imgData);
        }


        if (!contacts.image) {
            contacts.image = [UIImage imageNamed:@"icon"];
        }


        //get Phone Numbers
        NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
        ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);

        for(CFIndex i=0; i<ABMultiValueGetCount(multiPhones); i++) {
            @autoreleasepool {
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                NSString *phoneNumber = CFBridgingRelease(phoneNumberRef);
                if (phoneNumber != nil)[phoneNumbers addObject:phoneNumber];
                //NSLog(@"All numbers %@", phoneNumbers);
            }
        }

        if (multiPhones != NULL) {
            CFRelease(multiPhones);
        }

        [contacts setNumbers:phoneNumbers];

        //get Contact email
        NSMutableArray *contactEmails = [NSMutableArray new];
        ABMultiValueRef multiEmails = ABRecordCopyValue(person, kABPersonEmailProperty);

        for (CFIndex i=0; i<ABMultiValueGetCount(multiEmails); i++) {
            @autoreleasepool {
                CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
                NSString *contactEmail = CFBridgingRelease(contactEmailRef);
                if (contactEmail != nil)[contactEmails addObject:contactEmail];
                // NSLog(@"All emails are:%@", contactEmails);
            }
        }
        
        if (multiPhones != NULL) {
            CFRelease(multiEmails);
        }
        
        [contacts setEmails:contactEmails];
        
        [contactList addObject:contacts];
        
    }
    NSLog(@"Contacts = %@",contactList);
    return [contactList copy];
}


-(NSArray*) validPhoneNumber:(NSArray*)phoneNum
{
    
    NSMutableArray* phones = CREATE_MUTABLE_ARRAY;
    for(NSString* phone in phoneNum){
        NSString* validPhone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
        validPhone = [validPhone stringByReplacingOccurrencesOfString:@"(" withString:@""];
        validPhone = [validPhone stringByReplacingOccurrencesOfString:@")" withString:@""];
        validPhone = [validPhone stringByReplacingOccurrencesOfString:@"+" withString:@""];
        validPhone = [validPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
        validPhone = [validPhone stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSString* firstIndex ;
        if ([validPhone length]>1) {
            firstIndex= [validPhone substringToIndex:1];
            if([firstIndex isEqualToString:@"0"]){
                validPhone = [validPhone substringWithRange:NSMakeRange(1, [validPhone length]-1)];
            }
            [phones addObject:validPhone];
            
        }
    }
    
    return phones;
}


-(BOOL) validName:(NSString*)fullName
{
    NSString* nospaceName  = [fullName stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"@#"] invertedSet];
    set = [set invertedSet];
    
    if ([nospaceName rangeOfCharacterFromSet:set].location != NSNotFound || nospaceName.length<=1) {
        return false;
    }
    
    return true;
}

#pragma mark SearchUserDelegate
-(void)shoudResignSearchbar
{
    [_searchBar resignFirstResponder];
    _mainTableView.frame = CGRectMake(_mainTableView.frame.origin.x, _mainTableView.frame.origin.y, SCREEN_WIDTH, tableViewHeight);
}

-(void)shoudBecomeFirstResponse
{
    if(![_searchBar.text isEqualToString:@""])
        [_searchBar becomeFirstResponder];
}

-(void)didFollowUser:(UserObject*)user
{
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_searchBar resignFirstResponder];
    _mainTableView.frame = CGRectMake(_mainTableView.frame.origin.x, _mainTableView.frame.origin.y, SCREEN_WIDTH, tableViewHeight);
}

/* Friend Invitation Part */
-(void) inviteAction
{
    NSString* shareText = [NSString stringWithFormat:LOCALIZE(@"SHARE_BODY"), GET_DEFAULT(USER_OPEN_ID), APP_SHARE_URL];
    
    StatusBarColorApplyingActivityViewController *activityVC = [[StatusBarColorApplyingActivityViewController alloc] initWithActivityItems:@[shareText] applicationActivities:nil];
    
    activityVC.excludedActivityTypes = @[UIActivityTypeAirDrop, UIActivityTypePrint, UIActivityTypeSaveToCameraRoll, UIActivityTypeAddToReadingList, UIActivityTypeAssignToContact];
    [activityVC setValue:LOCALIZE(@"SHARE_SUBJECT") forKey:@"subject"];
    
    [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed) {
        
        if(completed){
            [DIALOG_MANAGER showCompleteToast];
            if([activityType isEqualToString:UIActivityTypeCopyToPasteboard]){
                [[UIPasteboard generalPasteboard] setString:[NSString stringWithFormat:LOCALIZE(@"copy_link"),@""]];
            }
        }
    }];
    
    [self presentViewController:activityVC animated:YES completion:^{
        
        NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   BLACK_COLOR,NSForegroundColorAttributeName,
                                                   [UIFont systemFontOfSize:18.0], NSFontAttributeName, nil];
        [[UINavigationBar appearance] setBarTintColor: WHITE_COLOR];
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:BLACK_COLOR];
        [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    }];
}

#pragma mark - FacebookManagerDelegate

- (void)facebookManager:(FacebookManager *)facebookManager didLoginComplete:(BOOL)success
{
    if (success) {
        [self loginSuccessHandler];
    }
    else {
        [self loginFailHandler];
    }
}


#pragma mark - Twitter Related

-(void)twitterLoginAPI
{
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session)
        {
            TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
            NSString *twitterID = store.session.userID;
            NSString *userName = session.userName;
            NSString *twitterAccessToken = session.authToken;
            
            
            NSLog(@"Twitter auth token : %@" , twitterAccessToken);
            NSLog(@"Twiiter ID : %@ ", session.userID);
            
  
            NSDictionary *twitterDic = @{@"twitterID":twitterID,@"twitterAccessToken":twitterAccessToken};
            [self updateUserInfoAPI:twitterDic userName:userName];
        }
        else
        {
            NSLog(@"Twitter 登入失敗");
        }
    }];
    
}
-(void)updateUserInfoAPI:(NSDictionary*)twitterDic userName:(NSString*)userName
{
    [API_MANAGER updateUserInfo:twitterDic fetchSelfInfo:NO completion:^(BOOL success) {
        if (success)
        {
            NSLog(@"成功上傳 Twitter UserInfo");
            [self getTwitterFriendsAPI:twitterDic[@"twitterID"] userName:userName];
        }
    }];
    
}

-(void)getTwitterFriendsAPI:(NSString*)userID userName:(NSString*)userName
{
    
    NSError *clientError;
    
    NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc]init];
    [parameterDic setObject:userID forKey:@"user_id"];
    
    NSURLRequest *request = [[Twitter sharedInstance].APIClient URLRequestWithMethod:@"GET" URL:@"https://api.twitter.com/1.1/followers/list.json" parameters:parameterDic error:&clientError];
    
    
    [[Twitter sharedInstance].APIClient sendTwitterRequest:request completion:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        if (connectionError == nil)
        {
            if (data)
            {
                NSError *jsonError;
                NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                
                NSLog(@"解析 Twitter json :%@ userID:",jsonDic);
                
                [self updateTwitterFriendsAPI:jsonDic twitterName:userName];
                
            }
            else
            {
                NSLog(@"Twitter API 錯誤: %@", connectionError);
                [DIALOG_MANAGER showNetworkFailToast];
                return ;
            }
            
        }
    }];
    
}

-(void)updateTwitterFriendsAPI:(NSDictionary*)twitterDic twitterName:(NSString*)twitterName
{
    NSMutableArray *twitterUserArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dic in [twitterDic objectForKey:@"users"]) {
        
        NSString *screenname = [dic  objectForKey:@"screen_name"];
        NSLog(@"解析id:%@ ",screenname);
        
        NSString *twitterID = [dic objectForKey:@"id_str"];
        NSLog(@"解析id_str:%@ ",twitterID);
        
        NSDictionary *userDic = @{@"name":screenname, @"id":twitterID};
        
        [twitterUserArray addObject:userDic];
        
    }
    
    NSLog(@"twitter user array : %@ 轉檔案成JSON: %@ ", twitterUserArray ,TO_JSON(twitterUserArray) );
    
    [API_MANAGER uploadTwitterFriends:twitterName twitterIDs:TO_JSON(twitterUserArray) withCompletion:^(BOOL success, NSString *message) {
        if (success)
        {
            NSLog(@"成功上傳 Twitter Friends ");
        }
    }];
    
}


#pragma mark - Instagram Related

-(void)modalToInstagramLoginWebViewController
{
    InstagramLoginWebViewController *vc = [[InstagramLoginWebViewController alloc]init];
    [self.navigationController presentViewController:vc animated:YES completion:nil];
    
}

@end
