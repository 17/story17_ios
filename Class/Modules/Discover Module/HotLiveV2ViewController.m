//
//  NotificationViewController.m
//  story17
//
//  Created by POPO Chen on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "HotLiveV2ViewController.h"
#import "Constant.h"
#import "SVPullToRefresh.h"


@implementation HotLiveV2ViewController

#define btnWidth SCREEN_WIDTH/2
#define scrollViewHeight SCREEN_HEIGHT-2*NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT
#define fetchNum 20
#define sectionHeight 25

-(id)init
{
    self = [super init];
    if(self){
        
    }
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    
    self.title = LOCALIZE(@"explorer_live");
    
    
    _flagButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_flagButton setFrame:CGRectMake(0, 0, NAVI_BAR_HEIGHT, NAVI_BAR_HEIGHT)];
    [_flagButton setShowsTouchWhenHighlighted:YES];

    [_flagButton addTarget:self action:@selector(didClickGlobalSelection:) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_flagButton];

#ifdef INTERNATIONAL_VERSION
    
    if([GET_DEFAULT(IN_SANDBOX) isEqualToString:@"0"]){
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_flagButton];
    }
#endif

    /* Default user local info */
    _isLocal = YES;

    
    _hotLiveArray = CREATE_MUTABLE_ARRAY;
    _suggestLiveArray = CREATE_MUTABLE_ARRAY;
    _latestLiveArray = CREATE_MUTABLE_ARRAY;

    _isFirstFetching = true;

    _currentCountry = @"local"; // default local
    
    [self setup];
    [self reloadFlagButton];
    
    [self fetchData:0];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(fetchData) name:REFRESH_FEEDS object:nil];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _enterLatestTimeStamp=CURRENT_TIMESTAMP;
    _enterTrendingTimeStamp=CURRENT_TIMESTAMP;
    _visibleTrendingCount=0;
    _visibleLatestCount=0;
    [EVENT_HANDLER addEventTracking:@"EnterHotLiveTrendingPage" withDict:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];

//    [NOTIFICATION_CENTER removeObserver:self];
    /* Level 5 crash fixed */
//    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (_viewMode==HOT_LIVESTREAM_MODE) {
        [EVENT_HANDLER addEventTracking:@"LeaveHotLiveTrendingPage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterTrendingTimeStamp),@"count":INT_TO_STRING(_visibleTrendingCount)}];
    }else if (_viewMode==LATEST_LIVESTREAM_MODE){
        [EVENT_HANDLER addEventTracking:@"LeaveHotLiveLatestPage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterTrendingTimeStamp),@"count":INT_TO_STRING(_visibleLatestCount)}];

    }
//    [NOTIFICATION_CENTER removeObserver:self];

}

-(void)setup
{
    _viewMode = HOT_LIVESTREAM_MODE;
    
    _mainView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
    
    _hotBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, btnWidth, NAVI_BAR_HEIGHT)];
    _hotBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [_hotBtn setTitle:LOCALIZE(@"TrendingLive") forState:UIControlStateNormal];
    [_hotBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_hotBtn setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
    [_hotBtn addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];
    [_hotBtn setSelected:YES];
    
    _sugBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/3, 0, btnWidth, NAVI_BAR_HEIGHT)];
    _sugBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [_sugBtn setTitle:LOCALIZE(@"SuggestionLive") forState:UIControlStateNormal];
    [_sugBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_sugBtn setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
    [_sugBtn addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _latestBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2, 0, btnWidth, NAVI_BAR_HEIGHT)];
    _latestBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [_latestBtn setTitle:LOCALIZE(@"LatestLive") forState:UIControlStateNormal];
    [_latestBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_latestBtn setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
    [_latestBtn addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];

    _btnArray = @[_hotBtn,_latestBtn];
//    _btnArray = @[_hotBtn,_sugBtn,_latestBtn];
    
    _modeSelectorContainer = [ThemeManager maintabBackgroundImageView];
    _modeSelectorContainer.frame = CGRectMake(0, 0, SCREEN_WIDTH, NAVI_BAR_HEIGHT);
    _modeSelectorContainer.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
    [_modeSelectorContainer setBackgroundColor:[UIColor clearColor]];
    
    _modeIndicatorImageView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"bar")];
    _modeIndicatorImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH/2, 3);
    _modeIndicatorImageView.layer.masksToBounds = YES;
    _modeIndicatorImageView.center = CGPointMake(SCREEN_WIDTH/4, NAVI_BAR_HEIGHT-1.5f);
    [_modeSelectorContainer addSubview:_modeIndicatorImageView];
    
//    [_modeSelectorContainer addSubview:_sugBtn];
    [_modeSelectorContainer addSubview:_hotBtn];
    [_modeSelectorContainer addSubview:_latestBtn];
    
    _mainScrollView = [ThemeManager getBouncedScrollView];
    _mainScrollView.frame = CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, scrollViewHeight);
    _mainScrollView.delegate = self;
    [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH*2, _mainScrollView.frame.size.height)];
    
    
    /* CollectionView Setting */
    
    __weak HotLiveV2ViewController *weakSelf = self;
    
    UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    _hotCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, scrollViewHeight) collectionViewLayout:layout];
    [self collectionViewSettings:_hotCollectionView];
    
    
    [_hotCollectionView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData:-1];
    }];
    
    UICollectionViewFlowLayout* layout2 = [[UICollectionViewFlowLayout alloc] init];
    [layout2 setScrollDirection:UICollectionViewScrollDirectionVertical];
    _suggestionCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, scrollViewHeight) collectionViewLayout:layout2];
    [self collectionViewSettings:_suggestionCollectionView];

      [_suggestionCollectionView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData:-1];
    }];
    
    UICollectionViewFlowLayout* layout3 = [[UICollectionViewFlowLayout alloc] init];
    [layout3 setScrollDirection:UICollectionViewScrollDirectionVertical];
    _latestCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, scrollViewHeight) collectionViewLayout:layout3];
    [self collectionViewSettings:_latestCollectionView];
    
    [_latestCollectionView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData:-1];
    }];

//    _collectionViewArray = @[_hotCollectionView,_suggestionCollectionView,_latestCollectionView];
    _collectionViewArray = @[_hotCollectionView,_latestCollectionView];
    _liveArray = @[_hotLiveArray,_latestLiveArray];
//    _liveArray = @[_hotLiveArray,_suggestLiveArray,_latestLiveArray];
    
    
    /* no data view & progress view settings */
    _noDataImageView = [ThemeManager getNoDataImageView];
    _noDataImageView2 = [ThemeManager getNoDataImageView];
    _noDataImageView3 = [ThemeManager getNoDataImageView];
    
    CGRect rect = _noDataImageView.frame;
    _noDataImageView.frame = rect;
//    rect.origin.x += SCREEN_WIDTH;
    _noDataImageView2.frame = rect;
    rect.origin.x += SCREEN_WIDTH;
    _noDataImageView3.frame = rect;
    
//    _noDataArray = @[_noDataImageView,_noDataImageView2,_noDataImageView3];
    _noDataArray = @[_noDataImageView,_noDataImageView3];
    
    _progressView = [UIActivityIndicatorView new];
    _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView setColor:MAIN_COLOR];
    
    _progressView2 = [UIActivityIndicatorView new];
    _progressView2.frame = CGRectMake(SCREEN_WIDTH/2-15+SCREEN_WIDTH, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView2 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView2 setColor:MAIN_COLOR];
    
    _progressView3 = [UIActivityIndicatorView new];
    _progressView3.frame = CGRectMake(SCREEN_WIDTH/2-15+SCREEN_WIDTH, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView3 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView3 setColor:MAIN_COLOR];
    
//    _progressArray = @[_progressView,_progressView2,_progressView3];
    _progressArray = @[_progressView,_progressView3];
    
    [_mainScrollView addSubview:_hotCollectionView];
//    [_mainScrollView addSubview:_suggestionCollectionView];
    [_mainScrollView addSubview:_latestCollectionView];
    
    [_mainScrollView addSubview:_noDataImageView];
//    [_mainScrollView addSubview:_noDataImageView2];
    [_mainScrollView addSubview:_noDataImageView3];
    
    [_mainScrollView addSubview:_progressView];
//    [_mainScrollView addSubview:_progressView2];
    [_mainScrollView addSubview:_progressView3];
    
    [_mainView addSubview:_mainScrollView];
    [_mainView addSubview:_modeSelectorContainer];
    [self.view addSubview:_mainView];
    
}

-(void) fetchData
{
    [self fetchData:-1];
}

-(void)fetchData:(int)viewMode
{

    if(_isFetching) return;
    
    int modeChange = viewMode==-1?_viewMode:viewMode;
    
    /* current view */
    NSMutableArray* notifArray = _liveArray[modeChange];
    UILabel* noDataView = _noDataArray[modeChange];
    UICollectionView* collectionView = _collectionViewArray[modeChange];
    UIActivityIndicatorView* progressView = _progressArray[modeChange];
    int currentMode = modeChange;

    
    _isFetching = true;
    
    if( viewMode==-1 && !_isFirstFetching){
        [collectionView scrollsToTop];

        if([notifArray count]==0)
            noDataView.hidden = NO;
        
        [collectionView.pullToRefreshView startAnimating];
    }
    
    if(_isFirstFetching){
        [progressView startAnimating];
        _isFirstFetching = false;
    }
    
    [self callApiWithMode:currentMode withCompletion:^(BOOL success, NSArray *dict) {
        
        [progressView stopAnimating];
        [collectionView.pullToRefreshView stopAnimating];
        _isFetching = false;
        
        if([dict count]==0 && [notifArray count]==0){
            noDataView.hidden = NO;
        }else{
            noDataView.hidden = YES;
        }
        
        if(success){

            [notifArray removeAllObjects];
            
            /* filter reported user post */
            for(LiveStreamObject* live in dict){
                if(![[[DatabaseManager sharedInstance] getReportRecord:@"userID"] containsObject:live.user.userID]){
                    [notifArray addObject:live];
                }
            }
            
            // 如果跑到一半換 mode 本來這個 collectionview reload就變成另一個 to avoid wrong reoload
            if(currentMode==modeChange){
                [collectionView reloadData];
                [collectionView scrollsToTop];
            }
            
        }
        
    }];
}





#pragma mark - UICollectionView DataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(3, 0, 0, 0);
    
    if(section==0){
        return UIEdgeInsetsMake(5, 0, 5, 0);
    }else{
        return UIEdgeInsetsMake(5, 0, 5, 0);
    }
    
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
//{
//    return  CGSizeMake(SCREEN_WIDTH,0);
//
////    if(section==0){
////        return  CGSizeMake(SCREEN_WIDTH,NAVI_BAR_HEIGHT+STATUS_BAR_HEIGHT);
////    }else{
////        return  CGSizeMake(SCREEN_WIDTH,0);
////    }
//}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10.0f;

//    if(section==0){
//        return 2.0f;
//    }else{
//        return 10.0f;
//    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{

    if([collectionView isEqual:_hotCollectionView]){
        float height =[LivestreamCell getHeightFromLivestream:_hotLiveArray[indexPath.row]];
        return  CGSizeMake(SCREEN_WIDTH, height);

    }else if([collectionView isEqual:_suggestionCollectionView]){
        return  CGSizeMake(SCREEN_WIDTH, [LivestreamCell getHeightFromLivestream:_suggestLiveArray[indexPath.row]]);

//        NotificationObject* notifObj = _followNotifArray[indexPath.row];
//        return [NotifCell getCellHeigthWithNotif:notifObj];
    }else if([collectionView isEqual:_latestCollectionView]){
        return  CGSizeMake(SCREEN_WIDTH, [LivestreamCell getHeightFromLivestream:_latestLiveArray[indexPath.row]]);
        
//        if(indexPath.section==0 && [_requestArray count]!=0){
//            if([_requestArray count]!=0){
//                return [RequestCell getCellHeigt];
//            }
//        }else{
//            NotificationObject* notifObj = _selfNotifArray[indexPath.row];
//            return [NotifCell getCellHeigthWithNotif:notifObj];
//        }
        
    }
    return CGSizeMake(0, 0);
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{

    if([collectionView isEqual:_hotCollectionView]){
        return [_hotLiveArray count];
    }else if([collectionView isEqual:_suggestionCollectionView]){
        return [_suggestLiveArray count];
    }else if([collectionView isEqual:_latestCollectionView]){
        return [_latestLiveArray count];
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LiveStreamObject* liveObj;
    
    if([collectionView isEqual:_hotCollectionView]){
        liveObj = [_hotLiveArray objectAtIndex:indexPath.row];
    }else if([collectionView isEqual:_suggestionCollectionView]){
        liveObj = [_suggestLiveArray objectAtIndex:indexPath.row];
    }else if([collectionView isEqual:_latestCollectionView]){
        liveObj = [_latestLiveArray objectAtIndex:indexPath.row];
    }
    
    LivestreamCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveListCell" forIndexPath:indexPath];
    cell.liveMode = HotLive;
    if (cell == nil)
    {
        cell = [[LivestreamCell alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,100)];
    }
    
    [cell.livestreamImageView setImage:[UIImage imageNamed:@"onlive"]];
    cell.navCtrl = self.navigationController;
    cell.delegate = self;
    cell.livestream = liveObj;  // reloadUI Action
    if (_viewMode==HOT_LIVESTREAM_MODE) {
        _visibleTrendingCount++;
    }else{
        _visibleLatestCount++;
    }
    return cell;
}

#pragma mark - API Action with callback
-(void)callApiWithMode:(HOTLIVE_MODE)mode withCompletion:(void(^)(BOOL success,NSArray* dict))callback
{
    NSString* countryMode = @"";
    if([_currentCountry isEqualToString:@"local"]){
        countryMode = @"local";
    }else if([_currentCountry isEqualToString:@"global"]){
        countryMode = @"global";
    }else{
        countryMode = _currentCountry;
    }

    NSString* country = countryMode;
    if([GET_DEFAULT(IN_SANDBOX) isEqualToString:@"1"]){
        country = @"TW";
    }
    
    if(mode==HOT_LIVESTREAM_MODE){
        
//        [API_MANAGER getHotLiveStreams2:country count:50 withCompletion:^(BOOL success, NSArray *liveSteamObjects) {
//            callback(success,liveSteamObjects);
//        }];
        
//        [API_MANAGER getHotLiveStreams:^(BOOL success, NSArray *liveSteamObjects) {
//            callback(success,liveSteamObjects);
//        }];
        
        
        [API_MANAGER getSuggestedLiveStreams:country count:50 withCompletion:^(BOOL success, NSArray *liveSteamObjects) {
            callback(success,liveSteamObjects);
        }];
        
    }else if(mode==SUGGEST_LIVESTREAM_MODE){
        [API_MANAGER getSuggestedLiveStreams:country count:50 withCompletion:^(BOOL success, NSArray *liveSteamObjects) {
            callback(success,liveSteamObjects);
        }];
    }else if(mode==LATEST_LIVESTREAM_MODE){
        [API_MANAGER getLatestLiveStreams:country count:50 withCompletion:^(BOOL success, NSArray *liveSteamObjects) {
            callback(success,liveSteamObjects);
        }];
    }
}

-(void)changeModeAction:(id)sender
{
    if([sender isEqual:_sugBtn]){
        [self switchToMode:SUGGEST_LIVESTREAM_MODE animated:YES];
    }else if ([sender isEqual:_hotBtn]){
        [self switchToMode:HOT_LIVESTREAM_MODE animated:YES];
        [API_MANAGER readNotif:^(BOOL success) {}];
    }else if([sender isEqual:_latestBtn]){
        [self switchToMode:LATEST_LIVESTREAM_MODE animated:YES];
    }
}

-(void) switchToMode:(HOTLIVE_MODE)mode animated:(BOOL) animated
{
    if(_viewMode==mode) {
        return;
    }
    

    _viewMode = mode;
    NSMutableArray* notifs = _liveArray[mode];
    
    if((int)[notifs count]==0){
        _isFirstFetching = true;
        [self fetchData:_viewMode];
    }
    
    if(_viewMode==HOT_LIVESTREAM_MODE){
        _visibleLatestCount=0;
        _enterTrendingTimeStamp=CURRENT_TIMESTAMP;
        [EVENT_HANDLER addEventTracking:@"LeaveHotLiveLatestPage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterTrendingTimeStamp),@"cellCount":INT_TO_STRING(_visibleLatestCount)}];
        [EVENT_HANDLER addEventTracking:@"EnterHotLiveTrendingPage" withDict:nil];
        [_hotBtn setSelected:YES];
        [_sugBtn setSelected:NO];
        [_latestBtn setSelected:NO];
    }else if(_viewMode==SUGGEST_LIVESTREAM_MODE){
        [_hotBtn setSelected:NO];
        [_sugBtn setSelected:YES];
        [_latestBtn setSelected:NO];
    }else if(_viewMode==LATEST_LIVESTREAM_MODE){
        _enterLatestTimeStamp=CURRENT_TIMESTAMP;
        [EVENT_HANDLER addEventTracking:@"LeaveHotLiveTrendingPage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterTrendingTimeStamp),@"cellCount":INT_TO_STRING(_visibleTrendingCount)}];
        [EVENT_HANDLER addEventTracking:@"EnterHotLiveLatestPage" withDict:nil];
        
        [_hotBtn setSelected:NO];
        [_sugBtn setSelected:NO];
        [_latestBtn setSelected:YES];
    }
    [self reloadUIAnimated:animated];
}

-(void) reloadUIAnimated:(BOOL)animated
{
    for (int mode=0; mode < 2; mode++) {
        UIButton* modeButton = _btnArray[mode];
        UIScrollView *tableView = _collectionViewArray[mode];
        
        if(mode == _viewMode) {
            
            tableView.scrollsToTop = YES;
            modeButton.userInteractionEnabled = NO;
            
            [_mainScrollView scrollRectToVisible:CGRectMake(SCREEN_WIDTH*mode, 0, SCREEN_WIDTH, _mainScrollView.frame.size.height) animated:YES];
            
            if(animated) {
                [UIView animateWithDuration:0.3 animations:^{
                    _modeIndicatorImageView.center = CGPointMake((SCREEN_WIDTH/4)+(SCREEN_WIDTH/2)*mode, NAVI_BAR_HEIGHT-1);
                }];
            } else {
                _modeIndicatorImageView.center = CGPointMake((SCREEN_WIDTH/4)+(SCREEN_WIDTH/2)*mode, NAVI_BAR_HEIGHT-1);
            }
        }else {
            tableView.scrollsToTop = NO;
            modeButton.userInteractionEnabled = YES;
        }
    }
}


#pragma mark UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // to avoid refresh action go this way
    if([_collectionViewArray containsObject:scrollView]) return;
    
    if(scrollView.contentOffset.x==0) {
        [self switchToMode:0 animated:YES];
    }else if(scrollView.contentOffset.x==SCREEN_WIDTH) {
        [self switchToMode:1 animated:YES];
    }else if(scrollView.contentOffset.x==SCREEN_WIDTH*2) {
        [self switchToMode:2 animated:YES];
    }
}


/* UI Settings */
-(void)collectionViewSettings:(UICollectionView*)collectionView
{
    collectionView.delegate = self;
    collectionView.dataSource = self;
    [collectionView setAlwaysBounceVertical:YES];
    collectionView.scrollsToTop = YES;
    collectionView.alwaysBounceVertical = YES;
    [collectionView setBackgroundColor:LIGHT_BG_COLOR];
    [collectionView registerClass:[LivestreamCell class] forCellWithReuseIdentifier:@"LiveListCell"];
}


-(void)didClickGlobalSelection:(UIButton*)sender
{
    LiveNationViewController* nVC = [LiveNationViewController new];
    nVC.selectedCountry = _currentCountry ;
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:nVC];
    nVC.delegate = self;
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)didSelectNations:(NSString*)nation
{
    /* Clear if  */
    if (![nation isEqualToString:_currentCountry]) {

        [_liveArray enumerateObjectsUsingBlock:^(NSMutableArray *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            UICollectionView *collectionView = _collectionViewArray[idx];
            [obj removeAllObjects];
            [collectionView reloadData];
            [collectionView scrollsToTop];
        }];
        
    }
    
    if ([nation isEqualToString:@"GLOBAL"]) {
        _currentCountry = @"global";
        
    } else if ([nation isEqualToString:@"LOCAL"]) {
        _currentCountry = @"local";
        
    } else {
        _currentCountry = nation;
        
    }
    
#ifndef INTERNATIONAL_VERSION
    _currentCountry = @"global";
#endif
    
    [self fetchData:-1];
    [self reloadFlagButton];
}


-(void)reloadFlagButton
{
    if([_currentCountry isEqualToString:@"global"]){
        [_flagButton setTitle:@"" forState:UIControlStateNormal];
        [_flagButton setImage:IMAGE_FROM_BUNDLE(@"globa") forState:UIControlStateNormal];
        [_flagButton setImage:IMAGE_FROM_BUNDLE(@"globa_down")  forState:UIControlStateHighlighted];
    }else if([_currentCountry isEqualToString:@"local"]){
        [_flagButton setTitle:@"" forState:UIControlStateNormal];
        [_flagButton setImage:IMAGE_FROM_BUNDLE(@"local")  forState:UIControlStateNormal];
        [_flagButton setImage:IMAGE_FROM_BUNDLE(@"local_down")  forState:UIControlStateHighlighted];
    }else{
//        [_flagButton setImage:IMAGE_FROM_BUNDLE([SINGLETON getCountryImageFileName:_currentCountry]) forState:UIControlStateNormal];
//        [_flagButton setImage:IMAGE_FROM_BUNDLE([SINGLETON getCountryImageFileName:_currentCountry]) forState:UIControlStateHighlighted];
        
        [_flagButton setImage:IMAGE_FROM_BUNDLE(@"") forState:UIControlStateNormal];
        [_flagButton setImage:IMAGE_FROM_BUNDLE(@"") forState:UIControlStateHighlighted];
        [_flagButton setTitle:_currentCountry forState:UIControlStateNormal];
        
    }
}

#pragma mark - LivestreamCellDelegate
-(void)didClickLiveMore:(LivestreamCell*)cell
{
    NSMutableArray* array = [NSMutableArray arrayWithObjects:LOCALIZE(@"hideHotLiveStream_confirm"),  nil];
    
    if([GET_DEFAULT(IS_ADMIN) intValue]>=PRIMO_ADMIN){
        [array addObject:LOCALIZE(@"promot_to_country")];
        [array addObject:LOCALIZE(@"remove_promot_to_country")];
        [array addObject:LOCALIZE(@"promote_to_top")];
        [array addObject:LOCALIZE(@"cancel_promote_to_top")];
    }
    
    [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"live_stream_control") options:[array copy] destructiveIndexes:@[@0] cancelable:YES withCompletion:^(int selectedOption) {
        
        if([array[selectedOption] isEqualToString:LOCALIZE(@"hideHotLiveStream_confirm")]){
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"hideHotLiveStream_confirm") options:@[LOCALIZE(@"OK")] destructiveIndexes:@[@0] cancelable:YES withCompletion:^(int selectedOption) {
                    if(selectedOption==0){
                        [API_MANAGER hideHotLivestreamAction:cell.livestream.liveStreamID withCompletion:^(BOOL success) {
                            if(success){
                                [DIALOG_MANAGER showCompleteToast];
                            }else{
                                [DIALOG_MANAGER showNetworkFailToast];
                            }
                        }];
                    }
                }];
            });
        } else if([array[selectedOption] isEqualToString:LOCALIZE(@"promot_to_country")]){
         

            NSMutableArray* countryArray = CREATE_MUTABLE_ARRAY;

            [API_MANAGER getLiveStreamHotCountryList:^(BOOL success, NSArray *regionList) {
                if(success){
                    for(NSArray* arr in regionList){
                        [countryArray addObject:arr[0]];
                    }
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"promot_to_country") options:countryArray destructiveIndexes:@[] cancelable:YES withCompletion:^(int selectedOption) {
                            NSString* selectedCountry = countryArray[selectedOption];
                            if(selectedCountry.length!=0){
                                [API_MANAGER promoteLiveStream:cell.livestream.liveStreamID countryName:selectedCountry withCompletion:^(BOOL success, int hasRestreamed) {
                                    if(success){
                                        [DIALOG_MANAGER showCompleteToast];
                                    }else{
                                        [DIALOG_MANAGER showNetworkFailToast];
                                    }
                                }];
                            }
                        }];
                    });
                }
            }];
            
        }else if([array[selectedOption] isEqualToString:LOCALIZE(@"remove_promot_to_country")]){
            
            NSMutableArray* countryArray = CREATE_MUTABLE_ARRAY;
            
            [API_MANAGER getLiveStreamHotCountryList:^(BOOL success, NSArray *regionList) {
                if(success){
                    
                    for(NSArray* arr in regionList){
                        [countryArray addObject:arr[0]];
                    }
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"promot_to_country") options:countryArray destructiveIndexes:@[] cancelable:YES withCompletion:^(int selectedOption) {
                            NSString* selectedCountry = countryArray[selectedOption];
                            if(selectedCountry.length!=0){
                                [API_MANAGER removePromotedLiveStream:cell.livestream.liveStreamID countryName:selectedCountry withCompletion:^(BOOL success, int hasRestreamed) {
                                    if(success){
                                        [DIALOG_MANAGER showCompleteToast];
                                    }else{
                                        [DIALOG_MANAGER showNetworkFailToast];
                                    }
                                }];
                            }
                        }];
                    });
                }
            }];

        } else if([array[selectedOption] isEqualToString:LOCALIZE(@"promote_to_top")]){
        
            [API_MANAGER promoteTopLiveStream:cell.livestream.liveStreamID withCompletion:^(BOOL success) {
                if(success){
                    [DIALOG_MANAGER showCompleteToast];
                }else{
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
        } else if([array[selectedOption] isEqualToString:LOCALIZE(@"cancel_promote_to_top")]){
        
            [API_MANAGER cancelPromoteTopLiveStream:cell.livestream.liveStreamID withCompletion:^(BOOL success) {
                if(success){
                    [DIALOG_MANAGER showCompleteToast];
                }else{
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
        
        }
    }];
}

-(void)dealloc
{
    [NOTIFICATION_CENTER removeObserver:self];
}




@end
