//
//  NationTableViewController.m
//  Dare
//
//  Created by POPO on 11/20/14.
//  Copyright (c) 2014 POPO_INNOVATION. All rights reserved.
//

#import "LiveNationViewController.h"
#import "Constant.h"

@implementation LiveNationViewController
@synthesize mode;

#define nationIdentify @"nationIdentify"

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    self.title = LOCALIZE(@"Region");
    
    self.countryArray = CREATE_MUTABLE_ARRAY;
    self.searchArray = CREATE_MUTABLE_ARRAY;
    
//    UISearchBar *searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
//    searchBar.delegate = self;
//    searchBar.placeholder = LOCALIZE(@"search_nation");
//    self.searchDisplay = [[UISearchDisplayController  alloc] initWithSearchBar:searchBar contentsController:self];
//    self.searchDisplay.active = NO;
//    self.searchDisplay.delegate = self;
//    self.searchDisplay.searchResultsDelegate=self;
//    self.searchDisplay.searchResultsDataSource = self;
//    [self.searchDisplay.searchResultsTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:nationIdentify];
//    self.searchDisplay.searchResultsTableView.scrollEnabled = YES;
    
    self.mode = self.mode!=1?0:1;
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:nationIdentify];
    
    [self.view addSubview:self.tableView];
    
    _progressView = [UIActivityIndicatorView new];
    _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView setColor:MAIN_COLOR];

    [self.view addSubview:_progressView];
    
//    [self.view addSubview:searchBar];
    
//    for (UIView *subView in (SYSTEM_VERSION_LESS_THAN(@"7.0")?searchBar.subviews:[[searchBar.subviews objectAtIndex:0] subviews])) {
//        if ([subView isKindOfClass:[UITextField class]]) {
//            UITextField *searchField = (UITextField *)subView;
//            searchField.font = SYSTEM_FONT_WITH_SIZE(16.0);
//        }
//    }
    
//    NSMutableArray* tmpArray = CREATE_MUTABLE_ARRAY;
//    for(NSString* key in [[SINGLETON countryName2countryCode] allKeys]){
//        NSMutableArray* array = CREATE_MUTABLE_ARRAY;
//        [array addObject:key];
//        [array addObject:[SINGLETON countryName2countryCode][key]];
//        [tmpArray addObject:array];
//    }
//    _countryArray = [tmpArray copy];
    
    [_progressView startAnimating];
    [API_MANAGER getLiveStreamHotCountryList:^(BOOL success, NSArray *regionList) {
        [_progressView stopAnimating];
        
        if(success){
            
            if([GET_DEFAULT(IS_ADMIN) intValue] >= NORMAL_ADMIN){
                int totalLiveCount = 0;
                for(NSArray* data in regionList){
                    totalLiveCount += [data[[data count]-1] intValue];
//                    DLog(@"%@",data[[data count]-1]);
                }
                self.title = [NSString stringWithFormat:@"%@(%d)",LOCALIZE(@"Region"),totalLiveCount];
            }
            _countryArray = regionList;
            [self.tableView reloadData];
        }else{
            [self.navigationController popoverPresentationController];
        }
    }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView == self.searchDisplayController.searchResultsTableView){
        return [self.searchArray count];
    } else{
        return [_countryArray count]+2;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nationIdentify forIndexPath:indexPath];
    if (cell == nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nationIdentify];
    }
    
    for(UIView* view in [cell.contentView subviews]){
        [view removeFromSuperview];
    }
    
    NSString* countryName;
    NSString* liveCount;

    if(indexPath.row == 0){
        countryName = LOCALIZE(@"global_region");
        liveCount = @"";
        if([_selectedCountry isEqualToString:@"global"]){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }else if(indexPath.row == 1){
        countryName = LOCALIZE(@"local_region");
        liveCount = @"";
        if([_selectedCountry isEqualToString:@"local"]){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }else{
        NSArray* infoArray = _countryArray[indexPath.row-2];
        countryName = infoArray[0];
        liveCount = [infoArray[1] stringValue];

        if([_selectedCountry isEqualToString:countryName]){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    UIImage* img ;
    
    if(indexPath.row==0){
        img = IMAGE_FROM_BUNDLE(@"globa_down");
    }else if(indexPath.row==1){
        img = IMAGE_FROM_BUNDLE(@"local_down");
    }else{
        img = IMAGE_FROM_BUNDLE([SINGLETON getCountryImageFileName:countryName]);;
    }
    
//    if([countryName isEqualToString:@"TW"]&&( [GET_DEFAULT(IP_COUNTRY) isEqualToString:@"CN"] || [GET_DEFAULT(IP_COUNTRY) isEqualToString:@""]) ){
//        img = IMAGE_FROM_BUNDLE(@"taiwan_2");
//        countryName = @"Chinese_taipei";
//    }

    UILabel *mainLabel = [UILabel new];
    mainLabel.textAlignment = NSTextAlignmentCenter;
    [mainLabel setText:LOCALIZE(countryName)];
//    [mainLabel setText:countryName];

    [mainLabel sizeToFit];
    mainLabel.center = CGPointMake(60+mainLabel.frame.size.width/2, 22);
    [cell.contentView addSubview:mainLabel];
    
    UILabel *secondLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-80, 0.0, 60, 45.0)];
    secondLabel.textAlignment = NSTextAlignmentCenter;
    [secondLabel setFont:BOLD_FONT_WITH_SIZE(14)];
    [secondLabel setTextColor:GRAY_COLOR];
    [secondLabel setText:liveCount];
    
    if([GET_DEFAULT(IS_ADMIN) intValue] >= 1 ){
        [cell.contentView addSubview:secondLabel];
    }
    
    UIImageView *photo = [[UIImageView alloc] initWithFrame:CGRectMake(5.0, 2.0, 50, 40.0)];
    if(indexPath.row<=1){
        photo.frame = CGRectMake(12.0, 5.0, 35, 35.0);
    }
    
    [photo setImage:img];
    photo.contentMode = UIViewContentModeScaleAspectFit;
//    [cell.contentView addSubview:photo];

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  45.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *countryName;
    
    if (indexPath.row == 0) {
        countryName = @"GLOBAL";
        
    } else if (indexPath.row == 1) {
        countryName = @"LOCAL";
        
    } else {
        NSArray *infoArray = _countryArray[indexPath.row-2];
        countryName = infoArray[0];
        
    }
    
    [self.delegate didSelectNations:countryName];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    tableView.rowHeight = 45.0f; // or some other height
}

@end
