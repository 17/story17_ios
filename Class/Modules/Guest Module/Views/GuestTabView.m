//
//  GuestTabView.m
//  Story17
//
//  Created by Racing on 2015/12/31.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "GuestTabView.h"
#import "Constant.h"

@implementation GuestTabView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [_liveButton setTitle:LOCALIZE(@"Live") forState:UIControlStateNormal];
    [_exploreButton setTitle:LOCALIZE(@"Explore") forState:UIControlStateNormal];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
