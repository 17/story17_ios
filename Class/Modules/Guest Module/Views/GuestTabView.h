//
//  GuestTabView.h
//  Story17
//
//  Created by Racing on 2015/12/31.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuestTabView : UIView
@property (strong, nonatomic) IBOutlet UIButton *liveButton;
@property (strong, nonatomic) IBOutlet UIButton *exploreButton;
@property (strong, nonatomic) IBOutlet UIView *indicatorView;

@end
