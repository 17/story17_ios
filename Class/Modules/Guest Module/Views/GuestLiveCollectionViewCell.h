//
//  GuestLiveCollectionViewCell.h
//  Story17
//
//  Created by Racing on 2015/12/31.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuestLiveCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *coverImageView;
@property (strong, nonatomic) IBOutlet UIView *profileInfoView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *verifyImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
