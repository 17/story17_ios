//
//  GuestViewController.m
//  Story17
//
//  Created by Racing on 2015/12/31.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "GuestViewController.h"
#import "Constant.h"
#import "ExploreUserViewController.h"
#import "GuestTabView.h"
#import "GuestLiveViewController.h"
#import "GuestExploreViewController.h"
#import "LoginV4ViewController.h"
#import "RegisterV4ViewController.h"
#import "WeiboSDK.h"
#import "WXApiManager.h"

@interface GuestViewController () <UIScrollViewDelegate, UINavigationControllerDelegate, FacebookManagerDelegate,LoginHandlerDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (strong, nonatomic) IBOutlet UIButton *signUpButton;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *indicatorViewLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *footerViewHC;
@property (strong, nonatomic) GuestTabView *guestTabView;
@property (copy, nonatomic) NSArray *tabButtons;
@property (copy, nonatomic) NSArray *subViewControllers;
@property (strong, nonatomic) MultiFileUploader* multiFileUploader;
@property (strong, nonatomic) UIView *topCoverView;


- (IBAction)onLive:(UIButton *)sender;
- (IBAction)onExplore:(UIButton *)sender;
- (IBAction)onSignUp:(id)sender;
- (IBAction)onLogin:(id)sender;
@end

@implementation GuestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIButton *exploreUserButton = [ThemeManager getExploreUserButton];
    [exploreUserButton addTarget:self action:@selector(onExploreUser:) forControlEvents:UIControlEventTouchUpInside];
    
    [EVENT_HANDLER addEventTracking:@"BeginGuestMode" withDict:nil];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:exploreUserButton];
    self.navigationController.delegate = self;
    
    
    _topCoverView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _topCoverView.hidden = YES;
    
    self.guestTabView = [[[NSBundle mainBundle] loadNibNamed:@"GuestTabView" owner:self options:nil] firstObject];
    _guestTabView.frame = CGRectMake(0, 0, SCREEN_WIDTH*0.6, NAVI_BAR_HEIGHT);
    self.navigationItem.titleView = _guestTabView;
    
    [_signUpButton setTitle:LOCALIZE(@"sign_up") forState:UIControlStateNormal];
    
    [_loginButton setTitle:LOCALIZE(@"log_in") forState:UIControlStateNormal];
    _loginButton.layer.borderWidth = 1.0;
    _loginButton.layer.borderColor = [[UIColor blackColor] CGColor];
    
    [_signUpButton setExclusiveTouch:YES];
    [_loginButton setExclusiveTouch:YES];
    
    self.tabButtons = @[_guestTabView.liveButton, _guestTabView.exploreButton];
    
    self.subViewControllers = @[[[GuestLiveViewController alloc] init],
                                [[GuestExploreViewController alloc] init]
                                ];
    
    [NOTIFICATION_CENTER addObserver:self selector:@selector(notifySocialLogin:)
                                name:SocialLoginNotification
                              object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(notifyLogin:)
                                name:LoginNotification
                              object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(notifySignUp:)
                                name:SignUpNotification
                              object:nil];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    // Insert child view controllers
    CGFloat width = SCREEN_WIDTH;
    CGFloat height = SCREEN_HEIGHT - (NAVI_BAR_HEIGHT + STATUS_BAR_HEIGHT + _footerViewHC.constant);
    _contentScrollView.contentSize = CGSizeMake(_tabButtons.count * width, height);
    
    [_subViewControllers enumerateObjectsUsingBlock:^(UIViewController *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.view.frame = CGRectMake(idx*width, 0, width, height);
        [_contentScrollView addSubview:obj.view];
        
        [self addChildViewController:obj];

    }];

    [self.view addSubview:_topCoverView];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [LoginHandler sharedInstance].delegate = self;
    
    self.navigationController.title = @"";
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollToIndex:(NSUInteger)index
{
    UIViewController *vc = _subViewControllers[index];
    [_contentScrollView scrollRectToVisible:vc.view.frame animated:YES];
    
    [_tabButtons enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.selected = (idx == index) ? YES : NO;
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma marl - Facebook login

-(void)loginSuccessHandler
{
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link,gender, first_name, last_name, picture.type(large), email, birthday"}]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (error) {
             [DIALOG_MANAGER showNetworkFailToast];
             return ;
         }
         
         NSString *email = result[@"email"];
         NSString *name = result[@"name"];
         NSString *gender = result[@"gender"];
         NSString *facebookID = result[@"id"];
         NSString *fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
         
         if (!fbAccessToken || !name || !facebookID) {
             [DIALOG_MANAGER hideLoadingView];
             [DIALOG_MANAGER showSadToastWithMessage:LOCALIZE(@"fb_login_reject_prompt")];
             return ;
         }
         
         if (!email) {
             email = @"";
         }
         if (!gender) {
             gender = @"";
         }
         
         NSString *profileImageFileName = [SINGLETON generateRandomFileNameWithExtension:@"png"];
         
         [API_MANAGER checkFacebookIDAvailable:facebookID completion:^(BOOL success, NSString *message) {
             if (success) {
                 
                 DLog(@"facebook register");
                 
                 // get profile photo
                 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                 NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"myFacebookPic"];
                 
                 AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                 manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                 
                 AFHTTPRequestOperation *op = [manager GET:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", facebookID] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                     
                     [SINGLETON saveImageWithThumbnailImage:[UIImage imageWithContentsOfFile:path] andFileName:profileImageFileName];
                     self.multiFileUploader = [[MultiFileUploader alloc] initWithFileNameArray:@[profileImageFileName, [NSString stringWithFormat:@"THUMBNAIL_%@", profileImageFileName]]];
                     
                     [self.multiFileUploader startUploadWithCallback:^(NSArray *fileNameArray, int code, float progress) {
//                         if(code==MULTI_UPLOAD_SUCCESS) {
//                             [DIALOG_MANAGER hideLoadingView];
//                         } else if(code==MULTI_UPLOAD_FAIL) {
//                             [DIALOG_MANAGER showNetworkFailToast];
//                             return ;
//                         }
                     }];
                 } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     DELETE_FILE(GET_LOCAL_FILE_PATH(path));
                 }];
                 
                 op.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
                 [op start];
                 
                 [DEFAULTS setObject:[[email componentsSeparatedByString:@"@"] objectAtIndex:0] forKey:USER_OPEN_ID];
                 [DEFAULTS setObject:email forKey:EMAIL];
                 [DEFAULTS setObject:gender forKey:GENDER];
                 [DEFAULTS setObject:profileImageFileName forKey:CACHE_PICTURE];
                 [DEFAULTS setObject:facebookID forKey:FACEBOOK_ID];
                 [DEFAULTS setObject:fbAccessToken forKey:FACEBOOK_TOKEN];
                 [DEFAULTS synchronize];
                 
                 DLog(@"facebookID:%@",facebookID);
                 DLog(@"fbAccessToken:%@",fbAccessToken);
                 
                 /* RegisterInfo */
                 RegisterV4ViewController* regVC = [RegisterV4ViewController new];
                 regVC.registerMode = USER_NAME_MODE_V4;
                 regVC.registerType = FB_MODE;
                 regVC.backButton.hidden = YES;
                 regVC.imageTextField.mainTextField.text = name;
                 [self.navigationController pushViewController:regVC animated:YES];
                 
             }
             else {
                 DLog(@"facebook login");
                 [self loginWithDict:@{@"facebookID": facebookID, @"facebookAccessToken": fbAccessToken}];
             }
         }];
     }];
}

-(void)loginFailHandler
{
    
}

- (void)loginWithDict:(NSDictionary*)dict
{
    if ([dict[@"openID"] isEqualToString:@""]) {
        [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"Please enter user name") message:nil buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
            _topCoverView.hidden = YES;
        }];
    }
    else if ([dict[@"password"] isEqualToString:@""]) {
        [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"Please enter password") message:nil buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
            _topCoverView.hidden = YES;
        }];
    }
    else {
        [DIALOG_MANAGER showLoadingView];
        
        [API_MANAGER loginAction2:dict completion:^(BOOL success, NSString *message) {
            [DIALOG_MANAGER hideLoadingView];
            _topCoverView.hidden = YES;

            if (success) {
                
                if ([message isEqualToString:@"password_wrong"]) {
                    
                    [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"wrong_password") message:nil buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                        
                    }];
                }
                else {
                    
                    [DEFAULTS setObject:dict[@"password"] forKey:@"PASSWORD"];
                    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
                    [SINGLETON setupMainTabBarViewController];
                }
                
            }
            else {
                DLog(@"message = %@", message);
                if ([message isEqualToString:@"password_wrong"]) {
                    
                    [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"wrong_password") message:nil buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                        
                    }];
                }
                else if ([message isEqualToString:@"freeze"]) {
                    
                    [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"is_freezed") message:nil buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                        
                    }];
                    
                }
                else {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }
        }];
    }
}

#pragma mark - Notifications

- (void)notifySocialLogin:(NSNotification *)notification
{
    LiveStreamViewController *vc = [SINGLETON liveStreamSharedManager];
    if (vc.canPresentView) {
        [FACEBOOK_MANAGER setDelegate:self];
        [FACEBOOK_MANAGER loginAction:self];
    }
    else {
        [vc dismissViewControllerAnimated:YES completion:^{
            [FACEBOOK_MANAGER setDelegate:self];
            [FACEBOOK_MANAGER loginAction:self];
        }];
    }
}

- (void)notifyLogin:(NSNotification *)notification
{
    [self onLogin:nil];
}

- (void)notifySignUp:(NSNotification *)notification
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self onSignUp:nil];
    });
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    _indicatorViewLC.constant = scrollView.contentOffset.x * (CGRectGetWidth(self.navigationItem.titleView.frame) / SCREEN_WIDTH) / _tabButtons.count;
    [_guestTabView.indicatorView layoutIfNeeded];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSUInteger currIndex = scrollView.contentOffset.x / scrollView.bounds.size.width;
    [_tabButtons enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.selected = (idx == currIndex) ? YES : NO;
    }];
}

#pragma mark - FacebookManagerDelegate

- (void)facebookManager:(FacebookManager *)facebookManager didLoginComplete:(BOOL)success
{
    if (success)
    {
        [self loginSuccessHandler];
    }
    else
    {
        [self loginFailHandler];
    }
}


#pragma mark LoginHandlerDelegate
- (void) loginSuccess:(BOOL)success loginMode:(loginMode)loginMode userid:(NSString*)userid token:(NSString*)token
{
    _topCoverView.hidden = NO;

    if( loginMode == weiboLoginMode ){
        
        if(success){
            
            [DIALOG_MANAGER showLoadingView];
            [API_MANAGER checkWeiboIDAvailable:userid completion:^(BOOL success, NSString *message) {
                [DIALOG_MANAGER hideLoadingView];
                if (success) {
                    [[LoginHandler sharedInstance] getUserInfo:weiboLoginMode];
                } else {
                    DLog(@"weibo login");
                    [self loginWithDict:@{@"weiboID": userid,@"weiboAccessToken":token}];
                }
            }];
            
            
        }else{
            
        }
        
    }else if(loginMode == wechatLoginMode){
        
        if(success){
            [DIALOG_MANAGER showLoadingView];
            
            [API_MANAGER checkWechatIDAvailable:userid completion:^(BOOL success, NSString *message) {
                [DIALOG_MANAGER hideLoadingView];
                if(success){
                    
                    [[LoginHandler sharedInstance] getUserInfo:wechatLoginMode];
                    
                }else{
                    // wechat login
                    [self loginWithDict:@{@"wechatID": userid,@"wechatAccessToken":token}];
                }
            }];
        }
    }else if( loginMode == qqLoginMode ){
        
        if(success){
            
            [DIALOG_MANAGER showLoadingView];
            [API_MANAGER checkQQIDAvailable:userid completion:^(BOOL success, NSString *message) {
                [DIALOG_MANAGER hideLoadingView];
                if (success) {
                    [[LoginHandler sharedInstance] getUserInfo:qqLoginMode];
                }else{
                    [self loginWithDict:@{@"qqID":userid,@"qqAccessToken":token}];
                }
            }];
            
        }
    }
}

- (void) requestUserInfo:(BOOL)success loginMode:(loginMode)loginMode
{
    /* RegisterInfo */
    RegisterV4ViewController* regVC = [RegisterV4ViewController new];
    
    if( loginMode == weiboLoginMode ){
        
        regVC.registerType = WEIBO_MODE;
        
    }else if(loginMode == wechatLoginMode){
        
        regVC.registerType = WECHAT_MODE;
        
    }else if(loginMode == qqLoginMode){
        
        regVC.registerType = QQ_MODE;
        
    }
    
    [DIALOG_MANAGER showLoadingView];

    DLog(@"push view controller %ld username:%@",loginMode,GET_DEFAULT(USER_OPEN_ID));
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [DIALOG_MANAGER hideLoadingView];
        _topCoverView.hidden = YES;

        regVC.imageTextField.mainTextField.text = GET_DEFAULT(USER_OPEN_ID);
        [self.navigationController pushViewController:regVC animated:YES];
    });
    
    
}



#pragma mark - Actions

- (void)onExploreUser:(id)sender
{
    ExploreUserViewController *vc = [[ExploreUserViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)onLive:(UIButton *)sender {
    if (sender.selected) {
        GuestLiveViewController *vc = _subViewControllers[0];
        [vc scrollToTop];
    }
    else {
        [self scrollToIndex:0];
    }
}

- (IBAction)onExplore:(UIButton *)sender {
    if (sender.selected) {
        GuestExploreViewController *vc = _subViewControllers[1];
        [vc scrollToTop];
    }
    else {
        [self scrollToIndex:1];
    }}

- (IBAction)onSignUp:(id)sender {
#ifdef INTERNATIONAL_VERSION
//    NSArray* dest = @[];
//    dest = @[@2];
    NSMutableArray* arr;
    arr = [NSMutableArray arrayWithObjects:@"Phone register",@"Use Facebook to Sign Up",  nil];

    [DIALOG_MANAGER showActionSheetDialogTitleV2:@"" options:arr destructiveIndexes:nil adminIndexes:nil cancelable:YES withCompletion:^(int selectedOption) {
        [EVENT_HANDLER addEventTracking:@"ClickRegisterButton" withDict:nil];

        if (selectedOption==0) {
            [EVENT_HANDLER addEventTracking:@"ClickSMSRegisterButton" withDict:nil];
            RegisterV4ViewController *vc = [[RegisterV4ViewController alloc] init];
            vc.registerType = MSG_MODE;
            [self.navigationController pushViewController:vc animated:YES];

        }else if (selectedOption==1){
            [EVENT_HANDLER addEventTracking:@"ClickFbRegisterButton" withDict:nil];

            [FACEBOOK_MANAGER setDelegate:self];
            [FACEBOOK_MANAGER loginAction:self];

        }

        
    }];
#else
    [DIALOG_MANAGER showRegisterActionSheetWithCompletion:^(NSString *action) {
        [EVENT_HANDLER addEventTracking:@"ClickRegisterButton" withDict:nil];
        if([action isEqualToString:@"qq"]){
            [EVENT_HANDLER addEventTracking:@"ClickQQRegisterButton" withDict:nil];

            [[LoginHandler sharedInstance] loginWithType:qqLoginMode withViewController:self];

        }else if([action isEqualToString:@"wechat"]){
            [EVENT_HANDLER addEventTracking:@"ClickWechatRegisterButton" withDict:nil];

            [[LoginHandler sharedInstance] loginWithType:wechatLoginMode withViewController:self];
            
        }else if([action isEqualToString:@"facebook"]){
        
            [FACEBOOK_MANAGER setDelegate:self];
            [FACEBOOK_MANAGER loginAction:self];

            
        }else if([action isEqualToString:@"weibo"]){
            [EVENT_HANDLER addEventTracking:@"ClickWeiboRegisterButton" withDict:nil];

            [[LoginHandler sharedInstance] loginWithType:weiboLoginMode withViewController:self];

        }else if([action isEqualToString:@"msg"]){
        
            [EVENT_HANDLER addEventTracking:@"ClickSMSRegisterButton" withDict:nil];

            RegisterV4ViewController *vc = [[RegisterV4ViewController alloc] init];
            vc.registerType = MSG_MODE;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }];
#endif
}

- (IBAction)onLogin:(id)sender {
    LoginV4ViewController* loginVC = [LoginV4ViewController new];
    [self.navigationController pushViewController:loginVC animated:YES];
}

@end
