//
//  GuestFeedViewController.h
//  Story17
//
//  Created by Racing on 2016/1/4.
//  Copyright © 2016年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GuestFeedViewController;
@class PostObject;
@protocol GuestFeedViewControllerDelegate <NSObject>
- (void)guestFeedViewController:(GuestFeedViewController *)guestFeedViewController willPopAndScrollToIndexPath:(NSIndexPath *)indexPath;

@end

@interface GuestFeedViewController : UIViewController
@property (strong, nonatomic) NSMutableArray *posts;
@property (strong, nonatomic) NSIndexPath *currIndexPath;
@property (weak, nonatomic) id<GuestFeedViewControllerDelegate> delegate;

@end
