//
//  GuestExploreViewController.m
//  Story17
//
//  Created by Racing on 2015/12/31.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "GuestExploreViewController.h"
#import "Constant.h"
#import "PostGridCell.h"
#import "PostObject.h"
#import "GuestFeedViewController.h"
#import "SVPullToRefresh.h"

static int const kQueryCount = 18.0f;
static CGFloat const kCellSpace = 2.0f;
static NSUInteger const kQueryThreshold = 9;
static NSString * const kReuseCellIdentifier = @"PostGridCell";

@interface GuestExploreViewController () <UICollectionViewDataSource, UICollectionViewDelegate, GuestFeedViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) NSMutableArray *posts;
@property (assign, nonatomic) BOOL isFetching;

@end

@implementation GuestExploreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_collectionView registerNib:[UINib nibWithNibName:kReuseCellIdentifier bundle:nil] forCellWithReuseIdentifier:kReuseCellIdentifier];
    
    self.posts = [NSMutableArray array];
    self.isFetching = NO;
    
    __weak typeof(self) weakSelf = self;
    [_collectionView addPullToRefreshWithActionHandler:^{
        [weakSelf queryPostsWithCount:kQueryCount beforeTime:INT32_MAX needRefresh:YES];
    }];
    
    [self queryPostsWithCount:kQueryCount beforeTime:INT32_MAX needRefresh:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertPosts:(NSArray *)posts
{
    if (posts.count <= 0) {
        return;
    }
    
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (NSUInteger i = _posts.count; i < _posts.count + posts.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPaths addObject:indexPath];
    }
    
    [_collectionView performBatchUpdates:^{
        [_posts addObjectsFromArray:posts];
        [_collectionView insertItemsAtIndexPaths:indexPaths];
        
    } completion:nil];
}

- (void)showEmptyTips
{
    // Display a message when the table is empty
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    
    messageLabel.text = LOCALIZE(@"There is no posts.");
    messageLabel.textColor = [UIColor colorFromHexString:@"#999999"];
    messageLabel.font = [UIFont systemFontOfSize:14];
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    [messageLabel sizeToFit];
    
    _collectionView.backgroundView = messageLabel;
}

- (void)queryPostsWithCount:(int)count beforeTime:(int)beforeTime needRefresh:(BOOL)needRefresh
{
    self.isFetching = YES;
    if (needRefresh) {
        [_collectionView.pullToRefreshView startAnimating];
    }
    [API_MANAGER getHotPost:beforeTime count:count withCompletion:^(BOOL success, NSArray *posts) {
        if (success) {
            if (needRefresh) {
                [_posts removeAllObjects];
            }
            
            if (_posts.count) {
                // Second time query
                
                [self insertPosts:posts];
            }
            else {
                // First time query
                
                if (posts.count > 0) {
                    [_posts addObjectsFromArray:posts];
                    [_collectionView reloadData];
                }
                else {
                    [self showEmptyTips];
                }
            }
            
        }
        self.isFetching = NO;
        [_activityIndicator stopAnimating];
        if (needRefresh) {
            [_collectionView.pullToRefreshView stopAnimating];
        }
    }];
}

- (void)scrollToTop
{
    if([_posts count]==0)
        return;
    
    [_collectionView setContentOffset:CGPointZero animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PostGridCell *cell = (PostGridCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kReuseCellIdentifier forIndexPath:indexPath];
    
    cell.post = _posts[indexPath.row];
    
    if (indexPath.row == _posts.count - kQueryThreshold - 1 && !_isFetching) {
        int timestamp = ((PostObject *)[_posts lastObject]).timestamp;
        [self queryPostsWithCount:kQueryCount beforeTime:timestamp needRefresh:NO];

    }
    
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _posts.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = (SCREEN_WIDTH - 4 * kCellSpace) / 3;
    
    return CGSizeMake(width, width);
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    GuestFeedViewController *vc = [[GuestFeedViewController alloc] init];
    vc.posts = _posts;
    vc.currIndexPath = indexPath;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - GuestFeedViewControllerDelegate

- (void)guestFeedViewController:(GuestFeedViewController *)guestFeedViewController willPopAndScrollToIndexPath:(NSIndexPath *)indexPath
{
    [_collectionView reloadData];
    [_collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
}

@end
