//
//  GuestLiveViewController.m
//  Story17
//
//  Created by Racing on 2015/12/31.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "GuestLiveViewController.h"
#import "Constant.h"
#import "GuestLiveCollectionViewCell.h"
#import "LiveStreamObject.h"
#import "LiveStreamViewController.h"
#import "UpAndDownAnimator.h"
#import "UpAndDownInteractive.h"
#import "SVPullToRefresh.h"
#import "UserProfileViewController.h"

static int const kQueryCount = 50.0f;
static CGFloat const kCellSpace = 2.0f;
static CGFloat const kLabelGap = 8.0f;
static CGFloat const kLabelHeight = 20.0f;
static NSString * const kReuseCellIdentifier = @"GuestLiveCollectionViewCell";

@interface GuestLiveViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, LiveStreamViewControllerDelegate, UIViewControllerTransitioningDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) NSMutableArray *lives;
@property (nonatomic, strong) UpAndDownInteractive *dismissInteractor;
@property (nonatomic, assign) BOOL enableInteractivePlayerTransitioning;

@end

@implementation GuestLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_collectionView registerNib:[UINib nibWithNibName:kReuseCellIdentifier bundle:nil] forCellWithReuseIdentifier:kReuseCellIdentifier];
    
    self.lives = [NSMutableArray array];
    
    self.dismissInteractor = [[UpAndDownInteractive alloc] init];
    self.enableInteractivePlayerTransitioning = NO;
    
    __weak typeof(self) weakSelf = self;
    [_collectionView addPullToRefreshWithActionHandler:^{
        [weakSelf queryLivesWithCount:kQueryCount needRefresh:YES];
    }];
    
    [self queryLivesWithCount:kQueryCount needRefresh:NO];
    
    [NOTIFICATION_CENTER addObserver:self selector:@selector(notifyRefreshLives:) name:REFRESH_FEEDS object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertLives:(NSArray *)lives
{
    if (lives.count <= 0) {
        return;
    }
    
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (NSUInteger i = _lives.count; i < _lives.count + lives.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPaths addObject:indexPath];
    }
    
    [_collectionView performBatchUpdates:^{
        [_lives addObjectsFromArray:lives];
        [_collectionView insertItemsAtIndexPaths:indexPaths];
        
    } completion:nil];
}

- (void)showEmptyTips
{
    // Display a message when the table is empty
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    
    messageLabel.text = LOCALIZE(@"There is no live streams.");
    messageLabel.textColor = [UIColor colorFromHexString:@"#999999"];
    messageLabel.font = [UIFont systemFontOfSize:14];
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    [messageLabel sizeToFit];
    
    _collectionView.backgroundView = messageLabel;
}

- (void)queryLivesWithCount:(int)count needRefresh:(BOOL)needRefresh
{
    if (needRefresh) {
        [_collectionView.pullToRefreshView startAnimating];
    }

    [API_MANAGER getSuggestedLiveStreams:@"local" count:count withCompletion:^(BOOL success, NSArray *liveSteamObjects) {
        if (success) {
            if (needRefresh) {
                [_lives removeAllObjects];
            }
            
            if (_lives.count) {
                // Second time query
                
                [self insertLives:liveSteamObjects];
            }
            else {
                // First time query
                
                if (liveSteamObjects.count > 0) {
                    [_lives addObjectsFromArray:liveSteamObjects];
                    [_collectionView reloadData];
                }
                else {
                    [self showEmptyTips];
                }
            }
            
        }
        
        [_activityIndicator stopAnimating];
        if (needRefresh) {
            [_collectionView.pullToRefreshView stopAnimating];
        }
    }];
}

- (void)scrollToTop
{
    if([_lives count] ==0)
        return;
    
    [_collectionView setContentOffset:CGPointZero animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Notifications

- (void)notifyRefreshLives:(NSNotification *)notification
{
    [self queryLivesWithCount:kQueryCount needRefresh:YES];
}

#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GuestLiveCollectionViewCell *cell = (GuestLiveCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kReuseCellIdentifier forIndexPath:indexPath];
    
    LiveStreamObject *liveStreamObj = _lives[indexPath.row];
    
    [cell.coverImageView setImageWithURL:S3_FILE_URL(liveStreamObj.user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
    cell.nameLabel.text = liveStreamObj.user.openID;
    cell.titleLabel.text = liveStreamObj.caption;
    cell.verifyImageView.hidden = !liveStreamObj.user.isVerified;
    
    UITapGestureRecognizer *tapProfileGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickProfile:)];
    [cell.profileInfoView addGestureRecognizer:tapProfileGR];
    
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _lives.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = (SCREEN_WIDTH - 3 * kCellSpace) / 2;
    CGFloat height = width + 2 * kLabelGap + 2 * kLabelHeight;
    
    return CGSizeMake(width, height);
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    LiveStreamObject *liveStreamObj = _lives[indexPath.row];
    
    LiveStreamViewController* vc = [SINGLETON liveStreamSharedManager];
    if (vc.canPresentView) {
        vc.livestreamEnterMode = LivestreamEnterModeGuest;
        vc.liveStreamMode = LiveStreamModeWatch;
        vc.liveStream = liveStreamObj;
        vc.delegate = self;
        vc.transitioningDelegate = self;
        vc.modalTransitionStyle = UIModalPresentationCustom;
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        
        [self.navigationController presentViewController:vc animated:YES completion:nil];
    }
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    UpAndDownAnimator *animator = [[UpAndDownAnimator alloc] init];
    animator.transitionType = ModalAnimatedTransitioningTypeDismiss;
    return animator;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    UpAndDownAnimator *animator = [[UpAndDownAnimator alloc] init];
    animator.transitionType = ModalAnimatedTransitioningTypePresent;
    return animator;
}

- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForPresentation:(id<UIViewControllerAnimatedTransitioning>)animator
{
    return nil;
}

- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id<UIViewControllerAnimatedTransitioning>)animator
{
    if (_enableInteractivePlayerTransitioning) {
        return _dismissInteractor;
    }
    return nil;
}

#pragma mark - LiveStreamViewControllerDelegate

- (void)liveStreamViewControllerBeginInteractiveTransitioning:(LiveStreamViewController *)liveStreamViewController
{
    self.enableInteractivePlayerTransitioning = YES;
}

- (void)liveStreamViewController:(LiveStreamViewController *)liveStreamViewController updateInteractiveTransition:(CGFloat)percentComplete shouldComplete:(BOOL)shouldComplete
{
    [_dismissInteractor updateInteractiveTransition:percentComplete];
    _dismissInteractor.shouldComplete = shouldComplete;
}

- (BOOL)liveStreamViewControllerShouldCompleteInteractiveTransition:(LiveStreamViewController *)liveStreamViewController
{
    return _dismissInteractor.shouldComplete;
}

- (void)liveStreamViewController:(LiveStreamViewController *)liveStreamViewController completeInteractiveTransition:(BOOL)complete
{
    if (complete) {
        [_dismissInteractor finishInteractiveTransition];
    }
    else {
        [_dismissInteractor cancelInteractiveTransition];
    }
    
    self.enableInteractivePlayerTransitioning = NO;
}

#pragma mark - Actions

- (void)onClickProfile:(id)sender
{
    CGPoint touchPoint = [sender locationInView: _collectionView];
    NSIndexPath *indexPath = [_collectionView indexPathForItemAtPoint:touchPoint];
    
    LiveStreamObject *liveStreamObj = _lives[indexPath.row];
    
    UserProfileViewController *cv = [[UserProfileViewController alloc] init];
    cv.user = liveStreamObj.user;
    [cv getUserWithUserID:cv.user.userID];
    [self.navigationController pushViewController:cv animated:YES];
    
}

@end
