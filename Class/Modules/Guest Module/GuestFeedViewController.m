//
//  GuestFeedViewController.m
//  Story17
//
//  Created by Racing on 2016/1/4.
//  Copyright © 2016年 POPO_INNOVATION. All rights reserved.
//

#import "GuestFeedViewController.h"
#import "PostListCell.h"
#import "Constant.h"
#import "PostObject.h"
#import "UserProfileViewController.h"
#import "SVPullToRefresh.h"

static int const kQueryCount = 12.0f;
static NSUInteger const kQueryThreshold = 3;
static NSString * const kReuseCellIdentifier = @"FeedCollectionViewCell";

@interface GuestFeedViewController () <UICollectionViewDataSource, UICollectionViewDelegate>
{
    struct {
        unsigned int willPopAndScrollToIndexPath : 1;
    } _delegateFlags;
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (assign, nonatomic) BOOL isFetching;

@end

@implementation GuestFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = LOCALIZE(@"Feed");
    
    [_collectionView registerClass:[PostListCell class] forCellWithReuseIdentifier:kReuseCellIdentifier];
    
    self.isFetching = NO;
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    __weak typeof(self) weakSelf = self;
    [_collectionView addPullToRefreshWithActionHandler:^{
        [weakSelf queryPostsWithCount:kQueryCount beforeTime:INT32_MAX needRefresh:YES];
    }];
    
    [_collectionView reloadData];
    [_collectionView layoutIfNeeded];
    [_collectionView scrollToItemAtIndexPath:_currIndexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        
        if (_delegateFlags.willPopAndScrollToIndexPath) {
            
            NSIndexPath *indexPath = _collectionView.indexPathsForVisibleItems[0];
            [_delegate guestFeedViewController:self willPopAndScrollToIndexPath:indexPath];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertPosts:(NSArray *)posts
{
    if (posts.count <= 0) {
        return;
    }
    
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (NSUInteger i = _posts.count; i < _posts.count + posts.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPaths addObject:indexPath];
    }
    
    [_collectionView performBatchUpdates:^{
        [_posts addObjectsFromArray:posts];
        [_collectionView insertItemsAtIndexPaths:indexPaths];
        
    } completion:nil];
}

- (void)showEmptyTips
{
    // Display a message when the table is empty
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    
    messageLabel.text = LOCALIZE(@"There is no posts.");
    messageLabel.textColor = [UIColor colorFromHexString:@"#999999"];
    messageLabel.font = [UIFont systemFontOfSize:14];
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    [messageLabel sizeToFit];
    
    _collectionView.backgroundView = messageLabel;
}

- (void)queryPostsWithCount:(int)count beforeTime:(int)beforeTime needRefresh:(BOOL)needRefresh
{
    self.isFetching = YES;
    if (needRefresh) {
        [_collectionView.pullToRefreshView startAnimating];
    }
    
    [API_MANAGER getHotPost:beforeTime count:count withCompletion:^(BOOL success, NSArray *posts) {
        if (success) {
            if (needRefresh) {
                [_posts removeAllObjects];
            }
            
            if (_posts.count) {
                // Second time query
                
                [self insertPosts:posts];
            }
            else {
                // First time query
                                
                if (posts.count > 0) {
                    [_posts addObjectsFromArray:posts];
                    [_collectionView reloadData];
                }
                else {
                    [self showEmptyTips];
                }
            }
            
        }
        self.isFetching = NO;
        if (needRefresh) {
            [_collectionView.pullToRefreshView stopAnimating];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PostObject *postObj = _posts[indexPath.row];
    
    NSString* identifier = kReuseCellIdentifier;
    
    if([postObj.type isEqualToString:@"video"]){
        identifier = postObj.postID;
        [_collectionView registerClass:[PostListCell class] forCellWithReuseIdentifier:identifier];
    }
    
    PostListCell *cell = (PostListCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.navCtrl = self.navigationController;
    cell.post = postObj;
    
    if (indexPath.row == _posts.count - kQueryThreshold - 1 && !_isFetching) {
        int timestamp = ((PostObject *)[_posts lastObject]).timestamp;
        [self queryPostsWithCount:kQueryCount beforeTime:timestamp needRefresh:NO];
        
    }
    
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _posts.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PostObject *postObj = _posts[indexPath.row];

    return CGSizeMake(SCREEN_WIDTH, [PostListCell getCellHeigthWithPost:postObj]);
}

#pragma mark - Override

- (void)setDelegate:(id<GuestFeedViewControllerDelegate>)delegate
{
    _delegate = delegate;
    _delegateFlags.willPopAndScrollToIndexPath = [delegate respondsToSelector:@selector(guestFeedViewController:willPopAndScrollToIndexPath:)];
}

@end
