//
//  ListTableViewController.m
//  story17
//
//  Created by POPO Chen on 5/29/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "SearchViewController.h"
//#import "FindFriendViewController.h"
#import "Constant.h"
#import "SVPullToRefresh.h"
#import "SearchUserCell.h"
#import "TagViewCell.h"
#import "SinglePostViewController.h"
#import "UserProfileViewController.h"
#import "BrowsePostsViewController.h"
//list
#import "LocationSearchController.h"
#import "ReasonCell.h"
#import "Constant.h"
#import "MyLocaitionManager.h"

@implementation LocationSearchController

@synthesize selectionCallback;
#define horizontalMargin 5

#define searchUserIdentity @"searchUserIdentity"
#define searchTagIdentity @"searchTagIdentity"

#define searchUserCellHeight 55;
#define searchTagCellHeight 55;


#define listCellIdentity @"listCellIdentity"

-(id)init
{
    self = [super init];
    
    if(self){
        self.currentMode = SEARCH_LEFT;
        
        _isFirstFetching = YES;
        _noMoreData = NO;
        _isFetching = NO;
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    

    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    [self.view setBackgroundColor:LIGHT_BG_COLOR];
    
    self.title = LOCALIZE(@"Search");
    self.navigationController.title = @"";
    
    //_tempArray=[_dataArray copy];
    _searchBar = [UISearchBar new];
    _searchBar.delegate = self;
    _searchBar.showsCancelButton = NO;
    _searchBar.placeholder = LOCALIZE(@"enter_query");
    [_searchBar setTintColor:MAIN_COLOR];
    _searchBar .autocapitalizationType = UITextAutocapitalizationTypeNone;
    _searchBar.keyboardType = UIKeyboardTypeTwitter;
    
    [self.view addSubview:_searchBar];
    
    _listTableView = [UITableView new];
    [_listTableView registerClass:[ReasonCell class] forCellReuseIdentifier:listCellIdentity];
    _listTableView.delegate = self;
    _listTableView.dataSource = self;
    
    [self.view addSubview:_listTableView];
    
    _searchingTable = [UITableView new];
    [_searchingTable registerClass:[ReasonCell class] forCellReuseIdentifier:listCellIdentity];
    _searchingTable.delegate = self;
    _searchingTable.dataSource = self;
    [self.view addSubview:_searchingTable];
    [self setup];

}

-(void)setup
{
    NSLog(@"setup");
    
    _searchBar.frame = CGRectMake(0, 0 , SCREEN_WIDTH, NAVI_BAR_HEIGHT);
    _searchingTable.frame = CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, 0);
    _listTableView.frame = CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-NAVI_BAR_HEIGHT);
    _searchingTable.alpha=0;
}

-(void)viewDidAppear:(BOOL)animated
{
    

    [super viewDidAppear:animated];
    /* Level 5 crash fixed */
      self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    
    [NOTIFICATION_CENTER addObserver:self selector:@selector(keyboardNotificationHandler:) name: UIKeyboardWillChangeFrameNotification object:nil];
    
    LOCATION_MANAGER.delegate= self;
    
    if(self.dataArray==nil){
        [LOCATION_MANAGER updateLocation];
        [DIALOG_MANAGER showLoadingView];
    }else{
        [_listTableView reloadData];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [NOTIFICATION_CENTER removeObserver:self];
    [LOCATION_MANAGER setDelegate:nil];
}


#pragma mark SearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    _searchBar.showsCancelButton = YES;
    _searchingTable.alpha=1;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];

    _searchingTable.alpha=0;
    [_searchBar resignFirstResponder];
    _searchBar.showsCancelButton = NO;
    
    _searchdataarray = @[];
    [_searchingTable reloadData];
    
    _searchBar.text = @"";
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if(searchText.length==0){

//        _searchingTable.alpha=0;
        
    }else{
            
        [LOCATION_MANAGER googleSearchPlace:searchText withCallback:^(BOOL succcess, NSArray *locObjArray) {
            if(succcess){
//                _searchingTable.alpha=1;
                _searchdataarray = locObjArray ;
                [_searchingTable reloadData];
            }
        }];
        
    }

}



#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_listTableView){
        
        return [_dataArray count];
    
    }else if (tableView==_searchingTable){
        
        return [_searchdataarray count];
    }
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReasonCell* cell;
    
    if (tableView==_listTableView)
    {
        cell = [_listTableView dequeueReusableCellWithIdentifier:listCellIdentity];
        if(!cell){
            cell = [[ReasonCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:listCellIdentity];
        }
        [cell reloadCell:_dataArray[indexPath.row]];
        
    }else if (tableView==_searchingTable){
        
        cell = [_searchingTable dequeueReusableCellWithIdentifier:listCellIdentity];
        if(!cell){
            cell = [[ReasonCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:listCellIdentity];
        }
        [cell reloadCell:_searchdataarray[indexPath.row]];
    }
    
    if([cell.mainTextLabel.text isEqualToString:_selectedItem]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _listTableView)
    {
    return [ReasonCell getHeightWithText:_dataArray[indexPath.row]];
    }
    else
    {
    return [ReasonCell getHeightWithText:_searchdataarray[indexPath.row]];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_listTableView)
    {
    
    selectionCallback(_dataArray[indexPath.row]);
    }
    if (tableView==_searchingTable)
    {
        selectionCallback(_searchdataarray[indexPath.row]);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)keyboardNotificationHandler:(NSNotification *)notification
{
     /* Start Button Adjust */
        CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        _searchingTable.frame = CGRectMake(0,NAVI_BAR_HEIGHT ,SCREEN_WIDTH, SCREEN_HEIGHT-2*NAVI_BAR_HEIGHT-keyboardFrame.size.height);
}

-(void)googlePlaceCallback:(BOOL)success
{
    [DIALOG_MANAGER hideLoadingView];
    if(success){
        _dataArray = [LOCATION_MANAGER locationObjArray];
        [_listTableView reloadData];
    }
}

-(void)finishUpdateLocation:(BOOL)success localName:(NSString *)localName geo:(NSDictionary *)geo
{
    
}

@end
