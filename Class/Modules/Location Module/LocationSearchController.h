//
//  LocationSearchController.h
//  story17
//
//  Created by POPO Chen on 5/29/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationSearchController : UIViewController <UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UIScrollViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate,UITableViewDataSource,UITableViewDelegate,UpdateLocationDelegate>

@property (nonatomic,strong) UITableView* listTableView;
@property (nonatomic,strong) UITableView* searchingTable;
@property (nonatomic,strong) NSMutableArray* dataArray;
@property (nonatomic,strong) NSArray* searchdataarray;
@property (nonatomic,strong) NSArray* tempArray;
@property (nonatomic,strong) NSString* selectedItem;
@property (copy)void (^selectionCallback)(LocationObject* locObj);
@property (nonatomic,strong) NSString* locid;


//search bar
@property(nonatomic,strong) UIView* statusView;
@property(nonatomic,strong) UISearchBar* searchBar;
@property(nonatomic,strong) UIView* topView;
/* mode indicator */
@property(nonatomic,strong) UIView* tableModeSelectorContainer;
@property(nonatomic,strong) NSArray* searchButtonArray;
@property(nonatomic,strong) UIImageView* searchIndicatorImageView;


/* mode btn array */
@property(nonatomic,strong) NSArray* searchBtnArray;
@property(nonatomic,strong) UIButton* searchUserBtn;
@property(nonatomic,strong) UIButton* searchTagBtn;

/* main scrollview */
@property(nonatomic,strong) UIScrollView* searchScrollView;

@property(nonatomic,strong) NSArray* tableViewArray;
@property(nonatomic,strong) UITableView* userTableView;
@property(nonatomic,strong) UITableView* tagTableView;
@property(nonatomic,strong) UIImageView* noUserLabel;
@property(nonatomic,strong) UIImageView* noTagLabel;

/* data array */
@property (nonatomic,strong) NSArray* searchArray;
@property (nonatomic,strong) NSArray* placename;


#pragma mark - CollectionView

@property (nonatomic,strong) UIScrollView* mainScrollView;
@property (nonatomic,strong) UICollectionView* photoCollectionView;
@property (nonatomic,strong) NSMutableArray* photoArray;
@property (nonatomic,strong) UIActivityIndicatorView* progressView;
@property (nonatomic,strong) UIImageView* noDataImageView;


@property  int currentMode;

@property BOOL isFirstFetching;
@property BOOL isFetching;
@property BOOL noMoreData;





@end
