//
//  TagObject.h
//  story17
//
//  Created by York on 5/3/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TagObject : NSObject

@property (nonatomic,strong) NSString* hashTag;
@property (nonatomic) int postCount;

+(TagObject*) getTagWithDict:(NSDictionary*)dict;

@end
