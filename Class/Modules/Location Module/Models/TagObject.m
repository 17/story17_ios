//
//  TagObject.m
//  story17
//
//  Created by York on 5/3/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "TagObject.h"

@implementation TagObject

+(TagObject*) getTagWithDict:(NSDictionary*)dict
{
    TagObject* tag = [TagObject new];
    tag.hashTag = dict[@"hashTag"];
    tag.postCount = [dict[@"postCount"] intValue];
    return tag;
}


@end
