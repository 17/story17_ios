//
//  LocationObject.m
//  story17
//
//  Created by York on 5/3/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "LocationObject.h"

@implementation LocationObject

+(LocationObject*) getLocationWithDict:(NSDictionary*)dict
{
    LocationObject* location = [LocationObject new];
    location.locationID = dict[@"locationID"];
    location.name = dict[@"name"];
    location.latitude = [dict[@"latitude"] floatValue];
    location.latitude = [dict[@"latitude"] floatValue];
    return location;
}

@end
