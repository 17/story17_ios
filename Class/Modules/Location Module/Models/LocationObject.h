//
//  LocationObject.h
//  story17
//
//  Created by York on 5/3/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationObject : NSObject

@property (nonatomic,strong) NSString* locationID;
@property (nonatomic) float latitude;
@property (nonatomic) float longitude;
@property (nonatomic,strong) NSString* name;

+(LocationObject*) getLocationWithDict:(NSDictionary*)dict;

@end
