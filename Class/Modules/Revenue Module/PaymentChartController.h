//
//  PaymentChartController.h
//  Story17
//
//  Created by POPO on 2015/5/15.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNChart.h"

@interface PaymentChartController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>


@property (nonatomic,strong) UIView* mainView;

@property (nonatomic,strong) UIScrollView* mainScrollView;

@property(nonatomic,strong) NSArray* tableViewArray;
@property (nonatomic,strong) UITableView* weeklyTableView;
@property (nonatomic,strong) UITableView* monthlyTableView;
@property (nonatomic,strong) UITableView* yearTableView;

/* Date view */
@property(nonatomic,strong) UILabel* weekDateLabel;
@property(nonatomic,strong) UILabel* monthDateLabel;
@property(nonatomic,strong) UILabel* yearDateLabel;

/* mode indicator */
@property(nonatomic,strong) UIView* modeSelectorContainer;
@property(nonatomic,strong) NSArray* modeButtonArray;
@property(nonatomic,strong) UIImageView* modeIndicatorImageView;

/* mode btn array */
@property(nonatomic,strong) NSArray* btnArray;
@property(nonatomic,strong) UIButton* weeklyButton;
@property(nonatomic,strong) UIButton* monthlyButton;
@property(nonatomic,strong) UIButton* yearBotton;

@property (nonatomic,strong) NSMutableArray *weeklyselected;
@property (nonatomic,strong) NSMutableArray *monthlyselected;
@property (nonatomic,strong) NSMutableArray *yearselected;
@property (nonatomic,strong) NSArray* selectedArray;

@property  int viewMode;

/* 7 days revenue */
@property (nonatomic,strong) NSArray* revenue7daysArray;
@property (nonatomic,strong) NSArray* revenue7daysBeforeArray;

/* 30 days revenue */
@property (nonatomic,strong) NSArray* revenue30daysArray;
@property (nonatomic,strong) NSArray* revenue30daysBeforeArray;

/* 12 monthes revenue */
@property (nonatomic,strong) NSArray* revenue12mArray;
@property (nonatomic,strong) NSArray* revenue12mBeforeArray;

@end
