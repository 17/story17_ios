//
//  PostRevenueController.m
//  Story17
//
//  Created by POPO on 2015/5/13.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "PostRevenueController.h"
#import "Constant.h"
#import "PostRevenueCell.h"
#import "SVPullToRefresh.h"
#import "SinglePostViewController.h"


@implementation PostRevenueController

- (id)init
{
    self = [super init];
    if (self)
    {
        _posts = [[NSMutableArray alloc] init];
        _isFetching = NO;
        _noMoreData = NO;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    self.title = LOCALIZE(@"user_revenue_report_post");
    
    if (_tableView == nil)
    {
        _isFirstFetching = YES;
        _progressView = [UIActivityIndicatorView new];
        _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
        [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [_progressView setColor:MAIN_COLOR];
        [self.view addSubview:_progressView];

        CGRect rect = self.view.bounds;
        rect.size.height -= NAVI_BAR_HEIGHT + STATUS_BAR_HEIGHT;
        
//        float size = [PostRevenueCell getDefaultHeight];
        float size = 0;
        float padding = [PostRevenueCell getPadding];
        rect.size.height -= size;
        rect.origin.y += size;
        
        _tableView = [[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
        [_tableView setDataSource:self];
        [_tableView setDelegate:self];
        [_tableView registerClass:[PostRevenueCell class] forCellReuseIdentifier:@"cell"];
        _tableView.contentInset = UIEdgeInsetsMake(10, 0, 5, 0);
        [self.view addSubview:_tableView];
        
        _noDataImageView = [ThemeManager getNoDataImageView];
        [self.view addSubview:_noDataImageView];
        
        rect = CGRectMake(0, 0, size, size-10);
        _timeLabel = [[UILabel alloc] initWithFrame:rect];
        [_timeLabel setText:LOCALIZE(@"Time")];
        [_timeLabel setFont:[UIFont systemFontOfSize:[PostRevenueCell getDefaultFontSize]]];
        [_timeLabel setTextAlignment:NSTextAlignmentCenter];
        [_timeLabel setBackgroundColor:[UIColor clearColor]];
        [_timeLabel setTextColor:DARK_GRAY_COLOR];
//        [self.view addSubview:_timeLabel];
        
        rect.origin.x += 2 * (rect.size.width + padding);
        rect.size.width = 2 * size;
        _captionLabel = [[UILabel alloc] initWithFrame:rect];
        [_captionLabel setText:LOCALIZE(@"Caption")];
        [_captionLabel setFont:[UIFont systemFontOfSize:[PostRevenueCell getDefaultFontSize]]];
        [_captionLabel setTextAlignment:NSTextAlignmentCenter];
        [_captionLabel setBackgroundColor:[UIColor clearColor]];
        [_captionLabel setTextColor:DARK_GRAY_COLOR];

//        [self.view addSubview:_captionLabel];
        
        rect.origin.x = SCREEN_WIDTH/2-70;
        rect.size.width = 140;
        _viewLabel = [[UILabel alloc] initWithFrame:rect];
        [_viewLabel setText:LOCALIZE(@"Views")];
        [_viewLabel setFont:[UIFont systemFontOfSize:[PostRevenueCell getDefaultFontSize]]];
        [_viewLabel setTextAlignment:NSTextAlignmentCenter];
        [_viewLabel setBackgroundColor:[UIColor clearColor]];
        [_viewLabel setTextColor:DARK_GRAY_COLOR];
//        [self.view addSubview:_viewLabel];
        
        rect.origin.x = SCREEN_WIDTH-140;
        rect.size.width = 140;
        _earningLabel = [[UILabel alloc] initWithFrame:rect];
        [_earningLabel setText:LOCALIZE(@"Royalties")];
        [_earningLabel setFont:[UIFont systemFontOfSize:[PostRevenueCell getDefaultFontSize]]];
        [_earningLabel setTextAlignment:NSTextAlignmentCenter];
        [_earningLabel setBackgroundColor:[UIColor clearColor]];
        [_earningLabel setTextColor:DARK_GRAY_COLOR];

//        [self.view addSubview:_earningLabel];
        
        [_tableView setBackgroundColor:[UIColor clearColor]];
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
        [_tableView setAlwaysBounceVertical:YES];
        
        __weak PostRevenueController* ptr = self;
        [_tableView addPullToRefreshWithActionHandler:^{
            [ptr pullDataWithRefresh:YES];
        }];
        
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    if ([_posts count] == 0)
        [self pullDataWithRefresh:YES];
}

- (void)pullDataWithRefresh:(bool)refresh
{
    
    if (refresh){
        _noMoreData = NO;
    }

    if(_isFetching || _noMoreData)
        return;
    
    if(_isFirstFetching){
        [_progressView startAnimating];
        _isFirstFetching = NO;
    }
    
    int lastTime = INT32_MAX;
    
    if ([_posts count] > 0 && !refresh){
        PostObject* obj = [_posts lastObject];
        lastTime = obj.timestamp;
    }
    
    _isFetching = YES;
    [API_MANAGER getUserPost:_userID beforeTime:lastTime andCount:18 withCompletion:^(BOOL success,NSArray* posts){
        
        _isFetching = NO;
        if(success){
            
            if(refresh){
                [_posts removeAllObjects];
            }
            
            [_tableView.pullToRefreshView stopAnimating];
            [_progressView stopAnimating];

            [_posts addObjectsFromArray:posts];

            if([_posts count]==0){
                _noDataImageView.hidden = NO;
            }else{
                _noDataImageView.hidden = YES;
            }            
            
            [_tableView reloadData];
            
            if([posts count]<18){
                _noMoreData = YES;
            }
        }
    }];
}

#pragma mark - data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_posts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PostRevenueCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil)
        cell = [[PostRevenueCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    if (indexPath.row < [_posts count]-9)
        [self pullDataWithRefresh:NO];
    
    if (indexPath.row < [_posts count])
        cell.post = [_posts objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark - delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [PostRevenueCell getDefaultHeight] + [PostRevenueCell getPadding];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PostObject* post = [_posts objectAtIndex:indexPath.row];
    
    SinglePostViewController* sVC = [SinglePostViewController new];
    sVC.enterMode=EnterFromRevenue;
    sVC.hidesBottomBarWhenPushed = YES;
    sVC.post = post;
    [self.navigationController pushViewController:sVC animated:YES];

}


@end
