//
//  PaymentWebViewController.h
//  story17
//
//  Created by POPO Chen on 7/28/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentWebViewController : UIViewController <UIWebViewDelegate>

@end
