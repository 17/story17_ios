//
//  PaymentInfoViewController.m
//  Story17
//
//  Created by POPO on 2015/5/14.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "PaymentInfoViewControllerV2.h"
#import "ListTableViewController.h"
#import "Constant.h"
#import "FillAccountViewController.h"

@implementation FillAccountViewController

#define verticalMargin 5
#define titleLabelHeight 30

- (id)init
{
    self = [super init];
    if (self)
    {
        _Account=@"";
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self addCustomNavigationBackButton];
    [self configureViewForIOS7];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:LOCALIZE(@"Done") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];

    
    
    self.title = LOCALIZE(@"user_revenue_report_account");
    [self.view setBackgroundColor:LIGHT_BG_COLOR];
    _Paypalimg=[[UIImageView alloc]initWithFrame:CGRectMake(100, 40, 91.25f, 32.5f)];

    
    _Paypalimg.center=CGPointMake(SCREEN_WIDTH/2, 50);

    [self.view addSubview:_Paypalimg];
    _Or=[[UILabel alloc]initWithFrame:CGRectMake(300,250, 20, 30)];
    [_Or setText:LOCALIZE(@"Or")];
    [_Or setFont:SYSTEM_FONT_WITH_SIZE(18)];
    [_Or setTextColor:MAIN_COLOR];
    _Or.center=CGPointMake(SCREEN_WIDTH/2, 170);
    _Or.textAlignment = NSTextAlignmentCenter;

    [self.view addSubview:_Or];
    
    
    _GotoSingup=[[UILabel alloc]initWithFrame:CGRectMake(10, 200, SCREEN_WIDTH-20, titleLabelHeight)];

//    [_GotoSingup setBackgroundColor:WHITE_COLOR];
    [_GotoSingup setTextColor:MAIN_COLOR];
    _GotoSingup.center=CGPointMake(SCREEN_WIDTH/2, 210);
    _GotoSingup.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_GotoSingup];

 

    _Form=[[ImageTextField alloc] initWithFrame:CGRectMake(10, 100, SCREEN_WIDTH, [ImageTextField getDefaultHeight])];
    _Form.mainTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _Form.mainTextField.tintColor = GRAY_COLOR;
//    [_Form.bgImage setImage:[IMAGE_FROM_BUNDLE(@"btnlod") resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]];
    [_Form setBackgroundColor:WHITE_COLOR];
    [_Form.mainTextField setTextColor:BLACK_COLOR];
    _Form.mainTextField.delegate = self;

    [self.view addSubview:_Form];

    
    if(_Paypal)
    {
      _Paypalimg.frame=CGRectMake(_Paypalimg.frame.origin.x, _Paypalimg.frame.origin.y, 105, 25);
        [_Paypalimg setImage:[UIImage imageNamed:@"paypal"]];
        
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:LOCALIZE(@"GotoPaypalSignUp")];
        [attributeString addAttribute:NSUnderlineStyleAttributeName
                                value:[NSNumber numberWithInt:1]
                                range:(NSRange){0,[attributeString length]}];
        _GotoSingup.attributedText = [attributeString copy];
        
        
        if([_Account isEqualToString:@""])
        {
            
        _Form.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"Please_fill_your_paypal") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];
        }else{
            _GotoSingup.hidden=YES;

           _Form.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_Account attributes:@{NSForegroundColorAttributeName: BLACK_COLOR}];
           // _Or.hidden=YES;
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:LOCALIZE(@"Delete Account")];
            [attributeString addAttribute:NSUnderlineStyleAttributeName
                                    value:[NSNumber numberWithInt:1]
                                    range:(NSRange){0,[attributeString length]}];
            _Or.attributedText = [attributeString copy];
//            [_Or setText:LOCALIZE(@"Delete Account")];
            [_Or setFont:SYSTEM_FONT_WITH_SIZE(14)];
            [_Or sizeToFit];
            _Or.userInteractionEnabled = YES;
            UITapGestureRecognizer *tapGesture =
            [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickDeletePaypal:)];
            _Or.center=CGPointMake(SCREEN_WIDTH/2, 200);
            [_Or addGestureRecognizer:tapGesture];
            

            
        }
        [_Form.headImage setImage:[UIImage imageNamed:@"lod_email"]];

    }else{
        _Paypalimg.frame=CGRectMake(_Paypalimg.frame.origin.x, _Paypalimg.frame.origin.y, 91.25f, 32.5f);
        [_Paypalimg setImage:[UIImage imageNamed:@"alipay"]];
        
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:LOCALIZE(@"GotoAlipaySignUp")];
        [attributeString addAttribute:NSUnderlineStyleAttributeName
                                value:[NSNumber numberWithInt:1]
                                range:(NSRange){0,[attributeString length]}];
        _GotoSingup.attributedText = [attributeString copy];
        

        
        
        if([_Account isEqualToString:@""])
        {
            _Form.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"Please_fill_your_Alipay") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];
        }else{
            _GotoSingup.hidden=YES;

            _Form.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_Account attributes:@{NSForegroundColorAttributeName: BLACK_COLOR}];
            //_Or.text=YES;
//            [_Or setText:LOCALIZE(@"Delete Account")];
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:LOCALIZE(@"Delete Account")];
            [attributeString addAttribute:NSUnderlineStyleAttributeName
                                    value:[NSNumber numberWithInt:1]
                                    range:(NSRange){0,[attributeString length]}];
            _Or.attributedText = [attributeString copy];
            [_Or setFont:SYSTEM_FONT_WITH_SIZE(14)];
            [_Or sizeToFit];
            _Or.userInteractionEnabled = YES;
            UITapGestureRecognizer *tapGesture =
            [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickDeletePaypal:)];
            _Or.center=CGPointMake(SCREEN_WIDTH/2, 200);
            [_Or addGestureRecognizer:tapGesture];
        }
        [_Form.headImage setImage:[UIImage imageNamed:@"lod_phone"]];

    }
    
    
    
    _GotoSingup.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didclicksignup:)];
    [_GotoSingup addGestureRecognizer:tapGesture];
    
    
    
}



-(void)doneAction:(id)sender
{
    UIViewController* vc=self.navigationController.viewControllers[1];
    
    
    if (_Form.mainTextField.text==nil) {
        _Form.mainTextField.text=@"";
    }
    if (![_Form.mainTextField.text isEqualToString:@""]) {
        if (_Paypal) {
        
            [API_MANAGER updateUserInfo:@{@"paypalEmail": _Form.mainTextField.text,@"alipayPhone":@""} fetchSelfInfo:YES completion:^(BOOL success) {
            if (success) {
                [DIALOG_MANAGER showCompleteToast];
              //  [self.navigationController popViewControllerAnimated:YES ];
                [self.navigationController popToViewController:vc animated:YES];
            }
            else {
                [DIALOG_MANAGER showNetworkFailToast];
            }
            }];

        }else{
        [API_MANAGER updateUserInfo:@{@"paypalEmail":@"",@"alipayPhone":_Form.mainTextField.text} fetchSelfInfo:YES completion:^(BOOL success) {
            if (success) {
                [DIALOG_MANAGER showCompleteToast];
//                [self.navigationController popViewControllerAnimated:YES ];
                [self.navigationController popToViewController:vc animated:YES];

            }
            else {
                [DIALOG_MANAGER showNetworkFailToast];
            }
        }];
     }
        
        
    }else{
        [self.navigationController popToViewController:vc animated:YES];
    }

}


-(void)didclicksignup:(id)sender
{
    [self.view endEditing:YES];

    if (_Paypal)
    {
        [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"open_on_safari") message:LOCALIZE(@"GotoPaypalSignUp")  buttonText:LOCALIZE(@"OK") cancelable:YES withCompletion:^(BOOL okClicked) {
        if(okClicked){
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:PaypalSign_IP]];
            }
        }];
    }else{
        [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"open_on_safari") message:LOCALIZE(@"GotoAlipaySignUp") buttonText:LOCALIZE(@"OK") cancelable:YES withCompletion:^(BOOL okClicked) {
            if(okClicked){
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:AlipaySign_IP]];
            }
        }];
        }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
        _Form.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: BLACK_COLOR}];
}

-(void)didClickDeletePaypal:(id)sender
{
    UIViewController* vc=self.navigationController.viewControllers[1];

    [API_MANAGER updateUserInfo:@{@"paypalEmail":@"",@"alipayPhone":@""} fetchSelfInfo:YES completion:^(BOOL success) {
        if (success) {
            [DIALOG_MANAGER showCompleteToast];
            //  [self.navigationController popViewControllerAnimated:YES ];
            [self.navigationController popToViewController:vc animated:YES];
        }
        else {
            [DIALOG_MANAGER showNetworkFailToast];
        }
    }];
}

@end
