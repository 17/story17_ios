//
//  MonthlyRevenueController.m
//  Story17
//
//  Created by POPO on 2015/5/13.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "MonthlyRevenueController.h"
#import "Constant.h"
#import "TotalRevenueCell.h"
#import "MonthlyRevenueCell.h"

@implementation MonthlyRevenueController

- (id)init
{
    self = [super init];
    if (self)
    {
        _monthlyData = nil;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    self.title = LOCALIZE(@"user_revenue_report_monthly");
    
    if (_tableView == nil)
    {
        CGRect rect = self.view.bounds;
        rect.size.height -= NAVI_BAR_HEIGHT + STATUS_BAR_HEIGHT;
        
        _tableView = [[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
        [self.view addSubview:_tableView];
        
        [_tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
        [_tableView setDataSource:self];
        [_tableView setDelegate:self];
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_tableView registerClass:[TotalRevenueCell class] forCellReuseIdentifier:@"totalRevenueCell"];
        [_tableView registerClass:[MonthlyRevenueCell class] forCellReuseIdentifier:@"monthlyRevenueCell"];

        [_tableView setBackgroundColor:LIGHT_BG_COLOR];
        
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    if (![REVENUE_MANAGER eof])
    {
        //pull data
        [DIALOG_MANAGER showLoadingView];
        [self pullData:YES];
    }else{
        [self loadData];
        [_tableView reloadData];
    }
}

-(void)loadData
{
    _monthlyData = [REVENUE_MANAGER getMonthly];
 
//    for(RevenueObject* rev in _monthlyData){
//        DLog(@"%f",rev.revenue);
//    }
}

- (void)pullData:(BOOL)refresh
{
    if ([REVENUE_MANAGER eof])
        return;
    
    [REVENUE_MANAGER pullDataWithCallback:refresh withCallback:^(BOOL success, NSArray* dataArray){
        if (!success)
            return;
        
        if (![REVENUE_MANAGER eof])
            [self pullData:NO];
        else
        {
            NSLog(@"Get All Data Count : %lu", (unsigned long)[[REVENUE_MANAGER rawData] count]);
            [DIALOG_MANAGER hideLoadingView];
            [self loadData];
            [_tableView reloadData];
        }
    }];
}

#pragma mark - Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

#pragma mark - UITableViewDelegagte
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.section==0){
        return [TotalRevenueCell getDefaultHeight];
    }else{
        return [MonthlyRevenueCell getDefaultHeight];
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return 1;
    
    return _monthlyData == nil ? 0 : [_monthlyData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (indexPath.section == 0)
    {
        TotalRevenueCell* totalCell = [tableView dequeueReusableCellWithIdentifier:@"totalRevenueCell"];
        totalCell.titleLabel.text = LOCALIZE(@"total_revenue");
        [totalCell.revenueLabel setText:[NSString stringWithFormat:LOCALIZE(@"revenues_with_type"),[SINGLETON getCurrencyRate]*[REVENUE_MANAGER totalRevenue],[SINGLETON getCurrencyType]]];
        return totalCell;
    }
    else
    {
        if (_monthlyData != nil && indexPath.row < [_monthlyData count])
        {
            RevenueObject* obj = [_monthlyData objectAtIndex:indexPath.row];

            MonthlyRevenueCell* monthlyCell = [tableView dequeueReusableCellWithIdentifier:@"monthlyRevenueCell"];
            monthlyCell.titleLabel.text = [NSString stringWithFormat:@"%d/%d", obj.year, obj.month];

            [monthlyCell.revenueLabel setText:[NSString stringWithFormat:LOCALIZE(@"revenues_with_type"),[SINGLETON getCurrencyRate]*obj.revenue,[SINGLETON getCurrencyType]]];
            
            return monthlyCell;

        }
    }
    return nil;
}

@end
