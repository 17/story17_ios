//
//  RevenueReportViewController.h
//  Story17
//
//  Created by POPO on 2015/5/13.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserObject.h"
#import "ArrowSwitchLabelView.h"

@interface RevenueReportViewController : UIViewController
{
@private
    UITableView* _postRevenueTableView;
}

@property(nonatomic, strong)UserObject* user;

@property (nonatomic,strong) UIScrollView* mainScrollView;
@property (nonatomic,strong) UILabel* warningLabel;
@property (nonatomic,strong) UILabel* paymentWarningLabel;

@property (nonatomic,strong) UIImageView* lifeTimeTopView;
@property (nonatomic,strong) UILabel* lifeTimeTitle;
@property (nonatomic,strong) UILabel* lifeTimeRevinue;

@property (nonatomic,strong) ArrowSwitchLabelView* todayRevenueView;
@property (nonatomic,strong) ArrowSwitchLabelView* monthRevenueView;
@property (nonatomic,strong) ArrowSwitchLabelView* unpayedRevenueView;
@property (nonatomic,strong) ArrowSwitchLabelView* totalRevenueView;
@property (nonatomic,strong) ArrowSwitchLabelView* paidRevenueView;

@property (nonatomic,strong) ArrowSwitchLabelView* monthlyView;
@property (nonatomic,strong) ArrowSwitchLabelView* dataView;
@property (nonatomic,strong) ArrowSwitchLabelView* postView;
@property (nonatomic,strong) ArrowSwitchLabelView* liveView;
@property (nonatomic,strong) ArrowSwitchLabelView* receiveGiftView;
@property (nonatomic,strong) ArrowSwitchLabelView* accountView;
@property (nonatomic,strong) ArrowSwitchLabelView* paymentView;

@property int enterPageTimeStamp;
@end