//
//  PostRevenueController.h
//  Story17
//
//  Created by POPO on 2015/5/13.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserObject.h"

@interface PostRevenueController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
@private
    UITableView* _tableView;
    NSMutableArray* _posts;
    
    UILabel* _timeLabel;
    UILabel* _captionLabel;
    UILabel* _viewLabel;
    UILabel* _earningLabel;
}

@property (nonatomic, strong)NSString* userID;
@property (nonatomic, strong)UIImageView* noDataImageView;
@property (nonatomic,strong) UIActivityIndicatorView* progressView;
@property BOOL isFirstFetching;
@property BOOL isFetching;
@property BOOL noMoreData;

@end
