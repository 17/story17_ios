//
//  LivestreamRevenueViewController.m
//  story17
//
//  Created by POPO Chen on 8/24/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "LivestreamRevenueViewController.h"
#import "Constant.h"
#import "SVPullToRefresh.h"

@implementation LivestreamRevenueViewController

- (id)init
{
    self = [super init];
    if (self)
    {
        _livestreams = [[NSMutableArray alloc] init];
        _isFetching = NO;
        _noMoreData = NO;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    self.title = LOCALIZE(@"user_revenue_report_live");
    
    if (_mainTableView == nil)
    {
        _isFirstFetching = YES;
        _progressView = [UIActivityIndicatorView new];
        _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
        [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [_progressView setColor:MAIN_COLOR];
        
        CGRect rect = self.view.bounds;
        rect.size.height -= NAVI_BAR_HEIGHT + STATUS_BAR_HEIGHT;
        
        float size = 0;
        float padding = [LivestreamRevenueCell getPadding];
        rect.size.height -= size;
        rect.origin.y += size;
        
        _mainTableView = [[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
        [_mainTableView setDataSource:self];
        [_mainTableView setDelegate:self];
        [_mainTableView registerClass:[LivestreamRevenueCell class] forCellReuseIdentifier:@"cell"];
        _mainTableView.contentInset = UIEdgeInsetsMake(10, 0, 5, 0);
        [self.view addSubview:_mainTableView];

        _noDataImageView = [ThemeManager getNoDataImageView];
        [self.view addSubview:_noDataImageView];
        
        [_mainTableView setBackgroundColor:[UIColor clearColor]];
        [_mainTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_mainTableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
        [_mainTableView setAlwaysBounceVertical:YES];
        [_mainTableView setBackgroundColor:LIGHT_BG_COLOR];
        
        [self.view addSubview:_progressView];

        __weak LivestreamRevenueViewController* ptr = self;
        [_mainTableView addPullToRefreshWithActionHandler:^{
            [ptr pullDataWithRefresh:YES];
        }];
        
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    if ([_livestreams count] == 0)
        [self pullDataWithRefresh:YES];
}

- (void)pullDataWithRefresh:(bool)refresh
{
    
    if (refresh){
        _noMoreData = NO;
    }
 
    if(_isFetching || _noMoreData)
        return;
    
    if(_isFirstFetching){
        [_progressView startAnimating];
        _isFirstFetching = NO;
    }
    
    int lastTime = INT32_MAX;
    
    if ([_livestreams count] > 0 && !refresh){
        LiveStreamObject* obj = [_livestreams lastObject];
        lastTime = obj.beginTime;
    }

    _isFetching = YES;
    [API_MANAGER getUserLiveStreams:_userID beforeTime:lastTime count:18 withCompletion:^(BOOL success, NSArray *livestreams) {
        if(success){
            
            if(refresh){
                [_livestreams removeAllObjects];
            }
            
            _isFetching = NO;
            [_mainTableView.pullToRefreshView stopAnimating];
            [_progressView stopAnimating];
            
            [_livestreams addObjectsFromArray:livestreams];
            
            if([_livestreams count]==0){
                _noDataImageView.hidden = NO;
            }else{
                _noDataImageView.hidden = YES;
            }
            
            [_mainTableView reloadData];
        
            if([livestreams count]<18){
                _noMoreData = YES;
            }
        }
    }];
    

}

#pragma mark - data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_livestreams count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LivestreamRevenueCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil)
        cell = [[LivestreamRevenueCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    if (indexPath.row < [_livestreams count]-9)
        [self pullDataWithRefresh:NO];
    
    if (indexPath.row < [_livestreams count])
        cell.livestream = [_livestreams objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [LivestreamRevenueCell getDefaultHeight] + [LivestreamRevenueCell getPadding];
}



@end
