//
//  PaymentWebViewController.m
//  story17
//
//  Created by POPO Chen on 7/28/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "PaymentWebViewController.h"
#import "Constant.h"

@interface PaymentWebViewController ()

@property (nonatomic,strong) UIWebView* webView;

@end

@implementation PaymentWebViewController

-(id)init
{
    self = [super init];
    if(self){
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    
    self.title = LOCALIZE(@"withdraw_royal");
    
    [DIALOG_MANAGER showLoadingView];
    
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
    _webView.scalesPageToFit = YES;
    _webView.scrollView.bounces = NO;
    _webView.delegate = self;
    
    
    NSString* webUrl = @"";
    
    #ifdef INTERNATIONAL_VERSION
        webUrl = @"http://17app.co/royalty";
    #else
        webUrl = @"http://media17.cn/royalty";
    #endif
    
    NSString* urlString = [NSString stringWithFormat:@"%@?lang=%@",webUrl,LOCALIZE(@"CURRENT_LANGUAGE")];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
    [self.view addSubview:_webView];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [DIALOG_MANAGER hideLoadingView];

}


@end
