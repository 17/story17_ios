//
//  LivestreamRevenueViewController.h
//  story17
//
//  Created by POPO Chen on 8/24/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LivestreamRevenueCell.h"

@interface LivestreamRevenueViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong)UITableView* mainTableView;
@property (nonatomic, strong)NSMutableArray* livestreams;

@property (nonatomic, strong)NSString* userID;
@property (nonatomic, strong)UIImageView* noDataImageView;
@property (nonatomic,strong) UIActivityIndicatorView* progressView;
@property BOOL isFirstFetching;
@property BOOL isFetching;
@property BOOL noMoreData;

@end
