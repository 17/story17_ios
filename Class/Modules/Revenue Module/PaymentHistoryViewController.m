//
//  PaymentHistoryViewController.m
//  story17
//
//  Created by POPO Chen on 5/18/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "PaymentHistoryViewController.h"
#import "Constant.h"
#import "PaymentCell.h"

@implementation PaymentHistoryViewController

#define paymentCellIdentity @"paymentCellIdentity"

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    
    self.title = LOCALIZE(@"user_revenue_report_history");
    
    _mainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT) ];
    _mainTableView.delegate = self;
    _mainTableView.dataSource = self;
    [_mainTableView registerClass:[PaymentCell class] forCellReuseIdentifier:paymentCellIdentity];
    _mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_mainTableView setBackgroundColor:LIGHT_BG_COLOR];
    
    _noDataImageView = [ThemeManager getNoDataImageView];
    [_mainTableView addSubview:_noDataImageView];
    
    _selectedIndexes = CREATE_MUTABLE_ARRAY;
    _paymentArray = CREATE_MUTABLE_ARRAY;
    
    [self.view addSubview:_mainTableView];
    
    [self reloadData];
}

-(void)reloadData
{
    [DIALOG_MANAGER showLoadingView];
    [API_MANAGER getPayments:0 count:200 withCompletion:^(BOOL success, NSArray *paymentArray) {
        [DIALOG_MANAGER hideLoadingView];
        if(success){
            [_paymentArray removeAllObjects];
            for(PaymentObject* payObj in paymentArray){
                [_paymentArray addObject:payObj];
            }
            
            if([_paymentArray count]==0){
                _noDataImageView.hidden = NO;
            }else{
                _noDataImageView.hidden = YES;
            }
            
            [_mainTableView reloadData];
        }
    }];
}

#pragma mark - Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_paymentArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PaymentCell* cell = [tableView dequeueReusableCellWithIdentifier:paymentCellIdentity forIndexPath:indexPath];
    if (cell== nil) {
        cell = [[PaymentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:paymentCellIdentity];
    }
    cell.layer.masksToBounds = YES;
    [cell reloadCell:_paymentArray[indexPath.section]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([self cellIsSelected:indexPath]) {
        return 90;
    }
    
    // Cell isn't selected so return single height
    return 60;
    
}

- (BOOL)cellIsSelected:(NSIndexPath *)indexPath {
    // Return whether the cell at the specified index path is selected or not
    if([_selectedIndexes containsObject:indexPath]){
        return TRUE;
    } else{
        return FALSE;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Deselect cell
    //    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    
    if([_selectedIndexes containsObject:indexPath]){
        [_selectedIndexes removeObject:indexPath];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }else{
        [_selectedIndexes addObject:indexPath];
    }


    [_mainTableView beginUpdates];
    [_mainTableView endUpdates];
    
}


@end
