//
//  PaymentInfoViewController.h
//  Story17
//
//  Created by POPO on 2015/5/14.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TitleLabelView.h"
#import "MultiFileUploader.h"

@interface PaymentInfoViewControllerV2 : UIViewController
{
@private

}


@property (nonatomic,strong)UILabel* PickOneAccountLabel;
@property (nonatomic,strong)UILabel* ChooseOneLabel;
@property (nonatomic,strong)UIButton* PaypalBtn;
@property (nonatomic,strong)UIButton* AlipayBtn;
@property (nonatomic)BOOL Paypal;
@property (nonatomic)BOOL Neverfill;

@end
