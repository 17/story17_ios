//
//  PaymentCell.h
//  story17
//
//  Created by POPO Chen on 5/18/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentObject.h"

@interface PaymentCell : UITableViewCell

@property(nonatomic,strong) UIView* bgView;

@property(nonatomic,strong) UILabel* timeLabel;
@property(nonatomic,strong) UILabel* revenueLabel;
@property(nonatomic,strong) UIView* separatedLine;

@property(nonatomic,strong) UILabel* bankName;
@property(nonatomic,strong) UILabel* accountNumber;
@property(nonatomic,strong) UILabel* accountName;

-(void)reloadCell:(PaymentObject*)payObj;


@end
