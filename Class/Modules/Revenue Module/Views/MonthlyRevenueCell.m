//
//  MonthlyRevenueCell.m
//  story17
//
//  Created by POPO Chen on 5/27/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "MonthlyRevenueCell.h"
#import "Constant.h"

@implementation MonthlyRevenueCell

#define labelHeight 30

+(float)getDefaultHeight
{
    return labelHeight+10;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self awakeFromNib];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)awakeFromNib {
    
    _bgView = [[UIView alloc]initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-20, 40)];
    
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, SCREEN_WIDTH/2, labelHeight)];
    _revenueLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2-10, 5, SCREEN_WIDTH/2-20, labelHeight)];
    
    [_titleLabel setTextColor:GRAY_COLOR];
    [_revenueLabel setTextColor:DARK_GRAY_COLOR];
    _revenueLabel.textAlignment = NSTextAlignmentRight;
    
    _titleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    _revenueLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    
    
    [_bgView setBackgroundColor:WHITE_COLOR];
    [self.contentView setBackgroundColor:LIGHT_BG_COLOR];
    
    [_bgView addSubview:_titleLabel];
    [_bgView addSubview:_revenueLabel];
    
    UIView* separatorLine = [ThemeManager separaterLine];
    separatorLine.frame = CGRectMake(0, labelHeight+10-1, SCREEN_WIDTH-20, 0.5);
    
    [_bgView setBackgroundColor:WHITE_COLOR];
    [_bgView addSubview:_titleLabel];
    [_bgView addSubview:_revenueLabel];
    [_bgView addSubview:separatorLine];
    
    [self.contentView addSubview:_bgView];
    
}


@end
