//
//  LivestreamRevenueCell.h
//  story17
//
//  Created by POPO Chen on 8/24/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveStreamObject.h"

@interface LivestreamRevenueCell : UITableViewCell


@property(nonatomic, strong) UILabel* dateLabel;

@property(nonatomic, strong) UILabel* captionTextLabel;
@property(nonatomic, strong) UILabel* captionLabel;

@property(nonatomic, strong) UILabel* likeTextLabel;
@property(nonatomic, strong) UILabel* likeLabel;

@property(nonatomic, strong) UILabel* durationTextLabel;
@property(nonatomic, strong) UILabel* durationLabel;

@property(nonatomic, strong) UILabel* viewTextLabel;
@property(nonatomic, strong) UILabel* viewCountLabel;

@property(nonatomic, strong) UILabel* totalTimeTextLabel;
@property(nonatomic, strong) UILabel* totalTimeLabel;

@property(nonatomic, strong) UILabel* revenueLabel;
@property(nonatomic, strong) UILabel* revenueTextLabel;

@property(nonatomic, weak)LiveStreamObject* livestream;

+ (float)getDefaultHeight;
+ (float)getDefaultFontSize;
+ (float)getPadding;

@end
