//
//  PostRevenueCell.m
//  Story17
//
//  Created by POPO on 2015/5/13.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "PostRevenueCell.h"
#import "Constant.h"
#import "UIImageView+AFNetworking.h"

#define PostRevenueCellUISize 150
#define PostRevenueCellLabelFontSize 14
#define PostRevenueCellPadding 10

@implementation PostRevenueCell

@synthesize post = _post;

+ (float)getDefaultHeight
{
    return PostRevenueCellUISize;
}

+ (float)getDefaultFontSize
{
    return PostRevenueCellLabelFontSize;
}

+ (float)getPadding
{
    return PostRevenueCellPadding;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _postImage = [[UIImageView alloc] init];
        [self addSubview:_postImage];
        
        _videoImageView = [[UIImageView alloc] init];
        [_videoImageView setImage:IMAGE_FROM_BUNDLE(@"video")];
        [self addSubview:_videoImageView];
        
        _videoImageView.hidden = YES;
        
        _postCaption = [[UILabel alloc] init];
        [_postCaption setTextAlignment:NSTextAlignmentCenter];
        [_postCaption setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_postCaption setTextColor:GRAY_COLOR];

        _revenueLabel = [[UILabel alloc] init];
        [_revenueLabel setTextAlignment:NSTextAlignmentCenter];
        [_revenueLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_revenueLabel setTextColor:GRAY_COLOR];
        [self addSubview:_revenueLabel];
        
        _revenueTextLabel = [[UILabel alloc] init];
        [_revenueTextLabel setTextAlignment:NSTextAlignmentCenter];
        [_revenueTextLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_revenueTextLabel setTextColor:GRAY_COLOR];
        [self addSubview:_revenueTextLabel];
        
        _viewCountLabel = [[UILabel alloc] init];
        [_viewCountLabel setTextAlignment:NSTextAlignmentCenter];
        [_viewCountLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_viewCountLabel setTextColor:GRAY_COLOR];
        [self addSubview:_viewCountLabel];
        
        _viewTextLabel = [[UILabel alloc] init];
        [_viewTextLabel setTextAlignment:NSTextAlignmentCenter];
        [_viewTextLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_viewTextLabel setTextColor:GRAY_COLOR];
        [self addSubview:_viewTextLabel];
        
        
        _likeLabel = [[UILabel alloc] init];
        [_likeLabel setTextAlignment:NSTextAlignmentCenter];
        [_likeLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_likeLabel setTextColor:GRAY_COLOR];
        [self addSubview:_likeLabel];
        
        _likeTextLabel = [[UILabel alloc] init];
        [_likeTextLabel setTextAlignment:NSTextAlignmentCenter];
        [_likeTextLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_likeTextLabel setTextColor:GRAY_COLOR];
        [self addSubview:_likeTextLabel];

        
        _dateLabel = [[UILabel alloc] init];
        [_dateLabel setNumberOfLines:0];
        [_dateLabel setTextAlignment:NSTextAlignmentRight];
        [_dateLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [_dateLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize-1]];
        [_dateLabel setTextColor:GRAY_COLOR];
        [self addSubview:_dateLabel];
        
        
        UIView* lineView1 = [ThemeManager separaterLine];
        lineView1.frame = CGRectMake(PostRevenueCellUISize+2*PostRevenueCellPadding, 3*PostRevenueCellUISize/7, SCREEN_WIDTH-PostRevenueCellUISize-3*PostRevenueCellPadding, 0.5);
        
        UIView* lineView2 = [ThemeManager separaterLine];
        lineView2.frame = CGRectMake(PostRevenueCellUISize+2*PostRevenueCellPadding, 5*PostRevenueCellUISize/7, SCREEN_WIDTH-PostRevenueCellUISize-3*PostRevenueCellPadding, 0.5);
        
        [self addSubview:lineView1];
        [self addSubview:lineView2];

        [_likeTextLabel setText:LOCALIZE(@"LikeCount")];
        [_revenueTextLabel setText:LOCALIZE(@"Royalties")];
        [_viewTextLabel setText:LOCALIZE(@"Views")];
        
        [self setBackgroundColor:[UIColor whiteColor]];
    }
    
    return self;
}

- (void)setFrame:(CGRect)frame
{
    frame.size.height -= 5;
    [super setFrame:frame];
    
    
    _postImage.frame =  CGRectMake(PostRevenueCellPadding, 0, PostRevenueCellUISize, PostRevenueCellUISize);

    float fromX = _postImage.frame.origin.x+_postImage.frame.size.width;
    float nextY = 0;
    float labelWidth = (SCREEN_WIDTH-2*PostRevenueCellPadding-PostRevenueCellUISize)/2;
    
    _dateLabel.frame = CGRectMake(fromX, 0, SCREEN_WIDTH-2*PostRevenueCellPadding-PostRevenueCellUISize, PostRevenueCellUISize/7);
    nextY += _dateLabel.frame.origin.y + _dateLabel.frame.size.height;
    
    _viewTextLabel.frame = CGRectMake(fromX, nextY, labelWidth, 2*PostRevenueCellUISize/7);
    _viewCountLabel.frame = CGRectMake(_viewTextLabel.frame.origin.x+_viewTextLabel.frame.size.width,nextY, labelWidth, 2*PostRevenueCellUISize/7);

    nextY += 2*PostRevenueCellUISize/7;
    
    _likeTextLabel.frame = CGRectMake(fromX, nextY, labelWidth, 2*PostRevenueCellUISize/7);
    _likeLabel.frame = CGRectMake(_likeTextLabel.frame.origin.x+_likeTextLabel.frame.size.width,nextY, labelWidth, 2*PostRevenueCellUISize/7);
    nextY += 2*PostRevenueCellUISize/7;

    
    _revenueTextLabel.frame = CGRectMake(fromX, nextY, labelWidth, 2*PostRevenueCellUISize/7);
    _revenueLabel.frame = CGRectMake(_revenueTextLabel.frame.origin.x+_revenueTextLabel.frame.size.width, nextY, labelWidth, 2*PostRevenueCellUISize/7);

}

- (void)setPost:(PostObject *)post
{
    _post = post;
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    
    NSDate* date = [[NSDate alloc] initWithTimeIntervalSince1970:_post.timestamp];
    _dateLabel.text = [dateFormat stringFromDate:date];
    
    [_postImage setImageWithURL:S3_THUMB_IMAGE_URL(_post.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_s")];
    _postCaption.text = _post.caption;
    _viewCountLabel.text = [NSString stringWithFormat:@"%@", [SINGLETON getCountNumNormalization:_post.viewCount]];
    
    [_revenueLabel setText:[NSString stringWithFormat:LOCALIZE(@"revenues_with_type"),[SINGLETON getCurrencyRate]*post.totalRevenue,[SINGLETON getCurrencyType]]];
    [_likeLabel setText:[NSString stringWithFormat:@"%d",_post.likeCount]];
    
    if([post.type isEqualToString:@"video"]){
        _videoImageView.hidden = NO;
    }else if([post.type isEqualToString:@"image"]){
        _videoImageView.hidden = YES;
    }
    
}

@end
