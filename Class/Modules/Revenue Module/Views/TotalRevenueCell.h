//
//  TotalRevenueCell.h
//  story17
//
//  Created by POPO Chen on 5/27/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TotalRevenueCell : UITableViewCell

@property (nonatomic,strong) UIView* bgView;
@property (nonatomic,strong) UILabel* titleLabel;
@property (nonatomic,strong) UILabel* revenueLabel;

+(float)getDefaultHeight;

@end
