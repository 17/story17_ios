//
//  PaymentChartCell.h
//  Story17
//
//  Created by POPO on 2015/5/15.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNChart.h"
#import "RevenueObject.h"

@interface PaymentChartCell : UITableViewCell
{
@private
    PNLineChart* _lineChart;
    
    UILabel* _titleLabel;
    UILabel* _subTitleLabel;
    UILabel* _upDownLabel;
}

@property(nonatomic, strong)NSString* title;
@property(nonatomic, strong)NSString* subTitle;
@property(nonatomic) float upDown;
@property(nonatomic, strong)NSArray* item;

@property(nonatomic, strong)UIView* bgView;

@property(nonatomic, strong)UILabel* yAxisLabel;


+ (float)getTitleHeight;

+ (float)getFullCellHeight;

-(void)reloadCellWithRevenue:(NSString*)title andArray:(NSArray*)revenueObjArray compareTo:(NSArray*)revenueBeforeArray ;
-(void)reloadCellWithViews:(NSString*)title andArray:(NSArray*)revenueObjArray compareTo:(NSArray*)revenueBeforeArray ;
-(void)chartAnimation;

@end
