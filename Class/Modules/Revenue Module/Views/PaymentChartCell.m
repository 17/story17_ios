//
//  PaymentChartCell.m
//  Story17
//
//  Created by POPO on 2015/5/15.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "PaymentChartCell.h"
#import "RevenueObject.h"
#import "Constant.h"

#define PaymentTitleHeight 50
#define PaymentChartHeight 180

@implementation PaymentChartCell

+ (float)getTitleHeight
{
    return PaymentTitleHeight;
}

+ (float)getFullCellHeight
{
    return PaymentTitleHeight + PaymentChartHeight;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self awakeFromNib];
    }
    return self;
}

- (void)awakeFromNib {
    
    [self setBackgroundColor:WHITE_COLOR];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CGRect rect = CGRectMake(10, 5, SCREEN_WIDTH / 2, PaymentTitleHeight / 2-5);
    _titleLabel = [[UILabel alloc] initWithFrame:rect];
    _titleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [_titleLabel setTextColor:DARK_GRAY_COLOR];
    [self.contentView addSubview:_titleLabel];
    
    rect.origin.y += rect.size.height;
    _subTitleLabel = [[UILabel alloc] initWithFrame:rect];
    _subTitleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [_subTitleLabel setTextColor:GRAY_COLOR];
    [self.contentView addSubview:_subTitleLabel];
    
    rect.origin.x = SCREEN_WIDTH / 2;
    rect.origin.y = 0;
    rect.size.width = SCREEN_WIDTH / 2-10;
    rect.size.height = PaymentTitleHeight;
    _upDownLabel = [[UILabel alloc] initWithFrame:rect];
    _upDownLabel.textAlignment = NSTextAlignmentRight;
    _upDownLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [self.contentView addSubview:_upDownLabel];
    
    rect.origin.x = -20;
    rect.origin.y = PaymentTitleHeight;
    rect.size.width = SCREEN_WIDTH ;
    rect.size.height = PaymentChartHeight;
    _lineChart = [[PNLineChart alloc] initWithFrame:rect];
    [self.contentView addSubview:_lineChart];

    rect.origin.x = 10;
    rect.size.width = SCREEN_WIDTH;
    rect.size.height = 40;
    _yAxisLabel = [[UILabel alloc]initWithFrame:rect];
    [_yAxisLabel setTextColor:BLACK_COLOR];
    _yAxisLabel.font = BOLD_FONT_WITH_SIZE(12);
    
    [self.contentView addSubview:_yAxisLabel];
    
}

- (void)setTitle:(NSString *)title
{
    _titleLabel.text = title;
}

- (NSString *)title
{
    return _titleLabel.text;
}

- (void)setSubTitle:(NSString *)subTitle
{
    _subTitleLabel.text = subTitle;
}

- (NSString *)subTitle
{
    return _subTitleLabel.text;
}

- (void)setUpDown:(float)total withBeforeValue:(float)before mode:(NSString*)mode
{
    
    if(before==0){
        
        if([mode isEqual:@"Revenue"]){
                        
            _upDownLabel.text = [NSString stringWithFormat:LOCALIZE(@"revenues_rate_up_type"),[SINGLETON getCurrencyRate]*total,[SINGLETON getCurrencyType]];

        }else{
            _upDownLabel.text = [NSString stringWithFormat:@"%d ▲",(int)total];

        }
        [_upDownLabel setTextColor:ALERT_TEXT_COLOR];

    }else{
        /* %%%%% */
        float upDown = (total-before)/before;
        _upDown = upDown;
        
        NSString* tag = nil;
        if (upDown >= 0){
            tag = @"▲";
            [_upDownLabel setTextColor:ALERT_TEXT_COLOR];
        }else if (upDown < 0){
            tag = @"▼";
            [_upDownLabel setTextColor:MAIN_COLOR];
        }
        
        _upDownLabel.text = [NSString stringWithFormat:@"%@ %0.1f%%", tag, ABS(upDown)];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)reloadCellWithRevenue:(NSString*)title andArray:(NSArray*)revenueObjArray compareTo:(NSArray*)revenueBeforeArray ;
{
 
    if([revenueObjArray count]<7) return;
    
    NSMutableArray * data01Array = CREATE_MUTABLE_ARRAY;
    NSMutableArray * xLabels = CREATE_MUTABLE_ARRAY;
    float totalRevenue = 0;
    float preRevenue = 0;
    
    float maxValue = 0.0f;
    float minValue = 1.0f;
    
    float rent = [SINGLETON getCurrencyRate];
   
    /* Axis Fixed  */
    for(RevenueObject* revObj in revenueObjArray){
        if(revObj.revenue==0.0){
            [data01Array addObject:[NSNumber numberWithFloat:1.0f]];
        }else{
            [data01Array addObject:[NSNumber numberWithFloat:revObj.revenue*10000*rent]];
        }
        
        if(revObj.revenue*rent>maxValue){
            maxValue = revObj.revenue*rent;
        }
        if(revObj.revenue*rent<minValue && revObj.revenue*rent>1.0f){
            minValue = revObj.revenue*rent;
        }
        
        if([revenueObjArray count]<20){
            
            if([revenueBeforeArray count]>7){
                [xLabels addObject:[NSString stringWithFormat:@"%d",revObj.month]];
            }else{
                [xLabels addObject:[NSString stringWithFormat:@"%d/%d",revObj.month,revObj.day]];
            }
        
        }else{
            [xLabels addObject:@""];
        }
        totalRevenue += revObj.revenue ;
    }
    
    for(RevenueObject* revObj in revenueBeforeArray){
        preRevenue += revObj.revenue ;
    }
    
    /* Label Settings */
    _lineChart.yFixedValueMax = maxValue*10000<10?10:maxValue*10000;
    _lineChart.yFixedValueMin = 0.0f;
    
    [_lineChart setXLabels:xLabels];
    [_lineChart setShowGenYLabels:NO];
    
    

    PNLineChartData *data01 = [PNLineChartData new];
    data01.color = PNFreshGreen;
    data01.itemCount = _lineChart.xLabels.count;
    data01.inflexionPointStyle = PNLineChartPointStyleNone;

    data01.getData = ^(NSUInteger index) {
        CGFloat yValue = [data01Array[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    
    _lineChart.chartData = @[data01];
    [self chartAnimation];
    

    [_yAxisLabel setText:[NSString stringWithFormat:LOCALIZE(@"revenues_with_type"),maxValue,[SINGLETON getCurrencyType]]];
    [_titleLabel setText:[NSString stringWithFormat:LOCALIZE(@"revenues_with_type"),totalRevenue,[SINGLETON getCurrencyType]]];
    [_subTitleLabel setText:LOCALIZE(@"Royalties")];


    [self setUpDown:totalRevenue withBeforeValue:preRevenue mode:@"Revenue"];
    
}

-(void)reloadCellWithViews:(NSString*)title andArray:(NSArray*)revenueObjArray compareTo:(NSArray*)revenueBeforeArray
{
    
    if([revenueObjArray count]<7) return;
    
    NSMutableArray * data01Array = CREATE_MUTABLE_ARRAY;
    NSMutableArray * xLabels = CREATE_MUTABLE_ARRAY;
    
    int totalViews = 0;
    int preViews = 0;
    
    int maxValue = 0.0;
    int minValue = 0.0;
    
    for(RevenueObject* revObj in revenueObjArray){
        if(revObj.totalViews==0.0){
            [data01Array addObject:[NSNumber numberWithFloat:1.0f]];
        }else{
            [data01Array addObject:[NSNumber numberWithFloat:revObj.totalViews*10000]];
        }
        
        if(revObj.totalViews>maxValue){
            maxValue = revObj.totalViews;
        }
        if(revObj.totalViews<minValue && revObj.totalViews>0){
            minValue = revObj.totalViews;
        }
        
        if([revenueObjArray count]<20){
            if([revenueBeforeArray count]>7){
                [xLabels addObject:[NSString stringWithFormat:@"%d",revObj.month]];
            }else{
                [xLabels addObject:[NSString stringWithFormat:@"%d/%d",revObj.month,revObj.day]];
            }
        }else{
            [xLabels addObject:@""];
        }
        totalViews += revObj.totalViews ;
    }
    
    for(RevenueObject* revObj in revenueBeforeArray){
        preViews += revObj.totalViews ;
    }
    
    _lineChart.yFixedValueMax = maxValue*10000<10?10:maxValue*10000;
    _lineChart.yFixedValueMin = minValue;
    
    [_lineChart setXLabels:xLabels];
    [_lineChart setShowGenYLabels:NO];
    
    
    PNLineChartData *data01 = [PNLineChartData new];
    data01.color = PNFreshGreen;
    data01.itemCount = data01Array.count;
    data01.inflexionPointStyle = PNLineChartPointStyleNone;
    data01.getData = ^(NSUInteger index) {
        CGFloat yValue = [data01Array[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    data01.color = PNTwitterColor;

    
    _lineChart.chartData = @[data01];
    [self chartAnimation];
    [_yAxisLabel setText:[NSString stringWithFormat:@"%d",maxValue]];
    [_titleLabel setText:[NSString stringWithFormat:@"%d",totalViews]];
    [_subTitleLabel setText:LOCALIZE(@"Views")];
    [self setUpDown:totalViews withBeforeValue:preViews  mode:@"Views"];
}

-(void)chartAnimation
{
    [_lineChart strokeChart];
}

-(void)userClickedOnLineKeyPoint:(CGPoint)point lineIndex:(NSInteger)lineIndex pointIndex:(NSInteger)pointIndex{
    NSLog(@"Click Key on line %f, %f line index is %d and point index is %d",point.x, point.y,(int)lineIndex, (int)pointIndex);
}

-(void)userClickedOnLinePoint:(CGPoint)point lineIndex:(NSInteger)lineIndex{
    NSLog(@"Click on line %f, %f, line index is %d",point.x, point.y, (int)lineIndex);
}

@end
