//
//  TotalRevenueCell.m
//  story17
//
//  Created by POPO Chen on 5/27/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "TotalRevenueCell.h"
#import "Constant.h"

@implementation TotalRevenueCell

#define labelHeight 30

+(float)getDefaultHeight
{
    return 2*labelHeight+30;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self awakeFromNib];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)awakeFromNib {
    
    _bgView = [[UIView alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH-20, 70)];
    
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, SCREEN_WIDTH, labelHeight)];
    _revenueLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, labelHeight, SCREEN_WIDTH, labelHeight)];
    
    [_titleLabel setTextColor:GRAY_COLOR];
    [_revenueLabel setTextColor:DARK_GRAY_COLOR];
    
    _titleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    _revenueLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    
    [_bgView setBackgroundColor:WHITE_COLOR];
    [self.contentView setBackgroundColor:LIGHT_BG_COLOR];
    
    [_bgView addSubview:_titleLabel];
    [_bgView addSubview:_revenueLabel];
    
    [self.contentView addSubview:_bgView];
    
}


@end
