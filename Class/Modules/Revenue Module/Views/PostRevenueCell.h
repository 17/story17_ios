//
//  PostRevenueCell.h
//  Story17
//
//  Created by POPO on 2015/5/13.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostObject.h"

@interface PostRevenueCell : UITableViewCell
{
@private
    UIImageView* _postImage;
    UIImageView* _videoImageView;
    UILabel* _postCaption;
    UILabel* _revenueLabel;
    UILabel* _viewCountLabel;
    UILabel* _dateLabel;
}

@property(nonatomic, strong) UILabel* viewTextLabel;
@property(nonatomic, strong) UILabel* likeTextLabel;
@property(nonatomic, strong) UILabel* likeLabel;
@property(nonatomic, strong) UILabel* revenueTextLabel;

@property(nonatomic, weak)PostObject* post;

+ (float)getDefaultHeight;

+ (float)getDefaultFontSize;

+ (float)getPadding;

@end
