//
//  PaymentCell.m
//  story17
//
//  Created by POPO Chen on 5/18/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "PaymentCell.h"
#import "Constant.h"
#import "ThemeManager.h"


@implementation PaymentCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self awakeFromNib];
    }
    return self;
}

- (void)awakeFromNib
{
    
    _bgView = [[UIView alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH-20, 150)];
    [_bgView setBackgroundColor:WHITE_COLOR];
    [self.contentView setBackgroundColor:LIGHT_BG_COLOR];
    
    _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 10, SCREEN_WIDTH/3, 30)];
    _revenueLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2-10, 10, SCREEN_WIDTH/2-20, 30)];
    _revenueLabel.textAlignment = NSTextAlignmentRight;
    _separatedLine = [ThemeManager separaterLine];
    _separatedLine.frame = CGRectMake(0, 59, SCREEN_WIDTH, 1);
    
    _timeLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    _revenueLabel.font = SYSTEM_FONT_WITH_SIZE(14);

    [_timeLabel setTextColor:DARK_GRAY_COLOR];
    [_revenueLabel setTextColor:GRAY_COLOR];
    
    UIView* separatorLine = [ThemeManager separaterLine];
    separatorLine.frame = CGRectMake(5, 51, SCREEN_WIDTH-30, 0.5);
    [_bgView addSubview:separatorLine];
    
    _bankName = [[UILabel alloc]initWithFrame:CGRectMake(10, 53, SCREEN_WIDTH/4, 25)];
    
    _accountNumber = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/4, 53, 3*SCREEN_WIDTH/4, 25)];
    _accountName = [[UILabel alloc]initWithFrame:CGRectMake(5, 109, SCREEN_WIDTH/3, 25)];

    _bankName.font = SYSTEM_FONT_WITH_SIZE(14);
    _accountNumber.font = SYSTEM_FONT_WITH_SIZE(12);
    _accountName.font = SYSTEM_FONT_WITH_SIZE(12);
    
    [_bgView addSubview:_timeLabel];
    [_bgView addSubview:_revenueLabel];
    [_bgView addSubview:_bankName];
    [_bgView addSubview:_accountName];
    [_bgView addSubview:_accountNumber];
    [self.contentView addSubview:_bgView];
    
}

-(void)reloadCell:(PaymentObject*)paymentObj
{
    [_timeLabel setText: [NSString stringWithFormat:@"%d/%d",paymentObj.year,paymentObj.month]];
    [_revenueLabel setText:[NSString stringWithFormat:@"%.2lf USD",paymentObj.amount]];
    [_bankName setText:paymentObj.bankName];
    [_accountNumber setText:[NSString stringWithFormat:@"%@",paymentObj.accountNumber]];
    [_accountName setText:paymentObj.beneficiaryName];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

@end
