//
//  LivestreamRevenueCell.m
//  story17
//
//  Created by POPO Chen on 8/24/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "LivestreamRevenueCell.h"
#import "Constant.h"
#import "UIImageView+AFNetworking.h"

#define PostRevenueCellUISize 200
#define PostRevenueCellLabelFontSize 14
#define PostRevenueCellPadding 10

@implementation LivestreamRevenueCell

+ (float)getDefaultHeight
{
    return PostRevenueCellUISize;
}

+ (float)getDefaultFontSize
{
    return PostRevenueCellLabelFontSize;
}

+ (float)getPadding
{
    return PostRevenueCellPadding;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _captionTextLabel = [[UILabel alloc] init];
        [_captionTextLabel setTextAlignment:NSTextAlignmentCenter];
        [_captionTextLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_captionTextLabel setTextColor:GRAY_COLOR];
        [self addSubview:_captionTextLabel];
        
        _captionLabel = [[UILabel alloc] init];
        [_captionLabel setTextAlignment:NSTextAlignmentCenter];
        [_captionLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_captionLabel setTextColor:GRAY_COLOR];
        [self addSubview:_captionLabel];

        _durationTextLabel = [[UILabel alloc] init];
        [_durationTextLabel setTextAlignment:NSTextAlignmentCenter];
        [_durationTextLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_durationTextLabel setTextColor:GRAY_COLOR];
        [self addSubview:_durationTextLabel];
        
        _durationLabel = [[UILabel alloc] init];
        [_durationLabel setTextAlignment:NSTextAlignmentCenter];
        [_durationLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_durationLabel setTextColor:GRAY_COLOR];
        [self addSubview:_durationLabel];

        _totalTimeTextLabel = [[UILabel alloc] init];
        [_totalTimeTextLabel setTextAlignment:NSTextAlignmentCenter];
        [_totalTimeTextLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_totalTimeTextLabel setTextColor:GRAY_COLOR];
        [self addSubview:_totalTimeTextLabel];
        
        _totalTimeLabel = [[UILabel alloc] init];
        [_totalTimeLabel setTextAlignment:NSTextAlignmentCenter];
        [_totalTimeLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_totalTimeLabel setTextColor:GRAY_COLOR];
        [self addSubview:_totalTimeLabel];
        
        _revenueLabel = [[UILabel alloc] init];
        [_revenueLabel setTextAlignment:NSTextAlignmentCenter];
        [_revenueLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_revenueLabel setTextColor:GRAY_COLOR];
        [self addSubview:_revenueLabel];
        
        _revenueTextLabel = [[UILabel alloc] init];
        [_revenueTextLabel setTextAlignment:NSTextAlignmentCenter];
        [_revenueTextLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_revenueTextLabel setTextColor:GRAY_COLOR];
        [self addSubview:_revenueTextLabel];
        
        _viewCountLabel = [[UILabel alloc] init];
        [_viewCountLabel setTextAlignment:NSTextAlignmentCenter];
        [_viewCountLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_viewCountLabel setTextColor:GRAY_COLOR];
        [self addSubview:_viewCountLabel];
        
        _viewTextLabel = [[UILabel alloc] init];
        [_viewTextLabel setTextAlignment:NSTextAlignmentCenter];
        [_viewTextLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_viewTextLabel setTextColor:GRAY_COLOR];
        [self addSubview:_viewTextLabel];
        
        _likeLabel = [[UILabel alloc] init];
        [_likeLabel setTextAlignment:NSTextAlignmentCenter];
        [_likeLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_likeLabel setTextColor:GRAY_COLOR];
        [self addSubview:_likeLabel];
        
        _likeTextLabel = [[UILabel alloc] init];
        [_likeTextLabel setTextAlignment:NSTextAlignmentCenter];
        [_likeTextLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize]];
        [_likeTextLabel setTextColor:GRAY_COLOR];
        [self addSubview:_likeTextLabel];
        
        _dateLabel = [[UILabel alloc] init];
        [_dateLabel setNumberOfLines:0];
        [_dateLabel setTextAlignment:NSTextAlignmentRight];
        [_dateLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [_dateLabel setFont:[UIFont systemFontOfSize:PostRevenueCellLabelFontSize-1]];
        [_dateLabel setTextColor:GRAY_COLOR];
        [self addSubview:_dateLabel];
        
        UIView* lineView1 = [ThemeManager separaterLine];
        lineView1.frame = CGRectMake(PostRevenueCellPadding, 2*PostRevenueCellUISize/7-1, SCREEN_WIDTH-2*PostRevenueCellPadding, 0.5);
        
        UIView* lineView2 = [ThemeManager separaterLine];
        lineView2.frame = CGRectMake(PostRevenueCellPadding, 3*PostRevenueCellUISize/7-1, SCREEN_WIDTH-2*PostRevenueCellPadding, 0.5);
        
        UIView* lineView3 = [ThemeManager separaterLine];
        lineView3.frame = CGRectMake(PostRevenueCellPadding, 4*PostRevenueCellUISize/7-1, SCREEN_WIDTH-2*PostRevenueCellPadding, 0.5);
        
        UIView* lineView4 = [ThemeManager separaterLine];
        lineView4.frame = CGRectMake(PostRevenueCellPadding, 5*PostRevenueCellUISize/7-1, SCREEN_WIDTH-2*PostRevenueCellPadding, 0.5);
        
        UIView* lineView5 = [ThemeManager separaterLine];
        lineView5.frame = CGRectMake(PostRevenueCellPadding, 6*PostRevenueCellUISize/7-1, SCREEN_WIDTH-2*PostRevenueCellPadding, 0.5);
        
        [self addSubview:lineView1];
        [self addSubview:lineView2];
        [self addSubview:lineView3];
        [self addSubview:lineView4];
        [self addSubview:lineView5];

        [_captionTextLabel setText:LOCALIZE(@"LivestreamCaption")];
        [_totalTimeTextLabel setText:LOCALIZE(@"Total_Duration")];
        [_durationTextLabel setText:LOCALIZE(@"Live_Duration")];
        [_likeTextLabel setText:LOCALIZE(@"Total_Likes")];
        [_revenueTextLabel setText:LOCALIZE(@"Royalties")];
        [_viewTextLabel setText:LOCALIZE(@"Total_Views")];
        
        [self setBackgroundColor:[UIColor whiteColor]];
    }
    
    return self;
}

- (void)setFrame:(CGRect)frame
{
    frame.size.height -= 5;
    [super setFrame:frame];
    
    float fromX =0;
    float nextY = 0;
    float labelWidth = SCREEN_WIDTH/3;
    
    _dateLabel.frame = CGRectMake(fromX, 0, SCREEN_WIDTH-PostRevenueCellPadding, PostRevenueCellUISize/7);
    nextY += _dateLabel.frame.origin.y + _dateLabel.frame.size.height;
    
    _captionTextLabel.frame = CGRectMake(fromX, nextY, labelWidth, PostRevenueCellUISize/7);
    _captionLabel.frame = CGRectMake(_captionTextLabel.frame.origin.x+_captionTextLabel.frame.size.width,nextY,SCREEN_WIDTH-labelWidth, PostRevenueCellUISize/7);
    nextY += PostRevenueCellUISize/7;

    _likeTextLabel.frame = CGRectMake(fromX, nextY, labelWidth, PostRevenueCellUISize/7);
    _likeLabel.frame = CGRectMake(_likeTextLabel.frame.origin.x+_likeTextLabel.frame.size.width,nextY, SCREEN_WIDTH-labelWidth, PostRevenueCellUISize/7);
    nextY += PostRevenueCellUISize/7;

    _durationTextLabel.frame = CGRectMake(fromX, nextY, labelWidth, PostRevenueCellUISize/7);
    _durationLabel.frame = CGRectMake(_durationTextLabel.frame.origin.x+_durationTextLabel.frame.size.width,nextY, SCREEN_WIDTH-labelWidth, PostRevenueCellUISize/7);
    nextY += PostRevenueCellUISize/7;

    
    _viewTextLabel.frame = CGRectMake(fromX, nextY, labelWidth, PostRevenueCellUISize/7);
    _viewCountLabel.frame = CGRectMake(_viewTextLabel.frame.origin.x+_viewTextLabel.frame.size.width,nextY, SCREEN_WIDTH-labelWidth, PostRevenueCellUISize/7);
    nextY += PostRevenueCellUISize/7;
    
    _totalTimeTextLabel.frame = CGRectMake(fromX, nextY, labelWidth, PostRevenueCellUISize/7);
    _totalTimeLabel.frame = CGRectMake(_totalTimeTextLabel.frame.origin.x+_totalTimeTextLabel.frame.size.width,nextY, SCREEN_WIDTH-labelWidth, PostRevenueCellUISize/7);
    nextY += PostRevenueCellUISize/7;

    _revenueTextLabel.frame = CGRectMake(fromX, nextY, labelWidth, PostRevenueCellUISize/7);
    _revenueLabel.frame = CGRectMake(_revenueTextLabel.frame.origin.x+_revenueTextLabel.frame.size.width, nextY, SCREEN_WIDTH-labelWidth, PostRevenueCellUISize/7);
    
}

- (void)setLivestream:(LiveStreamObject *)livestream
{
    _livestream = livestream;
    
    if([_livestream.caption isEqualToString:@""]){
        [_captionLabel setText:LOCALIZE(@"Untitled")];
    }else{
        [_captionLabel setText:_livestream.caption];
    }
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    
    NSDateFormatter* timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm:ss"];
    
    NSDate* date = [[NSDate alloc] initWithTimeIntervalSince1970:_livestream.beginTime];
    _dateLabel.text = [dateFormat stringFromDate:date];

    _durationLabel.text = [SINGLETON getTimeFormatBySecond:_livestream.duration];

    _totalTimeLabel.text = [SINGLETON getTimeFormatBySecond:_livestream.totalViewTime];
    
    _viewCountLabel.text = [NSString stringWithFormat:@"%@", [SINGLETON getCountNumNormalization:_livestream.viewerCount]];
    [_revenueLabel setText:[NSString stringWithFormat:LOCALIZE(@"revenues_with_type"),[SINGLETON getCurrencyRate]*_livestream.revenue,[SINGLETON getCurrencyType]]];
    [_likeLabel setText:[NSString stringWithFormat:@"%d",_livestream.receivedLikeCount]];
    
}


@end
