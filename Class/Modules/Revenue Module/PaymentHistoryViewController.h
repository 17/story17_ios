//
//  PaymentHistoryViewController.h
//  story17
//
//  Created by POPO Chen on 5/18/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentHistoryViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) UITableView* mainTableView;
@property (nonatomic,strong) NSMutableArray* paymentArray;
@property (nonatomic,strong)  NSMutableArray *selectedIndexes;
@property (nonatomic,strong)  UIImageView *noDataImageView;


@end
