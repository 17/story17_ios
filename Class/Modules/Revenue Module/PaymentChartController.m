//
//  PaymentChartController.m
//  Story17
//
//  Created by POPO on 2015/5/15.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "PaymentChartController.h"
#import "PaymentChartCell.h"
#import "Constant.h"

//#define ButtonHeight 40
#define chartCellIdentity  @"chartCellIdentity"

#define btnWidth SCREEN_WIDTH/3
#define scrollViewHeight SCREEN_HEIGHT-TAB_BAR_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT

@implementation PaymentChartController

- (void)viewDidLoad
{
    [self addCustomNavigationBackButton];
    [self configureViewForIOS7];
    self.title = LOCALIZE(@"user_revenue_report_data");

    if (_mainScrollView == nil)
    {
        
        _viewMode = WEEKLY_REVENUE;
        
        _weeklyselected = CREATE_MUTABLE_ARRAY;
        _monthlyselected = CREATE_MUTABLE_ARRAY;
        _yearselected = CREATE_MUTABLE_ARRAY;
        _selectedArray = @[_weeklyselected,_monthlyselected,_yearselected];
        
        _mainView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];

        /* Button Container */
        
        _weeklyButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, btnWidth, NAVI_BAR_HEIGHT)];
        _weeklyButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_weeklyButton setTitle:LOCALIZE(@"weekly_revenue") forState:UIControlStateNormal];
        [_weeklyButton setTitleColor:GRAY_COLOR forState:UIControlStateNormal];
        [_weeklyButton setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
        [_weeklyButton addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];
        [_weeklyButton setSelected:YES];
        
        _monthlyButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/3, 0, btnWidth, NAVI_BAR_HEIGHT)];
        _monthlyButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_monthlyButton setTitle:LOCALIZE(@"monthly_revenue") forState:UIControlStateNormal];
        [_monthlyButton setTitleColor:GRAY_COLOR forState:UIControlStateNormal];
        [_monthlyButton setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
        [_monthlyButton addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];
        
        _yearBotton = [[UIButton alloc] initWithFrame:CGRectMake(2*SCREEN_WIDTH/3, 0, btnWidth, NAVI_BAR_HEIGHT)];
        _yearBotton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_yearBotton setTitle:LOCALIZE(@"yearly_revenue") forState:UIControlStateNormal];
        [_yearBotton setTitleColor:GRAY_COLOR forState:UIControlStateNormal];
        [_yearBotton setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
        [_yearBotton addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];
        
        _btnArray = @[_weeklyButton,_monthlyButton,_yearBotton];
        
        _modeSelectorContainer = [ThemeManager maintabBackgroundImageView];
        _modeSelectorContainer.frame = CGRectMake(0, 0, SCREEN_WIDTH, NAVI_BAR_HEIGHT);
        _modeSelectorContainer.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
        [_modeSelectorContainer setBackgroundColor:WHITE_COLOR];
        
        _modeIndicatorImageView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"bar")];
        _modeIndicatorImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH/3, 3);
        _modeIndicatorImageView.layer.masksToBounds = YES;
        _modeIndicatorImageView.center = CGPointMake(SCREEN_WIDTH/6, NAVI_BAR_HEIGHT-3);
        [_modeSelectorContainer addSubview:_modeIndicatorImageView];
        
        [_modeSelectorContainer addSubview:_weeklyButton];
        [_modeSelectorContainer addSubview:_monthlyButton];
        [_modeSelectorContainer addSubview:_yearBotton];
        
        /* MainTable Settings */
        _mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, scrollViewHeight)];
        [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH*3, _mainScrollView.frame.size.height)];
        [self scrollViewSettings:_mainScrollView];
        
        /* CollectionView Setting */
        
        _weeklyTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, scrollViewHeight) style:UITableViewStylePlain];
        _monthlyTableView = [[UITableView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, scrollViewHeight) style:UITableViewStylePlain];
        _yearTableView = [[UITableView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*2, 0, SCREEN_WIDTH, scrollViewHeight) style:UITableViewStylePlain];
        
        UIView* weekDateView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
        [weekDateView setBackgroundColor:[UIColor clearColor]];
        _weekDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, SCREEN_WIDTH-20, 30)];
        [weekDateView addSubview:_weekDateLabel];
        [self dateLabelSettings:_weekDateLabel];
        _weeklyTableView.tableHeaderView = weekDateView;
        
        UIView* monthDateView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
        [monthDateView setBackgroundColor:[UIColor clearColor]];
        _monthDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, SCREEN_WIDTH-20, 30)];
        [monthDateView addSubview:_monthDateLabel];
        [self dateLabelSettings:_monthDateLabel];
        _monthlyTableView.tableHeaderView = monthDateView;
        
        UIView* yearDateView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
        [yearDateView setBackgroundColor:[UIColor clearColor]];
        _yearDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, SCREEN_WIDTH-20, 30)];
        [yearDateView addSubview:_yearDateLabel];
        [self dateLabelSettings:_yearDateLabel];
        _yearTableView.tableHeaderView = yearDateView;
        
        _tableViewArray = @[_weeklyTableView,_monthlyTableView,_yearTableView];
        
        [self tableViewSettings:_weeklyTableView];
        [self tableViewSettings:_monthlyTableView];
        [self tableViewSettings:_yearTableView];
        
        [_mainScrollView addSubview:_weeklyTableView];
        [_mainScrollView addSubview:_monthlyTableView];
        [_mainScrollView addSubview:_yearTableView];
        
        [_mainView addSubview:_mainScrollView];
        [_mainView addSubview:_modeSelectorContainer];
        [self.view addSubview:_mainView];

    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (![REVENUE_MANAGER eof] )
    {
        //pull data
        [DIALOG_MANAGER showLoadingView];
        [self pullData:YES];
    }else{
        [self loadData];
        [self reloadUI];
    }
}

-(void)reloadUI
{
    [_weeklyTableView reloadData];
    [_monthlyTableView reloadData];
    [_yearTableView reloadData];
    
    [self reloadDate];
}

-(void)reloadDate
{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSDate* date = [[NSDate alloc] initWithTimeIntervalSince1970:CURRENT_TIMESTAMP-86400];
    NSDate *weekDate = [date dateByAddingTimeInterval:86400*-6];
    _weekDateLabel.text = [NSString stringWithFormat:@"%@-%@",[dateFormat stringFromDate:weekDate],[dateFormat stringFromDate:date]];
    
    NSDate *monthDate = [date dateByAddingTimeInterval:86400*-30];
    _monthDateLabel.text = [NSString stringWithFormat:@"%@-%@",[dateFormat stringFromDate:monthDate],[dateFormat stringFromDate:date]];

    [dateFormat setDateFormat:@"MM/yyyy"];
    NSDate *yearDate = [date dateByAddingTimeInterval:86400*-365];
    _yearDateLabel.text = [NSString stringWithFormat:@"%@-%@",[dateFormat stringFromDate:yearDate],[dateFormat stringFromDate:date]];
}

-(void)loadData
{
    _revenue7daysArray = [REVENUE_MANAGER getDaysRevenue:7 beforeTime:CURRENT_TIMESTAMP+86400];
    _revenue7daysBeforeArray = [REVENUE_MANAGER getDaysRevenue:7 beforeTime:CURRENT_TIMESTAMP-6*86400];
    
    _revenue30daysArray = [REVENUE_MANAGER getDaysRevenue:30 beforeTime:CURRENT_TIMESTAMP+86400];
   
    _revenue30daysBeforeArray =[REVENUE_MANAGER getDaysRevenue:30 beforeTime:CURRENT_TIMESTAMP-29*86400];
    
    _revenue12mArray = [REVENUE_MANAGER get12MonthesRevenue];
    _revenue12mBeforeArray = [REVENUE_MANAGER getBefore12MonthesRevenue];

}

-(void)pullData:(BOOL)refresh
{
    if ([REVENUE_MANAGER eof] || [REVENUE_MANAGER isLoading])
        return;
    
    [DIALOG_MANAGER showLoadingView];
    [REVENUE_MANAGER pullDataWithCallback:refresh withCallback:^(BOOL success, NSArray* dataArray){
        if (!success)
            return;
        
        if (![[SINGLETON revenueDataManager] eof])
            [self pullData:NO];
        else
        {
            
            [self loadData];
            [self reloadUI];

            [DIALOG_MANAGER hideLoadingView];
            
        }
    }];
}


#pragma mark - Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PaymentChartCell* cell = [tableView dequeueReusableCellWithIdentifier:chartCellIdentity forIndexPath:indexPath];
    if (cell== nil) {
        cell = [[PaymentChartCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:chartCellIdentity];
    }
    cell.layer.masksToBounds = YES;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    NSArray* revenueArray;
    NSArray* beforeRevenueArray;
    
    if([tableView isEqual:_weeklyTableView]){
        revenueArray =_revenue7daysArray;
        beforeRevenueArray =_revenue7daysBeforeArray;
    }else if([tableView isEqual:_monthlyTableView]){
        revenueArray =_revenue30daysArray;
        beforeRevenueArray =_revenue30daysBeforeArray;
    }else if([tableView isEqual:_yearTableView]){
        revenueArray =_revenue12mArray;
        beforeRevenueArray =_revenue12mBeforeArray;
    }
    
    if(indexPath.section == 0 ){
        [cell reloadCellWithRevenue:@"Revenue" andArray:revenueArray compareTo:beforeRevenueArray];
    }else{
        [cell reloadCellWithViews:@"Views" andArray:revenueArray compareTo:beforeRevenueArray];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([self cellIsSelected:indexPath withTableView:tableView ]) {
        if([tableView isEqual:_monthlyTableView]){
            return [PaymentChartCell getFullCellHeight]-40.0f;
        }else{
            return [PaymentChartCell getFullCellHeight];
        }
    }
    return [PaymentChartCell getTitleHeight];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    PaymentChartCell *cell = (PaymentChartCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    NSMutableArray* selectedArray;
    
    if([tableView isEqual:_weeklyTableView]){
        selectedArray = _weeklyselected;
    }else if([tableView isEqual:_monthlyTableView]){
        selectedArray = _monthlyselected;
    }else if([tableView isEqual:_yearTableView]){
        selectedArray = _yearselected;
    }
    
    if([selectedArray containsObject:indexPath]){
        [selectedArray removeObject:indexPath];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }else{
        [cell chartAnimation];
        [selectedArray addObject:indexPath];
    }
    
    [tableView beginUpdates];
    [tableView endUpdates];
}

- (BOOL)cellIsSelected:(NSIndexPath *)indexPath withTableView:(UITableView*)tableView {
    
    if([tableView isEqual:_weeklyTableView]){
        if([_weeklyselected containsObject:indexPath]){
            return YES;
        }
    }else if([tableView isEqual:_monthlyTableView]){
        if([_monthlyselected containsObject:indexPath]){
            return YES;
        }
    }else if([tableView isEqual:_yearTableView]){
        if([_yearselected containsObject:indexPath]){
            return YES;
        }
    }
    return NO;
}

#pragma mark UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // to avoid refresh action go this way
    if([_tableViewArray containsObject:scrollView]) return;
    if(scrollView.contentOffset.x==0) {
        [self switchToMode:WEEKLY_REVENUE animated:YES];
    }else if(scrollView.contentOffset.x==SCREEN_WIDTH) {
        [self switchToMode:MONTHLY_REVENUE animated:YES];
    }else if(scrollView.contentOffset.x==SCREEN_WIDTH*2) {
        [self switchToMode:YEARLY_REVENUE animated:YES];
    }
}

/* Change Mode Animation */

-(void)changeModeAction:(id)sender
{
    if([sender isEqual:_weeklyButton]){
        [self switchToMode:WEEKLY_REVENUE animated:YES];
    }else if ([sender isEqual:_monthlyButton]){
        [self switchToMode:MONTHLY_REVENUE animated:YES];
    }else if([sender isEqual:_yearBotton]){
        [self switchToMode:YEARLY_REVENUE animated:YES];
    }
}

-(void) switchToMode:(int)mode animated:(BOOL) animated
{
    if(_viewMode==mode) {
        return;
    }
    
    if(mode==WEEKLY_REVENUE){
        [_weeklyButton setSelected:YES];
        [_monthlyButton setSelected:NO];
        [_yearBotton setSelected:NO];
    }else if (mode==MONTHLY_REVENUE){
        [_weeklyButton setSelected:NO];
        [_monthlyButton setSelected:YES];
        [_yearBotton setSelected:NO];
    }else if(mode==YEARLY_REVENUE){
        [_weeklyButton setSelected:NO];
        [_monthlyButton setSelected:NO];
        [_yearBotton setSelected:YES];
    }
    
    _viewMode = mode;
    [self reloadUIAnimated:animated];
    
}

-(void) reloadUIAnimated:(BOOL)animated
{
    for (int mode=0; mode < 3; mode++) {
        UIButton* modeButton = _btnArray[mode];
        UIScrollView *tableView = _tableViewArray[mode];
        
        if(mode == _viewMode) {
 
            tableView.scrollsToTop = YES;
            modeButton.userInteractionEnabled = NO;
            
            [_mainScrollView scrollRectToVisible:CGRectMake(SCREEN_WIDTH*mode, 0, SCREEN_WIDTH, _mainScrollView.frame.size.height) animated:YES];
            
            if(animated) {
                [UIView animateWithDuration:0.3 animations:^{
                    _modeIndicatorImageView.center = CGPointMake((SCREEN_WIDTH/6)+(SCREEN_WIDTH/3)*mode, NAVI_BAR_HEIGHT-1);
                }];
            } else {
                _modeIndicatorImageView.center = CGPointMake((SCREEN_WIDTH/6)+(SCREEN_WIDTH/3)*mode, NAVI_BAR_HEIGHT-1);
            }
            
        }else {

            tableView.scrollsToTop = NO;
            modeButton.userInteractionEnabled = YES;
        }
    }
}


/* UI Settings */
-(void) tableViewSettings:(UITableView*)tableView
{
    tableView.delegate = self;
    tableView.dataSource = self;
    [tableView setBackgroundColor:LIGHT_BG_COLOR];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tableView registerClass:[PaymentChartCell class] forCellReuseIdentifier:chartCellIdentity];

}

-(void) scrollViewSettings:(UIScrollView*)scrollView
{
    scrollView.delegate = self;
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = YES;
    scrollView.delaysContentTouches = YES;
    scrollView.canCancelContentTouches = NO;
    scrollView.scrollsToTop = NO;
}

-(void) dateLabelSettings:(UILabel*)label
{
    label.font = SYSTEM_FONT_WITH_SIZE(14);
    [label setTextColor:GRAY_COLOR];
}

@end
