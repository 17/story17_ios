//
//  RevenueObject.m
//  Story17
//
//  Created by POPO on 2015/5/13.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "RevenueObject.h"

@implementation RevenueObject



+ (RevenueObject *)getObjectWithDict:(NSDictionary *)dict
{
    RevenueObject* obj = [[RevenueObject alloc] init];
    obj.year = [dict[@"year"] intValue];
    obj.month = [dict[@"month"] intValue];
    obj.day = [dict[@"day"] intValue];
    obj.totalViews = [dict[@"totalViews"] intValue];
    obj.revenue = [dict[@"revenue"] floatValue];
    obj.timestamp = [dict[@"timestamp"] intValue];
    obj.payedStatus = [dict[@"payed"] intValue];
    
    return obj;
}

@end
