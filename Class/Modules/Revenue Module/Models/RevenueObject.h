//
//  RevenueObject.h
//  Story17
//
//  Created by POPO on 2015/5/13.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RevenueObject : NSObject

@property(nonatomic)int year;
@property(nonatomic)int month;
@property(nonatomic)int day;
@property(nonatomic)int totalViews;
@property(nonatomic)float revenue;
@property(nonatomic)int timestamp;
@property(nonatomic)int payedStatus;

+ (RevenueObject *)getObjectWithDict:(NSDictionary *)dict;

@end