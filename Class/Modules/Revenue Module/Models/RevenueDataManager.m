//
//  RevenueDataManager.m
//  Story17
//
//  Created by POPO on 2015/5/14.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "RevenueDataManager.h"
#import "Constant.h"

#define PullDays 1000
#define SecondsPerDay 86400

const int PullDaysOfSeconds = PullDays * SecondsPerDay;

@implementation RevenueDataManager

@synthesize eof = _eof;
@synthesize totalRevenue = _totalRevenue;
@synthesize unpayedRevenue = _unpayedRevenue;
@synthesize lastMonthRevenue = _lastMonthRevenue;
@synthesize todayRevenue = _todayRevenue;

@synthesize cachetotalRevenue = _cachetotalRevenue;
@synthesize cacheunpayedRevenue = _cacheunpayedRevenue;
@synthesize cachelastMonthRevenue = _cachelastMonthRevenue;
@synthesize cachetodayRevenue = _cachetodayRevenue;

@synthesize isLoading;

- (id)init
{
    self = [super init];
    if (self)
    {
        _rawData = [[NSMutableArray alloc] init];
        _preRawData = CREATE_MUTABLE_ARRAY;
        _eof = false;
        isLoading = false;
        _totalRevenue = 0;
        _unpayedRevenue = 0;
        _lastMonthRevenue = 0;
        _todayRevenue = 0;
    
        _cachetotalRevenue = 0;
        _cacheunpayedRevenue = 0;
        _cachelastMonthRevenue = 0;
        _cachetodayRevenue = 0;
    }
    
    return self;
}

- (int)dataCount
{
    return (int)[_rawData count];
}

- (NSArray *)rawData
{
    return _rawData;
}

#pragma mark - PullData
- (void)pullData:(BOOL)refresh
{
    [self pullDataWithCallback:refresh withCallback:^(BOOL success, NSArray* dataArray){
        if (!success)
            return;
        
        if (![self eof])
            [self pullData:NO];
    }];
}

- (void)pullDataWithCallback:(BOOL)refresh withCallback:(void(^)(BOOL success, NSArray* dataArray))callback
{
    if(isLoading)
        return;
    
    if(refresh){
        [_preRawData removeAllObjects];

        _cachetotalRevenue = 0;
        _cacheunpayedRevenue = 0;
        _cachelastMonthRevenue = 0;
        _cachetodayRevenue = 0;

    }
    
    isLoading = true;
    
    NSTimeInterval lastTime = CURRENT_TIMESTAMP + [SINGLETON getTimezone];  // current local time
    
    if ([_preRawData count] > 0)
    {
        RevenueObject* obj = [_preRawData lastObject];
        lastTime = obj.timestamp;
    }
    
    NSTimeInterval startTime = lastTime - PullDaysOfSeconds;
//    DLog(@"startTime:%f lastTime:%f",startTime,lastTime);

    [API_MANAGER getRevenueReport:startTime maxTimestamp:lastTime withCompletion:^(BOOL success, NSArray* dataArray){
        
        isLoading = false;

        if (!success)
        {
            [DIALOG_MANAGER showNetworkFailToast];
            callback(success, _rawData);
            return;
        }
        
        if ([dataArray count] == 0)
        {
            _eof = true;
            _rawData = [_preRawData mutableCopy];
            
            _totalRevenue = _cachetotalRevenue;
            _unpayedRevenue = _cacheunpayedRevenue;
            _lastMonthRevenue = _cachelastMonthRevenue;
            _todayRevenue = _cachetodayRevenue;

            if(_todayRevenue==0){
            
            }
            [self addNULLRevenueData];
        }
        else
        {
//            int currentLocalTime = CURRENT_TIMESTAMP+ [SINGLETON getTimezone];
//            int currentLocalTime = CURRENT_TIMESTAMP - 25200;
            
            for (RevenueObject* obj in dataArray){
                
                if(obj.payedStatus==0){
                    _cacheunpayedRevenue += obj.revenue;
                }
                
//                if(obj.timestamp<CURRENT_TIMESTAMP && obj.timestamp>CURRENT_TIMESTAMP-30*86400){
//                    _cachelastMonthRevenue += obj.revenue;
//                }
                
                if( [self isOnSameMonth:obj.timestamp-[SINGLETON getTimezone] with:CURRENT_TIMESTAMP]){
                    _cachelastMonthRevenue += obj.revenue;
                }
                
                
                _cachetotalRevenue += obj.revenue;
//                DLog(@"revenue Time:%d  Current Time:%d",obj.timestamp,CURRENT_TIMESTAMP);
                
                /* TRICKT :  obj.timestamp-[SINGLETON getTimezone] normalize to Realt unixTime */
                if( [self isOnSameDate:obj.timestamp-[SINGLETON getTimezone] with:CURRENT_TIMESTAMP]){
                    _cachetodayRevenue = obj.revenue;
                }
            }
            [_preRawData addObjectsFromArray:dataArray];
            [self pullData:NO];
        }
        callback(success, _preRawData);

    }];
}

#pragma mark - Getter Method From RawData

- (NSArray *)getDaysRevenue:(int)days beforeTime:(int)beforeTime
{
    
    NSMutableArray* revenueData = [[NSMutableArray alloc] init];
    int totalCount = self.dataCount;
    
    
    RevenueObject* pre = [self getEmptyRevenue:beforeTime];
    
    for(int i =0;i<totalCount;i++){
        
        if([revenueData count]==days){
            return  [[revenueData reverseObjectEnumerator] allObjects];
        }
        
        RevenueObject* now = [_rawData objectAtIndex:i];
        
        if([self isOnSameDate:pre.timestamp with:now.timestamp]){
           
            [revenueData addObject:now];
            pre = now;
        
        }else{
            
            int hole = (pre.timestamp-now.timestamp)/86400;
            
            for(int j=1;j<hole;j++){
                
                RevenueObject* empty = [self getEmptyRevenue:pre.timestamp-86400];
                [revenueData addObject:empty];
                pre = empty;

                if([revenueData count]==days){
                    return  [[revenueData reverseObjectEnumerator] allObjects];
                }
            }
            
            if(hole>=0){
                [revenueData addObject:now];
                pre = now;
            }
        }
    }
    
    // Insert 0 if raw data less than 7 days
    if([revenueData count]<days){
        RevenueObject* preRevenue;
        if([revenueData count]==0){
            preRevenue = [self getEmptyRevenue:beforeTime+86400];
        }else{
            preRevenue = revenueData[[revenueData count]-1];
        }
        int remainCount = days- (int)[revenueData count];
        
        for(int i=0;i<remainCount;i++){
            RevenueObject* new = [self getEmptyRevenue:preRevenue.timestamp-86400];
            [revenueData addObject:new];
            preRevenue = new;
        }
    }
    
    return  [[revenueData reverseObjectEnumerator] allObjects];
}


- (NSArray *)get12MonthesRevenue
{
    
    NSMutableArray* monthlyData = [[self getMonthly] mutableCopy];
    
    RevenueObject* lastMonthObj;
    if([monthlyData count]==0){
        lastMonthObj = [self getEmptyRevenue:CURRENT_TIMESTAMP+86400*30];
    }else{
        lastMonthObj = monthlyData[[monthlyData count]-1];
    }

    // more than 12 monthes
    if([monthlyData count]>=12){
        NSMutableArray* monthData = CREATE_MUTABLE_ARRAY;
        for(int i=0;i<12;i++){
            [monthData addObject:[monthlyData objectAtIndex:i]];
        }
        return monthData;
    }
    
    // data not enough
    int count = 12-(int)[monthlyData count];
    for(int i=0;i<count;i++){

        RevenueObject* monthObj = [RevenueObject new];
        int year = lastMonthObj.year;
        int month = lastMonthObj.month;
        if(month-1==0){
            year-=1;
            month = 12;
        }else{
            month -=1;
        }
        
        monthObj.year = year;
        monthObj.month = month;
        monthObj.day = lastMonthObj.day;
        
        monthObj.totalViews = 0;
        monthObj.revenue = 0;
        monthObj.timestamp = 0;
        
        [monthlyData addObject:monthObj];
        lastMonthObj = monthObj;
    }
    
    return [[monthlyData reverseObjectEnumerator] allObjects];
}

- (NSArray *)getBefore12MonthesRevenue
{
    NSMutableArray* monthlyData = [[self getMonthly] mutableCopy];  //monthly data
    NSMutableArray* monthData = CREATE_MUTABLE_ARRAY;
    
    // more than 24 monthes
    if([monthlyData count]>=24){
        for(int i=11;i<24;i++){
            [monthData addObject:[monthlyData objectAtIndex:i]];
        }
        return monthData;
    }
    
    // less than 12 monthes
    RevenueObject* lastMonthObj = [self getEmptyRevenue:CURRENT_TIMESTAMP];
    
    if([monthlyData count]<=12){
        
        for(int i=0;i<12;i++){
        
            if(i<[monthData count]){
                [monthData addObject:monthlyData[i]];
                lastMonthObj = monthlyData[i];
            }else{
                RevenueObject* monthObj = [[RevenueObject alloc] init];
                int year = lastMonthObj.year;
                int month = lastMonthObj.month-1;
                if(month-1<0){
                    year-=1;
                    month = 12;
                }else{
                    month -=1;
                }
                monthObj.year = year;
                monthObj.month = month;
                monthObj.day = lastMonthObj.day;
                
                monthObj.totalViews = 0;
                monthObj.revenue = 0;
                monthObj.timestamp = 0;
                lastMonthObj = monthObj;
                [monthData addObject:monthObj];
                
            }
        }
        return monthData;
    }
    
    // 13 - 24 monthes
    lastMonthObj = [monthlyData objectAtIndex:12];
    for(int i=0;i<12;i++){
        if(i < [monthlyData count]-12){
            
            // insert last monthlyData
            [monthData addObject: lastMonthObj];
            lastMonthObj = [monthlyData objectAtIndex:i+13];
            
        }else{
            
            // insert zero data
            RevenueObject* monthObj = [[RevenueObject alloc] init];
            int year = lastMonthObj.year;
            int month = lastMonthObj.month;
            if(month-1==0){
                year-=1;
                month = 12;
            }else{
                month -=1;
            }
            monthObj.year = year;
            monthObj.month = month;
            monthObj.day = lastMonthObj.day;
            
            monthObj.totalViews = 0;
            monthObj.revenue = 0;
            monthObj.timestamp = 0;
            lastMonthObj = monthObj;
            [monthData addObject:monthObj];
            
        }
    }
    
    return [[monthData reverseObjectEnumerator] allObjects];
}


- (NSArray *)getMonthly
{
    NSMutableArray* monthlyData = [[NSMutableArray alloc] init];
    RevenueObject* monthObj = [self getEmptyRevenue:CURRENT_TIMESTAMP];
    
    //list data
    int totalCount = self.dataCount;
    
    for (int i = 0; i < totalCount; ++i)
    {
        
        RevenueObject* now = [_rawData objectAtIndex:i];
        
        if([self isOnSameMonth:monthObj.timestamp with:now.timestamp]){
            
            monthObj.revenue += now.revenue;
            monthObj.totalViews += now.totalViews;
            
        }else{
            [monthlyData addObject:monthObj];
            monthObj = [self getEmptyRevenue:now.timestamp];
            monthObj.revenue = now.revenue;
            monthObj.totalViews = now.totalViews;
        }
        
        if (i == totalCount - 1)  //  the last one
        {
            [monthlyData addObject:monthObj];
        }

    }
    
    return monthlyData;
}


#pragma mark - Empty Object

-(RevenueObject*)getEmptyRevenue:(int)timestamp
{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy"];
    NSDate* date = [[NSDate alloc] initWithTimeIntervalSince1970:timestamp];
    
    RevenueObject* rev = [RevenueObject new];
    rev.timestamp = timestamp;
    rev.year = [[dateFormat stringFromDate:date] intValue];
    [dateFormat setDateFormat:@"MM"];
    rev.month = [[dateFormat stringFromDate:date] intValue];
    [dateFormat setDateFormat:@"dd"];
    rev.day = [[dateFormat stringFromDate:date] intValue];
    rev.totalViews = 0;
    rev.revenue = 0;
    
    rev.payedStatus=1;
    return rev;
}

- (void)addNULLRevenueData
{
    int count = (int)[_rawData count];
    if (count == 0)
        return;
    
    NSMutableArray* _newRawData = [[NSMutableArray alloc] init];
    NSDateFormatter* format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy/MM/dd"];
    
    NSDate* lastDate = nil;
    NSDate* nowDate = nil;
    for (int i = 0; i < count; ++i)
    {
        RevenueObject* obj = [_rawData objectAtIndex:i];
        if (lastDate == nil)
            lastDate = [format dateFromString:[NSString stringWithFormat:@"%d/%d/%d", obj.year, obj.month, obj.day]];
        
        if (nowDate == nil)
            nowDate = [format dateFromString:[NSString stringWithFormat:@"%d/%d/%d", obj.year, obj.month, obj.day]];
        
        if ([nowDate isEqualToDate:lastDate])
        {
            [_newRawData addObject:obj];
            nowDate = nil;
        }
        else
        {
            NSTimeInterval diff = [lastDate timeIntervalSince1970] - [nowDate timeIntervalSince1970];
            if (diff <= 86400)
            {
                [_newRawData addObject:obj];
                lastDate = nowDate;
                nowDate = nil;
            }
            else
            {
                nowDate = [lastDate dateByAddingDays:-1];
                RevenueObject* _nullObj = [[RevenueObject alloc] init];
                _nullObj.year = (int)nowDate.year;
                _nullObj.month = (int)nowDate.month;
                _nullObj.day = (int)nowDate.day;
                
                [_newRawData addObject:_nullObj];
                nowDate = nil;
            }
        }
    }
}

/* Date Judgement */
-(BOOL)isOnSameDate:(int)timestamp with:(int)otherTimestamp
{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSDate* date1 = [[NSDate alloc] initWithTimeIntervalSince1970:timestamp];
    NSDate* date2 = [[NSDate alloc] initWithTimeIntervalSince1970:otherTimestamp];
//    DLog(@"timestamp:%d otherTimestamp:%d",timestamp,otherTimestamp);
//    DLog(@"date1:%@  date2:%@",[dateFormat stringFromDate:date1],[dateFormat stringFromDate:date2]);
    if(![[dateFormat stringFromDate:date1] isEqualToString:[dateFormat stringFromDate:date2]]){
        return NO;
    }
    return YES;
}


-(BOOL)isOnSameMonth:(int)timestamp with:(int)otherTimestamp
{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/yyyy"];
    NSDate* date1 = [[NSDate alloc] initWithTimeIntervalSince1970:timestamp];
    NSDate* date2 = [[NSDate alloc] initWithTimeIntervalSince1970:otherTimestamp];
    
    if(![[dateFormat stringFromDate:date1] isEqualToString:[dateFormat stringFromDate:date2]]){
        return NO;
    }
    return YES;
}

@end
