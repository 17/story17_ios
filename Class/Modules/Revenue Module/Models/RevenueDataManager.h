//
//  RevenueDataManager.h
//  Story17
//
//  Created by POPO on 2015/5/14.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RevenueObject.h"

@interface RevenueDataManager : NSObject
{
@private
    NSMutableArray* _rawData;
    NSMutableArray* _preRawData;

}

@property(nonatomic, readonly)int dataCount;
@property(nonatomic, readonly)bool eof;
@property(nonatomic, readonly)bool isLoading;

@property(nonatomic)double totalRevenue;
@property(nonatomic)double unpayedRevenue;
@property(nonatomic)double lastMonthRevenue;
@property(nonatomic)double todayRevenue;

@property(nonatomic)double cachetotalRevenue;
@property(nonatomic)double cacheunpayedRevenue;
@property(nonatomic)double cachelastMonthRevenue;
@property(nonatomic)double cachetodayRevenue;


@property(nonatomic, readonly)NSArray* rawData;

- (void)pullDataWithCallback:(BOOL)refresh withCallback:(void(^)(BOOL success, NSArray* dataArray))callback;
- (void)pullData:(BOOL)refresh;

- (NSArray *)getMonthly;
- (NSArray *)getDaysRevenue:(int)days beforeTime:(int)beforeTime;

- (NSArray *)get12MonthesRevenue;
- (NSArray *)getBefore12MonthesRevenue;


@end
