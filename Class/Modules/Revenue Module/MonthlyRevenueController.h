//
//  MonthlyRevenueController.h
//  Story17
//
//  Created by POPO on 2015/5/13.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonthlyRevenueController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
@private
    UITableView* _tableView;
    
    NSArray* _monthlyData;
}

@end
