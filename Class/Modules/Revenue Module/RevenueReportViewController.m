//
//  RevenueReportViewController.m
//  Story17
//
//  Created by POPO on 2015/5/13.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "RevenueReportViewController.h"
#import "PostRevenueController.h"
#import "MonthlyRevenueController.h"
#import "PaymentChartController.h"
#import "PaymentInfoViewControllerV2.h"
#import "PaymentHistoryViewController.h"
#import "PaymentWebViewController.h"
#import "LivestreamRevenueViewController.h"
#import "GiftReceiveViewController.h"

@implementation RevenueReportViewController

#define topMargin 5;
#define mainLabelHeight 45
#define arrowLabelHeight 45

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    self.title = LOCALIZE(@"Revenue_report");
    
    if(_mainScrollView==nil){
        
        _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
        [_mainScrollView setBackgroundColor:LIGHT_BG_COLOR];
        
        _warningLabel = [UILabel new];
        [_warningLabel setBackgroundColor:[UIColor colorWithRed:228.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0]];
        [_warningLabel setTextColor:BLACK_COLOR];
        _warningLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        [_warningLabel setText:LOCALIZE(@"havent_fill_info")];
        _warningLabel.textAlignment = NSTextAlignmentCenter;
        [_warningLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickDetail:)]];
        _warningLabel.userInteractionEnabled = YES;
        
        _paymentWarningLabel = [UILabel new];
        [_paymentWarningLabel setBackgroundColor:[UIColor colorWithRed:228.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0]];
        [_paymentWarningLabel setTextColor:BLACK_COLOR];
        _paymentWarningLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        [_paymentWarningLabel setText:LOCALIZE(@"payment_withdraw")];
        _paymentWarningLabel.textAlignment = NSTextAlignmentCenter;
        [_paymentWarningLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickDetail:)]];
        _paymentWarningLabel.userInteractionEnabled = YES;
        
        
        UIImageView* arrowImageView = [[UIImageView alloc]initWithImage:IMAGE_FROM_BUNDLE(@"nav_arrow_gray_next")];
        arrowImageView.frame = CGRectMake(SCREEN_WIDTH-20, 11, 12, 10);
        [_paymentWarningLabel addSubview:arrowImageView];
        
        /* topView  */
        _lifeTimeTopView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 75)];
//        [_lifeTimeTopView setImage:IMAGE_FROM_BUNDLE(@"banner1")];
//        [_lifeTimeTopView setAnimationImages:@[IMAGE_FROM_BUNDLE(@"banner"),IMAGE_FROM_BUNDLE(@"banner1")]];
//        _lifeTimeTopView.animationDuration = 0.3;
//        [_lifeTimeTopView startAnimating];

        _lifeTimeTopView.contentMode = UIViewContentModeScaleAspectFit;
        
        _lifeTimeTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, SCREEN_WIDTH, 25)];
        [_lifeTimeTitle setText:LOCALIZE(@"user_revenue_report_total")];

        _lifeTimeRevinue = [[UILabel alloc]initWithFrame:CGRectMake(0, 35, SCREEN_WIDTH, 30)];
        
        [_lifeTimeTopView setBackgroundColor:WHITE_COLOR];
        
        _lifeTimeTitle.textAlignment = NSTextAlignmentCenter;
        _lifeTimeTitle.font = SYSTEM_FONT_WITH_SIZE(14);
        [_lifeTimeTitle setTextColor:DARK_GRAY_COLOR];
        _lifeTimeRevinue.textAlignment = NSTextAlignmentCenter;
        _lifeTimeRevinue.font = BOLD_FONT_WITH_SIZE(20);
        [_lifeTimeRevinue setTextColor:DARK_GRAY_COLOR];

        [_lifeTimeTopView addSubview:_lifeTimeTitle];
        [_lifeTimeTopView addSubview:_lifeTimeRevinue];
        
        [_lifeTimeTopView addSubview:_lifeTimeTitle];
        [_lifeTimeTopView addSubview:_lifeTimeRevinue];
        
        
        [_mainScrollView addSubview:_lifeTimeTopView];
        
        _todayRevenueView = [ArrowSwitchLabelView new];
        [_todayRevenueView styleSettingsWithMode:WITH_SUBTITLE];
        [_todayRevenueView.titleLabel setText:LOCALIZE(@"user_revenue_report_today")];
        [_mainScrollView addSubview:_todayRevenueView];
        
        _monthRevenueView =[ArrowSwitchLabelView new];
        [_monthRevenueView styleSettingsWithMode:WITH_SUBTITLE];
        [_monthRevenueView.titleLabel setText:LOCALIZE(@"user_revenue_report_month")];
        [_mainScrollView addSubview:_monthRevenueView];
        
        _unpayedRevenueView = [ArrowSwitchLabelView new];
        [_unpayedRevenueView styleSettingsWithMode:WITH_SUBTITLE];
        [_unpayedRevenueView.titleLabel setText:LOCALIZE(@"user_revenue_report_unclaimed")];
        [_mainScrollView addSubview:_unpayedRevenueView];
        
        _paidRevenueView = [ArrowSwitchLabelView new];
        [_paidRevenueView styleSettingsWithMode:WITH_SUBTITLE];
        [_paidRevenueView.titleLabel setText:LOCALIZE(@"user_revenue_report_paid")];
        [_mainScrollView addSubview:_paidRevenueView];
        
        
        _monthlyView = [ArrowSwitchLabelView new];
        [_monthlyView styleSettingsWithMode:WITH_ARROW];
        [_monthlyView.titleLabel setText:LOCALIZE(@"user_revenue_report_monthly")];
        [_monthlyView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickDetail:)]];
        _monthlyView.userInteractionEnabled = YES;
        [_mainScrollView addSubview:_monthlyView];
        
        _dataView = [ArrowSwitchLabelView new];
        [_dataView styleSettingsWithMode:WITH_ARROW];
        [_dataView.titleLabel setText:LOCALIZE(@"user_revenue_report_data")];
        [_dataView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickDetail:)]];
        _dataView.userInteractionEnabled = YES;
        [_mainScrollView addSubview:_dataView];
        
        _postView = [ArrowSwitchLabelView new];
        [_postView styleSettingsWithMode:WITH_ARROW];
        [_postView.titleLabel setText:LOCALIZE(@"user_revenue_report_post")];
        [_postView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickDetail:)]];
        _postView.userInteractionEnabled = YES;
        [_mainScrollView addSubview:_postView];
        
        _liveView = [ArrowSwitchLabelView new];
        [_liveView styleSettingsWithMode:WITH_ARROW];
        [_liveView.titleLabel setText:LOCALIZE(@"user_revenue_report_live")];
        [_liveView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickDetail:)]];
        _liveView.userInteractionEnabled = YES;
        [_mainScrollView addSubview:_liveView];
        
        _receiveGiftView = [ArrowSwitchLabelView new];
        [_receiveGiftView styleSettingsWithMode:WITH_ARROW];
        [_receiveGiftView.titleLabel setText:LOCALIZE(@"received_gift")];
        [_receiveGiftView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickDetail:)]];
        _receiveGiftView.userInteractionEnabled = YES;
        [_mainScrollView addSubview:_receiveGiftView];
        
        
        
        _accountView = [ArrowSwitchLabelView new];
        [_accountView styleSettingsWithMode:WITH_ARROW];
        [_accountView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickDetail:)]];
        _accountView.userInteractionEnabled = YES;
        [_accountView.titleLabel setText:LOCALIZE(@"user_revenue_report_account")];
        [_mainScrollView addSubview:_accountView];
        
        _paymentView = [ArrowSwitchLabelView new];
        [_paymentView styleSettingsWithMode:WITH_ARROW];
        [_paymentView.titleLabel setText:LOCALIZE(@"user_revenue_report_history")];
        [_paymentView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickDetail:)]];
        _paymentView.userInteractionEnabled = YES;
        [_mainScrollView addSubview:_paymentView];
        
        
        [self.view addSubview:_mainScrollView];
        
        [self reloadUI];
    }

}

-(void)reloadUI
{
    for(UIView* view in [_mainScrollView subviews]){
        [view removeFromSuperview];
    }
    
    
    int nextY = 0;
    

    _lifeTimeTopView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, 75) ;
    [_mainScrollView addSubview:_lifeTimeTopView];
    nextY += _lifeTimeTopView.frame.size.height+topMargin;

    _todayRevenueView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, mainLabelHeight);
    [_mainScrollView addSubview:_todayRevenueView];

    nextY += _todayRevenueView.frame.size.height+topMargin;
    
    _monthRevenueView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, mainLabelHeight);
    [_mainScrollView addSubview:_monthRevenueView];
    nextY += _monthRevenueView.frame.size.height+topMargin;
    
//    _totalRevenueView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, mainLabelHeight);
//    [_mainScrollView addSubview:_totalRevenueView];
//    nextY += _totalRevenueView.frame.size.height+topMargin;
    
    _unpayedRevenueView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, mainLabelHeight);
    [_mainScrollView addSubview:_unpayedRevenueView];
    nextY += _unpayedRevenueView.frame.size.height;

    
    _paidRevenueView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, mainLabelHeight);
    [_mainScrollView addSubview:_paidRevenueView];
    nextY += _paidRevenueView.frame.size.height;
    
    _paymentWarningLabel.frame = CGRectMake(0, nextY, SCREEN_WIDTH, 30);
    nextY += _paymentWarningLabel.frame.size.height+topMargin;
    [_mainScrollView addSubview:_paymentWarningLabel];

    
    _monthlyView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, arrowLabelHeight);
    [_mainScrollView addSubview:_monthlyView];
    nextY += _monthlyView.frame.size.height+topMargin;
    
    _dataView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, arrowLabelHeight);
    [_mainScrollView addSubview:_dataView];
    nextY += _dataView.frame.size.height+topMargin;
    
    _postView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, arrowLabelHeight);
    [_mainScrollView addSubview:_postView];
    nextY += _postView.frame.size.height+topMargin;
    
    _liveView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, arrowLabelHeight);
    [_mainScrollView addSubview:_liveView];
    nextY += _liveView.frame.size.height+topMargin;
    
    
    _receiveGiftView.frame= CGRectMake(0, nextY, SCREEN_WIDTH, arrowLabelHeight);
    [_mainScrollView addSubview:_receiveGiftView];
    nextY += _receiveGiftView.frame.size.height+topMargin;

    
    _accountView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, arrowLabelHeight);
    [_mainScrollView addSubview:_accountView];
    nextY += _accountView.frame.size.height;
    
    if(![self checkingPaymentInfo]){
        _warningLabel.frame = CGRectMake(0, nextY, SCREEN_WIDTH, 30);
        nextY += _warningLabel.frame.size.height+topMargin;
        [_mainScrollView addSubview:_warningLabel];
        
    }else{
        nextY += topMargin;
    }
    
    _paymentView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, arrowLabelHeight);
    [_mainScrollView addSubview:_paymentView];
    nextY += _paymentView.frame.size.height+topMargin;
    
    [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, nextY+10)];
    
    _todayRevenueView.subTitleLabel.text = [NSString stringWithFormat:LOCALIZE(@"revenues_with_type"),[SINGLETON getCurrencyRate] *[REVENUE_MANAGER todayRevenue],[SINGLETON getCurrencyType]];
    _monthRevenueView.subTitleLabel.text = [NSString stringWithFormat:LOCALIZE(@"revenues_with_type"),[SINGLETON getCurrencyRate]*[REVENUE_MANAGER lastMonthRevenue],[SINGLETON getCurrencyType]];
 
    _unpayedRevenueView.subTitleLabel.text = [NSString stringWithFormat:LOCALIZE(@"revenues_with_type"),[SINGLETON getCurrencyRate]*[REVENUE_MANAGER unpayedRevenue],[SINGLETON getCurrencyType]];
    _totalRevenueView.subTitleLabel.text = [NSString stringWithFormat:LOCALIZE(@"revenues_with_type"),[SINGLETON getCurrencyRate]*[REVENUE_MANAGER totalRevenue],[SINGLETON getCurrencyType]];
    
    _paidRevenueView.subTitleLabel.text = [NSString stringWithFormat:LOCALIZE(@"revenues_with_type"),[SINGLETON getCurrencyRate]*([GET_DEFAULT(TOTAL_REVENUE_PAYED) intValue]),[SINGLETON getCurrencyType]];

    _lifeTimeRevinue.text = [NSString stringWithFormat:LOCALIZE(@"revenues_with_type"),[SINGLETON getCurrencyRate]*[REVENUE_MANAGER totalRevenue],[SINGLETON getCurrencyType]];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    _enterPageTimeStamp=CURRENT_TIMESTAMP;
    [EVENT_HANDLER addEventTracking:@"EnterRevenuePage" withDict:nil];
    if (![REVENUE_MANAGER eof])
    {
        [self pullData:YES];
    }else{
        [self reloadUI];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [EVENT_HANDLER addEventTracking:@"LeaveRevenuePage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp)}];
}


-(void)pullData:(BOOL)refresh
{

    [DIALOG_MANAGER showLoadingView];
    [REVENUE_MANAGER pullDataWithCallback:refresh withCallback:^(BOOL success, NSArray* dataArray){
        
        DLog(@"pullDataWithCallback");
        
        if (!success)
            return;
        
        if (![[SINGLETON revenueDataManager] eof])
            [self pullData:NO];
        else
        {
            [DIALOG_MANAGER hideLoadingView];
            [self reloadUI];
        }
    }];
}

-(void)didClickDetail:(UITapGestureRecognizer*)tapGesture
{
    if([tapGesture.view isEqual:_monthlyView]){
       
        static MonthlyRevenueController* ctrl = nil;
        if (ctrl == nil)
            ctrl = [[MonthlyRevenueController alloc] init];
        [self.navigationController pushViewController:ctrl animated:YES];
    
    }else if([tapGesture.view isEqual:_dataView]){
        
        static PaymentChartController* ctrl = nil;
        if (ctrl == nil)
            ctrl = [[PaymentChartController alloc] init];
        
        [self.navigationController pushViewController:ctrl animated:YES];
   
    }else if([tapGesture.view isEqual:_postView]){
        
        PostRevenueController* ctrl = nil;
        if (ctrl == nil)
            ctrl = [[PostRevenueController alloc] init];
        
        ctrl.userID = _user.userID;
        [self.navigationController pushViewController:ctrl animated:YES];

    }else if([tapGesture.view isEqual:_liveView]){
        
        LivestreamRevenueViewController* ctrl = nil;
        if (ctrl == nil)
            ctrl = [[LivestreamRevenueViewController alloc] init];
        
        ctrl.userID = _user.userID;
        [self.navigationController pushViewController:ctrl animated:YES];
        
    }else if([tapGesture.view isEqual:_receiveGiftView]){
        
        GiftReceiveViewController* ctrl = nil;
        if (ctrl == nil)
            ctrl = [[GiftReceiveViewController alloc] init];
        
//        ctrl.userID = _user.userID;
        [self.navigationController pushViewController:ctrl animated:YES];
        
    }else if([tapGesture.view isEqual:_accountView] || [tapGesture.view isEqual:_warningLabel]){

            PaymentInfoViewControllerV2* ctrl = [[PaymentInfoViewControllerV2 alloc] init];
            ctrl.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:ctrl animated:YES];
            DLog(@"%@",GET_DEFAULT(ALIPAYPHONE));
            DLog(@"%@",GET_DEFAULT(PAYPALEMAIL));
            ctrl.Neverfill=YES;
            if (![GET_DEFAULT(ALIPAYPHONE)isEqualToString:@""]) {
                DLog(@"ALIPAY");
                ctrl.Paypal=NO;
                ctrl.Neverfill=NO;
            }
            else if(![GET_DEFAULT(PAYPALEMAIL)isEqualToString:@""]){
                DLog(@"PAYPAL");
                ctrl.Paypal=YES;
                ctrl.Neverfill=NO;
            }
//            }
    }else if([tapGesture.view isEqual:_paymentView]){
        
        PaymentHistoryViewController* ctrl = nil;
        if (ctrl == nil)
            ctrl = [[PaymentHistoryViewController alloc] init];
        
        [self.navigationController pushViewController:ctrl animated:YES];
    }else if([tapGesture.view isEqual:_paymentWarningLabel]){
        
        PaymentWebViewController* vc = [PaymentWebViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }

    
}

-(BOOL)checkingPaymentInfo
{
//    if([GET_DEFAULT(IP_COUNTRY)isEqualToString:@"TW"])
//    {
//    
//    if([GET_DEFAULT(ID_CARD_NAME) isEqualToString:@""]){
//        return NO;
//    }
//    
//    if([GET_DEFAULT(ID_CARD_NUMBER) isEqualToString:@""]){
//        return NO;
//    }
//
//    if([GET_DEFAULT(ID_CARD_NAME) isEqualToString:@""]){
//        return NO;
//    }
//    
//    if([GET_DEFAULT(ID_CARD_FRONT_PICTURE) isEqualToString:@""]){
//        return NO;
//    }
//    
//    if([GET_DEFAULT(ID_CAR_BACK_PICTURE) isEqualToString:@""]){
//        return NO;
//    }
//    
//    if([GET_DEFAULT(BANK_NAME) isEqualToString:@""]){
//        return NO;
//    }
//    
//    if([GET_DEFAULT(BRANCH_NAME) isEqualToString:@""]){
//        return NO;
//    }
//    
//    if([GET_DEFAULT(ACCOUNT_NUMBER) isEqualToString:@""]){
//        return NO;
//    }
//    if([GET_DEFAULT(BENEFICIARY_NAME) isEqualToString:@""]){
//        return NO;
//    }
//    if([GET_DEFAULT(PASSBOOK_PICTURE) isEqualToString:@""]){
//        return NO;
//    }
//    }else{
        if([GET_DEFAULT(PAYPALEMAIL) isEqualToString:@""] && [GET_DEFAULT(ALIPAYPHONE) isEqualToString:@""]){
            return NO;
        }
//    }
    return YES;
}

@end
