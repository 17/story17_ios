//
//  PaymentInfoViewController.m
//  Story17
//
//  Created by POPO on 2015/5/14.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "PaymentInfoViewControllerV2.h"
#import "ListTableViewController.h"
#import "Constant.h"
#import "FillAccountViewController.h"

@implementation PaymentInfoViewControllerV2

#define verticalMargin 5
#define titleLabelHeight 30

- (id)init
{
    self = [super init];
    if (self)
    {

    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self addCustomNavigationBackButton];
    [self configureViewForIOS7];
    
    self.title = LOCALIZE(@"user_revenue_report_account");
    
    [self.view setBackgroundColor:LIGHT_BG_COLOR];
    _PickOneAccountLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 70,SCREEN_WIDTH-20, 2*titleLabelHeight)];
    [_PickOneAccountLabel setTextColor:MAIN_COLOR];
    _PickOneAccountLabel.textAlignment = NSTextAlignmentCenter;
    _PickOneAccountLabel.numberOfLines = 2;
    [_PickOneAccountLabel setText:LOCALIZE(@"Pick_account_to_fill")];
    [_PickOneAccountLabel setFont:SYSTEM_FONT_WITH_SIZE(15)];
//    [_PickOneAccountLabel sizeToFit];
    _PickOneAccountLabel.center=CGPointMake(SCREEN_WIDTH/2, 30);
    [self.view addSubview:_PickOneAccountLabel];
    
    _PaypalBtn=[[UIButton alloc]initWithFrame:CGRectMake(100, 100, SCREEN_WIDTH-30, 40    )];
    [_PaypalBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [_PaypalBtn setImage:IMAGE_FROM_BUNDLE(@"paypal") forState:UIControlStateNormal];
    [_PaypalBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
    [_PaypalBtn setSelected:YES];
    [_PaypalBtn addTarget:self action:@selector(clickPaypal:) forControlEvents:UIControlEventTouchUpInside];
    _PaypalBtn.center=CGPointMake(SCREEN_WIDTH/2, 80);
    
    _AlipayBtn=[[UIButton alloc]initWithFrame:CGRectMake(100, 100, SCREEN_WIDTH-30, 40    )];
    [_AlipayBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [_AlipayBtn setImage:IMAGE_FROM_BUNDLE(@"alipay") forState:UIControlStateNormal];
    [_AlipayBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
    [_AlipayBtn setSelected:YES];
    [_AlipayBtn addTarget:self action:@selector(cliclAlipay:) forControlEvents:UIControlEventTouchUpInside];
    _AlipayBtn.center=CGPointMake(SCREEN_WIDTH/2, 80+50);
    

    
    #ifdef INTERNATIONAL_VERSION
        [self.view addSubview:_PaypalBtn];
    #else
        [self.view addSubview:_AlipayBtn];
    #endif
    
    [self.view setBackgroundColor:LIGHT_BG_COLOR];
    _ChooseOneLabel=[[UILabel alloc]initWithFrame:CGRectMake(100, 250, 2/3*SCREEN_WIDTH, titleLabelHeight)];
    [_ChooseOneLabel setTextColor:MAIN_COLOR];
    _ChooseOneLabel.textAlignment = NSTextAlignmentCenter;
    [_ChooseOneLabel setText:LOCALIZE(@"Choose_only_one")];
    [_ChooseOneLabel setFont:SYSTEM_FONT_WITH_SIZE(14)];
    [_ChooseOneLabel sizeToFit];
    _ChooseOneLabel.center=CGPointMake(SCREEN_WIDTH/2, 200);
//    [self.view addSubview:_ChooseOneLabel];
    
      if (!_Neverfill) {
        [_PickOneAccountLabel setText:LOCALIZE(@"Already_Pick_account")];
//        [_ChooseOneLabel setFont:SYSTEM_FONT_WITH_SIZE(18)];
        _ChooseOneLabel.userInteractionEnabled = YES;

          if (_Paypal) {
//            _AlipayBtn.hidden=YES;
            [_ChooseOneLabel setText:LOCALIZE(@"Change_to_alipay")];
            UITapGestureRecognizer *tapGesture =
            [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cliclAlipay:)];
            [_ChooseOneLabel addGestureRecognizer:tapGesture];
          }
        else{
            [_ChooseOneLabel setText:LOCALIZE(@"Change_to_paypal")];
//            _PaypalBtn.hidden=YES;
            _AlipayBtn.frame=_PaypalBtn.frame;
            
            UITapGestureRecognizer *tapGesture =
            [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickPaypal:)];
            [_ChooseOneLabel addGestureRecognizer:tapGesture];
        }
        [_ChooseOneLabel sizeToFit];
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:_ChooseOneLabel.text];
        [attributeString addAttribute:NSUnderlineStyleAttributeName
                                value:[NSNumber numberWithInt:1]
                                range:(NSRange){0,[attributeString length]}];
        _ChooseOneLabel.attributedText = [attributeString copy];
        _ChooseOneLabel.center=CGPointMake(SCREEN_WIDTH/2, 150);
      }
}

-(void)doneAction
{
//    
//    [API_MANAGER updateUserInfo:_updateInfoDict  fetchSelfInfo:YES  completion:^(BOOL success) {
//        if(success){
//            
//        }
//    }];
    
}

-(void)clickPaypal:(id)sender
{

    FillAccountViewController* fill = [[FillAccountViewController alloc]init];
    fill.Paypal=YES;
    if (![GET_DEFAULT(PAYPALEMAIL)isEqualToString:@""]&&_Paypal) {
        fill.Account=GET_DEFAULT(PAYPALEMAIL);
    }
    [self.navigationController pushViewController:fill animated:YES];
}

- (void)cliclAlipay:(id)sender

{
    
    FillAccountViewController* fill = [[FillAccountViewController alloc]init];
    fill.Paypal=NO;
    if (![GET_DEFAULT(ALIPAYPHONE)isEqualToString:@""]&&!_Paypal) {
        fill.Account=GET_DEFAULT(ALIPAYPHONE);
    }
    [self.navigationController pushViewController:fill animated:YES];
}




@end
