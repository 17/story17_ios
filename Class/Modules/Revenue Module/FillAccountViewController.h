//
//  PaymentInfoViewController.h
//  Story17
//
//  Created by POPO on 2015/5/14.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TitleLabelView.h"
#import "MultiFileUploader.h"
#import "ImageTextField.h"

@interface FillAccountViewController : UIViewController <UITextFieldDelegate>
{
@private
    
}


@property (nonatomic,strong)UIImageView* Paypalimg;
@property (nonatomic,strong)UILabel* Or;
@property (nonatomic,strong)UILabel* GotoSingup;
@property (nonatomic,strong)UIButton* DoneBtn;
@property (nonatomic,strong)UIButton* AlipayBtn;
@property (nonatomic,strong)ImageTextField* Form;
@property (nonatomic)BOOL Paypal;
@property (nonatomic,strong)NSString* Account;

@end
