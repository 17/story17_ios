#import "PCMAudioRecorder.h"

#define kOutputBus 0
#define kInputBus 1

@implementation PCMAudioRecorder

@synthesize pcmBufferByteSize;
@synthesize isRecording;
@synthesize delegate;
@synthesize pcmDataBuffer;
@synthesize audioUnit;
@synthesize recordMode;

static PCMAudioRecorder* RECORDER = NULL;
static float *convertedSampleBuffer = NULL;
static AudioBufferList audioBuffer;
static AudioStreamBasicDescription pcmAudioStreamDescription;

static OSStatus renderCallback(void *userData, AudioUnitRenderActionFlags *actionFlags,
                               const AudioTimeStamp *audioTimeStamp, UInt32 busNumber,
                               UInt32 numFrames, AudioBufferList *buffers) {
    if(RECORDER.isRecording==NO) {
        return noErr;
    }
    
    audioBuffer.mBuffers[0].mDataByteSize = numFrames * 2; // sample size
    
    // we put our buffer into a bufferlist array for rendering
    OSStatus status = AudioUnitRender(RECORDER.audioUnit, actionFlags, audioTimeStamp, busNumber, numFrames, &audioBuffer);
    
    if(status==noErr) {
        SInt16 *inputFrames = (SInt16*) (audioBuffer.mBuffers[0].mData);
        
        // convert int to float for 'PCM_RECORDER_MODE_MP4' record mode
        if(RECORDER.recordMode==PCM_RECORDER_MODE_MP4) {
            for(int i = 0; i < numFrames; i++) {
                convertedSampleBuffer[i] = ((float) inputFrames[i]) / 32768.0;
            }
            
            // add float data to buffer
            [RECORDER.pcmDataBuffer appendBytes:convertedSampleBuffer length:sizeof(float) * numFrames];
        } else {
            // add pcm unsinged short data to buffer
            [RECORDER.pcmDataBuffer appendBytes:inputFrames length:sizeof(SInt16) * numFrames];
        }
        
        // output to delegate
        while (RECORDER.pcmDataBuffer.length>=RECORDER.pcmBufferByteSize) {
            NSData* audioData = [NSData dataWithBytes:RECORDER.pcmDataBuffer.bytes length:RECORDER.pcmBufferByteSize];
            [RECORDER.delegate didOutputAudioData:audioData];
            [RECORDER.pcmDataBuffer replaceBytesInRange:NSMakeRange(0, RECORDER.pcmBufferByteSize) withBytes:NULL length:0];
        }
    }
    
    return noErr;
}

-(void) initAudioStream {
    OSStatus status;
    
    // We define the audio component
    AudioComponentDescription desc;
    desc.componentType = kAudioUnitType_Output; // we want to ouput
    
    if(recordMode==PCM_RECORDER_MODE_MP4) {
        desc.componentSubType = kAudioUnitSubType_RemoteIO; // we want in and ouput
    } else if(recordMode==PCM_RECORDER_MODE_OPUS) {
        desc.componentSubType = kAudioUnitSubType_RemoteIO; // we want in and ouput
    }
    
    desc.componentFlags = 0; // must be zero
    desc.componentFlagsMask = 0; // must be zero
    desc.componentManufacturer = kAudioUnitManufacturer_Apple; // select provider
    
    // find the AU component by description
    AudioComponent inputComponent = AudioComponentFindNext(NULL, &desc);
    
    // create audio unit by component
    status = AudioComponentInstanceNew(inputComponent, &audioUnit);
    
    // define that we want record io on the input bus
    UInt32 flag = 1;
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioOutputUnitProperty_EnableIO, // use io
                                  kAudioUnitScope_Input, // scope to input
                                  kInputBus, // select input bus (1)
                                  &flag, // set flag
                                  sizeof(flag));
    
    // define that we do not want to play on io on the output bus
    flag = 0;
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioOutputUnitProperty_EnableIO, // use io
                                  kAudioUnitScope_Output, // scope to output
                                  kOutputBus, // select output bus (0)
                                  &flag, // set flag
                                  sizeof(flag));
    
    // set the format on the output stream
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Output,
                                  kInputBus,
                                  &pcmAudioStreamDescription,
                                  sizeof(pcmAudioStreamDescription));
    
    // set the format on the input stream
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input,
                                  kOutputBus,
                                  &pcmAudioStreamDescription,
                                  sizeof(pcmAudioStreamDescription));
    
    /**
     We need to define a callback structure which holds
     a pointer to the recordingCallback and a reference to
     the audio processor object
     */
    AURenderCallbackStruct callbackStruct;
    
    // set recording callback
    callbackStruct.inputProc = renderCallback; // recordingCallback pointer
    callbackStruct.inputProcRefCon = NULL;
    
    // set input callback to recording callback on the input bus
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioOutputUnitProperty_SetInputCallback,
                                  kAudioUnitScope_Global,
                                  kInputBus,
                                  &callbackStruct,
                                  sizeof(callbackStruct));
    
    // reset flag to 0
    flag = 0;
    
    /*
     we need to tell the audio unit to allocate the render buffer,
     that we can directly write into it.
     */
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioUnitProperty_ShouldAllocateBuffer,
                                  kAudioUnitScope_Output,
                                  kInputBus,
                                  &flag,
                                  sizeof(flag));
    
//    if(recordMode==PCM_RECORDER_MODE_OPUS) {
//        int quality = 127;
//        status = AudioUnitSetProperty(audioUnit, kAUVoiceIOProperty_VoiceProcessingQuality, kAudioUnitScope_Global, 1, &quality, sizeof(quality));
//    }
}

- (id) initWithMode:(int) mode {
    self = [super init];
    
    if(!self) {
        return nil;
    }
    
    RECORDER = self;
    
    pcmDataBuffer = CREATE_MUTABLE_DATA;
    
    recordMode = mode;
    
    if(recordMode==PCM_RECORDER_MODE_MP4) {
        pcmBufferByteSize = 4096; // ffmpeg accepts 1024 samples for its buffer => 4096 bytes of float pcm samples
        pcmAudioStreamDescription.mSampleRate = 44100;
    } else if(recordMode==PCM_RECORDER_MODE_OPUS) {
        pcmBufferByteSize = 3840; // opus accepts 1920 samples for its buffer (40ms mode) => 3840 bytes of float pcm samples
        pcmAudioStreamDescription.mSampleRate = 48000;
    }
    
    pcmAudioStreamDescription.mFormatID = kAudioFormatLinearPCM;
    pcmAudioStreamDescription.mFormatFlags = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
    pcmAudioStreamDescription.mBitsPerChannel = 16;
    pcmAudioStreamDescription.mBytesPerFrame = 2;
    pcmAudioStreamDescription.mChannelsPerFrame = 1;
    pcmAudioStreamDescription.mBytesPerPacket = pcmAudioStreamDescription.mBytesPerFrame * pcmAudioStreamDescription.mChannelsPerFrame;
    pcmAudioStreamDescription.mFramesPerPacket = 1;
    
    convertedSampleBuffer = malloc(sizeof(float) * 1024);
    
    audioBuffer.mNumberBuffers = 1;
    audioBuffer.mBuffers[0].mNumberChannels = 1;
    audioBuffer.mBuffers[0].mData = malloc(4096); // big enough buffer size to hold pcm data
    
    // initialization
    [self initAudioStream];
    
    return self;
}

-(void) startRecording
{
    isRecording = YES;
    AudioUnitInitialize(audioUnit);
    AudioOutputUnitStart(audioUnit);
}

-(void) stopRecording
{
    AudioOutputUnitStop(audioUnit);
    AudioUnitUninitialize(audioUnit);
    
    isRecording = NO;
}

-(void) cleanUp
{
    AudioComponentInstanceDispose(audioUnit);
}

-(void)dealloc
{
    free(convertedSampleBuffer);
    free(audioBuffer.mBuffers[0].mData);
}

@end
