#import "MyOpusDecoder.h"

@implementation MyOpusDecoder

@synthesize decoder;
@synthesize audioDecodeBuffer;

- (id) init {
    self = [super init];
    
    if(!self) {
        return nil;
    }
    
    audioDecodeBuffer = malloc(sizeof(opus_int16) * 1920);
    
    int errCode;
    
    decoder = opus_decoder_create(48000, 1, &errCode);

    return self;
}

-(NSData*) decodeAudioWithData:(NSData*) audioData
{
    int numOfDecodedSamples = opus_decode(decoder, audioData.bytes, (opus_int32) audioData.length, audioDecodeBuffer, 1920, 0);
//    DLog(@"audioData.len: %d, numOfDecodedSamples: %d",audioData.length, numOfDecodedSamples);

    if(numOfDecodedSamples<0) {
        DLog(@"WTF: %s", opus_strerror(numOfDecodedSamples));
    }
    NSData* decodedAudioData;
    
    if (numOfDecodedSamples>0) {
        decodedAudioData = [NSData dataWithBytes:audioDecodeBuffer length:numOfDecodedSamples*2];
    } else {
        DLog(@"WTF");
    }
    
    return decodedAudioData;
}

-(NSData*) decodeAudioWithData:(NSData*) audioData recoverLostPacket:(int) recoverLostPacket
{
    int numOfDecodedSamples = 0;
    
    if(audioData==nil) { // indicate packet loss
        numOfDecodedSamples = opus_decode(decoder, NULL, 0, audioDecodeBuffer, 0, 0);
        
        return nil;
    } else {
        numOfDecodedSamples = opus_decode(decoder, audioData.bytes, (opus_int32) audioData.length, audioDecodeBuffer, 1920, recoverLostPacket);
    }
    
    NSData* decodedAudioData;
    
    if (numOfDecodedSamples>0) {
        decodedAudioData = [NSData dataWithBytes:audioDecodeBuffer length:numOfDecodedSamples*2];
    }
    
    return decodedAudioData;
}

-(void)dealloc
{
    opus_decoder_destroy(decoder);
    
    free(audioDecodeBuffer);
}

@end
