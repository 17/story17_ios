#import "VP8VideoDecoder.h"

@implementation VP8VideoDecoder

@synthesize codec;
@synthesize codecContext;

- (id) init {
    self = [super init];
    
    if(!self) {
        return nil;
    }
    
    codecContext = NULL;
    codec = NULL;
    
    avcodec_register_all();
    [self reset];
    
    return self;
}

-(void) reset
{
    av_log_set_level(AV_LOG_QUIET);

    //     av_log_set_level(AV_LOG_TRACE);

    if(codecContext!=NULL) {
        avcodec_close(codecContext);
        av_free(codecContext);
    }
    
    codec = avcodec_find_decoder(AV_CODEC_ID_VP8);

    codecContext = avcodec_alloc_context3(codec);
    
    if(codec->capabilities&CODEC_CAP_TRUNCATED) {
        codecContext->flags|= CODEC_FLAG_TRUNCATED;
    }
    
    codecContext->width = FRAME_WIDTH;
    codecContext->height = FRAME_HEIGHT;
    
    codecContext->pix_fmt = PIX_FMT_YUV420P;
    codecContext->thread_count = 2;

    avcodec_open2(codecContext, codec, NULL);
}

-(NSArray*) decodeWithData:(NSData*)frameData dataLength:(int)length cropToSquare:(int) cropToSquare discardNonIFrame:(BOOL)discardNonIFrame
{
    if(frameData==nil) {
        return nil;
    }
    
    if(frameData.length==0) {
        return nil;
    }
    
    NSMutableArray* yuvData = CREATE_MUTABLE_ARRAY;
    
    AVFrame* frame = av_frame_alloc();
    AVPacket framePacket;
    
    av_init_packet(&framePacket);
    
    // fill frame packet with data
    framePacket.size = length;
    framePacket.data = (void *) frameData.bytes;
    
    int len, gotFrame;
    len = avcodec_decode_video2(codecContext, frame, &gotFrame, &framePacket);
    
    DLog(@"frame->pict_type=%d, discardNonIFrame=%d", frame->pict_type, discardNonIFrame);
    
    if ( (len>0&&gotFrame) && (frame->pict_type==AV_PICTURE_TYPE_I||!discardNonIFrame) ) {
        
        //        if (cropToSquare) {
        //            [self decodeFrame:frame withWidth:frame->height andHeight:frame->height intoData:yuvData];
        //        } else {
        //           [self decodeFrame:frame withWidth:frame->width andHeight:frame->height intoData:yuvData];
        //        }
        
//        NSData* yData;
//        NSData* uData;
//        NSData* vData;
//        yData = [[NSData alloc] initWithBytes:yPlane length:frame->height*frame->height];
//        uData = [[NSData alloc] initWithBytes:uPlane length:frame->height*frame->height/4];
//        vData = [[NSData alloc] initWithBytes:vPlane length:frame->height*frame->height/4];
        
        uint8_t* yPlane;
        uint8_t* uPlane;
        uint8_t* vPlane;
        
        if(cropToSquare) {
            yPlane = malloc(frame->height*frame->height);
            uPlane = malloc(frame->height*frame->height/4);
            vPlane = malloc(frame->height*frame->height/4);
        } else {
            yPlane = malloc(frame->width*frame->height);
            uPlane = malloc(frame->width*frame->height/4);
            vPlane = malloc(frame->width*frame->height/4);
        }
        
        // copy y data
        for(int y=0;y<frame->height;y++) {
            if(cropToSquare) {
                memcpy(yPlane + y*frame->height, frame->data[0] + y*frame->linesize[0] + (frame->width-frame->height)/2, frame->height);
            } else {
                memcpy(yPlane + y*frame->width, frame->data[0] + y*frame->linesize[0], frame->width);
            }
        }
        
        // copy uv data
        int halfFrameWidth = frame->width / 2;
        int halfFrameHeight = frame->height / 2;
        
        for(int y=0;y<halfFrameHeight;y++) {
            if(cropToSquare) {
                memcpy(uPlane + y*halfFrameHeight, frame->data[1] + y*frame->linesize[1] + (halfFrameWidth-halfFrameHeight)/2, halfFrameHeight);
                memcpy(vPlane + y*halfFrameHeight, frame->data[2] + y*frame->linesize[2] + (halfFrameWidth-halfFrameHeight)/2, halfFrameHeight);
            } else {
                memcpy(uPlane + y*halfFrameWidth, frame->data[1] + y*frame->linesize[1], halfFrameWidth);
                memcpy(vPlane + y*halfFrameWidth, frame->data[2] + y*frame->linesize[2], halfFrameWidth);
            }
        }

        // local hold
//        NSData* yData;
//        NSData* uData;
//        NSData* vData;
//        if (cropToSquare) {
//            yData = [NSData dataWithBytesNoCopy:yPlane length:frame->height*frame->height freeWhenDone:YES];
//            uData = [NSData dataWithBytesNoCopy:uPlane length:frame->height*frame->height/4 freeWhenDone:YES];
//            vData = [NSData dataWithBytesNoCopy:vPlane length:frame->height*frame->height/4 freeWhenDone:YES];
//        } else {
//            yData = [NSData dataWithBytesNoCopy:yPlane length:frame->width*frame->height freeWhenDone:YES];
//            uData = [NSData dataWithBytesNoCopy:uPlane length:frame->width*frame->height/4 freeWhenDone:YES];
//            vData = [NSData dataWithBytesNoCopy:vPlane length:frame->width*frame->height/4 freeWhenDone:YES];
//        }
//        
//        [yuvData addObject:yData];
//        [yuvData addObject:uData];
//        [yuvData addObject:vData];
        
        
        // POPO ori dataWithBytesNoCopy
//        if (cropToSquare) {
//            [yuvData addObject:[NSData dataWithBytesNoCopy:yPlane length:frame->height*frame->height freeWhenDone:YES]];
//            [yuvData addObject:[NSData dataWithBytesNoCopy:uPlane length:frame->height*frame->height/4 freeWhenDone:YES]];
//            [yuvData addObject:[NSData dataWithBytesNoCopy:vPlane length:frame->height*frame->height/4 freeWhenDone:YES]];
//        } else {
//            [yuvData addObject:[NSData dataWithBytesNoCopy:yPlane length:frame->width*frame->height freeWhenDone:YES]];
//            [yuvData addObject:[NSData dataWithBytesNoCopy:uPlane length:frame->width*frame->height/4 freeWhenDone:YES]];
//            [yuvData addObject:[NSData dataWithBytesNoCopy:vPlane length:frame->width*frame->height/4 freeWhenDone:YES]];
//        }
        
        // dataWithBytes
        if (cropToSquare) {
            [yuvData addObject:[NSData dataWithBytes:yPlane length:frame->height*frame->height]];
            [yuvData addObject:[NSData dataWithBytes:uPlane length:frame->height*frame->height/4]];
            [yuvData addObject:[NSData dataWithBytes:vPlane length:frame->height*frame->height/4]];
        } else {
            [yuvData addObject:[NSData dataWithBytes:yPlane length:frame->width*frame->height]];
            [yuvData addObject:[NSData dataWithBytes:uPlane length:frame->width*frame->height/4]];
            [yuvData addObject:[NSData dataWithBytes:vPlane length:frame->width*frame->height/4]];
        }
        
        free(yPlane);
        free(uPlane);
        free(vPlane);
    }
    
    av_frame_free(&frame);
    av_free_packet(&framePacket);
    
    if(yuvData.count==0) { // decode failed
        return nil;
    }
    
    return yuvData;
}

- (void)decodeFrame:(AVFrame*)frame withWidth:(int)width andHeight:(int)height intoData:(NSMutableArray*)dataArray
{
    uint8_t* yPlane = malloc(width*height);
    uint8_t* uPlane = malloc(width*height/4);
    uint8_t* vPlane = malloc(width*height/4);

    // assumption: height is usually smaller than width
    // copy y data
    for(int y=0;y<height;y++) {
        memcpy(yPlane + y*width, frame->data[0] + y*frame->linesize[0], width);
    }
    
    // copy uv data
    int halfWidth = width / 2;
    int halfHeight = height / 2;
    
    for(int y=0;y<halfHeight;y++) {
        memcpy(uPlane + y*halfWidth, frame->data[1] + y*frame->linesize[1], halfWidth);
        memcpy(vPlane + y*halfWidth, frame->data[2] + y*frame->linesize[2], halfWidth);
    }
    
    [dataArray addObject:[NSData dataWithBytesNoCopy:yPlane length:width*height freeWhenDone:YES]];
    [dataArray addObject:[NSData dataWithBytesNoCopy:uPlane length:width*height/4 freeWhenDone:YES]];
    [dataArray addObject:[NSData dataWithBytesNoCopy:vPlane length:width*height/4 freeWhenDone:YES]];
}

-(void)dealloc
{
    if(codecContext!=NULL) {
        avcodec_close(codecContext);
        av_free(codecContext);
    }
}

@end
