//
//  LiveStreamCommentObject.m
//  Story17
//
//  Created by POPO on 6/18/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "LiveStreamCommentObject.h"

@implementation LiveStreamCommentObject

+(LiveStreamCommentObject*) getLiveStreamCommentWithDict:(NSDictionary*)commentDict
{
    LiveStreamCommentObject* comment = [LiveStreamCommentObject new];
    comment.colorCode = commentDict[@"colorCode"];
    comment.message = commentDict[@"message"];
    comment.timestamp = [commentDict[@"timestamp"] intValue];
    comment.isSystem = [commentDict[@"isSystem"] intValue];
    comment.user = [UserObject getUserWithDict:commentDict[@"userInfo"]];
    return comment;
    
}

+(LiveStreamCommentObject*) getLiveStreamCommentWithDictandUser:(NSDictionary *)commentDict user:(UserObject *)user
{
    LiveStreamCommentObject* comment = [LiveStreamCommentObject new];
    if (commentDict[@"colorCode"]!=nil) {
        comment.colorCode = commentDict[@"colorCode"];
    }
    if (commentDict[@"message"]) {
        comment.message = commentDict[@"message"];
    }
    if (commentDict[@"timestamp"]) {
        comment.timestamp = [commentDict[@"timestamp"] intValue];
    }
    if (commentDict[@"isSystem"]) {
        comment.isSystem = [commentDict[@"isSystem"] intValue];
    }
    
    comment.user = user;
    return comment;
}


@end