#import "Constant.h"

#import "opus.h"

@interface MyOpusEncoder : NSObject

@property OpusEncoder* encoder;
@property unsigned char* audioEncodeBuffer;

-(NSData*) encodeAudioWithData:(NSData*) audioData;

@end
