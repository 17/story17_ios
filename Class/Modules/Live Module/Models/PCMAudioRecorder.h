#import "Constant.h"

#define PCM_RECORDER_MODE_MP4 0
#define PCM_RECORDER_MODE_OPUS 1

@protocol AudioRecorderDelegate
@optional
-(void)didOutputAudioData:(NSData*)outputData;
@end

@interface PCMAudioRecorder : NSObject

@property int pcmBufferByteSize;
@property AudioComponentInstance audioUnit;
@property int recordMode;

@property (nonatomic, strong) NSMutableData* pcmDataBuffer;

@property bool isRecording;
@property (assign) id<AudioRecorderDelegate> delegate;

- (id) initWithMode:(int) mode;

-(void) startRecording;
-(void) stopRecording;

-(void) cleanUp;

@end
