//
//  LiveStreamChatMessage.h
//  Story17
//
//  Created by POPO on 7/1/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "LiveStreamCommentObject.h"

int timeStamp;

@class LiveStreamCommentObject;

@interface LiveStreamChatMessage : NSObject

@property (nonatomic, strong) NSAttributedString* message;
@property (nonatomic, strong) LiveStreamCommentObject* commentObj;

-(NSString*)getLivestreamCommentID;

+ (id) initWithLiveStreamCommentObject: (LiveStreamCommentObject*)lvCommentObj andAtUser: (NSString*)atUser;
- (id) initWithLiveStreamCommentObject: (LiveStreamCommentObject*)lvCommentObj andAtUser: (NSString*)atUser;
+ (id) initWithUserName: (NSString*)userName andAtUser: (NSString*)atUser andChatContent: (NSString*)content andHexColorCode: (NSString*)hexColorCode andTimestamp: (int)ts;
- (id) initWithUserName: (NSString*)userName andAtUser: (NSString*)atUser andChatContent: (NSString*)content andHexColorCode: (NSString*)hexColorCode andTimestamp: (int)ts;

+ (CGSize) getHeightFromAttributedString: (NSAttributedString*)string;

-(UIColor*)getMessageColor;

@end
