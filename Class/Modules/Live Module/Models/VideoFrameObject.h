#import "Constant.h"

@interface VideoFrameObject : NSObject

@property (nonatomic, strong) NSData* encodedVideoData;
@property (nonatomic, strong) NSArray* yuvData; // 0: y Data (NSData), 1: u Data (NSData), 2: v Data (NSData)
@property int absTimestamp;

-(id) init;

@end
