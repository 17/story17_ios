#import "Constant.h"
#include "libavcodec/avcodec.h"
#include "libavutil/opt.h"
#include "libavformat/avformat.h"

@interface VP8VideoDecoder : NSObject

@property AVCodec* codec;
@property AVCodecContext* codecContext;

-(void) reset;
-(NSArray*) decodeWithData:(NSData*)frameData dataLength:(int)length cropToSquare:(int) cropToSquare discardNonIFrame:(BOOL)discard;

@end
