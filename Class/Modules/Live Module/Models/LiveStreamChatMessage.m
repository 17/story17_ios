//
//  LiveStreamChatMessage.m
//  Story17
//
//  Created by POPO on 7/1/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "LiveStreamChatMessage.h"
#import "UIColor+Helper.h"

#define DEFAULT_CELL_HEIGHT 28

int timestamp;

@interface LiveStreamChatMessage()

@property (nonatomic, strong) LiveStreamChatMessage* msg;

@end

@implementation LiveStreamChatMessage

@synthesize message;

+ (id) initWithLiveStreamCommentObject: (LiveStreamCommentObject*)lvCommentObj andAtUser: (NSString*)atUser
{
    
    return [[LiveStreamChatMessage alloc] initWithLiveStreamCommentObject:lvCommentObj andAtUser:atUser];
}

- (id) initWithLiveStreamCommentObject: (LiveStreamCommentObject*)lvCommentObj andAtUser: (NSString*)atUser
{
    _commentObj = lvCommentObj;
    return [self initWithUserName:lvCommentObj.user.name andAtUser:atUser andChatContent:lvCommentObj.message andHexColorCode:lvCommentObj.colorCode andTimestamp:lvCommentObj.timestamp];
}

+ (id) initWithUserName: (NSString*)userName andAtUser: (NSString*)atUser andChatContent: (NSString*)content andHexColorCode: (NSString*)hexColorCode andTimestamp: (int)ts
{
    return [[LiveStreamChatMessage alloc] initWithUserName:userName andAtUser:atUser andChatContent:content andHexColorCode:hexColorCode andTimestamp: ts];
}

- (id) initWithUserName: (NSString*)userName andAtUser: (NSString*)atUser andChatContent: (NSString*)content andHexColorCode: (NSString*)hexColorCode andTimestamp: (int)ts
{
    self = [super init];
    if (self) {
        NSMutableAttributedString *chatMessage = [[NSMutableAttributedString alloc] initWithString:@""];
        
        // From name
//        NSDictionary *fromNameAttr = @{NSForegroundColorAttributeName: MAIN_COLOR};
//        NSAttributedString *fromName = [[NSAttributedString alloc] initWithString:userName attributes:fromNameAttr];
//        [chatMessage appendAttributedString:fromName];
        
        // space
//        NSDictionary *spaceAttr = @{NSForegroundColorAttributeName: [UIColor clearColor]};
//        NSAttributedString *space = [[NSAttributedString alloc] initWithString:@" " attributes:spaceAttr];
//        [chatMessage appendAttributedString:space];
        
        // @names
//        NSDictionary *atNameAttr = @{NSForegroundColorAttributeName: [UIColor blackColor]};
//        NSAttributedString *atNames = [[NSAttributedString alloc] initWithString:atUser attributes:atNameAttr];
//        [chatMessage appendAttributedString:atNames];
        
        // space
//        [chatMessage appendAttributedString:space];
        
        // content
        NSDictionary *msgAttr = @{NSForegroundColorAttributeName: [LiveStreamChatMessage colorWithHexString: hexColorCode]};
        NSAttributedString *msg = [[NSAttributedString alloc] initWithString:content attributes:msgAttr];
        [chatMessage appendAttributedString:msg];
        
        // timestamp
        timestamp = ts;
        message = [chatMessage copy];
    }
    
    return self;
}

+ (CGSize) getHeightFromAttributedString: (NSAttributedString*)string
{

    
    CGSize constraint = CGSizeMake(SCREEN_WIDTH-10, 20000.0f);
    CGSize size;
 
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [string boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin context:context].size;
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size;
    //return (DEFAULT_CELL_HEIGHT > size.height) ?DEFAULT_CELL_HEIGHT :size.height;
}

+ (UIColor *) colorWithHexString: (NSString *)hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue = 0.0, green;
//    DLog(@"color = %@", colorString);
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [LiveStreamChatMessage colorComponentFrom: colorString start: 0 length: 1];
            green = [LiveStreamChatMessage colorComponentFrom: colorString start: 1 length: 1];
            blue  = [LiveStreamChatMessage colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [LiveStreamChatMessage colorComponentFrom: colorString start: 0 length: 1];
            red   = [LiveStreamChatMessage colorComponentFrom: colorString start: 1 length: 1];
            green = [LiveStreamChatMessage colorComponentFrom: colorString start: 2 length: 1];
            blue  = [LiveStreamChatMessage colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [LiveStreamChatMessage colorComponentFrom: colorString start: 0 length: 2];
            green = [LiveStreamChatMessage colorComponentFrom: colorString start: 2 length: 2];
            blue  = [LiveStreamChatMessage colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [LiveStreamChatMessage colorComponentFrom: colorString start: 0 length: 2];
            red   = [LiveStreamChatMessage colorComponentFrom: colorString start: 2 length: 2];
            green = [LiveStreamChatMessage colorComponentFrom: colorString start: 4 length: 2];
            blue  = [LiveStreamChatMessage colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            alpha = 1.0f;
            red = 255.0f;
            green = 255.0f;
            blue = 255.0f;
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

-(NSString*)getLivestreamCommentID
{
    return [NSString stringWithFormat:@"%@-%@-%d",message,_commentObj.user.userID,_commentObj.timestamp];
}

-(UIColor*)getMessageColor
{
    int colorInt = [SINGLETON hexString2ColorCode:_commentObj.colorCode]%7;

//    return [UIColor colorFromHexString:_commentObj.colorCode];
    
    switch (colorInt) {
        case 0:
            return COMMENT_TEXT_COLOR1;
            break;
        case 1:
            return COMMENT_TEXT_COLOR2;
            break;
        case 2:
            return COMMENT_TEXT_COLOR3;
            break;
        case 3:
            return COMMENT_TEXT_COLOR4;
            break;
        case 4:
            return COMMENT_TEXT_COLOR5;
            break;
        case 5:
            return COMMENT_TEXT_COLOR6;
            break;
        case 6:
            return COMMENT_TEXT_COLOR7;
            break;
        default:
            return COMMENT_TEXT_COLOR7;
            break;
    }
}



@end
