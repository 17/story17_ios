#import "MessageObject.h"

@implementation MessageObject

+(MessageObject *)getMessageObjectWithDict:(NSDictionary *)dict
{
    MessageObject *m = [[MessageObject alloc] init];
    DLog(@"msgDict:%@",dict);
    
    m.content = dict[@"content"];
    m.type = dict[@"type"];
    m.senderID = dict[@"sender"];
    m.receiverID = dict[@"receiver"];
    m.isSent = 1;

    if([dict valueForKey:@"isRead"]!=nil) {
        m.isRead = [dict[@"isRead"] intValue];
    } else {
        m.isRead = 1;
    }
    
    m.isUploaded = 1;
    
    m.timestamp = [dict[@"timestamp"] intValue];
    
    if([dict valueForKey:@"needReview"]!=nil) {
        m.needReview = [dict[@"needReview"] intValue];
    } else {
        m.needReview = 0;
    }
    
    m.messageID = dict[@"messageID"];
    
    return m;
}

@end
