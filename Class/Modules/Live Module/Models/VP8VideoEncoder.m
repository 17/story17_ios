#import "VP8VideoEncoder.h"

@implementation VP8VideoEncoder

@synthesize codec;
@synthesize codecContext;
@synthesize videoFrame;

- (id) init {
    self = [super init];
    
    if(!self) {
        return nil;
    }
    
    codecContext = NULL;
    codec = NULL;
    
    avcodec_register_all();
    [self reset];
    
    return self;
}

-(void) reset
{
    av_log_set_level(AV_LOG_QUIET);

//     av_log_set_level(AV_LOG_TRACE);

    if(codecContext!=NULL) {
        avcodec_close(codecContext);
        av_free(codecContext);
    }
    
    int bitRate = MEDIUM_LIVESTREAM_VIDEO_BIT_RATE;
    int liveStreamQuality = [[DEFAULTS objectForKey:LIVESTREAM_QUALITY] intValue];
//    if(liveStreamQuality==LIVESTREAM_QUALITY_FULL_HD) {
//        bitRate = FULL_HD_LIVESTREAM_VIDEO_BIT_RATE;
//    } else
    if(liveStreamQuality==LIVESTREAM_QUALITY_VERY_HIGH) {
        bitRate = VERY_HIGH_LIVESTREAM_VIDEO_BIT_RATE;
        DLog(@"超高畫質開啟");
    } else if(liveStreamQuality==LIVESTREAM_QUALITY_HIGH) {
        bitRate = HIGH_LIVESTREAM_VIDEO_BIT_RATE;
        DLog(@"高畫質開啟");
    } else if(liveStreamQuality==LIVESTREAM_QUALITY_MEDIUM) {
        bitRate = MEDIUM_LIVESTREAM_VIDEO_BIT_RATE;
        DLog(@"中畫質開啟");
    } else if(liveStreamQuality==LIVESTREAM_QUALITY_LOW) {
        bitRate = LOW_LIVESTREAM_VIDEO_BIT_RATE;
        DLog(@"低畫質開啟");
    } else if(liveStreamQuality==LIVESTREAM_QUALITY_VERY_LOW) {
        bitRate = VERY_LOW_LIVESTREAM_VIDEO_BIT_RATE;
        DLog(@"超低畫質開啟");
    }

    
    codec = avcodec_find_encoder(AV_CODEC_ID_VP8);
//    if(IS_USING_WIFI_NETWORK || IS_USING_4G_NETWORK) {
//        bitRate = 1024000;
//    } else {
//        bitRate = 256000;
//    }
    
    
//        bitRate = 4096000;

//    bitRate = 256000;

//    DLog(@"VIDEO BIT RATE: %d", bitRate);
    
    codecContext = avcodec_alloc_context3(codec);
    codecContext->width = FRAME_WIDTH;
    codecContext->height = FRAME_HEIGHT;
    codecContext->bit_rate = bitRate;
    codecContext->gop_size = 30; // use smaller GOP size => make the live stream begin and resume faster!
    codecContext->time_base = (AVRational) {1, LIVE_STREAM_FRAME_RATE};
    codecContext->pix_fmt = PIX_FMT_YUV420P;
    
    av_opt_set(codecContext->priv_data, "quality", "realtime", 0);
    av_opt_set_int(codecContext->priv_data, "cpu-used", 2, 0);
//    av_opt_set_int(codecContext->priv_data, "deadline", 15, 0);

    avcodec_open2(codecContext, codec, NULL);
    
    // alloc reusable video frame buffer
    videoFrame = av_frame_alloc();
    videoFrame->format = codecContext->pix_fmt;
    videoFrame->width  = codecContext->width;
    videoFrame->height = codecContext->height;
    videoFrame->linesize[0] = codecContext->width;
    videoFrame->linesize[1] = codecContext->width/2;
    videoFrame->linesize[2] = codecContext->width/2;
}

-(NSData*) encodeWithYData:(u_int8_t*) yData andCbData:(u_int8_t*) cbData andCrData:(u_int8_t*) crData
{
    AVPacket framePacket;
    av_init_packet(&framePacket);
    framePacket.data = NULL;
    framePacket.size = 0;
    
    videoFrame->data[0] = yData;
    videoFrame->data[1] = cbData;
    videoFrame->data[2] = crData;
    
    /* encode the frame */
    int gotPacket = 0;
    
    int result = avcodec_encode_video2(codecContext, &framePacket, videoFrame, &gotPacket);
    
    if(result<0 || gotPacket==0) {
        DLog(@"VIDEO ENCODE ERROR!");
        return nil;
    }
    
    NSData* encodedData = [NSData dataWithBytes:framePacket.data length:framePacket.size];
    
    av_free_packet(&framePacket);

    return encodedData;
}

-(void)dealloc
{
    av_frame_free(&videoFrame);
    avcodec_close(codecContext);
}

@end
