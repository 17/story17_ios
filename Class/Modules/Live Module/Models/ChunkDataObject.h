#import "Constant.h"

@interface ChunkDataObject : NSObject

typedef NS_ENUM(NSInteger, ChunkStatus) {
    ChunkStatusNone = 0,
    ChunkStatusDownloading,
    ChunkStatusDownloaded,
    ChunktatusDecoding,
    ChunkStatusDecoded,
    
    ChunkStatusMax
};

@property (nonatomic, strong) NSData* chunkData;
@property int chunkID;
@property ChunkStatus status;

-(id) init;

@end
