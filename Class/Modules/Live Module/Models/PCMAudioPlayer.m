#import "PCMAudioPlayer.h"

#define AUDIO_BUFFER_COUNT 250
#define TARGET_NUM_OF_AUDIO_BUFFER_SAMPLE (480.0 * 30)
#define AUDIO_SYNC_TOLERANCE (480.0 * 15)

@implementation PCMAudioPlayer

@synthesize audioFormat;
@synthesize audioQueue;
@synthesize bufferByteSize;
@synthesize emptyAudioData;
@synthesize audioQueueTimeline;
@synthesize samplesToSkip;
@synthesize isFirstTimePlay;
@synthesize currentBufferIndex;
@synthesize playerTimeDelay;
@synthesize doNotNeedSyncCount;
@synthesize numberOfAudioBufferInAudioQueue;
@synthesize delegate;

static AudioQueueBufferRef playAudioQueueBuffers[AUDIO_BUFFER_COUNT];
static PCMAudioPlayer* audioPlayerInstance;

static void playAudioDataCompleteCallback (void *playerObject, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer) {
    
    if(inBuffer==NULL) {
        return;
    }
    
    audioPlayerInstance.numberOfAudioBufferInAudioQueue--;
    
    if(inBuffer->mUserData==NULL) {
        return;
    }
    
    int absTime = ((AudioQueueBufferUserData*) inBuffer->mUserData)->absTime;
    
    // this means it is silence data
    if(absTime==0) {
        return;
    }
    
    [audioPlayerInstance.delegate didPlayAudioAtAbsTime:absTime numberOfAudioBufferInQueue:audioPlayerInstance.numberOfAudioBufferInAudioQueue];
}

- (id) init {

    self = [super init];
    
    if(!self) {
        return nil;
    }
    
    audioPlayerInstance = self;

    // initialization
    audioFormat.mSampleRate = 48000.0;
    audioFormat.mFormatID = kAudioFormatLinearPCM;
    audioFormat.mChannelsPerFrame = 1;
    audioFormat.mFramesPerPacket = 1;
    audioFormat.mBytesPerPacket = 2;
    audioFormat.mBitsPerChannel = 16;
    audioFormat.mFormatFlags = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
    audioFormat.mBytesPerFrame = 2;
    
    bufferByteSize = 1920 * 2; // 1920 samples (40ms)
    
    return self;
}

-(void) setup
{
    int16_t emptyAudioSamples[1920] = {0};
    
    emptyAudioData = [NSData dataWithBytes:emptyAudioSamples length:3840];

    AudioQueueNewOutput(&audioFormat, playAudioDataCompleteCallback, (__bridge void *) self, CFRunLoopGetCurrent (), kCFRunLoopCommonModes, 0, &audioQueue);
    
    for(int i=0;i<AUDIO_BUFFER_COUNT;i++) {
        AudioQueueAllocateBuffer(audioQueue, bufferByteSize, &playAudioQueueBuffers[i]);
        playAudioQueueBuffers[i]->mAudioDataByteSize = bufferByteSize;
        playAudioQueueBuffers[i]->mUserData = malloc(sizeof(AudioQueueBufferUserData));
    }
    
    AudioQueueStart(audioQueue, NULL);
    
    AudioQueueCreateTimeline(audioQueue, &audioQueueTimeline);
    
    [self setGain:5.0];
    
    isFirstTimePlay = YES;
}

-(AudioQueueBufferRef) getNextAvailableAudioBuffer
{
    AudioQueueBufferRef audioBuffer = playAudioQueueBuffers[currentBufferIndex];
    currentBufferIndex++;
    currentBufferIndex = currentBufferIndex % AUDIO_BUFFER_COUNT;
    
    if(audioBuffer->mUserData==NULL) {
        return NULL;
    }
    
    return audioBuffer;
}

-(void) play:(NSData*) audioData absTime:(int) absTime
{
    if(samplesToSkip>0) {
        samplesToSkip--;
        return;
    }
    
    if(isFirstTimePlay) { // TRICKY: play at least three silence for priming audio data!
        
        isFirstTimePlay = NO;

        [self playSilence];
        [self playSilence];
        [self playSilence];
        
        float actualDelay = playerTimeDelay - TARGET_NUM_OF_AUDIO_BUFFER_SAMPLE;
        
        if(actualDelay>0) {
            samplesToSkip = ceil(actualDelay/1920.0) - 1;
        }
        
        return;
    }
    
    AudioTimeStamp inTimeStamp;
    AudioTimeStamp outTimestamp;
    
    AudioQueueGetCurrentTime(audioQueue, audioQueueTimeline, &inTimeStamp, NULL);
    
    AudioQueueBufferRef audioBuffer = [self getNextAvailableAudioBuffer];
    
    if(audioBuffer==NULL) {
        return;
    }
    
    ((AudioQueueBufferUserData*) audioBuffer->mUserData)->absTime = absTime;
    
    memcpy(audioBuffer->mAudioData, audioData.bytes, audioData.length);
    AudioQueueEnqueueBufferWithParameters(audioQueue, audioBuffer, 0, NULL, 0, 0, 0, NULL, NULL, &outTimestamp);
    numberOfAudioBufferInAudioQueue++;
    
    playerTimeDelay = outTimestamp.mSampleTime - inTimeStamp.mSampleTime;
    
    if(fabs(playerTimeDelay-TARGET_NUM_OF_AUDIO_BUFFER_SAMPLE)<AUDIO_SYNC_TOLERANCE) {
        doNotNeedSyncCount = 50; // force at least 1 second no need for audio sync, because audio is already synced now!
    }
    
    // TRICKY: IN VOD MODE, WE NEED TO DISABLE AUDIO ADVANCE CHECK!
//    float actualDelay = playerTimeDelay - TARGET_NUM_OF_AUDIO_BUFFER_SAMPLE;
//    
//    if(actualDelay>AUDIO_SYNC_TOLERANCE) {
//        samplesToSkip = ceil(actualDelay/1920.0) - 1;
//    }

//    DLog(@"playerTimeDelay: %f", playerTimeDelay);
}

-(void) syncAudio
{
    if(doNotNeedSyncCount>0) {
        doNotNeedSyncCount--;
        return;
    }
    
    int numberOfSilenceToFill = 0;
    
    if(playerTimeDelay<TARGET_NUM_OF_AUDIO_BUFFER_SAMPLE) {
        float actualDelay = TARGET_NUM_OF_AUDIO_BUFFER_SAMPLE - playerTimeDelay;

        numberOfSilenceToFill = ceil(actualDelay/1920.0);
        
        for(int i=0;i<numberOfSilenceToFill;i++) {
            [self playSilence];
        }
    }
}

-(void) setGain:(float) gain
{
    if(audioQueue==NULL) {
        return;
    }
    
    AudioQueueSetParameter(audioQueue, kAudioQueueParam_Volume, gain);
}

-(void) playSilence
{
//    DLog(@"SILENCE!");
    
    AudioTimeStamp inTimeStamp;
    AudioTimeStamp outTimestamp;
    
    AudioQueueGetCurrentTime(audioQueue, audioQueueTimeline, &inTimeStamp, NULL);
    
    AudioQueueBufferRef audioBuffer = [self getNextAvailableAudioBuffer];
    
    if(audioBuffer==NULL) {
        return;
    }
    
    ((AudioQueueBufferUserData*) audioBuffer->mUserData)->absTime = 0;

    memcpy(audioBuffer->mAudioData, emptyAudioData.bytes, bufferByteSize);
    AudioQueueEnqueueBufferWithParameters(audioQueue, audioBuffer, 0, NULL, 0, 0, 0, NULL, NULL, &outTimestamp);
    numberOfAudioBufferInAudioQueue++;
    
    playerTimeDelay = outTimestamp.mSampleTime - inTimeStamp.mSampleTime;
}

-(void) reset
{
    if(audioQueue==NULL) {
        return;
    }
    
    isFirstTimePlay = YES;
    
    AudioQueueReset(audioQueue);
    AudioQueueStart(audioQueue, NULL);
    
    numberOfAudioBufferInAudioQueue = 0;
}

-(void) cleanup
{
    if(audioQueue==NULL) {
        return;
    }
    
    AudioQueueStop(audioQueue, false);
    AudioQueueDisposeTimeline(audioQueue, audioQueueTimeline);

    for(int i=0;i<AUDIO_BUFFER_COUNT;i++) {
        free(playAudioQueueBuffers[i]->mUserData);
        AudioQueueFreeBuffer(audioQueue, playAudioQueueBuffers[i]);
        playAudioQueueBuffers[i]->mUserData = NULL;
    }

    AudioQueueDispose (audioQueue, true);

    audioQueue = NULL;
}

-(void)dealloc
{
//    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
//    [[AVAudioSession sharedInstance] setActive:NO error:nil];
}

@end
