#import "Constant.h"

typedef struct AudioQueueBufferUserData {
    UInt32 absTime;
} AudioQueueBufferUserData;

@protocol PCMAudioPlayerDelegate
-(void) didPlayAudioAtAbsTime:(int) absTime numberOfAudioBufferInQueue:(int) numberOfAudioBufferInQueue;
@end

@interface PCMAudioPlayer : NSObject

@property AudioStreamBasicDescription audioFormat;
@property AudioQueueRef audioQueue;
@property UInt32 bufferByteSize;
@property (nonatomic, strong) NSData* emptyAudioData;
@property AudioQueueTimelineRef audioQueueTimeline;
@property int samplesToSkip;
@property BOOL isFirstTimePlay;
@property BOOL setupCompleted;
@property int currentBufferIndex;
@property float playerTimeDelay;
@property int doNotNeedSyncCount;
@property dispatch_queue_t audioPlayerDispatchQueue;
@property int numberOfAudioBufferInAudioQueue;
@property (nonatomic, assign) id<PCMAudioPlayerDelegate> delegate;

-(void) play:(NSData*) audioData absTime:(int) absTime;
-(void) playSilence;
-(void) setup;
-(void) reset;
-(void) cleanup;
-(void) setGain:(float) gain;
-(void) syncAudio;

@end
