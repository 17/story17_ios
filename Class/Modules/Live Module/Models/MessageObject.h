#import "Constant.h"

@interface MessageObject : NSObject

@property (nonatomic, strong) NSString *type;
@property (nonatomic) int32_t isRead;
@property (nonatomic) int32_t isSent;
@property (nonatomic) int32_t isUploaded;
@property (nonatomic) NSString* receiverID;
@property (nonatomic) NSString* senderID;
@property (nonatomic) int32_t timestamp;
@property (nonatomic, strong) NSString * content;
@property (nonatomic, strong) NSString * messageID;
@property int needReview;
@property int isLocked;

+(MessageObject *)getMessageObjectWithDict:(NSDictionary *)dict;

@end
