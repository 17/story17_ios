#import "LiveStreamObject.h"
#import "UIImage+Helper.h"

@implementation LiveStreamObject

+(LiveStreamObject*)getLiveStreamWithDict:(NSDictionary*)dict
{
    if ([dict count] == 0) {
        return nil;
    }

    LiveStreamObject* liveStream = [LiveStreamObject new];
    
    liveStream.picturesArray = CREATE_MUTABLE_ARRAY;
    liveStream.viewerArray = CREATE_MUTABLE_ARRAY;

    if([dict objectForKey:@"userInfo"]){
        liveStream.user = [UserObject getUserWithDict:dict[@"userInfo"]];
    }
    if([dict objectForKey:@"pictures"]){
        NSArray *tempArray = [dict[@"pictures"] mj_JSONObject];
        liveStream.picturesArray = [tempArray mutableCopy];
    }
     
    if([dict objectForKey:@"viewers"]){
        NSDictionary *deserializedData = [dict[@"viewers"] mj_JSONObject];
        UserObject* user = [UserObject new];
        for(NSDictionary* dic in deserializedData) {
            user.openID = dic[@"openID"];
            user.userID = dic[@"userID"];
            user.picture = dic[@"picture"];
            [liveStream.viewerArray addObject:user];
        }
    }
    
    liveStream.restreamerOpenID = dict[@"restreamerOpenID"];
    liveStream.liveStreamID = [NSString stringWithFormat:@"%@", dict[@"liveStreamID"]];
    liveStream.liveStreamIDInt = [liveStream.liveStreamID intValue];
    liveStream.userID = dict[@"userID"];
    liveStream.user = [UserObject getUserWithDict:dict[@"userInfo"]];
    liveStream.caption = dict[@"caption"];
    liveStream.locationName = dict[@"locationName"] ;
    liveStream.coverPhoto = dict[@"coverPhoto"] ;
    liveStream.latitude = [dict[@"latitude"] floatValue];
    liveStream.longitude = [dict[@"longitude"] floatValue];
    liveStream.shareLocation = [dict[@"shareLocation"] boolValue];
    liveStream.followerOnlyChat = [dict[@"followerOnlyChat"] boolValue];
    liveStream.chatAvailable = [dict[@"chatAvailable"] boolValue];
    liveStream.beginTime = [dict[@"beginTime"] intValue];
    liveStream.endTime = [dict[@"endTime"] intValue];
    liveStream.viewerCount = [dict[@"viewerCount"] intValue];
    liveStream.liveViewerCount = [dict[@"liveViewerCount"] intValue];
    liveStream.receivedLikeCount = [dict[@"receivedLikeCount"] intValue];
    liveStream.replayCount = [dict[@"replayCount"] intValue];
    liveStream.duration = [dict[@"duration"] intValue];
    liveStream.replayAvailable = [dict[@"replayAvailable"] boolValue];
    liveStream.totalViewTime = [dict[@"totalViewTime"] intValue];
    liveStream.revenue = [dict[@"revenue"] floatValue];
    liveStream.audioOnly = [dict[@"audioOnly"] intValue];
    liveStream.numberOfChunks = [dict[@"numberOfChunks"] intValue];
    liveStream.canSendGift=[dict[@"canSendGift"]intValue];
    
    return liveStream;
}


@end
