//
//  LiveStreamCommentObject.h
//  Story17
//
//  Created by POPO on 6/18/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserObject.h"

@class UserObject;

@interface LiveStreamCommentObject : NSObject

@property (nonatomic,strong) NSString* colorCode;
@property (nonatomic,strong) NSString* message;
@property (nonatomic) int32_t timestamp;
@property (nonatomic) int32_t isSystem;
@property (nonatomic,strong) UserObject* user;

+(LiveStreamCommentObject*) getLiveStreamCommentWithDict:(NSDictionary*)commentDict;
+(LiveStreamCommentObject*) getLiveStreamCommentWithDictandUser:(NSDictionary*)commentDict user:(UserObject*)user;

@end
