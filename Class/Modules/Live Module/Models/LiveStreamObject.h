//
//  LiveStreamObject.h
//  story17
//
//  Created by POPO Chen on 6/18/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserObject.h"

@class UserObject;

@interface LiveStreamObject : NSObject

@property (nonatomic,strong) NSString* liveStreamID;
@property (nonatomic,strong) NSMutableArray* picturesArray;  // for groupLivestream
@property (nonatomic,strong) NSMutableArray* viewerArray;  // for groupLivestream
@property int32_t liveStreamIDInt;
@property (nonatomic,strong) NSString* userID;
@property (nonatomic,strong) NSString* coverPhoto;
@property (nonatomic,strong) UserObject* user;

@property (nonatomic,strong) NSString* restreamerOpenID;
@property (nonatomic,strong) UIImage* userBlurImage;

@property (nonatomic,strong) NSString* caption;
@property (nonatomic,strong) NSString* locationName;
@property (nonatomic) float latitude;
@property (nonatomic) float longitude;
@property (nonatomic) BOOL shareLocation;
@property (nonatomic) BOOL followerOnlyChat;
@property (nonatomic) BOOL chatAvailable;
@property (nonatomic) BOOL replayAvailable;
@property (nonatomic) int beginTime;
@property (nonatomic) int endTime;
@property (nonatomic) int duration;
@property (nonatomic) int viewerCount;
@property (nonatomic) int liveViewerCount;
@property (nonatomic) int receivedLikeCount;
@property (nonatomic) int replayCount;
@property (nonatomic) int totalViewTime;
@property (nonatomic) float revenue;
@property (nonatomic) int audioOnly;
@property (nonatomic) int numberOfChunks;
@property (nonatomic) int canSendGift;
+(LiveStreamObject*)getLiveStreamWithDict:(NSDictionary*)dict;

@end
