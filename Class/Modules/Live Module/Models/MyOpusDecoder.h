#import "Constant.h"

#include "opus.h"

@interface MyOpusDecoder : NSObject

@property OpusDecoder* decoder;
@property opus_int16* audioDecodeBuffer;

-(NSData*) decodeAudioWithData:(NSData*) audioData;
-(NSData*) decodeAudioWithData:(NSData*) audioData recoverLostPacket:(int) recoverLostPacket;

@end
