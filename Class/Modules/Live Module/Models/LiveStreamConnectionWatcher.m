 //
//  LiveStreamConnectionWatcher.m
//  Story17
//
//  Created by POPO on 7/15/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "LiveStreamConnectionWatcher.h"
#import "Constant.h"
#import "AudioFrameObject.h"
#import "VideoFrameObject.h"
#import "VP8VideoDecoder.h"
#import "MyOpusDecoder.h"
#import "ChunkDataObject.h"
#import "LiveStreamObject.h"
#import "GCDAsyncUdpSocket.h"
#import <UMengAnalytics/MobClick.h>

static NSTimeInterval const kVideoOutputViewFadeDuration = 0.35;

@interface LiveStreamConnectionWatcher() {
    struct {
        unsigned int connectionLinkFailByType : 1;
        unsigned int receiveUdpPacket : 1;
        unsigned int lossRateStatus : 1;
        unsigned int shouldShowHUDWithType : 1;
        unsigned int initialCommentTable : 1;
        unsigned int phonecall : 1;
    } _delegateFlags;
}

#define LIVE_STREAM_WATCHER_CONNECTION_MAX_KEEP_ALIVE_COUNT 6
#define LIVE_STREAM_WATCHER_KEEP_ALIVE_TIME 1.0

// Data Chunk Types
#define CHUNK_DATA_TYPE_AUDIO 0
#define CHUNK_DATA_TYPE_VIDEO 1

// Live stream
#define MIN_AUDIO_BUFFER_COUNT_IN_QUEUE 20
#define MIN_WINDOW_SIZE 3


// GCD Queue
@property dispatch_queue_t socketQueue;
@property dispatch_queue_t audioDecodeQueue;
@property dispatch_queue_t videoDecodeQueue;

// Networking
@property (nonatomic, strong) GCDAsyncUdpSocket* udpSocket;

//@property (nonatomic,strong) UDPSocket* udpSocket;
@property (nonatomic, strong) AFHTTPRequestOperationManager* fileDownloadManager;
@property (nonatomic, strong) AFHTTPRequestOperationManager* fileDownloadManager2;

// Data Queue
@property (nonatomic, strong) NSMutableArray* queuedVideoFrameObjects;
@property (nonatomic, strong) NSMutableArray* receivedChunkDataArray;
@property (nonatomic, strong) NSMutableSet* retriedChunkIDSet;

@property int lastRequestedChunkID;
@property int lastParsedChunkID;
@property float minChunkStartDecodeTime;
// AV Record and Presentation
@property PCMAudioPlayer* audioPlayer;
@property (nonatomic, strong) MyFilter* displayFilter;
@property (nonatomic, strong) CADisplayLink* displayTimer;
@property (nonatomic, strong) NSDate* timerStartDate;

// CODEC
@property VP8VideoDecoder* videoDecoder;
@property (nonatomic,strong) MyOpusDecoder* opusDecoder;

// Control
@property (nonatomic,strong) NSTimer* keepAliveTimer;
@property (nonatomic) int keepAliveCount;
@property (nonatomic,strong) NSString* liveStreamID;
@property (nonatomic,strong) NSString* publicIP;
@property float lastReceivedAudioDataTime;
@property int numberOfAudioBufferInQueue;
@property BOOL isFirst;
@property int chunkDownloaded;

@property dispatch_queue_t decodeQueue;
@property (nonatomic, strong) NSMutableArray* windowObjects;
@property (nonatomic, strong) NSMutableArray* playingObjects;
@property BOOL discardNonIFrame;
@property BOOL isResetWindow;
@property BOOL isDecodingAudio;
@property BOOL isDecodingVideo;
@property BOOL shouldDecodeLater;
@property int32_t window_size;
@property int test;
@property BOOL phoneComing;
@property int successDownloadCount;
@property int failDownloadCount;
@property int audioOnlyTimes;
@property float latestDownloadTime;
@property float avgDownloadTime;
@end

@implementation LiveStreamConnectionWatcher

- (LiveStreamConnectionWatcher*) init
{
    if (self = [super init]) {
        /* eventTracking */
        _successDownloadCount=0;
        _failDownloadCount=0;
        _audioOnlyTimes=0;
        _latestDownloadTime=0;
        _avgDownloadTime=0;
        
        /* queue */
     
        _socketQueue = dispatch_queue_create("live_stream_socket_queue_watcher", DISPATCH_QUEUE_SERIAL);
        _audioDecodeQueue = dispatch_queue_create("audio_decode_queue", DISPATCH_QUEUE_SERIAL);
        _videoDecodeQueue = dispatch_queue_create("live_stream_video_decode_queue", DISPATCH_QUEUE_SERIAL);
        _callCenter = [[CTCallCenter alloc] init];
        /* audio */
        _opusDecoder = [MyOpusDecoder new];
        _isDecodingAudio = NO;
        
        /* video */
        _queuedVideoFrameObjects = CREATE_MUTABLE_ARRAY;
        _receivedChunkDataArray = CREATE_MUTABLE_ARRAY;
        _retriedChunkIDSet = CREATE_MUTABLE_SET;
        _isDecodingVideo = NO;
        
        _displayFilter = [[MyFilter alloc] initWithImageSize:CGSizeMake(640, 360) filterProgram:FILTER_PROGRAM_SIMPLE];
        [_displayFilter setParameter:@"imageSourceInputType" intValue:1];
        [_displayFilter setRotation:180.0 needHorizontalFlip:1];
        _videoDecoder = [VP8VideoDecoder new];
        
        // use tcp-like grabbing algorithm
        _windowObjects =  CREATE_MUTABLE_ARRAY;
        _playingObjects = CREATE_MUTABLE_ARRAY;
        
        // control
        _shouldDecodeLater = NO;
        _chunkDownloaded=0;
        _totalFrame=0;

        // Live Stream Video Preview
        // Finn: modify later... assign frame size???


        if (IS_IPHONE_4) {
            _glVideoOutputView = [[GLKView alloc] initWithFrame:CGRectMake(0, -SCREEN_HEIGHT/6, SCREEN_WIDTH, SCREEN_HEIGHT*4/3)];
        } else {
            _glVideoOutputView = [[GLKView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        }
        
        _glVideoOutputView.drawableColorFormat = GLKViewDrawableColorFormatRGB565;
        _glVideoOutputView.drawableDepthFormat = GLKViewDrawableDepthFormatNone;
        _glVideoOutputView.drawableMultisample = GLKViewDrawableMultisampleNone;
        _glVideoOutputView.context = _displayFilter.glContext;
        _glVideoOutputView.contentScaleFactor = 1.0;
        _glVideoOutputView.alpha = 0;
        [_glVideoOutputView setEnableSetNeedsDisplay:NO];
    }
    
    return self;
}

- (void)fadeVideoOutputView:(BOOL)fadeIn
{
    if (fadeIn) {
        [UIView animateWithDuration:kVideoOutputViewFadeDuration animations:^{
            _glVideoOutputView.alpha = 1;
        }];
    }
    else {
        [UIView animateWithDuration:kVideoOutputViewFadeDuration animations:^{
            _glVideoOutputView.alpha = 0;
        }];
    }
}

#pragma mark - tcp-like playing algorithm
- (void)checkWindowForChunk:(int32_t)chunkID
{
    NSAssert1(chunkID>0, @"chunkID=%d", chunkID);
    if (chunkID <= 0) {
        return;
    } else if (chunkID > _windowStartPoint+_window_size-1) {
        // add 1 for safe
        [self resetWindowStartPointToNumber:chunkID-_window_size+1+1 andLastestChunkID:chunkID];
    } else {
        [self getChunksFromWindowStartPointToChunk:chunkID];
    }
}

- (void)initialWindowToNumber:(int32_t)chunkID andLastestChunkID:(int32_t)latestChunkID
{
    DLog(@"initial window to %d", chunkID);
    
    _windowStartPoint = chunkID;
    for (int i=0; i<_window_size; i++) {
        ChunkDataObject* chunk = [ChunkDataObject new];
        [_windowObjects addObject:chunk];
    }
    
    _isResetWindow = YES;
    _windowStartPoint = chunkID;
    
    // reset decoder
    _discardNonIFrame = YES;
    //    _opusDecoder = [MyOpusDecoder new];
    //    _videoDecoder = [VP8VideoDecoder new];
    
    // reset queue
    [_playingObjects removeAllObjects];
    [_queuedVideoFrameObjects removeAllObjects];
    
    [self getChunksFromWindowStartPointToChunk:latestChunkID];
    
    // check if it can play after reset
    [self checkIsContinuousChunk:[_windowObjects objectAtIndex:0]];
}

- (void)resetWindowStartPointToNumber:(int32_t)chunkID andLastestChunkID:(int32_t)latestChunkID
{
    DLog(@"resetWindowStartPointToNumber %d, latest chunk %d", chunkID, latestChunkID);
    
    NSAssert1(chunkID>0, @"chunkID=%d", chunkID);
    if (chunkID <= 0 || chunkID <= _windowStartPoint || _windowStartPoint==0)
        return;
    
    // reset window
    int windowObjRemoveCount = MIN(chunkID-_windowStartPoint, _window_size);
    for (int i=0; i<windowObjRemoveCount; i++) {
        [_windowObjects removeObjectAtIndex:0];
        ChunkDataObject* chunk = [ChunkDataObject new];
        [_windowObjects addObject:chunk];
    }
    _isResetWindow = YES;
    _windowStartPoint = chunkID;
    
    // reset decoder
    _discardNonIFrame = YES;
//    _opusDecoder = [MyOpusDecoder new];
//    _videoDecoder = [VP8VideoDecoder new];
    
    // reset queue
    [_playingObjects removeAllObjects];
    [_queuedVideoFrameObjects removeAllObjects];
    
    [self getChunksFromWindowStartPointToChunk:latestChunkID];
    
    // check if it can play after reset
    [self checkIsContinuousChunk:[_windowObjects objectAtIndex:0]];
}

- (void)getChunksFromWindowStartPointToChunk:(int32_t)chunkID
{
    if (chunkID <= 0)
        return;

    for (int i=0; i<chunkID-_windowStartPoint+1; i++) {
        ChunkDataObject* chunk = [_windowObjects objectAtIndex:i];
        if (chunk.status == ChunkStatusNone) {
            chunk.chunkID = _windowStartPoint+i;
            [self downloadChunk:chunk];
        }
    }
}

- (void)downloadChunk:(ChunkDataObject*)chunk
{
//    DLog(@"download chunk %d", chunk.chunkID);
    if (chunk.status != ChunkStatusNone || chunk.chunkID <=0)
        return;
    
    chunk.status = ChunkStatusDownloading;
    
    if(_fileDownloadManager == nil) {
        _fileDownloadManager = [AFHTTPRequestOperationManager manager];
        _fileDownloadManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _fileDownloadManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/octet-stream"];
    }
    
//    if(_fileDownloadManager2 == nil) {
//        _fileDownloadManager2 = [AFHTTPRequestOperationManager manager];
//        _fileDownloadManager2.responseSerializer = [AFHTTPResponseSerializer serializer];
//        _fileDownloadManager2.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/octet-stream"];
//    }
//    
//    
//    NSDate *originNow = [NSDate date];
//    
//    static double totalTime1 = 0.0;
//    static int count1 = 0;
//    
    
//    [_fileDownloadManager2 GET:[NSString stringWithFormat:@"%@%@/%@/%d", @"http://17media-1752451560.us-west-2.elb.amazonaws.com:8000/", @"getLiveStreamChunk", _liveStreamID, chunk.chunkID]
//                   parameters:nil
//                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                          
//                          double timePassed_mms = [originNow timeIntervalSinceNow] * -1000.0;
//                          
//                          InfoLog(@"download chunk %d success, windowStartPoint:%d", chunk.chunkID,_windowStartPoint);
//                          InfoLog(@"t2-t1: %f", timePassed_mms);
//                          totalTime1 += timePassed_mms;
//                          count1++;
//                          InfoLog(@"average: %f", totalTime1/count1);
//                          
//                          [_staticLabel2 setText:[NSString stringWithFormat:@"download chunk %f avg:%f totalChunk:%d",timePassed_mms,totalTime1/count1,count1]];
//                                        }
//                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                      }
//     ];
    
    
    NSDate *now = [NSDate date];

    static double totalTime = 0.0;
    static int count = 0;

    NSString* chunkUrl = @"";
    
    #ifdef INTERNATIONAL_VERSION
        chunkUrl = BUCKET_URL;
    #else
        chunkUrl = LIVE_CHUNK_CDN_URL;
    #endif
    
    [_fileDownloadManager GET:[NSString stringWithFormat:@"%@%@/%@/%d", chunkUrl, @"getLiveStreamChunk", _liveStreamID, chunk.chunkID]
                   parameters:nil
                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
                          
                          double timePassed_ms = [now timeIntervalSinceNow] * -1000.0;
                          _successDownloadCount++;
//                          InfoLog(@"download chunk %d success, windowStartPoint:%d", chunk.chunkID,_windowStartPoint);
//                          InfoLog(@"t2-t1: %f", timePassed_ms);
                          totalTime += timePassed_ms;
                          count++;
//                          InfoLog(@"average: %f", totalTime/count);
                          
                          [_staticLabel setText:[NSString stringWithFormat:@"download chunk %f avg:%f totalChunk:%d",timePassed_ms,totalTime/count,count]];
                          _avgDownloadTime=totalTime/count;
                          if (timePassed_ms>_latestDownloadTime) {
                              _latestDownloadTime=timePassed_ms;
                          }
                          if (chunk.chunkID>=_windowStartPoint && chunk.chunkID<_windowStartPoint+_window_size) {
                              chunk.status = ChunkStatusDownloaded;
                              chunk.chunkData = (NSData*) responseObject;
                              [self checkIsContinuousChunk:chunk];
                          }
                      }
                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                          _failDownloadCount++;
                          DLog(@"download chunk %d fail", chunk.chunkID);
                          if (chunk.chunkID>=_windowStartPoint && chunk.chunkID<_windowStartPoint+_window_size) {
                              chunk.status = ChunkStatusNone;
                              [self downloadChunk:chunk];
                          }
                      }
     ];
}

- (void)checkIsContinuousChunk:(ChunkDataObject*)chunk
{
    if (chunk.status != ChunkStatusDownloaded || chunk.chunkID <=0)
        return;
    
    if (chunk.chunkID == _windowStartPoint) {
        
        // move window
        _windowStartPoint++;
        [_playingObjects addObject:chunk];
//        _chunkDownloaded++;
        
//        DLog(@"move windowStartPoiplnt to %d when getting chunkID=%d, playingObj count=%ld", _windowStartPoint, chunk.chunkID, (unsigned long)[_playingObjects count]);

        // remove this chunk from window and then add a new empty chunk
        [_windowObjects removeObject:chunk];
        ChunkDataObject* newChunk = [ChunkDataObject new];
        [_windowObjects addObject:newChunk];

        // decode and play for reset condition
        NSInteger playingObjCount = [_playingObjects count];
        if (_isResetWindow && playingObjCount >= MIN_WINDOW_SIZE) {
            DLog(@"reset buffer is ready, playing Obj count = %ld", (unsigned long)[_playingObjects count]);
            _isResetWindow = NO;
            [self decodeAndPlayChunk:[_playingObjects objectAtIndex:0]];
        }
        
        // check the next chunk
        ChunkDataObject* nextChunk = [_windowObjects objectAtIndex:0];
        [self checkIsContinuousChunk:nextChunk];
    }
}

- (void)checkQueuedDecodeAndPlayAction
{
    if (!_isDecodingVideo && !_isDecodingAudio && _shouldDecodeLater && [_playingObjects count] != 0) {
        DLog(@"checkQueuedDecodeAndPlayAction");
        _shouldDecodeLater = NO;
        [self decodeAndPlayChunk:[_playingObjects objectAtIndex:0]];
    }
}

- (void)decodeAndPlayChunk:(ChunkDataObject*)chunk
{
    
//    DLog(@"decodeAndPlayChunk %d", chunk.chunkID);

    if (_isDecodingAudio || _isDecodingVideo) {
        _shouldDecodeLater = YES;
        return;
    }
    if (chunk.status!=ChunkStatusDownloaded || chunk.chunkID<=0) {
        return;
    }
    
    _isDecodingAudio = YES;
    _isDecodingVideo = YES;
    [_playingObjects removeObject:chunk];

    NSData* chunkDataToParse = chunk.chunkData;
    chunk.status = ChunktatusDecoding;

    dispatch_async(_socketQueue, ^{
        // Parse AV Data
        int dataPtr = 0;
        NSInteger datalength=0;
        NSMutableArray* audioFrameObjects = CREATE_MUTABLE_ARRAY;
        NSMutableArray* videoFrameObjects = CREATE_MUTABLE_ARRAY;
        
        while(dataPtr < chunkDataToParse.length) {
            // get info
            int32_t dataLength = *((int32_t*) (chunkDataToParse.bytes+dataPtr));
            int32_t absTimestamp = *((int32_t*) (chunkDataToParse.bytes+dataPtr+4));
            u_int8_t headerLength = *((u_int8_t*) (chunkDataToParse.bytes+dataPtr+8));
            u_int8_t dataType = *((u_int8_t*) (chunkDataToParse.bytes+dataPtr+9));
            
            NSData* packetData = [NSData dataWithBytes:(chunkDataToParse.bytes+dataPtr+headerLength) length:dataLength];
            
            if(dataType == CHUNK_DATA_TYPE_AUDIO && !_isPreview) {
                _chunkDownloaded++;
                AudioFrameObject* afObj = [AudioFrameObject new];
                afObj.audioData = packetData;
                afObj.absTimestamp = absTimestamp;
                [audioFrameObjects addObject:afObj];
            } else if(dataType == CHUNK_DATA_TYPE_VIDEO && _isVideoOn) {
                _chunkDownloaded=0;
                VideoFrameObject* vfObj = [VideoFrameObject new];
                vfObj.encodedVideoData = packetData;
                vfObj.absTimestamp = absTimestamp;
                datalength+=packetData.length;
                [videoFrameObjects addObject:vfObj];
                DLog(@"videoframecount=%lu",[videoFrameObjects count]);
            }
            
            dataPtr += (headerLength + dataLength);
        }
        _durationTimestamp = CURRENT_TIMESTAMP-_startTimestamp;
        _totalFrame+=[videoFrameObjects count];
        dispatch_async(MAIN_QUEUE, ^{
            [_staticLabel2 setText:[NSString stringWithFormat:@"nowFps : %lu avgFps:%d bitrate:%ld",([videoFrameObjects count]*10)/8,_totalFrame/_durationTimestamp,(long)datalength]];
        });
        if ([audioFrameObjects count] == 0) {
            _isDecodingAudio = NO;
        }
        if ([videoFrameObjects count] == 0) {
            _isDecodingVideo = NO;
        }
        [self checkQueuedDecodeAndPlayAction];
        
//        DLog(@"Parse chunk %d: %lu AF and %lu VF", chunk.chunkID, (unsigned long)audioFrameObjects.count, (unsigned long)videoFrameObjects.count);
        
        if (!_isPreview) {
            
            // TRICKY: over 3 seconds not receive audio data => need reset audio player!
            if(_lastReceivedAudioDataTime>0.0 && CACurrentMediaTime()-_lastReceivedAudioDataTime>3.0) {
                dispatch_async(MAIN_QUEUE, ^{
                    [_audioPlayer reset];
                });
            }
            
            _lastReceivedAudioDataTime = CACurrentMediaTime();
            
            dispatch_async(MAIN_QUEUE, ^{
                [_audioPlayer syncAudio];
            });
            
            dispatch_async(_audioDecodeQueue, ^{
                for(AudioFrameObject* afObj in audioFrameObjects) {
                    NSData* decodedAudioData = [_opusDecoder decodeAudioWithData:afObj.audioData];
                    
                    if ([[audioFrameObjects lastObject] isEqual:afObj]) {
                        _isDecodingAudio = NO;
                        [self checkQueuedDecodeAndPlayAction];
                    }
                    
                    dispatch_async(MAIN_QUEUE, ^{
                        int audioAbsTime = afObj.absTimestamp;
                        [_audioPlayer play:decodedAudioData absTime:audioAbsTime];
                    });
                }
            });
        }
        
        dispatch_async(_videoDecodeQueue, ^{
            
            for (VideoFrameObject* vfObj in videoFrameObjects) {
                vfObj.yuvData = [_videoDecoder decodeWithData:vfObj.encodedVideoData dataLength:(int) vfObj.encodedVideoData.length cropToSquare:_isPreview discardNonIFrame:_discardNonIFrame];
                
                if ([[videoFrameObjects lastObject] isEqual:vfObj]) {
                    _isDecodingVideo = NO;
                    [self checkQueuedDecodeAndPlayAction];
                }
                
                if (vfObj.yuvData != nil) {
                    
                    if (_discardNonIFrame) {
                        _discardNonIFrame = NO;
                    }
                    // Add to video frame queue for later display!
                    dispatch_async(MAIN_QUEUE, ^{
                        [_queuedVideoFrameObjects addObject:vfObj];
                        
                        if (_isPreview && _startTimestamp==0) {
                            [self initDisplayTimer:vfObj.absTimestamp];
                        }
                    });
                }
            }
            
        });
    });
}

#pragma mark - public method
- (void)connectToLive:(NSString*)lvID isPreview:(BOOL)isPreview withCompletion:(void(^)(LIVE_STREAM_CONNECT_TYPE status))callback
{
    _liveStreamID = lvID;
    _isPreview = isPreview;
    [NOTIFICATION_CENTER addObserver:self
                            selector:@selector(AudioInterruption:)
                                name:AVAudioSessionInterruptionNotification
                              object:_session];

    _isVideoOn = YES;
    
    if (_isConnected) {
        
        callback(LIVE_STREAM_CONNECT_TYPE_FAIL_ALREADY_CONNECT);
        
    } else {
        
        if (_delegateFlags.shouldShowHUDWithType && [_delegate shouldShowHUDWithType:HUDTypeFirstTimeLoading enabled:YES]) {
            [self fadeVideoOutputView:NO];
        }
        
        if(_isPreview){
            
            [API_MANAGER getLiveStreamInfo:lvID withCompletion:^(BOOL success, LiveStreamObject *liveSteamInfo) {
                if(success){
                    [self startWatcher];
                    callback(LIVE_STREAM_CONNECT_TYPE_SUCCESS);
                }else{
                    callback(LIVE_STREAM_CONNECT_TYPE_FAIL_ALREADY_END);
                }
            }];
        } else {
            
            [API_MANAGER enterLiveStream:lvID withCompletion:^(BOOL success, NSString *message, int timestamp, int numberOfChunks, NSString *colorCode,NSString* publicIP) {
                if (success) {
                    
                    _startTimestamp = timestamp;
                    _colorCode = colorCode;
//                    UIColor* color = [UIColor colorFromHexString:_colorCode];
                    
                    _publicIP = publicIP;
                    DLog(@"publicIP=%@",publicIP);
                    [self startWatcher];
                    callback(LIVE_STREAM_CONNECT_TYPE_SUCCESS);
                    

                   
                } else {
                    
                    [self disconnect];
                    
                    if ([message isEqualToString:@"ended"]) {
                        callback(LIVE_STREAM_CONNECT_TYPE_FAIL_ALREADY_END);
                    }
                    else if ([message isEqualToString:@"blocked"])
                        callback(LIVE_STREAM_CONNECT_TYPE_FAIL_BLOCK_BY_HOST);
                    else {
                        callback(LIVE_STREAM_CONNECT_TYPE_FAIL_NETWORK);
                    }
                }
            }];
        }
    }
}


- (void)disconnect
{
    if (_isConnected) {
        
        _durationTimestamp = CURRENT_TIMESTAMP-_startTimestamp;

        [self stopWatcher];
        
        if(!_isPreview){
            [API_MANAGER quitViewLiveStream:_liveStreamID duration:_durationTimestamp withCompletion:^(BOOL success) {
//                DLog(@"API quitViewLiveStream:%d", success);
            }];
        }
    }
}

#pragma mark - watcher control
- (void)startWatcher
{
//    DLog(@"startWatcher, ID=%@", _liveStreamID);
    
    if (!_isPreview) {
        [self startAudioPlayer];
    }
    NSError *error = nil;
    // start udp connection
   self.udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    if(![_udpSocket bindToPort:LIVE_STREAM_SERVER_PORT error:&error])
    {
        DLog("bind fail");
    }

    
    if (![self.udpSocket beginReceiving:&error])
    {
       // [self.udpSocket close];
        
        NSLog(@"Error starting server (recv): %@", error);
        return;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // force quick get udp data
        [self sendKeepAliveData];
    });
    
    /* timer */
    if (_keepAliveTimer) {
        [_keepAliveTimer invalidate];
        _keepAliveTimer = nil;
    }
    
    // start keep alive timer
    _keepAliveCount = LIVE_STREAM_WATCHER_CONNECTION_MAX_KEEP_ALIVE_COUNT;
    _keepAliveTimer = [NSTimer scheduledTimerWithTimeInterval:LIVE_STREAM_WATCHER_KEEP_ALIVE_TIME target:self selector:@selector(sendKeepAliveData) userInfo:nil repeats:YES];
    
    _isFirst = YES;
    _isConnected = YES;
}


-(void)AudioInterruption: (NSNotification *) notification
{
    
    if ([notification.name isEqualToString:AVAudioSessionInterruptionNotification]) {
        _phoneComing=YES;
        DLog(@"電話來了");
            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
            [[AVAudioSession sharedInstance] setActive:NO error:nil];

        //Check to see if it was a Begin interruption
        if ([[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] isEqualToNumber:[NSNumber numberWithInt:AVAudioSessionInterruptionTypeBegan]]) {
//            DLog(@"\n\nInterruption began!");
            _PhoneCallTimer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(detectphoneCall) userInfo:nil repeats:YES];
            
        } else if([[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] isEqualToNumber:[NSNumber numberWithInt:AVAudioSessionInterruptionTypeEnded]]){
            DLog(@"電話結束");
            
            [_audioPlayer reset];
            _phoneComing=NO;

            if(_PhoneCallTimer){
                [_PhoneCallTimer invalidate];
                _PhoneCallTimer=nil;
            }
            
//            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
//            [[AVAudioSession sharedInstance] setPreferredSampleRate:48000.0 error:NULL];
            [[AVAudioSession sharedInstance] setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
            _windowStartPoint=0;

            //Resume your audio
            // Resume playing the audio.
            
        }
    }
}


-(void)detectphoneCall
{
    _Count++;
    for (CTCall *call in _callCenter.currentCalls)  {
        if (call.callState == CTCallStateConnected ) {
            DLog(@"接起電話");
//            [_delegate phonecall];
            return;
        }
    }
    if (_Count>5) {
//        [_delegate phonecall];
        return;
    }
}

- (void)stopWatcher
{
    [NOTIFICATION_CENTER removeObserver:self];
    [DIALOG_MANAGER removeDialogImmediately];
    float percent = (_failDownloadCount * 100.0f) / (_successDownloadCount+_failDownloadCount);
    if (_liveStream!=nil) {
        [EVENT_HANDLER addEventTracking:@"LiveDownloadFailRate" withDict:@{@"failRate":FLOAT_TO_STRING(percent),@"audioOnlyCount":INT_TO_STRING(_audioOnlyTimes),@"targetUserID":_liveStream.user.userID,@"avgDownloadTime":FLOAT_TO_STRING(_avgDownloadTime),@"latestDownloadTime":FLOAT_TO_STRING(_latestDownloadTime),@"totalDownloadChunk":INT_TO_STRING(_successDownloadCount),@"viewerCount":INT_TO_STRING(_liveStream.viewerCount)}];

    }
    _isConnected = NO;
    _isFirst = NO;
    _totalFrame=0;
    [_queuedVideoFrameObjects removeAllObjects];
    [_playingObjects removeAllObjects];
    [_windowObjects removeAllObjects];
    _isDecodingAudio = NO;
    _isDecodingVideo = NO;
    _shouldDecodeLater = NO;
    _chunkDownloaded=0;
    /* eventTracking */
    _successDownloadCount=0;
    _failDownloadCount=0;
    _audioOnlyTimes=0;
    _latestDownloadTime=0;
    _avgDownloadTime=0;
    // stop keep alive timer
    if(_keepAliveTimer != nil) {
        [_keepAliveTimer invalidate];
        _keepAliveTimer = nil;
    }
    
    // stop window
    _windowStartPoint+=_window_size;
    
    // stop udp connection
    if(_udpSocket != nil) {
       // [_udpSocket stop];
        [self.udpSocket close];
        _udpSocket.delegate = nil;
        _udpSocket = nil;
    }
//    DLog(@"UDP CLOSE");
    if (_isPreview) {
        [self stopDisplayTimer];
    } else {
        [self stopAudioPlayer];
    }
    
//    DLog(@"stopWatcher, ID=%@, vf=%lu", _liveStreamID, (unsigned long)[_queuedVideoFrameObjects count]);
}

#pragma mark - network related
- (void) sendKeepAliveData
{

    if (_isConnected)
    {
        dispatch_async(MAIN_QUEUE, ^{
            // udp keep alive
            [self sendUDPPacket:LIVE_STREAM_PACKET_TYPE_KEEP_ALIVE_WATCHER toLiveStream:[_liveStreamID intValue]];
            
            if (!_isPreview) {
                // API keep alive
                [API_MANAGER keepViewLiveStream:_liveStreamID isPreview:NO withCompletion:^(BOOL success,BOOL blocked,NSString* message) {
                    if (success) {
                        _keepAliveCount = LIVE_STREAM_WATCHER_CONNECTION_MAX_KEEP_ALIVE_COUNT;
                        if (blocked) {
                            if (_delegateFlags.connectionLinkFailByType) {
                                [_delegate connectionLinkFailBy:LIVE_STREAM_DISCONNECT_TYPE_KICKED_BY_HOST];
                            }
                        }
                    }else{

                        if([message isEqualToString:@"live_killed"]){
                            [_delegate livestreamViewerKilled];
                            
//                            if(_udpSocket != nil) {
//                                // [_udpSocket stop];
//                                [self.udpSocket close];
//                                _udpSocket.delegate = nil;
//                                _udpSocket = nil;
//                            }
                            
                            if (_keepAliveTimer) {
                                [_keepAliveTimer invalidate];
                                _keepAliveTimer = nil;
                            }
                            return;
                        }
                        
                        
                        if(_keepAliveCount==0){
                            if (_delegateFlags.connectionLinkFailByType) {
                                [_delegate connectionLinkFailBy:LIVE_STREAM_DISCONNECT_TYPE_KEEP_ALIVE_FAIL];
                            }
                        }
                        _keepAliveCount --;
                    }
                }];
            }
        });
    }
}

- (void) sendUDPPacket:(LIVE_STREAM_PACKET_TYPE)type toLiveStream:(int)ID
{

    
    NSMutableData* data = CREATE_MUTABLE_DATA;
    u_int8_t packetType = type;
    [data appendBytes:&type length:sizeof(packetType)];
    [data appendBytes:&ID length:sizeof(ID)];
    
//    if(type==LIVE_STREAM_PACKET_TYPE_LIKE){
//        [data appendBytes:&_redCode length:sizeof(_redCode)];
//        [data appendBytes:&_greenCode length:sizeof(_greenCode)];
//        [data appendBytes:&_blueCode length:sizeof(_blueCode)];
//        DLog(@"r:%f g:%f b:%f ",_redCode,_greenCode,_blueCode);
//    }
    
    if (type == LIVE_STREAM_PACKET_TYPE_LIKE) {
        NSString *colorString = [[_colorCode stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
        unsigned r, g, b;
        [[NSScanner scannerWithString:[colorString substringFrom:0 to:2]] scanHexInt:&r];
        [[NSScanner scannerWithString:[colorString substringFrom:2 to:4]] scanHexInt:&g];
        [[NSScanner scannerWithString:[colorString substringFrom:4 to:6]] scanHexInt:&b];
        [data appendBytes:&r length:sizeof(r)];
        [data appendBytes:&g length:sizeof(g)];
        [data appendBytes:&b length:sizeof(b)];
    }
    
   // [_udpSocket sendData:data withTimeout:60 tag:0];
    [_udpSocket sendData:data toHost:_publicIP port:LIVE_STREAM_SERVER_PORT withTimeout:60 tag:0];
}

# pragma mark - UDPSocketDelegate
//- (void)echo:(UDPSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)addr
//{
//    
//    if(sock != _udpSocket) {
//        return;
//    }
//    
//    u_int8_t packetType = *((u_int8_t*) data.bytes);
//    int32_t liveStreamID = *((int32_t*) (data.bytes+1));
//
//    if(liveStreamID != [_liveStreamID intValue]) {
//        return;
//    }
//    
//    [_delegate receiveUdpPacket:packetType data:data];
//    
//    if(packetType == LIVE_STREAM_PACKET_TYPE_NEW_LIVE_CHUNK_READY) {
//        
//        int32_t currentChunkID = *((int32_t*) (data.bytes+5));
//        
//        if(_lastRequestedChunkID == 0) {
//            /* Grab  */
//            [self grabChunkData:currentChunkID-2];
//            [self grabChunkData:currentChunkID-1];
//        }
//        [self grabChunkData:currentChunkID];
//        
//        
//    } else if(packetType == LIVE_STREAM_PACKET_TYPE_KEEP_ALIVE_WATCHER) {
//        
//    } else if(packetType == LIVE_STREAM_PACKET_TYPE_STREAM_END){
//        
//        [LIKE_HANDLER likeLivestreamBatchUpdate];
//        [_delegate connectionLinkFailBy:LIVE_STREAM_DISCONNECT_TYPE_HOST_END];
//        
//    } else if(packetType == LIVE_STREAM_PACKET_TYPE_LIKE) {
//        // show like on screen
//        
//    }
//}

// GCDAsyncUdpSocket delegate
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext
{
    if(sock != _udpSocket||_phoneComing) {
        return;
    }
    
    u_int8_t packetType = *((u_int8_t*) data.bytes);
    int32_t liveStreamID = *((int32_t*) (data.bytes+1));
    
    if(liveStreamID != [_liveStreamID intValue]) {
        return;
    }
    
    if (_delegate==nil) {
        return;
    }
    
    if (_delegateFlags.receiveUdpPacket) {
        [_delegate receiveUdpPacket:packetType data:data];
    }
    
    if(packetType == LIVE_STREAM_PACKET_TYPE_NEW_LIVE_CHUNK_READY) {
        
        int32_t currentChunkID = *((int32_t*) (data.bytes+5));
        int32_t latestChunkID = *((int32_t*) (data.bytes+9));
        
        if (_window_size == 0) {
            _window_size = *((int32_t*) (data.bytes+13));
            if (_window_size < MIN_WINDOW_SIZE) {
                _window_size = MIN_WINDOW_SIZE;
            }
            
            if (_delegateFlags.initialCommentTable) {
                [_delegate initialCommentTable:_window_size];
            }
        }
        
//        DLog(@"get %d UDP: current chunk %d, latest chunk %d, buffer %d, playing_obj=%ld", liveStreamID, currentChunkID, latestChunkID, _window_size, [_playingObjects count]);
        
        //        _test++;
        // test for batch loss
        //        if (_test>50 && _test<61) {
        //            DLog(@"lost chunk %d is ready UDP ", currentChunkID);
        //            return;
        //        } else if (_test == 99){
        //            _test = 0;
        //        } else {
        //            DLog(@"get  chunk %d is ready UDP", currentChunkID);
        //        }
        
        // test for periodical poss
        //        if (_test%_window_size) {
        //            DLog(@"lost chunk %d is ready UDP ", currentChunkID);
        //            return;
        //        } else {
        //            DLog(@"get  chunk %d is ready UDP", currentChunkID);
        //        }
        
        // test for random loss
        //        if (RANDOM_FLOAT(0, 1) < 0.8) {
        //            DLog(@"lost chunk %d is ready UDP ", currentChunkID);
        //            return;
        //        } else {
        //            DLog(@"get  chunk %d is ready UDP", currentChunkID);
        //        }
        
//        if (_windowStartPoint) {
//            [self checkWindowForChunk:currentChunkID];
//        } else {
//            [self initialWindowToNumber:currentChunkID];
//            if (currentChunkID > _window_size) {
//                [self resetWindowStartPointToNumber:currentChunkID andLastestChunkID:currentChunkID];
//            } else {
//                [self resetWindowStartPointToNumber:1 andLastestChunkID:currentChunkID];
//            }
//        }
        
        if (_windowStartPoint) {
            [self checkWindowForChunk:latestChunkID-1];
        } else {
            if (latestChunkID > _window_size) {
                [self initialWindowToNumber:MAX(latestChunkID-_window_size, currentChunkID) andLastestChunkID:latestChunkID-1];
            } else {
                [self initialWindowToNumber:1 andLastestChunkID:latestChunkID];
            }
        }
        
    } else if(packetType == LIVE_STREAM_PACKET_TYPE_KEEP_ALIVE_WATCHER) {
        
    } else if(packetType == LIVE_STREAM_PACKET_TYPE_STREAM_END){
        DLog(@"get LIVE_STREAM_PACKET_TYPE_STREAM_END");

//        [LIKE_HANDLER likeLivestreamBatchUpdate];
        if (_delegateFlags.connectionLinkFailByType) {
            [_delegate connectionLinkFailBy:LIVE_STREAM_DISCONNECT_TYPE_HOST_END];
        }
        
    } else if(packetType == LIVE_STREAM_PACKET_TYPE_LIKE) {
        // show like on screen
        
    }
}


-(void) playAnimationSound:(NSURL *)soundfileURL
{
    _theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:soundfileURL error:NULL];
    // Play the sound file
    [_theAudio play];
    

}

#pragma mark - audio related
- (void) startAudioPlayer
{
    if(_audioPlayer != nil) {
        return;
    }
    
    [self stopAudioPlayer];
    
    _audioPlayer = [PCMAudioPlayer new];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
    [[AVAudioSession sharedInstance] setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
    _audioPlayer.delegate = self;
    [_audioPlayer setup];
}

- (void) stopAudioPlayer
{
    if(_delegate==nil){
        return;
    }
    if(_audioPlayer != nil) {
        [_audioPlayer cleanup];
        _audioPlayer = nil;
    }
}

# pragma mark - PCMAudioPlayerDelegate
-(void)didPlayAudioAtAbsTime:(int) audioAbsTimestamp numberOfAudioBufferInQueue:(int)numberOfAudioBufferInQueue
{
//    DLog(@"play audio at AbsTimestamp:%d, numberOfAudioBufferInAudioQueue=%d, _queuedVideoFrameObjects=%ld", audioAbsTimestamp, numberOfAudioBufferInQueue, [_queuedVideoFrameObjects count]);

    _numberOfAudioBufferInQueue = numberOfAudioBufferInQueue;
    
    if (audioAbsTimestamp==0) {
        return;
    }
    
    NSInteger playingObjCount = [_playingObjects count];
    if (playingObjCount!=0 && numberOfAudioBufferInQueue<=MIN_AUDIO_BUFFER_COUNT_IN_QUEUE) {
        // in theory, decode next chunk when numberOfAudioBufferInQueue=0, but we pre-decode next chunk to prevent bottleneck
        [self decodeAndPlayChunk:[_playingObjects objectAtIndex:0]];
    }
    
    // unstable alert control
    if (playingObjCount==0 && numberOfAudioBufferInQueue==0) {
        // no audio frame can be played, nustable situation!
        
        if (_delegateFlags.shouldShowHUDWithType &&
            [_delegate shouldShowHUDWithType:HUDTypePoorConnection enabled:YES]) {
            
            _isResetWindow = YES;
            [self fadeVideoOutputView:NO];
        }
    }
    else {
        if (_delegateFlags.shouldShowHUDWithType && !_isFirst &&
            [_delegate shouldShowHUDWithType:HUDTypePoorConnection enabled:NO]) {
            
            [self fadeVideoOutputView:YES];
        }
    }
    
    // audio only control
    if (([_queuedVideoFrameObjects count]==0 && !_isDecodingVideo&&_chunkDownloaded>4)) {
        if (_delegateFlags.shouldShowHUDWithType &&
            [_delegate shouldShowHUDWithType:HUDTypeAudioOnly enabled:YES]) {
            
            [self fadeVideoOutputView:NO];
        }
        _audioOnlyTimes++;
        [EVENT_HANDLER addAudioOnlyCount];
        DLog(@"show audio lonly. A:%d, V:%d", _isDecodingAudio, _isDecodingVideo);
    }
    
    if (([_queuedVideoFrameObjects count]!=0 && _isDecodingVideo)) {
        
        if (_delegateFlags.shouldShowHUDWithType && !_isFirst &&
            [_delegate shouldShowHUDWithType:HUDTypeAudioOnly enabled:NO]) {
            
            [self fadeVideoOutputView:YES];
        }
//        DLog(@"hide audio lonly. A:%d, V:%d, VF obj:%ld", _isDecodingAudio, _isDecodingVideo, [_queuedVideoFrameObjects count]);
    }
    
    // To Sync AV, play the latest video frame in queue, that has nearest absTimestamp to 'audioAbsTimestamp'
    [self playVideoAtAbsTime:audioAbsTimestamp];
}

#pragma mark - video related
- (void)initDisplayTimer:(int)timestamp
{
    if (_displayTimer == nil) {
        _startTimestamp = timestamp;
        _timerStartDate = [NSDate date];
        
        _displayTimer = [CADisplayLink displayLinkWithTarget:self selector:@selector(playVideo)];
        
        // fps 30
        _displayTimer.frameInterval = 2;
        
        [_displayTimer addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
    }
}

- (void)stopDisplayTimer
{
    if (_displayTimer) {
        [_displayTimer invalidate];
        _displayTimer = nil;
        [_queuedVideoFrameObjects removeAllObjects];
        _startTimestamp = 0;
    }
}

- (void)playVideo
{
    [self playVideoAtAbsTime:((int) ([_timerStartDate timeIntervalSinceNow] * -1000.0 + _startTimestamp))];
}

// timestamp is in millisecond
- (void)playVideoAtAbsTime:(int)timestamp
{
    if (!_isConnected) {
        return;
    }


    // find the nearest VF to display
    VideoFrameObject* videoFrameForDisplay = nil;
    for(VideoFrameObject* vfObj in _queuedVideoFrameObjects) {
        if (vfObj.absTimestamp <= timestamp) {
            videoFrameForDisplay = vfObj;
        } else {
            break;
        }
    }
    
    if(videoFrameForDisplay==nil) {
//        DLog(@"no vf can be played for %d af", timestamp);
        return;
    }
    
    [_queuedVideoFrameObjects removeObjectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [_queuedVideoFrameObjects indexOfObject:videoFrameForDisplay]+1)]];

//    DLog(@"VF time:%d while playing AF %d", videoFrameForDisplay.absTimestamp, timestamp);
    
    if([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive){
        return;
    }
    
    NSArray* yuvData = videoFrameForDisplay.yuvData;
    if (_isPreview) {
        // 360 x 360
        [_displayFilter loadLuminanceData:((NSData*) yuvData[0]).bytes textureSize:CGSizeMake(FRAME_HEIGHT, FRAME_HEIGHT) textureName:@"yTexture"];
        [_displayFilter loadLuminanceData:((NSData*) yuvData[1]).bytes textureSize:CGSizeMake(FRAME_HEIGHT/2, FRAME_HEIGHT/2) textureName:@"vTexture"];
        [_displayFilter loadLuminanceData:((NSData*) yuvData[2]).bytes textureSize:CGSizeMake(FRAME_HEIGHT/2, FRAME_HEIGHT/2) textureName:@"uTexture"];
    } else {
        // 640 x 360
        [_displayFilter loadLuminanceData:((NSData*) yuvData[0]).bytes textureSize:CGSizeMake(FRAME_WIDTH, FRAME_HEIGHT) textureName:@"yTexture"];
        [_displayFilter loadLuminanceData:((NSData*) yuvData[1]).bytes textureSize:CGSizeMake(FRAME_WIDTH/2, FRAME_HEIGHT/2) textureName:@"vTexture"];
        [_displayFilter loadLuminanceData:((NSData*) yuvData[2]).bytes textureSize:CGSizeMake(FRAME_WIDTH/2, FRAME_HEIGHT/2) textureName:@"uTexture"];
    }
    
    _displayFilter.targetViewPortSize = CGSizeMake(_glVideoOutputView.drawableWidth, _glVideoOutputView.drawableHeight);
    [_displayFilter render:RENDER_MODE_RGBA rebindFrameBuffer:0];
    
    [_glVideoOutputView display];
    
    if (_isFirst) {
        _isFirst = NO;
        
        if (_delegateFlags.shouldShowHUDWithType &&
            [_delegate shouldShowHUDWithType:HUDTypeFirstTimeLoading enabled:NO]) {
            
            [self fadeVideoOutputView:YES];
        }
    }
}

-(void)dealloc
{
//    DLog(@"lvSConnWatcher dealloc");
    [self stopWatcher];
}



-(void)likeActionTo:(int)livestreamID
{
    if([MobClick isJailbroken]){
        return;
    }
    [self sendUDPPacket:LIVE_STREAM_PACKET_TYPE_LIKE toLiveStream:livestreamID];
}

- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error
{

    DLog(@"UDP掛了");
}

#pragma mark - Override

- (void)setDelegate:(id<LiveStreamWatcherConncectionDelegate>)delegate
{
    _delegate = delegate;
    _delegateFlags.connectionLinkFailByType = [delegate respondsToSelector:@selector(connectionLinkFailBy:)];
    _delegateFlags.initialCommentTable = [delegate respondsToSelector:@selector(initialCommentTable:)];
    _delegateFlags.lossRateStatus = [delegate respondsToSelector:@selector(lossRateStatus:)];
    _delegateFlags.phonecall = [delegate respondsToSelector:@selector(phonecall)];
    _delegateFlags.receiveUdpPacket = [delegate respondsToSelector:@selector(receiveUdpPacket:data:)];
    _delegateFlags.shouldShowHUDWithType = [delegate respondsToSelector:@selector(shouldShowHUDWithType:enabled:)];
}

@end