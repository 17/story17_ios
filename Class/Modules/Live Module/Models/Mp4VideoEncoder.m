#import "Mp4VideoEncoder.h"

@implementation Mp4VideoEncoder

@synthesize videoCodecContext;
@synthesize audioCodecContext;
@synthesize avOutputFormat;
@synthesize avFormatContext;
@synthesize audioStream;
@synthesize videoStream;
@synthesize videoCodec;
@synthesize audioCodec;
@synthesize videoFrame;
@synthesize audioFrame;
@synthesize movieFileName;
@synthesize videoFrameCount;
@synthesize audioFrameCount;

-(void) start:(NSString*)videoUUID
{
    av_log_set_level(AV_LOG_QUIET);

//    av_log_set_level(AV_LOG_VERBOSE);
    
    int result;
    
//    movieFileName = [SINGLETON generateRandomFileNameWithExtension:@"mp4"];
    movieFileName = [NSString stringWithFormat:@"%@.mp4",videoUUID];
    
    [FILE_MANAGER removeItemAtPath:GET_LOCAL_FILE_PATH(movieFileName) error:nil];
    char* filename = (char*) [GET_LOCAL_FILE_PATH(movieFileName) cStringUsingEncoding:NSASCIIStringEncoding];
    
    audioFrameCount = 0;
    
    av_register_all();
    
    avformat_alloc_output_context2(&avFormatContext, NULL, NULL, filename);
    avFormatContext->oformat->video_codec = CODEC_ID_H264;
    avFormatContext->oformat->audio_codec = CODEC_ID_AAC;
    avOutputFormat = avFormatContext->oformat;
    
    // add video stream
    videoCodec = avcodec_find_encoder(avOutputFormat->video_codec);
    videoStream = avformat_new_stream(avFormatContext, videoCodec);
    videoStream->id = avFormatContext->nb_streams-1;
    videoStream->time_base = (AVRational) {1, MOVIE_FRAME_RATE};
    videoCodecContext = videoStream->codec;
    videoCodecContext->time_base = (AVRational) {1, MOVIE_FRAME_RATE};
    videoCodecContext->codec_id = avOutputFormat->video_codec;
    videoCodecContext->bit_rate = MOVIE_VIDEO_BIT_RATE;
    videoCodecContext->width = MOVIE_WIDTH;
    videoCodecContext->height = MOVIE_HEIGHT;
    videoCodecContext->pix_fmt = PIX_FMT_YUV420P;
    videoCodecContext->rc_strategy = 1;
//    videoCodecContext->rc_strategy = 0;
    videoCodecContext->thread_count = 2;
    
    // rotate video stream 90 degrees
    av_dict_set(&videoStream->metadata, "rotate", "90", 0);

    // add audio stream
    audioCodec = avcodec_find_encoder(avOutputFormat->audio_codec);
    audioStream = avformat_new_stream(avFormatContext, audioCodec);
    audioStream->id = avFormatContext->nb_streams-1;
    audioCodecContext = audioStream->codec;
    audioCodecContext->codec_id = avOutputFormat->audio_codec;
    audioCodecContext->bit_rate = MOVIE_AUDIO_BIT_RATE;
    audioCodecContext->sample_rate = MOVIE_AUDIO_SAMPLE_RATE;
    audioCodecContext->sample_fmt = AV_SAMPLE_FMT_FLTP;
    audioCodecContext->channels = 1;
    audioCodecContext->channel_layout = AV_CH_LAYOUT_MONO;
    audioCodecContext->thread_count = 2;

    // check if need add global header
    if (avFormatContext->oformat->flags & AVFMT_GLOBALHEADER) {
        videoCodecContext->flags |= CODEC_FLAG_GLOBAL_HEADER;
        audioCodecContext->flags |= CODEC_FLAG_GLOBAL_HEADER;
    }
    
    // open video codec
    result = avcodec_open2(videoCodecContext, videoCodec, NULL);
    
    // alloc reusable video frame buffer
    videoFrame = av_frame_alloc();
    videoFrame->format = videoCodecContext->pix_fmt;
    videoFrame->width  = videoCodecContext->width;
    videoFrame->height = videoCodecContext->height;
    videoFrame->linesize[0] = videoCodecContext->width;
    videoFrame->linesize[1] = videoCodecContext->width/2;
    videoFrame->linesize[2] = videoCodecContext->width/2;
    
    // open audio coded
    audioCodecContext->strict_std_compliance = -2; // turn off experimental codec warning
    result = avcodec_open2(audioCodecContext, audioCodec, NULL);
    
    // alloc reusable audio frame buffer
    audioFrame = av_frame_alloc();
    
    audioFrame->nb_samples = audioCodecContext->frame_size; // 1024
    audioFrame->format = AV_SAMPLE_FMT_FLTP;
    audioFrame->sample_rate = MOVIE_AUDIO_SAMPLE_RATE;
    audioFrame->channel_layout = AV_CH_LAYOUT_MONO;
    audioFrame->channels = 1;
    
    av_dump_format(avFormatContext, 0, filename, 1);

    if (!(avOutputFormat->flags & AVFMT_NOFILE)) {
        avio_open(&avFormatContext->pb, filename, AVIO_FLAG_WRITE);
    }
    
    // write header
    result = avformat_write_header(avFormatContext, NULL);
}

-(void) encodeAudioData:(NSData*) audioData
{
    int result;
    
    result = avcodec_fill_audio_frame(audioFrame, audioCodecContext->channels, audioCodecContext->sample_fmt, audioData.bytes, (int) audioData.length, 0);
    
    if(result<0) {
        DLog(@"FILL AUDIO FRAME FAIL!");
        return;
    }
    
    AVPacket audioPacket = {0};
    av_init_packet(&audioPacket);
    audioPacket.data = NULL;
    audioPacket.size = 0;
    
    int gotPacket;
    result = avcodec_encode_audio2(audioCodecContext, &audioPacket, audioFrame, &gotPacket);
    
    if(result<0) {
        DLog(@"ENCODE AUDIO FRAME FAIL!");
        return;
    }
    
    audioPacket.stream_index = audioStream->index;
    audioFrameCount++;
    
    result = av_interleaved_write_frame(avFormatContext, &audioPacket);
        
    if(result<0) {
        DLog(@"WRITE AUDIO ERROR!");
    }
}

-(void) encodeVideoWithYData:(u_int8_t*) yData andCbData:(u_int8_t*) cbData andCrData:(u_int8_t*) crData
{
    AVPacket videoPacket;
    av_init_packet(&videoPacket);
    videoPacket.data = NULL;
    videoPacket.size = 0;
    
    videoFrame->data[0] = yData;
    videoFrame->data[1] = cbData;
    videoFrame->data[2] = crData;
    
    /* encode the image */
    int gotPacket;

    int result = avcodec_encode_video2(videoCodecContext, &videoPacket, videoFrame, &gotPacket);

    if(result<0) {
        DLog(@"VIDEO ENCODE ERROR!");
        return;
    }
    
//    DLog(@"videoPacket.size: %d", videoPacket.size);
    
    videoPacket.stream_index = videoStream->index;
    videoFrameCount++;
    
    result = av_interleaved_write_frame(avFormatContext, &videoPacket);
    av_free_packet(&videoPacket);

    if(result<0) {
        DLog(@"WRITE VIDEO ERROR!");
    }
}

-(NSString*) finish
{
    av_write_trailer(avFormatContext);

    if (!(avOutputFormat->flags & AVFMT_NOFILE)) {
        avio_close(avFormatContext->pb);
    }
    
    // close video
    av_frame_free(&videoFrame);
    avcodec_close(videoCodecContext);
    
    // close audio
    av_frame_free(&audioFrame);
    avcodec_close(audioCodecContext);

    avformat_free_context(avFormatContext);

    return movieFileName;
}

-(float) getAudioTime
{
        return (float) av_stream_get_end_pts(audioStream)/44100.0;
}

-(float) getVideoTime
{
    return (float) av_stream_get_end_pts(videoStream)/512.0/24.0;
}

@end
