#import "MyOpusEncoder.h"

@implementation MyOpusEncoder

@synthesize encoder;
@synthesize audioEncodeBuffer;

- (id) init {
    self = [super init];
    
    if(!self) {
        return nil;
    }
    
    audioEncodeBuffer = malloc(512);
    
    int errCode;
    
    encoder = opus_encoder_create(48000, 1, OPUS_APPLICATION_AUDIO, &errCode);
    
    int bitRate = HIGH_LIVESTREAM_AUDIO_BIT_RATE;
//    int liveStreamQuality = [[DEFAULTS objectForKey:LIVESTREAM_QUALITY] intValue];
    
//    if(liveStreamQuality==LIVESTREAM_QUALITY_FULL_HD) {
//        bitRate = FULL_HD_LIVESTREAM_AUDIO_BIT_RATE;
//    } else
//        
//    if(liveStreamQuality==LIVESTREAM_QUALITY_HIGH) {
//        bitRate = HIGH_LIVESTREAM_AUDIO_BIT_RATE;
//    } else if(liveStreamQuality==LIVESTREAM_QUALITY_MEDIUM) {
//        bitRate = MEDIUM_LIVESTREAM_AUDIO_BIT_RATE;
//    } else if(liveStreamQuality==LIVESTREAM_QUALITY_LOW) {
//        bitRate = LOW_LIVESTREAM_AUDIO_BIT_RATE;
//    }
    
//    if(IS_USING_WIFI_NETWORK || IS_USING_4G_NETWORK) {
//        bitRate = 48000;
//    } else {
//        bitRate = 16000;
//    }
    
    DLog(@"AUDIO BITRATE: %d", bitRate);

    errCode = opus_encoder_ctl(encoder, OPUS_SET_BITRATE(bitRate));
    errCode = opus_encoder_ctl(encoder, OPUS_SET_COMPLEXITY(10));
    errCode = opus_encoder_ctl(encoder, OPUS_SET_VBR(1));
    errCode = opus_encoder_ctl(encoder, OPUS_SET_VBR_CONSTRAINT(0));
//    errCode = opus_encoder_ctl(encoder, OPUS_SET_INBAND_FEC(1));
    errCode = opus_encoder_ctl(encoder, OPUS_SET_PACKET_LOSS_PERC(0));
    errCode = opus_encoder_ctl(encoder, OPUS_SET_MAX_BANDWIDTH(OPUS_BANDWIDTH_FULLBAND));
    errCode = opus_encoder_ctl(encoder, OPUS_SET_EXPERT_FRAME_DURATION(OPUS_FRAMESIZE_40_MS));
    
    return self;
}

-(NSData*) encodeAudioWithData:(NSData*) audioData
{
    // audioData must be of length 3840 bytes in 40ms mode!
    if(audioData==nil || audioData.length!=3840) {
        return nil;
    }

    int encodedDataLength = opus_encode(encoder, audioData.bytes, 1920, audioEncodeBuffer, 512); // 1920 samples

    NSData* encodedAudioData;

    if(encodedDataLength>0) {
        encodedAudioData = [NSData dataWithBytes:audioEncodeBuffer length:encodedDataLength];
    }

    return encodedAudioData;
}

-(void)dealloc
{
    opus_encoder_destroy(encoder);
    
    free(audioEncodeBuffer);
}

@end
