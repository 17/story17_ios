#import "Constant.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <libavutil/opt.h>
#include <libavutil/mathematics.h>
#include <libavformat/avformat.h>
#include <libswresample/swresample.h>

@interface Mp4VideoEncoder : NSObject

@property AVOutputFormat* avOutputFormat;
@property AVFormatContext* avFormatContext;
@property AVStream* audioStream;
@property AVStream* videoStream;
@property AVCodecContext* videoCodecContext;
@property AVCodecContext* audioCodecContext;
@property AVCodec* audioCodec;
@property AVCodec* videoCodec;
@property AVFrame* videoFrame;
@property AVFrame* audioFrame;
@property int audioFrameCount;
@property int videoFrameCount;
@property (nonatomic, strong) NSString* movieFileName;

-(void) start:(NSString*)videoUUID;
-(void) encodeAudioData:(NSData*) audioData;
-(void) encodeVideoWithYData:(u_int8_t*) yData andCbData:(u_int8_t*) cbData andCrData:(u_int8_t*) crData;
-(NSString*) finish;
-(float) getAudioTime;
-(float) getVideoTime;

@end
