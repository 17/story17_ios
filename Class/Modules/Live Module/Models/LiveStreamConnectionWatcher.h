//
//  LiveStreamConnectionWatcher.h
//  Story17
//
//  Created by POPO on 7/15/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UDPSocket.h"
#import "GCDAsyncSocket.h"
#import "PCMAudioPlayer.h"
#import "MyFilter.h"
#import "LivestreamConstant.h"
#import "GCDAsyncUdpSocket.h"
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>
#import "HUDView.h"

@class GLKView;

/*  */
@protocol LiveStreamWatcherConncectionDelegate<NSObject>

@optional
- (void) connectionLinkFailBy:(LIVE_STREAM_DISCONNECT_TYPE)type;
- (void) receiveUdpPacket:(LIVE_STREAM_PACKET_TYPE)type data:(NSData*)data;
- (void) lossRateStatus:(float)lossrate;
- (BOOL) shouldShowHUDWithType:(HUDType)HUDType enabled:(BOOL)enabled;
- (void) initialCommentTable:(NSInteger)windowSize;
- (void) phonecall;
- (void) livestreamViewerKilled;
@end

@interface LiveStreamConnectionWatcher : NSObject <UDPSocketDelegate, PCMAudioPlayerDelegate,GCDAsyncUdpSocketDelegate>

@property int32_t windowStartPoint;
@property (nonatomic, strong, readonly) GLKView* glVideoOutputView;
@property (readonly) BOOL isConnected;
@property (readonly) int startTimestamp;
@property (readonly) int totalFrame;
@property (readonly) int durationTimestamp;
@property (readonly) int currentAbsTimestamp; // for watcher
@property (nonatomic, strong, readonly) NSString* colorCode;
@property (nonatomic,strong) CTCallCenter *callCenter;
@property (nonatomic,strong) AVAudioSession *session;
@property (nonatomic,strong) NSTimer* PhoneCallTimer;
@property (nonatomic,strong) AVAudioPlayer * theAudio;
@property (nonatomic,weak) UILabel* staticLabel;
@property (nonatomic,weak) UILabel* staticLabel2;


// mute audio and crop to square for preview
@property (readonly) BOOL isPreview;
@property (nonatomic) int Count;
// when in bg mode, no need for render video
@property BOOL isVideoOn;

@property LiveStreamObject * liveStream;
@property (weak, nonatomic) id<LiveStreamWatcherConncectionDelegate> delegate;

- (void)connectToLive:(NSString*)lvID isPreview:(BOOL)isPreview withCompletion:(void(^)(LIVE_STREAM_CONNECT_TYPE status))callback;
- (void)disconnect;
- (void)stopWatcher;
- (void)likeActionTo:(int)livestreamID;
- (void)playAnimationSound:(NSURL*)soundfileURL;
@end