#import "Constant.h"

@interface AudioFrameObject : NSObject

@property (nonatomic, strong) NSData* audioData;
@property int absTimestamp;

-(id) init;

@end
