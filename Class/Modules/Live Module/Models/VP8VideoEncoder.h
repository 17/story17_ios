#import "Constant.h"

#include "libavcodec/avcodec.h"
#include "libavutil/opt.h"
#include "libavformat/avformat.h"
#import <VPX/vpx/vp8.h>
#import "codec_api.h"
#import "codec_app_def.h"
#import "codec_ver.h"
#import "codec_def.h"

@interface VP8VideoEncoder : NSObject

@property AVCodec* codec;
@property AVCodecContext* codecContext;
@property AVFrame* videoFrame;

-(void) reset;
-(NSData*) encodeWithYData:(u_int8_t*) yData andCbData:(u_int8_t*) cbData andCrData:(u_int8_t*) crData;

@end
