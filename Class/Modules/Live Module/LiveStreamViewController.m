#import "LiveStreamViewController.h"
#import "Constant.h"
#import "UIPlaceholderTextView.h"
#import "LiveStreamTableView.h"
#import "LiveStreamChatMessage.h"
#import "MessageInputBar.h"
#import "LiveStreamConnectionWatcher.h"
#import "LivestreamBroadcasterController.h"
#import "MyScene.h"
#import "GiftPurchesInLivestreamViewController.h"
#import "IAPManager.h"
#import "UIImage+Helper.h"
#import <UMengAnalytics/MobClick.h>
#import "CustomTabbarController.h"
#import "AutoHintView.h"
#import "HUDView.h"
#import "ReceivedGiftViewController.h"
#import "LiveStreamHeaderViewController.h"
#import "LoginHandler.h"

static CGFloat const kAutoHintViewHeight = 30.0f;
static CGFloat const kExitButtonSize = 50;
static CGFloat const kLeftMargin = 30;

@interface LiveStreamViewController()<AutoHintViewDelegate, LiveStreamHeaderViewControllerDelegate, LiveViewerDelegate, LiveStreamTableViewDataSource, LiveStreamTableViewDelegate, LiveStreamWatcherConncectionDelegate, BroadcasterDelegate, MFMailComposeViewControllerDelegate, GiftPurchaseInLive, UIGestureRecognizerDelegate, UITextViewDelegate, UIPopoverPresentationControllerDelegate>
{
    struct {
        unsigned int beginInteractiveTransitioning : 1;
        unsigned int updateInteractiveTransitionShouldComplete : 1;
        unsigned int shouldCompleteInteractiveTransition : 1;
        unsigned int completeInteractiveTransition : 1;

    } _delegateFlags;
}
@property (nonatomic, assign) int ari;

@property (nonatomic, assign) BOOL animating;
@property (nonatomic, assign) BOOL previewAnimating;

@property (nonatomic, assign) int nextAnimate;
@property (nonatomic, assign) int mostExpensiveAnimatePrice;
@property (nonatomic, strong) IAPManager* purchaseManager;

/*admin Button*/
@property (nonatomic, strong) UIButton* killLiveButton;

/*gift textview*/
@property (nonatomic, strong) UITextView* gifttext;
@property (nonatomic, strong) UILabel* textRemainWarning;
@property (nonatomic, strong) UIButton* textGiftSendButton;
@property (nonatomic, strong) UIButton* textGiftCancelButton;

/* Livestream Mode */
@property (nonatomic, strong) NSMutableArray *giftAnimate;
@property (nonatomic, strong) NSMutableArray *giftAnimateQuene;
@property (nonatomic, strong) NSMutableArray *giftPreviewAnimateQuene;

/* UI Related */
@property (nonatomic, strong) LiveStreamTableView* lvTable;
@property (nonatomic, strong) NSMutableArray *liveStreamCommentObjects;
@property (nonatomic, strong) UILabel *staticLabel;
@property (nonatomic, strong) UILabel *staticLabel2;
@property (nonatomic, strong) GiftPurchesInLivestreamViewController* purchaseGiftViewController;

/* Fetching data status */
@property (nonatomic, assign) BOOL isFirstFetchInfo;
@property (nonatomic, assign) BOOL isFetchingInfo;
@property (nonatomic, assign) BOOL isFetchingComment;
@property (nonatomic, assign) BOOL isBackground;

@property (nonatomic, assign) int lastCommentTime;
@property (nonatomic, assign) int lastSelfCommentTime;

@property (nonatomic, assign) float cellTotalHeight;
@property (nonatomic, strong) MessageInputBar* messageInputBar;
@property (nonatomic, strong) NSString* searchString;
@property (nonatomic, assign) BOOL POP;
@property (nonatomic, strong) NSMutableArray* privacySettingUser;
@property (nonatomic, strong) UIImageView* animationImageView;
@property (nonatomic, strong) UIView* blackView;
@property (nonatomic, strong) UIImageView* mask;
@property (nonatomic, strong) UIImageView* textView;

@property (nonatomic, strong) UIImageView* giftImage;
@property (nonatomic, strong) UIImageView* giftHeadImage;
@property (nonatomic, strong) UIImageView* textGiftHeadImage;
@property (nonatomic, strong) UIView* textGiftBgView;

@property (nonatomic, strong) UILabel* nameLabel;
@property (nonatomic, strong) UIImageView* bgImageView;
@property (nonatomic, strong) UILabel* chatMessageLabel;

@property (nonatomic, strong) UIPopoverController *popOverMenu;
@property (nonatomic, strong) LivestreamBroadcasterController* broadcasterController;
@property (nonatomic, strong) LiveStreamConnectionWatcher* lvConnWatcher;

/* UI control related */
@property (nonatomic, assign) BOOL isLiveStreamStart;
@property (nonatomic, assign) BOOL isLocationSharedOn;
@property (nonatomic, assign) LiveStreamChatMode chatMode;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (nonatomic, copy) NSString *hexColorCode;  /* Watcher hexcolor */

@property (nonatomic, assign) int32_t likeCount;

@property (nonatomic, assign) int liveStreamStartTime;
@property (nonatomic, assign) int liveStreamStopTime;

// network related
@property (nonatomic,strong) NSTimer* heartTimer;
@property (nonatomic,strong) NSTimer* giftTimer;

// control btns
@property (nonatomic, strong) UIButton* exitButton;
@property (nonatomic, strong) NSArray* giftArray;
@property (nonatomic, strong) UIPlaceholderTextView* liveStreamSubjectTextView;
@property (nonatomic, strong) UILabel* chatModeSwitchReminderLabel;
@property (nonatomic, strong) UIButton* liveStreamStartButton;

@property (nonatomic, strong) UIButton* chatStartButton;
@property (nonatomic, strong) UIButton* torchModeSwitchButton;
@property (nonatomic, strong) UIButton* cameraSwitchButton;
@property (nonatomic, strong) UIButton* fullScreenSwitchButton;
@property (nonatomic, strong) UIButton* likeButton;
@property (nonatomic, strong) UIButton* restreamButton;
@property (nonatomic, strong) UIButton* giftButton;

@property (nonatomic, assign) int keyboardHeight;

@property (nonatomic, assign) int rateforheart;

// UI related
@property (nonatomic, strong) NSMutableArray* chatMessages;
@property (nonatomic, strong) NSMutableArray* receivedMessageID;
@property (nonatomic, assign) UIBackgroundTaskIdentifier backgroundTask;

@property (nonatomic, strong) UIImageView* blurBgImageView;
@property (nonatomic, strong) HUDView *hudView;
@property (nonatomic, assign) int changerate;
@property (nonatomic, strong) UIView* topView;
@property (nonatomic, strong) UILabel* warningLabel;
@property (nonatomic, strong) UILabel* descriptLabel;
@property (nonatomic, strong) UILabel* linkLabel;
@property (nonatomic, assign) BOOL hasRestream;

/* SpriteKit kit animation */
@property (nonatomic, strong) MyScene* scene;
@property (nonatomic, strong) MyScene* animateScene;
@property (nonatomic, strong) SKView* skView;
@property (nonatomic, assign) BOOL privacypresent;

@property (nonatomic, assign) int heartCount;

@property (nonatomic, assign) CGPoint startPanPoint;
@property (nonatomic, assign) BOOL shouldLoadStream;
@property (nonatomic, strong) UIPanGestureRecognizer *panToFullScreenGR;
@property (nonatomic, strong) UIPanGestureRecognizer *panToLeaveGR;
@property (nonatomic, strong) AutoHintView *autoHintView;

// Header
@property (nonatomic, strong) LiveStreamHeaderViewController *liveStreamHeaderViewController;

@end

@implementation LiveStreamViewController

// layout constants
#define LIVE_STREAM_VIEW_PADDING 8
#define LIVE_STREAM_BUTTON_WIDTH SCREEN_WIDTH*4/5    // start button width
#define LIVE_STREAM_BUTTON_HEIGHT 40
#define LIVE_STREAM_FONT_SIZE 18
#define fetchNum 10
#define fetchViewerNum 10
#define maxTableHeight 90
#define CHECKING_WINDOW_SIZE 100
#define LIVE_STREAM_MAX_CHAT_COMMENT_COUNT 100

#define PLANE_Y 0
#define PLANE_CBCR 1

#define USE_SPRITEKIT SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")

#define COMMENT_INTERVAL 8
#define MAX_COMMENT_COUNT 50


#pragma mark - Life Cycle

- (id)init
{
    self = [super init];
    
    if(self){
        _canPresentView = YES;
        _shouldLoadStream = YES;
        _giftAnimate = CREATE_MUTABLE_ARRAY;
        _giftAnimateQuene = CREATE_MUTABLE_ARRAY;
        _giftPreviewAnimateQuene = CREATE_MUTABLE_ARRAY;
        //文字禮物相關
        self.liveStreamSubjectTextView = [[UIPlaceholderTextView alloc] initWithFrame:CGRectMake(0, LIVE_STREAM_VIEW_PADDING, SCREEN_WIDTH-LIVE_STREAM_BUTTON_HEIGHT-2*LIVE_STREAM_VIEW_PADDING, 100)];

        _textView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"btn_whiteline_r"] resizableImageWithCapInsets:UIEdgeInsetsMake(22, 22, 22, 22)]];
        _textGiftBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];

        _textView.frame = CGRectMake(SCREEN_WIDTH/7, SCREEN_HEIGHT/3, SCREEN_WIDTH*5/7, SCREEN_HEIGHT/4);
        _gifttext = [[UITextView alloc]initWithFrame:CGRectMake(10, 25, _textView.frame.size.width-20, _textView.frame.size.height-50)];
        [_gifttext setBackgroundColor:[UIColor clearColor]];
        _textGiftCancelButton = [[UIButton alloc]init];
        _textGiftSendButton = [[UIButton alloc]init];
        _textRemainWarning = [[UILabel alloc]initWithFrame:CGRectMake(_textView.frame.size.width-60, _textView.frame.size.height-30, 60, 20)];
        
        _textGiftHeadImage = [[UIImageView alloc]init];
        CGRect rect = CGRectMake(6, 10, 40, 40);
        [_textGiftHeadImage setFrame:rect];
        _textGiftHeadImage.center = CGPointMake(_textView.frame.size.width/2, 0);
        _textGiftHeadImage.layer.masksToBounds = YES;
        _textGiftHeadImage.layer.cornerRadius = 40 / 2.0;
        _textGiftHeadImage.layer.borderWidth = 3.0;
        _textGiftHeadImage.layer.borderColor = [WHITE_COLOR CGColor];
        //文字禮物相關

        if(USE_SPRITEKIT) {
            CGRect screenFrame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            _skView = [[SKView alloc]initWithFrame:screenFrame];
            [_skView presentScene:_scene];
        }
        _mask = [[UIImageView alloc]init];
        _giftImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"giftpoint_%d",rand()%7+1]]];
        _giftHeadImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"giftpoint_%d",rand()%7+1]]];
      
        
        [_giftImage setFrame:CGRectMake(5, 7, 13, 15)];
        _bgImageView = [[UIImageView alloc]initWithImage:[IMAGE_FROM_BUNDLE(@"btn_green_circle") resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 32)]];
        [_bgImageView setFrame:CGRectMake(5, 100, 100, 30)];
        _nameLabel = [UILabel new];
        [_nameLabel setTextColor:WHITE_COLOR];
        _nameLabel.font = BOLD_FONT_WITH_SIZE(16);
        _nameLabel.userInteractionEnabled = YES;
        [_nameLabel setFrame:CGRectMake(_giftImage.frame.origin.x+17, 5, 15, 20)];
        [_nameLabel setText:@"test17"];
        [_nameLabel sizeToFit];
        _chatMessageLabel = [UILabel new];

        _chatMessageLabel.font = SYSTEM_FONT_WITH_SIZE(16);
        [_chatMessageLabel setFrame:CGRectMake(_nameLabel.frame.origin.x+_nameLabel.frame.size.width+5, 5, 100, 20)];
        [_chatMessageLabel setText:@"送了雪花愛心"];
        [_chatMessageLabel setTextColor:WHITE_COLOR];
        [_chatMessageLabel sizeToFit];
        
        [_bgImageView setFrame:CGRectMake(_bgImageView.frame.origin.x, _bgImageView.frame.origin.y, _chatMessageLabel.frame.origin.x+_chatMessageLabel.frame.size.width+10, _bgImageView.frame.size.height)];
        
        _hudView = [[[NSBundle mainBundle] loadNibNamed:@"HUDView" owner:self options:nil] firstObject];
        _exitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _liveStreamMode = LiveStreamModeWatch;
        _lastSelfCommentTime = CURRENT_TIMESTAMP-COMMENT_INTERVAL;
        _warningLabel = [UILabel new];
        _descriptLabel = [UILabel new];
        _linkLabel = [UILabel new];
        _topView = [[UIView alloc]initWithFrame:CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT+10)];

        _liveStreamStartButton = [UIButton buttonWithType:UIButtonTypeCustom];

        _purchaseManager = [SINGLETON iapManager];
        _lvConnWatcher = [LiveStreamConnectionWatcher new];
        _broadcasterController = [LivestreamBroadcasterController new];
        
        _topView = [[UIView alloc]initWithFrame:CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT+10)];
        _purchaseGiftViewController = [[GiftPurchesInLivestreamViewController alloc]init];

        _lvTable = [[LiveStreamTableView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-50, SCREEN_WIDTH,_cellTotalHeight)];
        _likeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _fullScreenSwitchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _restreamButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _giftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _messageInputBar = [[MessageInputBar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 44.0)];
        _messageInputBar.messageMode = LIVE_COMMENT_MESSAGE;
        _torchModeSwitchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _chatStartButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        _panToFullScreenGR = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(onPanToFullScreen:)];
        _panToFullScreenGR.delegate = self;
        
        _panToLeaveGR = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(onPanToLeave:)];
        _panToLeaveGR.delegate = self;
        
        _blurBgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _blurBgImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        _liveStreamHeaderViewController = [[LiveStreamHeaderViewController alloc] init];
        _liveStreamHeaderViewController.delegate = self;
        
        _killLiveButton = [ThemeManager getGreenCircleBtn];
        _killLiveButton.frame = CGRectMake(0, 0, SCREEN_WIDTH/4, 30);
        _killLiveButton.layer.cornerRadius = 10;
        _killLiveButton.titleLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_killLiveButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
//        [_killLiveButton setBackgroundColor:WHITE_COLOR];
        [_killLiveButton addTarget:self action:@selector(adminKillLiveBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        _receivedMessageID = CREATE_MUTABLE_ARRAY;
        if ([_giftArray count] == 0) {
            [API_MANAGER getGiftListWithCompletion:^(BOOL success, NSArray *gifts) {
                if (success) {
                    if ([gifts count]>0) {
                        dispatch_async(GLOBAL_QUEUE, ^{
                            [GiftObject getGiftZipFile:gifts index:0 firstCatch:YES];
                        });

                    }
                    
                    DLog(@"拉禮物LIST");
                    _giftArray = [NSArray arrayWithArray:gifts];
                }
            }];
            
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /* Defaults to High Quality */
    
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [_liveStreamStartButton setSelected:NO];
    if(_liveStreamMode == LiveStreamModeBroadcast) {
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
        [[AVAudioSession sharedInstance] setPreferredSampleRate:44100.0 error:NULL];
        [[AVAudioSession sharedInstance] setActive:NO error:nil];
    }
    
    if(IS_USING_WIFI_NETWORK || IS_USING_4G_NETWORK) {
        [DEFAULTS setObject:INT_TO_NUMBER(LIVESTREAM_QUALITY_VERY_HIGH) forKey:LIVESTREAM_QUALITY];
        [DEFAULTS synchronize];
    }else{
        [DEFAULTS setObject:INT_TO_NUMBER(LIVESTREAM_QUALITY_VERY_LOW) forKey:LIVESTREAM_QUALITY];
        [DEFAULTS synchronize];
    }
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];

    self.mostExpensiveAnimatePrice = 0;
    if (!_shouldLoadStream) {
        return;
    }
    DLog(@"livestreamview will appear");
    self.animating = NO;
    self.hasRestream = NO;
    self.canPresentView = NO;
    [DEFAULTS setObject:INT_TO_STRING(_canPresentView) forKey:NOTLIVESTREAMING];
    [DEFAULTS synchronize];
    
    _enterModeForET=@"";
        switch (_liveStreamMode) {
            case LiveStreamModeBroadcast:
            {
                _enterModeForET=@"BroadCaster";
            }
                break;
                
            case LiveStreamModeWatch:
            {
                
                switch (_livestreamEnterMode) {
                    case LiveStreamEnterModeFollowed:
                    {
                        _enterModeForET=@"Followed";

                    }
                        break;
                        
                    case LiveStreamEnterModeHotLive:
                    {
                        _enterModeForET=@"HotLive";

                    }
                        break;
                    case LiveStreamEnterModeRestreamed:
                    {
                        _enterModeForET=@"Restream";
                    }
                        break;
                    case LiveStreamEnterModeWebPage:
                    {
                        _enterModeForET=@"web";
                    }
                        break;
                    case LivestreamEnterModeGuest:
                    {
                        _enterModeForET=@"Guest";

                    }
                        break;
                    case LivestreamEnterModeShared:
                    {
                        _enterModeForET=@"Shared";

                    }
                        break;

                }
            }
                break;
        }
    if (_liveStream!=nil) {
        
        if (_liveStreamMode==LiveStreamModeBroadcast) {
            [EVENT_HANDLER addEventTracking:@"EnterPublishLivePage" withDict:nil];

        }else{
            [EVENT_HANDLER addEventTracking:@"EnterLivePage" withDict:@{@"targetUserID":_liveStream.userID,@"liveStreamID":_liveStream.liveStreamID,@"from":_enterModeForET,@"caption":_liveStream.caption}];
        }
    }

    _enterPageTimeStamp=CURRENT_TIMESTAMP;
    _enterFullScreenCount=0;
    _commentCount=0;
    _likeCountForET=0;
    _consumePoint=0;

    [_liveStreamSubjectTextView becomeFirstResponder];
    _lvConnWatcher.windowStartPoint = 0;
    [DEFAULTS setObject:@1 forKey:IS_ON_LIVESTREAMING];
    [DEFAULTS synchronize];
    _liveStream.liveViewerCount = 0;
    
    /* Fetching data status */
    self.isFirstFetchInfo = YES;
    self.isFetchingComment = NO;
    self.isFetchingInfo = NO;
    self.heartCount = 0;
    self.isBackground = NO;
    self.changerate = 0;
    
    /* tableview height */
    self.cellTotalHeight = 0.0f;
    self.backgroundTask = UIBackgroundTaskInvalid;
    self.heartTimer = [NSTimer scheduledTimerWithTimeInterval:0.035 target:self selector:@selector(randomHeart) userInfo:nil repeats:YES];
    self.giftTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(giftCatch) userInfo:nil repeats:YES];

    if (_liveStreamMode == LiveStreamModeBroadcast) {
        [_liveStreamSubjectTextView becomeFirstResponder];
        [_broadcasterController reinit];
    }
    
    self.privacypresent = NO;
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    [DEFAULTS setObject:INT_TO_NUMBER(LIVESTREAM_QUALITY_HIGH) forKey:LIVESTREAM_QUALITY];
    [DEFAULTS synchronize];
    
    self.view.backgroundColor = BLACK_COLOR;

    if(USE_SPRITEKIT){
        self.scene = [MyScene sceneWithSize:_skView.bounds.size];
        self.animateScene = [MyScene sceneWithSize:_skView.bounds.size];
        _skView.allowsTransparency = YES;
        _scene.scaleMode = SKSceneScaleModeResizeFill;
        _animateScene.scaleMode = SKSceneScaleModeResizeFill;

        [_skView setBackgroundColor:[UIColor clearColor]];
        _skView.userInteractionEnabled = YES;
        [_skView presentScene:_scene];
    }
    
    switch (_liveStreamMode) {
        case LiveStreamModeBroadcast:
        {
            self.view.backgroundColor = BLACK_COLOR;
            _broadcasterController.delegate = self;
            [self.view addSubview:_broadcasterController.glVideoOutputView];
            
            UISwipeGestureRecognizer* leftPanRecog = [[UISwipeGestureRecognizer alloc] bk_initWithHandler:_broadcasterController.panRecogHandler];
            leftPanRecog.direction = UISwipeGestureRecognizerDirectionLeft;
            UISwipeGestureRecognizer* rightPanRecog = [[UISwipeGestureRecognizer alloc] bk_initWithHandler:_broadcasterController.panRecogHandler];
            rightPanRecog.direction = UISwipeGestureRecognizerDirectionRight;
            
            
            if (USE_SPRITEKIT) {
                [self.view addSubview:_skView];
                [_skView addGestureRecognizer:leftPanRecog];
                [_skView addGestureRecognizer:rightPanRecog];
                [_skView removeGestureRecognizer:_panToFullScreenGR];
                [_skView removeGestureRecognizer:_panToLeaveGR];
            }
            else {
                [_broadcasterController.glVideoOutputView addGestureRecognizer:leftPanRecog];
                [_broadcasterController.glVideoOutputView addGestureRecognizer:rightPanRecog];
                [_broadcasterController.glVideoOutputView removeGestureRecognizer:_panToFullScreenGR];
                [_broadcasterController.glVideoOutputView removeGestureRecognizer:_panToLeaveGR];
            }
            
            [self buildSettingView];
#ifdef SANDBOX_MODE
            
            self.staticLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 210, SCREEN_WIDTH-20, 30)];
            [_staticLabel2 setBackgroundColor:[UIColor blackColor]];
            [_staticLabel2 setTextColor:WHITE_COLOR];
            _staticLabel2.font = SYSTEM_FONT_WITH_SIZE(12);
            _broadcasterController.staticLabel2 = _staticLabel2;
            [self.view addSubview:_staticLabel2];
#endif
        }
            break;
            
        case LiveStreamModeWatch:
        {
            _lvConnWatcher.delegate = self;
            
            __weak __typeof(self)weakSelf = self;
            [_blurBgImageView setImageWithURLRequest:[NSURLRequest requestWithURL:S3_FILE_URL(_liveStream.user.picture)] placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s") success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                weakSelf.blurBgImageView.image = [UIImage imageByApplyingBlurToImage:image withRadius:SCREEN_WIDTH/64 tintColor:[UIColor colorWithRed:0.f green:0.f blue:0.f alpha:0.0f] saturationDeltaFactor:1.f maskImage:nil];
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                NSLog(@"[LiveStreamViewController] Load profile image error - %@", error);
            }];
            
            [self.view addSubview:_blurBgImageView];
            [self.view addSubview:_lvConnWatcher.glVideoOutputView];
            
            _hudView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            _hudView.currentHUDType = HUDTypeNone;
            [self.view addSubview:_hudView];
            
            [self connectToLiveStream];
            
            UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTap:)];
            
            if(USE_SPRITEKIT){
                [self.view addSubview:_skView];
                
                [_skView addGestureRecognizer:tapGesture];
                [_skView addGestureRecognizer:_panToFullScreenGR];
                [_skView addGestureRecognizer:_panToLeaveGR];
                
            }else{
                [_lvConnWatcher.glVideoOutputView addGestureRecognizer:tapGesture];
                [_lvConnWatcher.glVideoOutputView addGestureRecognizer:_panToFullScreenGR];
                [_lvConnWatcher.glVideoOutputView addGestureRecognizer:_panToLeaveGR];
                
            }
            
#ifdef SANDBOX_MODE
            self.staticLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 180, SCREEN_WIDTH-20, 30)];
            [_staticLabel setBackgroundColor:[UIColor blackColor]];
            [_staticLabel setTextColor:WHITE_COLOR];
            _staticLabel.font = SYSTEM_FONT_WITH_SIZE(12);
            _lvConnWatcher.staticLabel = _staticLabel;
            [self.view addSubview:_staticLabel];
            
            
            self.staticLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 210, SCREEN_WIDTH-20, 30)];
            [_staticLabel2 setBackgroundColor:[UIColor blackColor]];
            [_staticLabel2 setTextColor:WHITE_COLOR];
            _staticLabel2.font = SYSTEM_FONT_WITH_SIZE(12);
            _lvConnWatcher.staticLabel2 = _staticLabel2;
            [self.view addSubview:_staticLabel2];
#endif
        }
            break;
            
    }
    _firstTimeFullScreen=YES;
    [self buildStreamingView];
    [self reloadUI];
    [self enableFullScreen:NO];
    _firstTimeFullScreen=NO;

    _exitButton.frame = CGRectMake(SCREEN_WIDTH-kExitButtonSize, 0, kExitButtonSize, kExitButtonSize);
    [_exitButton setImage:[UIImage imageNamed:@"nav_cancel"] forState:UIControlStateNormal];
    _exitButton.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    _exitButton.layer.shadowOffset = CGSizeMake(0.3f, 0.3f);
    _exitButton.layer.shadowOpacity = 1.0f;
    _exitButton.layer.shadowRadius = 0.3f;
    [_exitButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:_exitButton];

    
    /* Top View */
    _topView.hidden = YES;
    [_topView setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.3]];
    _topView.layer.masksToBounds = YES;
    
    
    UIView* blackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT)];
    [blackView setBackgroundColor:BLACK_COLOR];
    
    int nextY = 100;
    __weak LiveStreamViewController* weakSelf=self;
    //    _warningLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 44)];
    _warningLabel.frame=CGRectMake(0, nextY, SCREEN_WIDTH, 44);
    _warningLabel.font = BOLD_FONT_WITH_SIZE(20);
    _warningLabel.textAlignment = NSTextAlignmentCenter;
    _warningLabel.text = LOCALIZE(@"take_live_with_17");
    [_warningLabel setTextColor:WHITE_COLOR];
    
    nextY += _warningLabel.frame.size.height+3;
    
    _descriptLabel.frame=CGRectMake(30, nextY, SCREEN_WIDTH-60, 40);
    _descriptLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    _descriptLabel.text = LOCALIZE(@"cam_not_access_description_live");
    [_descriptLabel sizeToFit];
    _descriptLabel.center=CGPointMake(SCREEN_WIDTH/2, nextY+44);
    _descriptLabel.textAlignment = NSTextAlignmentCenter;
    _descriptLabel.numberOfLines = 0;
    [_descriptLabel setTextColor:LIGHT_GRAY_COLOR];
    
    //    _linkLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 30)];
    _linkLabel.frame=CGRectMake(0, nextY, SCREEN_WIDTH, 30);
    _linkLabel.textAlignment = NSTextAlignmentCenter;
    _linkLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    _linkLabel.text = LOCALIZE(@"link_to_camera");
    [_linkLabel setTextColor:[[UIColor alloc] initWithRed:129.0/255.0 green:171.0/255/.0 blue:193.0/255.0 alpha:1.0]];
    _linkLabel.userInteractionEnabled = YES;
    
    [_linkLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender,
                                                                                          UIGestureRecognizerState state, CGPoint location) {

        [weakSelf dismissViewControllerAnimated:YES completion:nil];
        
        [SINGLETON gotoSystemSettings];
        
    }]];
    
    [blackView addSubview:_warningLabel];
    [blackView addSubview:_descriptLabel];
    [blackView addSubview:_linkLabel];
    [_topView addSubview:blackView];
    
    [self.view addSubview:_topView];
    
    [self requestAudioPermission];
    
    [NOTIFICATION_CENTER addObserver:self
                            selector:@selector(enterBackground:)
                                name:UIApplicationWillResignActiveNotification
                              object:nil];
    
    [NOTIFICATION_CENTER addObserver:self
                            selector:@selector(enterForeground:)
                                name:UIApplicationDidBecomeActiveNotification
                              object:nil];
    
    [NOTIFICATION_CENTER addObserver:self
                            selector:@selector(keyboardNotificationHandler:)
                                name: UIKeyboardWillChangeFrameNotification
                              object:nil];
    
    [NOTIFICATION_CENTER addObserver:self
                            selector:@selector(userDidTakeScreenshot:)
                                name: UIApplicationUserDidTakeScreenshotNotification
                              object:nil];
    
    [NOTIFICATION_CENTER addObserver:self
                            selector:@selector(playAnimateFromNotifcation:)
                                name:GIFT_DOWNLOAD_COMPLETE
                              object:nil];
    
    [NOTIFICATION_CENTER addObserver:self
                            selector:@selector(notifySocialLogin:)
                                name:SocialLoginNotification
                              object:nil];
    
    [NOTIFICATION_CENTER addObserver:self
                            selector:@selector(notifyLogin:)
                                name:LoginNotification
                              object:nil];
    
    [NOTIFICATION_CENTER addObserver:self
                            selector:@selector(notifySignUp:)
                                name:SignUpNotification
                              object:nil];

    [self.bgImageView addSubview:_chatMessageLabel];
    [self.bgImageView addSubview:_nameLabel];
    [self.bgImageView addSubview:_giftImage];
    [_bgImageView addSubview:_mask];
    [_bgImageView addSubview:_giftHeadImage];
    [self.view addSubview:_bgImageView];
    _bgImageView.alpha = 0;
    self.POP = YES;
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    if (!_shouldLoadStream) {
        self.shouldLoadStream = YES;
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_giftAnimateQuene removeAllObjects];
    [_liveStreamStartButton setSelected:NO];
    
    self.canPresentView = YES;
    [DEFAULTS setObject:INT_TO_STRING(_canPresentView) forKey:NOTLIVESTREAMING];
    [DEFAULTS synchronize];
    
    [API_MANAGER getGiftListWithCompletion:^(BOOL success, NSArray *gifts) {
        if (success) {
            dispatch_async(GLOBAL_QUEUE, ^{
                [GiftObject getGiftZipFile:gifts index:0 firstCatch:YES];
            });
        }
    }];
    _enterModeForET=@"";
    switch (_liveStreamMode) {
        case LiveStreamModeBroadcast:
        {
            _enterModeForET=@"BroadCaster";
        }
            break;
            
        case LiveStreamModeWatch:
        {
            switch (_livestreamEnterMode) {
                case LiveStreamEnterModeFollowed:
                {
                    _enterModeForET=@"Follwed";
                    
                }
                    break;
                    
                case LiveStreamEnterModeHotLive:
                {
                    _enterModeForET=@"HotLive";
                    
                }
                    break;
                case LiveStreamEnterModeRestreamed:
                {
                    _enterModeForET=@"Restreamed";
                    
                }
                    break;
                case LiveStreamEnterModeWebPage:
                {
                    
                }
                    break;
                case LivestreamEnterModeGuest:
                {
                    _enterModeForET=@"Guest";
                    
                }
                    break;
                case LivestreamEnterModeShared:
                {
                    _enterModeForET=@"Shared";
                    
                }
                    break;
            }
        }
            break;
    }
    
    if (_liveStreamMode==LiveStreamModeBroadcast&&!_liveStreamDidStart) {
        [EVENT_HANDLER addEventTracking:@"LeavePublishPageWithoutPub" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp),@"type":@"live"}];
    }else{
        if (_liveStream.user!=nil) {
            [EVENT_HANDLER addEventTracking:@"LeaveLivePage" withDict:@{@"targetUserID":_liveStream.userID,@"liveStreamID":_liveStream.liveStreamID,@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp),@"fullScreenCount":INT_TO_STRING(_enterFullScreenCount),@"commentCount":INT_TO_STRING(_commentCount),@"likeCount":INT_TO_STRING(_likeCountForET),@"consumePoint":INT_TO_STRING(_consumePoint),@"from":_enterModeForET}];

        }else{
            
        }
    }
    
    
    _liveStreamDidStart=NO;

    [_blurBgImageView removeFromSuperview];
    [_broadcasterController.glVideoOutputView removeFromSuperview];
    if(_heartTimer != nil) {
        [_heartTimer invalidate];
        _heartTimer = nil;
    }
    if(_giftTimer != nil) {
        [_giftTimer invalidate];
        _giftTimer = nil;
    }
    [DEFAULTS setObject:@0 forKey:IS_ON_LIVESTREAMING];
    [DEFAULTS synchronize];
    
    if (_privacypresent) {
        return;
    }
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    [DIALOG_MANAGER removePopImmediately];
    [_liveStreamSubjectTextView resignFirstResponder];
    
    DLog(@"\n\nView will disappear");
    [_lvConnWatcher disconnect];
    
    if(_liveStreamMode == LiveStreamModeBroadcast) {
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
        [[AVAudioSession sharedInstance] setPreferredSampleRate:44100.0 error:NULL];
        [[AVAudioSession sharedInstance] setActive:NO error:nil];
    }
    _lvConnWatcher.delegate = nil;
    _broadcasterController.delegate = nil;
    _lvConnWatcher.glVideoOutputView.alpha = 0.0;
    
    [NOTIFICATION_CENTER removeObserver:self
                                   name:UIApplicationWillResignActiveNotification
                                 object:nil];
    
    [NOTIFICATION_CENTER removeObserver:self
                                   name:UIApplicationDidBecomeActiveNotification
                                 object:nil];
    
    [NOTIFICATION_CENTER removeObserver:self
                                   name: UIKeyboardWillChangeFrameNotification
                                 object:nil];
    
    [NOTIFICATION_CENTER removeObserver:self
                                   name: UIApplicationUserDidTakeScreenshotNotification
                                 object:nil];
    
    [NOTIFICATION_CENTER removeObserver:self
                                   name:GIFT_DOWNLOAD_COMPLETE
                                 object:nil];
    
    [NOTIFICATION_CENTER removeObserver:self
                                   name:SocialLoginNotification
                                 object:nil];
    
    [NOTIFICATION_CENTER removeObserver:self
                                   name:LoginNotification
                                 object:nil];
    
    [NOTIFICATION_CENTER removeObserver:self
                                   name:SignUpNotification
                                 object:nil];
}


#pragma mark - Private Method

- (void)randomHeart
{
    if (_liveStream.liveViewerCount < 10) {
        return;
    }
    
    if (_changerate > 200 || _changerate == 0) {
        
        self.changerate = 1;
        
        if (_liveStream.liveViewerCount < 30) {
            self.rateforheart = 10 + rand() % 10;
        }
        else if (_liveStream.liveViewerCount < 250) {
            self.rateforheart = 7 + rand() % 7;
        }
        else if (_liveStream.liveViewerCount < 500) {
            self.rateforheart = 5 + rand() % 5;
        }
        else if (_liveStream.liveViewerCount < 1000) {
            self.rateforheart = 3 + rand() % 3;
        }
        else {
            self.rateforheart = 2 + rand() % 2;
        }
    }
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        if (_rateforheart < 10) {
            _rateforheart = 10;
        }
    }
    self.changerate++;
    
    if (rand() % _rateforheart == 0) {
        NSMutableData* data = CREATE_MUTABLE_DATA;
        NSString *colorString = _lvConnWatcher.colorCode;
        unsigned r, g, b;
        [[NSScanner scannerWithString:[colorString substringFrom:0 to:2]] scanHexInt:&r];
        [[NSScanner scannerWithString:[colorString substringFrom:2 to:4]] scanHexInt:&g];
        [[NSScanner scannerWithString:[colorString substringFrom:4 to:6]] scanHexInt:&b];
        [data appendBytes:&r length:sizeof(r)];
        [data appendBytes:&g length:sizeof(g)];
        [data appendBytes:&b length:sizeof(b)];
        [self popoverHeart:data random:YES];
    }
}

- (void)giftCatch
{
    if (_liveStreamMode == LiveStreamModeBroadcast) {
        [API_MANAGER getUnreadReceivedGift:_liveStream.liveStreamID beforeTime:CURRENT_TIMESTAMP withCount:20 withCompletion:^(BOOL success, NSArray *gifts) {
            if (success) {
                if ([gifts count]>0) {
//                    for (int index=0; index<[gifts count]; index++) {
//                        UserGiftObject* a=[gifts objectAtIndex:index];
//                        if (![_giftAnimateQuene containsObject:a]) {
//                            [_giftAnimateQuene addObject:a];
//                        }
//                    }
                    DLog(@"getunread");
                }
            }
        }];
    }
    
}

- (void)didClickButton:(UIButton*)sender
{
    if ([sender isEqual:_exitButton]) {
        
        [LIKE_HANDLER likeLivestreamBatchUpdate];
        
        if(_messageInputBar.messageTextView.isFirstResponder){
            [self resignMessageInputBar];
        }
        
        if (_liveStreamMode == LiveStreamModeBroadcast) {
            [self removeSettingView];
            [self resignMessageInputBar];

        }
        
        if(_liveStreamMode == LiveStreamModeBroadcast && _isLiveStreamStart){
            [self resignMessageInputBar];
            [self exitLiveConfirm];
            
        } else {
            [_cameraSwitchButton removeFromSuperview];
            [self exitLiveStream];
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
    }
    else if ([sender isEqual:_liveStreamStartButton]) {
        self.isLiveStreamStart = YES;
        _liveStreamDidStart=YES;

        [SINGLETON detectIpGeoLocation:^(BOOL success, NSDictionary *jsonDict) {
            
            NSString* lat = @"";
            NSString* lon = @"";
            NSString* regionName = @"";
            NSMutableDictionary* infoDict;
            
            if(success){
                
                lat = jsonDict[@"lat"];
                lon = jsonDict[@"lon"];
                regionName = jsonDict[@"regionName"];

                infoDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:_liveStreamSubjectTextView.text, @"caption", regionName, @"locationName", lat, @"latitude", lon, @"longitude", @"", @"coverPhoto", INT_TO_STRING(_isLocationSharedOn), @"shareLocation", NSINTEGER_TO_STRING(_chatMode), @"followerOnlyChat",@"1",@"canSendGift", nil];
                
            }else{
                
                infoDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:_liveStreamSubjectTextView.text, @"caption", @"", @"coverPhoto", INT_TO_STRING(_isLocationSharedOn), @"shareLocation", NSINTEGER_TO_STRING(_chatMode), @"followerOnlyChat",@"1",@"canSendGift", nil];
            }
            
            if([_privacySettingUser count]!=0){
                [infoDict setObject:TO_JSON(_privacySettingUser) forKey:@"privateWatcher"];
                [infoDict setObject:@"1" forKey:@"privateLiveStream"];
            }
            
            if(_liveStreamStartButton.isSelected)
                return;
            
            [_liveStreamStartButton setSelected:YES];
            [_broadcasterController startLiveStreamWithLivestreamInfo:infoDict];
            
            /* camera switch */
            _cameraSwitchButton.center = CGPointMake(kLeftMargin, SCREEN_HEIGHT-3*LIVE_STREAM_BUTTON_HEIGHT/4);
            [_cameraSwitchButton setBackgroundImage:[UIImage imageNamed:@"live_cameratoggle"] forState:UIControlStateNormal];
            [_cameraSwitchButton setBackgroundImage:[UIImage imageNamed:@"live_cameratoggle_down"] forState:UIControlStateHighlighted];

        }];
        
    }
    else if ([sender isEqual:_fullScreenSwitchButton]) {

        if (_fullScreenSwitchButton.selected) {
            [self enableFullScreen:NO];
        }
        else {
            [self enableFullScreen:YES];
        }
    }
    else if ([sender isEqual:_cameraSwitchButton]) {
        
        _broadcasterController.isFrontCamera = !_broadcasterController.isFrontCamera;
        _broadcasterController.isTorchOn = NO;
        [_broadcasterController setupCamera];
        [self reloadUI];
        
    }
    else if ([sender isEqual:_torchModeSwitchButton]) {
        
        if(_broadcasterController.isFrontCamera) {
            return ;
        }
        
        _broadcasterController.isTorchOn = !_broadcasterController.isTorchOn;
        
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        [device lockForConfiguration:nil];
        
        if ([device hasTorch]) {
            if(_broadcasterController.isTorchOn) {
                device.torchMode = AVCaptureTorchModeOn;
            } else {
                device.torchMode = AVCaptureTorchModeOff;
            }
        }
        
        [device unlockForConfiguration];
        
        [self reloadUI];
        
    }
    else if ([sender isEqual:_chatStartButton]) {
        if ([LoginHandler isGuest]) {
            [LoginHandler handleUserLogin];
            return;
        }
        if ([GET_DEFAULT(PICTURE)isEqualToString:@""]) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"Please_verify_your_photo_before_live") message:@"" buttonText:LOCALIZE(@"OK") cancelable:YES withCompletion:^(BOOL okClicked) {
                    if (okClicked) {
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            
                            [DIALOG_MANAGER showActionSheetDialogTitle:@"" options:@[LOCALIZE(@"take_picture"),LOCALIZE(@"albom")] destructiveIndexes:@[] cancelable:YES withCompletion:^(int selectedOption) {
                                if(selectedOption==1){
                                    
                                    // 從相機膠卷選擇
                                    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                                    [[UIBarButtonItem appearance] setBackgroundImage:nil  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
                                    [[UIBarButtonItem appearance] setBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
                                    
                                    imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
                                    imagePicker.delegate = (id) self;
                                    imagePicker.allowsEditing = YES;
                                    
                                    
                                    [self presentViewController:imagePicker animated:YES completion:^{
                                        
                                    }];
                                }else if (selectedOption==0){
                                    
                                    // 照相
                                    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                                    imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
                                    imagePicker.cameraDevice=UIImagePickerControllerCameraDeviceFront;
                                    imagePicker.delegate = (id) self;
                                    imagePicker.allowsEditing = YES;
                                    [self presentViewController:imagePicker animated:YES completion:^{
                                        
                                    }];
                                }
                            }];});
                        
                    }
                }];
            });
            
        }else{
            
            WarningLog(@"chat else");
            
            if (_lastSelfCommentTime <= CURRENT_TIMESTAMP-COMMENT_INTERVAL || [GET_DEFAULT(IS_ADMIN) intValue] >= 1 ) {
                [self becomeFirstMessageInputBar];
                WarningLog(@"chat becomeFirstMessageInputBar");
            }
            else {
                NSString* alertStr = [NSString stringWithFormat:LOCALIZE(@"Cannot_commment_message"),abs(COMMENT_INTERVAL-CURRENT_TIMESTAMP+_lastSelfCommentTime)];
                [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"Cannot_commment") message:alertStr buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {}];
            }
        }
    }
        
}

- (BOOL)sendMessageWithType:(NSString*)type andContent:(NSString*) content
{
    if (_liveStreamMode == LiveStreamModeWatch) {
        _lastSelfCommentTime = CURRENT_TIMESTAMP;
        DLog(@"Send Livestream Message : %@",content);
        
        if (_liveStream.liveStreamID == nil) {
            return NO;
        }
        _commentCount++;
        [EVENT_HANDLER addEventTracking:@"CommentInLiveStream" withDict:@{@"targetUserID":_liveStream.userID,@"liveStreamID":_liveStream.liveStreamID,@"comment":content}];
        [API_MANAGER commentLiveStream:_liveStream.liveStreamID comment:content colorCode:_lvConnWatcher.colorCode absTimestamp:_lvConnWatcher.currentAbsTimestamp withCompletion:^(BOOL success, NSString *message) {
            if (!success){
                [DIALOG_MANAGER showNetworkFailToast];
            }
        }];
    }
    
    return YES;
}

- (void)enableFullScreen:(BOOL)enabled
{
    [_liveStreamHeaderViewController enableFullScreenMode:enabled];
    _fullScreenSwitchButton.selected = enabled;
    _autoHintView.isFullScreen = enabled;
    
    if (enabled) {
        _enterFullScreenCount++;
        [EVENT_HANDLER addEventTracking:@"EnterFullScreen" withDict:nil];
        _enterFullScreenTimeStamp=CURRENT_TIMESTAMP;
        
        [_fullScreenSwitchButton setBackgroundImage:[UIImage imageNamed:@"live_fullscreen_off"] forState:UIControlStateNormal];
        [_fullScreenSwitchButton setBackgroundImage:[UIImage imageNamed:@"live_fullscreen_off_down"] forState:UIControlStateHighlighted];

        [UIView animateWithDuration:0.2 animations:^{
            _fullScreenSwitchButton.center = CGPointMake(SCREEN_WIDTH/12*11, SCREEN_HEIGHT-3*LIVE_STREAM_BUTTON_HEIGHT/4);
            
            switch (_liveStreamMode) {
                case LiveStreamModeBroadcast:
                {
                    _cameraSwitchButton.alpha = 0;
                    _torchModeSwitchButton.alpha = 0;
                }
                    break;
                
                case LiveStreamModeWatch:
                {
                    _giftButton.alpha = 0;
                    _autoHintView.alpha = 0;
                }
                    break;
            }

            
            if([GET_DEFAULT(IS_ADMIN) intValue]>=1){
                _killLiveButton.alpha = 0;
            }
            _chatStartButton.alpha = 0;
            _likeButton.alpha = 0;
            _restreamButton.alpha = 0;
            _lvTable.alpha = 0;
        }];

    }
    else {
        if (_firstTimeFullScreen==NO) {
            [EVENT_HANDLER addEventTracking:@"LeaveFullScreen" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterFullScreenTimeStamp)}];

        }
        
        [_fullScreenSwitchButton setBackgroundImage:[UIImage imageNamed:@"live_fullscreen"] forState:UIControlStateNormal];
        [_fullScreenSwitchButton setBackgroundImage:[UIImage imageNamed:@"live_fullscreen_down"] forState:UIControlStateHighlighted];
        
        [UIView animateWithDuration:0.2 animations:^{
            _fullScreenSwitchButton.center = CGPointMake((SCREEN_WIDTH-2*kLeftMargin)/4*3+kLeftMargin, SCREEN_HEIGHT-3*LIVE_STREAM_BUTTON_HEIGHT/4);
            
            switch (_liveStreamMode) {
                case LiveStreamModeBroadcast:
                {
                    _cameraSwitchButton.alpha = 1;
                    _torchModeSwitchButton.alpha = 1;
                }
                    break;
                
                case LiveStreamModeWatch:
                {
                    _giftButton.alpha = 1;
                    _autoHintView.alpha = 1;
                }
                    break;
            }
            _chatStartButton.alpha = 1;
            _likeButton.alpha = 1;
            _lvTable.alpha = 1;
            _restreamButton.alpha = 1;
            
            if([GET_DEFAULT(IS_ADMIN) intValue]>=1){
                _killLiveButton.alpha = 1;
            }
            
        }];
    }
}

- (void)forceSwitchFullScreen:(CGFloat)buttonCenterX alpha:(CGFloat)alpha
{
    [_liveStreamHeaderViewController forceSwitchFullScreenWithalpha:alpha];
    _fullScreenSwitchButton.center = CGPointMake(buttonCenterX, SCREEN_HEIGHT-3*LIVE_STREAM_BUTTON_HEIGHT/4);
    
    switch (_liveStreamMode) {
        case LiveStreamModeBroadcast:
        {
            _cameraSwitchButton.alpha = alpha;
            _torchModeSwitchButton.alpha = alpha;
        }
            break;
            
        case LiveStreamModeWatch:
        {
            _giftButton.alpha = alpha;
            _autoHintView.alpha = alpha;
        }
            break;
    }
    
    _chatStartButton.alpha = alpha;
    _likeButton.alpha = alpha;
    _restreamButton.alpha = alpha;
    _lvTable.alpha = alpha;
}

- (void)requestAudioPermission
{
    if (![[AVAudioSession sharedInstance] respondsToSelector:@selector(requestRecordPermission:)]) {
        return;
    }
    
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted) {
            [self requestVedioPermission];
            
        } else {
            if (self.liveStreamMode == LiveStreamModeBroadcast) {
                self.linkLabel.text = LOCALIZE(@"link_to_mic");
                self.descriptLabel.text = LOCALIZE(@"mic_not_access_description_live");
                self.warningLabel.text = LOCALIZE(@"take_live_with_17");
                
                [self.descriptLabel sizeToFit];
                
                self.topView.hidden = NO;
                [self.topView bringSubviewToFront:self.view];
                
            }
        }
    }];

}

- (void)requestVedioPermission
{
    if (![AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
        return;
    }

    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        dispatch_async(MAIN_QUEUE, ^{
            if (granted) {
                self.topView.hidden = YES;
                
            } else {
                if (self.liveStreamMode == LiveStreamModeBroadcast) {
                    [self.view endEditing:YES];

                    self.topView.hidden = NO;
                    [self.topView bringSubviewToFront:self.view];
                    
                }
            }
        });
    }];
}

#pragma mark - UI views
- (void)actIndicatorBegin {
    [_activityIndicator startAnimating];
}

- (void)actIndicatorEnd {
    [_activityIndicator stopAnimating];
}

- (void)singleTap:(UIGestureRecognizer *)recognizer
{
//    [_scene showAnimation1FromPoint:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2)];
//    [_scene showAnimation3FromPoint];
    
    if (_fullScreenSwitchButton.isSelected || _liveStreamMode == LiveStreamModeBroadcast) {
        return;
    }
    
    if (_messageInputBar.messageTextView.isFirstResponder) {
        [self resignMessageInputBar];
        return;
    }
    
    if (_isLiveStreamStart && _liveStream!=nil) {
        
        _likeCountForET++;
        [EVENT_HANDLER addEventTracking:@"LikeLive" withDict:@{@"targetUserID":_liveStream.userID,@"liveStreamID":_liveStream.liveStreamID}];
        if ([MobClick isJailbroken]) {
            NSMutableData* data = CREATE_MUTABLE_DATA;
            NSString *colorString = _lvConnWatcher.colorCode;
            unsigned r, g, b;
            [[NSScanner scannerWithString:[colorString substringFrom:0 to:2]] scanHexInt:&r];
            [[NSScanner scannerWithString:[colorString substringFrom:2 to:4]] scanHexInt:&g];
            [[NSScanner scannerWithString:[colorString substringFrom:4 to:6]] scanHexInt:&b];
            [data appendBytes:&r length:sizeof(r)];
            [data appendBytes:&g length:sizeof(g)];
            [data appendBytes:&b length:sizeof(b)];
            [self popoverHeart:data random:NO];
        }
        else {
            if (_liveStream.liveViewerCount<10&&_liveStream.liveViewerCount>0) {
                [_lvConnWatcher likeActionTo:[_liveStream.liveStreamID intValue]];
            }
            else {
                NSMutableData* data = CREATE_MUTABLE_DATA;
                NSString *colorString = _lvConnWatcher.colorCode;
                unsigned r, g, b;
                [[NSScanner scannerWithString:[colorString substringFrom:0 to:2]] scanHexInt:&r];
                [[NSScanner scannerWithString:[colorString substringFrom:2 to:4]] scanHexInt:&g];
                [[NSScanner scannerWithString:[colorString substringFrom:4 to:6]] scanHexInt:&b];
                [data appendBytes:&r length:sizeof(r)];
                [data appendBytes:&g length:sizeof(g)];
                [data appendBytes:&b length:sizeof(b)];
                [self popoverHeart:data random:NO];
            }
            
            [LIKE_HANDLER likeLivestream:_liveStream.liveStreamID];
        }
    }
}

#pragma mark - message input box
- (void)messageInputBar:(MessageInputBar *)bar willEditWithKeybordHeight:(float)keybordHeight
{
    CGRect rect = self.lvTable.frame;
    rect.size.height -= keybordHeight+TAB_BAR_HEIGHT;
    [self.lvTable setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-2*NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-keybordHeight-TAB_BAR_HEIGHT)];
    _bgImageView.frame = CGRectMake(5, _lvTable.frame.origin.y-5-_bgImageView.frame.size.height, _bgImageView.frame.size.width, _bgImageView.frame.size.height);

    rect = self.messageInputBar.frame;
    rect.origin.y -= keybordHeight;
    [self.messageInputBar setFrame:rect];
}

- (void)sendTextMessage
{
    NSString *text = self.messageInputBar.messageTextView.text;
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if([text isEqualToString:@""]) {
        return;
    }
    
    BOOL success = [self sendMessageWithType:@"text" andContent:text];
    
    if(success) {
        self.messageInputBar.messageTextView.text = @"";
        [self.messageInputBar.messageTextView setText:nil];
        [self resignMessageInputBar];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.messageInputBar textViewDidChange:self.messageInputBar.messageTextView];
            
        });
    }
}

- (void)repositionTableViewWithAnimation:(BOOL)animated
{
    if(_messageInputBar.mode == GAME_MODE) {
        return;
    }
    
    static float lastExecutionTime = 0.0;
    float currentTime = CACurrentMediaTime();
    
    if(currentTime-lastExecutionTime < 0.05) {
        animated = NO;
    }
    
    lastExecutionTime = currentTime;
    
    // check if need to scroll to bottom
    //    self.needScrollToBottom = [self isLastCellVisible];
    
    int messageInputBarHeight = _messageInputBar.messageInputBarHeight;
    int keyboardHeight = _messageInputBar.keyboardHeight;
    
    /* resize chatTableView => very tricky and complicated algorithm, do not modify! */
    
    //    int originalHeight = self.lvTable.frame.size.height;
    
    if(animated == YES) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.25];
        [UIView setAnimationCurve:7]; // undocumented hack!
    }
    
    float tableHeight = (_cellTotalHeight > maxTableHeight? maxTableHeight : _cellTotalHeight);
    int newY = SCREEN_HEIGHT-28-tableHeight-messageInputBarHeight-keyboardHeight;
    [_lvTable setFrame:CGRectMake(0, newY, _lvTable.frame.size.width, tableHeight)];
    _bgImageView.frame = CGRectMake(5, _lvTable.frame.origin.y-5-_bgImageView.frame.size.height, _bgImageView.frame.size.width, _bgImageView.frame.size.height);

    [_messageInputBar setFrame:CGRectMake(0, SCREEN_HEIGHT-_messageInputBar.keyboardHeight-_messageInputBar.messageInputBarHeight, SCREEN_WIDTH, messageInputBarHeight)];
    
    if(animated == YES) {
        [UIView commitAnimations];
    }
    
    if(self.lvTable.contentOffset.y > 0 && _lvTable.contentSize.height < _lvTable.frame.size.height){
        [self.lvTable setContentOffset:CGPointMake(0, 0)];
        return;
    }
}

#pragma mark - Tagging Handler

- (void)searchModeDetected:(NSString*)searchString
{
    self.searchString = searchString;
}

#pragma mark - UI view creation and destruction

- (void)buildStreamingView
{
    [_lvTable registerClass:[LiveStreamTableViewCell class] forCellReuseIdentifier:@"lvChatCell"];
    _lvTable.lvDataSource = self;
    _lvTable.lvDelegate = self;
    _lvTable.lvViewerDelegate = self;
    
    [self.view addSubview:_lvTable];
    
    _chatMessages = [NSMutableArray new];
    
    [_lvTable reloadData];
    
    // full screen button
    [_fullScreenSwitchButton setBackgroundImage:[UIImage imageNamed:@"live_fullscreen"] forState:UIControlStateNormal];
    [_fullScreenSwitchButton setBackgroundImage:[UIImage imageNamed:@"live_fullscreen_down"] forState:UIControlStateHighlighted];
    _fullScreenSwitchButton.frame = CGRectMake(0, 0, LIVE_STREAM_BUTTON_HEIGHT, LIVE_STREAM_BUTTON_HEIGHT);
    _fullScreenSwitchButton.center = CGPointMake((SCREEN_WIDTH-2*kLeftMargin)/4*3+kLeftMargin, SCREEN_HEIGHT-3*LIVE_STREAM_BUTTON_HEIGHT/4);
    [self.view addSubview:_fullScreenSwitchButton];
    
    [_fullScreenSwitchButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [_likeButton setBackgroundImage:[UIImage imageNamed:@"live_love"] forState:UIControlStateNormal];
    [_likeButton setBackgroundImage:[UIImage imageNamed:@"live_love_down"] forState:UIControlStateHighlighted];
    _likeButton.titleLabel.adjustsFontSizeToFitWidth = true;
    _likeButton.frame = CGRectMake(0, 0, LIVE_STREAM_BUTTON_HEIGHT, LIVE_STREAM_BUTTON_HEIGHT);
    
    CGPoint likeButtonCenter;
    likeButtonCenter.x = SCREEN_WIDTH - kLeftMargin;
    likeButtonCenter.y = SCREEN_HEIGHT - 3 * LIVE_STREAM_BUTTON_HEIGHT / 4;
    _likeButton.center = likeButtonCenter;
    [_likeButton addTarget:self action:@selector(singleTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_likeButton];
    
    [_restreamButton setBackgroundImage:[UIImage imageNamed:@"live_share"] forState:UIControlStateNormal];
    [_restreamButton setBackgroundImage:[UIImage imageNamed:@"live_share_down"] forState:UIControlStateHighlighted];
    int d = rand() % 7 + 1;
    [_giftButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"live_gift_%d", d]] forState:UIControlStateNormal];
    [_giftButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"live_gift_%d_down", d]] forState:UIControlStateHighlighted];
    _giftButton.titleLabel.adjustsFontSizeToFitWidth = true;
    _giftButton.frame = CGRectMake(SCREEN_WIDTH/12*5 -LIVE_STREAM_BUTTON_HEIGHT/2, SCREEN_HEIGHT-3*LIVE_STREAM_BUTTON_HEIGHT/4-LIVE_STREAM_BUTTON_HEIGHT/2, LIVE_STREAM_BUTTON_HEIGHT, LIVE_STREAM_BUTTON_HEIGHT);
    _giftButton.center = CGPointMake((SCREEN_WIDTH-2*kLeftMargin)/4*2+kLeftMargin, SCREEN_HEIGHT-3*LIVE_STREAM_BUTTON_HEIGHT/4);
    
    switch (_liveStreamMode) {
        case LiveStreamModeBroadcast:
        {
            [_giftButton removeFromSuperview];

        }
            break;
            
        case LiveStreamModeWatch:
        {
            [self.view addSubview:_giftButton];
            
        }
            break;
    }
    
    [_giftButton addTarget:self action:@selector(didClickGift:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [_restreamButton setBackgroundImage:[UIImage imageNamed:@"live_repost"] forState:UIControlStateNormal];
    [_restreamButton setBackgroundImage:[UIImage imageNamed:@"live_repost_down"] forState:UIControlStateHighlighted];

    _restreamButton.titleLabel.adjustsFontSizeToFitWidth = true;
    _restreamButton.frame = CGRectMake(SCREEN_WIDTH/12*3 -LIVE_STREAM_BUTTON_HEIGHT/2, SCREEN_HEIGHT-3*LIVE_STREAM_BUTTON_HEIGHT/4-LIVE_STREAM_BUTTON_HEIGHT/2, LIVE_STREAM_BUTTON_HEIGHT, LIVE_STREAM_BUTTON_HEIGHT);
    _restreamButton.center = CGPointMake((SCREEN_WIDTH-2*kLeftMargin)/4+kLeftMargin, SCREEN_HEIGHT-3*LIVE_STREAM_BUTTON_HEIGHT/4);
    _restreamButton.alpha = 1.0;
    
    [_restreamButton addTarget:self action:@selector(restream) forControlEvents:UIControlEventTouchUpInside];
    
    if(_liveStreamMode == LiveStreamModeWatch){
        [self.view addSubview:_restreamButton];
        
    }
    
    // chat message input bar
    _messageInputBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    _messageInputBar.chatVC = self;
    _messageInputBar.alpha = 0.0f;
    
    [self.view addSubview:_messageInputBar];
    
    [_messageInputBar.sendMessageButton addTarget:self
                                           action:@selector(sendTextMessage)
                                 forControlEvents:UIControlEventTouchUpInside];
    _messageInputBar.messageTextView.keyboardType = UIKeyboardTypeTwitter;
    
    switch (_liveStreamMode) {
        case LiveStreamModeBroadcast:
        {
            // flash switch
            _torchModeSwitchButton.showsTouchWhenHighlighted = YES;
            [_torchModeSwitchButton setBackgroundImage:[UIImage imageNamed:@"live_flash"] forState:UIControlStateNormal];
            [_torchModeSwitchButton setBackgroundImage:[UIImage imageNamed:@"live_flash_down"] forState:UIControlStateHighlighted];
            _torchModeSwitchButton.frame = CGRectMake(0, 0, LIVE_STREAM_BUTTON_HEIGHT, LIVE_STREAM_BUTTON_HEIGHT);
            _torchModeSwitchButton.center = CGPointMake((SCREEN_WIDTH-2*kLeftMargin)/4+kLeftMargin, SCREEN_HEIGHT-3*LIVE_STREAM_BUTTON_HEIGHT/4);
            [_torchModeSwitchButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.view addSubview:_torchModeSwitchButton];
        }
            break;
            
        case LiveStreamModeWatch:
        {
            // chat start button
            [_chatStartButton setBackgroundImage:[UIImage imageNamed:@"live_chat"] forState:UIControlStateNormal];
            [_chatStartButton setBackgroundImage:[UIImage imageNamed:@"live_chat_down"] forState:UIControlStateHighlighted];
            _chatStartButton.frame = CGRectMake(0, 0, LIVE_STREAM_BUTTON_HEIGHT, LIVE_STREAM_BUTTON_HEIGHT);
            _chatStartButton.center = CGPointMake(kLeftMargin, SCREEN_HEIGHT-3*LIVE_STREAM_BUTTON_HEIGHT/4);
            
            [_chatStartButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:_chatStartButton];
        }
            break;
    }
    
    [self.view addSubview:_liveStreamHeaderViewController.view];
    [self addChildViewController:_liveStreamHeaderViewController];
    
    [_killLiveButton setTitle:LOCALIZE(@"kill_live") forState:UIControlStateNormal];
    _killLiveButton.center = CGPointMake(SCREEN_WIDTH-_killLiveButton.frame.size.width/2-5, kWatcherHeaderViewHeight-_killLiveButton.frame.size.height/2);
    if([GET_DEFAULT(IS_ADMIN) intValue]>=1){
        [self.view addSubview:_killLiveButton];
    }
    
    self.autoHintView = [[[NSBundle mainBundle] loadNibNamed:@"AutoHintView" owner:self options:nil] firstObject];
    _autoHintView.frame = CGRectMake(0, kWatcherHeaderViewHeight, SCREEN_WIDTH, kAutoHintViewHeight);
    _autoHintView.delegate = self;
    [self.view addSubview:_autoHintView];
  
    //文字禮物UI相關
    [self.view addSubview:_textView];
    [_textView addSubview:_gifttext];
    [_textView addSubview:_textGiftHeadImage];
    _textView.userInteractionEnabled=YES;
    [_gifttext setTextColor:WHITE_COLOR];
    [_gifttext setBackgroundColor:[UIColor clearColor]];
    [_gifttext setText:@"對直播主說的話"];
    _gifttext.delegate = self;
    _gifttext.keyboardType = UIKeyboardAppearanceDefault;
    _gifttext.returnKeyType = UIReturnKeySend;//返回键的类型
    _gifttext.font = SYSTEM_FONT_WITH_SIZE(18);
    _textGiftBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
    [self.view addSubview:_textGiftBgView];
    [_textView addSubview:_textRemainWarning];
    [self.view addSubview:_textView];
    [_textView addSubview:_gifttext];
    [_textView addSubview:_textGiftHeadImage];
    [_textGiftBgView addSubview:_textGiftSendButton];
    [_textGiftBgView addSubview:_textGiftCancelButton];
    float btnH = 45;
    _textGiftSendButton.frame=CGRectMake(10, SCREEN_HEIGHT-30-2*btnH-10, SCREEN_WIDTH-20, btnH);
    [_textGiftSendButton setBackgroundImage:[UIImage imageNamed:@"btn_whitebg"] forState:UIControlStateNormal];
    [_textGiftSendButton setBackgroundImage:[UIImage imageNamed:@"btn_whitebg_down"] forState:UIControlStateHighlighted];
    [_textGiftSendButton setTitle:@"發送" forState:UIControlStateNormal];
    [_textGiftSendButton setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
    
    _textGiftCancelButton.frame=CGRectMake(10, SCREEN_HEIGHT-30-btnH, SCREEN_WIDTH-20, btnH);
    [_textGiftCancelButton setBackgroundImage:[[UIImage imageNamed:@"btn_black_wline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [_textGiftCancelButton setBackgroundImage:[UIImage imageNamed:@"btn_black_wline_down"] forState:UIControlStateHighlighted];
    [_textGiftCancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [_textGiftCancelButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    
    _textGiftBgView.hidden = YES;
    _textView.hidden = YES;
//    
    
    _textView.userInteractionEnabled = YES;
    [_gifttext setTextColor:WHITE_COLOR];
    [_gifttext setBackgroundColor:[UIColor clearColor]];
    NSString* str = @"這是您送給直播主的專屬訊息\n顯示於畫面中央並停留8秒\n請輸入您想對直播主說的話";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:str];
    [attributedString addAttribute:NSForegroundColorAttributeName value:GRAY_COLOR range:NSMakeRange(0,str.length)];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    [paragraphStyle setLineSpacing:10];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, str.length)];
    [_textRemainWarning setText:@"0/70"];
    [_textRemainWarning setTextColor:GRAY_COLOR];
    [_textRemainWarning setFont:SYSTEM_FONT_WITH_SIZE(16)];
    _gifttext.attributedText = attributedString;
    _gifttext.font = SYSTEM_FONT_WITH_SIZE(20);
    _gifttext.delegate = self;
    _gifttext.keyboardType = UIKeyboardAppearanceDefault;
    _gifttext.returnKeyType = UIReturnKeySend;//返回键的类型
    _gifttext.font = SYSTEM_FONT_WITH_SIZE(18);
    //文字禮物UI相關
    
    [self.view sendSubviewToBack:_broadcasterController.glVideoOutputView];
    
}

- (void)removeStreamingView:(int32_t)mode
{
    [_fullScreenSwitchButton removeFromSuperview];
    [_likeButton removeFromSuperview];
    
    switch (_liveStreamMode) {
        case LiveStreamModeBroadcast:
        {
            [_cameraSwitchButton removeFromSuperview];
            [_torchModeSwitchButton removeFromSuperview];
        }
            break;
            
        case LiveStreamModeWatch:
        {
            [_chatStartButton removeFromSuperview];
        }
            break;
    }
}

- (void)buildSettingView
{
    // subject text view
    _liveStreamSubjectTextView.placeholder = LOCALIZE(@"LIVE_STREAM_TEXT_SUBJECT_DEFAULT");
    [_liveStreamSubjectTextView setPlaceholderTextColor:WHITE_COLOR];
    _liveStreamSubjectTextView.backgroundColor = [UIColor clearColor];
    [_liveStreamSubjectTextView setTextColor:WHITE_COLOR];
    [_liveStreamSubjectTextView setTintColor:MAIN_COLOR];
    _liveStreamSubjectTextView.font = SYSTEM_FONT_WITH_SIZE(LIVE_STREAM_FONT_SIZE);
    [_liveStreamSubjectTextView setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.view addSubview:_liveStreamSubjectTextView];
    [_liveStreamSubjectTextView becomeFirstResponder];
    
    // camera switch button
    self.cameraSwitchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _cameraSwitchButton.showsTouchWhenHighlighted = YES;
    _cameraSwitchButton.frame = CGRectMake(SCREEN_WIDTH-2*(LIVE_STREAM_BUTTON_HEIGHT+LIVE_STREAM_VIEW_PADDING), 0, LIVE_STREAM_BUTTON_HEIGHT, LIVE_STREAM_BUTTON_HEIGHT);
    [_cameraSwitchButton setBackgroundImage:[UIImage imageNamed:@"stream_cameratoggle"] forState:UIControlStateNormal];
    [_cameraSwitchButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_cameraSwitchButton];
    
    // live stream start button
    [_liveStreamStartButton setBackgroundImage:[[UIImage imageNamed:@"btn_greenline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
    [_liveStreamStartButton setBackgroundImage:[[UIImage imageNamed:@"btn_greenline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
    _liveStreamStartButton.frame = CGRectMake(SCREEN_WIDTH/2-LIVE_STREAM_BUTTON_WIDTH/2, SCREEN_HEIGHT-KEYBOARD_HEIGHT-LIVE_STREAM_BUTTON_HEIGHT-2*LIVE_STREAM_VIEW_PADDING-LIVE_STREAM_BUTTON_HEIGHT-10,LIVE_STREAM_BUTTON_WIDTH, LIVE_STREAM_BUTTON_HEIGHT);
    _liveStreamStartButton.titleLabel.font =  SYSTEM_FONT_WITH_SIZE(LIVE_STREAM_FONT_SIZE);
    [_liveStreamStartButton setTitle:LOCALIZE(@"LIVE_STREAM_TEXT_START") forState:UIControlStateNormal];
    [_liveStreamStartButton setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
    
    [_liveStreamStartButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_liveStreamStartButton];
    
    CGRect rect = _liveStreamStartButton.frame;
    rect.origin.y -= LIVE_STREAM_BUTTON_HEIGHT+LIVE_STREAM_VIEW_PADDING;
    rect.origin.x = SCREEN_WIDTH/2-LIVE_STREAM_BUTTON_HEIGHT-LIVE_STREAM_VIEW_PADDING;
    
    rect.origin.x = SCREEN_WIDTH/2+LIVE_STREAM_VIEW_PADDING;
    
    
    [self.view sendSubviewToBack:_broadcasterController.glVideoOutputView];
}

- (void)removeSettingView
{
    [_liveStreamSubjectTextView removeFromSuperview];
    [_liveStreamStartButton removeFromSuperview];
}

- (void)reloadUI
{
    if (_broadcasterController.isFrontCamera) {
        
        _torchModeSwitchButton.hidden = YES;
        
    }
    else {
        _torchModeSwitchButton.hidden = NO;
        
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        
        if (![device hasTorch]) {
            _torchModeSwitchButton.hidden = YES;
        }
    }
}

#pragma mark - Live Stream Control
- (void)connectToLiveStream
{
    _lvConnWatcher.liveStream=_liveStream;
    [_lvConnWatcher connectToLive:_liveStream.liveStreamID isPreview:NO withCompletion:^(LIVE_STREAM_CONNECT_TYPE status) {
        
        switch (status) {
            case LIVE_STREAM_CONNECT_TYPE_SUCCESS:
                
                //                _hexColorCode = _lvConnWatcher.colorCode;
                
                _liveStreamStartTime = _lvConnWatcher.startTimestamp;
                _isLiveStreamStart = YES;
                [self fetchLivestreamInfo:_liveStream.liveStreamID];

                break;
                
            case LIVE_STREAM_CONNECT_TYPE_FAIL_ALREADY_CONNECT:
                
                break;
                
            case LIVE_STREAM_CONNECT_TYPE_FAIL_ALREADY_END:
                [self forceExit:LOCALIZE(@"Live_stream_ended")];
                [_hudView removeFromSuperview];
                break;
                
            case LIVE_STREAM_CONNECT_TYPE_FAIL_BLOCK_BY_HOST:
                [self forceExit:LOCALIZE(@"block_by_host")];
                break;
            case LIVE_STREAM_CONNECT_TYPE_FAIL_NETWORK:
                [DIALOG_MANAGER showNetworkFailToast];
                [self forceExit:LOCALIZE(@"network_unstable")];
                break;
                
            default:
                break;
        }
        
    }];
    
}

- (void)exitLiveConfirm
{
    __weak LiveStreamViewController* weakSelf = self;
    
    UIWindow *topView = (UIWindow *)[UIApplication sharedApplication].keyWindow;
    CGRect rect = topView.frame;
    UIView* dialogBgView = [[UIView alloc] initWithFrame:rect];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
    dialogBgView.userInteractionEnabled = YES;
    [topView addSubview:dialogBgView];
    dialogBgView.alpha = 0.0f;
    
    int topMargin = 7 ;
    int leftMargin = 15 ;
    int btnHeight = 45 ;
    int nextY = topMargin;
    float animationDuration = 0.3f;
    
    UIImageView* dialogBoxView = [UIImageView new];
    
    dialogBoxView.userInteractionEnabled = YES;
    [dialogBgView addSubview:dialogBoxView];
    
    /* Add Title */
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, btnHeight-10)];
    titleLabel.text = LOCALIZE(@"Confirm_end_livestream") ;
    titleLabel.textColor = WHITE_COLOR;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = BOLD_FONT_WITH_SIZE(18);
    titleLabel.numberOfLines = 2;
    titleLabel.minimumScaleFactor = 0.0;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.userInteractionEnabled = NO;
    [dialogBoxView addSubview:titleLabel];
    
    nextY += titleLabel.frame.size.height;
    
    nextY+= topMargin;
    
    /* Add OK Button */
    
    UIButton* okButton = [UIButton new];
    [okButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateNormal];
    [okButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateHighlighted];
    [okButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    okButton.titleLabel.font = BOLD_FONT_WITH_SIZE(16);
    [okButton setTitle:LOCALIZE(@"OK") forState:UIControlStateNormal];
    
    okButton.frame = CGRectMake(leftMargin, nextY, SCREEN_WIDTH*3/5, btnHeight);
    okButton.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2);
    
    okButton.titleLabel.font = BOLD_FONT_WITH_SIZE(18);
    nextY += btnHeight+topMargin;
    [dialogBoxView addSubview:okButton];
    
    UIButton* cancelButton = [UIButton new];
    [cancelButton setTitle:LOCALIZE(@"Cancel") forState:UIControlStateNormal];
    [cancelButton setBackgroundImage:[[UIImage imageNamed:@"btn_black"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateNormal];
    [cancelButton setBackgroundImage:[[UIImage imageNamed:@"btn_black_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateHighlighted];
    [cancelButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    cancelButton.titleLabel.font = BOLD_FONT_WITH_SIZE(16);
    
    cancelButton.frame = CGRectMake(SCREEN_WIDTH*5/10, nextY, SCREEN_WIDTH*3/5, btnHeight);
    cancelButton.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2);
    
    [dialogBoxView addSubview:cancelButton];
    nextY += topMargin + cancelButton.frame.size.height;
    
    
    
    float boxHeight = nextY;
    
    dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
    
    [okButton bk_addEventHandler:^(id sender) {
        if(_heartTimer != nil) {
            [_heartTimer invalidate];
            _heartTimer = nil;
        }
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
        });
        [weakSelf exitLiveStream];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [dialogBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBgView.alpha = 0.0f;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
        });
    }]];
    
    
    [cancelButton bk_addEventHandler:^(id sender) {
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBgView.alpha = 0.0f;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
        });
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    dialogBgView.alpha = 0;
    
    dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT/2-boxHeight/2, SCREEN_WIDTH, boxHeight);
    
    [UIView animateWithDuration:animationDuration animations:^{
        dialogBgView.alpha = 1.0f;
        dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT/2-boxHeight/2, SCREEN_WIDTH, boxHeight);
    }];
}


- (void)exitLiveStream
{
    [_broadcasterController calDuration];
    
    if (_isLiveStreamStart) {
        
        self.isLiveStreamStart = NO;
        
        switch (_liveStreamMode) {
            case LiveStreamModeBroadcast:
            {
                [_restreamButton removeFromSuperview];
                [_likeButton removeFromSuperview];
                [_fullScreenSwitchButton removeFromSuperview];
                [_cameraSwitchButton removeFromSuperview];
                [_chatStartButton removeFromSuperview];
                [_torchModeSwitchButton removeFromSuperview];
                [_exitButton removeFromSuperview];
                [_lvTable removeFromSuperview];
                [_giftButton removeFromSuperview];
                [_broadcasterController endLivestream];
            }
                break;
                
            case LiveStreamModeWatch:
            {
                [_restreamButton removeFromSuperview];
                [_likeButton removeFromSuperview];
                [_chatStartButton removeFromSuperview];
                [_autoHintView removeFromSuperview];
                [_hudView removeFromSuperview];
                [_lvConnWatcher disconnect];
            }
                break;
        }
    }
}

- (UIImage*)snapshotViewWithBlur:(BOOL)isBlur
{
    UIImage *screenImg;
    UIGraphicsBeginImageContext(CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT));
    [self.view drawViewHierarchyInRect:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) afterScreenUpdates:YES];
    screenImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    if (isBlur) {
        screenImg = [UIImage imageByApplyingBlurToImage:screenImg withRadius:SCREEN_WIDTH/16 tintColor:[UIColor colorWithRed:0.f green:0.f blue:0.f alpha:0.6f] saturationDeltaFactor:1.f maskImage:nil];
    }
    
    return screenImg;
}

#pragma mark - LiveStreamConnejctionWatcher delegate
- (void)phonecall
{

    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)livestreamViewerKilled
{
    [self forceExit:LOCALIZE(@"livestream_viewer_killed_alert")];
}

- (void)connectionLinkFailBy:(LIVE_STREAM_DISCONNECT_TYPE)type
{
    if(_heartTimer != nil) {
        [_heartTimer invalidate];
        _heartTimer = nil;
    }
    switch (type) {
        case LIVE_STREAM_DISCONNECT_TYPE_HOST_END:
        {
            [_restreamButton removeFromSuperview];
            [_likeButton removeFromSuperview];
            [_fullScreenSwitchButton removeFromSuperview];
            [_cameraSwitchButton removeFromSuperview];
            [_chatStartButton removeFromSuperview];
            [_torchModeSwitchButton removeFromSuperview];
            [_exitButton removeFromSuperview];
            [_lvTable removeFromSuperview];
            [_hudView removeFromSuperview];

            [self.view endEditing:YES];

            _blurBgImageView.image = [self snapshotViewWithBlur:YES];
            _blurBgImageView.alpha = 0.0f;
            [self.view addSubview:_blurBgImageView];
            [UIView animateWithDuration:0.5f animations:^{
                _blurBgImageView.alpha = 1.0f;
            }];
            
            [NOTIFICATION_CENTER postNotificationName:REFRESH_FEEDS object:self userInfo:nil];
            [self showLivestreamDetail_watcher];
        }
            break;
        case LIVE_STREAM_DISCONNECT_TYPE_KEEP_ALIVE_FAIL:
        {
            [self forceExit:LOCALIZE(@"network_unstable")];
        }
            break;
        case LIVE_STREAM_DISCONNECT_TYPE_KICKED_BY_HOST:
        {
            [self forceExit:LOCALIZE(@"block_by_host")];
            
        }
            break;
            
        default:
            break;
    }
    [_purchaseGiftViewController dismiss];
    [_lvConnWatcher disconnect];
}

- (void)receiveUdpPacket:(LIVE_STREAM_PACKET_TYPE)packetType data:(NSData*)data
{
    if (packetType == LIVE_STREAM_PACKET_TYPE_LIKE
       && _liveStream.liveViewerCount < 10
       && _liveStream.liveViewerCount > 0) {
        
        [self popoverHeart:data random:NO];
        
    }
    else if (packetType == LIVE_STREAM_PACKET_TYPE_KEEP_ALIVE_PUBLISHER) {
        
    }
    else if (packetType == LIVE_STREAM_PACKET_TYPE_COMMENT) {
        
        [self receiveComment:data];
        
    }
    else if (packetType == LIVE_STREAM_PACKET_TYPE_STREAM_INFO_CHANGE) {
        
        [self livestreamInfoChange:data];
    }
    else if (packetType == LIVE_STREAM_PACKET_TYPE_NEW_GIFT) {
        int32_t liveStreamID = *((int32_t*) (data.bytes + 1));
        int32_t giftToken = *((int32_t*) (data.bytes + 5));
        
        [API_MANAGER getLiveStream:INT_TO_STRING(liveStreamID) giftToken:giftToken withCompletion:^(BOOL success, UserGiftObject *userGift) {
            if (success) {
                if (userGift==nil||userGift.gift.giftID==nil) {
                    return ;
                }
                [_giftAnimateQuene addObject:userGift];
                if (userGift.gift.point>_mostExpensiveAnimatePrice) {
                    _mostExpensiveAnimatePrice=userGift.gift.point;
                    _nextAnimate=(int)[_giftAnimateQuene count]-1;
                }
                [self playAnimate:_nextAnimate];

            }else{
                
            }
            DLog(@"!!!!!! getLiveStream: %@", userGift);
        }];
    }
}

- (BOOL)shouldShowHUDWithType:(HUDType)HUDType enabled:(BOOL)enabled
{
    if (enabled) {
        return [_hudView openHUDWithType:HUDType];
    }
    else {
        return [_hudView closeHUDWithType:HUDType];
    }
}

- (void)initialCommentTable:(NSInteger)windowSize
{
    _lastCommentTime = _lvConnWatcher.startTimestamp - windowSize * 0.8; // only fetch comments after you enter minus the buffer time to match content
    DLog(@"windowSize=%ld, windowSize*0.8=%f", windowSize, windowSize*0.8);
    [self receiveComment:nil];
}

# pragma mark - LiveStreamTableViewDataSource
- (NSInteger)numberOfRowsInLiveSteamTableView:(LiveStreamTableView*)lvTableView
{
    NSInteger i = [_chatMessages count];
    return i;
}

- (LiveStreamChatMessage*)liveStreamTableView:(LiveStreamTableView*)lvTableView dataForRow: (NSInteger) row
{
    return [_chatMessages objectAtIndex:row];
}

# pragma mark - LiveStreamTableViewDelegate
- (void)liveStreamTableView:(LiveStreamTableView*)lvTableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath;
{
    [self singleTap:nil];
}

#pragma mark - WatcherFetcher
- (void)showFollowAutoHint
{
    if(_liveStreamMode == LiveStreamModeBroadcast || _autoHintView.isFollowHintShown){
        return;
    }
    
    [API_MANAGER getUserInfo:_liveStream.user.openID completion:^(BOOL success, UserObject *userObject) {
        if (success) {
            if (userObject.isFollowing == 0
                && (![userObject privacyMode]
                || userObject.followRequestTime == 0)) {
                    
                [_autoHintView openAutoHint:AutoHintTypeFollow withName:_liveStream.user.openID];
            }
        }
        
    }];
}

- (void)fetchLivestreamInfo:(NSString*)livestreamID
{
    
    DLog(@"fetchLiveStream");

    if(livestreamID == nil)
        return;
    
    if(_isFetchingInfo)
        return;

    DLog(@"fetchLiveStream3");

    self.isFetchingInfo = YES;
    
    [API_MANAGER getLiveStreamInfo:livestreamID withCompletion:^(BOOL success, LiveStreamObject *liveSteamInfo) {

        self.isFetchingInfo = NO;
        if(success){
            self.isFirstFetchInfo = NO;
            self.liveStream = liveSteamInfo;
            _liveStreamHeaderViewController.liveStreamObj = liveSteamInfo;
            
            [self showFollowAutoHint];
            
            DLog(@"\n\n_livestream.following=%d",_liveStream.user.isFollowing);
        }
        
    }];
    
}

- (void)forceExit:(NSString*)titleText
{
    if (_canPresentView==YES) {
        return;
    }
    [_liveStreamSubjectTextView resignFirstResponder];
    [self resignMessageInputBar];
    [_messageInputBar.messageTextView endEditing:YES];
    [_liveStreamSubjectTextView endEditing:YES];
    __weak LiveStreamViewController* weakself = self;
    [NOTIFICATION_CENTER postNotificationName:REFRESH_FEEDS object:self userInfo:nil];
    if (_liveStreamMode==LiveStreamModeBroadcast) {
        [_broadcasterController endLivestream];
    }
    [DIALOG_MANAGER showActionSheetOKDialogTitle:titleText message:@"" buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
        [DIALOG_MANAGER hideLoadingView];
        [weakself dismissViewControllerAnimated:YES completion:nil];
    }];
}

#pragma mark LiveViewerDelegate
- (void)didClickUserwithcomment:(UserObject *)user withMessageString:(NSString *)withMessageString
{
    [self resignMessageInputBar];
    [DIALOG_MANAGER showActionSheetUserDialogcommentver:user isPublisher:(_liveStreamMode == LiveStreamModeBroadcast) isLivestreamer:([user.userID isEqualToString:_liveStream.userID]) withCompletion:^(NSString *selectedAction) {
        if ([LoginHandler isGuest]) {
            [LoginHandler handleUserLogin];
            return;
        }
        
        if ([selectedAction isEqualToString:@"follow"]) {
            [self followUser:user];
        }
        else if ([selectedAction isEqualToString:@"response"]) {
            [self responseUser:user];
        }
        else if ([selectedAction isEqualToString:@"unfollow"]){
            [self unfollowUser:user];
        }
        else if ([selectedAction isEqualToString:@"report"]){
            [self reportUserAction:user withMessageString:withMessageString];
        }
        else if ([selectedAction isEqualToString:@"block"]){
            [self blockUserAction:user];
        }
        else if ([selectedAction isEqualToString:@"unblock"]){
            [self unblockUserAction:user];
        }
        else if ([selectedAction isEqualToString:@"admin"]){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self showAdminDialog:user messageString:withMessageString];
            });
        }
    }];
}

- (void)showAdminDialog:(UserObject*)user messageString:(NSString*)messageString
{

    [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"Admin_Button") options:@[LOCALIZE(@"admin_ban_user"),LOCALIZE(@"admin_freeze_user"),LOCALIZE(@"admin_block_dirty_word"),LOCALIZE(@"admin_block_dirty_user"),LOCALIZE(@"block")] destructiveIndexes:@[@0,@1] cancelable:YES withCompletion:^(int selectedOption) {
        if(selectedOption == 0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self adminBanAction:user];
            });
        }
        else if (selectedOption == 1) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self adminFreezeAction:user];
            });
        }
        else if (selectedOption == 2) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self adminBlockDirtyWord:user messageString:messageString];
            });
        }
        else if (selectedOption == 3) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self adminBlockDirtyUser:user];
            });
        }
        else if (selectedOption == 4) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self adminBlockAction:user];
            });
        }
    }];
}

- (void)adminBlockDirtyUser:(UserObject*)user{
    
    /**   **/
    [DIALOG_MANAGER showActionSheetDialogTitle:[NSString stringWithFormat:LOCALIZE(@"DIRTY_USER_ALERT"),user.openID] options:@[LOCALIZE(@"OK")] destructiveIndexes:@[@0,@1] cancelable:YES withCompletion:^(int selectedOption) {
        
        if (selectedOption == 0) {
            
           [API_MANAGER adOnAction:user.userID withCompletion:^(BOOL success, NSString *message) {
               if(success){
                   [DIALOG_MANAGER showCompleteToast];
               }else{
                   [DIALOG_MANAGER showNetworkFailToast];
               }
           }];
        }
    }];
}

-(void)adminKillLiveBtn:(id)sender
{
    [DIALOG_MANAGER showActionSheetDialogTitle:[NSString stringWithFormat:LOCALIZE(@"KILL_LIVE_ALERT"),_liveStream.liveStreamID] options:@[LOCALIZE(@"OK")] destructiveIndexes:@[@0,@1] cancelable:YES withCompletion:^(int selectedOption) {
        
        if (selectedOption == 0) {
            
            [API_MANAGER killLiveStreamAction:_liveStream.liveStreamID targetUserID:_liveStream.user.userID withCompletion:^(BOOL success) {
                if(success){
                    [DIALOG_MANAGER showCompleteToast];
                }else{
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
            
//            [API_MANAGER adOnAction:user.userID withCompletion:^(BOOL success, NSString *message) {
//                if(success){
//                    [DIALOG_MANAGER showCompleteToast];
//                }else{
//                    [DIALOG_MANAGER showNetworkFailToast];
//                }
//            }];
        }
    }];
    
}


- (void)adminBlockDirtyWord:(UserObject*)user messageString:(NSString*)messageString{
    
    /**   **/
    NSString* message = [messageString substringToIndex:[messageString length]-5];
    [DIALOG_MANAGER showActionSheetDialogTitle:message options:@[LOCALIZE(@"OK")] destructiveIndexes:@[@0,@1] cancelable:YES withCompletion:^(int selectedOption) {
        
        if (selectedOption == 0) {
            
            [API_MANAGER adminBlockDirtyWordAction:message withCompletion:^(BOOL success) {
                if (success) {
                    [DIALOG_MANAGER showCompleteToast];
                }
                else {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
        
        }
    }];
}

- (void)adminBlockAction:(UserObject*)user
{
    [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"Block_confirm") options:@[LOCALIZE(@"OK")] destructiveIndexes:@[@0,@1] cancelable:YES withCompletion:^(int selectedOption) {
        if (selectedOption == 0) {
            [API_MANAGER adminBlockUserAction:MY_USER_ID publisherUserID:_liveStream.userID blockedUserID:user.userID inLivestreamID:_liveStream.liveStreamID  withCompletion:^(BOOL success) {
                [DIALOG_MANAGER showCompleteToast];
            }];
        }
    }];
}


- (void)adminBanAction:(UserObject*)user
{
    [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"Ban_confirm") options:@[LOCALIZE(@"OK")] destructiveIndexes:@[@0,@1] cancelable:YES withCompletion:^(int selectedOption) {
        if (selectedOption == 0) {
            [API_MANAGER banUserAction:user.userID inLivestreamID:_liveStream.liveStreamID absTime:_lvConnWatcher.currentAbsTimestamp withCompletion:^(BOOL success, NSString *message) {
                [DIALOG_MANAGER showCompleteToast];
            }];
        }
    }];
}

- (void)adminFreezeAction:(UserObject*)user
{
    [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"Freeze_confirm") options:@[LOCALIZE(@"OK")] destructiveIndexes:@[@0,@1] cancelable:YES withCompletion:^(int selectedOption) {
        if (selectedOption == 0) {
            [API_MANAGER freezeUserAction:user.userID inLivestreamID:_liveStream.liveStreamID absTime:_lvConnWatcher.currentAbsTimestamp  withComletion:^(BOOL success,NSString* message) {
                [DIALOG_MANAGER showCompleteToast];
            }];
        }
    }];
}

- (void)didClickBackground
{
    [self singleTap:nil];
}


- (void)blockUserAction:(UserObject*)user
{
    [API_MANAGER blockUserAction:user.userID withCompletion:^(BOOL success) {
        if (success) {
            user.isBlocked = 1;
            [DIALOG_MANAGER showCompleteToast];
        }
    }];
}

- (void)unblockUserAction:(UserObject*)user
{
    [API_MANAGER unblockUserAction:user.userID withCompletion:^(BOOL success) {
        if (success) {
            user.isBlocked = 0;
            [DIALOG_MANAGER showCompleteToast];
        }
    }];
}

- (void)cancelFollowUserRequest:(UserObject *)user
{
    user.followRequestTime = 0;
    [API_MANAGER cancelFollowRequests:user.userID withCompletion:^(BOOL success) {
        if (success) {
            [DIALOG_MANAGER showCompleteToast];
        }
    }];
}

- (void)followUser:(UserObject*)user
{
    _autoHintView.isFollowHintShown = YES;
    
    if ([GET_DEFAULT(FOLLOWING_COUNT) intValue] >= MAX_FOLLOWER_COUNT) {
        [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"cannot_follow_user") message:[NSString stringWithFormat:LOCALIZE(@"max_followcount"),MAX_FOLLOWER_COUNT] buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
            if (okClicked) {
                
            }
        }];
    }
    else {
        if (![user isPrivacyMode]) {
            [API_MANAGER followUserAction:user.userID withCompletion:^(BOOL success) {
                if (success) {
                    user.isFollowing = 1;
                    [DIALOG_MANAGER showCompleteToast];
                }
            }];
        }
        else {
            user.followRequestTime = CURRENT_TIMESTAMP;
            [API_MANAGER sendFollowRequest:user.userID withCompletion:^(BOOL success) {
                if (success) {
                    [DIALOG_MANAGER showCompleteToast];
                }
            }];
        }
    }
}

- (void)reportUserAction:(UserObject*)user withMessageString:(NSString *)withMessageString
{
    [API_MANAGER reportUserAction:user.userID withReason:withMessageString andMessage:@"Livestream Comment Report" withCompletion:^(BOOL success, NSString *message) {
        if (success) {
            
            [DIALOG_MANAGER showCompleteToast];
        }
    }];
}

- (void)reportUserAction:(UserObject*)user
{
    [API_MANAGER reportUserAction:user.userID withReason:@"in livestream,no certainly reason" andMessage:@"Livestream Comment Report" withCompletion:^(BOOL success, NSString *message) {
        if (success) {
            [DIALOG_MANAGER showCompleteToast];
        }
    }];
}

- (void)unfollowUser:(UserObject*)user
{
    
    [API_MANAGER unfollowUserAction:user.userID withCompletion:^(BOOL success) {
        
        if (success) {
            user.isFollowing = 0;
            [DIALOG_MANAGER showCompleteToast];
        }
    }];
}


- (void)responseUser:(UserObject*)user
{
    [_messageInputBar.messageTextView setText:[NSString stringWithFormat:@"@%@ ",user.openID]];
    [self becomeFirstMessageInputBar];
}

- (void)becomeFirstMessageInputBar
{
    [_messageInputBar.messageTextView becomeFirstResponder];
    [UIView animateWithDuration:0.2 animations:^{
        _messageInputBar.alpha = 1.0f;
    }];
}

- (void)resignMessageInputBar
{
    [_messageInputBar.messageTextView resignFirstResponder];
    [UIView animateWithDuration:0.2 animations:^{
        _messageInputBar.alpha = 0.0f;
    }];
}

#pragma mark - Notifications

- (void)enterBackground:(id)sender
{
    self.isBackground = YES;
    _skView.paused = YES;
    
    if (_liveStreamMode == LiveStreamModeBroadcast ) {
        _broadcasterController.audioMode=YES;
        if (_isLiveStreamStart) {
            _liveStream.audioOnly = YES;
            if (_liveStream.liveStreamID!=nil) {
                [API_MANAGER updateLiveStreamInfo:@{@"liveStreamID":_liveStream.liveStreamID,@"audioOnly":@"1"} withCompletion:^(BOOL success) {
                    
                }];
            }
        }
    }
    
    [self updateNowPlaying];
}

- (void)enterForeground:(id)sender
{
    _skView.paused = NO;
    self.isBackground = NO;
    _broadcasterController.audioMode = NO;

    if (_liveStreamMode == LiveStreamModeBroadcast && _isLiveStreamStart) {
        _liveStream.audioOnly = NO;
        if (_liveStream.liveStreamID!=nil) {
            [API_MANAGER updateLiveStreamInfo:@{@"liveStreamID":_liveStream.liveStreamID,@"audioOnly":@"0"} withCompletion:^(BOOL success) {
            }];
        }
        
        
    }
}

- (void)keyboardNotificationHandler:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    _keyboardHeight = keyboardFrame.size.height;
    
    if(_liveStreamMode == LiveStreamModeBroadcast && !_isLiveStreamStart){
        /* Start Button Adjust */
        
        _keyboardHeight = keyboardFrame.size.height;
        
//        _liveStreamStartButton.frame = CGRectMake(SCREEN_WIDTH/2-LIVE_STREAM_BUTTON_WIDTH/2, SCREEN_HEIGHT-keyboardFrame.size.height-LIVE_STREAM_BUTTON_HEIGHT-2*LIVE_STREAM_VIEW_PADDING,LIVE_STREAM_BUTTON_WIDTH, LIVE_STREAM_BUTTON_HEIGHT);

    }
}

- (void)userDidTakeScreenshot:(NSNotification *)notification
{
    [_autoHintView openAutoHint:AutoHintTypeShare withName:nil];
}

/* Media Settings  */
- (void)updateNowPlaying;
{
    if ([MPNowPlayingInfoCenter class]) {
        UIImage *musicImage = [UIImage imageNamed:@"onair"];
        MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc]
                                        initWithImage:musicImage];
        
        NSString *trackName = _liveStream.user.openID;
        NSString *albumName = _liveStream.caption;
        
        NSArray *objs = [NSArray arrayWithObjects:
                         trackName,albumName,
                         albumArt, nil];
        
        NSArray *keys = [NSArray arrayWithObjects:
                         MPMediaItemPropertyTitle,
                         MPMediaItemPropertyAlbumTitle,
                         MPMediaItemPropertyArtwork, nil];
        
        // TRICKY: TO BLOCK CRASH!
        if(objs.count!=keys.count) {
            return;
        }
        
        NSDictionary *currentTrackInfo = [NSDictionary dictionaryWithObjects:objs forKeys:keys];
        [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = currentTrackInfo;
        MPRemoteCommandCenter *remoteCommandCenter = [MPRemoteCommandCenter sharedCommandCenter];
        [[remoteCommandCenter seekForwardCommand] addTarget:self action:@selector(remoteCommandFunction)];
    }
}

- (void)notifySocialLogin:(NSNotification *)notification
{
    // Handle social login notification...
}

- (void)notifyLogin:(NSNotification *)notification
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)notifySignUp:(NSNotification *)notification
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)remoteCommandFunction
{
    //prevent from crash---york
}
#pragma mark - Action for diff Packet

- (void)popoverHeart:(NSData*)data random:(BOOL)random
{
    self.heartCount++;
    if (_isBackground) {
        return;
    }
 
    u_int32_t rValue = *((u_int32_t*) (data.bytes+5));
    u_int32_t gValue = *((u_int32_t*) (data.bytes+9));
    u_int32_t bValue = *((u_int32_t*) (data.bytes+13));
    
    
    NSString* hexString = [NSString stringWithFormat:@"#%2X%2X%2X",rValue,gValue,bValue];
    int colorCode = [SINGLETON hexString2ColorCode:hexString];
    
    if (random) {
        colorCode = rand() % 28;
    }
    
    dispatch_async(MAIN_QUEUE, ^{
        
        if (!_fullScreenSwitchButton.isSelected) {
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
//                [_scene showAnimation1FromPoint:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2)];
                [_scene showheartFrompout:_likeButton.center selectedColor:colorCode];
            }
            else {
                //                [DIALOG_MANAGER showSameHeartFromPoint:_likeButton.center selectedColor:colorCode];
                [DIALOG_MANAGER showSingleEmitterHeartFromPoint:_likeButton.center selectedColor:colorCode];
            }
        }
        _likeCount++;
//        if(_likeCount%1000==0 ){
//            [self.view emitterAnimation:_likeButton.center];
//        }
        
    });
}

-(void)livestreamInfoChange:(NSData*)data
{
    [self fetchLivestreamInfo:_liveStream.liveStreamID];
}

- (void)receiveComment:(NSData*)data
{
    if(_isFetchingComment || _liveStream.liveStreamID == nil) {
        return;
    }

    self.isFetchingComment = YES;
    
    [API_MANAGER getLiveStreamComments:_liveStream.liveStreamID afterTime:_lastCommentTime-1 commentCount:LIVE_STREAM_MAX_CHAT_COMMENT_COUNT withCompletion:^(BOOL success, NSArray *liveStreamCommentObjects) {
        
        self.isFetchingComment = NO;
        if (success) {
            
            for (int i = 0; i < [liveStreamCommentObjects count]; i++) {
                
                LiveStreamCommentObject *obj = [liveStreamCommentObjects objectAtIndex:i];
                LiveStreamChatMessage *msg = [[LiveStreamChatMessage alloc] initWithLiveStreamCommentObject:obj andAtUser:@""];
                NSString* livestreamMsgID = [msg getLivestreamCommentID];
                if(![_receivedMessageID containsObject:livestreamMsgID]){
                    
                    [_chatMessages addObject:msg];
                }
                [_receivedMessageID addObject:livestreamMsgID];
                _cellTotalHeight += [LiveStreamTableViewCell getHeightForCell:msg];
                
                if (i == [liveStreamCommentObjects count]-1){
                    _lastCommentTime = obj.timestamp;
                }
            }
            
            [_lvTable reloadData];
            
            CGRect rect = _lvTable.frame;
            float messageInputBarHeight = _messageInputBar.frame.size.height;
            float keyboardHeight = 0.0f;
            if (_messageInputBar.messageTextView.isFirstResponder) {
                keyboardHeight = _keyboardHeight;
            }
            
            float tableHeight = (_cellTotalHeight > maxTableHeight? maxTableHeight : _cellTotalHeight);
            rect.origin.y = SCREEN_HEIGHT - 28 - tableHeight - messageInputBarHeight - keyboardHeight;
            rect.size.height = tableHeight;
            [UIView animateWithDuration:0.2 animations:^{
                _lvTable.frame = rect;
                _bgImageView.frame = CGRectMake(5,
                                                _lvTable.frame.origin.y-5-_bgImageView.frame.size.height,
                                                _bgImageView.frame.size.width,
                                                _bgImageView.frame.size.height);

            }];
            
            if ([_chatMessages count] > 1 && _lvTable.isOnBottom) {
                
                /* truncate comment to MAX_COMMENT_COUNT */
                if ([_chatMessages count] >= 2 * MAX_COMMENT_COUNT) {
                    [_chatMessages removeObjectAtIndex:0];
                    NSArray *truncateArray = [_chatMessages subarrayWithRange:NSMakeRange([_chatMessages count]-1-MAX_COMMENT_COUNT, MAX_COMMENT_COUNT)];
                    _chatMessages = [truncateArray mutableCopy];
                    [_lvTable reloadData];
                }
                
                [_lvTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_chatMessages count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            }
        }
    }];
}


- (void)restream
{
    if (_liveStream == nil) {
        return;
    }
    
    if ([LoginHandler isGuest]) {
        [LoginHandler handleUserLogin];
        return;
    }
    
//    NSString* liveStreamID = _liveStream.liveStreamID ;
    NSString* liveStreamerOpenID = _liveStream.user.openID ;
    NSString* myOpenID = GET_DEFAULT(USER_OPEN_ID) ;

    [DIALOG_MANAGER showActionSheetShareLink:ShareMediaTypeLive withObj:_liveStream hasRestream:_hasRestream withCompletion:^(NSString *action) {
        
        if ([action isEqualToString:@"share_17"]) {
            if (_hasRestream) {
                return ;
            }
            [API_MANAGER hasRestreamedLiveStream:_liveStream.liveStreamID withCompletion:^(BOOL success, int hasRestreamed) {
                if(success){
                    if (hasRestreamed==0){
                        _hasRestream=YES;
                        [DIALOG_MANAGER showCompleteToast];
                        [API_MANAGER restreamLiveStream:_liveStream.liveStreamID liveStreamerOpenID:liveStreamerOpenID myOpenID:myOpenID withCompletion:^(BOOL success) {
                            [API_MANAGER commentLiveStream:_liveStream.liveStreamID comment:LOCALIZE(@"Livestream_Restream_message") colorCode:_lvConnWatcher.colorCode absTimestamp:_lvConnWatcher.currentAbsTimestamp withCompletion:^(BOOL success, NSString *message) {}];
                        }];                        }
                }else{
                    //                        [self.view addSubview:_restreamButton];
                }
            }];
            
            
        }
        else if ([action isEqualToString:@"share_fb"]) {
            
        }
        else if ([action isEqualToString:@"share_twitter"]) {
        
        }
        else if ([action isEqualToString:@"share_ig"]) {
            
        }
        else if ([action isEqualToString:@"share_weibo"]) {
            
        }
        else if ([action isEqualToString:@"share_copy"]) {
            
        }
        else if ([action isEqualToString:@"share_mail"]) {
            
        }
        else if ([action isEqualToString:@"share_message"]) {
            
        }
        else {
            
        }
        if (![action isEqualToString:@"cancel"]) {
            [EVENT_HANDLER addEventTracking:@"Share_Live" withDict:@{@"channel":action,@"liveStreamID":_liveStream.liveStreamID,@"targetUserID":_liveStream.userID}];
            
        }
    }];

}


- (void)dealloc
{
    DLog(@"dealloc");
}

#pragma mark BroadcasterDelegate
- (void)startBroadcast:(BOOL)success withLivestream:(LiveStreamObject*)livestream
{
    if (success) {
        self.liveStream = livestream;
        [self fetchLivestreamInfo:_liveStream.liveStreamID];
        [self removeSettingView];
        self.isLiveStreamStart = YES;
        _liveStreamDidStart=YES;
//        [_liveStreamStartButton setSelected:NO];
    }else {
        self.isLiveStreamStart = NO;
        _liveStreamDidStart=NO;
        [_liveStreamStartButton setSelected:NO];
        [DIALOG_MANAGER showNetworkFailToast];
        [self forceExit:LOCALIZE(@"Live_stream_ended")];
    }
    
}

-(void)livestreamKilled
{
    [self forceExit:LOCALIZE(@"livestream_killed_alert")];
}

- (void)didReceiveJsonFormatData:(LIVESTREAM_ACTION_TYPE)type infoDict:(NSDictionary*)infoDict
{
    DLog(@"receive Server Ack : %@",infoDict);
}

- (void)broadcastKeepAliveFail:(int)failTimes
{
    if (failTimes == 6) {
        if (_canPresentView==NO) {
            [self forceExit:LOCALIZE(@"network_unstable")];
        }
    }
}
- (void)broadcastReceiveUdpPacket:(LIVE_STREAM_PACKET_TYPE)packetType data:(NSData*)data
{

    if (packetType == LIVE_STREAM_PACKET_TYPE_LIKE && _liveStream.liveViewerCount < 10) {
        [self popoverHeart:data random:NO];
        
    }
    else if (packetType == LIVE_STREAM_PACKET_TYPE_KEEP_ALIVE_PUBLISHER) {
        
    }
    else if (packetType == LIVE_STREAM_PACKET_TYPE_COMMENT) {
        
        [self receiveComment:data];
        
    }
    else if (packetType == LIVE_STREAM_PACKET_TYPE_STREAM_INFO_CHANGE) {
        
        [self livestreamInfoChange:data];
    }
    else if (packetType == LIVE_STREAM_PACKET_TYPE_NEW_GIFT) {
        int32_t liveStreamID = *((int32_t*) (data.bytes + 1));
        int32_t giftToken = *((int32_t*) (data.bytes + 5));
        
        [API_MANAGER getLiveStream:INT_TO_STRING(liveStreamID) giftToken:giftToken withCompletion:^(BOOL success, UserGiftObject *userGift) {
                if (userGift == nil || userGift.gift.giftID == nil) {
                    return ;
                }
            if (success) {
                userGift.gift.giftToken = giftToken;
                [_giftAnimateQuene addObject:userGift];
                if (userGift.gift.point > _mostExpensiveAnimatePrice) {
                    _mostExpensiveAnimatePrice = userGift.gift.point;
                    _nextAnimate = (int)[_giftAnimateQuene count] - 1;
                }
                [self playAnimate:_nextAnimate];

            }
            else {
                
            }
            DLog(@"!!!!!! getLiveStream: %@", userGift);
        }];
    }
}

- (void)endlivestreamWithLivestreamInfo:(LiveStreamObject *)livestream
{
    /* Show livestream Result */
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        _blurBgImageView.image = [self snapshotViewWithBlur:YES];
        _blurBgImageView.alpha = 0.f;
        [_broadcasterController.glVideoOutputView addSubview:_blurBgImageView];
        [UIView animateWithDuration:0.5f animations:^{
            _blurBgImageView.alpha = 1.f;
        }];
        
        // show live stream data
        [self resignMessageInputBar];
        
        __weak LiveStreamViewController* weakSelf = self;
        //        [DIALOG_MANAGER showActionSheetLivestreamDetail:_livestream withCompletion:^(NSString *selectedAction) {
        //            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        //        }];
        
        [weakSelf showLivestreamDetail];
    });
}

- (void)updatelivestream:(LiveStreamObject*)livestream
{
    self.liveStream = livestream;
}

- (void)showLivestreamDetail
{
    __weak LiveStreamViewController* weakSelf = self;
    
    UIWindow *topView = (UIWindow *)[UIApplication sharedApplication].keyWindow;
    
    float duration = _broadcasterController.duration;
    
    int topMargin = 7;
    int btnHeight = 35;
    
    float animationDuration = 0.3f;
    float textLabelHeight = 30.0f;
    float dataLabelHeight = 60.0f;
    
    UIImageView* dialogBoxView = [UIImageView new];
    
    dialogBoxView.userInteractionEnabled = YES;
    [topView addSubview:dialogBoxView];
    
    int nextY = topMargin;
    /* Add Label */
    
    UILabel* endLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [endLabel setTextColor:WHITE_COLOR];
    endLabel.textAlignment = NSTextAlignmentCenter;
    endLabel.font = BOLD_FONT_WITH_SIZE(35);
    [endLabel setText:LOCALIZE(@"livestreamended")];
    [endLabel sizeToFit];
    endLabel.center = CGPointMake(SCREEN_WIDTH/2, nextY + endLabel.frame.size.height/2);
    [dialogBoxView addSubview:endLabel];
    nextY += endLabel.frame.size.height+30;
    
    UILabel* totalViewLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [totalViewLabel setTextColor:WHITE_COLOR];
    totalViewLabel.textAlignment = NSTextAlignmentCenter;
    totalViewLabel.font = BOLD_FONT_WITH_SIZE(25);
    
    [totalViewLabel setText:INT_TO_STRING(_liveStream.viewerCount)];
    [totalViewLabel sizeToFit];
    totalViewLabel.center = CGPointMake(SCREEN_WIDTH/4+10, nextY + totalViewLabel.frame.size.height/2);
    [dialogBoxView addSubview:totalViewLabel];
    
    
    UILabel* totalLikeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, dataLabelHeight)];
    
    
    [totalLikeLabel setTextColor:WHITE_COLOR];
    totalLikeLabel.textAlignment = NSTextAlignmentCenter;
    totalLikeLabel.font = BOLD_FONT_WITH_SIZE(25);
    [totalLikeLabel setText:INT_TO_STRING(_heartCount)];
    [totalLikeLabel sizeToFit];
    totalLikeLabel.center = CGPointMake(3*SCREEN_WIDTH/4-10, nextY + totalLikeLabel.frame.size.height/2);
    [dialogBoxView addSubview:totalLikeLabel];
    
    nextY += totalLikeLabel.frame.size.height+10;
    
    UILabel* viewsLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, textLabelHeight)];
    [viewsLabel setTextColor:WHITE_COLOR];
    viewsLabel.textAlignment = NSTextAlignmentCenter;
    viewsLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    [viewsLabel setText:LOCALIZE(@"Total_Views")];
    [viewsLabel sizeToFit];
    viewsLabel.center = CGPointMake(totalViewLabel.center.x, nextY + viewsLabel.frame.size.height/2);
    [dialogBoxView addSubview:viewsLabel];
    
    UILabel* likesLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, textLabelHeight)];
    [likesLabel setTextColor:WHITE_COLOR];
    likesLabel.textAlignment = NSTextAlignmentCenter;
    likesLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    
    [likesLabel setText:LOCALIZE(@"Total_Likes")];
    [likesLabel sizeToFit];
    likesLabel.center = CGPointMake(totalLikeLabel.center.x, nextY + likesLabel.frame.size.height/2);
    [dialogBoxView addSubview:likesLabel];
    
    nextY += viewsLabel.frame.size.height+topMargin;
    
    UILabel* totalTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [totalTimeLabel setTextColor:WHITE_COLOR];
    totalTimeLabel.textAlignment = NSTextAlignmentCenter;
    totalTimeLabel.font = BOLD_FONT_WITH_SIZE(25);
    
    [totalTimeLabel setText:[SINGLETON getTimeFormatBySecond:_liveStream.totalViewTime]];
    totalTimeLabel.numberOfLines = 1;
    totalTimeLabel.adjustsFontSizeToFitWidth = YES;
    [totalTimeLabel sizeToFit];
    
    if (totalTimeLabel.frame.size.width > SCREEN_WIDTH / 2) {
        totalTimeLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH/2, textLabelHeight);
    }
    
    totalTimeLabel.center = CGPointMake(totalViewLabel.center.x, nextY + totalTimeLabel.frame.size.height/2);
    [dialogBoxView addSubview:totalTimeLabel];
    
    UILabel* durationLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [durationLabel setTextColor:WHITE_COLOR];
    durationLabel.textAlignment = NSTextAlignmentCenter;
    durationLabel.font = BOLD_FONT_WITH_SIZE(25);
    
    
    [durationLabel setText:[SINGLETON getTimeFormatBySecond:duration]];
    [durationLabel sizeToFit];
    durationLabel.center = CGPointMake(totalLikeLabel.center.x, nextY + durationLabel.frame.size.height/2);
    [dialogBoxView addSubview:durationLabel];
    
    nextY += totalTimeLabel.frame.size.height+3;
    
    UILabel* timesLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, textLabelHeight)];
    [timesLabel setTextColor:WHITE_COLOR];
    timesLabel.textAlignment = NSTextAlignmentCenter;
    timesLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    
    [timesLabel setText:LOCALIZE(@"Total_Duration")];
    [timesLabel sizeToFit];
    timesLabel.center = CGPointMake(totalViewLabel.center.x, nextY + timesLabel.frame.size.height/2);
    [dialogBoxView addSubview:timesLabel];
    
    UILabel* dLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, textLabelHeight)];
    [dLabel setTextColor:WHITE_COLOR];
    dLabel.textAlignment = NSTextAlignmentCenter;
    dLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    
    [dLabel setText:LOCALIZE(@"Live_Duration")];
    [dLabel sizeToFit];
    dLabel.center = CGPointMake(totalLikeLabel.center.x, nextY + dLabel.frame.size.height/2);
    [dialogBoxView addSubview:dLabel];
    
    nextY += timesLabel.frame.size.height+topMargin;
    
    /* Add Buttons */
    
    float boxHeight = 0.0f;
    
    boxHeight = nextY+btnHeight+topMargin;
    
    UIButton* deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteBtn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    [deleteBtn setTitleColor:ALERT_TEXT_COLOR forState:UIControlStateNormal];
    deleteBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH/3, btnHeight);
    deleteBtn.layer.cornerRadius = btnHeight/2.0f;
    [deleteBtn.layer setBorderColor:[ALERT_TEXT_COLOR CGColor]];
    deleteBtn.layer.borderWidth = 1.0f;
    deleteBtn.center = CGPointMake(timesLabel.center.x, nextY+btnHeight/2);
    
    [deleteBtn setTitle:LOCALIZE(@"Delete_Replay") forState:UIControlStateNormal];
    //    [dialogBoxView addSubview:deleteBtn];
    
    UIButton* saveBtn = [UIButton new];
    [saveBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateNormal];
    [saveBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateHighlighted];
    [saveBtn setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    
    saveBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH*2/3, btnHeight);
    saveBtn.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2+30);
    [saveBtn setTitle:LOCALIZE(@"OK") forState:UIControlStateNormal];
    [dialogBoxView addSubview:saveBtn];
    
    
    [deleteBtn bk_addEventHandler:^(id sender) {
        
        [UIView animateWithDuration:animationDuration animations:^{
            
            //            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBoxView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBoxView removeFromSuperview];
            
        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    [saveBtn bk_addEventHandler:^(id sender) {
        [UIView animateWithDuration:animationDuration animations:^{
            //  dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBoxView.alpha = 0.0f;
            
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBoxView removeFromSuperview];
            
        });
        
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT/2-boxHeight/2, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    [UIView animateWithDuration:animationDuration animations:^{
        dialogBoxView.alpha = 1.0f;
    }];
}


- (void)showLivestreamDetail_watcher
{
    __weak LiveStreamViewController* weakSelf = self;
    
    UIWindow *topView = (UIWindow *)[UIApplication sharedApplication].keyWindow;
    
    float duration = _broadcasterController.duration;
    
    int topMargin = 7;
    int btnHeight = 35;
    
    float animationDuration = 0.3f;
    float textLabelHeight = 30.0f;
    float dataLabelHeight = 60.0f;
    
    UIImageView* dialogBoxView = [UIImageView new];
    
    dialogBoxView.userInteractionEnabled = YES;
    [topView addSubview:dialogBoxView];
    
    int nextY = topMargin;
    /* Add Label */
    
    UILabel* endLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [endLabel setTextColor:WHITE_COLOR];
    endLabel.textAlignment = NSTextAlignmentCenter;
    endLabel.font = BOLD_FONT_WITH_SIZE(35);
    [endLabel setText:LOCALIZE(@"livestreamended")];
    [endLabel sizeToFit];
    endLabel.center = CGPointMake(SCREEN_WIDTH/2, nextY + endLabel.frame.size.height/2);
    [dialogBoxView addSubview:endLabel];
    nextY += endLabel.frame.size.height + 30;
    
    UILabel* totalViewLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [totalViewLabel setTextColor:WHITE_COLOR];
    totalViewLabel.textAlignment = NSTextAlignmentCenter;
    totalViewLabel.font = BOLD_FONT_WITH_SIZE(25);
    
    [totalViewLabel setText:INT_TO_STRING(_liveStream.viewerCount)];
    [totalViewLabel sizeToFit];
    totalViewLabel.center = CGPointMake(SCREEN_WIDTH/4+10, nextY + totalViewLabel.frame.size.height/2);
    //    [dialogBoxView addSubview:totalViewLabel];
    
    UILabel* totalLikeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, dataLabelHeight)];
    
    [totalLikeLabel setTextColor:WHITE_COLOR];
    totalLikeLabel.textAlignment = NSTextAlignmentCenter;
    totalLikeLabel.font = BOLD_FONT_WITH_SIZE(25);
    [totalLikeLabel setText:INT_TO_STRING(_heartCount)];
    [totalLikeLabel sizeToFit];
    totalLikeLabel.center = CGPointMake(3*SCREEN_WIDTH/4-10, nextY + totalLikeLabel.frame.size.height/2);
    //    [dialogBoxView addSubview:totalLikeLabel];
    
    nextY += totalLikeLabel.frame.size.height + 10;
    
    UILabel* viewsLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, textLabelHeight)];
    [viewsLabel setTextColor:WHITE_COLOR];
    viewsLabel.textAlignment = NSTextAlignmentCenter;
    viewsLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    [viewsLabel setText:LOCALIZE(@"Total_Views")];
    [viewsLabel sizeToFit];
    viewsLabel.center = CGPointMake(totalViewLabel.center.x, nextY + viewsLabel.frame.size.height/2);
    //    [dialogBoxView addSubview:viewsLabel];
    
    UILabel* likesLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, textLabelHeight)];
    [likesLabel setTextColor:WHITE_COLOR];
    likesLabel.textAlignment = NSTextAlignmentCenter;
    likesLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    
    [likesLabel setText:LOCALIZE(@"Total_Likes")];
    [likesLabel sizeToFit];
    likesLabel.center = CGPointMake(totalLikeLabel.center.x, nextY + likesLabel.frame.size.height/2);
    //    [dialogBoxView addSubview:likesLabel];
    
    nextY += viewsLabel.frame.size.height + topMargin;
    
    
    UILabel* totalTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [totalTimeLabel setTextColor:WHITE_COLOR];
    totalTimeLabel.textAlignment = NSTextAlignmentCenter;
    totalTimeLabel.font = BOLD_FONT_WITH_SIZE(25);
    
    [totalTimeLabel setText:[SINGLETON getTimeFormatBySecond:_liveStream.totalViewTime]];
    totalTimeLabel.numberOfLines = 1;
    totalTimeLabel.adjustsFontSizeToFitWidth = YES;
    [totalTimeLabel sizeToFit];
    if (totalTimeLabel.frame.size.width > SCREEN_WIDTH / 2) {
        totalTimeLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH/2, textLabelHeight);
    }
    totalTimeLabel.center = CGPointMake(totalViewLabel.center.x, nextY + totalTimeLabel.frame.size.height/2);
    //    [dialogBoxView addSubview:totalTimeLabel];
    
    UILabel* durationLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [durationLabel setTextColor:WHITE_COLOR];
    durationLabel.textAlignment = NSTextAlignmentCenter;
    durationLabel.font = BOLD_FONT_WITH_SIZE(25);
    
    [durationLabel setText:[SINGLETON getTimeFormatBySecond:duration]];
    [durationLabel sizeToFit];
    durationLabel.center = CGPointMake(totalLikeLabel.center.x, nextY + durationLabel.frame.size.height/2);
    //    [dialogBoxView addSubview:durationLabel];
    
    nextY += totalTimeLabel.frame.size.height + 3;
    
    UILabel* timesLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, textLabelHeight)];
    [timesLabel setTextColor:WHITE_COLOR];
    timesLabel.textAlignment = NSTextAlignmentCenter;
    timesLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    
    [timesLabel setText:LOCALIZE(@"Total_Duration")];
    [timesLabel sizeToFit];
    timesLabel.center = CGPointMake(totalViewLabel.center.x, nextY + timesLabel.frame.size.height/2);
    //    [dialogBoxView addSubview:timesLabel];
    
    UILabel* dLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, textLabelHeight)];
    [dLabel setTextColor:WHITE_COLOR];
    dLabel.textAlignment = NSTextAlignmentCenter;
    dLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    
    [dLabel setText:LOCALIZE(@"Live_Duration")];
    [dLabel sizeToFit];
    dLabel.center = CGPointMake(totalLikeLabel.center.x, nextY + dLabel.frame.size.height/2);
    //    [dialogBoxView addSubview:dLabel];
    
    nextY += timesLabel.frame.size.height + topMargin;
    
    /* Add Buttons */
    
    float boxHeight = 0.0f;
    
    boxHeight = nextY + btnHeight + topMargin;
    
    UIButton* deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteBtn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    [deleteBtn setTitleColor:ALERT_TEXT_COLOR forState:UIControlStateNormal];
    deleteBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH/3, btnHeight);
    deleteBtn.layer.cornerRadius = btnHeight / 2.0f;
    [deleteBtn.layer setBorderColor:[ALERT_TEXT_COLOR CGColor]];
    deleteBtn.layer.borderWidth = 1.0f;
    deleteBtn.center = CGPointMake(timesLabel.center.x, nextY+btnHeight/2);
    
    [deleteBtn setTitle:LOCALIZE(@"Delete_Replay") forState:UIControlStateNormal];
    //    [dialogBoxView addSubview:deleteBtn];
    
    UIButton* saveBtn = [UIButton new];
    [saveBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateNormal];
    [saveBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateHighlighted];
    [saveBtn setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    
    saveBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH*2/3, btnHeight);
    saveBtn.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2+30);
    [saveBtn setTitle:LOCALIZE(@"OK") forState:UIControlStateNormal];
    [dialogBoxView addSubview:saveBtn];
    
    
    [deleteBtn bk_addEventHandler:^(id sender) {
        
        [UIView animateWithDuration:animationDuration animations:^{
            
            //            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBoxView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBoxView removeFromSuperview];
            
        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    [saveBtn bk_addEventHandler:^(id sender) {
        [UIView animateWithDuration:animationDuration animations:^{
            //  dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBoxView.alpha = 0.0f;
            
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBoxView removeFromSuperview];
            
        });
        
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT/2-boxHeight/2, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    [UIView animateWithDuration:animationDuration animations:^{
        dialogBoxView.alpha = 1.0f;
    }];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    //執行取消發送電子郵件畫面的動畫

    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)giftbuyViewPopOver
{
    [self didClickGift:_giftButton];
    [_animateScene removeAllActions];
}

- (void)giftAnimatePreviewStop
{
    [_animateScene removeAllActions];

}

- (void)didClickGift:(id)sender
{
    if ([LoginHandler isGuest]) {
        [LoginHandler handleUserLogin];
        return;
    }
    [EVENT_HANDLER addEventTracking:@"ClickSendGiftButton" withDict:@{@"hasPoint":GET_DEFAULT(MY_POINT)}];
//    GiftPurchesInLivestreamViewController*
    //check gift module state from server
    if (_liveStream.canSendGift != 1) {
        [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"livestreamerCantGet") message:@"" buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
            if (okClicked) {
                
            }
        }];
        return;
    }
    
    if([GET_DEFAULT(GIFTMODULESTATE) isEqual:@2]){
        [DIALOG_MANAGER hideLoadingView];
        [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"pleaseupdateNewApp") message:@"" buttonText:LOCALIZE(@"goToappstore") cancelable:NO withCompletion:^(BOOL okClicked) {
            if (okClicked) {
                
            }
        }];
        return;
    }
    else if ([GET_DEFAULT(GIFTMODULESTATE) isEqual:@0]) {
        [DIALOG_MANAGER hideLoadingView];
        [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"giftmoduleClose") message:@"" buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
            if (okClicked) {
              
            }
        }];
        return;
    }
    
    
    if ([_giftArray count] == 0) {

        [API_MANAGER getGiftListWithCompletion:^(BOOL success, NSArray *gifts) {
            if (success) {
                if ([gifts count]==0) {
                    return ;
                }
//                DLog(@"拉禮物LIST");
                _giftArray = [NSArray arrayWithArray:gifts];
                
                _purchaseGiftViewController.delegate = self;
                _purchaseGiftViewController.modalPresentationStyle = UIModalPresentationPopover;

                int row;
                if ([_giftArray count] > 4 * 4) {
                    row = 4;
//                    DLog(@"禮物個數=%lu",[_giftArray count]);
                }
                else {
                    row = (int)[_giftArray count]/4+1;
//                    DLog(@"禮物個數=%lu",[_giftArray count]);
                }
                
                _purchaseGiftViewController.giftArray = [NSArray arrayWithArray:_giftArray];
                
                _purchaseGiftViewController.preferredContentSize = CGSizeMake(SCREEN_WIDTH/9*8, (0.1733*SCREEN_WIDTH+50)*row+35);
                
                // Get the popover presentation controller and configure it.
                UIPopoverPresentationController *presentationController = _purchaseGiftViewController.popoverPresentationController;
                presentationController.delegate = self;
                presentationController.permittedArrowDirections = UIPopoverArrowDirectionUp | UIPopoverArrowDirectionDown;
                presentationController.backgroundColor = [UIColor colorWithRed:10.0/255.0 green:10.0/255.0 blue:10.0/255.0 alpha:0.7];
                presentationController.sourceView = sender;
                presentationController.sourceRect = CGRectMake(-5, 0, 50, 50);
                
                if (self.presentedViewController == nil) {
                    [self presentViewController:_purchaseGiftViewController animated: YES completion: nil];
                }
                
            }
            else {
                DLog(@"failfailfail");
            }
        }];
        
    }
    else {
        [API_MANAGER getGiftListWithCompletion:^(BOOL success, NSArray *gifts) {
            _giftArray = [NSArray arrayWithArray:gifts];
        }];

        _purchaseGiftViewController.delegate = self;
        _purchaseGiftViewController.modalPresentationStyle = UIModalPresentationPopover;

        int row;
        if ([_giftArray count] > 4 * 4) {
            row = 4;
            DLog(@"禮物個數=%lu",[_giftArray count]);
        }
        else {
            row = (int)[_giftArray count] / 4 + 1;
            DLog(@"禮物個數=%lu",[_giftArray count]);
        }


        _purchaseGiftViewController.giftArray = [_giftArray copy];

        _purchaseGiftViewController.preferredContentSize = CGSizeMake(SCREEN_WIDTH/9*8, (0.1733*SCREEN_WIDTH+50)*row+35);
     
        // Get the popover presentation controller and configure it.
        UIPopoverPresentationController *presentationController = _purchaseGiftViewController.popoverPresentationController;
        presentationController.delegate = self;
        presentationController.permittedArrowDirections = UIPopoverArrowDirectionUp | UIPopoverArrowDirectionDown;
        presentationController.backgroundColor = [UIColor colorWithRed:10.0/255.0 green:10.0/255.0 blue:10.0/255.0 alpha:0.7];
        presentationController.sourceView = sender;
        presentationController.sourceRect = CGRectMake(-5, 0, 50, 50);
        
        if (self.presentedViewController == nil) {
            [self presentViewController:_purchaseGiftViewController animated: YES completion: nil];
        }

    }

}

- (void)buyAgift:(UserGiftObject *)usergift notEnough:(BOOL)notEnough
{
    if (notEnough) {
        _purchaseManager.remainingWarning=YES;
        [EVENT_HANDLER addEventTracking:@"EnterBuyPointInLive" withDict:nil];
        [_purchaseManager getProductList];
    }
    else {
        if (_liveStream.liveStreamID==nil) {
            return;
        }

        _textView.userInteractionEnabled=YES;
        
        [_gifttext setTextColor:WHITE_COLOR];
        [_gifttext setBackgroundColor:[UIColor clearColor]];
        [_gifttext setText:@"對直播主說的話"];
        _gifttext.delegate=self;
        _gifttext.keyboardType=UIKeyboardAppearanceDefault;
        _gifttext.returnKeyType = UIReturnKeySend;//返回键的类型
        _gifttext.font=SYSTEM_FONT_WITH_SIZE(18);
        [_textGiftHeadImage setImageWithURL:S3_THUMB_IMAGE_URL(GET_DEFAULT(PICTURE)) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
        
        [API_MANAGER sendGift:usergift.gift.giftID toLiveStream:_liveStream.liveStreamID withCompletion:^(BOOL success) {
            if (success) {
                
                _consumePoint=_consumePoint+usergift.gift.point;
                [EVENT_HANDLER addEventTracking:@"SendGiftLive" withDict:@{ @"targetUserID":_liveStream.userID,@"livestreamID":_liveStream.liveStreamID,@"giftName":usergift.gift.name, @"Point": INT_TO_STRING(usergift.gift.point),@"GiftID": usergift.gift.giftID}];
                
                [DEFAULTS setObject:[NSNumber numberWithInt:[GET_DEFAULT(MY_POINT) intValue]-usergift.gift.point] forKey:MY_POINT];
                [DEFAULTS synchronize];
                
                [_giftAnimateQuene addObject:usergift];
                
                if (usergift.gift.point>_mostExpensiveAnimatePrice) {
                    _mostExpensiveAnimatePrice=usergift.gift.point;
                    _nextAnimate=(int)[_giftAnimateQuene count]-1;
                }
                [self playAnimate:_nextAnimate];
            } else {
                [DIALOG_MANAGER showNetworkFailToast];
            }
        }];
    }
}

#pragma mark - Actions

- (void)onPanToFullScreen:(UIPanGestureRecognizer *)sender
{
    if (_liveStreamMode == LiveStreamModeBroadcast) {
        return;
    }
    
    CGPoint panPosition = [sender translationInView:self.view];
    
    CGFloat offset = panPosition.x - _startPanPoint.x;
    CGFloat offsetPercent = fabs(offset) / self.view.frame.size.width;
    
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
            self.startPanPoint = panPosition;
        }
            break;
            
        case UIGestureRecognizerStateChanged:
        {
            if (offset > 0) {
                // Pan right
                if (_fullScreenSwitchButton.selected) {
                    return;
                }
                
                [_fullScreenSwitchButton setBackgroundImage:[UIImage imageNamed:@"live_fullscreen_off"] forState:UIControlStateNormal];
                [_fullScreenSwitchButton setBackgroundImage:[UIImage imageNamed:@"live_fullscreen_off_down"] forState:UIControlStateHighlighted];
                
                CGFloat startX = (SCREEN_WIDTH-2*kLeftMargin)/4*3+kLeftMargin;
                CGFloat switchButtonCenterOffset = (SCREEN_WIDTH/12*11 - startX) * offsetPercent;
                
                [self forceSwitchFullScreen:(startX+switchButtonCenterOffset) alpha:(1-offsetPercent)];
            }
            else {
                // Pan left
                if (!_fullScreenSwitchButton.selected) {
                    return;
                }
                NSLog(@"%f", offsetPercent);
                [_fullScreenSwitchButton setBackgroundImage:[UIImage imageNamed:@"live_fullscreen"] forState:UIControlStateNormal];
                [_fullScreenSwitchButton setBackgroundImage:[UIImage imageNamed:@"live_fullscreen_down"] forState:UIControlStateHighlighted];
                
                CGFloat startX = SCREEN_WIDTH / 12 * 11;
                CGFloat switchButtonCenterOffset = (startX - ((SCREEN_WIDTH - 2 * kLeftMargin) / 4 * 3 + kLeftMargin)) * offsetPercent;
                
                [self forceSwitchFullScreen:(startX-switchButtonCenterOffset) alpha:offsetPercent];
            }
        }
            break;
            
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        {
            if (offset > 0) {
                if (_fullScreenSwitchButton.selected) {
                    return;
                }
                [self enableFullScreen:YES];
            }
            else {
                if (!_fullScreenSwitchButton.selected) {
                    return;
                }
                [self enableFullScreen:NO];
            }
        }
            break;
        default:
            break;
    }
}

- (void)onPanToLeave:(UIPanGestureRecognizer *)sender
{
    if (_liveStreamMode == LiveStreamModeBroadcast) {
        return;
    }
    
    CGPoint panPosition = [sender translationInView:sender.view.superview];

    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
            if (_delegateFlags.beginInteractiveTransitioning) {
                [_delegate liveStreamViewControllerBeginInteractiveTransitioning:self];
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }
            break;
            
        case UIGestureRecognizerStateChanged:
        {
            const CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height - 50.f;
            const CGFloat DragAmount = screenHeight;
            const CGFloat Threshold = 0.3f;
            CGFloat percent = panPosition.y / DragAmount;
            
            percent = fmaxf(percent, 0.f);
            percent = fminf(percent, 1.f);
            if (_delegateFlags.updateInteractiveTransitionShouldComplete) {
                [_delegate liveStreamViewController:self updateInteractiveTransition:percent shouldComplete:percent > Threshold];
            }
        }
            break;
            
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        {
            if (sender.state == UIGestureRecognizerStateCancelled
                || (_delegateFlags.shouldCompleteInteractiveTransition
                    && ![_delegate liveStreamViewControllerShouldCompleteInteractiveTransition:self])) {
                    
                    self.shouldLoadStream = NO;

                    if (_delegateFlags.completeInteractiveTransition) {
                        [_delegate liveStreamViewController:self completeInteractiveTransition:NO];
                    }
                }
            else {
                [_cameraSwitchButton removeFromSuperview];
                [self exitLiveStream];
                
                if (_delegateFlags.completeInteractiveTransition) {
                    [_delegate liveStreamViewController:self completeInteractiveTransition:YES];
                }
            }
        }
            break;
        default:
            break;
    }
}

-(void)playAnimateFromNotifcation:(NSNotification *)notification
{
    [self playAnimate:_nextAnimate];
}

- (void)playAnimate:(int)animateIndex
{
    [_animateScene removeAllActions];
    if ([_giftAnimateQuene count] == 0 || _animating) {
        return;
    }
    UserGiftObject* gift = [_giftAnimateQuene objectAtIndex:animateIndex];

    self.ari++;
    [_skView presentScene:_scene];
    
    
    DLog(@"%@",[DEFAULTS arrayForKey:GIFT_ZIP]);
    
    if (![[DEFAULTS arrayForKey:GIFT_ZIP] containsObject:gift.gift.archiveFileName])
    {
                    dispatch_async(GLOBAL_QUEUE, ^{
                        [GiftObject getGiftZipFile:gift.gift];
                    });
    
              _mostExpensiveAnimatePrice = 0;
        return;
    }
    self.animating = YES;

    if (gift.gift.point >= 499) {
        [_chatMessageLabel setText:gift.gift.name];
        [_nameLabel setText:gift.user.openID];
        [_nameLabel setFont:SYSTEM_FONT_WITH_SIZE(18)];
        [_chatMessageLabel setFont:SYSTEM_FONT_WITH_SIZE(20)];
        [_chatMessageLabel sizeToFit];
        [_nameLabel sizeToFit];
//        [_mask setImage:[UIImage imageNamed:@"head_mask"]];
        LiveStreamChatMessage *msg = [[LiveStreamChatMessage alloc]init];
        
        [_nameLabel setTextColor:[msg getMessageColor]];
        CGRect rect = CGRectMake(6, 10, 40, 40);
        [_giftHeadImage setFrame:rect];
        _giftHeadImage.layer.masksToBounds = YES;
        _giftHeadImage.layer.cornerRadius =40/2.0;
        _giftHeadImage.hidden = NO;
        _giftImage.hidden = YES;
        if ([gift.user.picture isEqualToString:@""]) {
            [_giftHeadImage setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
        }
        else {
            [_giftHeadImage setImageWithURL:S3_THUMB_IMAGE_URL(gift.user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
        }
        [_nameLabel setFrame:CGRectMake(_giftImage.frame.origin.x+43, 5,_nameLabel.frame.size.width, _nameLabel.frame.size.height)];
        _chatMessageLabel.frame=CGRectMake(_nameLabel.frame.origin.x, _nameLabel.frame.origin.y+_nameLabel.frame.size.height+2, _chatMessageLabel.frame.size.width, _chatMessageLabel.frame.size.height);
        if (_nameLabel.frame.size.width>_chatMessageLabel.frame.size.width) {
            _bgImageView.frame=CGRectMake(5, _lvTable.frame.origin.y-65, _chatMessageLabel.frame.origin.x+_nameLabel.frame.size.width+10, 60);
        }
        else {
            _bgImageView.frame=CGRectMake(5, _lvTable.frame.origin.y-65, _chatMessageLabel.frame.origin.x+_chatMessageLabel.frame.size.width+10, 60);
        }
    }
    else {
        self.chatMessageLabel.font = SYSTEM_FONT_WITH_SIZE(16);
        self.nameLabel.font = BOLD_FONT_WITH_SIZE(16);
        [_giftImage setFrame:CGRectMake(5, 7, 13, 15)];
        _giftHeadImage.hidden = YES;
        _giftImage.hidden = NO;
        [_giftImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"giftpoint_%d",rand()%7+1]]];
        [_chatMessageLabel setText:gift.gift.name];
        [_nameLabel setText:gift.user.openID];
        [_chatMessageLabel sizeToFit];

        [_nameLabel sizeToFit];
        [_nameLabel setFrame:CGRectMake(_giftImage.frame.origin.x+17, 5, _nameLabel.frame.size.width, _nameLabel.frame.size.height)];
        _chatMessageLabel.frame = CGRectMake(_nameLabel.frame.origin.x+_nameLabel.frame.size.width+5, _nameLabel.frame.origin.y, _chatMessageLabel.frame.size.width, _chatMessageLabel.frame.size.height);
        _bgImageView.frame = CGRectMake(5, _lvTable.frame.origin.y-35, _chatMessageLabel.frame.origin.x+_chatMessageLabel.frame.size.width+10, 30);
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        _bgImageView.alpha = 1;
    }];
    
    int32_t giftToken = gift.gift.giftToken;
    [_giftAnimateQuene removeObject:gift];
    [_giftAnimate removeAllObjects];

    for (int i = 1; i <= gift.gift.numOfImages; i++) {
        NSString* rootpath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)objectAtIndex:0];
        NSRange dirNameRange = NSMakeRange(0, [gift.gift.archiveFileName length]-4); // remove ".tar"
        NSString *path = [NSString stringWithFormat:@"%@/animatePng/%@/%@_%d.png", rootpath, [gift.gift.archiveFileName substringWithRange:dirNameRange], gift.gift.giftID, i];
        
        if ([[NSFileManager defaultManager] isReadableFileAtPath:path]) {
            [_giftAnimate addObject:path];
//            DLog (@"path exist :%@",path);
        } else {
            DLog(@"path not exist :%@",path);
            break;
        }
//        DLog(@"====%d",gift.gift.frameDuration);
    }


    if (FILE_EXIST(gift.gift.soundTrack) && ![gift.gift.soundTrack isEqualToString:@""]) {
        NSString *soundFilePath = GET_LOCAL_FILE_PATH(gift.gift.soundTrack);
        
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
        
        
        [_lvConnWatcher playAnimationSound:fileURL];
    }
    [_scene showAnimate:CGPointMake(SCREEN_WIDTH, SCREEN_HEIGHT)
               imgarray:_giftAnimate
        perFramDuraiton:(float)gift.gift.frameDuration/1000
             startPoint:0 withCompletion:^(BOOL Done) {
             if (Done) {
                 NSLog(@"%@livestreamviewController~~~animateFinish",gift.gift.name);
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((gift.gift.frameDuration*(24)) * NSEC_PER_MSEC)), dispatch_get_main_queue(), ^{
                     [UIView animateWithDuration:0.3 animations:^{
                         _bgImageView.alpha = 0;
                         _animating = NO;
                         int maxExpensive = 0;

                         for (int index = 0; index < [_giftAnimateQuene count]; index++) {
                             UserGiftObject* a = [_giftAnimateQuene objectAtIndex:index];
                             if (a.gift.point > maxExpensive) {
                                 maxExpensive = a.gift.point;
                                 self.nextAnimate = index;
                             }
                         }
                         [self playAnimate:_nextAnimate];

                     }];
                     
                 });

                 

                 if (_liveStreamMode == LiveStreamModeBroadcast) {
                     [API_MANAGER readGift:giftToken toLiveStream:_liveStream.liveStreamID withCompletion:^(BOOL success) {
                         if (success) {
                             
                         }
                     }];
                 }
             }
             
             DLog(@"%d",_ari);
             
         }];
}

- (void)showAnimate:(GiftObject *)gift dismiss:(BOOL)dismiss
{
    self.animating = YES;
    if (dismiss) {
        [_purchaseGiftViewController dismiss];
    }
    [_giftPreviewAnimateQuene addObject:gift];
    
    if (_previewAnimating) {
        return;
    }
    self.previewAnimating = YES;
    
    [_skView presentScene:_animateScene];
    NSMutableArray* giftanimate = [[NSMutableArray alloc]init];
    
    for (int i = 1; i <= gift.numOfImages; i++) {
        NSString* rootpath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)objectAtIndex:0];
        NSRange dirNameRange = NSMakeRange(0, [gift.archiveFileName length]-4); // remove ".tar"
        NSString *path = [NSString stringWithFormat:@"%@/animatePng/%@/%@_%d", rootpath, [gift.archiveFileName substringWithRange:dirNameRange], gift.giftID, i];
        
        if ([[NSFileManager defaultManager] isReadableFileAtPath:path]) {
            [giftanimate addObject:path];
            DLog(@"path exist :%@",path);
        }
        else {
            DLog(@"path not exist :%@",path);
            break;
        }
        //        DLog(@"====%d",gift.gift.frameDuration);
    }
    
    [_animateScene showAnimate:CGPointMake(SCREEN_WIDTH, SCREEN_HEIGHT)
                      imgarray:giftanimate
               perFramDuraiton:(float)gift.frameDuration/1000 startPoint:0
                withCompletion:^(BOOL Done) {

                    self.animating = NO;
                    self.previewAnimating = NO;
                    
                    [_skView presentScene:_scene];
                    //        [self didClickGift:_giftButton];
                    
                    [UIView animateWithDuration:0.3 animations:^{
                        _bgImageView.alpha = 0;
                    }];
                    [_giftPreviewAnimateQuene removeObject:gift];
                    if ([_giftPreviewAnimateQuene count] > 0) {
                        GiftObject* gift = [_giftPreviewAnimateQuene objectAtIndex:0];
                        [self showAnimate:gift dismiss:NO];
                    }
                    int maxExpensive = 0;
                    
                    for (int index=0; index<[_giftAnimateQuene count]; index++) {
                        UserGiftObject* a = [_giftAnimateQuene objectAtIndex:index];
                        if (a.gift.point > maxExpensive) {
                            maxExpensive = a.gift.point;
                            _nextAnimate = index;
                        }
                    }
                    
                    if ([_giftAnimateQuene count] > 0) {
                        [self playAnimate:_nextAnimate];
                    }
                }];

}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)panGestureRecognizer {
    CGPoint velocity = [panGestureRecognizer velocityInView:self.view];
    
    if (panGestureRecognizer == _panToFullScreenGR) {
        return fabs(velocity.y) < fabs(velocity.x);
    }
    else if (panGestureRecognizer == _panToLeaveGR) {
        return fabs(velocity.y) > fabs(velocity.x);
        
    }
    return YES;
}

#pragma mark - AutoHintViewDelegate

- (void)AutoHintView:autoHintView didTapViewWithType:(AutoHintType)autoHintType
{
    switch (autoHintType) {
        case AutoHintTypeFollow:
        {
            if ([LoginHandler isGuest]) {
                [LoginHandler handleUserLogin];
                return;
            }

            [self followUser:_liveStream.user];
        }
            break;
            
        case AutoHintTypeShare:
        {
            [self restream];
        }
            break;
    }
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    //    textView.text=@"";
    _gifttext.attributedText = nil;
    _gifttext.textAlignment = NSTextAlignmentLeft;
    _gifttext.textColor = WHITE_COLOR;
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text; {
    
    // Any new character added is passed in as the "text" parameter.
    if ([text isEqualToString:@"\n"]) {
        
        // If the Done button was pressed, resign the keyboard.
        [textView resignFirstResponder];
        // Return FALSE so that the final '\n' character doesn't get added.
        return NO;
    }
    if (range.length == 1) {
        [_textRemainWarning setText:[NSString stringWithFormat:@"%lu/70",(unsigned long)_gifttext.text.length-1]];
    }
    else {
        if (_gifttext.text.length + 1 > 70) {
            return NO;
        }
        
        [_textRemainWarning setText:[NSString stringWithFormat:@"%lu/70",(unsigned long)_gifttext.text.length+1]];
    }

    // For any other character return TRUE so that the text gets added to the view.
    return YES;
}

#pragma mark - LiveStreamHeaderViewControllerDelegate

- (void)liveStreamHeaderViewController:(LiveStreamHeaderViewController *)liveStreamHeaderViewController didClickUser:(UserObject *)user
{
    [self resignMessageInputBar];
    [DIALOG_MANAGER showActionSheetUserDialog:user
                                  isPublisher:(_liveStreamMode == LiveStreamModeBroadcast)
                               isLivestreamer:([user.userID isEqualToString:_liveStream.userID])
                               withCompletion:^(UserDialogAction selectedAction) {
                                   if ([LoginHandler isGuest]) {
                                       [LoginHandler handleUserLogin];
                                       return;
                                   }
                                   
                                   switch (selectedAction) {
                                       case UserDialogActionFollow:
                                       {
                                           [self followUser:user];
                                       }
                                           break;
                                       case UserDialogActionResponse:
                                       {
                                           [self responseUser:user];
                                       }
                                           break;
                                       case UserDialogActionUnfollow:
                                       {
                                           [self unfollowUser:user];
                                       }
                                           break;
                                       case UserDialogActionReport:
                                       {
                                           [self reportUserAction:user];
                                       }
                                           break;
                                       case UserDialogActionBlock:
                                       {
                                           [self blockUserAction:user];
                                       }
                                           break;
                                       case UserDialogActionUnblock:
                                       {
                                           [self unblockUserAction:user];
                                       }
                                           break;
                                       case UserDialogActionCancelFollowRequest:
                                       {
                                           [self cancelFollowUserRequest:user];
                                       }
                                           break;
                                   }
                               }];

}

- (void)liveStreamHeaderViewControllerDidClickLeaderboardButton:(LiveStreamHeaderViewController *)liveStreamHeaderViewController
{
    if ([LoginHandler isGuest]) {
        [LoginHandler handleUserLogin];
        return;
    }
    
    ReceivedGiftViewController *vc = [[ReceivedGiftViewController alloc] init];
    vc.liveStreamObj = _liveStream;
    
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:vc animated: YES completion: nil];
}

- (void)liveStreamHeaderViewControllerDidClickReportButton:(LiveStreamHeaderViewController *)liveStreamHeaderViewController
{
    if ([LoginHandler isGuest]) {
        [LoginHandler handleUserLogin];
        return;
    }
    
    [self resignMessageInputBar];
    
    NSArray* reasonArray = @[LOCALIZE(@"i_dont_like_live"), LOCALIZE(@"garbage_message_live"), LOCALIZE(@"bad_to_others_live"), LOCALIZE(@"not_good_for_live")];
    
    [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"report_reason") options:reasonArray destructiveIndexes:@[] cancelable:YES withCompletion:^(int selectedOption) {
        
        [API_MANAGER reportLiveAction:_liveStream withReason:reasonArray[selectedOption] withCompletion:^(BOOL success, NSString *message) {
            if(success){
                [DIALOG_MANAGER showCompleteToast];
            }
        }];
    }];

}

- (void)liveStreamHeaderViewControllerDidClickSubscribeButton:(LiveStreamHeaderViewController *)liveStreamHeaderViewController
{
    if ([LoginHandler isGuest]) {
        [LoginHandler handleUserLogin];
        return;
    }
    
    // @Todo
}

#pragma mark - UIPopoverPresentationControllerDelegate

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller traitCollection:(UITraitCollection *)traitCollection{
    return UIModalPresentationNone;
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
    return YES;
}

#pragma mark - Override

- (void)setDelegate:(id<LiveStreamViewControllerDelegate>)delegate
{
    _delegate = delegate;
    
    _delegateFlags.beginInteractiveTransitioning = [delegate respondsToSelector:@selector(liveStreamViewControllerBeginInteractiveTransitioning:)];
    _delegateFlags.updateInteractiveTransitionShouldComplete = [delegate respondsToSelector:@selector(liveStreamViewController:updateInteractiveTransition:shouldComplete:)];
    _delegateFlags.shouldCompleteInteractiveTransition = [delegate respondsToSelector:@selector(liveStreamViewControllerShouldCompleteInteractiveTransition:)];
    _delegateFlags.completeInteractiveTransition = [delegate respondsToSelector:@selector(liveStreamViewController:completeInteractiveTransition:)];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];

}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [SINGLETON customizeInterface];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        UIImage* imageTaken = [info valueForKey: UIImagePickerControllerEditedImage];
        
        if(picker.sourceType!=UIImagePickerControllerSourceTypePhotoLibrary){
            [SINGLETON saveImageWithWhiteBackgroundWithoutCompleteMessage:imageTaken];
        };
        
        [DIALOG_MANAGER showLoadingView];
        
        NSString* profilePictureFileName = [SINGLETON generateRandomFileNameWithExtension:@"jpg"];
        [SINGLETON saveImageWithThumbnailImage:imageTaken andFileName:profilePictureFileName];
        
        // Handle the result image here
        
        DLog(@"%@",profilePictureFileName);
        NSError *err = nil;
        NSData *data = [NSData dataWithContentsOfFile:GET_LOCAL_FILE_PATH(profilePictureFileName)
                                              options:NSDataReadingUncached
                                                error:&err];
        
        NSLog(@"File size is : %.2f MB",(float)data.length/1024.0f/1024.0f);
        
        self.multiFileUploader = [[MultiFileUploader alloc] initWithFileNameArray:@[profilePictureFileName, [NSString stringWithFormat:@"THUMBNAIL_%@", profilePictureFileName]]];
        
        [self.multiFileUploader startUploadWithCallback:^(NSArray *fileNameArray, int code , float progress) {
            
            if(code==MULTI_UPLOAD_SUCCESS) {
                // UPLOAD SUCCESS UPDATE USER INFO
                
                [API_MANAGER  updateUserInfo:@{@"picture":profilePictureFileName}  fetchSelfInfo:YES  completion:^(BOOL success) {
                    [DIALOG_MANAGER hideLoadingView];
                    if(success){
                        //                      [DIALOG_MANAGER SHOwACtion]
                    }
                    
                }];
                
            } else if(code==MULTI_UPLOAD_FAIL) {
                [DIALOG_MANAGER hideLoadingView];
                [DIALOG_MANAGER showNetworkFailToast];
            } else if(code==PROGRESSING){
                
            }
        }];
    }];
}
@end
