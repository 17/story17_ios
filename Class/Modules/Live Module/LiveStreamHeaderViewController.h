//
//  LiveStreamHeaderViewController.h
//  Story17
//
//  Created by Racing on 2015/12/22.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

extern CGFloat const kWatcherHeaderViewHeight;

@class UserObject;
@class LiveStreamHeaderViewController;
@protocol LiveStreamHeaderViewControllerDelegate <NSObject>
- (void)liveStreamHeaderViewController:(LiveStreamHeaderViewController *)liveStreamHeaderViewController didClickUser:(UserObject *)user;
- (void)liveStreamHeaderViewControllerDidClickReportButton:(LiveStreamHeaderViewController *)liveStreamHeaderViewController;
- (void)liveStreamHeaderViewControllerDidClickLeaderboardButton:(LiveStreamHeaderViewController *)liveStreamHeaderViewController;
- (void)liveStreamHeaderViewControllerDidClickSubscribeButton:(LiveStreamHeaderViewController *)liveStreamHeaderViewController;
@end

@class LiveStreamObject;
@interface LiveStreamHeaderViewController : UIViewController
@property (strong, nonatomic) LiveStreamObject *liveStreamObj;
@property (weak, nonatomic) id<LiveStreamHeaderViewControllerDelegate> delegate;

- (void)enableFullScreenMode:(BOOL)enabled;
- (void)forceSwitchFullScreenWithalpha:(CGFloat)alpha;
@end
