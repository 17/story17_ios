//
//  LiveStreamHeaderViewController.m
//  Story17
//
//  Created by Racing on 2015/12/22.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "LiveStreamHeaderViewController.h"
#import "ViewerCollectionViewCell.h"
#import "UserObject.h"
#import "LiveStreamObject.h"
#import "UIImage+AFNetworking.h"
#import "Constant.h"

static int const kQueryCount = 10;
static CGFloat const kStreamerCollectionViewTC = 38.0f;
static CGFloat const kWatcherCollectionViewTC = 58.0f;
static CGFloat const kStreamerHeaderViewHeight = 110.0f;
static NSTimeInterval const kFadeDuration = 0.2f;
static NSString * const kReuseCellIdentifier = @"ViewerCollectionViewCell";

CGFloat const kWatcherHeaderViewHeight = 130.0f;

@interface LiveStreamHeaderViewController ()<UICollectionViewDataSource, UICollectionViewDelegate>
{
    struct {
        unsigned int didClickUser : 1;
        unsigned int didClickReportButton : 1;
        unsigned int didClickLeaderboardButton : 1;
        unsigned int didClickSubscribeButton : 1;

    } _delegateFlags;
}
@property (strong, nonatomic) IBOutlet UIView *watcherHeaderView;
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *streamerNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *watcherCountLabel;
@property (strong, nonatomic) IBOutlet UIView *streamerHeaderView;
@property (strong, nonatomic) IBOutlet UILabel *streamerCountLabel;
@property (strong, nonatomic) IBOutlet UICollectionView *viewerCollectionView;
@property (strong, nonatomic) IBOutlet UIButton *leaderboardButton;
@property (strong, nonatomic) IBOutlet UIButton *subscribeButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *collectionViewTC;
@property (strong, nonatomic) NSMutableArray *viewers;
@property (assign, nonatomic) BOOL isFullScreen;

- (IBAction)onReport:(id)sender;
- (IBAction)onClickLeaderboard:(id)sender;
- (IBAction)onSubscribe:(UIButton *)sender;
- (IBAction)onClickStreamerProfile:(id)sender;
@end

@implementation LiveStreamHeaderViewController


#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _leaderboardButton.layer.borderWidth = 1.0;
    _leaderboardButton.layer.borderColor = [[[UIColor whiteColor] colorWithAlphaComponent:0.5] CGColor];
    [_leaderboardButton setTitle:LOCALIZE(@"Leaderboard") forState:UIControlStateNormal];
    _leaderboardButton.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    _leaderboardButton.layer.shadowOffset = CGSizeMake(0.3f, 0.3f);
    _leaderboardButton.layer.shadowOpacity = 1.0f;
    _leaderboardButton.layer.shadowRadius = 0.3f;

    [_subscribeButton setTitle:LOCALIZE(@"Subscribe") forState:UIControlStateNormal];
    [_subscribeButton setTitle:LOCALIZE(@"Subscribed") forState:UIControlStateSelected];
    
    [_viewerCollectionView registerNib:[UINib nibWithNibName:kReuseCellIdentifier bundle:nil] forCellWithReuseIdentifier:kReuseCellIdentifier];
    
    self.viewers = [NSMutableArray array];
    self.isFullScreen = NO;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    _watcherHeaderView.layer.cornerRadius = CGRectGetHeight(_watcherHeaderView.frame) / 2;
    _watcherHeaderView.clipsToBounds = YES;
    
    _streamerHeaderView.layer.cornerRadius = CGRectGetHeight(_streamerHeaderView.frame) / 2;
    _streamerHeaderView.clipsToBounds = YES;
    
    _profileImageView.layer.cornerRadius = CGRectGetWidth(_profileImageView.frame) / 2;
    _profileImageView.clipsToBounds = YES;
    
    _leaderboardButton.layer.cornerRadius = CGRectGetHeight(_leaderboardButton.frame) / 2;
    _leaderboardButton.clipsToBounds = YES;
    
    _subscribeButton.layer.cornerRadius = CGRectGetHeight(_subscribeButton.frame) / 2;
    _subscribeButton.clipsToBounds = YES;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [_viewers removeAllObjects];
    self.view.hidden = YES;
    _viewerCollectionView.hidden = YES;
    self.isFullScreen = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Private Method 

- (void)reloadHeader
{
    if ([_liveStreamObj.user.userID isEqualToString:MY_USER_ID]) {
        _streamerCountLabel.text = [NSString stringWithFormat:LOCALIZE(@"LIVE_STREAM_TEXT_USER_COUNT"), _liveStreamObj.viewerCount, _liveStreamObj.liveViewerCount];
    }
    else {
        [_profileImageView setImageWithURL:S3_THUMB_IMAGE_URL(_liveStreamObj.user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
        _streamerNameLabel.text = _liveStreamObj.user.openID;
        _watcherCountLabel.text = [NSString stringWithFormat:LOCALIZE(@"LIVE_STREAM_TEXT_USER_COUNT"), _liveStreamObj.viewerCount, _liveStreamObj.liveViewerCount];

    }
}

- (void)queryViewers
{
    [API_MANAGER getLiveStreamViewers:_liveStreamObj.liveStreamID
                            afterTime:0
                          viewerCount:kQueryCount
                       withCompletion:^(BOOL success, NSArray *users) {
                           if (success) {
                               [_viewers removeAllObjects];
                               [_viewers addObjectsFromArray:users];
                               [_viewerCollectionView reloadData];
                               
                               if (!_viewerCollectionView.hidden) {
                                   return;
                               }
                               
                               _viewerCollectionView.alpha = 0;
                               _viewerCollectionView.hidden = NO;
                               
                               [UIView animateWithDuration:kFadeDuration animations:^{
                                   _viewerCollectionView.alpha = 1;
                               }];
                           }
                       }];

}

#pragma mark - Public Method

- (void)enableFullScreenMode:(BOOL)enabled
{
    if (enabled) {
        [UIView animateWithDuration:kFadeDuration animations:^{
            self.view.alpha = 0;
        }];
    }
    else {
        [UIView animateWithDuration:kFadeDuration animations:^{
            self.view.alpha = 1;
        }];
    }
    
    self.isFullScreen = enabled;
}

- (void)forceSwitchFullScreenWithalpha:(CGFloat)alpha
{
    self.view.alpha = alpha;
}



#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ViewerCollectionViewCell *cell = (ViewerCollectionViewCell *)[_viewerCollectionView dequeueReusableCellWithReuseIdentifier:kReuseCellIdentifier forIndexPath:indexPath];
    
    UserObject *userObj = _viewers[indexPath.row];
    
    [cell.profileImageView setImageWithURL:S3_THUMB_IMAGE_URL(userObj.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _viewers.count;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UserObject *userObj = _viewers[indexPath.row];
    if (_delegateFlags.didClickUser) {
        [_delegate liveStreamHeaderViewController:self didClickUser:userObj];
    }
}

#pragma mark - Override

- (void)setDelegate:(id<LiveStreamHeaderViewControllerDelegate>)delegate
{
    _delegate = delegate;
    
    _delegateFlags.didClickLeaderboardButton = [delegate respondsToSelector:@selector(liveStreamHeaderViewControllerDidClickLeaderboardButton:)];
    _delegateFlags.didClickReportButton = [delegate respondsToSelector:@selector(liveStreamHeaderViewControllerDidClickReportButton:)];
    _delegateFlags.didClickSubscribeButton = [delegate respondsToSelector:@selector(liveStreamHeaderViewControllerDidClickSubscribeButton::)];
    _delegateFlags.didClickUser = [delegate respondsToSelector:@selector(liveStreamHeaderViewController:didClickUser:)];
}

- (void)setLiveStreamObj:(LiveStreamObject *)liveStreamObj
{
    _liveStreamObj = liveStreamObj;
    
    if ([liveStreamObj.user.userID isEqualToString:MY_USER_ID]) {
        _streamerHeaderView.hidden = NO;
        _watcherHeaderView.hidden = YES;
        _subscribeButton.hidden = YES;
        _collectionViewTC.constant = kStreamerCollectionViewTC;
        self.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, kStreamerHeaderViewHeight);
    }
    else {
        _streamerHeaderView.hidden = YES;
        _watcherHeaderView.hidden = NO;
        _subscribeButton.hidden = YES;
        _collectionViewTC.constant = kWatcherCollectionViewTC;
        self.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, kWatcherHeaderViewHeight);
    }

    [self reloadHeader];
    [self queryViewers];
    
    if (!self.view.hidden) {
        return;
    }
    
    self.view.alpha = 0;
    self.view.hidden = NO;
    [UIView animateWithDuration:kFadeDuration animations:^{
        self.view.alpha = 1;
    }];
}

#pragma mark - IBActions

- (IBAction)onReport:(id)sender {
    if (_delegateFlags.didClickReportButton) {
        [_delegate liveStreamHeaderViewControllerDidClickReportButton:self];
    }
}

- (IBAction)onClickLeaderboard:(id)sender {
    if (_delegateFlags.didClickLeaderboardButton) {
        [_delegate liveStreamHeaderViewControllerDidClickLeaderboardButton:self];
    }
}

- (IBAction)onSubscribe:(UIButton *)sender {
    if (sender.selected) {
        return;
    }
    
    if (_delegateFlags.didClickSubscribeButton) {
        [_delegate liveStreamHeaderViewControllerDidClickSubscribeButton:self];
    }
}

- (IBAction)onClickStreamerProfile:(id)sender {
    if (_delegateFlags.didClickUser) {
        [_delegate liveStreamHeaderViewController:self didClickUser:_liveStreamObj.user];
    }
}
@end
