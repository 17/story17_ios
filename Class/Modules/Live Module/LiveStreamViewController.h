#import <UIKit/UIKit.h>
@class MultiFileUploader;

typedef NS_ENUM(NSUInteger, LiveStreamChatMode) {
    LiveStreamChatModeEveryone,
    LiveStreamChatModeFollowedUsers,
    LiveStreamChatModeMax
};

typedef NS_ENUM(NSUInteger, LiveStreamEnterMode) {
    LiveStreamEnterModeHotLive,
    LiveStreamEnterModeFollowed,
    LiveStreamEnterModeRestreamed,
    LiveStreamEnterModeWebPage,
    LivestreamEnterModeGuest,
    LivestreamEnterModeShared

};

typedef NS_ENUM(NSUInteger, LiveStreamMode) {
    LiveStreamModeBroadcast,
    LiveStreamModeWatch
};

@class LiveStreamViewController;
@protocol LiveStreamViewControllerDelegate <NSObject>
- (void)liveStreamViewControllerBeginInteractiveTransitioning:(LiveStreamViewController *)liveStreamViewController;
- (void)liveStreamViewController:(LiveStreamViewController *)liveStreamViewController updateInteractiveTransition:(CGFloat)percentComplete shouldComplete:(BOOL)shouldComplete;
- (BOOL)liveStreamViewControllerShouldCompleteInteractiveTransition:(LiveStreamViewController *)liveStreamViewController;
- (void)liveStreamViewController:(LiveStreamViewController *)liveStreamViewController completeInteractiveTransition:(BOOL)complete;

@end

@class LiveStreamObject;
@interface LiveStreamViewController : UIViewController <UIImagePickerControllerDelegate>
@property (nonatomic, strong) LiveStreamObject* liveStream;
@property (nonatomic, assign) LiveStreamMode liveStreamMode;
@property (nonatomic, assign) LiveStreamEnterMode livestreamEnterMode;
@property (nonatomic, assign) BOOL canPresentView;
@property (nonatomic, weak) id<LiveStreamViewControllerDelegate> delegate;
@property (nonatomic) int enterPageTimeStamp;
@property (nonatomic) int enterFullScreenCount;
@property (nonatomic) int commentCount;
@property (nonatomic) int likeCountForET;
@property (nonatomic) int consumePoint;
@property (nonatomic) int enterFullScreenTimeStamp;
@property (nonatomic,strong) NSString* enterModeForET;
@property (nonatomic,strong) MultiFileUploader* multiFileUploader;

@property BOOL liveStreamDidStart;
@property BOOL firstTimeFullScreen;
@end
