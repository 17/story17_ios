//
//  LiveViewScrollView.m
//  story17
//
//  Created by POPO Chen on 6/30/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "LiveViewScrollView.h"
#import "Constant.h"
#import "UIImageView+AFNetworking.h"

@interface LiveViewScrollView() {
    struct {
        unsigned int didClickUser : 1;
        unsigned int didClickBackground : 1;
        unsigned int didClickGiftLeaderboardButton : 1;
    } _delegateFlags;

}
@property (strong, nonatomic) UIButton *giftLeaderboardButton;

@end

@implementation LiveViewScrollView

#define imageWidth 40
#define topMargin 3
#define imageMargin 5
#define leftMargin 10
#define animationDuration 0.2

#define labelHeight (imageWidth-3*topMargin)/2

+(float)getHeaderViewHeight
{
    return imageWidth+3*topMargin+labelHeight;
}

-(id)init
{
    self = [super init];
    if(self){
        
        _viewerCount =  0;
        _liveViewerCount = 0;
        
        _viewBgView = [[UIImageView alloc]initWithFrame:CGRectMake(2*leftMargin+labelHeight, topMargin, SCREEN_WIDTH-2*leftMargin, labelHeight)];
        [_viewBgView setImage:[[UIImage imageNamed:@"black_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]];

        _viewerLabel = [ThemeManager getLivestreamlabel:CGRectMake(leftMargin, 2, _viewBgView.frame.size.width,  _viewBgView.frame.size.height)];
        _viewerScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, topMargin*2+_viewBgView.frame.size.height, SCREEN_WIDTH, imageWidth)];
        _viewerArray = CREATE_MUTABLE_ARRAY;
        _userImageViewDict = CREATE_MUTABLE_DICTIONARY;
        
        [_viewerLabel setTextColor:MAIN_COLOR];
        _viewerLabel.font = BOLD_FONT_WITH_SIZE(12);
        
        _userCountImageView = [UIImageView new];
        _userCountImageView.layer.masksToBounds = YES;
        _userCountImageView.layer.cornerRadius = imageWidth/2.0;
        [_userCountImageView setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.5]];
        
        _userCountLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, imageWidth, imageWidth)];
        [_userCountLabel setText:@"+0"];
        [_userCountLabel setTextColor:WHITE_COLOR];
        _userCountLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        _userCountLabel.center = CGPointMake(imageWidth/2, imageWidth/2);
        [_userCountImageView addSubview:_userCountLabel];
        _userCountLabel.textAlignment = NSTextAlignmentCenter;
        
        _giftLeaderboardButton = [[UIButton alloc] initWithFrame:CGRectMake(leftMargin, topMargin, labelHeight, labelHeight)];
        [_giftLeaderboardButton setImage:[UIImage imageNamed:@"giftpoint_white"] forState:UIControlStateNormal];
        _giftLeaderboardButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
        [_giftLeaderboardButton addTarget:self action:@selector(onClickGiftLeaderboardButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewBgView addSubview:_viewerLabel];
        [self addSubview:_viewBgView];
        [self addSubview:_viewerScrollView];
        [self addSubview:_giftLeaderboardButton];
        
        __weak LiveViewScrollView* weakSelf = self;
        UITapGestureRecognizer* tapRecog = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            if (_delegate && _delegateFlags.didClickBackground) {
                [weakSelf.delegate didClickBackground];
            }
        }];
        [self addGestureRecognizer:tapRecog];
        self.userInteractionEnabled = YES;
    }
    
    return self;
    
}

- (void) reloadLiveStreamUserCount:(int)viewerCount liveViewerCount:(int)liveViewerCount
{
    _viewerCount = viewerCount;
    _liveViewerCount = liveViewerCount;
    
    DLog(@"liveViewerCount:%d viewerCount:%d ",_liveViewerCount,_viewerCount);
    
    if( _liveViewerCount > _viewerCount){
        _viewerCount = _liveViewerCount;
    }
    
    [_viewerLabel setText:[NSString stringWithFormat:LOCALIZE(@"LIVE_STREAM_TEXT_USER_COUNT"),_viewerCount,_liveViewerCount]];
    [_viewerLabel sizeToFit];
    [_viewerLabel setTextColor:WHITE_COLOR];
    
    
    CGRect rect = _viewerLabel.frame;
    rect.size.width += 15;
    rect.size.height += 4;
    rect.origin.x = 2*leftMargin+labelHeight;
    _viewBgView.frame = rect;
    _giftLeaderboardButton.frame = CGRectMake(leftMargin, rect.origin.y, rect.size.height, rect.size.height);
    
    _viewerScrollView.frame = CGRectMake(0, topMargin+_viewBgView.frame.size.height+5, SCREEN_WIDTH, imageWidth);
    _viewerScrollView.layer.masksToBounds = NO;
    
}

-(void)reloadViewer:(LiveStreamObject*)livestream viewers:(NSArray*)viewers
{

//    [self reloadLiveStreamUserCount:_viewerCount liveViewerCount:(int)[viewers count]];
    
    /* Reload scrollView insert left to right */
    for(UIView* view in [_viewerScrollView subviews]){
        [view removeFromSuperview];
    }
    
//    UIColor* randomColor = [SINGLETON getRandomColor];
    
    for(int i=0;i<[viewers count];i++){
//        if(i%5==0)
//            randomColor = [SINGLETON getRandomColor];

        UserObject* user = viewers[i];
        
//        DLog(@"reloadViewer userID:%@",user.userID);
//        DLog(@"reloadViewer picture:%@",user.picture);

        UIImageView* imageView = [[UIImageView alloc]initWithFrame:CGRectMake((imageWidth+imageMargin)*(i)+imageMargin,0, imageWidth, imageWidth)];
        imageView.layer.masksToBounds = YES;
        imageView.layer.cornerRadius = imageWidth/2.0;
        [imageView setBackgroundColor:WHITE_COLOR];

//        [imageView setBackgroundColor:randomColor];
        if (!IS_IPHONE_4) {
            if(user.picture.length==0){
                [imageView setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
            }else{
                [imageView setImageWithURL:S3_THUMB_IMAGE_URL(user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
            }
            [_viewerScrollView addSubview:imageView];
            [_userImageViewDict setObject:imageView forKey:user.userID];

        }
       
        imageView.userInteractionEnabled = YES;
        [imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            if(_delegate && _delegateFlags.didClickUser) {
                [_delegate didClickUser:user];
            }
        }]];
    }
    
    if(livestream.liveViewerCount>[viewers count]){
        _userCountImageView.frame = CGRectMake((imageWidth+imageMargin)*([viewers count])+imageMargin,0, imageWidth, imageWidth);
        [_userCountLabel setText:[NSString stringWithFormat:@"+%lu",livestream.liveViewerCount-[viewers count]]];
        [_viewerScrollView addSubview:_userCountImageView];
    }
    
//    int renewOffset = (int)[viewers count]-(int)[_viewerArray count];
//    if(renewOffset>0){
//        /* Increase */
//        [_viewerScrollView setContentOffset:CGPointMake(renewOffset*(imageWidth+imageMargin), 0)];
//    }else{
//        /* Decrease */
//        [_viewerScrollView setContentOffset:CGPointMake(0, 0)];
//    }
    
    _viewerArray = [viewers mutableCopy];
    [_viewerScrollView setContentSize:CGSizeMake(([_viewerArray count]+1)*(imageWidth+imageMargin)+imageMargin, imageWidth)];
    
    
    // reset contentSize
    [UIView animateWithDuration:0.6 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [_viewerScrollView setContentOffset:CGPointMake(0, 0)];
    } completion:^(BOOL finished) {
        
    }];
    
}

-(void)reloadPositioning:(LiveStreamObject*)livestream viewers:(NSArray*)viewers
{
    [_userCountImageView removeFromSuperview];
    
//    DLog(@"viewers %lu",(unsigned long)[viewers count]);
//    [self reloadLiveStreamUserCount:_viewerCount liveViewerCount:(int)[viewers count]];
    
    /* 1) new_in -> alpha in   2) leaved -> alpha out  3)keep -> pos animation */
    NSMutableArray* oldUserID = [[_userImageViewDict allKeys] mutableCopy];
    
    for(int i=0;i<[viewers count];i++){
        
        UserObject* user = viewers[i];
        
//        DLog(@"userID:%@",user.userID);
        
        if([oldUserID containsObject:user.userID]){
            /* old user */

            [UIView animateWithDuration:animationDuration animations:^{
                ((UIImageView*)_userImageViewDict[user.userID]).frame = CGRectMake((imageWidth+imageMargin)*(i)+imageMargin,0, imageWidth, imageWidth);
                
            }];
            [oldUserID removeObject:user.userID];
            
        }else{
            
            /* new joined */
            if (!IS_IPHONE_4) {
                    UIImageView* imageView = [[UIImageView alloc]initWithFrame:CGRectMake((imageWidth+imageMargin)*(i)+imageMargin,0, imageWidth, imageWidth)];
                    imageView.layer.masksToBounds = YES;
                    imageView.layer.cornerRadius = imageWidth/2.0;
                    [imageView setBackgroundColor:WHITE_COLOR];
                    
                    if(user.picture.length==0){
                        [imageView setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
                    }else{
                        [imageView setImageWithURL:S3_THUMB_IMAGE_URL(user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
                    }
                    
                    [_viewerScrollView addSubview:imageView];
                    
                    [_userImageViewDict setObject:imageView forKey:user.userID];
                    
                    imageView.userInteractionEnabled = YES;
                    [imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
                        if(_delegate && _delegateFlags.didClickUser) {
                            [_delegate didClickUser:user];
                        }
                    }]];
                    
                    imageView.alpha = 0.0f;
                    [UIView animateWithDuration:animationDuration animations:^{
                        imageView.alpha = 1.0f;
                    }];
                }
                if(i==[viewers count]-1){
                    
                    if(livestream.liveViewerCount>[viewers count]){
                        [UIView animateWithDuration:animationDuration animations:^{
                            _userCountImageView.frame = CGRectMake((imageWidth+imageMargin)*([viewers count])+imageMargin,0, imageWidth, imageWidth);
                        }];
                        [_userCountLabel setText:[NSString stringWithFormat:@"+%lu",livestream.liveViewerCount-[viewers count]]];
                        [_viewerScrollView addSubview:_userCountImageView];
                    }
                }
                
            }

            }
            

    for(NSString* userID in oldUserID){
        /* Leaved users */
        UIImageView* imageView = _userImageViewDict[userID];
        [UIView animateWithDuration:animationDuration animations:^{
            imageView.alpha = 0;
        } completion:^(BOOL finished) {
            [imageView removeFromSuperview];
            [_userImageViewDict removeObjectForKey:userID];
        }];
    }

    _viewerArray = [viewers mutableCopy];
    [_viewerScrollView setContentSize:CGSizeMake(([_viewerArray count]+1)*(imageWidth+imageMargin)+2*imageMargin, imageWidth)];
    
//
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration+0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        _viewerArray = [viewers mutableCopy];
//        [_viewerScrollView setContentSize:CGSizeMake([_viewerArray count]*(imageWidth+imageMargin)+2*imageWidth, imageWidth)];
//
//    });
    
}

#pragma mark - Override

- (void)setDelegate:(id<LiveViewerDelegate>)delegate
{
    _delegate = delegate;
    _delegateFlags.didClickBackground = [_delegate respondsToSelector:@selector(didClickBackground)];
    _delegateFlags.didClickGiftLeaderboardButton = [_delegate respondsToSelector:@selector(didClickGiftLeaderboardButton)];
    _delegateFlags.didClickUser = [_delegate respondsToSelector:@selector(didClickUser:)];
}

#pragma mark - Actions

- (void)onClickGiftLeaderboardButton:(id)sender
{
    if (_delegate && _delegateFlags.didClickGiftLeaderboardButton) {
        [_delegate didClickGiftLeaderboardButton];
    }
}

@end
