//
//  LiveStreamTableView.h
//  Story17
//
//  Created by POPO on 6/29/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveStreamTableViewCell.h"

@class LiveStreamChatMessage;
@class LiveStreamTableView;

@protocol LiveStreamTableViewDataSource <NSObject>

- (NSInteger) numberOfRowsInLiveSteamTableView: (LiveStreamTableView*) lvTableView;
- (LiveStreamChatMessage *) liveStreamTableView:(LiveStreamTableView*) lvTableView dataForRow: (NSInteger) row;

@end

@protocol LiveStreamTableViewDelegate <NSObject>
- (void)liveStreamTableView:(LiveStreamTableView*)lvTableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath;
@end


@interface LiveStreamTableView : UITableView <UITableViewDataSource, UITableViewDelegate,LiveViewerDelegate>

@property (nonatomic, weak) id<LiveStreamTableViewDataSource> lvDataSource;
@property (nonatomic, weak) id<LiveStreamTableViewDelegate> lvDelegate;
@property (nonatomic, weak) id<LiveViewerDelegate> lvViewerDelegate;
@property (nonatomic) BOOL isOnBottom;
- (id)forwardingTargetForSelector:(SEL)aSelector;

@end
