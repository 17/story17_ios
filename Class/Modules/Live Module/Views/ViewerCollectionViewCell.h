//
//  ViewerCollectionViewCell.h
//  Story17
//
//  Created by Racing on 2015/12/22.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewerCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;

@end
