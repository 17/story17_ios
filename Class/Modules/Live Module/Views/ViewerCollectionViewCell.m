//
//  ViewerCollectionViewCell.h
//  Story17
//
//  Created by Racing on 2015/12/22.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "ViewerCollectionViewCell.h"

@implementation ViewerCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    
    _profileImageView.layer.cornerRadius = CGRectGetWidth(_profileImageView.frame) / 2;
    _profileImageView.clipsToBounds = YES;
}

@end
