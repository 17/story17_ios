//
//  LiveStreamTableView.m
//  Story17
//
//  Created by POPO on 6/29/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "LiveStreamTableView.h"

@interface LiveStreamTableView()

@property (nonatomic, weak) LiveStreamCommentObject* lvCommentObj;

@end

@implementation LiveStreamTableView


#pragma mark - init
- (void) initializator
{
    self.backgroundColor = [UIColor clearColor];
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.delegate = self;
    self.dataSource = self;
    _isOnBottom = YES;
}

- (id)init
{
    self = [super init];
    if (self) [self initializator];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:UITableViewStylePlain];
    if (self) [self initializator];
    return self;
}

#pragma mark - LiveStreamTableViewCellDelegate forwarding
- (id)forwardingTargetForSelector:(SEL)aSelector {
    if ([self.lvViewerDelegate respondsToSelector:aSelector]) {
        return self.lvViewerDelegate;
    }
    return [super forwardingTargetForSelector:aSelector];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_lvDataSource numberOfRowsInLiveSteamTableView:self];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellId = @"lvChatCell";
    
    LiveStreamTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil)
        cell = [[LiveStreamTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    
    cell.lvMsg = [_lvDataSource liveStreamTableView:self dataForRow:indexPath.row];
    cell.delegate = self;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_lvDelegate != nil)
        [_lvDelegate liveStreamTableView:self didSelectRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [LiveStreamTableViewCell getHeightForCell:[_lvDataSource liveStreamTableView:self dataForRow:indexPath.row]];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    DLog(@"%@ %@",[NSThread currentThread],[[NSRunLoop currentRunLoop] currentMode]);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    
    if ((int)endScrolling >= (int)scrollView.contentSize.height-30)
    {
        _isOnBottom = YES;
    }else{
        _isOnBottom = NO;
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if ((int)endScrolling >= (int)scrollView.contentSize.height-30)
    {
        _isOnBottom = YES;
    }else{
        _isOnBottom = NO;
    }
}

@end
