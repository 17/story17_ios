//
//  LiveStreamTableViewCell.m
//  Story17
//
//  Created by POPO on 6/29/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "LiveStreamTableViewCell.h"
#import "STTweetLabel.h"
#import "UserProfileViewController.h"
#import "BrowsePostsViewController.h"

@interface LiveStreamTableViewCell()
@property (nonatomic, strong) UILabel* nameLabel;
@property (nonatomic, strong) UIImageView* bgImageView;
@property (nonatomic, strong) STTweetLabel* chatMessageLabel;
@property (nonatomic, strong) NSMutableAttributedString* chatMessage;
@end


@implementation LiveStreamTableViewCell

#define messageFontSize 14
#define leftMargin 5
#define topMargin 5
#define cellTopMargin 2

@synthesize lvMsg;

+(float)getHeightForCell:(LiveStreamChatMessage*)msg
{
    NSString* string = [ msg.message string];
    
    UILabel* nameLabel = [UILabel new];
    nameLabel.font = BOLD_FONT_WITH_SIZE(messageFontSize);
    [nameLabel setText:msg.commentObj.user.openID];
    [nameLabel sizeToFit];
    nameLabel.frame = CGRectMake(2*leftMargin, topMargin, nameLabel.frame.size.width, nameLabel.frame.size.height);

    STTweetLabel* chatMessageLabel = [SINGLETON msgTextLabel];
    chatMessageLabel.numberOfLines = 0;
    chatMessageLabel.font = SYSTEM_FONT_WITH_SIZE(messageFontSize);
    
    [chatMessageLabel setText:string];

    CGSize size = VIEW_SIZE([chatMessageLabel suggestedFrameSizeToFitEntireStringConstrainedToWidth:(SCREEN_WIDTH-4*leftMargin)]);

    if(size.width + nameLabel.frame.size.width + 5*leftMargin >= SCREEN_WIDTH || size.height > nameLabel.frame.size.height+2*topMargin){
        return 2*cellTopMargin+3*topMargin +nameLabel.frame.size.height + size.height;
    }else{
        return 2*cellTopMargin+2*topMargin + size.height;
    }
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
  
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    if (self) {
        
        // Helpers
//        CGSize size = self.contentView.frame.size;
        // Initialize chatMessageLabel Label
        
        self.bgImageView = [[UIImageView alloc]initWithImage:[IMAGE_FROM_BUNDLE(@"btn_white1_circle") resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 32)]];
        self.bgImageView.userInteractionEnabled = YES;
        [self.contentView addSubview:self.bgImageView];
        
        self.nameLabel = [UILabel new];
        [self.nameLabel setTextColor:MAIN_COLOR];
        self.nameLabel.font = BOLD_FONT_WITH_SIZE(messageFontSize);
        self.nameLabel.userInteractionEnabled = YES;
        [self.nameLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            
            if(lvMsg==nil)
                return;
            
            [API_MANAGER getUserInfo:lvMsg.commentObj.user.openID completion:^(BOOL success, UserObject *userObject) {
                if(success){
                    [_delegate didClickUserwithcomment:userObject withMessageString:lvMsg.commentObj.message];
                }
            }];
        }]];
        
        self.chatMessageLabel = [STTweetLabel new];
        self.chatMessageLabel.numberOfLines = 0;
        [_chatMessageLabel setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                           NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(messageFontSize)}
                                 hotWord:STTweetHandle];
        [_chatMessageLabel setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                           NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(messageFontSize)}
                                 hotWord:STTweetHashtag];
        [_chatMessageLabel setAttributes:@{NSForegroundColorAttributeName: DARK_GRAY_COLOR,
                                           NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(messageFontSize)}];

        __weak typeof(self) weakSelf = self;
        [self.chatMessageLabel setDetectionBlock:^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range) {
            
            NSString* userOpenID = [string substringToIndex:1];
//            if([[string substringFromIndex:1] isEqualToString:GET_DEFAULT(USER_OPEN_ID)])
//                return;

            NSString* keyword = [string substringFromIndex:1];
            if([userOpenID isEqualToString:@"@"]){
                
                [API_MANAGER getUserInfo:keyword completion:^(BOOL success, UserObject *userObject) {
                    if(success){
                        [weakSelf.delegate didClickUserwithcomment:userObject withMessageString:weakSelf.lvMsg.commentObj.message];
                    }
                }];
            }
        }];
        
        // Add Main Label to Content View
        [self.bgImageView addSubview:self.chatMessageLabel];
        [self.bgImageView addSubview:self.nameLabel];
    }
    return self;
}

- (void) setLvMsg: (LiveStreamChatMessage*) msg
{
    lvMsg = msg;
    
    [_nameLabel setText:msg.commentObj.user.openID];
    [_nameLabel sizeToFit];
    _nameLabel.frame = CGRectMake(2*leftMargin, topMargin, _nameLabel.frame.size.width, _nameLabel.frame.size.height);
    
    NSString* string = [ msg.message string];
    [_chatMessageLabel setText:string];

    CGSize size = VIEW_SIZE([_chatMessageLabel suggestedFrameSizeToFitEntireStringConstrainedToWidth:(SCREEN_WIDTH-4*leftMargin)]);
     
    if(size.width + _nameLabel.frame.size.width + 5*leftMargin >= SCREEN_WIDTH || size.height > _nameLabel.frame.size.height+2*topMargin ){

        _chatMessageLabel.frame = CGRectMake(2*leftMargin+cellTopMargin, cellTopMargin + topMargin + _nameLabel.frame.size.height, size.width, size.height);
        float bgWidth = size.width > _nameLabel.frame.size.width? size.width:_nameLabel.frame.size.width;
        bgWidth += + 4*leftMargin;
        _bgImageView.frame = CGRectMake(leftMargin, cellTopMargin, bgWidth, _nameLabel.frame.size.height+3*topMargin+size.height);

    }else{

        _chatMessageLabel.frame = CGRectMake(_nameLabel.frame.origin.x+_nameLabel.frame.size.width+leftMargin, _nameLabel.frame.origin.y, size.width, size.height);
        _bgImageView.frame = CGRectMake(leftMargin, cellTopMargin, 4*leftMargin+_nameLabel.frame.size.width + size.width, 2*topMargin+_nameLabel.frame.size.height);
    }
    

    if(![string isEqualToString:@""] && string.length>=2){
        
        char beginChar = [string characterAtIndex:0];
        char endChar = [string characterAtIndex:[string length]-1];
        
        if(endChar=='*' && beginChar=='*'){
            [_nameLabel setTextColor:WHITE_COLOR];
            [self.bgImageView setImage:[IMAGE_FROM_BUNDLE(@"btn_green_circle") resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 32)]];
            [self.chatMessageLabel setAttributes:@{NSForegroundColorAttributeName:WHITE_COLOR, NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:messageFontSize]}];
            [_chatMessageLabel setText:[ string substringFrom:1 to:string.length-1]];
        
        }else if(msg.commentObj.isSystem == 0){
                [_nameLabel setTextColor:[lvMsg getMessageColor]];
                [self.bgImageView setImage:[IMAGE_FROM_BUNDLE(@"btn_white1_circle") resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 32)]];
                [self.chatMessageLabel setAttributes:@{NSForegroundColorAttributeName:DARK_GRAY_COLOR, NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:messageFontSize]}];
        }else if(msg.commentObj.isSystem == 1){
            [_nameLabel setTextColor:WHITE_COLOR];
            [self.bgImageView setImage:[IMAGE_FROM_BUNDLE(@"btn_red_c") resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 32)]];
            [self.chatMessageLabel setAttributes:@{NSForegroundColorAttributeName:WHITE_COLOR, NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:messageFontSize]}];
        }else if(msg.commentObj.isSystem == 2){
            [_nameLabel setTextColor:WHITE_COLOR];
            [self.bgImageView setImage:[IMAGE_FROM_BUNDLE(@"btn_orange_c") resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 32)]];
            [self.chatMessageLabel setAttributes:@{NSForegroundColorAttributeName:WHITE_COLOR, NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:messageFontSize]}];
        }else if(msg.commentObj.isSystem == 3){
            [_nameLabel setTextColor:WHITE_COLOR];
            [self.bgImageView setImage:[IMAGE_FROM_BUNDLE(@"btn_green_c") resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 32)]];
            [self.chatMessageLabel setAttributes:@{NSForegroundColorAttributeName:WHITE_COLOR, NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:messageFontSize]}];
        }else if(msg.commentObj.isSystem == 4){
            [_nameLabel setTextColor:WHITE_COLOR];
            [self.bgImageView setImage:[IMAGE_FROM_BUNDLE(@"btn_blue_c") resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 32)]];
            [self.chatMessageLabel setAttributes:@{NSForegroundColorAttributeName:WHITE_COLOR, NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:messageFontSize]}];
        }else if(msg.commentObj.isSystem == 5){
            [_nameLabel setTextColor:WHITE_COLOR];
            [self.bgImageView setImage:[IMAGE_FROM_BUNDLE(@"btn_green_circle") resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 32)]];
            [self.chatMessageLabel setAttributes:@{NSForegroundColorAttributeName:WHITE_COLOR, NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:messageFontSize]}];
        }
    }else{
        [_nameLabel setTextColor:[lvMsg getMessageColor]];
        [self.bgImageView setImage:[IMAGE_FROM_BUNDLE(@"btn_white1_circle") resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 32)]];
        [self.chatMessageLabel setAttributes:@{NSForegroundColorAttributeName:DARK_GRAY_COLOR, NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:messageFontSize]}];
    }
    

    
//    NSString* nameRegex = @"\^*.\*$";
//    NSPredicate* nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
//    if(![nameTest evaluateWithObject:string]){
//        
//        
//    }
//
    
}

@end
