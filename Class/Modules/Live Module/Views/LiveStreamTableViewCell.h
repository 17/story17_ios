//
//  LiveStreamTableViewCell.h
//  Story17
//
//  Created by POPO on 6/29/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveStreamChatMessage.h"
#import "LiveViewScrollView.h"

@interface LiveStreamTableViewCell : UITableViewCell

@property (nonatomic, weak) LiveStreamChatMessage* lvMsg;
@property (nonatomic, weak) id<LiveViewerDelegate> delegate;

+(float)getHeightForCell:(LiveStreamChatMessage*)msg;

@end