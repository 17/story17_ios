//
//  AutoHintView.h
//  Story17
//
//  Created by Racing on 2015/12/1.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, AutoHintType) {
    AutoHintTypeFollow,
    AutoHintTypeShare
};

@class AutoHintView;
@protocol AutoHintViewDelegate <NSObject>
- (void)AutoHintView:(AutoHintView *)autoHintView didTapViewWithType:(AutoHintType)autoHintType;

@end

@interface AutoHintView : UIView
@property (weak, nonatomic) id<AutoHintViewDelegate> delegate;
@property (assign, nonatomic) BOOL isFollowHintShown;
@property (assign, nonatomic) BOOL isFullScreen;

- (void)openAutoHint:(AutoHintType)autoHintType withName:(NSString *)name;

@end
