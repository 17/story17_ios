//
//  AutoHintView.m
//  Story17
//
//  Created by Racing on 2015/12/1.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "AutoHintView.h"
#import "Constant.h"

static NSTimeInterval const kAutoHintViewFadeDuration = 0.75f;
static NSTimeInterval const kAutoFollowHintWaitingDuration = 10.0f;
static NSString * const kAutoFollowHintBackgroundHex = @"#62bbdb";
static NSString * const kAutoShareHintBackgroundHex = @"#88c6bd";
//static CGFloat const kAutoHineBackgroundAlpha = 0.8f;

@interface AutoHintView() {
    struct {
        unsigned int didTapViewWithType : 1;
    } _delegateFlags;
}
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;
@property (assign, nonatomic) AutoHintType autoHintType;
@property (assign, nonatomic) NSUInteger currentIndex;

- (IBAction)onTapView:(id)sender;
@end

@implementation AutoHintView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.isFollowHintShown = NO;
    self.isFullScreen = NO;
    self.currentIndex = 0;
    
    self.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    self.layer.shadowOffset = CGSizeMake(0.3f, 0.3f);
    self.layer.shadowOpacity = 1.0f;
    self.layer.shadowRadius = 0.3f;
    
}

- (void)activateAutoHint
{
    [self fadeView:YES];
    
    self.currentIndex += 1;
    NSUInteger taskIndex = _currentIndex;
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kAutoFollowHintWaitingDuration * NSEC_PER_SEC));
    dispatch_after(time, dispatch_get_main_queue(), ^{
        if (taskIndex == _currentIndex) {
            [self fadeView:NO];
        }
    });
}

- (void)openAutoHint:(AutoHintType)autoHintType withName:(NSString *)name
{
    switch (autoHintType) {
        case AutoHintTypeFollow:
        {
            dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kAutoFollowHintWaitingDuration * NSEC_PER_SEC));
            dispatch_after(time, dispatch_get_main_queue(), ^{
                self.autoHintType = autoHintType;
                
                _iconImageView.image = [UIImage imageNamed:@"suggest_follow"];
                _messageLabel.text = [NSString stringWithFormat:LOCALIZE(@"Follow %@ to catch their next stream."), name];
                if (_isFollowHintShown) {
                    return;
                }
                self.isFollowHintShown = YES;
                
                [self activateAutoHint];
            });

        }
            break;
            
        case AutoHintTypeShare:
        {
            self.autoHintType = autoHintType;

            _iconImageView.image = [UIImage imageNamed:@"suggest_share"];
            _messageLabel.text = LOCALIZE(@"Share this stream to friends!");

            [self activateAutoHint];
        }
            break;
    }

}

- (void)fadeView:(BOOL)fadeIn
{
    if (_isFullScreen) {
        return;
    }
    
    if (fadeIn) {
        self.alpha = 0;
        self.hidden = NO;
        [UIView animateWithDuration:kAutoHintViewFadeDuration animations:^{
            self.alpha = 1;
        }];
    }
    else {
        [UIView animateWithDuration:kAutoHintViewFadeDuration animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            self.hidden = YES;
        }];

    }
}

#pragma mark - Override

- (void)setDelegate:(id<AutoHintViewDelegate>)delegate
{
    _delegate = delegate;
    _delegateFlags.didTapViewWithType = [delegate respondsToSelector:@selector(AutoHintView:didTapViewWithType:)];
}

#pragma mark - Actions
- (IBAction)onTapView:(id)sender {
    if (_delegateFlags.didTapViewWithType) {
        [_delegate AutoHintView:self didTapViewWithType:_autoHintType];
    }
    [self fadeView:NO];
}
@end
