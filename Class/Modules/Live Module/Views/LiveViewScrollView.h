//
//  LiveViewScrollView.h
//  story17
//
//  Created by POPO Chen on 6/30/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveStreamObject.h"

@protocol LiveViewerDelegate <NSObject>

@optional
-(void)didClickUser:(UserObject*)user;
-(void)didClickUserwithcomment:(UserObject*)user withMessageString:(NSString*)withMessageString;
-(void)didClickBackground;
-(void)didClickGiftLeaderboardButton;

@end

@interface LiveViewScrollView : UIView

@property (nonatomic,strong) UIScrollView* viewerScrollView;
@property (nonatomic,strong) UIImageView* viewBgView;
@property (nonatomic,strong) UILabel* viewerLabel;
@property (nonatomic,strong) NSArray* viewerArray;
@property (nonatomic,strong) UIImageView* userCountImageView;
@property (nonatomic,strong) UILabel* userCountLabel;

@property (nonatomic,strong) NSMutableDictionary* userImageViewDict;
@property (nonatomic,weak) id<LiveViewerDelegate> delegate;
@property int viewerCount;
@property int liveViewerCount;

- (void) addViewer:(NSArray*)userArray;
- (void) reloadLiveStreamUserCount:(int)viewerCount liveViewerCount:(int)liveViewerCount;
- (void) reloadViewer:(LiveStreamObject*)livestream viewers:(NSArray*)viewers;
- (void) reloadPositioning:(LiveStreamObject*)livestream viewers:(NSArray*)viewers;

+ (float) getHeaderViewHeight;

@end
