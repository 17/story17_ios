//
//  HUDView.m
//  Story17
//
//  Created by Racing on 2015/12/16.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "HUDView.h"
#import "ThemeManager.h"

static NSTimeInterval const kHUDFadeDuration = 0.35f;
static NSTimeInterval const kAnimationDuration = 1.0f;

@interface HUDView()
@property (strong, nonatomic) IBOutlet UIImageView *animationImageView;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;

@end

@implementation HUDView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.messageLabel.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    self.messageLabel.layer.shadowOffset = CGSizeMake(0.3f, 0.3f);
    self.messageLabel.layer.shadowOpacity = 1.0f;
    self.messageLabel.layer.shadowRadius = 0.3f;
    
    self.currentHUDType = HUDTypeNone;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (BOOL)openHUDWithType:(HUDType)HUDType
{
 
    if (_currentHUDType == HUDType) {
        return NO;
    }
    
    UIImageView *animationImageView;
    NSString *message;
    
    switch (HUDType) {
        case HUDTypeAudioOnly:
        {
            animationImageView = [ThemeManager audioOnlyImageView];
            message = LOCALIZE(@"Livestreamer is busy");
        }
            break;
        case HUDTypePoorConnection:
        {
            animationImageView = [ThemeManager busyImageView];
            message = LOCALIZE(@"Poor connection");
        }
            break;
        case HUDTypeFirstTimeLoading:
        {
            animationImageView = [ThemeManager loadingImageView];
            message = LOCALIZE(@"Loading stream");
        }
            break;
        case HUDTypeNone:
        {
            return NO;
        }
            break;
    }
    _animationImageView.animationImages = animationImageView.animationImages;
    _animationImageView.animationDuration = kAnimationDuration;
    [_animationImageView startAnimating];
    
    _messageLabel.text = message;
    [self fadeHUD:YES];
    
    self.currentHUDType = HUDType;
    
    return YES;
}

- (BOOL)closeHUDWithType:(HUDType)HUDType
{
    if (_currentHUDType == HUDType) {
        [self fadeHUD:NO];
        self.currentHUDType = HUDTypeNone;
        
        return YES;
    }
    
    return NO;
}

- (void)fadeHUD:(BOOL)fadeIn
{
    if (fadeIn) {
        [UIView animateWithDuration:kHUDFadeDuration animations:^{
            self.alpha = 1;
        }];
    }
    else {
        [UIView animateWithDuration:kHUDFadeDuration animations:^{
            self.alpha = 0;
        }];

    }
}

@end
