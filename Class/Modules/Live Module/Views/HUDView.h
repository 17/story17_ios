//
//  HUDView.h
//  Story17
//
//  Created by Racing on 2015/12/16.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, HUDType) {
    HUDTypeFirstTimeLoading,
    HUDTypeAudioOnly,
    HUDTypePoorConnection,
    HUDTypeNone
};

@interface HUDView : UIView
@property (assign, nonatomic) HUDType currentHUDType;

- (BOOL)openHUDWithType:(HUDType)HUDType;
- (BOOL)closeHUDWithType:(HUDType)HUDType;

@end
