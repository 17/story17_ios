//
//  BlabViewController.m
//  Story17
//
//  Created by POPO on 2015/9/4.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BlabViewController.h"
#import "UIImageView+AFNetworking.h"
#import "UIImage+Helper.h"

#define GROUP_MEDIA_CTRLLER
#ifdef GROUP_MEDIA_CTRLLER
#import "GroupMediaController.h"
#endif

@interface BlabViewController()<LiveViewerDelegate>

@property (nonatomic, strong) LiveStreamObject* livestream;
@property (nonatomic,strong) GroupChunkDownloader* chunkDownloader;
@property BOOL isFirstDownloadChunk;

@property (nonatomic,strong) PCMAUAudioPlayer* audioPlayer;
#ifdef GROUP_MEDIA_CTRLLER
@property (nonatomic, strong) GroupMediaController* groupMediaCtrller;
#endif

@property BOOL broadFirst;
@property BOOL watch1First;
@property BOOL watch2First;
@property BOOL watch3First;

/* UI control related */
@property BOOL isLiveStreamStart;
@property BOOL isLocationSharedOn;
//@property LIVE_STREAM_CHAT_MODE chatMode;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) NSString *hexColorCode;  /* Watcher hexcolor */

@property int32_t likeCount;

@property (nonatomic) int liveStreamStartTime;
@property (nonatomic) int liveStreamStopTime;

// network related
@property (nonatomic,strong) NSTimer* keepAliveTimer;

// control btns
@property (nonatomic, strong) UIButton* exitButton;

@property (nonatomic, strong) UIView* settingView;
@property (nonatomic, strong) UIButton* locationShareSwitchButton;
@property (nonatomic, strong) UILabel* locationShareReminderLabel;
@property (nonatomic, strong) UILabel* chatModeSwitchReminderLabel;
@property (nonatomic, strong) UIButton* privacyStreamSettingButton;

@property (nonatomic, strong) UIView* streamingView;
@property (nonatomic, strong) UIButton* hostImageButton;
@property (nonatomic, strong) UILabel* hostNameLabel;


@property (nonatomic, strong) UIButton* torchModeSwitchButton;
@property (nonatomic, strong) UIButton* cameraSwitchButton;

@property int keyboardHeight;
@property int requestingSeat;
// UI related
@property (nonatomic, strong) NSMutableArray* chatMessages;
@property (nonatomic, strong) NSMutableArray* receivedMessageID;
@property (nonatomic, strong) NSMutableArray* requesterArrary;
@property (nonatomic, strong) NSMutableArray* playerPicArrary;

@property (nonatomic) UIBackgroundTaskIdentifier backgroundTask;
@property (nonatomic, strong) UIImageView* blurBgImageView;
@property (nonatomic, strong) UIButton *liveshare;



/* SpriteKit kit animation */
@property (nonatomic,strong) MyScene* scene;
@property (nonatomic) BOOL privacypresent;

@property (nonatomic) int heartCount;


@end

@implementation BlabViewController

@synthesize cameraSwitchButton;
@synthesize torchModeSwitchButton;
@synthesize exitButton;
@synthesize lastCommentTime;
@synthesize liveshare;


#define left 0
#define right 1
#define LIVE_STREAM_VIEW_PADDING 8
#define LIVE_STREAM_BUTTON_WIDTH SCREEN_WIDTH*4/5    // start button width
#define LIVE_STREAM_BUTTON_HEIGHT 36
#define LIVE_STREAM_FONT_SIZE 18

#define fetchNum 10
#define fetchViewerNum 10
#define maxTableHeight 153

#define CHECKING_WINDOW_SIZE 100
#define LIVE_STREAM_MAX_CHAT_COMMENT_COUNT 100

#define PLANE_Y 0
#define PLANE_CBCR 1

#define USE_SPRITEKIT SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")

#define COMMENT_INTERVAL 8
#define MAX_COMMENT_COUNT 50

- (id)init
{
    self = [super init];
    
    if(self){
        
        _lastSelfCommentTime = CURRENT_TIMESTAMP-COMMENT_INTERVAL;
        
        /* ViewerScrollView  */
        _viewerIDArray = CREATE_MUTABLE_ARRAY;
        _viewerDict = CREATE_MUTABLE_DICTIONARY;
        _receivedMessageID = CREATE_MUTABLE_ARRAY;
        _requesterArrary = CREATE_MUTABLE_ARRAY;
        _playerPicArrary=CREATE_MUTABLE_ARRAY;
        
        /* Fetching data status */
        _isFirstDownloadChunk = YES;
        _noMoreWatcher = NO;
        _isFirstFetching = YES;
        _isFirstFetchInfo = YES;
        _isFetching = NO;
        _isFetchingInfo = NO;
        _heartCount=0;
        _isBackground = NO;
        _avaliableSeats=3;
        /* tableview height */
        _cellTotalHeight = 0.0f;
        self.backgroundTask = UIBackgroundTaskInvalid;
        _requestUser=[GroupUserObject new];
        _requestUser.userID=@"";
        _requestingSeat=0;
        _colorcode=[NSString stringWithFormat:@"#%d",rand()%1000000];
        
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
        [[AVAudioSession sharedInstance] setPreferredSampleRate:48000.0 error:NULL];
        [[AVAudioSession sharedInstance] setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
#ifdef GROUP_MEDIA_CTRLLER
        _groupMediaCtrller = [GroupMediaController new];
        _groupMediaCtrller.delegate = self;
#else
        [self startAudioPlayer];
#endif
    }
    return self;
}

-(void) dealloc
{
    _viewerScrollView.delegate = nil;
    _liveView1.delegate = nil;
    _liveView2.delegate=nil;
    _liveView3.delegate=nil;
    _liveView4.delegate=nil;
    
#ifdef GROUP_MEDIA_CTRLLER
    [_groupMediaCtrller stop];
#else
    [self stopAudioPlayer];
#endif
    DLog(@"dealloc");
}

- (void) viewWillAppear:(BOOL)animated{
    
    
    [DEFAULTS setObject:@1 forKey:IS_ON_LIVESTREAMING];
    [DEFAULTS synchronize];
    
    [super viewWillAppear:animated];
    

    _privacypresent=NO;
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];

    if (_liveStreamMode==GROUP_WATCHER) {
        [self wactherEnter];
    }
    [NOTIFICATION_CENTER addObserver:self selector:@selector(enterBackground:) name:UIApplicationWillResignActiveNotification object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(enterForeground:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(keyboardNotificationHandler:) name: UIKeyboardWillChangeFrameNotification object:nil];
    
    _POP=YES;
}


-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _tcpSocket = [[GroupTcpConnection alloc] init:_livestreamID];
    _tcpSocket.delegate = self;
    [_tcpSocket startTcpConn];


    /* Defaults to High Quality */
    [DEFAULTS setObject:INT_TO_NUMBER(LIVESTREAM_QUALITY_HIGH) forKey:LIVESTREAM_QUALITY];
    [DEFAULTS synchronize];
    _cover=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [_cover setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.5]];
    _cover.alpha=0;
    
    if (IS_IPHONE_4) {
        
        _liveView1 = [[LiveShowMediaView alloc]initWithChannelID:GroupLiveChannelPositionLeftTop liveShowMode:_liveStreamMode];
        _liveView1.frame = CGRectMake(0, 38, SCREEN_WIDTH/2, SCREEN_WIDTH/2);
        _liveView1.delegate = self;
        
        _liveView2 = [[LiveShowMediaView alloc]initWithChannelID:GroupLiveChannelPositionRightTop liveShowMode:_liveStreamMode];
        _liveView2.frame = CGRectMake(SCREEN_WIDTH/2, 38, SCREEN_WIDTH/2, SCREEN_WIDTH/2);
        _liveView2.delegate = self;
        
        _liveView3 = [[LiveShowMediaView alloc]initWithChannelID:GroupLiveChannelPositionLeftBottom liveShowMode:_liveStreamMode];
        _liveView3.frame = CGRectMake(0, 38+SCREEN_WIDTH/2, SCREEN_WIDTH/2, SCREEN_WIDTH/2);
        _liveView3.delegate = self;
        
        _liveView4 = [[LiveShowMediaView alloc]initWithChannelID:GroupLiveChannelPositionRightBottom liveShowMode:_liveStreamMode];
        _liveView4.frame = CGRectMake(SCREEN_WIDTH/2, 38+SCREEN_WIDTH/2, SCREEN_WIDTH/2, SCREEN_WIDTH/2);
        _liveView4.delegate = self;
        
    }else{
        
        _liveView1 = [[LiveShowMediaView alloc]initWithChannelID:GroupLiveChannelPositionLeftTop liveShowMode:_liveStreamMode];
        _liveView1.frame = CGRectMake(0, 80, SCREEN_WIDTH/2, SCREEN_WIDTH/2);
        _liveView1.delegate = self;
        
        _liveView2 = [[LiveShowMediaView alloc]initWithChannelID:GroupLiveChannelPositionRightTop liveShowMode:_liveStreamMode];
        _liveView2.frame = CGRectMake(SCREEN_WIDTH/2, 80, SCREEN_WIDTH/2, SCREEN_WIDTH/2);
        _liveView2.delegate = self;
        
        _liveView3 = [[LiveShowMediaView alloc]initWithChannelID:GroupLiveChannelPositionLeftBottom liveShowMode:_liveStreamMode];
        _liveView3.frame = CGRectMake(0, 80+SCREEN_WIDTH/2, SCREEN_WIDTH/2, SCREEN_WIDTH/2);
        _liveView3.delegate = self;
        
        _liveView4 = [[LiveShowMediaView alloc]initWithChannelID:GroupLiveChannelPositionRightBottom liveShowMode:_liveStreamMode];
        _liveView4.frame = CGRectMake(SCREEN_WIDTH/2, 80+SCREEN_WIDTH/2, SCREEN_WIDTH/2, SCREEN_WIDTH/2);
        _liveView4.delegate = self;
      
    }
    _liveView1.mediaPlayer.delegate=self;
    _liveView2.mediaPlayer.delegate=self;
    _liveView3.mediaPlayer.delegate=self;
    _liveView4.mediaPlayer.delegate=self;

    [self.view addSubview:_liveView1];
    [self.view addSubview:_liveView2];
    [self.view addSubview:_liveView3];
    [self.view addSubview:_liveView4];

    
    _liveView1.channelID=GroupLiveChannelPositionLeftTop;
    _liveView2.channelID=GroupLiveChannelPositionRightTop;
    _liveView3.channelID=GroupLiveChannelPositionLeftBottom;
    _liveView4.channelID=GroupLiveChannelPositionRightBottom;
    
    if (_liveStreamMode==LIVE_SHOW_MODE_HOLDER) {
        _isLiveStreamStart=YES;
        _broadFirst = NO;
        _liveView1.mediaPlayer.userStatus = GROUP_HOSTER;
        _mySeat=1;
        
        [_tcpSocket holdLiveStream];
    }else{
        _liveView1.mediaPlayer.userStatus=GROUP_WATCHER;
        
        _chunkDownloader = [[GroupChunkDownloader alloc] initWithGroupLivestreamID:_livestreamID];
        _chunkDownloader.delegate = self;
        
        [_tcpSocket enterLiveStream];
    }
    
    
    _liveView2.mediaPlayer.userStatus=GROUP_WATCHER;
    _liveView3.mediaPlayer.userStatus=GROUP_WATCHER;
    _liveView4.mediaPlayer.userStatus=GROUP_WATCHER;
    
    self.view.backgroundColor = BLACK_COLOR;
    

    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTap:)];
    
    [_cover addGestureRecognizer:tapGesture];
    
    
    [self buildStreamingView];
    [self reloadUI];
    
    
    //////////////////////////////test code
    [self reloadHeader];
    [_viewerScrollView reloadLiveStreamUserCount:0 liveViewerCount:0];
    //////////////////////////////////test code
    exitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exitButton.frame = CGRectMake(SCREEN_WIDTH-LIVE_STREAM_BUTTON_HEIGHT-LIVE_STREAM_VIEW_PADDING, 0, LIVE_STREAM_BUTTON_HEIGHT, LIVE_STREAM_BUTTON_HEIGHT);
    [exitButton setBackgroundImage:[UIImage imageNamed:@"stream_close"] forState:UIControlStateNormal];
    [exitButton setBackgroundImage:[UIImage imageNamed:@"stream_close_down"] forState:UIControlStateHighlighted];
    [exitButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
    
    liveshare = [UIButton buttonWithType:UIButtonTypeCustom];
    liveshare.frame = CGRectMake(SCREEN_WIDTH-2*LIVE_STREAM_BUTTON_HEIGHT-LIVE_STREAM_VIEW_PADDING-10, 0, LIVE_STREAM_BUTTON_HEIGHT, LIVE_STREAM_BUTTON_HEIGHT);
    [liveshare setBackgroundImage:[UIImage imageNamed:@"live_share"] forState:UIControlStateNormal];
    [liveshare setBackgroundImage:[UIImage imageNamed:@"live_share_down"] forState:UIControlStateHighlighted];
    [liveshare addTarget:self action:@selector(restream) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_cover];
    [self.view addSubview:_messageInputBar];

    _blurBgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)] ;
    
    cameraSwitchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cameraSwitchButton.showsTouchWhenHighlighted = YES;
    cameraSwitchButton.frame = CGRectMake(SCREEN_WIDTH-3*LIVE_STREAM_BUTTON_HEIGHT-LIVE_STREAM_VIEW_PADDING-20, 0, LIVE_STREAM_BUTTON_HEIGHT, LIVE_STREAM_BUTTON_HEIGHT);
    [cameraSwitchButton setBackgroundImage:[UIImage imageNamed:@"stream_cameratoggle"] forState:UIControlStateNormal];
    [cameraSwitchButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
    if (_liveStreamMode!=LIVE_SHOW_MODE_HOLDER) {
        cameraSwitchButton.hidden=YES;
    }
    [self.view addSubview:cameraSwitchButton];
    
    _broadFirst = YES;
    _watch1First = YES;
    _watch2First = YES;
    _watch3First = YES;
    
#ifdef GROUP_MEDIA_CTRLLER
    [_groupMediaCtrller start];
    _groupMediaCtrller.delegate = self;
#endif

    [self.view addSubview:liveshare];
    [self.view addSubview:exitButton];
}

-(void)didClickButton:(UIButton*)sender
{
    
    if([sender isEqual:exitButton]){
        if(_messageInputBar.messageTextView.isFirstResponder){
            [self resignMessageInputBar];
        }
        if(_liveStreamMode==LIVE_SHOW_MODE_HOLDER ){
//            [self exitLiveStream];
//            [self dismissViewControllerAnimated:YES completion:nil];
            [self exitLiveConfirm];
            
        } else if (_liveStreamMode==LIVE_SHOW_MODE_WATCH){
            [self exitLiveStream];
            [self dismissViewControllerAnimated:YES completion:nil];
        }else{
            [self exitLiveConfirm];
        }
    }else if([sender isEqual:cameraSwitchButton]){
        if (_mySeat==1) {
            _liveView1.mediaPlayer.isFrontCamera = !_liveView1.mediaPlayer.isFrontCamera;
            _liveView1.mediaPlayer.isTorchOn = NO;
            [_liveView1.mediaPlayer setupCamera];
            [self reloadUI];
        }else if (_mySeat==2){
            _liveView2.mediaPlayer.isFrontCamera=!_liveView2.mediaPlayer.isFrontCamera;
            _liveView2.mediaPlayer.isTorchOn=NO;
            [_liveView2.mediaPlayer setupCamera];
        }else if(_mySeat==3){
            _liveView3.mediaPlayer.isFrontCamera=!_liveView3.mediaPlayer.isFrontCamera;
            _liveView3.mediaPlayer.isTorchOn=NO;
            [_liveView3.mediaPlayer setupCamera];
        }else if(_mySeat==4){
            _liveView4.mediaPlayer.isFrontCamera=!_liveView4.mediaPlayer.isFrontCamera;
            _liveView4.mediaPlayer.isTorchOn=NO;
            [_liveView4.mediaPlayer setupCamera];
        }
 
        
    }else if([sender isEqual:torchModeSwitchButton]){
        
        if(_liveView1.mediaPlayer.isFrontCamera) {
            return ;
        }
        
        _liveView1.mediaPlayer.isTorchOn = !_liveView1.mediaPlayer.isTorchOn;
        
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        [device lockForConfiguration:nil];
        
        if ([device hasTorch]) {
            if(_liveView1.mediaPlayer.isTorchOn) {
                device.torchMode = AVCaptureTorchModeOn;
            } else {
                device.torchMode = AVCaptureTorchModeOff;
            }
        }
        
        [device unlockForConfiguration];
        
        [self reloadUI];
        
    }
}

-(void)viewWillDisappear:(BOOL)animated
{

    [DEFAULTS setObject:@0 forKey:IS_ON_LIVESTREAMING];
    [DEFAULTS synchronize];
    
    
    if (_privacypresent) {
        return;
    }
    
    [NOTIFICATION_CENTER removeObserver:self];
    [super viewWillDisappear:animated];
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [DIALOG_MANAGER removePopImmediately];
    [_tcpSocket cleanupSocket];

#ifdef GROUP_MEDIA_CTRLLER
    [_groupMediaCtrller stop];
#else
    [self stopAudioPlayer];
#endif

    DLog(@"\n\nView will disappear");
    
    if (_scene != nil && USE_SPRITEKIT)
    {
        [_scene setPaused:YES];
        [_scene removeAllActions];
        [_scene removeAllChildren];
        
        _scene = nil;
    }
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
    [[AVAudioSession sharedInstance] setPreferredSampleRate:44100.0 error:NULL];
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
}

//UI related
-(UIView*) detailHeaderView:(NSString *)picture openID:(NSString*)openID
{
    UIView *detailheaderView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, SCREEN_WIDTH/2)];
    UILabel* openIDLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/4, 60)];
    [openIDLabel setFont:SYSTEM_FONT_WITH_SIZE(18)];
    [openIDLabel setText:openID];
    [openIDLabel setTextAlignment:NSTextAlignmentCenter];
    [openIDLabel sizeToFit];
    openIDLabel.center=CGPointMake(SCREEN_WIDTH/4, 100+5);
    [openIDLabel setTextColor:WHITE_COLOR];
    [detailheaderView addSubview:openIDLabel];
    
    UIButton* followBtn = [ThemeManager getGreenLineBtn];
    followBtn.frame = CGRectMake(0, 0, SCREEN_WIDTH/4+30, 30);
    followBtn.layer.cornerRadius = 45/2.0f;
    [followBtn setTitle:LOCALIZE(@"+Follow") forState:UIControlStateNormal];
    followBtn.center = CGPointMake(SCREEN_WIDTH/4, 140);
    [detailheaderView addSubview:followBtn];
    followBtn.hidden=YES;
    __block UserObject *user=[UserObject new];
    
    UIButton* followingBtn = [ThemeManager getGreenLineBtn];
    
    followingBtn.frame = CGRectMake(0, 0, SCREEN_WIDTH/4+30, 30);
    followingBtn.layer.cornerRadius = 45/2.0f;
    [followingBtn setTitle:LOCALIZE(@"Following") forState:UIControlStateNormal];
    followingBtn.center = CGPointMake(SCREEN_WIDTH/4, 140);
    [detailheaderView addSubview:followingBtn];
    followingBtn.hidden=YES;
    [followingBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [followingBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
    [followingBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
    
    [followingBtn setTitle:LOCALIZE(@"user_profile_following") forState:UIControlStateNormal];
    
    
    
    
    
    [API_MANAGER getUserInfo:openID completion:^(BOOL success, UserObject *userObject) {
        if (success) {
            user=userObject;
            if ([openID isEqualToString:GET_DEFAULT(USER_OPEN_ID)]) {
                followBtn.hidden=YES;
                followingBtn.hidden=YES;
            }else{
                if(userObject.isFollowing>0){
                    followBtn.hidden = YES;
                    followingBtn.hidden=NO;
                }else{
                    followBtn.hidden=NO;
                    followingBtn.hidden=YES;
                }
            }
            
        }
    }];
    
    
    [followBtn bk_addEventHandler:^(id sender) {
        
        [API_MANAGER followUserAction:user.userID withCompletion:^(BOOL success) {
            if (success) {
                [DIALOG_MANAGER showCompleteToast];
                followingBtn.hidden=NO;
                followBtn.hidden=YES;
                user.isFollowing=1;
            }
        }];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [followingBtn bk_addEventHandler:^(id sender) {
        [API_MANAGER unfollowUserAction:user.userID withCompletion:^(BOOL success) {
            if (success) {
                [DIALOG_MANAGER showCompleteToast];
                followingBtn.hidden=YES;
                followBtn.hidden=NO;
                user.isFollowing=0;
            }
        }];
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIImageView* header = [[UIImageView alloc]initWithFrame:CGRectMake(5,0, 60, 60)];
    header.layer.masksToBounds = YES;
    header.layer.cornerRadius = 60/2.0;
    header.center=CGPointMake(SCREEN_WIDTH/4, 60);
    [header setBackgroundColor:WHITE_COLOR];
    
    
    
    
    
    
    
    if(picture.length==0){
        [header setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }else{
        [header setImageWithURL:S3_THUMB_IMAGE_URL(picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }
    
    //        [imageView setBackgroundColor:randomColor];
    
    [detailheaderView addSubview:header];
    return detailheaderView;
}

-(BOOL)sendMessageWithType:(NSString*)type andContent:(NSString*) content
{
    DLog(@"Send Livestream Message : %@",content);
    [_tcpSocket sendCommentAction:GET_DEFAULT(USER_OPEN_ID) picture:GET_DEFAULT(PICTURE) message:content colorCode:_colorcode];
    //colorcode
    return YES;
}

#pragma mark - UI views
- (void) actIndicatorBegin {
    [_activityIndicator startAnimating];
}

-(void) actIndicatorEnd {
    [_activityIndicator stopAnimating];
}

- (void)singleTap:(UIGestureRecognizer *)recognizer
{
    if(_messageInputBar.messageTextView.isFirstResponder) {
        [self resignMessageInputBar];
    }
    
    
    CGRect rect = _lvTable.frame;
    float messageInputBarHeight = _messageInputBar.frame.size.height;
    
    float maxtableH;
    if (IS_IPHONE_4) {
        maxtableH=75;
    }else{
        maxtableH=maxTableHeight;
    }
    float tableHeight = (_cellTotalHeight > maxtableH ? maxtableH : _cellTotalHeight);
    rect.origin.y = SCREEN_HEIGHT-tableHeight-messageInputBarHeight;
    rect.size.height = tableHeight;
    [UIView animateWithDuration:0.2 animations:^{
        _cover.alpha=0;
        _lvTable.frame = rect;
        if([_lvTable contentSize].height > _lvTable.frame.size.height)
        {
            CGPoint bottomOffset = CGPointMake(0, [_lvTable contentSize].height - _lvTable.frame.size.height);
            [_lvTable setContentOffset:bottomOffset animated:YES];
        }
    }];
    
    
    
}

#pragma mark - message input box
- (void)messageInputBar:(MessageInputBar *)bar willEditWithKeybordHeight:(float)keybordHeight
{
    CGRect rect = self.lvTable.frame;
    rect.size.height -= keybordHeight+TAB_BAR_HEIGHT;
    [self.lvTable setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-2*NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-keybordHeight-TAB_BAR_HEIGHT)];
    rect = self.messageInputBar.frame;
    rect.origin.y -= keybordHeight;
    [self.messageInputBar setFrame:rect];
}

- (void)sendTextMessage
{
    NSString *text = self.messageInputBar.messageTextView.text;
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    if([text isEqualToString:@""]) {
        return;
    }
    
    
    
    BOOL success = [self sendMessageWithType:@"text" andContent:text];
    
    if(success) {
        self.messageInputBar.messageTextView.text = @"";
        [self.messageInputBar.messageTextView setText:nil];
        [self resignMessageInputBar];
        CGRect rect = _lvTable.frame;
        float messageInputBarHeight = _messageInputBar.frame.size.height;
        float maxtableH;
        if (IS_IPHONE_4) {
            maxtableH=75;
        }else{
            maxtableH=maxTableHeight;
        }
        
        float tableHeight = (_cellTotalHeight > maxtableH? maxtableH : _cellTotalHeight);
        rect.origin.y = SCREEN_HEIGHT-tableHeight-messageInputBarHeight;
        rect.size.height = tableHeight;
        [UIView animateWithDuration:0.2 animations:^{
            _lvTable.frame = rect;
            _cover.alpha=0;
            if([_lvTable contentSize].height > _lvTable.frame.size.height)
            {
                CGPoint bottomOffset = CGPointMake(0, [_lvTable contentSize].height - _lvTable.frame.size.height);
                [_lvTable setContentOffset:bottomOffset animated:YES];
            }
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.messageInputBar textViewDidChange:self.messageInputBar.messageTextView];
            
        });
    }
}

-(void) repositionTableViewWithAnimation:(BOOL) animated
{
    if(_messageInputBar.mode==GAME_MODE) {
        return;
    }
    
    static float lastExecutionTime = 0.0;
    float currentTime = CACurrentMediaTime();
    
    if(currentTime-lastExecutionTime<0.05) {
        animated = NO;
    }
    
    lastExecutionTime = currentTime;
    
    int messageInputBarHeight = _messageInputBar.messageInputBarHeight;
    int keyboardHeight = _messageInputBar.keyboardHeight;
    
    
    if(animated==YES) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.25];
        [UIView setAnimationCurve:7]; // undocumented hack!
    }
    if (keyboardHeight>0)
    {
        float tableHeight = (_cellTotalHeight > (SCREEN_HEIGHT-keyboardHeight-messageInputBarHeight)? (SCREEN_HEIGHT-keyboardHeight-messageInputBarHeight) : _cellTotalHeight);
        int newY = SCREEN_HEIGHT-tableHeight-messageInputBarHeight-keyboardHeight;
        [_lvTable setFrame:CGRectMake(0, newY, _lvTable.frame.size.width, tableHeight)];
        [self.view bringSubviewToFront:_lvTable];
        [UIView animateWithDuration:0.2 animations:^{
            _cover.alpha=1;
        }];
        
    }
    [_messageInputBar setFrame:CGRectMake(0, SCREEN_HEIGHT-_messageInputBar.keyboardHeight-_messageInputBar.messageInputBarHeight, SCREEN_WIDTH, messageInputBarHeight)];
    
    if(animated==YES) {
        [UIView commitAnimations];
    }
    
    if(self.lvTable.contentOffset.y>0 && self.lvTable.contentSize.height<self.lvTable.frame.size.height){
        [self.lvTable setContentOffset:CGPointMake(0, 0)];
        return;
    }
}

#pragma mark - Tagging Handler
-(void)searchModeDetected:(NSString*)searchString
{
    _searchString = searchString;
}

#pragma mark - UI view creation and destruction
-(void) buildStreamingView
{
    _lvTable = [[LiveStreamTableView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-50, SCREEN_WIDTH,_cellTotalHeight)];
    [_lvTable registerClass:[LiveStreamTableViewCell class] forCellReuseIdentifier:@"lvChatCell"];
    _lvTable.lvDataSource = self;
    _lvTable.lvDelegate = self;
    _lvTable.lvViewerDelegate = self;
    
    [self.view addSubview:_lvTable];

    _chatMessages = [NSMutableArray new];
    
    [_lvTable reloadData];
    
    // full screen button
    
    // chat message input bar
    _messageInputBar = [[MessageInputBar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-44.0, SCREEN_WIDTH, 44.0)];
    _messageInputBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    _messageInputBar.chatVC = self;
    _messageInputBar.alpha = 1.0f;
    
    
    [_messageInputBar.sendMessageButton addTarget:self
                                           action:@selector(sendTextMessage)
                                 forControlEvents:UIControlEventTouchUpInside];
    _messageInputBar.messageTextView.keyboardType = UIKeyboardTypeTwitter;
    
//    if(_liveStreamMode == LIVE_SHOW_MODE_WATCH) {
    
        _viewerScrollView = [LiveViewScrollView new];
        _viewerScrollView.frame = CGRectMake(0, 10, SCREEN_WIDTH, [LiveViewScrollView getHeaderViewHeight]);
        _viewerScrollView.alpha = 0;
        _viewerScrollView.viewerScrollView.layer.masksToBounds = YES;
        _viewerScrollView.layer.masksToBounds = NO;
        _viewerScrollView.delegate = self;
        [self.view addSubview:_viewerScrollView];
        
    
//    }
}




-(void) reloadUI
{
    if(_liveView1.mediaPlayer.isFrontCamera) {
        
        torchModeSwitchButton.hidden = YES;
        
    } else {
        torchModeSwitchButton.hidden = NO;
        
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        
        if (![device hasTorch]) {
            torchModeSwitchButton.hidden = YES;
        }
    }
}



-(void)exitLiveConfirm
{
    
    UIWindow *topView = (UIWindow *)[UIApplication sharedApplication].keyWindow;
    CGRect rect = topView.frame;
    UIView* dialogBgView = [[UIView alloc] initWithFrame:rect];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
    dialogBgView.userInteractionEnabled = YES;
    [topView addSubview:dialogBgView];
    dialogBgView.alpha = 0.0f;
    
    int topMargin =7 ;
    int leftMargin =15 ;
    int btnHeight =45 ;
    int nextY = topMargin;
    float animationDuration = 0.3f;
    
    UIImageView* dialogBoxView = [UIImageView new];
    
    dialogBoxView.userInteractionEnabled = YES;
    [dialogBgView addSubview:dialogBoxView];
    
    /* Add Title */
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, btnHeight-10)];
    titleLabel.text = LOCALIZE(@"Confirm_end_group") ;
    titleLabel.textColor = WHITE_COLOR;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = BOLD_FONT_WITH_SIZE(18);
    titleLabel.numberOfLines = 2;
    titleLabel.minimumScaleFactor = 0.0;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.userInteractionEnabled = NO;
    [dialogBoxView addSubview:titleLabel];
    
    nextY += titleLabel.frame.size.height;
    
    
    
    
    nextY+= topMargin;
    
    /* Add OK Button */
    
    UIButton* okButton = [UIButton new];
    [okButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateNormal];
    [okButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateHighlighted];
    [okButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    okButton.titleLabel.font = BOLD_FONT_WITH_SIZE(16);
    [okButton setTitle:LOCALIZE(@"OK") forState:UIControlStateNormal];
    
    
    okButton.frame = CGRectMake(leftMargin, nextY, SCREEN_WIDTH*3/5, btnHeight);
    okButton.center=CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2);
    
    okButton.titleLabel.font = BOLD_FONT_WITH_SIZE(18);
    nextY += btnHeight+topMargin;
    [dialogBoxView addSubview:okButton];
    
    UIButton* cancelButton = [UIButton new];
    [cancelButton setTitle:LOCALIZE(@"Cancel") forState:UIControlStateNormal];
    [cancelButton setBackgroundImage:[[UIImage imageNamed:@"btn_black"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateNormal];
    [cancelButton setBackgroundImage:[[UIImage imageNamed:@"btn_black_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateHighlighted];
    [cancelButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    cancelButton.titleLabel.font = BOLD_FONT_WITH_SIZE(16);
    
    cancelButton.frame = CGRectMake(SCREEN_WIDTH*5/10, nextY, SCREEN_WIDTH*3/5, btnHeight);
    cancelButton.center=CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2);
    
    [dialogBoxView addSubview:cancelButton];
    nextY += topMargin + cancelButton.frame.size.height;
    
    
    
    float boxHeight = nextY;
    
    dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
    
    
    [dialogBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBgView.alpha = 0.0f;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
        });
    }]];
    
    if (_liveStreamMode==LIVE_SHOW_MODE_BROADCAST) {
        titleLabel.text = LOCALIZE(@"Confirm_end_group_in_broadcast") ;
    }
    __weak BlabViewController* weakSelf = self;

    
    [okButton bk_addEventHandler:^(id sender) {
   
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
        });
        if (_liveStreamMode==LIVE_SHOW_MODE_HOLDER) {
            [weakSelf exitLiveStream];
        }else{
            [weakSelf brocastLeaveLivestream];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    } forControlEvents:UIControlEventTouchUpInside];

    [cancelButton bk_addEventHandler:^(id sender) {
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBgView.alpha = 0.0f;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
        });
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    dialogBgView.alpha = 0;
    
    dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT/2-boxHeight/2, SCREEN_WIDTH, boxHeight);
    
    [UIView animateWithDuration:animationDuration animations:^{
        dialogBgView.alpha = 1.0f;
        dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT/2-boxHeight/2, SCREEN_WIDTH, boxHeight);
    }];
}


-(void) exitLiveStream
{
    
    [DIALOG_MANAGER showLoadingView];
    [API_MANAGER getGroupLiveStreamInfo:_livestreamID withCompletion:^(BOOL success, LiveStreamObject *liveSteamInfo) {
        [DIALOG_MANAGER hideLoadingView];
        [_liveView1.mediaPlayer calDuration];
        [_liveView1.mediaPlayer endLivestream];
        [_liveView2.mediaPlayer endLivestream];
        [_liveView3.mediaPlayer endLivestream];
        [_liveView4.mediaPlayer endLivestream];
        if (_isLiveStreamStart) {
            
            _isLiveStreamStart = NO;
            
            if (_liveStreamMode == LIVE_SHOW_MODE_HOLDER) {
                [cameraSwitchButton removeFromSuperview];
                [_tcpSocket livestreamEndedAction];

                [torchModeSwitchButton removeFromSuperview];
                [exitButton removeFromSuperview];
                [_viewerScrollView removeFromSuperview];
                [_lvTable removeFromSuperview];
     
                _blurBgImageView.image = [self snapshotViewWithBlur:YES];
                _blurBgImageView.alpha = 0.0f;
                [self.view addSubview:_blurBgImageView];
                [UIView animateWithDuration:0.5f animations:^{
                    _blurBgImageView.alpha = 1.0f;
                }];
                
                
                //            [self endlivestreamWithLivestreamInfo:_livestream];
                [self showLivestreamDetail];
            } else if(_liveStreamMode == LIVE_SHOW_MODE_WATCH) {
                
                //            [_lvConnWatcher disconnect];
            }
        }
    }];
 
}

- (UIImage*) snapshotViewWithBlur:(BOOL)isBlur
{
    UIImage *screenImg;
    UIGraphicsBeginImageContext(CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT));
    [self.view drawViewHierarchyInRect:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) afterScreenUpdates:YES];
    screenImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    if (isBlur) {
    screenImg = [UIImage imageByApplyingBlurToImage:screenImg withRadius:SCREEN_WIDTH/16 tintColor:[UIColor colorWithRed:0.f green:0.f blue:0.f alpha:0.6f] saturationDeltaFactor:1.f maskImage:nil];
    }
    
    return screenImg;
}

-(void)showLivestreamDetail
{
    __weak BlabViewController* weakSelf = self;
    _cover.hidden=NO;
    UIWindow *topView = (UIWindow *)[UIApplication sharedApplication].keyWindow;
    
    float duration=_liveView1.mediaPlayer.duration;
    
    int topMargin =SCREEN_HEIGHT/8 ;
    int btnHeight =35 ;
    
    float animationDuration = 0.3f;
    float textLabelHeight = 30.0f;
    float dataLabelHeight = 60.0f;

    UIImageView* dialogBoxView = [UIImageView new];
    
    dialogBoxView.userInteractionEnabled = YES;
    [topView addSubview:dialogBoxView];
    
  
    int nextY = topMargin;
    /* Add Label */
    if (IS_IPHONE_4) {
        nextY=5;
    }else{
        nextY=15;
    }
    UILabel* endLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [endLabel setTextColor:WHITE_COLOR];
    endLabel.textAlignment = NSTextAlignmentCenter;
    endLabel.font = BOLD_FONT_WITH_SIZE(35);
    [endLabel setText:LOCALIZE(@"grouplivestreamended")];
    [endLabel sizeToFit];
    endLabel.center = CGPointMake(SCREEN_WIDTH/2, nextY + endLabel.frame.size.height/2);
    [dialogBoxView addSubview:endLabel];
    nextY += endLabel.frame.size.height+30;
    
    UIView* header1=[self detailHeaderView:GET_DEFAULT(PICTURE) openID:GET_DEFAULT(USER_OPEN_ID)];
    [dialogBoxView addSubview:header1];
    
    if (IS_IPHONE_4) {
        header1.center=CGPointMake(SCREEN_WIDTH/4, SCREEN_HEIGHT/4-10);
    }else{
        header1.center=CGPointMake(SCREEN_WIDTH/4, SCREEN_HEIGHT/3-50);
    }
    if (![_liveView2.player.userID isEqualToString:@""]) {
        UIView* header2=[self detailHeaderView:_liveView2.player.picture openID:_liveView2.player.openID];
        if (IS_IPHONE_4) {
        header2.center=CGPointMake(SCREEN_WIDTH*3/4, SCREEN_HEIGHT/4-10);
        }else{
        header2.center=CGPointMake(SCREEN_WIDTH*3/4, SCREEN_HEIGHT/3-50);
        }
        [dialogBoxView addSubview:header2];
    }
    
    if (![_liveView3.player.userID isEqualToString:@""]) {
        UIView* header3=[self detailHeaderView:_liveView3.player.picture openID:_liveView3.player.openID];
        //        UIView* header3=[self detailHeaderView:GET_DEFAULT(PICTURE) openID:GET_DEFAULT(USER_OPEN_ID)];
        if (IS_IPHONE_4) {
            header3.center=CGPointMake(SCREEN_WIDTH/4, SCREEN_HEIGHT/4+150-10);
        }else{
            header3.center=CGPointMake(SCREEN_WIDTH/4, SCREEN_HEIGHT/3+100);
        }
        [dialogBoxView addSubview:header3];
    }
    
    if (![_liveView4.player.userID isEqualToString:@""]) {
        UIView* header4=[self detailHeaderView:_liveView4.player.picture openID:_liveView4.player.openID];
        //        UIView* header4=[self detailHeaderView:GET_DEFAULT(PICTURE) openID:GET_DEFAULT(USER_OPEN_ID)];
        if (IS_IPHONE_4) {
            header4.center=CGPointMake(SCREEN_WIDTH*3/4, SCREEN_HEIGHT/4+150-10 );
        }else{
            header4.center=CGPointMake(SCREEN_WIDTH*3/4, SCREEN_HEIGHT/3+100 );
        }
        [dialogBoxView addSubview:header4];
    }
    
    
    
    
    UILabel* totalViewLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [totalViewLabel setTextColor:WHITE_COLOR];
    totalViewLabel.textAlignment = NSTextAlignmentCenter;
    totalViewLabel.font = BOLD_FONT_WITH_SIZE(28);
    
    [totalViewLabel setText:INT_TO_STRING(_livestream.viewerCount)];
    [totalViewLabel sizeToFit];
    if (IS_IPHONE_4) {
        totalViewLabel.center = CGPointMake(SCREEN_WIDTH*1/4-20, SCREEN_HEIGHT-btnHeight-100+10);
    }else{
        totalViewLabel.center = CGPointMake(SCREEN_WIDTH*1/4-20, SCREEN_HEIGHT-btnHeight-120);
    }
    [dialogBoxView addSubview:totalViewLabel];
    
    

    
    
    UILabel* viewsLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, textLabelHeight)];
    [viewsLabel setTextColor:WHITE_COLOR];
    viewsLabel.textAlignment = NSTextAlignmentCenter;
    viewsLabel.font = SYSTEM_FONT_WITH_SIZE(18);
    [viewsLabel setText:LOCALIZE(@"Total_Views")];
    [viewsLabel sizeToFit];
    if (IS_IPHONE_4) {
        viewsLabel.center = CGPointMake(SCREEN_WIDTH*1/4-20, SCREEN_HEIGHT-btnHeight-100+50);
    }else{
        viewsLabel.center = CGPointMake(SCREEN_WIDTH*1/4-20, SCREEN_HEIGHT-btnHeight-80);
    }
    [dialogBoxView addSubview:viewsLabel];
    

    
    nextY += viewsLabel.frame.size.height+topMargin;
    
    
    UILabel* totalTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [totalTimeLabel setTextColor:WHITE_COLOR];
    totalTimeLabel.textAlignment = NSTextAlignmentCenter;
    totalTimeLabel.font = BOLD_FONT_WITH_SIZE(28);
    
    
    [totalTimeLabel setText:[SINGLETON getTimeFormatBySecond:_livestream.totalViewTime]];
    totalTimeLabel.numberOfLines = 1;
    totalTimeLabel.adjustsFontSizeToFitWidth = YES;
    [totalTimeLabel sizeToFit];
    if(totalTimeLabel.frame.size.width>SCREEN_WIDTH/2){
        totalTimeLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH/2, textLabelHeight);
    }
    if (IS_IPHONE_4) {
        totalTimeLabel.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT-btnHeight-100+10);
    }else{
        totalTimeLabel.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT-btnHeight-120);
    }
    [dialogBoxView addSubview:totalTimeLabel];
    
    UILabel* durationLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [durationLabel setTextColor:WHITE_COLOR];
    durationLabel.textAlignment = NSTextAlignmentCenter;
    durationLabel.font = BOLD_FONT_WITH_SIZE(28);
    
    
    [durationLabel setText:[SINGLETON getTimeFormatBySecond:duration]];
    [durationLabel sizeToFit];
    if (IS_IPHONE_4) {
        durationLabel.center = CGPointMake(SCREEN_WIDTH*3/4+20, SCREEN_HEIGHT-btnHeight-100+10);
    }else{
        durationLabel.center = CGPointMake(SCREEN_WIDTH*3/4+20, SCREEN_HEIGHT-btnHeight-120);
    }
    [dialogBoxView addSubview:durationLabel];
    
    nextY += totalTimeLabel.frame.size.height+3;
    
    
    UILabel* timesLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, 0, textLabelHeight)];
    [timesLabel setTextColor:WHITE_COLOR];
    timesLabel.textAlignment = NSTextAlignmentCenter;
    timesLabel.font = SYSTEM_FONT_WITH_SIZE(18);
    
    [timesLabel setText:LOCALIZE(@"Total_Duration")];
    [timesLabel sizeToFit];
    if (IS_IPHONE_4) {
        timesLabel.center = CGPointMake(SCREEN_WIDTH/2,SCREEN_HEIGHT-btnHeight-100+50 );
    }else{
        timesLabel.center = CGPointMake(SCREEN_WIDTH/2,SCREEN_HEIGHT-btnHeight-80 );
    }
    [dialogBoxView addSubview:timesLabel];
    
    UILabel* dLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, textLabelHeight)];
    [dLabel setTextColor:WHITE_COLOR];
    dLabel.textAlignment = NSTextAlignmentCenter;
    dLabel.font = SYSTEM_FONT_WITH_SIZE(18);
    [dLabel setText:LOCALIZE(@"Live_Duration")];
    [dLabel sizeToFit];
    if (IS_IPHONE_4) {
        dLabel.center = CGPointMake(SCREEN_WIDTH*3/4+20, SCREEN_HEIGHT-btnHeight-100+50);
    }else{
        dLabel.center = CGPointMake(SCREEN_WIDTH*3/4+20, SCREEN_HEIGHT-btnHeight-80);
    }
    [dialogBoxView addSubview:dLabel];
    
    nextY += timesLabel.frame.size.height+topMargin;
    
    /* Add Buttons */
    
    float boxHeight = 0.0f;
    
    boxHeight = nextY+btnHeight+topMargin;
    
    UIButton* deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteBtn.titleLabel.font =SYSTEM_FONT_WITH_SIZE(18);
    [deleteBtn setTitleColor:ALERT_TEXT_COLOR forState:UIControlStateNormal];
    deleteBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH/3, btnHeight);
    deleteBtn.layer.cornerRadius = btnHeight/2.0f;
    [deleteBtn.layer setBorderColor:[ALERT_TEXT_COLOR CGColor]];
    deleteBtn.layer.borderWidth=1.0f;
    deleteBtn.center = CGPointMake(timesLabel.center.x, nextY+btnHeight/2);
    
    [deleteBtn setTitle:LOCALIZE(@"Delete_Replay") forState:UIControlStateNormal];
    //    [dialogBoxView addSubview:deleteBtn];
    
    UIButton* saveBtn = [UIButton new];
    [saveBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateNormal];
    [saveBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateHighlighted];
    [saveBtn setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    
    saveBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH*2/3, btnHeight);
    if (IS_IPHONE_4) {
        saveBtn.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT-btnHeight-10);
    }else{
        saveBtn.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT-btnHeight-30);
    }
    [saveBtn setTitle:LOCALIZE(@"OK") forState:UIControlStateNormal];
    [dialogBoxView addSubview:saveBtn];
    
    
    [deleteBtn bk_addEventHandler:^(id sender) {
        
        [UIView animateWithDuration:animationDuration animations:^{
            
            //            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBoxView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBoxView removeFromSuperview];
            
        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    [saveBtn bk_addEventHandler:^(id sender) {
        [UIView animateWithDuration:animationDuration animations:^{
            //  dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBoxView.alpha = 0.0f;
            
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBoxView removeFromSuperview];
            
        });
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    } forControlEvents:UIControlEventTouchUpInside];
    
    dialogBoxView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    [UIView animateWithDuration:animationDuration animations:^{
        dialogBoxView.alpha = 1.0f;
    }];
}


#pragma mark - LiveStreamConnectionWatcher delegate
-(void)phonecall
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)connectionLinkFailBy:(LIVE_STREAM_DISCONNECT_TYPE)type
{
    switch (type) {
        case LIVE_STREAM_DISCONNECT_TYPE_HOST_END:
        {
            [cameraSwitchButton removeFromSuperview];
            [torchModeSwitchButton removeFromSuperview];
            [exitButton removeFromSuperview];
            [_viewerScrollView removeFromSuperview];
            [_lvTable removeFromSuperview];
            [self.view endEditing:YES];
            
            _blurBgImageView.image = [self snapshotViewWithBlur:YES];
            _blurBgImageView.alpha = 0.0f;
            [self.view addSubview:_blurBgImageView];
            [UIView animateWithDuration:0.5f animations:^{
                _blurBgImageView.alpha = 1.0f;
            }];
            
            [NOTIFICATION_CENTER postNotificationName:REFRESH_FEEDS object:self userInfo:nil];
        }
            break;
        case LIVE_STREAM_DISCONNECT_TYPE_KEEP_ALIVE_FAIL:
        {
            [self forceExit:LOCALIZE(@"network_unstable")];
        }
            break;
        case LIVE_STREAM_DISCONNECT_TYPE_KICKED_BY_HOST:
        {
            [self forceExit:LOCALIZE(@"block_by_host")];
            
        }
            break;
            
        default:
            break;
    }
    
//    [_lvConnWatcher disconnect];
}



# pragma mark - LiveStreamTableViewDataSource
- (NSInteger) numberOfRowsInLiveSteamTableView: (LiveStreamTableView*) lvTableView
{
    NSInteger i = [_chatMessages count];
    return i;
}

- (LiveStreamChatMessage*) liveStreamTableView:(LiveStreamTableView*) lvTableView dataForRow: (NSInteger) row
{
    return [_chatMessages objectAtIndex:row];
}

# pragma mark - LiveStreamTableViewDelegate
- (void)liveStreamTableView:(LiveStreamTableView*)lvTableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath;
{
    [self singleTap:nil];
}

#pragma mark - WatcherFetcher
- (void)reloadHeader
{
    
    CGRect rect = _viewerScrollView.frame;
    rect.origin.y =10;
    _viewerScrollView.frame = rect;
    
    [UIView animateWithDuration:0.2 animations:^{
        _viewerScrollView.alpha = 1;
    }];
    
    //    _likeCount = _livestream.receivedLikeCount;
}



-(void) fetchLivestreamInfo:(NSString*)livestreamID
{
    
    if(livestreamID==nil)
        return;
    
//    if(_noMoreWatcher || _isFirstFetching){
//        [self fetchingWatchers:livestreamID refresh:YES];
//    }
    
    if(_isFetchingInfo)
        return;
    
    _isFetchingInfo = YES;
    
    [API_MANAGER getGroupLiveStreamInfo:livestreamID withCompletion:^(BOOL success, LiveStreamObject *liveSteamInfo) {
        _isFetchingInfo = NO;
        
        if(success){
            _isFirstFetchInfo = NO;
            _livestream = liveSteamInfo;
            [_viewerScrollView reloadLiveStreamUserCount:_livestream.viewerCount liveViewerCount:_livestream.liveViewerCount];
//            
//            DLog(@"\n\n_livestream.following=%d",_livestream.user.isFollowing);
//            DLog(@"viewerArray:%@",_livestream.viewerArray);
            [_viewerScrollView reloadPositioning:_livestream viewers:_livestream.viewerArray];

            [self reloadHeader];
            //            [self reloadAudioMode];
        }
    }];

    
}

-(void)forceExit:(NSString*)titleText
{
    [self resignMessageInputBar];
    
    [NOTIFICATION_CENTER postNotificationName:REFRESH_FEEDS object:self userInfo:nil];
    
    [DIALOG_MANAGER showActionSheetOKDialogTitle:titleText message:@"" buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

#pragma mark LiveViewerDelegate

-(void)didClickUser:(UserObject*)user
{
    
    [self resignMessageInputBar];
    [DIALOG_MANAGER showActionSheetUserDialog:user isPublisher:(LIVE_SHOW_MODE_HOLDER==_liveStreamMode) isLivestreamer:(user.userID==_livestream.userID) withCompletion:^(UserDialogAction selectedAction) {
        switch (selectedAction) {
            case UserDialogActionFollow:
            {
                [self followUser:user];
            }
                break;
                
            case UserDialogActionResponse:
            {
                [self responseUser:user];
            }
                break;
            case UserDialogActionUnfollow:
            {
                [self unfollowUser:user];
            }
                break;
            case UserDialogActionReport:
            {
                [self reportUserAction:user];
            }
                break;
            case UserDialogActionBlock:
            {
                [self blockUserAction:user];
            }
                break;
            case UserDialogActionUnblock:
            {
                [self unblockUserAction:user];
            }
                break;
            case UserDialogActionCancelFollowRequest:
            {
                
            }
                break;
        }
    }];
}

-(void)didClickUserwithcomment:(UserObject *)user withMessageString:(NSString *)withMessageString
{
    [self resignMessageInputBar];
    [DIALOG_MANAGER showActionSheetUserDialogcommentver:user isPublisher:(LIVE_SHOW_MODE_HOLDER==_liveStreamMode) isLivestreamer:(user.userID==_livestream.userID) withCompletion:^(NSString *selectedAction) {
        if([selectedAction isEqualToString:@"follow"]) {
            [self followUser:user];
            
        }else if([selectedAction isEqualToString:@"response"]) {
            [self responseUser:user];
        }else if([selectedAction isEqualToString:@"unfollow"]){
            [self unfollowUser:user];
        }else if([selectedAction isEqualToString:@"report"]){
            [self reportUserAction:user withMessageString:withMessageString];
        }else if([selectedAction isEqualToString:@"block"]){
            [self blockUserAction:user];
        }else if([selectedAction isEqualToString:@"unblock"]){
            [self unblockUserAction:user];
        }else if([selectedAction isEqualToString:@"admin"]){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self showAdminDialog:user];
            });
            
        }
    }];
}


-(void) showAdminDialog:(UserObject*)user
{
    
    
    [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"Admin_Button") options:@[LOCALIZE(@"admin_ban_user"),LOCALIZE(@"admin_freeze_user"),LOCALIZE(@"block")] destructiveIndexes:@[@0,@1] cancelable:YES withCompletion:^(int selectedOption) {
        if(selectedOption==0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self adminBanAction:user];
            });
        }else if (selectedOption==1){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self adminFreezeAction:user];
            });
        }else if (selectedOption==2){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self adminBlockAction:user];
            });
        }
    }];
}

-(void) adminBlockAction:(UserObject*)user
{
    [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"Block_confirm") options:@[LOCALIZE(@"OK")] destructiveIndexes:@[@0,@1] cancelable:YES withCompletion:^(int selectedOption) {
        if(selectedOption==0){
            [API_MANAGER adminBlockUserAction:MY_USER_ID publisherUserID:_livestream.userID blockedUserID:user.userID inLivestreamID:@""  withCompletion:^(BOOL success) {
                [DIALOG_MANAGER showCompleteToast];
            }];
        }
    }];
}


-(void) adminBanAction:(UserObject*)user
{
    [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"Ban_confirm") options:@[LOCALIZE(@"OK")] destructiveIndexes:@[@0,@1] cancelable:YES withCompletion:^(int selectedOption) {
        if(selectedOption==0){
            [API_MANAGER banUserAction:user.userID inLivestreamID:@"" absTime:0 withCompletion:^(BOOL success, NSString *message) {
                [DIALOG_MANAGER showCompleteToast];
            }];
        }
    }];
}

-(void) adminFreezeAction:(UserObject*)user
{
    [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"Freeze_confirm") options:@[LOCALIZE(@"OK")] destructiveIndexes:@[@0,@1] cancelable:YES withCompletion:^(int selectedOption) {
        if(selectedOption==0){
            [API_MANAGER freezeUserAction:user.userID inLivestreamID:@"" absTime:0 withComletion:^(BOOL success,NSString* message) {
                [DIALOG_MANAGER showCompleteToast];
            }];
        }
    }];
}

-(void)didClickBackground
{
    [self singleTap:nil];
}


-(void)blockUserAction:(UserObject*)user
{
    [API_MANAGER blockUserAction:user.userID withCompletion:^(BOOL success) {
        if(success){
            user.isBlocked = 1;
            [DIALOG_MANAGER showCompleteToast];
        }
    }];
}

-(void)unblockUserAction:(UserObject*)user
{
    [API_MANAGER unblockUserAction:user.userID withCompletion:^(BOOL success) {
        if(success){
            user.isBlocked = 0;
            [DIALOG_MANAGER showCompleteToast];
        }
    }];
}



-(void)followUser:(UserObject*)user
{
    if([GET_DEFAULT(FOLLOWING_COUNT) intValue] >= MAX_FOLLOWER_COUNT){
        [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"cannot_follow_user") message:[NSString stringWithFormat:LOCALIZE(@"max_followcount"),MAX_FOLLOWER_COUNT] buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
            if(okClicked){
                
            }
        }];
    }else{
        [API_MANAGER followUserAction:user.userID withCompletion:^(BOOL success) {
            if(success){
                user.isFollowing = 1;
                [DIALOG_MANAGER showCompleteToast];
            }
        }];
    }
}

-(void)reportUserAction:(UserObject*)user withMessageString:(NSString *)withMessageString
{
    [API_MANAGER reportUserAction:user.userID withReason:withMessageString andMessage:@"Livestream Comment Report" withCompletion:^(BOOL success, NSString *message) {
        if(success){
            [DIALOG_MANAGER showCompleteToast];
        }
    }];
}

-(void)reportUserAction:(UserObject*)user
{
    [API_MANAGER reportUserAction:user.userID withReason:@"in livestream,no certainly reason" andMessage:@"Livestream Comment Report" withCompletion:^(BOOL success, NSString *message) {
        if(success){
            [DIALOG_MANAGER showCompleteToast];
        }
    }];
}

-(void)unfollowUser:(UserObject*)user
{
    
    [API_MANAGER unfollowUserAction:user.userID withCompletion:^(BOOL success) {
        if(success){
            user.isFollowing = 0;
            [DIALOG_MANAGER showCompleteToast];
        }
    }];
}


-(void)responseUser:(UserObject*)user
{
    [_messageInputBar.messageTextView setText:[NSString stringWithFormat:@"@%@ ",user.openID]];
    [self becomeFirstMessageInputBar];
}

- (void) becomeFirstMessageInputBar
{
    [_messageInputBar.messageTextView becomeFirstResponder];
    [UIView animateWithDuration:0.2 animations:^{
        _messageInputBar.alpha = 1.0f;
    }];
}

- (void) resignMessageInputBar
{
    [_messageInputBar.messageTextView resignFirstResponder];
    
}

#pragma mark - System Notification
-(void)enterBackground:(id)sender
{
    _isBackground = YES;
    _liveView1.mediaPlayer.audioOnly=YES;
    _liveView2.mediaPlayer.audioOnly=YES;
    _liveView3.mediaPlayer.audioOnly=YES;
    _liveView4.mediaPlayer.audioOnly=YES;
    if(_liveStreamMode==LIVE_SHOW_MODE_HOLDER ) {
        
        if(_isLiveStreamStart){
            _livestream.audioOnly = YES;
        }
        
    } else if(_liveStreamMode==LIVE_SHOW_MODE_WATCH && _isLiveStreamStart) {
        
    }
    [self updateNowPlaying];
}

-(void)enterForeground:(id)sender
{
    _isBackground = NO;
    _liveView1.mediaPlayer.audioOnly=NO;
    _liveView2.mediaPlayer.audioOnly=NO;
    _liveView3.mediaPlayer.audioOnly=NO;
    _liveView4.mediaPlayer.audioOnly=NO;
    if(_liveStreamMode==LIVE_SHOW_MODE_HOLDER && _isLiveStreamStart) {
        
        _livestream.audioOnly = NO;

        
    } else if(_liveStreamMode==LIVE_SHOW_MODE_WATCH && _isLiveStreamStart) {
        
        
    }
}

-(void)keyboardNotificationHandler:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    _keyboardHeight = keyboardFrame.size.height;
    
    if(_liveStreamMode ==LIVE_SHOW_MODE_HOLDER && !_isLiveStreamStart){
        /* Start Button Adjust */
        
        _keyboardHeight = keyboardFrame.size.height;
        
    }
}

/* Media Settings  */
-(void)updateNowPlaying;
{
    if ([MPNowPlayingInfoCenter class]) {
        UIImage *musicImage = [UIImage imageNamed:@"onair"];
        MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc]
                                        initWithImage:musicImage];
        
        NSString *trackName = _livestream.user.openID;
        NSString *albumName = _livestream.caption;
        
        NSArray *objs = [NSArray arrayWithObjects:
                         trackName,albumName,
                         albumArt, nil];
        
        NSArray *keys = [NSArray arrayWithObjects:
                         MPMediaItemPropertyTitle,
                         MPMediaItemPropertyAlbumTitle,
                         MPMediaItemPropertyArtwork, nil];
        
        // TRICKY: TO BLOCK CRASH!
        if(objs.count!=keys.count) {
            return;
        }
        
        NSDictionary *currentTrackInfo = [NSDictionary dictionaryWithObjects:objs forKeys:keys];
        [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = currentTrackInfo;
        MPRemoteCommandCenter *remoteCommandCenter = [MPRemoteCommandCenter sharedCommandCenter];
        [[remoteCommandCenter seekForwardCommand] addTarget:self action:@selector(test)];
    }
}





-(void) livestreamInfoChange:(NSData*)data
{
    if(_liveStreamMode==LIVE_SHOW_MODE_HOLDER){
        [self fetchLivestreamInfo:_livestream.liveStreamID];
    }else{
        [self fetchLivestreamInfo:_livestream.liveStreamID];
    }
}

-(void)receiveComment:(LiveStreamCommentObject*)data
{

        LiveStreamCommentObject *obj = data;

        LiveStreamChatMessage *msg = [[LiveStreamChatMessage alloc] initWithLiveStreamCommentObject:obj andAtUser:@""];
        NSString* livestreamMsgID = [msg getLivestreamCommentID];
        if(![_receivedMessageID containsObject:livestreamMsgID]){
            
            [_chatMessages addObject:msg];
        }
    [_receivedMessageID addObject:livestreamMsgID];
    _cellTotalHeight += [LiveStreamTableViewCell getHeightForCell:msg];
    
    [_lvTable reloadData];
    
    CGRect rect = _lvTable.frame;
    float messageInputBarHeight = _messageInputBar.frame.size.height;
    float keyboardHeight = 0.0f;
    if (_messageInputBar.messageTextView.isFirstResponder) {
        keyboardHeight = _keyboardHeight;
    }
    
    float tableHeight = (_cellTotalHeight > maxTableHeight? maxTableHeight : _cellTotalHeight);
    rect.origin.y = SCREEN_HEIGHT-tableHeight-messageInputBarHeight-keyboardHeight;
    rect.size.height = tableHeight;
    [UIView animateWithDuration:0.2 animations:^{
        _lvTable.frame = rect;
    }];
    
    if ([_chatMessages count]>1 && _lvTable.isOnBottom) {
        
        /* truncate comment to MAX_COMMENT_COUNT */
        if ([_chatMessages count] >= 2*MAX_COMMENT_COUNT) {
            [_chatMessages removeObjectAtIndex:0];
            NSArray *truncateArray = [_chatMessages subarrayWithRange:NSMakeRange([_chatMessages count]-1-MAX_COMMENT_COUNT, MAX_COMMENT_COUNT)];
            _chatMessages = [truncateArray mutableCopy];
            [_lvTable reloadData];
            
            
            [_lvTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_chatMessages count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
        
        }
}


-(void)restream
{
    
    NSString* liveStreamID = _livestream.liveStreamID ;
    NSString* liveStreamerOpenID = _livestream.user.openID ;
    NSString* myOpenID = GET_DEFAULT(USER_OPEN_ID) ;
    
    [API_MANAGER restreamLiveStream:liveStreamID liveStreamerOpenID:liveStreamerOpenID myOpenID:myOpenID withCompletion:^(BOOL success) {
        
        if(success){
            [API_MANAGER commentLiveStream:_livestream.liveStreamID comment:LOCALIZE(@"Livestream_Restream_message") colorCode:@"" absTimestamp:1 withCompletion:^(BOOL success, NSString *message) {}];
        }else{
        }
    }];
    
}

-(void)endlivestreamWithLivestreamInfo:(LiveStreamObject *)livestream
{
    /* Show livestream Result */
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        _blurBgImageView.image = [self snapshotViewWithBlur:YES];
        _blurBgImageView.alpha = 0.f;
        [UIView animateWithDuration:0.5f animations:^{
            _blurBgImageView.alpha = 1.f;
        }];
        
        // show live stream data
        [self resignMessageInputBar];
        __weak  BlabViewController* weakself=self;
                [DIALOG_MANAGER showActionSheetLivestreamDetail:_livestream withCompletion:^(NSString *selectedAction) {
                    [weakself dismissViewControllerAnimated:YES completion:nil];
                }];
        
    });
}



-(void)longTap:(UILongPressGestureRecognizer*)longGesture
{
    if (longGesture.state == UIGestureRecognizerStateEnded) {
        //        NSLog(@"UIGestureRecognizerStateEnded");
        //Do Whatever You want on End of Gesture
    }
    else if (longGesture.state == UIGestureRecognizerStateBegan){
        //        NSLog(@"UIGestureRecognizerStateBegan.");
        //Do Whatever You want on Began of Gesture
    }else if(longGesture.state == UIGestureRecognizerStateChanged){
        //        NSLog(@"UIGestureRecognizerStateChanged.");
        
    }
}

#ifdef GROUP_MEDIA_CTRLLER
#pragma mark - GroupMediaControllerDelegate
- (void)playVideoData:(GroupVideoFrame*)gVF
{
    switch (gVF.channelID) {
        case GroupLiveChannelPositionLeftTop:
            [_liveView1.mediaPlayer playVideoFrame:gVF];
            break;
        case GroupLiveChannelPositionRightTop:
            [_liveView2.mediaPlayer playVideoFrame:gVF];
            break;
        case GroupLiveChannelPositionLeftBottom:
            [_liveView3.mediaPlayer playVideoFrame:gVF];
            break;
        case GroupLiveChannelPositionRightBottom:
            [_liveView4.mediaPlayer playVideoFrame:gVF];
            break;
            
        default:
            DLog(@"Err: GroupVideoFrame channelID");
            break;
    }
}
#else
#pragma mark - Audio
- (void) stopAudioPlayer
{
    if(_audioPlayer != nil) {
        [_audioPlayer cleanup];
        _audioPlayer.delegate = nil;
        _audioPlayer = nil;
    }
}

- (void) startAudioPlayer
{
    if (_audioPlayer == nil) {
        _audioPlayer = [PCMAUAudioPlayer new];
        _audioPlayer.delegate = self;
        [_audioPlayer setup];
    }
}

-(void) didPlayAudioinChannel:(int)ch atAbsTime:(int)absTime numberOfAudioBufferInQueue:(int)numberOfAudioBufferInQueue
{
    DLog(@"play ch %d audio at AbsTimestamp:%d, numberOfAudioBufferInAudioQueue=%d", ch, absTime, numberOfAudioBufferInQueue);
}
#endif

#pragma mark - GroupBroadcasterDelegate
-(void)sendVideoEncodedData:(NSData*)data videoSequence:(u_int8_t)videoSequence absTime:(int32_t)absTime channelID:(u_int8_t)channel
{
    [_tcpSocket sendVideoData:data videoSequence:videoSequence absTime:absTime channelID:channel];
}

-(void)sendAudioEncodedData:(NSData*)data audioSequence:(u_int8_t)audioSequence absTime:(int32_t)absTime channelID:(u_int8_t)channel
{
    [_tcpSocket sendAudioData:data audioSequence:audioSequence absTime:absTime channelID:channel];
}

#ifdef GROUP_MEDIA_CTRLLER
#else
#endif

#pragma mark - GroupChunkDownloaderDelegate
- (void)groupAudioFrameReady:(GroupAudioFrame*)audioFrame inChunk:(int32_t)chunkID
{
#ifdef GROUP_MEDIA_CTRLLER
    [_groupMediaCtrller schedulePlayGroupAudioFrame:audioFrame];
#else
    [_audioPlayer playGroupAudioFrame:audioFrame];
#endif
}

- (void)groupVideoFrameReady:(GroupVideoFrame*)videoFrame inChunk:(int32_t)chunkID
{
#ifdef GROUP_MEDIA_CTRLLER
    [_groupMediaCtrller schedulePlayGroupVideoFrame:videoFrame];
#else
    switch (videoFrame.channelID) {
        case GroupLiveChannelPositionLeftTop:
            [_liveView1.mediaPlayer playVideoFrame:videoFrame];
            break;
        case GroupLiveChannelPositionRightTop:
        {
            [_liveView2.mediaPlayer playVideoFrame:videoFrame];
            if (!_liveView2.isPlaying) {
                if (_requestingJoin) {
                    [self repositionRequestView];
                }
                _liveView2.isPlaying=YES;
            }
        }
            break;
        case GroupLiveChannelPositionLeftBottom:
            [_liveView3.mediaPlayer playVideoFrame:videoFrame];
            if (!_liveView3.isPlaying) {
                if (_requestingJoin) {
                    [self repositionRequestView];
                }
                _liveView3.isPlaying=YES;
            }
            break;
        case GroupLiveChannelPositionRightBottom:
            [_liveView4.mediaPlayer playVideoFrame:videoFrame];
            if (!_liveView2.isPlaying) {
                if (_requestingJoin) {
                    [self repositionRequestView];
                }
                [self repositionRequestView];
                _liveView4.isPlaying=YES;
            }
            break;
            
        default:
            DLog(@"Err: GroupVideoFrame channelID");
            break;
    }
#endif
}

#pragma mark - GroupTcpConnectionDelegate
-(void)didReceiveVideoData:(GroupVideoFrame*)videoFrameObj
{
    
    if (videoFrameObj.channelID ==1&&_mySeat!=1) {
        [_liveView1.mediaPlayer playVideoFrame:videoFrameObj];
    }
    if (videoFrameObj.channelID ==2&&_mySeat!=2) {
        [_liveView2.mediaPlayer playVideoFrame:videoFrameObj];
        if (!_liveView2.isPlaying) {
            if (_liveStreamMode!=LIVE_SHOW_MODE_HOLDER) {
                _liveView2.isPlaying=YES;
                [_liveView2 showGlView];
            }
        }
    }
    if (videoFrameObj.channelID ==3&&_mySeat!=3) {
        [_liveView3.mediaPlayer playVideoFrame:videoFrameObj];
        if (!_liveView3.isPlaying) {
            if (_liveStreamMode!=LIVE_SHOW_MODE_HOLDER) {
                _liveView3.isPlaying=YES;
            }
            [_liveView3 showGlView];
        }
    }
    
    
    if (videoFrameObj.channelID ==4&&_mySeat!=4) {
        [_liveView4.mediaPlayer playVideoFrame:videoFrameObj];
        if (!_liveView4.isPlaying) {
            if (_liveStreamMode!=LIVE_SHOW_MODE_HOLDER) {
                _liveView4.isPlaying=YES;
            }
            [_liveView4 showGlView];
        }
    }
}

-(void)didReceiveAudioData:(GroupAudioFrame*)audioFrame
{
#ifdef GROUP_MEDIA_CTRLLER
    [_groupMediaCtrller schedulePlayGroupAudioFrame:audioFrame];
#else
    [_audioPlayer playGroupAudioFrame:audioFrame];
#endif
}

-(void)didReceiveChunkReadyData:(ChunkReadyObject *)chunkObj
{
//    DLog(@"CHUNK READY CHUNKID:%d",chunkObj.chunkID);
    
    if (_isFirstDownloadChunk) {
        _isFirstDownloadChunk = NO;
        [_chunkDownloader initialFromChunkID:chunkObj.chunkID toChunkID:chunkObj.prefetchTargetChunkID];
    } else {
        [_chunkDownloader downloadGroupChunk:chunkObj.prefetchTargetChunkID];
    }
}

-(void)didReceiveJsonFormatData:(BLAB_ACTION_TYPE)type infoDict:(NSDictionary*)infoDict
{
    DLog(@"receiveInfoDict:%@",infoDict);
//    DLog(@"MY_USER_ID=%@",MY_USER_ID);
//    DLog(@"來的userid=%@",infoDict[@"userID"]);


    switch (type) {
        case COMMAND_ACK:
            
            break;
        case ENTER_GROUP_LIVESTREAM:
            
            break;
        case LEAVE_GROUP_LIVESTREAM:
            
            break;
        case SEND_COMMENT_ACTION:
        {
            UserObject* user=[UserObject getUserWithDict:infoDict];
            LiveStreamCommentObject *obj=[LiveStreamCommentObject getLiveStreamCommentWithDictandUser:infoDict user:user];
            [self receiveComment:obj];
            break;
        }
        case BLOCK_USER_ACTION:
            if ([infoDict[@"targetUserID"]isEqualToString:MY_USER_ID]) {
                [self forceExit:LOCALIZE(@"block_by_host")];
            }
            break;
        case REQUEST_JOIN_ACTION:
//            [_requestingUser addObject:infoDict[@"userID"]];
            if (_liveStreamMode==LIVE_SHOW_MODE_HOLDER) {
                GroupUserObject* user=[GroupUserObject new];
                user.userID=infoDict[@"userID"];
                user.picture=infoDict[@"picture"];
                user.openID=infoDict[@"openID"];
                for (int index=0; index<[_playerPicArrary count]; index++) {
                    if ([user.picture isEqualToString:[_playerPicArrary objectAtIndex:index]]) {
                        break;
                    }
                }
                [_requesterArrary addObject:user];
                if ([_requestUser.userID isEqualToString:@""]) {
                    [self showJoinRequest:0];
                }
            }
            break;
        case REJECT_JOIN_ACTION:
            if ([infoDict[@"targetUserID"]isEqualToString:MY_USER_ID]) {
                _requestingJoin=NO;
                switch (_requestingSeat) {
                    case 2:
                        _liveView2.requestDenyView.hidden=NO;
                        break;
                    case 3:
                        _liveView3.requestDenyView.hidden=NO;
                    case 4:
                        _liveView4.requestDenyView.hidden=NO;

                    default:
                        break;
                }
            }
            break;
        case CANCEL_JOIN_ACTION:
            if (_liveStreamMode==LIVE_SHOW_MODE_HOLDER) {
        
                [self removeRequester:infoDict[@"userID"]];
                if ([infoDict[@"userID"]isEqualToString:_requestUser.userID]) {
                    [self showJoinRequest:0];
                }
            }
            break;
        case LOCK_SEAT_ACTION:

            if (_liveStreamMode!=LIVE_SHOW_MODE_HOLDER)
            {
                switch ([infoDict[@"locknums"] intValue]) {
                    case 2:
                    {
                        [_liveView2 lockSeat];
                        [self reloadLockBtn:2];
                    }
                        break;
                    case 3:
                    {
                        [_liveView3 lockSeat];
                        [self reloadLockBtn:3];
                    }
                        break;
                    case 4:
                    {
                        [_liveView4 lockSeat];
                        [self reloadLockBtn:4];
                    }
                        break;
                    default:
                        break;
                }
                
            }
            break;
        case ACCEPT_JOIN_ACTION:

            if ([infoDict[@"targetUserID"]isEqualToString:MY_USER_ID]) {
                _requestingJoin=NO;
                if ([infoDict[@"channelID"]isEqualToString:@"2"]) {
                    [_liveView2 showGlView];
                    _liveView2.isPlaying=YES;
                    _liveView2.mediaPlayer.userStatus=GROUP_PARTICIPANT;
                    _mySeat=2;
                    _liveView2.kickBtn.hidden=NO;
                }else if([infoDict[@"channelID"]isEqualToString:@"3"]){
                    [_liveView3 showGlView];
                    _liveView3.isPlaying=YES;
                    _liveView3.mediaPlayer.userStatus=GROUP_PARTICIPANT;
                    _mySeat=3;
                    _liveView3.kickBtn.hidden=NO;
                }else if ([infoDict[@"channelID"]isEqualToString:@"4"]){
                    [_liveView4 showGlView];
                    _liveView4.isPlaying=YES;
                    _liveView4.mediaPlayer.userStatus=GROUP_PARTICIPANT;
                    _mySeat=4;
                    _liveView4.kickBtn.hidden=NO;
                }
                _liveStreamMode=LIVE_SHOW_MODE_BROADCAST;
                cameraSwitchButton.hidden=NO;
            }
//            if (_liveStreamMode!=LIVE_SHOW_MODE_HOLDER) {
//            }
            break;
        case UNLOCK_SEAT_ACTION:
            if (_liveStreamMode!=LIVE_SHOW_MODE_HOLDER)
            {
                switch ([infoDict[@"unlocknums"] intValue]) {
                    case 2:
                    {
                        [_liveView2 unlockSeat];
                        [self reloadLockBtn:2];
                    }
                        break;
                    case 3:
                    {
                        [_liveView3 unlockSeat];
                        [self reloadLockBtn:3];
                    }
                        break;
                    case 4:
                    {
                        [_liveView4 unlockSeat];
                        [self reloadLockBtn:4];
                    }
                        break;
                    default:
                        break;
                }

            }

            break;
        case KICK_OUT_ACTION:
            if ([infoDict[@"targetUserID"]isEqualToString:MY_USER_ID]) {
                [self brocastLeaveLivestream];
            }
            break;
        case LIVESTREAM_ENDED:
            if(_liveStreamMode!=LIVE_SHOW_MODE_HOLDER)
            {
                [self forceExit:LOCALIZE(@"livestreamended")];
            }
            break;
        case QUIT_BROADCAST_ACTION:
            if (_liveStreamMode==LIVE_SHOW_MODE_HOLDER) {
                _avaliableSeats++;
                if ([infoDict[@"channelID"]isEqualToString:@"2"]) {
                    _liveView2.isPlaying=NO;
                    _liveView2.kickBtn.hidden=YES;
                    _liveView2.player.userID=@"";
                    _liveView2.openIDLabel.hidden=YES;
                    [_liveView2 hideGlview];
                }else if ([infoDict[@"channelID"]isEqualToString:@"3"]){
                    _liveView3.isPlaying=NO;
                    _liveView3.kickBtn.hidden=YES;
                    _liveView3.player.userID=@"";
                    _liveView3.openIDLabel.hidden=YES;
                    [_liveView3 hideGlview];
                }else if ([infoDict[@"channelID"]isEqualToString:@"4"]){
                    _liveView4.isPlaying=NO;
                    _liveView4.kickBtn.hidden=YES;
                    _liveView4.player.userID=@"";
                    _liveView4.openIDLabel.hidden=YES;
                    [_liveView4 hideGlview];
                }
                for (int index=0; index<[_playerPicArrary count]; index++) {
                    if ( [_playerPicArrary objectAtIndex:index]!=nil)
                    {
                        if ([[_playerPicArrary objectAtIndex:index] isEqualToString:infoDict[@"picture"]]) {
                            [_playerPicArrary removeObjectAtIndex:index];
                        }
                    }
                }
                
                [self updateInfo];
                
            }else{
                if ([infoDict[@"channelID"]isEqualToString:@"2"]) {
                    _liveView2.isPlaying=NO;
                    _liveView2.player.userID=@"";
                    _liveView2.openIDLabel.hidden=YES;
                    [_liveView2 hideGlview];
                }else if ([infoDict[@"channelID"]isEqualToString:@"3"]){
                    _liveView3.isPlaying=NO;
                    _liveView3.player.userID=@"";
                    _liveView3.openIDLabel.hidden=YES;
                    [_liveView3 hideGlview];
                }else if ([infoDict[@"channelID"]isEqualToString:@"4"]){
                    _liveView4.isPlaying=NO;
                    _liveView4.player.userID=@"";
                    _liveView4.openIDLabel.hidden=YES;
                    [_liveView4 hideGlview];
                }
            }
            break;
        default:
            break;
    }
}

-(void)didConnectToHost:(BOOL)isConnect
{
}




-(void)rollLeftAction
{
    _requesterIndex--;
    [self showJoinRequest:_requesterIndex];
}

-(void)rollRightAction
{
    _requesterIndex++;
    [self showJoinRequest:_requesterIndex];
}

-(void)showJoinRequest:(int)index
{
    BOOL rightBtn;
    BOOL leftBtn;
    if([_requesterArrary count]-_requesterIndex==1){
        rightBtn=NO;
    }else if ([_requesterArrary count]-_requesterIndex>1) {
        rightBtn=YES;
    }
    
    if (_requesterIndex==0) {
        leftBtn=NO;
    }else{
        leftBtn=YES;
    }
    
    if([_requesterArrary count]==0)
    {
        _requestingSeat=0;
        _requestUser.userID=@"";
        _liveView2.requestJoinHolderView.hidden=YES;
        _liveView3.requestJoinHolderView.hidden=YES;
        _liveView4.requestJoinHolderView.hidden=YES;
        return;
    }
    
    if ([_requesterArrary count]-index<1) {
        return;
    }
    if (_avaliableSeats==0) {
        return;
    }
//    _requestUserID=userID;
        _requestUser=[GroupUserObject new];
        _requesterIndex=index;
        GroupUserObject *user=[_requesterArrary objectAtIndex:index];
        _requestUser.userID=user.userID;
        _requestUser.openID=user.openID;
        _requestUser.picture=user.picture;
    
    
        if (!_liveView2.isPlaying) {
            _requestingSeat=2;
            [_liveView2 showJoinRequest:user leftBtn:leftBtn rightBtn:rightBtn];
        }else if (!_liveView3.isPlaying) {
            _requestingSeat=3;
            [_liveView3 showJoinRequest:user leftBtn:leftBtn rightBtn:rightBtn];
        }else if (!_liveView4.isPlaying) {
            _requestingSeat=4;
            [_liveView4 showJoinRequest:user leftBtn:leftBtn rightBtn:rightBtn];
        }else{
            
        }


    
    
    
}

-(void)wactherEnter
{
    [API_MANAGER enterGroupLiveStream:_livestreamID withCompletion:^(BOOL success, NSString *message, int timestamp, int numberOfChunks, NSString *colorCode) {
        if (success) {
//            [API_MANAGER getGroupLiveStreamInfo:_livestreamID withCompletion:^(BOOL success, LiveStreamObject *liveSteamInfo) {
//                
//            }];
            _colorcode=colorCode;
            [self fetchLivestreamInfo:_livestreamID];
        }else{
            [self forceExit:LOCALIZE(@"network_unstable")];

        }
        
    }];
}

//-(void)reloadSeatUI
//{
//
//            if (!_isPlaying2&&_requestingSeat!=2) {
//                lockbtn2.hidden=NO;
//                _locklabel2.hidden=NO;
//                [self hideGlView:2];
//            }else if (!_isPlaying3&&_requestingSeat!=3){
//                lockbtn3.hidden=NO;
//                _locklabel3.hidden=NO;
//                [self hideGlView:3];
//            }else if (!_isPlaying4&&_requestingSeat!=4){
//                lockbtn4.hidden=NO;
//                _locklabel4.hidden=NO;
//                [self hideGlView:4];
//            }
//    
//    if (_isPlaying2) {
//        [self ShowGlView:2];
//    }
//    if (_isPlaying3) {
//        [self ShowGlView:3];
//    }
//    if (_isPlaying4) {
//        [self ShowGlView:4];
//    }
//    
//    if (_liveStreamMode==LIVE_SHOW_MODE_WATCH&&_requestingJoin) {
//        _request2.hidden=YES;
//        _request3.hidden=YES;
//        _request4.hidden=YES;
//        if (!_isPlaying2) {
//            _request2.hidden=NO;
//            lockbtn2.hidden=YES;
//            _locklabel2.hidden=YES;
//        }else if (!_isPlaying3){
//            _request3.hidden=NO;
//            lockbtn3.hidden=YES;
//            _locklabel3.hidden=YES;
//        }else if (!_isPlaying4){
//            _request4.hidden=NO;
//            lockbtn4.hidden=YES;
//            _locklabel4.hidden=YES;
//        }
//    }else if (_liveStreamMode==LIVE_SHOW_MODE_WATCH&&!_requestingJoin){
//        _request2.hidden=YES;
//        _request3.hidden=YES;
//        _request4.hidden=YES;
//    }
//    
//    if (_liveStreamMode==LIVE_SHOW_MODE_HOLDER&&_avaliableSeats>0&&[_requesterArrary count]>0) {
//        
//        
//        [self showJoinRequest:_requesterIndex];
//        
//        
//        if(_requesterIndex>0)
//        {
//            rollLeftBtn.hidden=NO;
//        }else if (_requesterIndex==0){
//            rollLeftBtn.hidden=YES;
//        }
//        
//        if ([_requesterArrary count]-_requesterIndex>1){
//                    rollRightBtn.hidden=NO;
//                }
//    }
//}

-(void)removeRequester:(NSString*)userID
{
    for (NSInteger index=0; index<[_requesterArrary count]; index++) {
        GroupUserObject *user=_requesterArrary[index];
        if ([user.userID isEqualToString:userID]) {
            [_requesterArrary removeObjectAtIndex:index];
            [self showJoinRequest:0];
            break;
        }
    }
}

-(void)applyRequestYes
{
    if (_avaliableSeats==0) {
        return;
    }
    BOOL contain;
    contain=NO;
    for (int index=0; index<[_playerPicArrary count]; index++) {
        if ( [_playerPicArrary objectAtIndex:index]!=nil)
        {
            if ([[_playerPicArrary objectAtIndex:index] isEqualToString:_requestUser.picture]) {
                contain=YES;
            }
        }
    }
    if (!contain) {
        [_playerPicArrary addObject:_requestUser.picture];
    }
    [self updateInfo];
    
    int channel=0;
    if (!_liveView2.isPlaying) {
        channel=2;
        [_liveView2 applyRequestYes:_requestUser];
    }else if (!_liveView3.isPlaying){
        channel=3;
        [_liveView3 applyRequestYes:_requestUser];

    }else if (!_liveView4.isPlaying) {
        channel=4;
        [_liveView4 applyRequestYes:_requestUser];
    }
    
    if (channel!=0) {
        [_tcpSocket acceptJoinAction:_requestUser.userID channelID:channel];
        _avaliableSeats--;

        [self removeRequester:_requestUser.userID];

        if (_requesterIndex>0) {
            _requesterIndex--;
            [self showJoinRequest:_requesterIndex];
        }else{
            [self showJoinRequest:0];
        }
    }else{

    }

    
}



-(void)applyRequestNo
{
    [_tcpSocket rejectJoinAction:_requestUser.userID];

    [self removeRequester:_requestUser.userID];
    
    if (_requesterIndex>0) {
        _requesterIndex--;
        [self showJoinRequest:_requesterIndex];
    }else{
        [self showJoinRequest:0];
    }
}

-(void)updateInfo
{
    
    DLog(@"_playerPicArrary:%@",_playerPicArrary);
    
    [API_MANAGER updateGroupLiveStreamInfo:@{@"pictures":TO_JSON(_playerPicArrary),@"liveStreamID":_livestreamID} withCompletion:^(BOOL success) {
        if (success) {
            
        }
    }];
}



-(void)brocastLeaveLivestream
{
    _chunkDownloader.delegate = nil;
    
    if (_mySeat==0 || _mySeat==1) {
        DLog(@"leave");
    }else{
        _requestingJoin=NO;
        _liveStreamMode=LIVE_SHOW_MODE_WATCH;
        cameraSwitchButton.hidden=YES;
        if (_mySeat==2) {
            [_tcpSocket quitBroadCaterAction:2 picture:GET_DEFAULT(PICTURE)];
            _liveView2.isPlaying=NO;
            _liveView2.kickBtn.hidden=YES;
            _liveView2.openIDLabel.hidden=YES;
            _liveView2.mediaPlayer.userStatus=GROUP_WATCHER;
            [_liveView2 hideGlview];
        }else if (_mySeat==3){
            [_tcpSocket quitBroadCaterAction:3 picture:GET_DEFAULT(PICTURE)];
            _liveView3.isPlaying=NO;
            _liveView3.kickBtn.hidden=YES;
            _liveView3.openIDLabel.hidden=YES;
            _liveView3.mediaPlayer.userStatus=GROUP_WATCHER;
            [_liveView3 hideGlview];
        }else if (_mySeat==4)
            [_tcpSocket quitBroadCaterAction:4 picture:GET_DEFAULT(PICTURE)];
            _liveView4.isPlaying=NO;
            _liveView4.kickBtn.hidden=YES;
            _liveView4.openIDLabel.hidden=YES;
            _liveView4.mediaPlayer.userStatus=GROUP_WATCHER;
            [_liveView4 hideGlview];
        
    }
    
}

-(void)kickAction:(GroupUserObject*)user channelID:(int)channelID
{
    for (int index=0; index<[_playerPicArrary count]; index++) {
        if ( [_playerPicArrary objectAtIndex:index]!=nil)
        {
            if ([[_playerPicArrary objectAtIndex:index] isEqualToString:user.picture]) {
                [_playerPicArrary removeObjectAtIndex:index];
            }
        }
    }
    [self updateInfo];
    if (_liveStreamMode==GROUP_HOSTER) {
        [_tcpSocket kickOutAction:user.userID];
        [_tcpSocket quitBroadCaterAction:channelID picture:user.picture];
    }else{
        _mySeat=0;
        _liveStreamMode=GROUP_WATCHER;
        [_tcpSocket quitBroadCaterAction:channelID picture:user.picture];
    }

}


-(void)quitLiveStream
{
    if (_liveStreamMode==LIVE_SHOW_MODE_BROADCAST) {
        [self brocastLeaveLivestream];
    }
    if (_liveStreamMode==LIVE_SHOW_MODE_WATCH&&_requestingJoin) {
        [_tcpSocket cancelJoinAction:GET_DEFAULT(USER_OPEN_ID) picture:GET_DEFAULT(PICTURE)];
    }
}



-(void)sendTcpRequest:(BLAB_ACTION_TYPE)type infoDict:(NSDictionary*)infoDict
{
    switch (type) {
        case REQUEST_JOIN_ACTION:
        {
            [_tcpSocket requestJoinAction:GET_DEFAULT(USER_OPEN_ID) picture:GET_DEFAULT(PICTURE)];
            _requestingJoin=YES;
        }
            break;
//        case REJECT_JOIN_ACTION:
//            [_tcpSocket rejectJoinAction:infoDict[@"userID"]];
//            break;
        case CANCEL_JOIN_ACTION:
        {
            [_tcpSocket cancelJoinAction:GET_DEFAULT(USER_OPEN_ID) picture:GET_DEFAULT(PICTURE)];
            _requestingJoin=NO;
        }
            break;
//        case ACCEPT_JOIN_ACTION:
//            [_tcpSocket acceptJoinAction:infoDict[@"userID"] channelID:[infoDict[@"channelID"] intValue]];
//            break;
        case LOCK_SEAT_ACTION:
            [_tcpSocket lockSeatAction:2];
            break;
        case UNLOCK_SEAT_ACTION:
            [_tcpSocket unlockSeatAction:2];
            break;
//        case KICK_OUT_ACTION:
//            [_tcpSocket kickOutAction:infoDict[@"userID"]];
//            break;

        default:
            break;
    }
}

-(void)reloadLockBtn:(int)lockNum
{
    switch (lockNum) {
        case 2:
        {
            [_liveView2 showLockBtn];
            [_liveView3 hideLockBtn];
            [_liveView4 hideLockBtn];
        }
            break;
        case 3:
        {
            [_liveView2 hideLockBtn];
            [_liveView3 showLockBtn];
            [_liveView4 hideLockBtn];
        }
            break;
        case 4:
        {
            [_liveView2 hideLockBtn];
            [_liveView3 hideLockBtn];
            [_liveView4 showLockBtn];
        }
            break;
        default:
            break;
    }
}

-(void)repositionRequestView
{
    
    if (!_liveView2.isPlaying) {
        _liveView2.requestJoinWatcherView.hidden=YES;
        _liveView3.requestJoinWatcherView.hidden=NO;
        _liveView4.requestJoinWatcherView.hidden=YES;
    }else if (!_liveView3.isPlaying){
        _liveView2.requestJoinWatcherView.hidden=YES;
        _liveView3.requestJoinWatcherView.hidden=NO;
        _liveView4.requestJoinWatcherView.hidden=YES;
    }else if (!_liveView4.isPlaying){
        _liveView2.requestJoinWatcherView.hidden=YES;
        _liveView3.requestJoinWatcherView.hidden=YES;
        _liveView4.requestJoinWatcherView.hidden=NO;
    }else{
        _liveView2.requestJoinWatcherView.hidden=YES;
        _liveView3.requestJoinWatcherView.hidden=YES;
        _liveView4.requestJoinWatcherView.hidden=YES;
    }
}

-(GroupUserObject*)getRequester
{
    return _requestUser;
}
@end