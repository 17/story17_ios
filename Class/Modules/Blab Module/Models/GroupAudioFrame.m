//
//  GroupAudioFrame.m
//  story17
//
//  Created by POPO Chen on 9/14/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "GroupAudioFrame.h"
#import "Constant.h"

@implementation GroupAudioFrame

+(GroupAudioFrame*)getGroupAudioFrameFromData:(NSData*)data
{
    
    GroupAudioFrame* groupAF = [GroupAudioFrame new];
    
    groupAF.packetLength = *((int32_t*) (data.bytes));
    groupAF.commandSequence = *((int32_t*) (data.bytes+5));
    groupAF.jsonLength = *((int32_t*) (data.bytes+9));
    NSString *string = [[NSString alloc] initWithData:[data subdataWithRange:NSMakeRange(13, groupAF.jsonLength)] encoding:NSUTF8StringEncoding];
    groupAF.jsonDict = [string mj_JSONObject];

    groupAF.audioDataLength = *((int32_t*) (data.bytes+13+groupAF.jsonLength));
    groupAF.channelID = *((u_int8_t*) (data.bytes+17+groupAF.jsonLength));
    groupAF.audioFrameNum = *((u_int8_t*) (data.bytes+18+groupAF.jsonLength));
    groupAF.audioAbsTime = *((int32_t*) (data.bytes+19+groupAF.jsonLength));
    groupAF.serverAbsTime = *((int32_t*) (data.bytes+23+groupAF.jsonLength));
    groupAF.audioEncodedData = [NSData dataWithBytes:(data.bytes+27+groupAF.jsonLength) length:groupAF.audioDataLength];
    
    return groupAF;
}

@end
