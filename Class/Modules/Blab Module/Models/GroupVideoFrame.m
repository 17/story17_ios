//
//  GroupVideoFrame.m
//  story17
//
//  Created by POPO Chen on 9/14/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "GroupVideoFrame.h"
#import "Constant.h"

@implementation GroupVideoFrame


+(GroupVideoFrame*)getGroupVideoFrameFromData:(NSData*)data
{

    GroupVideoFrame* groupVF = [GroupVideoFrame new];
    
    groupVF.packetLength = *((int32_t*) (data.bytes));
    groupVF.commandSequence = *((int32_t*) (data.bytes+5));
    groupVF.jsonLength = *((int32_t*) (data.bytes+9));
    NSString *string = [[NSString alloc] initWithData:[data subdataWithRange:NSMakeRange(13, groupVF.jsonLength)] encoding:NSUTF8StringEncoding];
    groupVF.jsonDict = [string mj_JSONObject];

    groupVF.videoDataLength = *((int32_t*) (data.bytes+13+groupVF.jsonLength));
    groupVF.channelID = *((u_int8_t*) (data.bytes+17+groupVF.jsonLength));
    groupVF.videoFrameNum = *((u_int8_t*) (data.bytes+18+groupVF.jsonLength));
    groupVF.videoAbsTime = *((int32_t*) (data.bytes+19+groupVF.jsonLength));
    groupVF.serverAbsTime = *((int32_t*) (data.bytes+23+groupVF.jsonLength));
    groupVF.videoEncodedData = [NSData dataWithBytes:(data.bytes+27+groupVF.jsonLength) length:groupVF.videoDataLength];
    
    return groupVF;
    
}


@end
