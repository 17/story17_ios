//
//  ChunkReadyObject.m
//  story17
//
//  Created by POPO Chen on 10/16/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "ChunkReadyObject.h"

@implementation ChunkReadyObject

+(ChunkReadyObject*)getChunkReadyData:(NSData*)data
{
    ChunkReadyObject* chunkData = [ChunkReadyObject new];
    
    chunkData.groupLiveStreamID = *((int32_t*) (data.bytes+5));
    chunkData.chunkID = *((int32_t*) (data.bytes+9));
    chunkData.prefetchTargetChunkID = *((int32_t*) (data.bytes+13));
    chunkData.latestChunkID = *((int32_t*) (data.bytes+17));

    return chunkData;
}

@end
