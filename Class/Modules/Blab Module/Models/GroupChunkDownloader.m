//
//  GroupConnectionWatcher.m
//  story17
//
//  Created by POPO Chen on 10/16/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "GroupChunkDownloader.h"
#import "Constant.h"
#import "GroupLiveConstants.h"
#import "VP8VideoDecoder.h"
#import "MyOpusDecoder.h"
#import "ChunkDataObject.h"

@interface GroupChunkDownloader()

// GCD Queue
@property dispatch_queue_t socketQueue;

@property (nonatomic, strong) AFHTTPRequestOperationManager* fileDownloadManager;

@property (nonatomic,strong) NSString* groupLivestreamID;
@property (nonatomic, strong) NSMutableArray* windowObjects;
@property (nonatomic, strong) NSMutableArray* parsingObjects;
@property BOOL isResetWindow;
@property int32_t window_size;

#define MIN_WINDOW_SIZE 3

@end

@implementation GroupChunkDownloader

- (GroupChunkDownloader*)initWithGroupLivestreamID:(NSString*)groupLivestreamID
{
    if (self = [super init]) {
        
        _groupLivestreamID = groupLivestreamID;

        /* queue */
        _socketQueue = dispatch_queue_create("live_stream_socket_queue_watcher", DISPATCH_QUEUE_SERIAL);

        /* tcp-like download algorithm */
        _windowObjects =  CREATE_MUTABLE_ARRAY;
        _parsingObjects = CREATE_MUTABLE_ARRAY;
    }
    return self;
}

- (void)stopGroupChunkDownloader
{
    /* reset */
    [_parsingObjects removeAllObjects];
    [_windowObjects removeAllObjects];
    
    /* set to the end of the window to stop download */
    _windowStartPoint+=_window_size;
}

- (void)downloadGroupChunk:(int32_t)chunkID
{
    [self checkWindowForChunk:chunkID];
}

#pragma mark - tcp-like playing algorithm
- (void)initialFromChunkID:(int32_t)chunkID toChunkID:(int32_t)toChunkID
{
    DLog(@"initial window to %d", chunkID);
    
    _windowStartPoint = chunkID;
    _window_size = toChunkID-chunkID+1;
    if (_window_size < MIN_WINDOW_SIZE) {
        _window_size = MIN_WINDOW_SIZE;
    }
    
    for (int i=0; i<_window_size; i++) {
        ChunkDataObject* chunk = [ChunkDataObject new];
        [_windowObjects addObject:chunk];
    }
    
    _isResetWindow = YES;
    _windowStartPoint = chunkID;
    
    /* reset queue */
    [_parsingObjects removeAllObjects];
    
    [self getChunksFromWindowStartPointToChunk:toChunkID];
    
    /* check if it can play after reset */
    [self checkIsContinuousChunk:[_windowObjects objectAtIndex:0]];
}

- (void)checkWindowForChunk:(int32_t)chunkID
{
    NSAssert1(chunkID>0, @"chunkID=%d", chunkID);
    if (chunkID <= 0) {
        return;
    } else if (chunkID > _windowStartPoint+_window_size-1) {
        // add 1 for safe
        [self resetWindowStartPointToNumber:chunkID-_window_size+1+1 andLastestChunkID:chunkID];
    } else {
        [self getChunksFromWindowStartPointToChunk:chunkID];
    }
}

- (void)resetWindowStartPointToNumber:(int32_t)chunkID andLastestChunkID:(int32_t)latestChunkID
{
    DLog(@"resetWindowStartPointToNumber %d, latest chunk %d", chunkID, latestChunkID);
    
    NSAssert1(chunkID>0, @"chunkID=%d", chunkID);
    if (chunkID <= 0 || chunkID <= _windowStartPoint || _windowStartPoint==0)
        return;
    
    /* reset window */
    int windowObjRemoveCount = MIN(chunkID-_windowStartPoint, _window_size);
    for (int i=0; i<windowObjRemoveCount; i++) {
        [_windowObjects removeObjectAtIndex:0];
        ChunkDataObject* chunk = [ChunkDataObject new];
        [_windowObjects addObject:chunk];
    }
    _isResetWindow = YES;
    _windowStartPoint = chunkID;
    
    /* reset queue */
    [_parsingObjects removeAllObjects];
    [self getChunksFromWindowStartPointToChunk:latestChunkID];
    
    /* check if it can play after reset */
    [self checkIsContinuousChunk:[_windowObjects objectAtIndex:0]];
}

- (void)getChunksFromWindowStartPointToChunk:(int32_t)chunkID
{
    if (chunkID <= 0)
        return;
    
    for (int i=0; i<chunkID-_windowStartPoint+1; i++) {
        ChunkDataObject* chunk = [_windowObjects objectAtIndex:i];
        if (chunk.status == ChunkStatusNone) {
            chunk.chunkID = _windowStartPoint+i;
            [self downloadChunkData:chunk];
        }
    }
}

- (void)downloadChunkData:(ChunkDataObject*)chunk
{
    if (chunk.status != ChunkStatusNone || chunk.chunkID <=0)
        return;
    
    chunk.status = ChunkStatusDownloading;
    
    if(_fileDownloadManager == nil) {
        _fileDownloadManager = [AFHTTPRequestOperationManager manager];
        _fileDownloadManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _fileDownloadManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/octet-stream"];
    }
    
    NSDate *now = [NSDate date];
    
    static double totalTime = 0.0;
    static int count = 0;
    
    [_fileDownloadManager GET:[NSString stringWithFormat:@"%@%@/%@/%d", SERVER_IP, @"getGroupLiveStreamChunk", _groupLivestreamID, chunk.chunkID]
                   parameters:nil
                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
                          double timePassed_ms = [now timeIntervalSinceNow] * -1000.0;
                          
//                          DLog(@"t2-t1: %f", timePassed_ms);
                          totalTime += timePassed_ms;
                          count++;
//                          DLog(@"average: %f", totalTime/count);
                          
                          DLog(@"download chunk %d success, windowStartPoint:%d", chunk.chunkID,_windowStartPoint);
                          
                          if (chunk.chunkID>=_windowStartPoint && chunk.chunkID<_windowStartPoint+_window_size) {
                              chunk.status = ChunkStatusDownloaded;
                              chunk.chunkData = (NSData*) responseObject;
                              [self checkIsContinuousChunk:chunk];
                          }
                      }
                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                          
                          DLog(@"download chunk %d fail", chunk.chunkID);
                          
                          if (chunk.chunkID>=_windowStartPoint && chunk.chunkID<_windowStartPoint+_window_size) {
                              chunk.status = ChunkStatusNone;
                              [self downloadChunkData:chunk];
                          }
                      }
     ];
}

- (void)checkIsContinuousChunk:(ChunkDataObject*)chunk
{
    if (chunk.status != ChunkStatusDownloaded || chunk.chunkID <=0)
        return;
    
    if (chunk.chunkID == _windowStartPoint) {
        
        /* move window */
        _windowStartPoint++;
        
        /* move this chunk for decoding */
        [_parsingObjects addObject:chunk];
        [_windowObjects removeObject:chunk];
        ChunkDataObject* newChunk = [ChunkDataObject new];
        [_windowObjects addObject:newChunk];
        
        /* parse chunk */
        [self parseChunk];
        
        // check the next chunk
        ChunkDataObject* nextChunk = [_windowObjects firstObject];
        [self checkIsContinuousChunk:nextChunk];
    }
}

- (void)parseChunk
{
    /* get chunk to decode */
//    if (_isResetWindow) {
//        if ([_parsingObjects count]<3) {
//            return;
//        } else {
//            _isResetWindow = NO;
//        }
//    } else {
//        if ([_parsingObjects count]==0) {
//            return;
//        }
//    }
    if ([_parsingObjects count]==0) {
        return;
    }
    
    ChunkDataObject* chunk = [_parsingObjects firstObject];
    [_parsingObjects removeObject:chunk];
    
    NSData* chunkDataToParse = chunk.chunkData;
    
    dispatch_async(_socketQueue, ^{
        
        int dataPtr = 0;
        while(dataPtr < chunkDataToParse.length) {
            /* get info */
            int32_t dataLength = *((int32_t*) (chunkDataToParse.bytes+dataPtr));
            u_int8_t packetType = *((u_int8_t*) (chunkDataToParse.bytes+dataPtr+4));
            NSData* newdata = [chunkDataToParse subdataWithRange:NSMakeRange(dataPtr,dataLength)];
            
            if (packetType == AUDIO_FRAME_DATA) {
                
                GroupAudioFrame* groupAF = [GroupAudioFrame getGroupAudioFrameFromData:newdata];
                if (_delegate!=nil) {
                    dispatch_async(MAIN_QUEUE, ^{
                        [_delegate groupAudioFrameReady:groupAF inChunk:chunk.chunkID];
                    });
                }
            } else if(packetType == VIDEO_FRAME_DATA) {
                
                GroupVideoFrame* groupVF = [GroupVideoFrame getGroupVideoFrameFromData:newdata];
                
                if(_delegate!=nil){
                    dispatch_async(MAIN_QUEUE, ^{
                        [_delegate groupVideoFrameReady:groupVF inChunk:chunk.chunkID];
                    });
                }
            }
            
            dataPtr += dataLength;
        }
        
        [self parseChunk];
    });
}

-(void)dealloc
{
    
}

@end

