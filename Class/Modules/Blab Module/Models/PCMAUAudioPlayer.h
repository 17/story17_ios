//
//  PCMAUAudioPlayer.h
//  Story17
//
//  Created by POPO on 10/7/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AudioFrameObject.h"
#import "GroupAudioFrame.h"

@protocol PCMAUAudioPlayerDelegate
-(void) didPlayAudioinChannel:(int)ch atAbsTime:(int)absTime numberOfAudioBufferInQueue:(int) numberOfAudioBufferInQueue;
@end

@interface PCMAUAudioPlayer : NSObject

@property (nonatomic, assign) id<PCMAUAudioPlayerDelegate> delegate;

-(void) play:(NSData*)audioData channel:(int)ch absTime:(int)absTime;
-(void) play:(NSData*)audioData absTime:(int)absTime;
-(void) playEncodedAudioData:(NSData*)audioData inChannel:(int)ch atAbsTime:(int)absTime;
-(void) playAudioFrame:(AudioFrameObject*)aF;
-(void) playGroupAudioFrame:(GroupAudioFrame*)gAF;
-(void) playSilence;
-(void) setup;
-(void) reset;
-(void) cleanup;
-(void) setGain:(float) gain;
-(void) syncAudio;

@end
