//
//  GroupVideoFrame.h
//  story17
//
//  Created by POPO Chen on 9/14/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GroupLiveConstants.h"

@interface GroupVideoFrame : NSObject

@property (nonatomic, strong) NSData* videoEncodedData;
@property int32_t commandSequence;
@property int32_t packetLength;
@property int32_t jsonLength;
@property (nonatomic, strong) NSDictionary* jsonDict;
@property int32_t videoDataLength;
@property GroupLiveChannelPosition channelID;
@property u_int8_t videoFrameNum;
@property int32_t videoAbsTime;
@property int32_t serverAbsTime;
@property (nonatomic, strong) NSArray* yuvData;

+(GroupVideoFrame*)getGroupVideoFrameFromData:(NSData*)data;

@end
