//
//  GroupConnectionWatcher.h
//  story17
//
//  Created by POPO Chen on 10/16/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GroupVideoFrame.h"
#import "GroupAudioFrame.h"
#import "ChunkReadyObject.h"

@protocol GroupChunkDownloaderDelegate

@optional
- (void)groupAudioFrameReady:(GroupAudioFrame*)audioFrame inChunk:(int32_t)chunkID;
- (void)groupVideoFrameReady:(GroupVideoFrame*)videoFrame inChunk:(int32_t)chunkID;

@end


@interface GroupChunkDownloader : NSObject

@property int32_t windowStartPoint;
@property (assign) id <GroupChunkDownloaderDelegate> delegate;

- (id)initWithGroupLivestreamID:(NSString*)groupLivestreamID;
- (void)stopGroupChunkDownloader;

- (void)initialFromChunkID:(int32_t)chunkID toChunkID:(int32_t)toChunkID;
- (void)downloadGroupChunk:(int32_t)chunkID;

@end