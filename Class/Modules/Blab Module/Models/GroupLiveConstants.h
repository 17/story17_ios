//
//  GroupLiveConstants.h
//  story17
//
//  Created by POPO Chen on 9/14/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#ifndef story17_GroupLiveConstants_h
#define story17_GroupLiveConstants_h

typedef enum {
    JSON_FORMAT_DATA,
    VIDEO_FRAME_DATA,
    AUDIO_FRAME_DATA,
    LIKE_DATA,
    CHUNK_READY_DATA
} BLAB_PACKET_TYPE;

typedef enum {
    COMMAND_ACK,
    HOLD_GROUP_LIVESTREAM,
    ENTER_GROUP_LIVESTREAM,
    LEAVE_GROUP_LIVESTREAM,
    UPLOAD_VIDEO_FRAME_DATA,
    UPLOAD_AUDIO_FRAME_DATA,
    
    SEND_COMMENT_ACTION,
    SEND_LIKE_ACTION,
    BLOCK_USER_ACTION,
    REQUEST_JOIN_ACTION,
    REJECT_JOIN_ACTION,
    CANCEL_JOIN_ACTION,
    ACCEPT_JOIN_ACTION,
    
    LOCK_SEAT_ACTION,
    UNLOCK_SEAT_ACTION,
    KICK_OUT_ACTION,
    QUIT_BROADCAST_ACTION,
    LIVESTREAM_ENDED
} BLAB_ACTION_TYPE;

typedef enum {
    GROUP_HOSTER,
    GROUP_PARTICIPANT,
    GROUP_WATCHER,
} BLAB_USER_STATUS;

typedef NS_ENUM(NSInteger, GroupLiveChannelPosition) {
    GroupLiveChannelPositionLeftTop = 1,
    GroupLiveChannelPositionRightTop,
    GroupLiveChannelPositionLeftBottom,
    GroupLiveChannelPositionRightBottom
};

#endif
