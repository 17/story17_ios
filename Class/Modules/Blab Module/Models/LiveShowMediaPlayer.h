//
//  LiveShowMediaPlayer.h
//  story17
//
//  Created by POPO Chen on 9/15/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PCMAUAudioPlayer.h"
#import "PCMAudioRecorder.h"
#import "MyOpusEncoder.h"
#import "ChunkDataObject.h"
#import "MyScene.h"
#import "MyFilter.h"
#import "Constant.h"
#import "VP8VideoDecoder.h"
#import "VP8VideoEncoder.h"
#import "UDPSocket.h"
#import "MyFilter.h"
#import "GroupLiveConstants.h"

#import "GroupVideoFrame.h"

@protocol LiveShowMediaPlayerDelegate <NSObject>

@optional

-(void)sendVideoEncodedData:(NSData*)data videoSequence:(u_int8_t)videoSequence absTime:(int32_t)absTime channelID:(u_int8_t)channel;
-(void)sendAudioEncodedData:(NSData*)data audioSequence:(u_int8_t)audioSequence absTime:(int32_t)absTime channelID:(u_int8_t)channel;
-(void)playVideoFromSelf:(NSData*)data;

@end

@interface LiveShowMediaPlayer : NSObject <AudioRecorderDelegate,AVCaptureVideoDataOutputSampleBufferDelegate>

@property (nonatomic) BLAB_USER_STATUS userStatus;

//camera setting
@property BOOL isFrontCamera;
@property BOOL isTorchOn;
@property GroupLiveChannelPosition channelID;
@property (nonatomic) BOOL audioOnly;
@property (nonatomic, weak) id<LiveShowMediaPlayerDelegate> delegate;
@property (nonatomic, strong) GLKView* glVideoOutputView;
@property int32_t liveStreamStartTime;
@property (nonatomic) float duration;

-(void) playVideoFrame:(GroupVideoFrame*)videoFrame;
-(void) playvideo:(NSData* )encodedData;
-(void) setupCamera;
-(void) endLivestream;
-(void) calDuration;

@end