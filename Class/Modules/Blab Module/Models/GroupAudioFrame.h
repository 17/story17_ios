//
//  GroupAudioFrame.h
//  story17
//
//  Created by POPO Chen on 9/14/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GroupAudioFrame : NSObject

@property (nonatomic, strong) NSData* audioEncodedData;
@property int32_t commandSequence;
@property int32_t packetLength;
@property int32_t jsonLength;
@property (nonatomic, strong) NSDictionary* jsonDict;
@property int32_t audioDataLength;
@property u_int8_t channelID;
@property u_int8_t audioFrameNum;
@property int32_t audioAbsTime;
@property int32_t serverAbsTime;
@property (nonatomic, strong) NSData* audioDecodedData;

+(GroupAudioFrame*)getGroupAudioFrameFromData:(NSData*)data;

@end
