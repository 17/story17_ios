//
//  CommentObject.h
//  story17
//
//  Created by POPO Chen on 4/30/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TcpObject : NSObject

@property (nonatomic,strong) NSString* userID;
@property (nonatomic,strong) NSString* picture;
@property (nonatomic,strong) NSString* comment;
@property int timestamp;

@end
