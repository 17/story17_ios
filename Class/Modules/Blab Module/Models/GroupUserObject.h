//
//  GroupUserObject.h
//  Story17
//
//  Created by POPO on 2015/9/21.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

@interface GroupUserObject : NSObject
@property (nonatomic,strong) NSString* userID;
@property (nonatomic,strong) NSString* openID;
@property (nonatomic,strong) NSString* picture;
@end

