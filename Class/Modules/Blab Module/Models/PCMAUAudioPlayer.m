//
//  PCMAUAudioPlayer.m
//  Story17
//
//  Created by POPO on 10/7/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "PCMAUAudioPlayer.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVAudioFormat.h>
#import "TPCircularBuffer+AudioBufferList.h"
#import "AudioFrameObject.h"
#import "MyOpusDecoder.h"

#import "Constant.h"

@interface PCMAUAudioPlayer()

#define BUS_COUNT 4
#define BUF_CNT 30
#define BUF_FRAME 1920
#define OPUS_FRAME_SIZE sizeof(opus_int16)
#define BUF_SIZE BUF_CNT*BUF_FRAME*OPUS_FRAME_SIZE

typedef struct {
    AudioStreamBasicDescription asbd;
    AudioBufferList audioBuffer;
    TPCircularBuffer audioBuffers[BUS_COUNT];
    AudioTimeStamp playTime;
} SoundBuffer, *SoundBufferPtr;

//@property (nonatomic, strong) AVAudioFormat *mAudioFormat;
//@property (nonatomic) AUGraph   mGraph;
//@property (nonatomic) AudioUnit mMixer;
//@property (nonatomic) AudioUnit mOutput;
@property (nonatomic) BOOL isFirstPlay;
@property (nonatomic) int startTime;
@property (nonatomic, strong) NSMutableArray* audioFrame;

// audio decode
@property dispatch_queue_t audioDecodeQueue;
@property (nonatomic, strong) MyOpusDecoder* opusDecoder;

// array to hold Group Audio Frame
@property (nonatomic, strong) NSMutableArray* groupAFs;
@property BOOL isDecoding;

@end


//AVAudioFormat *mAudioFormat;

static PCMAUAudioPlayer* PLAYER = NULL;

static AUGraph   mGraph;
static AudioUnit mMixer;
static AudioUnit mOutput;
static AudioUnit mVpInput;

SoundBuffer mSoundBuffer;

@implementation PCMAUAudioPlayer

#pragma mark- RenderProc
// audio render procedure, don't allocate memory, don't take any locks, don't waste time, printf statements for debugging only may adversly affect render you have been warned
static OSStatus renderInput(void *inRefCon, AudioUnitRenderActionFlags *ioActionFlags, const AudioTimeStamp *inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList *ioData)
{
    SoundBufferPtr sndbuf = (SoundBufferPtr)inRefCon;
    
#ifdef TPCircular
    int32_t availableBytes;
    opus_int16 *buffer = TPCircularBufferTail(&(sndbuf->audioBuffers[inBusNumber]), &availableBytes);
    
    int bytesToCopy = ioData->mBuffers[0].mDataByteSize;
    opus_int16 *targetBuffer = (opus_int16*)ioData->mBuffers[0].mData;
    
    if (buffer && availableBytes>0) {
        if (!buffer) {
            DLog(@"TPCircularBufferTail fail");
        }
        
//        InfoLog(@"bus=%d, Tsample=%f, frames=%d, bytesToCopy=%d, availableBytes=%d", inBusNumber, inTimeStamp->mSampleTime, inNumberFrames, bytesToCopy, availableBytes);

        memcpy(targetBuffer, buffer, bytesToCopy);
        TPCircularBufferConsume(&(sndbuf->audioBuffers[inBusNumber]), MIN(availableBytes, bytesToCopy));
        
        InfoLog(@"total bytes is %d, read %d", availableBytes, bytesToCopy);
        
        mSoundBuffer.playTime = *inTimeStamp;
        [PLAYER.delegate didPlayAudioAtAbsTime:(int)inTimeStamp->mHostTime numberOfAudioBufferInQueue:availableBytes/3840];
    } else {
        if (inBusNumber == 0) {
            *ioActionFlags |= kAudioUnitRenderAction_OutputIsSilence;
            memset(ioData->mBuffers[0].mData, 0, ioData->mBuffers[0].mDataByteSize);
        }
    }
#else
//    if (inBusNumber == 0) { // testing for just one bus
        NSMutableData *data = (__bridge NSMutableData *)(sndbuf->audioBuffer.mBuffers[inBusNumber].mData);
        if (data.length > 0) {
//            DLog(@"inBusNumber %u, count = %u", (unsigned int)inBusNumber, (unsigned int)ioData->mNumberBuffers);

            AudioBuffer buffer = ioData->mBuffers[0];
            
            UInt32 byte = MIN((UInt32)data.length, buffer.mDataByteSize);
            [data getBytes:buffer.mData length:byte];
            [data replaceBytesInRange:NSMakeRange(0, byte) withBytes:NULL length: 0];
            
            if (mSoundBuffer.playTime.mSampleTime != 0) {
                PLAYER.startTime+=(int)((byte/2/1920.f)*40);
            }
            mSoundBuffer.playTime = *inTimeStamp;
            [PLAYER.delegate didPlayAudioinChannel:inBusNumber+1 atAbsTime:PLAYER.startTime numberOfAudioBufferInQueue:(int)data.length/3840];

//            [PLAYER.delegate didPlayAudioinChannel:inBusNumber atAbsTimestamp:PLAYER.startTime numberOfAudioBufferInQueue:(int)data.length/3840];
        } else {
            *ioActionFlags |= kAudioUnitRenderAction_OutputIsSilence;
            memset(ioData->mBuffers[0].mData, 0, ioData->mBuffers[0].mDataByteSize);
        }
//    }
#endif
    return noErr;
}

- (id) init {
    
    self = [super init];
    if (!self) {
        return nil;
    }
    
    PLAYER = self;
    
    //
    // set the format for the graph
    mSoundBuffer.asbd.mSampleRate = 48000.0;
    mSoundBuffer.asbd.mFormatID = kAudioFormatLinearPCM;
    mSoundBuffer.asbd.mChannelsPerFrame = 1;
    mSoundBuffer.asbd.mFramesPerPacket = 1;
    mSoundBuffer.asbd.mBytesPerPacket = 2;
    mSoundBuffer.asbd.mBitsPerChannel = 16;
    mSoundBuffer.asbd.mFormatFlags = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
    mSoundBuffer.asbd.mBytesPerFrame = 2;
    
    //
    // audio
    _opusDecoder = [MyOpusDecoder new];
    _audioDecodeQueue = dispatch_queue_create("audio_decode_queue", DISPATCH_QUEUE_SERIAL);
    
    return self;
}

-(void) checkResult:(OSStatus) result withMessage:(NSString*) msg
{
    if (result != noErr) {
        DLog("%@ result %ld %08lX %4.4s\n", msg, (long)result, (long)result, (char*)&result);
        [NSException raise:@"OSStatusCheck"
                    format:@"Checking the return code of an operation failed: '%@'", msg];
    }
}

-(void) setup
{
    //
    // set buffers to hold audio data
#ifdef TPCircular
    for (int i=0; i<BUS_COUNT; i++) {
        TPCircularBufferInit(&(mSoundBuffer.audioBuffers[i]), BUF_SIZE);
    }
#else
    mSoundBuffer.audioBuffer.mNumberBuffers = BUS_COUNT;
    for (int i=0; i<BUS_COUNT; i++) {
        NSMutableData* audioDataToPlay = CREATE_MUTABLE_DATA;
        mSoundBuffer.audioBuffer.mBuffers[i].mNumberChannels = 1;
        mSoundBuffer.audioBuffer.mBuffers[i].mData = (void * _Nullable)CFBridgingRetain(audioDataToPlay);
//        InfoLog(@"bus %d, at %p", i, mSoundBuffer.audioBuffer.mBuffers[i].mData);
    }
#endif
    
    //
    // create a new AUGraph
    [self checkResult:NewAUGraph(&mGraph) withMessage:@"NewAUGraph"];
    
    //
    // create AudioUnit Nodes we want in the graph
    //
    
    //
    // output unit
    AUNode ioNode;
    AudioComponentDescription ioUnitDescription;
    ioUnitDescription.componentType = kAudioUnitType_Output;
    ioUnitDescription.componentSubType = kAudioUnitSubType_RemoteIO;
    ioUnitDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    ioUnitDescription.componentFlags = 0;
    ioUnitDescription.componentFlagsMask = 0;
    [self checkResult:AUGraphAddNode(mGraph, &ioUnitDescription, &ioNode) withMessage:@"AUGraphNewNode IO"];
    
    //
    // multichannel mixer unit
    AUNode mixerNode;
    AudioComponentDescription mixerUnitDescription;
    mixerUnitDescription.componentType = kAudioUnitType_Mixer;
    mixerUnitDescription.componentSubType = kAudioUnitSubType_MultiChannelMixer;
    mixerUnitDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    mixerUnitDescription.componentFlags = 0;
    mixerUnitDescription.componentFlagsMask = 0;
    [self checkResult:AUGraphAddNode(mGraph, &mixerUnitDescription, &mixerNode) withMessage:@"AUGraphNewNode Mixer"];
    
    // video-processing unit
//    AUNode vpNode;
//    AudioComponentDescription vpUnitDescription;
//    vpUnitDescription.componentType = kAudioUnitType_Output;
//    vpUnitDescription.componentSubType = kAudioUnitSubType_VoiceProcessingIO;
//    vpUnitDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
//    vpUnitDescription.componentFlags = 0;
//    vpUnitDescription.componentFlagsMask = 0;
//    result = AUGraphAddNode(mGraph, &vpUnitDescription, &vpNode);
//    if (result) {
//        DLog("AUGraphNewNode VPIO result %ld %4.4s\n", (long)result, (char*)&result);
//        return;
//    }
    
    //
    // connect nodes
    [self checkResult:AUGraphConnectNodeInput(mGraph, mixerNode, 0, ioNode, 0) withMessage:@"AUGraphConnectNodeInput"];
    
    // open the graph AudioUnits are open but not initialized (no resource allocation occurs here)
    [self checkResult:AUGraphOpen(mGraph) withMessage:@"AUGraphOpen"];
    [self checkResult:AUGraphNodeInfo(mGraph, mixerNode, NULL, &mMixer) withMessage:@"AUGraphNodeInfo mixer"];
    [self checkResult:AUGraphNodeInfo(mGraph, ioNode, NULL, &mOutput) withMessage:@"AUGraphNodeInfo remoteIO"];
    
//    result = AUGraphNodeInfo(mGraph, vpNode, NULL, &mVpInput);
//    if (result) {
//        DLog("AUGraphNodeInfo result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result);
//        return;
//    }

    //
    // set bus count
    DLog("set mixer node property, input callback");
    UInt32 numbuses = BUS_COUNT;
    [self checkResult:AudioUnitSetProperty(mMixer, kAudioUnitProperty_ElementCount, kAudioUnitScope_Input, 0, &numbuses, sizeof(numbuses)) withMessage:@"AudioUnitSetProperty elementCount"];
    
    for (int i = 0; i < numbuses; ++i) {
        // setup render callback struct
        AURenderCallbackStruct rcbs;
        rcbs.inputProc = &renderInput;
        rcbs.inputProcRefCon = &mSoundBuffer;
        
        // Set a callback for the specified node's specified input
        [self checkResult:AUGraphSetNodeInputCallback(mGraph, mixerNode, i, &rcbs) withMessage:@"AUGraphSetNodeInputCallback"];
        
        // set input stream format to what we want
        [self checkResult:AudioUnitSetProperty(mMixer, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, i, &(mSoundBuffer.asbd), sizeof(AudioStreamBasicDescription)) withMessage:@"AudioUnitSetProperty streamFormat"];
    }
    
    //
    // set output stream format to what we want
    [self checkResult:AudioUnitSetProperty(mMixer, kAudioUnitProperty_SampleRate, kAudioUnitScope_Output, 0, &(mSoundBuffer.asbd.mSampleRate), sizeof(Float64)) withMessage:@"AudioUnitSetProperty sampleRate"];
    
//    result = AudioUnitSetProperty(mOutput, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 1, &audioDesc, sizeof(AudioStreamBasicDescription));
//    if (result) {
//        DLog("AudioUnitSetProperty result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result);
//        return;
//    }
    
    //
    // Set maximum frames per slice to 4096 to allow playback during lock screen
    UInt32 maximumFramesPerSlice = 4096;
    [self checkResult:AudioUnitSetProperty(mOutput, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, 0, &maximumFramesPerSlice, sizeof(maximumFramesPerSlice)) withMessage:@"Failed to set maximum frames per slice on mixer node"];
    
    DLog ("AUGraphInitialize\n");
    
    // now that we've set everything up we can initialize the graph, this will also validate the connections
    [self checkResult:AUGraphInitialize(mGraph) withMessage:@"AUGraphInitialize"];
    
    _isFirstPlay = YES;
}

-(void) play:(NSData*)audioData channel:(int)ch absTime:(int)absTime
{
    dispatch_async(MAIN_QUEUE, ^{
        if (_isFirstPlay) {
            AUGraphStart(mGraph);
            CAShow(mGraph);
            _isFirstPlay = NO;
            _startTime = absTime;
        }
        
#ifdef TPCircular
        int32_t availableBytes, writeBytes;
        opus_int16 *input = (opus_int16*)TPCircularBufferHead(&(mSoundBuffer.audioBuffers[ch-1]), &availableBytes);
        writeBytes = MIN(availableBytes, (int32_t)audioData.length);
        
        TPCircularBufferTail(&(mSoundBuffer.audioBuffers[ch]), &availableBytes);
        //    InfoLog(@"total bytes %d, write %d, should write %ld", availableBytes, writeBytes, audioData.length);
        
        if (input) {
            [audioData getBytes:input length:writeBytes];
            TPCircularBufferProduce(&(mSoundBuffer.audioBuffers[ch-1]), writeBytes);
        } else {
            return;
        }
#else
        NSMutableData* data = (__bridge NSMutableData *)(mSoundBuffer.audioBuffer.mBuffers[ch-1].mData);
        [data appendData:audioData];
#endif
    });
}

-(void) playEncodedAudioData:(NSData*)audioData inChannel:(int)ch atAbsTime:(int)absTime
{
    if (_isFirstPlay) {
        AUGraphStart(mGraph);
        CAShow(mGraph);
        _isFirstPlay = NO;
        _startTime = absTime;
    }
    
    dispatch_async(_audioDecodeQueue, ^{
        NSData* decodedAudioData = [_opusDecoder decodeAudioWithData:audioData];
        [self play:decodedAudioData channel:ch absTime:absTime];
    });
}

-(void) play:(NSData*)audioData absTime:(int)absTime
{
    [self play:audioData channel:1 absTime:absTime];
}

-(void) playAudioFrame:(AudioFrameObject*)aF
{
    [self play:aF.audioData channel:1 absTime:aF.absTimestamp];
}

-(void) playGroupAudioFrame:(GroupAudioFrame*)gAF
{
    dispatch_async(_audioDecodeQueue, ^{
        NSData* decodedAudioData = [_opusDecoder decodeAudioWithData:gAF.audioEncodedData];
        [self play:decodedAudioData channel:gAF.channelID absTime:gAF.audioAbsTime];
    });
}

-(void) syncAudio
{
    
}

-(void) playSilence
{
    
}

-(void) reset
{
//    AUGraphStop(mGraph);
}

-(void) cleanup
{
    AUGraphStop(mGraph);
    DisposeAUGraph(mGraph);
    
    for (int i=0; i<BUS_COUNT; i++) {
//        CFRelease(mSoundBuffer.audioBuffer.mBuffers[i].mData);
    }
}

-(void) dealloc
{
    
}

@end
