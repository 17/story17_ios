//
//  LiveShowMediaPlayer.m
//  story17
//
//  Created by POPO Chen on 9/15/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "LiveShowMediaPlayer.h"

@interface LiveShowMediaPlayer()

/*  GROUP_HOSTER  GROUP_PARTICIPANT */

@property PCMAudioRecorder* audioRecorder;
@property VP8VideoEncoder* videoEncoder;
@property (nonatomic,strong) MyOpusEncoder* opusEncoder;
@property BOOL isEncoding;


@property BOOL isFirstStart;

/*  GROUP_WATCHER */
@property dispatch_queue_t videoDecodeQueue;

@property VP8VideoDecoder* videoDecoder;

@property unsigned char* yDataBuffer;
@property unsigned char* cbDataBuffer;
@property unsigned char* crDataBuffer;

@property (nonatomic, strong) NSDate* livestreamStartDate;
//@property int32_t commandSequence;

@property (nonatomic) int TargetFps;
@property (nonatomic,strong) NSTimer* UpFpsTimer;
@property (nonatomic) int FPSTimerInterval;
@property (nonatomic) long lateDataCount;
@property (nonatomic,strong) NSTimer* DownFpsTimer;
@property (nonatomic,strong) NSTimer* PhoneCallTimer;
@property (nonatomic) long limitlateDataCount;

@property dispatch_queue_t socketQueue;
@property dispatch_queue_t videoEncodeQueue;
@property dispatch_queue_t audioEncodeQueue;

@property u_int8_t videoSequenceNumber;
@property u_int8_t audioSequenceNumber;

@property (nonatomic, strong) MyFilter* displayFilter;
@property (nonatomic, strong) NSArray* filters;

/* Capture Session */
@property (nonatomic, strong) AVCaptureSession* captureSession;
@property (nonatomic, strong) AVCaptureDeviceInput *inputDevice;
@property (nonatomic, strong) AVCaptureDevice *captureDevice;
@property (nonatomic, strong) AVCaptureVideoDataOutput *videoDataOutput;
@property (nonatomic, strong) NSMutableArray* audioDataArray;

@end

@implementation LiveShowMediaPlayer

/* Chunking Params */
#define NUM_OF_AUDIO_SAMPLES_PER_CHUNK 20

-(id)init
{
    self = [super init];
    if(self) {
        
        _isFirstStart = YES;

        _audioDataArray = CREATE_MUTABLE_ARRAY;
        _liveStreamStartTime = CURRENT_TIMESTAMP;

        _videoDecodeQueue = dispatch_queue_create("live_stream_video_decode_queue", DISPATCH_QUEUE_SERIAL);
        
        _videoSequenceNumber = 0;
        _audioSequenceNumber = 0;
        
        _isFrontCamera = NO;
        _TargetFps=30;
        _FPSTimerInterval=15;
        
        if (IS_IPHONE_4) {
            _glVideoOutputView = [[GLKView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, SCREEN_WIDTH/2)];
        } else {
            _glVideoOutputView = [[GLKView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, SCREEN_WIDTH/2)];
        }
        
        /* audio */
        _videoDecoder = [VP8VideoDecoder new];
        _opusEncoder = [MyOpusEncoder new];
        _videoEncoder = [VP8VideoEncoder new];

        /* init broad caster */
        _yDataBuffer = malloc(FRAME_WIDTH*FRAME_HEIGHT);
        _cbDataBuffer = malloc(FRAME_WIDTH*FRAME_HEIGHT/4);
        _crDataBuffer = malloc(FRAME_WIDTH*FRAME_HEIGHT/4);
        DLog(@"init");
        /* queue */
        _socketQueue = dispatch_queue_create("live_stream_socket_queue", DISPATCH_QUEUE_SERIAL);
        _videoEncodeQueue = dispatch_queue_create("live_stream_video_encode_queue", DISPATCH_QUEUE_SERIAL);
        _audioEncodeQueue = dispatch_queue_create("audio_encode_queue", DISPATCH_QUEUE_SERIAL);
    }
    
    return self;
}

-(void)setUserStatus:(BLAB_USER_STATUS)userStatus
{
    if(_isFirstStart){
        
        _isFirstStart = NO;
        _userStatus = userStatus;
        
        if(_userStatus==GROUP_WATCHER){
            _displayFilter = [[MyFilter alloc] initWithImageSize:CGSizeMake(640, 360) filterProgram:FILTER_PROGRAM_SIMPLE];
            [_displayFilter setParameter:@"imageSourceInputType" intValue:1];  // for Watcher Mode
            [_displayFilter setRotation:180.0 needHorizontalFlip:1];
            
            _filters = @[NORMAL_FILTER, RICH_FILTER, WARM_FILTER, SOFT_FILTER, ROSE_FILTER, MORNING_FILTER, SUNSHINE_FILTER, SUNSET_FILTER, COOL_FILTER, FREEZE_FILTER, OCEAN_FILTER, DREAM_FILTER, VIOLET_FILTER, MELLOW_FILTER, BLEAK_FILTER, MEMORY_FILTER, PURE_FILTER, CALM_FILTER, AUTUMN_FILTER, FANTASY_FILTER, FREEDOM_FILTER, MILD_FILTER, PRAIRIE_FILTER, DEEP_FILTER, GLOW_FILTER, MEMOIR_FILTER, MIST_FILTER, VIVID_FILTER, CHILL_FILTER, PINKY_FILTER, ADVENTURE_FILTER];
            
            _glVideoOutputView.drawableColorFormat = GLKViewDrawableColorFormatRGB565;
            _glVideoOutputView.drawableDepthFormat = GLKViewDrawableDepthFormatNone;
            _glVideoOutputView.drawableMultisample = GLKViewDrawableMultisampleNone;
            _glVideoOutputView.context = _displayFilter.glContext;
            _glVideoOutputView.contentScaleFactor = 1.0;
            [_glVideoOutputView setEnableSetNeedsDisplay:NO];

            [_displayFilter setParameter:@"imageSourceInputType" intValue:1];
        }
        
        if(_userStatus==GROUP_HOSTER || _userStatus == GROUP_PARTICIPANT){
            _displayFilter = [[MyFilter alloc] initWithImageSize:CGSizeMake(640, 360) filterProgram:FILTER_PROGRAM_SIMPLE_SQUARE];
            [_displayFilter setParameter:@"imageSourceInputType" intValue:1];  // for Watcher Mode
            [_displayFilter setRotation:180.0 needHorizontalFlip:1];
            
            _filters = @[NORMAL_FILTER, RICH_FILTER, WARM_FILTER, SOFT_FILTER, ROSE_FILTER, MORNING_FILTER, SUNSHINE_FILTER, SUNSET_FILTER, COOL_FILTER, FREEZE_FILTER, OCEAN_FILTER, DREAM_FILTER, VIOLET_FILTER, MELLOW_FILTER, BLEAK_FILTER, MEMORY_FILTER, PURE_FILTER, CALM_FILTER, AUTUMN_FILTER, FANTASY_FILTER, FREEDOM_FILTER, MILD_FILTER, PRAIRIE_FILTER, DEEP_FILTER, GLOW_FILTER, MEMOIR_FILTER, MIST_FILTER, VIVID_FILTER, CHILL_FILTER, PINKY_FILTER, ADVENTURE_FILTER];
            
            _glVideoOutputView.drawableColorFormat = GLKViewDrawableColorFormatRGB565;
            _glVideoOutputView.drawableDepthFormat = GLKViewDrawableDepthFormatNone;
            _glVideoOutputView.drawableMultisample = GLKViewDrawableMultisampleNone;
            _glVideoOutputView.context = _displayFilter.glContext;
            _glVideoOutputView.contentScaleFactor = 1.0;
            [_glVideoOutputView setEnableSetNeedsDisplay:NO];

            [_displayFilter setParameter:@"imageSourceInputType" intValue:2];
            [self reloadCaptureSession];
            [self setupAudioRecorder];
            [self startAudioRecorder];
        }
    }else{
    
        if(_userStatus == GROUP_WATCHER && userStatus == GROUP_HOSTER){
            return;
        }
        if(_userStatus == GROUP_WATCHER && userStatus == GROUP_PARTICIPANT){
            [_displayFilter setParameter:@"imageSourceInputType" intValue:2];
            [self reloadCaptureSession];
            [self setupAudioRecorder];
            [self startAudioRecorder];
        }
        if(_userStatus == GROUP_PARTICIPANT && userStatus == GROUP_WATCHER){
            
            /* Close Capture Session */
            [_captureSession stopRunning];
//            [self cleanup];
            [_displayFilter setParameter:@"imageSourceInputType" intValue:1];
        }
        _userStatus = userStatus;
    }
}

# pragma mark - AVCaptureDataOutputSampleBufferDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{

    if(_userStatus==GROUP_WATCHER){
        return;
    }
    
    if (_audioOnly) {
        return;
    }
    
    if(_userStatus==GROUP_HOSTER || _userStatus == GROUP_PARTICIPANT){
        
        CVPixelBufferRef pixelBuffer = (CVPixelBufferRef) CMSampleBufferGetImageBuffer(sampleBuffer);
        [_displayFilter loadYUVData:pixelBuffer];
        
        [_glVideoOutputView bindDrawable];
        
        _displayFilter.targetViewPortSize = CGSizeMake(_glVideoOutputView.drawableWidth, _glVideoOutputView.drawableHeight);

        if(_isFrontCamera) {
            [_displayFilter setRotation:180.0 needHorizontalFlip:0];
        } else {
            [_displayFilter setRotation:180.0 needHorizontalFlip:1];
        }
        
        [_displayFilter rendernew:RENDER_MODE_RGBA rebindFrameBuffer:0];
        if (_audioOnly) {
            return;
        }
        if (!_delegate) {
            return;
        }
        [_glVideoOutputView display];
        
        /* For sending data */
        _displayFilter.targetViewPortSize = CGSizeMake(640, 360);
        
        if(_isFrontCamera) {
            [_displayFilter setRotation:90.0 needHorizontalFlip:0];
        } else {
            [_displayFilter setRotation:90.0 needHorizontalFlip:0];
        }
        
        [_displayFilter render:RENDER_MODE_YUV rebindFrameBuffer:1];
        
        int value = arc4random() % 30;
        
        
        if (value>_TargetFps)
        {
            return;
        }
        
        if([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive){
            return;
        }
        
        
        if(_isEncoding) {
            return;
            
        }
        
        _isEncoding = YES;

        // load data for video encoding
        dispatch_async(_videoEncodeQueue, ^{

            [_displayFilter loadYData:_yDataBuffer cbData:_cbDataBuffer crData:_crDataBuffer isSquare:0];
            NSData* encodedData = [_videoEncoder encodeWithYData:_yDataBuffer andCbData:_cbDataBuffer andCrData:_crDataBuffer];

            if(encodedData==nil || encodedData.length==0) {
                dispatch_async(MAIN_QUEUE, ^{
                    _isEncoding = NO;
                });
                return;
            }

            dispatch_async(MAIN_QUEUE, ^{
                _videoSequenceNumber++;
                [_delegate sendVideoEncodedData:encodedData videoSequence:_videoSequenceNumber absTime:[self getLiveStreamAbsTime] channelID:_channelID];
//                [_delegate playVideoFromSelf:encodedData];
                _isEncoding = NO;
            });
        });
        
    }
}

-(int32_t) getLiveStreamAbsTime
{
    if(_livestreamStartDate==nil) {
        _livestreamStartDate = [NSDate date];
    }
    return (int) ([_livestreamStartDate timeIntervalSinceNow] * -1000.0);
}

# pragma mark - AudioRecorderDelegate
-(void)didOutputAudioData:(NSData *)audioData
{
    
    if(_userStatus==GROUP_WATCHER) {
        return;
    }
    
    if(_userStatus==GROUP_HOSTER || _userStatus == GROUP_PARTICIPANT){
        
        dispatch_async(_audioEncodeQueue, ^{
            
            NSData* encodedAudioData = [_opusEncoder encodeAudioWithData:audioData];
            
            dispatch_async(MAIN_QUEUE, ^{
                
                _audioSequenceNumber++;
                [_delegate sendAudioEncodedData:encodedAudioData audioSequence:_audioSequenceNumber absTime:[self getLiveStreamAbsTime] channelID:_channelID];
                
            });
        });
    }
}

-(void) playVideoFrame:(GroupVideoFrame*)videoFrame
{
    
    dispatch_async(_videoDecodeQueue, ^{

        if([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive){
            return;
        }
        
        videoFrame.yuvData = [_videoDecoder decodeWithData:videoFrame.videoEncodedData dataLength:(int) videoFrame.videoEncodedData.length cropToSquare:YES discardNonIFrame:NO];
        NSArray* yuvData = videoFrame.yuvData;
        
        // 360 x 360
        [_displayFilter loadLuminanceData:((NSData*) yuvData[0]).bytes textureSize:CGSizeMake(FRAME_HEIGHT, FRAME_HEIGHT) textureName:@"yTexture"];
        [_displayFilter loadLuminanceData:((NSData*) yuvData[1]).bytes textureSize:CGSizeMake(FRAME_HEIGHT/2, FRAME_HEIGHT/2) textureName:@"vTexture"];
        [_displayFilter loadLuminanceData:((NSData*) yuvData[2]).bytes textureSize:CGSizeMake(FRAME_HEIGHT/2, FRAME_HEIGHT/2) textureName:@"uTexture"];
        
        _displayFilter.targetViewPortSize = CGSizeMake(_glVideoOutputView.drawableWidth, _glVideoOutputView.drawableHeight);
        [_displayFilter render:RENDER_MODE_RGBA rebindFrameBuffer:0];
        if (!_delegate) {
            return;
        }
        DLog(@"videoFrame.videoAbsTime = %d", videoFrame.videoAbsTime);
        [_glVideoOutputView display];

    });
}

-(void) playvideo:(NSData* )encodedData
{
    
    dispatch_async(_videoDecodeQueue, ^{
        
        if([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive){
            return;
        }
        
        NSArray* yuvData = [_videoDecoder decodeWithData:encodedData dataLength:(int) encodedData.length cropToSquare:YES discardNonIFrame:NO];
;
        
        // 360 x 360
        [_displayFilter loadLuminanceData:((NSData*) yuvData[0]).bytes textureSize:CGSizeMake(FRAME_HEIGHT, FRAME_HEIGHT) textureName:@"yTexture"];
        [_displayFilter loadLuminanceData:((NSData*) yuvData[1]).bytes textureSize:CGSizeMake(FRAME_HEIGHT/2, FRAME_HEIGHT/2) textureName:@"vTexture"];
        [_displayFilter loadLuminanceData:((NSData*) yuvData[2]).bytes textureSize:CGSizeMake(FRAME_HEIGHT/2, FRAME_HEIGHT/2) textureName:@"uTexture"];
        
        _displayFilter.targetViewPortSize = CGSizeMake(_glVideoOutputView.drawableWidth, _glVideoOutputView.drawableHeight);
        [_displayFilter render:RENDER_MODE_RGBA rebindFrameBuffer:0];
        if (!_delegate) {
            return;
        }
        [_glVideoOutputView display];
        
    });
}


-(void) setupAudioRecorder
{    
    if(_audioRecorder!=nil) {
        return;
    }
    
    [self stopAudioRecorder];
    
    _audioRecorder = [[PCMAudioRecorder alloc] initWithMode:PCM_RECORDER_MODE_OPUS];
    _audioRecorder.delegate = self;
}

-(void) startAudioRecorder
{
    if(_audioRecorder!=nil) {
        
        [_audioRecorder startRecording];
    }
}

-(void) stopAudioRecorder
{
    if(_audioRecorder!=nil) {
        [_audioRecorder stopRecording];
        _audioRecorder = nil;
    }
}

-(void) reloadCaptureSession
{
    if([_captureSession isRunning]) {
        [_captureSession stopRunning];
    }
    
    _captureSession = [[AVCaptureSession alloc] init];
    [_captureSession setSessionPreset:AVCaptureSessionPreset1280x720];
    
    _videoDataOutput = [[AVCaptureVideoDataOutput alloc]init];
    _videoDataOutput.videoSettings=[NSDictionary dictionaryWithObject: [NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange] forKey: (id)kCVPixelBufferPixelFormatTypeKey];
    [_videoDataOutput setSampleBufferDelegate:self queue:MAIN_QUEUE];
    [_captureSession addOutput:_videoDataOutput];
    
    [self setupCamera];
}

-(void) setupCamera
{
    [_captureSession stopRunning];
        
    if(self.inputDevice) {
        [_captureSession removeInput:self.inputDevice];
    }
    
    NSArray *devices = [AVCaptureDevice devices];
    AVCaptureDevice *frontCamera;
    AVCaptureDevice *backCamera;
    
    for (AVCaptureDevice *device in devices) {
        if ([device hasMediaType:AVMediaTypeVideo]) {
            if ([device position] == AVCaptureDevicePositionBack) {
                backCamera = device;
            }  else {
                frontCamera = device;
            }
        }
    }
    
    NSError *error = nil;
    

    
    if (_isFrontCamera) {
        _captureDevice = frontCamera;
        
        AVCaptureDeviceInput *frontFacingCameraDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:frontCamera error:&error];
        if (!error) {
            if ([_captureSession canAddInput:frontFacingCameraDeviceInput]) {
                [_captureSession addInput:frontFacingCameraDeviceInput];
                self.inputDevice = frontFacingCameraDeviceInput;
            }
        }
    } else {
        _captureDevice = backCamera;
        
        AVCaptureDeviceInput *backFacingCameraDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:backCamera error:&error];
        if (!error) {
            if ([_captureSession canAddInput:backFacingCameraDeviceInput]) {
                [_captureSession addInput:backFacingCameraDeviceInput];
                self.inputDevice = backFacingCameraDeviceInput;
            }
        }
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_captureSession startRunning];
    });
}

-(void) cleanup
{
    [NOTIFICATION_CENTER removeObserver:self];

    [_captureSession stopRunning];
    [self stopAudioRecorder];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        DLog(@"free");
        free(_yDataBuffer);
        free(_cbDataBuffer);
        free(_crDataBuffer);
    });
}

-(void)calDuration
{
    _duration=CURRENT_TIMESTAMP-_liveStreamStartTime;
}

-(void)endLivestream
{
    [self cleanup];
}

@end
