//
//  ChunkReadyObject.h
//  story17
//
//  Created by POPO Chen on 10/16/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChunkReadyObject : NSObject

@property int32_t groupLiveStreamID;
@property int32_t chunkID;
@property int32_t prefetchTargetChunkID;
@property int32_t latestChunkID;

+(ChunkReadyObject*)getChunkReadyData:(NSData*)data;

@end
