//
//  GroupTcpConnection.h
//  story17
//
//  Created by POPO Chen on 9/14/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncSocket.h"
#import "GroupVideoFrame.h"
#import "GroupAudioFrame.h"
#import "GroupLiveConstants.h"
#import "ChunkReadyObject.h"

@protocol GroupTcpConnectionDelegate <NSObject>
-(void)didReceiveVideoData:(GroupVideoFrame*)videoFrameObj;
-(void)didReceiveAudioData:(GroupAudioFrame*)audioFrameObj;
-(void)didReceiveChunkReadyData:(ChunkReadyObject*)chunkObj;
-(void)didReceiveJsonFormatData:(BLAB_ACTION_TYPE)type infoDict:(NSDictionary*)infoDict;
-(void)didConnectToHost:(BOOL)isConnect;
@end

@interface GroupTcpConnection : NSObject<GCDAsyncSocketDelegate>

@property (nonatomic, strong) NSString* groupLiveStreamID;
@property (nonatomic, strong) GCDAsyncSocket* tcpSocket;
@property (nonatomic, weak) id<GroupTcpConnectionDelegate> delegate;
@property (nonatomic, strong) NSMutableData* receivedData;
@property int32_t commandSequence;

/* Start | Stop Connection */
-(id)init:(NSString*)groupLivestreamID;
-(void) startTcpConn;
-(void) cleanupSocket;

/* Send Media Data */
-(void)sendAudioData:(NSData*)encodedData audioSequence:(u_int8_t)audioSequence absTime:(int32_t)absTime channelID:(u_int8_t)channel;
-(void)sendVideoData:(NSData*)encodedData videoSequence:(u_int8_t)videoSequence absTime:(int32_t)absTime channelID:(u_int8_t)channel;

/* GroupLive Send Info Action*/
-(void)holdLiveStream;
-(void)enterLiveStream;
-(void)leaveLiveStream:(int)duration;
-(void)sendCommentAction:(NSString*)openID picture:(NSString*)picture message:(NSString*)message colorCode:(NSString*)colorCode;
-(void)blockUserAction:(NSString*)blockUserID;
-(void)requestJoinAction:(NSString*)openID picture:(NSString*)picture;
-(void)rejectJoinAction:(NSString*)blockUserID;
-(void)cancelJoinAction:(NSString*)userID picture:(NSString*)picture;
-(void)acceptJoinAction:(NSString*)blockUserID channelID:(int)channel;
-(void)lockSeatAction:(int)lockNums;
-(void)unlockSeatAction:(int)lockNums;
-(void)kickOutAction:(NSString*)userID;
-(void)quitBroadCaterAction:(int)channel picture:(NSString*)picture;
-(void)livestreamEndedAction;

@end
