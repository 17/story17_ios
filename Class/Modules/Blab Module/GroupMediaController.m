//
//  GroupMediaController.m
//  Story17
//
//  Created by POPO on 10/28/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "GroupMediaController.h"

#import "Constant.h"

@interface GroupMediaController()

@property (nonatomic, strong) PCMAUAudioPlayer* audioPlayer;
@property (nonatomic, strong) NSMutableArray* groupVFChannelContainer;

@end

@implementation GroupMediaController

#pragma mark - public method
- (void)start
{
    _groupVFChannelContainer = CREATE_MUTABLE_ARRAY;
    for (int i=0; i<4; i++) {
        NSMutableArray* groupVFs = CREATE_MUTABLE_ARRAY;
        [_groupVFChannelContainer addObject:groupVFs];
    }
    [self startAudioPlayer];
}

- (void)stop
{
    [self stopAudioPlayer];
}

- (void)schedulePlayGroupAudioFrame:(GroupAudioFrame*)gAF
{
    [_audioPlayer playGroupAudioFrame:gAF];
}

- (void)schedulePlayGroupVideoFrame:(GroupVideoFrame*)gVF
{
    NSMutableArray* groupVFs = [_groupVFChannelContainer objectAtIndex:gVF.channelID-1];
    [groupVFs addObject:gVF];
}

#pragma mark - Audio
-(void) didPlayAudioinChannel:(int)ch atAbsTime:(int)absTime numberOfAudioBufferInQueue:(int)numberOfAudioBufferInQueue
{
    DLog(@"play ch %d audio at AbsTimestamp:%d, numberOfAudioBufferInAudioQueue=%d", ch, absTime, numberOfAudioBufferInQueue);
    [self playVideoInChannel:ch atAbsTime:absTime];
}

- (void) stopAudioPlayer
{
    if(_audioPlayer != nil) {
        [_audioPlayer cleanup];
        _audioPlayer.delegate = nil;
        _audioPlayer = nil;
    }
}

- (void) startAudioPlayer
{
    if (_audioPlayer == nil) {
        _audioPlayer = [PCMAUAudioPlayer new];
        _audioPlayer.delegate = self;
        [_audioPlayer setup];
    }
}

#pragma mark - Video
- (void)playVideoInChannel:(GroupLiveChannelPosition)ch atAbsTime:(int)timestamp
{
    NSMutableArray* groupVFs = [_groupVFChannelContainer objectAtIndex:ch-1];
    GroupVideoFrame* displayGroupVF = nil;
    
    for(GroupVideoFrame* gVF in groupVFs) {
        if (gVF.videoAbsTime <= timestamp) {
            displayGroupVF = gVF;
        } else {
            break;
        }
    }
    
    if(displayGroupVF==nil) {
        //        DLog(@"no vf can be played for %d af", timestamp);
        return;
    }
    
    [groupVFs removeObjectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [groupVFs indexOfObject:displayGroupVF]+1)]];
    
    [_delegate playVideoData:displayGroupVF];
}

@end
