 //
//  PaymentInfoViewController.m
//  Story17
//
//  Created by POPO on 2015/5/14.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "BlabSettingViewController.h"
#import "ListTableViewController.h"
#import "Constant.h"
#import "FillAccountViewController.h"
#import "BlabViewController.h"

@implementation BlabSettingViewController
#define typeBtnW (SCREEN_WIDTH/3-10)
#define typeBtnH 50
#define verticalMargin 5
#define titleLabelHeight 30


- (id)init
{
    self = [super init];
    if (self)
    {
        _choosedTypes=0;
        _typeArrary=CREATE_MUTABLE_ARRAY;
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self addCustomNavigationBackButton];
    [self configureViewForIOS7];
    _datePicker =[[UIDatePicker alloc]initWithFrame:CGRectMake(0, 165,10, 50)];
    if (IS_IPHONE_4) {
//        _datePicker.frame =CGRectMake(0, 160,10, 50);
        _datePicker.center=CGPointMake(SCREEN_WIDTH/2, 242);
    }
    _datePicker.alpha=0;
    self.title = LOCALIZE(@"livestream");
    
    [self.view setBackgroundColor:LIGHT_BG_COLOR];
    _tittleLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 10, 2/3*SCREEN_WIDTH, titleLabelHeight)];
    [_tittleLabel setTextColor:MAIN_COLOR];
    _tittleLabel.textAlignment = NSTextAlignmentCenter;
    [_tittleLabel setText:LOCALIZE(@"title")];
    [_tittleLabel setFont:SYSTEM_FONT_WITH_SIZE(18)];
    [_tittleLabel sizeToFit];
//    _tittleLabel.center=CGPointMake(SCREEN_WIDTH/2, 30);
    [self.view addSubview:_tittleLabel];
    
    
    UILabel * typeLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 100, 30, 30)];
    [typeLabel setTextColor:MAIN_COLOR];
    typeLabel.textAlignment = NSTextAlignmentCenter;
    [typeLabel setText:LOCALIZE(@"Type")];
    [typeLabel setFont:SYSTEM_FONT_WITH_SIZE(18)];
    [typeLabel sizeToFit];
    //    _tittleLabel.center=CGPointMake(SCREEN_WIDTH/2, 30);
    [self.view addSubview:typeLabel];
    
    UIButton* typeContainer=[[UIButton alloc]initWithFrame:CGRectMake(20, 120, SCREEN_WIDTH-30, 150)];
//    [typeContainer setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    
    [self.view addSubview:typeContainer];
    typeContainer.userInteractionEnabled=YES;
    
    
    UILabel * typeRemindLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [typeRemindLabel setTextColor:MAIN_COLOR];
    typeRemindLabel.textAlignment = NSTextAlignmentCenter;
    [typeRemindLabel setText:LOCALIZE(@"Choose_at_most_3_types")];
    [typeRemindLabel setFont:SYSTEM_FONT_WITH_SIZE(18)];
    [typeRemindLabel sizeToFit];
    typeRemindLabel.center=CGPointMake(SCREEN_WIDTH/2, 290);
    [self.view addSubview:typeRemindLabel];
    
    
    _type1=[self getTypeBtn];
    _type1.center=CGPointMake(typeBtnW/2, typeBtnH/2);
    [_type1 setTitle:LOCALIZE(@"Chat") forState:UIControlStateNormal];
    [typeContainer addSubview:_type1];

    _type2=[self getTypeBtn];
    _type2.center=CGPointMake(typeBtnW/2+typeBtnW, typeBtnH/2);
    [_type2 setTitle:LOCALIZE(@"Life") forState:UIControlStateNormal];
    [typeContainer addSubview:_type2];
    
    _type3=[self getTypeBtn];
    _type3.center=CGPointMake(typeBtnW/2+typeBtnW*2, typeBtnH/2);
    [_type3 setTitle:LOCALIZE(@"Work") forState:UIControlStateNormal];
    [typeContainer addSubview:_type3];
    
    _type4=[self getTypeBtn];
    _type4.center=CGPointMake(typeBtnW/2, typeBtnH/2+typeBtnH);
    [_type4 setTitle:LOCALIZE(@"Entertainment") forState:UIControlStateNormal];
    [typeContainer addSubview:_type4];
    
    _type5=[self getTypeBtn];
    _type5.center=CGPointMake(typeBtnW/2+typeBtnW, typeBtnH/2+typeBtnH);
    [_type5 setTitle:LOCALIZE(@"Food") forState:UIControlStateNormal];
    [typeContainer addSubview:_type5];
    
    _type6=[self getTypeBtn];
    _type6.center=CGPointMake(typeBtnW/2+2*typeBtnW, typeBtnH/2+typeBtnH);
    [_type6 setTitle:LOCALIZE(@"Travel") forState:UIControlStateNormal];
    [typeContainer addSubview:_type6];
    
    _type7=[self getTypeBtn];
    _type7.center=CGPointMake(typeBtnW/2, typeBtnH/2+2*typeBtnH);
    [_type7 setTitle:LOCALIZE(@"Beauty") forState:UIControlStateNormal];
    [typeContainer addSubview:_type7];
    
    _type8=[self getTypeBtn];
    _type8.center=CGPointMake(typeBtnW/2+typeBtnW, typeBtnH/2+2*typeBtnH);
    [_type8 setTitle:LOCALIZE(@"Pet") forState:UIControlStateNormal];
    [typeContainer addSubview:_type8];
    
    _type9=[self getTypeBtn];
    _type9.center=CGPointMake(typeBtnW/2+2*typeBtnW, typeBtnH/2+2*typeBtnH);
    [_type9 setTitle:LOCALIZE(@"Others") forState:UIControlStateNormal];
    [typeContainer addSubview:_type9];
    
    UIView* Hline1=[ThemeManager separaterLine];
    Hline1.frame=CGRectMake(2, 50, SCREEN_WIDTH-30-4, 1);
    [typeContainer addSubview:Hline1];
    
    UIView* Hline2=[ThemeManager separaterLine];
    Hline2.frame=CGRectMake(2, 150*2/3, SCREEN_WIDTH-30-4, 1);
    [typeContainer addSubview:Hline2];
    
    UIView* Vline1=[ThemeManager separaterLine];
    Vline1.frame=CGRectMake(SCREEN_WIDTH/3-10, 2, 1, 146);
    [typeContainer addSubview:Vline1];
    
    
    UIView* Vline2=[ThemeManager separaterLine];
    Vline2.frame=CGRectMake((SCREEN_WIDTH-30)*2/3, 2, 1, 146);
    [typeContainer addSubview:Vline2];
    
    UIView* outHline1=[ThemeManager separaterLine];
    [outHline1 setBackgroundColor:MAIN_COLOR];
    outHline1.frame=CGRectMake(0, 0, SCREEN_WIDTH-30+1, 1);
    [typeContainer addSubview:outHline1];
    
    UIView* outHline2=[ThemeManager separaterLine];
    [outHline2 setBackgroundColor:MAIN_COLOR];
    outHline2.frame=CGRectMake(0, 150, SCREEN_WIDTH-30, 1);
    [typeContainer addSubview:outHline2];
    
    UIView* outVline1=[ThemeManager separaterLine];
    [outVline1 setBackgroundColor:MAIN_COLOR];
    outVline1.frame=CGRectMake(0, 0, 1, 150);
    [typeContainer addSubview:outVline1];
    
    
    UIView* outVline2=[ThemeManager separaterLine];
    [outVline2 setBackgroundColor:MAIN_COLOR];
    outVline2.frame=CGRectMake(SCREEN_WIDTH-30, 0, 1, 151);
    [typeContainer addSubview:outVline2];
    
    _livetitle=[[UITextField alloc]initWithFrame:CGRectMake(20, 5+titleLabelHeight, SCREEN_WIDTH-30, titleLabelHeight+10)];
    _livetitle.delegate=self;
    [_livetitle setBackground:[[UIImage imageNamed:@"btn_grayline" ]resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)]];
    [_livetitle setText:LOCALIZE(@"enter_title_blab")];
    [_livetitle setTextColor:GRAY_COLOR];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _livetitle.leftView = paddingView;
    _livetitle.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview:_livetitle];
    
    [self.view setBackgroundColor:LIGHT_BG_COLOR];
    if (IS_IPHONE_4) {
        _startTimeLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, SCREEN_HEIGHT*3/5-50, 2/3*SCREEN_WIDTH, titleLabelHeight)];

    }else
    {
        _startTimeLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, SCREEN_HEIGHT*3/5-50, 2/3*SCREEN_WIDTH, titleLabelHeight)];

    }
    [_startTimeLabel setTextColor:MAIN_COLOR];
    _startTimeLabel.textAlignment = NSTextAlignmentCenter;
    [_startTimeLabel setText:LOCALIZE(@"Start_time")];
    [_startTimeLabel setFont:SYSTEM_FONT_WITH_SIZE(18)];
    [_startTimeLabel sizeToFit];
//    [self.view addSubview:_startTimeLabel];
    
    _targettimeLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    _targettimeLabel.center=CGPointMake(SCREEN_WIDTH/2, 445);
    if (IS_IPHONE_4) {
        _targettimeLabel.center=CGPointMake(SCREEN_WIDTH/2, 395);

    }
    _targettimeLabel.hidden=YES;
    
    
    _NowBtn=[[UIButton alloc]initWithFrame:CGRectMake(100, 100, SCREEN_WIDTH-30, 40    )];
    [_NowBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [_NowBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
    [_NowBtn setTitle:LOCALIZE(@"Now") forState:UIControlStateNormal];
    [_NowBtn addTarget:self action:@selector(clickNow:) forControlEvents:UIControlEventTouchUpInside];
    [_NowBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
//    _NowBtn.center=CGPointMake(SCREEN_WIDTH/4+7, 140);
    _NowBtn.center=CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT*3/5);
//    [self.view addSubview:_NowBtn];
    
    _LaterBtn=[[UIButton alloc]initWithFrame:CGRectMake(100, 100, SCREEN_WIDTH/2-30, 40    )];
    [_LaterBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [_LaterBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
    [_LaterBtn setTitle:LOCALIZE(@"Inadvance") forState:UIControlStateNormal];
    [_LaterBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
    [_LaterBtn addTarget:self action:@selector(cliclLater:) forControlEvents:UIControlEventTouchUpInside];

//    [_LaterBtn addTarget:self action:@selector(cliclLater:) forControlEvents:UIControlEventTouchUpInside];
    _LaterBtn.center=CGPointMake(SCREEN_WIDTH*3/4-7, 140);
//    [self.view addSubview:_LaterBtn];
    
    
    _ConfirmBtn=[[UIButton alloc]initWithFrame:CGRectMake(100, 100, SCREEN_WIDTH-30, 40    )];
    [_ConfirmBtn setBackgroundImage:[[UIImage imageNamed:@"btn_whitebg_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    _ConfirmBtn.userInteractionEnabled=NO;
//    [_ConfirmBtn setBackgroundImage:[[UIImage imageNamed:@"btn_black"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
    [_ConfirmBtn setTitle:LOCALIZE(@"OK") forState:UIControlStateNormal];
    [_ConfirmBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    [_ConfirmBtn addTarget:self action:@selector(clickConfirm:) forControlEvents:UIControlEventTouchUpInside];
    //No later
    _NowBtn.selected=YES;
    [self reloadBtnStatus];

    
    _ConfirmBtn.center=CGPointMake(SCREEN_WIDTH*1/2, SCREEN_HEIGHT-100);
    [self.view addSubview:_ConfirmBtn];
    
    // 時區的問題請再找其他協助 不是本篇重點
    _datelocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"];
    _datePicker.locale = _datelocale;
//    _datePicker.timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    [_datePicker addTarget:self action:@selector(dateIsChanged:) forControlEvents:UIControlEventValueChanged];
    [_targettimeLabel setText:[NSString stringWithFormat:LOCALIZE(@"target_time"),0,0]];
    [_targettimeLabel setTextAlignment:NSTextAlignmentCenter];
    if (IS_IPHONE_4) {
//        _startTimeLabel.center=CGPointMake(, 100);
        _LaterBtn.center=CGPointMake(SCREEN_WIDTH*3/4-7, 125);
        _NowBtn.center=CGPointMake(SCREEN_WIDTH/4+7, 125);    }
    [self.view addSubview:_targettimeLabel];
}

-(void)doneAction
{
    //
    //    [API_MANAGER updateUserInfo:_updateInfoDict  fetchSelfInfo:YES  completion:^(BOOL success) {
    //        if(success){
    //
    //        }
    //    }];
    
}

-(void)clickNow:(id)sender
{
    [self.view endEditing:YES];

    if (_NowBtn.selected) {
    _NowBtn.selected=NO;
    }else{
        _NowBtn.selected=YES;
        _LaterBtn.selected=NO;
    }
    [self reloadBtnStatus];

}

- (void)cliclLater:(id)sender

{
    [_targettimeLabel setText:[NSString stringWithFormat:LOCALIZE(@"target_time"),0,0]];

    
    _datePicker.datePickerMode=UIDatePickerModeTime;
    _datePicker.date=[NSDate date];
//    [_datePicker addTarget:self action:@selector(LadbelTitle:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_datePicker];

    [self.view endEditing:YES];
    if (_LaterBtn.selected) {

        _LaterBtn.selected=NO;
    }else{
        _LaterBtn.selected=YES;
        _NowBtn.selected=NO;

    }
    [self reloadBtnStatus];

}

-(void)dateIsChanged:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

    DLog(@"date chane");
    DLog(@"Date===%@",_datePicker.date);
    [dateFormatter setDateFormat:@"mm"];
//    int minute = [[dateFormatter stringFromDate:_datePicker.date] intValue];
    
    [dateFormatter setDateFormat:@"HH"];
//    int hour = [[dateFormatter stringFromDate:_datePicker.date] intValue];
    
//    DLog(@"minute=%d",minute);
//    DLog(@"hour=%d",hour);
    
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"HH mm"];       //Remove the time part
    NSString *TodayString = [df stringFromDate:[NSDate date]];
    NSString *TargetDateString = [df stringFromDate:_datePicker.date];
    NSTimeInterval time = [[df dateFromString:TargetDateString] timeIntervalSinceDate:[df dateFromString:TodayString]];
    int hours = time / 60 / 60;
    int minutes = time / 60 ;
    minutes=minutes%60;
    
    if (hours<0) {
        hours=hours+24;
    }
    
    if (hours==0 && minutes<0) {
        hours=23;
        minutes=minutes+60;
    }
    
    if(minutes<0)
    {
        hours--;
        minutes=minutes+60;
    }
    [_targettimeLabel setText:[NSString stringWithFormat:LOCALIZE(@"target_time"),hours,minutes]];
    [_targettimeLabel setTextColor:BLACK_COLOR];
    [_targettimeLabel setTextAlignment:NSTextAlignmentCenter];
    DLog(@"距離小時=%d",hours);
    DLog(@"距離分鐘=%d",minutes);
}

-(void)clickConfirm:(id)sender
{
    if (_publishing) {
        return;
    }
    _publishing=YES;
    
            NSArray* picArray =  @[];
            NSMutableDictionary* infoDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:_livetitle.text, @"caption", @"taipei", @"locationName", @"25.5", @"latitude", @"20.0f", @"longitude", @"", @"coverPhoto", INT_TO_STRING(1), @"shareLocation", INT_TO_STRING(1), @"followerOnlyChat", INT_TO_STRING(1), @"scheduleTime",TO_JSON(picArray),@"pictures",nil];
            
            

            [API_MANAGER publishGroupLiveStream:infoDict withCompletion:^(BOOL success, NSString *liveStreamID, int timestamp, NSString *colorCode) {
                if (success) {
                    BlabViewController *blabvc=[[BlabViewController alloc]init];
                    blabvc.liveStreamMode=LIVE_SHOW_MODE_HOLDER;
                    blabvc.livestreamID = liveStreamID;
                    [self presentViewController:blabvc animated:YES completion:nil];
                } else {
                    _publishing=NO;
                    [DIALOG_MANAGER showNetworkFailToast];
                }

            }];
            
     
}

-(void)reloadBtnStatus
{
    if (_NowBtn.selected||_LaterBtn.selected) {
        _ConfirmBtn.userInteractionEnabled=YES;
        [_ConfirmBtn setBackgroundImage:[[UIImage imageNamed:@"btn_black_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
        [_ConfirmBtn setBackgroundImage:[[UIImage imageNamed:@"btn_black"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
        if (_NowBtn.selected) {
            [_NowBtn setBackgroundImage:[[UIImage imageNamed:@"btn_black_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
            [_NowBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
        }else{
            [_NowBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
            [_NowBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
        }
        if (_LaterBtn.selected) {
            [_LaterBtn setBackgroundImage:[[UIImage imageNamed:@"btn_black_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
   
            [_LaterBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
            [UIView animateWithDuration:0.2 animations:^{
//                _ConfirmBtn.center=CGPointMake(SCREEN_WIDTH/2, 400);
                if (IS_IPHONE_4) {
//                    _ConfirmBtn.center=CGPointMake(SCREEN_WIDTH/2, 360);
                }
            }];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.2 animations:^{
                    _datePicker.alpha=1;
                    _targettimeLabel.hidden=NO;
                }];
            });
        }else{
            [UIView animateWithDuration:0.2 animations:^{
                _datePicker.alpha=0;
                _targettimeLabel.hidden=YES;
//                _ConfirmBtn.center=CGPointMake(SCREEN_WIDTH/2, 200);
            }];
            [_LaterBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
            [_LaterBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
        }
        
    }else if(!_NowBtn.selected&&!_LaterBtn.selected){
        _ConfirmBtn.userInteractionEnabled=NO;
        
        [_ConfirmBtn setBackgroundImage:[[UIImage imageNamed:@"btn_whitebg_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
        
        [_NowBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
        [_NowBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
        [_LaterBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
        [_LaterBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
        if (_LaterBtn.selected) {
            [_LaterBtn setBackgroundImage:[[UIImage imageNamed:@"btn_black_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
            [_LaterBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
            [UIView animateWithDuration:0.2 animations:^{
//                _ConfirmBtn.center=CGPointMake(SCREEN_WIDTH/2, 400);
                if (IS_IPHONE_4) {
//                    _ConfirmBtn.center=CGPointMake(SCREEN_WIDTH/2, 360);
                }
            }];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.2 animations:^{
                    _datePicker.alpha=1;
                    _targettimeLabel.hidden=NO;
                }];
            });
        }else{
            [UIView animateWithDuration:0.2 animations:^{
                _datePicker.alpha=0;
                _targettimeLabel.hidden=YES;
//                _ConfirmBtn.center=CGPointMake(SCREEN_WIDTH/2, 200);
            }];
            [_LaterBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
            [_LaterBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
        }
    }
    

}

-(void)viewWillAppear:(BOOL)animated
{
    _publishing=NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [_livetitle setText:@""];
    [_livetitle setTextColor:BLACK_COLOR];

}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.view endEditing:YES];
}

-(UIButton *)getTypeBtn{
    UIButton* type=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, typeBtnW, typeBtnH    )];
    [type setBackgroundImage:[[UIImage imageNamed:@""] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [type setBackgroundImage:[[UIImage imageNamed:@"btn_lod_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
    [type setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
    [type addTarget:self action:@selector(cliclkType:) forControlEvents:UIControlEventTouchUpInside];
    return type;
}

-(void)cliclkType:(UIButton*)sender
{

    if ([sender isEqual:_type1]) {
        if (_type1.selected==YES) {
            _type1.selected=NO;
            _choosedTypes--;
        }else{
            if (_choosedTypes>2) {
                return;
            }
            _type1.selected=YES;
            _choosedTypes++;
        }
    }else if ([sender isEqual:_type2]){
        if (_type2.selected==YES) {
            _type2.selected=NO;
            _choosedTypes--;
        }else{
            if (_choosedTypes>2) {
                return;
            }
            _type2.selected=YES;
            _choosedTypes++;
        }
    }else if ([sender isEqual:_type3]){
        if (_type3.selected==YES) {
            _type3.selected=NO;
            _choosedTypes--;
        }else{
            if (_choosedTypes>2) {
                return;
            }
            _type3.selected=YES;
            _choosedTypes++;
        }
    }else if ([sender isEqual:_type4]){
        if (_type4.selected==YES) {
            _type4.selected=NO;
            _choosedTypes--;
        }else{
            if (_choosedTypes>2) {
                return;
            }
            _choosedTypes++;
            _type4.selected=YES;
        }
    }else if ([sender isEqual:_type5]){
        if (_type5.selected==YES) {
            _choosedTypes--;
            _type5.selected=NO;
        }else{
            if (_choosedTypes>2) {
                return;
            }
            _choosedTypes++;
            _type5.selected=YES;
        }
    }else if ([sender isEqual:_type6]){
        if (_type6.selected==YES) {
            _type6.selected=NO;
            _choosedTypes--;
        }else{
            if (_choosedTypes>2) {
                return;
            }
            _choosedTypes++;
            _type6.selected=YES;
        }
    }else if ([sender isEqual:_type7]){
        if (_type7.selected==YES) {
            _type7.selected=NO;
            _choosedTypes--;
        }else{
            if (_choosedTypes>2) {
                return;
            }
            _choosedTypes++;
            _type7.selected=YES;
        }
    }else if ([sender isEqual:_type8]){
        if (_type8.selected==YES) {
            _choosedTypes--;
            _type8.selected=NO;
        }else{
            if (_choosedTypes>2) {
                return;
            }
            _choosedTypes++;
            _type8.selected=YES;
        }
    }else if ([sender isEqual:_type9]){
        if (_type9.selected==YES) {
            _choosedTypes--;
            _type9.selected=NO;
        }else{
            if (_choosedTypes>2) {
                return;
            }
            _choosedTypes++;
            _type9.selected=YES;
        }
    }
    [self reloadTypeStatus];
}
     
-(void)reloadTypeStatus
{
    if (_type1.selected) {
        [_type1 setBackgroundImage:[[UIImage imageNamed:@"btn_lod_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }else{
        [_type1 setBackgroundImage:[[UIImage imageNamed:@""] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }
    
    if (_type2.selected) {
        [_type2 setBackgroundImage:[[UIImage imageNamed:@"btn_lod_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }else{
        [_type2 setBackgroundImage:[[UIImage imageNamed:@""] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }
    
    if (_type3.selected) {
        [_type3 setBackgroundImage:[[UIImage imageNamed:@"btn_lod_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }else{
        [_type3 setBackgroundImage:[[UIImage imageNamed:@""] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }
    
    if (_type4.selected) {
        [_type4 setBackgroundImage:[[UIImage imageNamed:@"btn_lod_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }else{
        [_type4 setBackgroundImage:[[UIImage imageNamed:@""] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }
    
    if (_type5.selected) {
        [_type5 setBackgroundImage:[[UIImage imageNamed:@"btn_lod_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }else{
        [_type5 setBackgroundImage:[[UIImage imageNamed:@""] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }
    
    if (_type6.selected) {
        [_type6 setBackgroundImage:[[UIImage imageNamed:@"btn_lod_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }else{
        [_type6 setBackgroundImage:[[UIImage imageNamed:@""] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }
    
    if (_type7.selected) {
        [_type7 setBackgroundImage:[[UIImage imageNamed:@"btn_lod_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }else{
        [_type7 setBackgroundImage:[[UIImage imageNamed:@""] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }
    
    if (_type8.selected) {
        [_type8 setBackgroundImage:[[UIImage imageNamed:@"btn_lod_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }else{
        [_type8 setBackgroundImage:[[UIImage imageNamed:@""] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }
    
    if (_type9.selected) {
        [_type9 setBackgroundImage:[[UIImage imageNamed:@"btn_lod_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }else{
        [_type9 setBackgroundImage:[[UIImage imageNamed:@""] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
@end
