//
//  LivestreamBroadcasterController.m
//  story17
//
//  Created by POPO Chen on 7/30/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "BlabstreamBroadcasterController.h"
#import "MultiFileUploader.h"

@interface BlabstreamBroadcasterController()

/* Publisher Related */
@property (nonatomic, strong) AFHTTPRequestOperationManager* fileDownloadManager;
@property (nonatomic, strong) UDPSocket* udpSocket;
@property (nonatomic, strong) GCDAsyncSocket* tcpSocket;
@property BOOL isConnectingTcpSocket;
@property long currentTcpDataTag;

@property PCMAudioRecorder* audioRecorder;
@property (nonatomic, strong) MyFilter* displayFilter;
@property (nonatomic, strong) NSArray* filters;
@property int filterIndex;
@property VP8VideoEncoder* videoEncoder;
@property (nonatomic,strong) MyOpusEncoder* opusEncoder;
@property BOOL isEncoding;
@property unsigned char* yDataBuffer;
@property unsigned char* cbDataBuffer;
@property unsigned char* crDataBuffer;
@property (nonatomic, strong) NSDate* livestreamStartDate;
@property float lastReceivedAudioDataTime;

@property dispatch_queue_t socketQueue;
@property dispatch_queue_t videoEncodeQueue;
@property dispatch_queue_t audioEncodeQueue;

@property (nonatomic) BOOL startPublish;
@property (nonatomic) int keepAliveFailCount;
@property (nonatomic, strong) NSMutableArray* queuedVideoFrameObjects;
@property (nonatomic, strong) NSMutableArray* queuedResendData;

@property int32_t videoSequenceNumber;
@property int32_t audioSequenceNumber;
@property int chunkID;

@property int32_t liveStreamStartTime;

/* For upload cover photo */
@property (nonatomic, strong) UIImage* coverImage;
@property (nonatomic, strong) NSString* coverImageName;
@property int updateCount;
@property int lastUpdateTime;

/* Broadcaster timer */
@property (nonatomic,strong) NSTimer* keepAliveTimer;
@property (nonatomic) int keepAliveCount;

/* Capture Session */
@property (nonatomic, strong) AVCaptureSession* captureSession;
@property (nonatomic, strong) AVCaptureDeviceInput *inputDevice;
@property (nonatomic, strong) AVCaptureDevice *captureDevice;
@property (nonatomic, strong) AVCaptureVideoDataOutput *videoDataOutput;


//Down FPS
@property (nonatomic) int TargetFps;
@property (nonatomic,strong) NSTimer* UpFpsTimer;
@property (nonatomic) int FPSTimerInterval;
@property (nonatomic) long lateDataCount;
@property (nonatomic,strong) NSTimer* DownFpsTimer;
@property (nonatomic,strong) NSTimer* PhoneCallTimer;
@property (nonatomic) long limitlateDataCount;

@end


@implementation BlabstreamBroadcasterController

- (id)init
{
    self = [super init];
    _callCenter = [[CTCallCenter alloc] init];
    
    [NOTIFICATION_CENTER addObserver:self
                            selector:@selector(AudioInterruption:)
                                name:AVAudioSessionInterruptionNotification
                              object:_captureSession];
    
    DLog(@"init Retain count is %ld", CFGetRetainCount((__bridge CFTypeRef)self));
    
    if(self){
        
        _queuedResendData = CREATE_MUTABLE_ARRAY;
        
        /* Publisher Related */
        _audioOnly = 0;
        _TargetFps=30;
        _FPSTimerInterval=15;
        _limitlateDataCount=50;
        _keepAliveFailCount = 0;
        _startPublish = NO;
        _filters = @[NORMAL_FILTER, RICH_FILTER, WARM_FILTER, SOFT_FILTER, ROSE_FILTER, MORNING_FILTER, SUNSHINE_FILTER, SUNSET_FILTER, COOL_FILTER, FREEZE_FILTER, OCEAN_FILTER, DREAM_FILTER, VIOLET_FILTER, MELLOW_FILTER, BLEAK_FILTER, MEMORY_FILTER, PURE_FILTER, CALM_FILTER, AUTUMN_FILTER, FANTASY_FILTER, FREEDOM_FILTER, MILD_FILTER, PRAIRIE_FILTER, DEEP_FILTER, GLOW_FILTER, MEMOIR_FILTER, MIST_FILTER, VIVID_FILTER, CHILL_FILTER, PINKY_FILTER, ADVENTURE_FILTER];
        
        /* init broad caster */
        _yDataBuffer = malloc(FRAME_WIDTH*FRAME_HEIGHT);
        _cbDataBuffer = malloc(FRAME_WIDTH*FRAME_HEIGHT/4);
        _crDataBuffer = malloc(FRAME_WIDTH*FRAME_HEIGHT/4);
        
        _opusEncoder = [MyOpusEncoder new];
        
        /* queue */
        _socketQueue = dispatch_queue_create("live_stream_socket_queue", DISPATCH_QUEUE_SERIAL);
        _videoEncodeQueue = dispatch_queue_create("live_stream_video_encode_queue", DISPATCH_QUEUE_SERIAL);
        _audioEncodeQueue = dispatch_queue_create("audio_encode_queue", DISPATCH_QUEUE_SERIAL);
        
        //        [self setupSocket];
        [self downFpsTimerStart];
        [self upFpsTimerStart];
        _displayFilter = [[MyFilter alloc] initWithImageSize:CGSizeMake(640, 360) filterProgram:FILTER_PROGRAM_SIMPLE];
        [_displayFilter setParameter:@"imageSourceInputType" intValue:2];
        [_displayFilter setRotation:180.0 needHorizontalFlip:1];
        
        _videoEncoder = [VP8VideoEncoder new];
        
        if (IS_IPHONE_4) {
            _glVideoOutputView = [[GLKView alloc] initWithFrame:CGRectMake(0, -SCREEN_HEIGHT/6, SCREEN_WIDTH, SCREEN_HEIGHT*4/3)];
        } else {
            _glVideoOutputView = [[GLKView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, SCREEN_HEIGHT/2)];
        }
        
        _glVideoOutputView.drawableColorFormat = GLKViewDrawableColorFormatRGB565;
        _glVideoOutputView.drawableDepthFormat = GLKViewDrawableDepthFormatNone;
        _glVideoOutputView.drawableMultisample = GLKViewDrawableMultisampleNone;
        _glVideoOutputView.context = _displayFilter.glContext;
        _glVideoOutputView.contentScaleFactor = 1.0;
        _glVideoOutputView.alpha = 0.0;
        [_glVideoOutputView setEnableSetNeedsDisplay:NO];
        
        __weak BlabstreamBroadcasterController* weakself = self;
        self.panRecogHandler = ^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            
            if(state==UIGestureRecognizerStateEnded) {
                if (((UISwipeGestureRecognizer*) sender).direction == UISwipeGestureRecognizerDirectionLeft) {
                    weakself.filterIndex++;
                } else if (((UISwipeGestureRecognizer*) sender).direction == UISwipeGestureRecognizerDirectionRight){
                    weakself.filterIndex--;
                }
                
                if(weakself.filterIndex==-1) {
                    weakself.filterIndex = (int) weakself.filters.count - 1;
                }
                
                weakself.filterIndex = _filterIndex % _filters.count;
                
                [ weakself.displayFilter loadFilter: weakself.filters[ weakself.filterIndex]];
                
                for(UIView* v in  weakself.glVideoOutputView.subviews) {
                    [v removeFromSuperview];
                }
                
                UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,  weakself.glVideoOutputView.frame.size.width,  weakself.glVideoOutputView.frame.size.height*4/5)];
                label.textColor = TIFFINY_BLUE_COLOR;
                label.font = BOLD_FONT_WITH_SIZE(28.0);
                label.text = LOCALIZE( weakself.filters[ weakself.filterIndex]);
                [ weakself.glVideoOutputView addSubview:label];
                
                label.alpha = 0.0;
                label.textAlignment = NSTextAlignmentCenter;
                
                [UIView animateWithDuration:0.0 animations:^{
                    label.alpha = 1.0;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.25 delay:0.25 options:UIViewAnimationOptionCurveLinear animations:^{
                        label.alpha = 0.0;
                    } completion:^(BOOL finished) {
                        [label removeFromSuperview];
                    }];
                }];
            }
        };
        
        [self reloadCaptureSession];
        [self setupAudioRecorder];
        
    }
    return self;
}

-(void) startLiveStreamWithLivestreamInfo:(NSDictionary*)infodict
{
    
    DLog(@"publishLiveStream");
    [DIALOG_MANAGER showLoadingView];
    
    [API_MANAGER publishLiveStream:infodict withCompletion:^(BOOL success, NSString *liveStreamID, int timestamp, NSString *colorCode,NSString* publicIP) {
        
        [DIALOG_MANAGER hideLoadingView];
        
        if (success) {
            
            _liveStreamStartTime = CURRENT_TIMESTAMP;
            LiveStreamObject* livestream = [LiveStreamObject new];
            livestream.liveStreamID = liveStreamID;
            _livestreamID = liveStreamID;
            
            if(_publicIP!=nil)
            {
                _publicIP = publicIP;
            }else{
                _publicIP =LIVE_STREAM_UDP_SERVER_IP ;
            }
            
            livestream.liveStreamIDInt = [liveStreamID intValue];
            if(_delegate!=nil)
                [_delegate startBroadcast:YES withLivestream:livestream];
            
            _broadcasterHexcolor = [colorCode copy];
            _startPublish = YES;
            
            [self setupSocket];
            [self keepAliveTimerStart];
            [_audioRecorder startRecording];
        } else {
            if(_delegate!=nil)
                [_delegate startBroadcast:NO withLivestream:nil];
        }
    }];
}

-(void) cleanup
{
    
    [NOTIFICATION_CENTER removeObserver:self];
    
    _startPublish = NO;
    [_captureSession stopRunning];
    [NOTIFICATION_CENTER removeObserver:self];
    
    
    
    [self cleanupSocket];
    
    [self stopAudioRecorder];
    
    if (_keepAliveTimer) {
        [_keepAliveTimer invalidate];
        _keepAliveTimer = nil;
    }
    
    if(_PhoneCallTimer){
        [_PhoneCallTimer invalidate];
        _PhoneCallTimer=nil;
    }
    
    if(_DownFpsTimer){
        [_DownFpsTimer invalidate];
        _DownFpsTimer=nil;
    }
    
    if(_UpFpsTimer){
        [_UpFpsTimer invalidate];
        _UpFpsTimer=nil;
    }
    
    // delay free data to avoid crash (some data still processing in threads)
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        free(_yDataBuffer);
        free(_cbDataBuffer);
        free(_crDataBuffer);
    });
}

-(void)keepAliveTimerStart
{
    if (_keepAliveTimer) {
        [_keepAliveTimer invalidate];
        _keepAliveTimer = nil;
    }
    _keepAliveTimer = [NSTimer scheduledTimerWithTimeInterval:LIVE_STREAM_KEEP_ALIVE_TIME target:self selector:@selector(sendKeepAliveData) userInfo:nil repeats:YES];
}

-(void)upFpsTimerStart
{
    if (_UpFpsTimer) {
        [_UpFpsTimer invalidate];
        _UpFpsTimer=nil;
    }
    _UpFpsTimer = [NSTimer scheduledTimerWithTimeInterval:_FPSTimerInterval target:self selector:@selector(UpFpsParameter) userInfo:nil repeats:YES];
}

-(void)downFpsTimerStart
{
    
    if (_DownFpsTimer) {
        [_DownFpsTimer invalidate];
        _DownFpsTimer=nil;
    }
    
    _DownFpsTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(DownFpsParameter) userInfo:nil repeats:YES];
}

-(void)DownFpsParameter
{
    if (_lateDataCount>_limitlateDataCount) {
        _limitlateDataCount=_limitlateDataCount+50;
        _TargetFps=_TargetFps-5;
        if (_TargetFps<6) {
            _TargetFps=6;
        }
        _FPSTimerInterval=_FPSTimerInterval+15;
        if (_FPSTimerInterval>120) {
            _FPSTimerInterval=120;
        }
        [self downFpsTimerStart];
    }
}

-(void)UpFpsParameter
{
    if (_lateDataCount<30) {
        _limitlateDataCount=50;
        if (_TargetFps==6) {
            _TargetFps=10;
        }else{
            _TargetFps=_TargetFps+5;
            if (_TargetFps>30) {
                _TargetFps=30;
            }
        }
        
        
    }
}

-(void) sendKeepAliveData
{
    DLog(@"sendKeepAliveData");
    
    dispatch_async(MAIN_QUEUE, ^{
        
        if(_startPublish)
        {
            [self sendUDPPacket:LIVE_STREAM_PACKET_TYPE_KEEP_ALIVE_PUBLISHER];
            
            if (_livestreamID!=nil) {
                
                [API_MANAGER keepLiveStream:_livestreamID withCompletion:^(BOOL success, NSString *message) {
                    if (success) {
                        _keepAliveFailCount = 0;
                    }else{
                        if(_keepAliveFailCount==6){
                            _keepAliveFailCount ++;
                        }else{
                            _keepAliveFailCount ++;
                        }
                        [_delegate broadcastKeepAliveFail:_keepAliveFailCount];
                    }
                }];
            }
        }
    });
}

-(void)AVCaptureSessionDidStopRunningNotification
{
    
}

-(void)AudioInterruption: (NSNotification *) notification
{
    NSLog(@"Interruption notification name %@", notification.name);
    
    if ([notification.name isEqualToString:AVAudioSessionInterruptionNotification]) {
        NSLog(@"Interruption notification received %@!", notification);
        
        //Check to see if it was a Begin interruption
        if ([[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] isEqualToNumber:[NSNumber numberWithInt:AVAudioSessionInterruptionTypeBegan]]) {
            DLog(@"\n\nInterruption began!");
            _PhoneCallTimer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(detectphoneCall) userInfo:nil repeats:YES];
            
            [_audioRecorder stopRecording];
        } else if([[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] isEqualToNumber:[NSNumber numberWithInt:AVAudioSessionInterruptionTypeEnded]]){
            DLog(@"\n\nInterruption ended!");
            
            if(_PhoneCallTimer){
                [_PhoneCallTimer invalidate];
                _PhoneCallTimer=nil;
            }
            
            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
            [[AVAudioSession sharedInstance] setPreferredSampleRate:48000.0 error:NULL];
            [[AVAudioSession sharedInstance] setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
            [_audioRecorder startRecording];
            
            //Resume your audio
            // Resume playing the audio.
            
        }
    }
}

-(void)detectphoneCall
{
    _Count++;
    for (CTCall *call in _callCenter.currentCalls)  {
        if (call.callState == CTCallStateConnected ) {
            DLog(@"接起電話");
            [self endLivestream];
            return;
        }
    }
    if (_Count>5) {
        [self endLivestream];
    }
}

-(void) setupAudioRecorder
{
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [[AVAudioSession sharedInstance] setPreferredSampleRate:48000.0 error:NULL];
    [[AVAudioSession sharedInstance] setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
    
    if(_audioRecorder!=nil) {
        return;
    }
    
    [self stopAudioRecorder];
    
    _audioRecorder = [[PCMAudioRecorder alloc] initWithMode:PCM_RECORDER_MODE_OPUS];
    _audioRecorder.delegate = self;
}

-(void) startAudioRecorder
{
    if(_audioRecorder!=nil) {
        [_audioRecorder startRecording];
    }
}

-(void) stopAudioRecorder
{
    if(_audioRecorder!=nil) {
        [_audioRecorder stopRecording];
        _audioRecorder = nil;
    }
}

-(void) reloadCaptureSession
{
    if([_captureSession isRunning]) {
        [_captureSession stopRunning];
    }
    
    _captureSession = [[AVCaptureSession alloc] init];
    [_captureSession setSessionPreset:AVCaptureSessionPreset1280x720];
    _videoDataOutput = [[AVCaptureVideoDataOutput alloc]init];
    _videoDataOutput.videoSettings=[NSDictionary dictionaryWithObject: [NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange] forKey: (id)kCVPixelBufferPixelFormatTypeKey];
    [_videoDataOutput setSampleBufferDelegate:self queue:MAIN_QUEUE];
    [_captureSession addOutput:_videoDataOutput];
    
    [self setupCamera];
}

-(void) setupCamera
{
    [_captureSession stopRunning];
    
    _isTorchOn = NO;
    
    if(self.inputDevice) {
        [_captureSession removeInput:self.inputDevice];
    }
    
    NSArray *devices = [AVCaptureDevice devices];
    AVCaptureDevice *frontCamera;
    AVCaptureDevice *backCamera;
    
    for (AVCaptureDevice *device in devices) {
        if ([device hasMediaType:AVMediaTypeVideo]) {
            if ([device position] == AVCaptureDevicePositionBack) {
                backCamera = device;
            }  else {
                frontCamera = device;
            }
        }
    }
    
    NSError *error = nil;
    
    if (_isFrontCamera) {
        _captureDevice = frontCamera;
        
        AVCaptureDeviceInput *frontFacingCameraDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:frontCamera error:&error];
        if (!error) {
            if ([_captureSession canAddInput:frontFacingCameraDeviceInput]) {
                [_captureSession addInput:frontFacingCameraDeviceInput];
                self.inputDevice = frontFacingCameraDeviceInput;
            }
        }
    } else {
        _captureDevice = backCamera;
        
        AVCaptureDeviceInput *backFacingCameraDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:backCamera error:&error];
        if (!error) {
            if ([_captureSession canAddInput:backFacingCameraDeviceInput]) {
                [_captureSession addInput:backFacingCameraDeviceInput];
                self.inputDevice = backFacingCameraDeviceInput;
            }
        }
    }
    
    // set frame rate
    //    [captureDevice lockForConfiguration:NULL];
    //    captureDevice.activeVideoMaxFrameDuration = CMTimeMake(1, LIVE_STREAM_FRAME_RATE);
    //    captureDevice.activeVideoMinFrameDuration = CMTimeMake(1, LIVE_STREAM_FRAME_RATE);
    //    [captureDevice unlockForConfiguration];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_captureSession startRunning];
    });
}


#pragma mark - network udp related
-(void) setupSocket
{
    DLog(@"setupSocket");
    _udpSocket = [[UDPSocket alloc] init];
    _udpSocket.delegate = self;
    [_udpSocket startConnectedToHostName:_publicIP port:LIVE_STREAM_SERVER_PORT];
    [self startTcpConn];
}

-(void) startTcpConn
{
    
    if(_tcpSocket.isConnected) {
        _isConnectingTcpSocket = NO;
        return;
    }
    
    if(_isConnectingTcpSocket) {
        return;
    }
    
    _isConnectingTcpSocket = YES;
    
    _tcpSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:MAIN_QUEUE];
    [_tcpSocket connectToHost:LIVE_STREAM_TCP_SERVER_IP onPort:20000 error:nil];
}

-(void) cleanupSocket
{
    if(_udpSocket!=nil) {
        _udpSocket.delegate = nil;
        [_udpSocket stop];
        _udpSocket = nil;
    }
    
    if(_tcpSocket!=nil) {
        _tcpSocket.delegate = nil;
        [_tcpSocket disconnect];
        _tcpSocket = nil;
    }
}

- (void) sendUDPPacket: (LIVE_STREAM_PACKET_TYPE)type
{
    if (type == LIVE_STREAM_PACKET_TYPE_LIKE) {
        return;
    }
    
    int liveID = [_livestreamID intValue];
    NSMutableData* data = CREATE_MUTABLE_DATA;
    u_int8_t packetType = type;
    [data appendBytes:&type length:sizeof(packetType)];
    [data appendBytes:&liveID length:sizeof(liveID)];
    
//    if (type == LIVE_STREAM_PACKET_TYPE_LIKE) {
//       
//        NSString *colorString = [[_broadcasterHexcolor stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
//        unsigned r, g, b;
//
//        [[NSScanner scannerWithString:[colorString substringFrom:0 to:2]] scanHexInt:&r];
//        [[NSScanner scannerWithString:[colorString substringFrom:2 to:4]] scanHexInt:&g];
//        [[NSScanner scannerWithString:[colorString substringFrom:4 to:6]] scanHexInt:&b];
//        [data appendBytes:&r length:sizeof(r)];
//        [data appendBytes:&g length:sizeof(g)];
//        [data appendBytes:&b length:sizeof(b)];
//    }
    
    [_udpSocket sendData:data];
}
-(void)endLivestream
{
    if (!_startPublish)
        return;
    
    [self cleanup];
    
    [DIALOG_MANAGER showLoadingView];
    float duration = CURRENT_TIMESTAMP - _liveStreamStartTime;
    
    [API_MANAGER endLiveStream:_livestreamID duration:duration withCompletion:^(BOOL success) {
        if (success) {
            // send end stream UDP packet
            int liveID = [_livestreamID intValue];
            NSMutableData* streamEndData = CREATE_MUTABLE_DATA;
            u_int8_t streamEndPacketType = LIVE_STREAM_PACKET_TYPE_STREAM_END;
            [streamEndData appendBytes:&streamEndPacketType length:sizeof(streamEndPacketType)];
            [streamEndData appendBytes:&liveID length:sizeof(liveID)];
            [_udpSocket sendData: streamEndData];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [API_MANAGER getLiveStreamInfo:_livestreamID withCompletion:^(BOOL success, LiveStreamObject *liveSteamInfo) {
                    [DIALOG_MANAGER hideLoadingView];
                    if(success){
                        _audioOnly = liveSteamInfo.audioOnly;
                        [_delegate updatelivestream:liveSteamInfo];
                    }
                    
                    [_delegate endlivestreamWithLivestreamInfo:liveSteamInfo];
                }];
                
            });
            
        }
    }];
    
}


# pragma mark - AVCaptureDataOutputSampleBufferDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    
    
    CVPixelBufferRef pixelBuffer = (CVPixelBufferRef) CMSampleBufferGetImageBuffer(sampleBuffer);
    
    [_displayFilter loadYUVData:pixelBuffer];
    
    [_glVideoOutputView bindDrawable];
    
    _displayFilter.targetViewPortSize = CGSizeMake(_glVideoOutputView.drawableWidth, _glVideoOutputView.drawableHeight);
    
    if(_isFrontCamera) {
        [_displayFilter setRotation:180.0 needHorizontalFlip:0];
    } else {
        [_displayFilter setRotation:180.0 needHorizontalFlip:1];
    }
    
    [_displayFilter render:RENDER_MODE_RGBA rebindFrameBuffer:0];
    
    [_glVideoOutputView display];
    
    if(_glVideoOutputView.alpha==0.0) {
        [UIView animateWithDuration:0.3 animations:^{
            _glVideoOutputView.alpha = 1.0;
        }];
    }
    
    if(!_startPublish) {
        return;
    }
    
    if(_updateCount < 1 && _lastUpdateTime < CURRENT_TIMESTAMP -2){
        
        _lastUpdateTime = CURRENT_TIMESTAMP;
        CIImage* ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer options:nil];
        ciImage = [ciImage imageByApplyingTransform:CGAffineTransformMakeRotation(-M_PI_2)];
        ciImage = [ciImage imageByCroppingToRect:CGRectMake(ciImage.extent.origin.x, ciImage.extent.origin.y+(ciImage.extent.size.height-ciImage.extent.size.width)/1.0, ciImage.extent.size.width, ciImage.extent.size.width)];
        
        // Finn: need to check
        CIContext *context = [CIContext contextWithOptions:nil];
        CGImageRef cgImageRef = [context createCGImage:ciImage fromRect:ciImage.extent];
        _coverImage = [UIImage imageWithCGImage:cgImageRef];
        CGImageRelease(cgImageRef);
        //        _coverImage = [UIImage imageWithCIImage:ciImage];
        
        _coverImageName = [SINGLETON generateRandomFileNameWithExtension:@"jpg"];
        [SINGLETON saveImageWithThumbnailImage:_coverImage andFileName:_coverImageName];
        MultiFileUploader* uploaded = [MultiFileUploader new];
        uploaded.fileNameArray = @[_coverImageName];
        
        [uploaded startUploadWithCallback:^(NSArray *fileNameArray, int code, float progress) {
            if(code==MULTI_UPLOAD_SUCCESS) {
                      [API_MANAGER updateLiveStreamInfo:@{@"liveStreamID":_livestreamID,@"coverPhoto":_coverImageName} withCompletion:^(BOOL success) {
                    if(success){
                        _updateCount ++;
                    }
                }];
            }else if(code==UPLOAD_FAIL){
                _coverImage =nil;
            }
        }];
    }
    
    
    _displayFilter.targetViewPortSize = CGSizeMake(640, 360);
    
    if(_isFrontCamera) {
        [_displayFilter setRotation:90.0 needHorizontalFlip:0];
    } else {
        [_displayFilter setRotation:90.0 needHorizontalFlip:0];
    }
    
    [_displayFilter render:RENDER_MODE_YUV rebindFrameBuffer:1];
    
    
    int value = arc4random() % 30;
    
    
    if (value>_TargetFps)
    {
        return;
    }
    
    if([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive){
        return;
    }
    
    
    if(_isEncoding || _audioOnly==1) {
        return;
        
    }
    
    _isEncoding = YES;
    // load data for video encoding
    dispatch_async(_videoEncodeQueue, ^{
        [_displayFilter loadYData:_yDataBuffer cbData:_cbDataBuffer crData:_crDataBuffer isSquare:1];
        NSData* encodedData = [_videoEncoder encodeWithYData:_yDataBuffer andCbData:_cbDataBuffer andCrData:_crDataBuffer];
        
        if(encodedData==nil || encodedData.length==0) {
            dispatch_async(MAIN_QUEUE, ^{
                _isEncoding = NO;
            });
            
            return;
        }
        
        
        
        dispatch_async(MAIN_QUEUE, ^{
            
            if(_chunkID>0) { // make sure socket connected and chunk has begun
                
                _videoSequenceNumber++;
                
                u_int8_t commandCode = COMMAND_CODE_CHUNK_DATA;
                u_int8_t packetDataType = CHUNK_DATA_TYPE_VIDEO;
                u_int8_t headerLength = 10;
                int32_t videoDataLength = (int) encodedData.length;
                int32_t videoAbsTimestamp = [self getLiveStreamAbsTime];
                
                u_int8_t* header = malloc(11);
                memcpy(header, &commandCode, 1);
                memcpy(header+1, &videoDataLength, 4);
                memcpy(header+5, &videoAbsTimestamp, 4);
                memcpy(header+9, &headerLength, 1);
                memcpy(header+10, &packetDataType, 1);
                
                _currentTcpDataTag++;
                
                //                [_tcpSocket writeData:[NSData dataWithBytesNoCopy:header length:(headerLength+1) freeWhenDone:YES] withTimeout:-1.0 tag:_currentTcpDataTag];
                //                [_tcpSocket writeData:encodedData withTimeout:-1.0 tag:_currentTcpDataTag];
                //
                if(_tcpSocket.isConnected){
                    [_tcpSocket writeData:[NSData dataWithBytesNoCopy:header length:(headerLength+1) freeWhenDone:YES] withTimeout:-1.0 tag:_currentTcpDataTag];
                    [_tcpSocket writeData:encodedData withTimeout:-1.0 tag:_currentTcpDataTag];
                }else{
                    [_queuedResendData insertObject:[NSData dataWithBytesNoCopy:header length:(headerLength+1) freeWhenDone:YES] atIndex:0];
                    [_queuedResendData insertObject:encodedData atIndex:0];
                }
                
                //                [_tcpSocket writeData:[NSData dataWithBytesNoCopy:header length:(headerLength+1) freeWhenDone:YES] withTimeout:-1.0 tag:_currentTcpDataTag];
                //                [_tcpSocket writeData:encodedData withTimeout:-1.0 tag:_currentTcpDataTag];
                //                                DLog(@"vf: currentTcpTag=%ld, chunkID=%d", _currentTcpDataTag, _chunkID);
            }
            
            _isEncoding = NO;
        });
    });
}

-(int) getLiveStreamAbsTime
{
    if(_livestreamStartDate==nil) {
        _livestreamStartDate = [NSDate date];
    }
    return (int) ([_livestreamStartDate timeIntervalSinceNow] * -1000.0);
}


# pragma mark - AudioRecorderDelegate
-(void)didOutputAudioData:(NSData *)audioData
{
    if(!_startPublish) {
        return;
    }
    
    dispatch_async(_audioEncodeQueue, ^{
        
        NSData* encodedAudioData = [_opusEncoder encodeAudioWithData:audioData];
        
        dispatch_async(MAIN_QUEUE, ^{
            
            if(!_tcpSocket.isConnected) {
                return ;
            }
            
            //            if(_tcpSocket.isConnected && [_queuedResendData count]!=0){
            //                for(int i = (int)[_queuedResendData count]-1 ;i>=0;i--){
            //                    NSData* data = [ _queuedResendData objectAtIndex:i];
            //                    [_tcpSocket writeData:[data copy] withTimeout:-1.0 tag:_currentTcpDataTag];
            //                }
            //                [_queuedResendData removeAllObjects];
            //            }
            //
            
            if(_audioSequenceNumber % NUM_OF_AUDIO_SAMPLES_PER_CHUNK==0) {
                
                _chunkID++;
                
                int32_t liveStreamID = [_livestreamID intValue];
                
                //                InfoLog(@"liveStreamID:%d",liveStreamID);
                
                // send 'COMMAND_CODE_START_CHUNK_UPLOAD'
                if(_audioSequenceNumber>0) {
                    
                    //                    DLog(@"COMMAND_CODE_START_CHUNK_UPLOAD");
                    u_int8_t commandCode = COMMAND_CODE_START_CHUNK_UPLOAD;
                    u_int8_t* command = malloc(1);
                    memcpy(command, &commandCode, 1);
                    
                    [_tcpSocket writeData:[NSData dataWithBytesNoCopy:command length:1 freeWhenDone:YES] withTimeout:-1.0 tag:_currentTcpDataTag];
                    
                    //                    DLog(@"START TCPSOCKET");
                    //                    if(_tcpSocket.isConnected){
                    //                        [_tcpSocket writeData:[NSData dataWithBytesNoCopy:command length:1 freeWhenDone:YES] withTimeout:-1.0 tag:_currentTcpDataTag];
                    //                    }else{
                    //                        [_queuedResendData insertObject:[NSData dataWithBytes:command length:1] atIndex:0];
                    //                    }
                }
                
                //                DLog(@"COMMAND_CODE_NEW_CHUNK_BEGIN");
                // send 'COMMAND_CODE_NEW_CHUNK_BEGIN'
                
                u_int8_t commandCode = COMMAND_CODE_NEW_CHUNK_BEGIN;
                
                u_int8_t* command = malloc(9);
                memcpy(command, &commandCode, 1);
                memcpy(command+1, &liveStreamID, 4);
                memcpy(command+5, &_chunkID, 4);
                DLog(@"livestreamID=%d",liveStreamID);
                [_tcpSocket writeData:[NSData dataWithBytesNoCopy:command length:9 freeWhenDone:YES] withTimeout:-1.0 tag:_currentTcpDataTag];
                
                //                if(_tcpSocket.isConnected){
                //                    [_tcpSocket writeData:[NSData dataWithBytesNoCopy:command length:9 freeWhenDone:YES] withTimeout:-1.0 tag:_currentTcpDataTag];
                //                }else{
                //                    [_queuedResendData insertObject:[NSData dataWithBytes:command length:9] atIndex:0];
                //                }
                
                //                DLog(@"af new: currentTcpTag=%ld, chunkID=%d", _currentTcpDataTag, _chunkID);
            }
            
            //            DLog(@"COMMAND_CODE_CHUNK_DATA");
            
            _audioSequenceNumber++;
            
            u_int8_t commandCode = COMMAND_CODE_CHUNK_DATA;
            u_int8_t packetDataType = CHUNK_DATA_TYPE_AUDIO;
            u_int8_t headerLength = 10;
            int32_t audioDataLength = (int) encodedAudioData.length;
            int32_t audioAbsTimestamp = [self getLiveStreamAbsTime];
            
            u_int8_t* header = malloc(11);
            memcpy(header, &commandCode, 1);
            memcpy(header+1, &audioDataLength, 4);
            memcpy(header+5, &audioAbsTimestamp, 4);
            memcpy(header+9, &headerLength, 1);
            memcpy(header+10, &packetDataType, 1);
            
            [_tcpSocket writeData:[NSData dataWithBytesNoCopy:header length:(headerLength+1) freeWhenDone:YES] withTimeout:-1.0 tag:_currentTcpDataTag];
            [_tcpSocket writeData:encodedAudioData withTimeout:-1.0 tag:_currentTcpDataTag];
            
            //
            //            if(_tcpSocket.isConnected){
            //                [_tcpSocket writeData:[NSData dataWithBytesNoCopy:header length:(headerLength+1) freeWhenDone:YES] withTimeout:-1.0 tag:_currentTcpDataTag];
            //                [_tcpSocket writeData:encodedAudioData withTimeout:-1.0 tag:_currentTcpDataTag];
            //            }else{
            //                [_queuedResendData insertObject:[NSData dataWithBytes:header length:(headerLength+1)] atIndex:0];
            //                [_queuedResendData insertObject:encodedAudioData atIndex:0];
            //            }
            
        });
    });
}

# pragma mark - GCDAsyncSocketDelegate
-(void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port
{
    DLog(@"TCP CONNECTED");
    _isConnectingTcpSocket = NO;
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    //    if(!_tcpSocket.isConnected) {
    //        return;
    //    }
    //
    
    DLog(@"_FPS=%d TCP DATA SENT with tag: %ld, currentTcpDataTag: %ld",_TargetFps, tag, _currentTcpDataTag);
    
    //
    /* Quality Adaptation Control */
    _lateDataCount = _currentTcpDataTag - tag;
    
    //    DLog(@"lateDataCount: %ld", lateDataCount);
    
    if(_lateDataCount>30) { // when video data queued at least over 1.0 second => lower the quality!
        
        //                    DLog(@"TargetFPS=%d  lateDataCount: %ld",_TargetFps, _lateDataCount);
        //
        //            int liveStreamQuality = [[DEFAULTS objectForKey:LIVESTREAM_QUALITY] intValue];
        //
        //            if(liveStreamQuality==LIVESTREAM_QUALITY_HIGH) {
        //                //            DLog(@"SWITCHED TO MEDIUM Q");
        //                [DEFAULTS setObject:INT_TO_NUMBER(LIVESTREAM_QUALITY_MEDIUM) forKey:LIVESTREAM_QUALITY];
        //                [DEFAULTS synchronize];
        //
        //                [self resetAllEncoders];
        //                [_tcpSocket disconnect];
        //            }
        //
        //            if(liveStreamQuality==LIVESTREAM_QUALITY_MEDIUM) {
        //
        //                [DEFAULTS setObject:INT_TO_NUMBER(LIVESTREAM_QUALITY_LOW) forKey:LIVESTREAM_QUALITY];
        //                [DEFAULTS synchronize];
        //
        //                [self resetAllEncoders];
        //                [_tcpSocket disconnect];
        //            }
    }
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
    DLog(@"TCP DISCONNECTED");
    
    _isConnectingTcpSocket = NO;
    
    // retry connection every 1.0 second
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self startTcpConn];
        //        [sock connectToHost:LIVE_STREAM_TCP_SERVER_IP onPort:20000 error:nil];
    });
}

# pragma mark - UDPSocketDelegate
- (void)echo:(UDPSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)addr
{
    //    DLog(@"broadcast echo");
    
    if(sock!=_udpSocket) {
        return;
    }
    
    if (!_startPublish) {
        return;
    }
    
    u_int8_t packetType = *((u_int8_t*) data.bytes);
    int32_t liveStreamID = *((int32_t*) (data.bytes+1));
    
    
    if(liveStreamID!=[_livestreamID intValue]) {
        return;
    }
    
    [_delegate broadcastReceiveUdpPacket:packetType data:data];
    
}

-(void) resetAllEncoders
{
    dispatch_async(_audioEncodeQueue, ^{
        _opusEncoder = [MyOpusEncoder new];
    });
    
    dispatch_async(_videoEncodeQueue, ^{
        _videoEncoder = [VP8VideoEncoder new];
    });
}


-(void)calDuration
{
    _duration=CURRENT_TIMESTAMP-_liveStreamStartTime;
}

@end
