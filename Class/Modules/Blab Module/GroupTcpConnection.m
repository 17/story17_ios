//
//  GroupTcpConnection.m
//  story17
//
//  Created by POPO Chen on 9/14/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "GroupTcpConnection.h"
#import "Constant.h"

@implementation GroupTcpConnection

-(id)init:(NSString*)groupLivestreamID
{
    self = [super init];
    if(self){
        _groupLiveStreamID = groupLivestreamID;
        _commandSequence = 0;
        _receivedData = CREATE_MUTABLE_DATA;
    }
    return self;
}

-(void) startTcpConn
{
    if(_tcpSocket.isConnected) {
        return;
    }
    _tcpSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:MAIN_QUEUE];
    [_tcpSocket connectToHost:BLAB_STREAM_SERVER_IP onPort:40000 error:nil];
}

-(void) cleanupSocket
{
    DLog(@"cleanupSocket");
    if(_tcpSocket!=nil) {
        _tcpSocket.delegate = nil;
        [_tcpSocket disconnect];
        _tcpSocket = nil;
    }
}


# pragma mark - GCDAsyncSocketDelegate
-(void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port
{
    DLog(@"didConnectToHost");
    [_delegate didConnectToHost:YES];
    [_tcpSocket readDataWithTimeout:-1 tag:0];
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag
{
        DLog(@"didWriteDataWithTag:%ld",tag);
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
    DLog(@"socketDidDisconnect");
    [_delegate didConnectToHost:NO];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self startTcpConn];
    });
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
//    DLog(@"Receive new Data Legth:%lu , Receive Data Length:%lu",(unsigned long)data.length,(unsigned long)_receivedData.length);
    
    [_receivedData appendData:data];
    int32_t packetLength = *((int32_t*) (_receivedData.bytes));
    
//    DLog(@"New Data Length :%lu",(unsigned long)_receivedData.length);
//    DLog(@"packetLength:%d",packetLength);
    
    while(_receivedData.length>=packetLength && _receivedData.length>0 && packetLength>0){
        
        NSData* newdata = [_receivedData subdataWithRange:NSMakeRange(0,packetLength)];
        u_int8_t packetType = *((u_int8_t*) newdata.bytes+4);
        
        switch (packetType) {
            case JSON_FORMAT_DATA:
            {
                NSString *string = [[NSString alloc] initWithData:[newdata subdataWithRange:NSMakeRange(5, packetLength-5)] encoding:NSUTF8StringEncoding];
                NSDictionary* infoDict = [string mj_JSONObject];
                NSString* commandStr = infoDict[@"command"];
                int type = 0;
                
                if([commandStr isEqualToString:@"commandAck"]){
                    type = COMMAND_ACK;
                }else if([commandStr isEqualToString:@"hold"]){
                    type = HOLD_GROUP_LIVESTREAM;
                }else if([commandStr isEqualToString:@"enter"]){
                    type = ENTER_GROUP_LIVESTREAM;
                }else if([commandStr isEqualToString:@"leave"]){
                    type = LEAVE_GROUP_LIVESTREAM;
                }else if([commandStr isEqualToString:@"comment"]){
                    type = SEND_COMMENT_ACTION;
                }else if([commandStr isEqualToString:@"block"]){
                    type = BLOCK_USER_ACTION;
                }else if([commandStr isEqualToString:@"requestJoin"]){
                    type = REQUEST_JOIN_ACTION;
                }else if([commandStr isEqualToString:@"rejectJoin"]){
                    type = REJECT_JOIN_ACTION;
                }else if([commandStr isEqualToString:@"cancelJoin"]){
                    type = CANCEL_JOIN_ACTION;
                }else if([commandStr isEqualToString:@"lockSeat"]){
                    type = LOCK_SEAT_ACTION;
                }else if([commandStr isEqualToString:@"acceptJoin"]){
                    type = ACCEPT_JOIN_ACTION;
                }else if([commandStr isEqualToString:@"unlockSeat"]){
                    type = UNLOCK_SEAT_ACTION;
                }else if([commandStr isEqualToString:@"kickOut"]){
                    type = KICK_OUT_ACTION;
                }else if([commandStr isEqualToString:@"endStream"]){
                    type = LIVESTREAM_ENDED;
                }else if([commandStr isEqualToString:@"quitBroadcast"]){
                    type = QUIT_BROADCAST_ACTION;
                }
                
                [_delegate didReceiveJsonFormatData:type infoDict:infoDict];
                
                break;
            }
            case VIDEO_FRAME_DATA:
            {
                GroupVideoFrame* groupVF = [GroupVideoFrame getGroupVideoFrameFromData:newdata];
                [_delegate didReceiveVideoData:groupVF];
                
                break;
            }
            case AUDIO_FRAME_DATA:
            {
                GroupAudioFrame* groupAF = [GroupAudioFrame getGroupAudioFrameFromData:newdata];
                [_delegate didReceiveAudioData:groupAF];
                
                break;
            }
            case LIKE_DATA:
            {
                break;
            }
            case CHUNK_READY_DATA:
            {
                ChunkReadyObject* chunkObj = [ChunkReadyObject getChunkReadyData:newdata];
//                DLog(@"groupLiveStreamID %d, chunkID %d, prefetchTargetChunkID %d, latestChunkID %d", chunkObj.groupLiveStreamID, chunkObj.chunkID, chunkObj.prefetchTargetChunkID, chunkObj.latestChunkID);
                [_delegate didReceiveChunkReadyData:chunkObj];
                
                break;
            }
                
            default:
            {
                DLog(@"packetType %d parsing error", packetType);
                
                break;
            }
        }
        
        _receivedData  = [[_receivedData subdataWithRange:NSMakeRange(packetLength, _receivedData.length-packetLength)] mutableCopy];
        packetLength = *((int32_t*) (_receivedData.bytes));
    }
    
    /* read data Info */
    [_tcpSocket readDataWithTimeout:-1 tag:0];
}


-(NSMutableDictionary *)jsonBasicInfo
{
    return [@{@"userID":MY_USER_ID,@"commandSequence":INT_TO_STRING(_commandSequence),@"groupLiveStreamID":_groupLiveStreamID} mutableCopy];
}

-(void)sendTcpPacket:(BLAB_ACTION_TYPE)type dict:(NSDictionary*)infoDict
{
    if(type == UPLOAD_VIDEO_FRAME_DATA || type== UPLOAD_AUDIO_FRAME_DATA ){
        return;
    }
    
    NSMutableDictionary* dict = [self jsonBasicInfo];
    [dict addEntriesFromDictionary:infoDict];
    
    if(type == LIVESTREAM_ENDED){
        [dict removeObjectForKey:@"userID"];
    }
    
    NSData *infoData = [TO_JSON(dict) dataUsingEncoding:NSUTF8StringEncoding];
    
    u_int8_t packetType = JSON_FORMAT_DATA;
    u_int32_t dataPacketLength = (u_int32_t)infoData.length+5;
    
    u_int8_t* command;
    command = malloc(5+dataPacketLength);
    memcpy(command, &dataPacketLength, 4);
    memcpy(command+4, &packetType, 1);
    
    NSMutableData *commandData = [NSMutableData dataWithBytesNoCopy:command length:5 freeWhenDone:YES];
    [commandData appendData:infoData];
    [_tcpSocket writeData:commandData withTimeout:-1.0 tag:_commandSequence];
    
    _commandSequence++;
}

-(void)sendVideoData:(NSData*)encodedData videoSequence:(u_int8_t)videoSequence absTime:(int32_t)absTime channelID:(u_int8_t)channel
{
    NSMutableData *videoData = CREATE_MUTABLE_DATA;
    
    /* Packet Info */
    NSDictionary* jsonDict = @{@"userID":MY_USER_ID};
    NSData *jsonData = [TO_JSON(jsonDict) dataUsingEncoding:NSUTF8StringEncoding];
    int32_t jsonLength = (int)jsonData.length;
    
    /* Video Info */
    int32_t videoDataLength = (int)encodedData.length;
    u_int8_t channelID = channel;
    u_int32_t videoAbsTimestamp = absTime;
    int32_t serverAbsTimestamp = (int)0;
    u_int8_t* videoInfo;
    
    u_int8_t packetType = VIDEO_FRAME_DATA;
    int32_t dataPacketLength = jsonLength+videoDataLength+27;
    int32_t commandSequence = _commandSequence;
    u_int8_t* command;
    
    command = malloc(13);
    memcpy(command, &dataPacketLength, 4);
    memcpy(command+4, &packetType, 1);
    memcpy(command+5, &commandSequence, 4);
    memcpy(command+9, &jsonLength, 4);
    
    [videoData appendData:[NSData dataWithBytesNoCopy:command length:13 freeWhenDone:YES]];
    [videoData appendData:jsonData];
    
    videoInfo = malloc(14);
    memcpy(videoInfo, &videoDataLength, 4);
    memcpy(videoInfo+4, &channelID, 1);
    memcpy(videoInfo+5, &videoSequence, 1);
    memcpy(videoInfo+6, &videoAbsTimestamp, 4);
    memcpy(videoInfo+10, &serverAbsTimestamp, 4);
    
    [videoData appendData:[NSData dataWithBytesNoCopy:videoInfo length:14 freeWhenDone:YES]];
    [videoData appendData:encodedData];
    
    [_tcpSocket writeData:videoData withTimeout:-1.0 tag:_commandSequence];
    
    _commandSequence++;
}

-(void)sendAudioData:(NSData*)encodedData audioSequence:(u_int8_t)audioSequence absTime:(int32_t)absTime channelID:(u_int8_t)channel
{
    NSMutableData *audioData = CREATE_MUTABLE_DATA;
    
    /* Packet Info */
    NSDictionary* jsonDict = @{@"userID":MY_USER_ID};
    NSData *jsonData = [TO_JSON(jsonDict) dataUsingEncoding:NSUTF8StringEncoding];
    u_int32_t jsonLength = (u_int32_t)jsonData.length;
    
    /* Audio Info */
    int32_t audioDataLength = (int)encodedData.length;
    u_int8_t channelID = channel;
    int32_t audioAbsTimestamp = (int)absTime;
    int32_t serverAbsTimestamp = (int)0;

    u_int8_t* audioInfo;
    
    u_int8_t packetType = AUDIO_FRAME_DATA;
    int32_t dataPacketLength = jsonLength+audioDataLength+27;
    int32_t commandSequence = _commandSequence;
    u_int8_t* command;
    
    command = malloc(13);
    memcpy(command, &dataPacketLength, 4);
    memcpy(command+4, &packetType, 1);
    memcpy(command+5, &commandSequence, 4);
    memcpy(command+9, &jsonLength, 4);
    
    [audioData appendData:[NSData dataWithBytesNoCopy:command length:13 freeWhenDone:YES]];
    [audioData appendData:jsonData];
    
    audioInfo = malloc(14);
    memcpy(audioInfo, &audioDataLength, 4);
    memcpy(audioInfo+4, &channelID, 1);
    memcpy(audioInfo+5, &audioSequence, 1);
    memcpy(audioInfo+6, &audioAbsTimestamp, 4);
    memcpy(audioInfo+10, &serverAbsTimestamp, 4);
    
    [audioData appendData:[NSData dataWithBytesNoCopy:audioInfo length:14 freeWhenDone:YES]];
    [audioData appendData:encodedData];
    
//    DLog(@"send Audio Data:%d commandSequence:%d",CURRENT_TIMESTAMP,commandSequence);

    [_tcpSocket writeData:audioData withTimeout:-1.0 tag:_commandSequence];
    _commandSequence++;
}

#pragma mark - Send TCP
-(void)holdLiveStream{
    
    InfoLog(@"Send TCP to SERVER - type:ENTER_GROUP_LIVESTREAM");
    [self sendTcpPacket:HOLD_GROUP_LIVESTREAM dict:@{@"command":@"hold",@"accesstoken":MY_ACCESS_TOKEN}];
}

-(void)enterLiveStream{
    
    InfoLog(@"Send TCP to SERVER - type:ENTER_GROUP_LIVESTREAM");
    [self sendTcpPacket:ENTER_GROUP_LIVESTREAM dict:@{@"command":@"enter",@"accesstoken":MY_ACCESS_TOKEN}];
}

-(void)leaveLiveStream:(int)duration{
    InfoLog(@"Send TCP to SERVER - type:LEAVE_GROUP_LIVESTREAM");
    [self sendTcpPacket:LEAVE_GROUP_LIVESTREAM dict:@{@"command":@"leave",@"duration":INT_TO_STRING(duration)}];
}

-(void)sendCommentAction:(NSString*)openID picture:(NSString*)picture message:(NSString*)message colorCode:(NSString*)colorCode{
    InfoLog(@"Send TCP to SERVER - type:SEND_COMMENT_ACTION");
    [self sendTcpPacket:LEAVE_GROUP_LIVESTREAM dict:@{@"command":@"comment",@"openID":openID,@"message":message,@"picture":picture,@"colorCode":colorCode}];
}

-(void)blockUserAction:(NSString*)blockUserID{
    InfoLog(@"Send TCP to SERVER - type:BLOCK_LIKE_ACTION");
    [self sendTcpPacket:BLOCK_USER_ACTION dict:@{@"command":@"block",@"blockUserID":blockUserID}];
}

-(void)quitBroadCaterAction:(int)channel picture:(NSString*)picture{
    
    InfoLog(@"Send TCP to SERVER - type:QUIT_BROADCAST_ACTION");
    [self sendTcpPacket:QUIT_BROADCAST_ACTION dict:@{@"command":@"quitBroadcast",@"channelID":[NSString stringWithFormat:@"%d",channel],@"picture":picture}];
}
-(void)livestreamEndedAction{
    
    InfoLog(@"Send TCP to SERVER - type:LIVESTREAM_ENDED");
    [self sendTcpPacket:LIVESTREAM_ENDED dict:@{@"command":@"endStream"}];
}


-(void)requestJoinAction:(NSString*)openID picture:(NSString*)picture{
    InfoLog(@"Send TCP to SERVER - type:REQUEST_JOIN_ACTION");
    [self sendTcpPacket:REQUEST_JOIN_ACTION dict:@{@"command":@"requestJoin",@"openID":openID,@"picture":picture}];
}
-(void)rejectJoinAction:(NSString*)userID{
    InfoLog(@"Send TCP to SERVER - type:REJECT_JOIN_ACTION");
    [self sendTcpPacket:REJECT_JOIN_ACTION dict:@{@"command":@"rejectJoin",@"targetUserID":userID}];
}
-(void)cancelJoinAction:(NSString*)openID picture:(NSString*)picture{
    
    InfoLog(@"Send TCP to SERVER - type:CANCEL_JOIN_ACTION");
    [self sendTcpPacket:CANCEL_JOIN_ACTION dict:@{@"command":@"cancelJoin",@"openID":openID,@"picture":picture}];
}
-(void)acceptJoinAction:(NSString*)userID channelID:(int)channel{
    
    InfoLog(@"Send TCP to SERVER - type:ACCEPT_JOIN_ACTION");
    [self sendTcpPacket:ACCEPT_JOIN_ACTION dict:@{@"command":@"acceptJoin",@"targetUserID":userID,@"channelID": [NSString stringWithFormat:@"%d",channel]}];
}
-(void)lockSeatAction:(int)lockNums{
    
    InfoLog(@"Send TCP to SERVER - type:LOCK_SEAT_ACTION");
    [self sendTcpPacket:LOCK_SEAT_ACTION dict:@{@"command":@"lockSeat",@"locknums":[NSString stringWithFormat:@"%d",lockNums]}];
}
-(void)unlockSeatAction:(int)unlockNums{
    
    InfoLog(@"Send TCP to SERVER - type:UNLOCK_SEAT_ACTION");
    [self sendTcpPacket:UNLOCK_SEAT_ACTION dict:@{@"command":@"unlockSeat",@"unlocknums":[NSString stringWithFormat:@"%d",unlockNums]}];
}
-(void)kickOutAction:(NSString*)userID{
    
    InfoLog(@"Send TCP to SERVER - type:KICK_OUT_ACTION");
    [self sendTcpPacket:KICK_OUT_ACTION dict:@{@"command":@"kickOut",@"targetUserID":userID}];
}


@end
