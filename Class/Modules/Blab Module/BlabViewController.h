#import "Constant.h"
#import "VP8VideoDecoder.h"
#import "VP8VideoEncoder.h"
#import "AudioFrameObject.h"
#import "VideoFrameObject.h"
#import "UIPlaceholderTextView.h"
#import "LiveViewScrollView.h"
#import "LiveStreamTableView.h"
#import "LiveStreamChatMessage.h"
#import "MessageInputBar.h"
#import "MyFilter.h"
#import "PCMAudioPlayer.h"
#import "PCMAudioRecorder.h"
#import "UDPSocket.h"
#import "MyOpusDecoder.h"
#import "MyOpusEncoder.h"
#import "GCDAsyncSocket.h"
#import "ChunkDataObject.h"
#import "LiveStreamConnectionWatcher.h"
#import "BlabstreamBroadcasterController.h"
#import "LiveShowMediaPlayer.h"
#import "PCMAudioPlayer.h"
#import "PCMAudioRecorder.h"
#import "MyOpusEncoder.h"
#import "GCDAsyncSocket.h"
#import "ChunkDataObject.h"
#import "MyScene.h"
#import "MyFilter.h"
#import "GroupTcpConnection.h"
#import "LiveShowMediaPlayer.h"
#import "GroupUserObject.h"
#import "LiveShowMediaView.h"

#import "GroupChunkDownloader.h" /* For Watcher */

#define LIVE_SHOW_MODE_HOLDER 0
#define LIVE_SHOW_MODE_BROADCAST 1
#define LIVE_SHOW_MODE_WATCH 2

#import "LiveShowMediaPlayer.h"
#import "GroupMediaController.h"

// UI control constants
//typedef enum {
//    LIVE_STREAM_CHAT_MODE_EVERYONE,
//    LIVE_STREAM_CHAT_MODE_FOLLOWED_USERS,
//    LIVE_STREAM_CHAT_MODE_MAX
//} LIVE_STREAM_CHAT_MODE;

@interface BlabViewController : UIViewController < LiveStreamTableViewDataSource, LiveStreamTableViewDelegate, BlabBroadcasterDelegate, GroupTcpConnectionDelegate, LiveShowMediaPlayerDelegate, GroupChunkDownloaderDelegate, PCMAUAudioPlayerDelegate, LiveShowMediaViewDelegate, GroupMediaControllerDelegate>

/* Livestream Mode */
@property int32_t liveStreamMode;
@property (nonatomic, strong) NSString* livestreamID;

/* UI Related */
@property (nonatomic,strong) LiveStreamTableView* lvTable;
@property (nonatomic,strong) LiveViewScrollView* viewerScrollView;
@property (nonatomic,strong) NSMutableArray* viewerIDArray;
@property (nonatomic,strong) NSMutableDictionary* viewerDict;
@property (nonatomic,strong) NSMutableArray *liveStreamCommentObjects;
@property (nonatomic,strong) UIView* cover;
@property (nonatomic,strong) LiveShowMediaView* liveView1;
@property (nonatomic,strong) LiveShowMediaView* liveView2;
@property (nonatomic,strong) LiveShowMediaView* liveView3;
@property (nonatomic,strong) LiveShowMediaView* liveView4;

/* Fetching data status */
@property (nonatomic) BOOL noMoreWatcher;
@property (nonatomic) BOOL isFirstFetching;
@property (nonatomic) BOOL isFirstFetchInfo;
@property (nonatomic) BOOL isFetching;
@property (nonatomic) BOOL isFetchingInfo;
@property (nonatomic) BOOL isBackground;



@property (nonatomic) BOOL requestingJoin;
@property (nonatomic) int mySeat;
@property (nonatomic) int avaliableSeats;

@property (nonatomic) int lastCommentTime;
@property (nonatomic) int lastSelfCommentTime;

@property (nonatomic) float cellTotalHeight;
@property(nonatomic, strong) MessageInputBar* messageInputBar;
@property(nonatomic, strong) NSString* searchString;
@property(nonatomic) BOOL POP;
@property(nonatomic,strong) NSMutableArray* privacySettingUser;
@property(nonatomic,strong) GroupTcpConnection* tcpSocket;
@property(nonatomic,strong) GroupUserObject* requestUser;
@property(nonatomic,strong)NSString * colorcode;


@property(nonatomic) int requesterIndex;
@property (nonatomic,strong) UIButton* testingBtn;

@end


