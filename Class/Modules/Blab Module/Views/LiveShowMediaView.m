//
//  LiveShowMediaView.m
//  story17
//
//  Created by POPO Chen on 10/27/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "LiveShowMediaView.h"

@implementation LiveShowMediaView

-(id)initWithChannelID:(int)channelID liveShowMode:(int32_t)liveShowMode
{
    self = [super init];
    if(self){
        _isPlaying=NO;
        _player=[GroupUserObject new];
        _player.userID=@"";
        _mediaPlayer=[LiveShowMediaPlayer new];
        _liveStreamMode=liveShowMode;
        _channelID=channelID;
        _mediaPlayer.channelID=channelID;

        _bgView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, SCREEN_WIDTH/2)];
        if (channelID==1||channelID==4) {
            [_bgView setBackgroundColor:[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1]];
        }else{
            [_bgView setBackgroundColor:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1]];
        }

        [self addSubview:_bgView];
        
        
        _lockBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        _lockBtn.frame = CGRectMake(SCREEN_WIDTH*3/4-20, 50+SCREEN_WIDTH*3/4, 40, 40);
        _lockBtn.center=CGPointMake(SCREEN_WIDTH/4, SCREEN_WIDTH/4);
        [_lockBtn setBackgroundImage:[UIImage imageNamed:@"group_open"] forState:UIControlStateNormal];
        [_lockBtn addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_lockBtn];

        
        
        _lockLabel=[[UILabel alloc]initWithFrame:CGRectMake(_lockBtn.frame.origin.x, _lockBtn.frame.origin.y, 30, 30)];
        [_lockLabel setText:LOCALIZE(@"Open_for_Join")];
        [_lockLabel setTextColor:[UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1]];
        [_lockLabel sizeToFit];
        [_lockLabel setTextAlignment:NSTextAlignmentCenter];
        _lockLabel.center=CGPointMake(_lockBtn.frame.origin.x+20, _lockBtn.frame.origin.y+60);
        [self addSubview:_lockLabel];
        
        
        if (channelID!=2) {
            [self hideLockBtn];
        }
        
        _kickConfirmLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        [_kickConfirmLabel setText:LOCALIZE(@"Sure_to_close?")];
        [_kickConfirmLabel setTextColor:WHITE_COLOR];
        [_kickConfirmLabel sizeToFit];
        [_kickConfirmLabel setTextAlignment:NSTextAlignmentCenter];
        _kickConfirmLabel.center=CGPointMake(SCREEN_WIDTH*1/4,SCREEN_WIDTH*1/4-20);
        _kickConfirmLabel.hidden=YES;
        [self addSubview:_kickConfirmLabel];
        
        
        _openIDLabel=[[UILabel alloc]initWithFrame:CGRectMake(5,SCREEN_WIDTH/2-25, 0, 30)];
        [_openIDLabel setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.5]];
        [self addSubview:_openIDLabel];
        
        _requestJoinWatcherView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, SCREEN_WIDTH/2)];
        UILabel* requestJoinLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/4, 30)];
        [requestJoinLabel setText:LOCALIZE(@"請求加入中")];
        [requestJoinLabel setFont:SYSTEM_FONT_WITH_SIZE(18)];
        [requestJoinLabel setTextColor:WHITE_COLOR];
        [requestJoinLabel sizeToFit];
        requestJoinLabel.center=CGPointMake(SCREEN_WIDTH/4, SCREEN_WIDTH/8);
        [_requestJoinWatcherView addSubview:requestJoinLabel];
        
        UIActivityIndicatorView* progressView = [UIActivityIndicatorView new];
        [progressView setTintColor:WHITE_COLOR];
        [progressView setColor:WHITE_COLOR];
        [progressView startAnimating];
        progressView.center=CGPointMake(SCREEN_WIDTH/4, SCREEN_WIDTH/4);
        [_requestJoinWatcherView addSubview:progressView];
        
        
        _cancelRequestWatcherbtn= [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancelRequestWatcherbtn setBackgroundImage:[UIImage imageNamed:@"btn_whiteline"] forState:UIControlStateNormal];
        [_cancelRequestWatcherbtn setBackgroundImage:[UIImage imageNamed:@"btn_whiteline_down"] forState:UIControlStateHighlighted];
        [_cancelRequestWatcherbtn setTitle:LOCALIZE(@"Cancel") forState:UIControlStateNormal];
        [_cancelRequestWatcherbtn setTintColor:WHITE_COLOR];
        _cancelRequestWatcherbtn.titleLabel.adjustsFontSizeToFitWidth = true;
        _cancelRequestWatcherbtn.frame = CGRectMake(0, 0 , SCREEN_WIDTH*2/7, 30);
        _cancelRequestWatcherbtn.center=CGPointMake(SCREEN_WIDTH*1/4, SCREEN_WIDTH*3/8);
        [_cancelRequestWatcherbtn addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        [_requestJoinWatcherView addSubview:_cancelRequestWatcherbtn];
        _requestJoinWatcherView.hidden=YES;
        
        [self addSubview:_requestJoinWatcherView];
        
        
        
        _requestDenyView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, SCREEN_WIDTH/2)];
        UILabel* requestJoinFailLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/4, 30)];
        [requestJoinFailLabel setText:LOCALIZE(@"無法加入")];
        [requestJoinFailLabel setFont:SYSTEM_FONT_WITH_SIZE(18)];
        [requestJoinFailLabel setTextColor:WHITE_COLOR];
        [requestJoinFailLabel sizeToFit];
        requestJoinFailLabel.center=CGPointMake(SCREEN_WIDTH/4, SCREEN_WIDTH/5);
        [_requestDenyView addSubview:requestJoinFailLabel];
        
        
        _denyConfirmBtn= [UIButton buttonWithType:UIButtonTypeCustom];
        [_denyConfirmBtn setBackgroundImage:[UIImage imageNamed:@"btn_whiteline"] forState:UIControlStateNormal];
        [_denyConfirmBtn setBackgroundImage:[UIImage imageNamed:@"btn_whiteline_down"] forState:UIControlStateHighlighted];
        [_denyConfirmBtn setTitle:LOCALIZE(@"OK") forState:UIControlStateNormal];
        [_denyConfirmBtn setTintColor:WHITE_COLOR];
        _denyConfirmBtn.titleLabel.adjustsFontSizeToFitWidth = true;
        _denyConfirmBtn.frame = CGRectMake(0, 0 , SCREEN_WIDTH*2/7, 30);
        _denyConfirmBtn.center=CGPointMake(SCREEN_WIDTH*1/4, SCREEN_WIDTH*3/8);
        [_denyConfirmBtn addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        [_requestDenyView addSubview:_denyConfirmBtn];

        _requestDenyView.hidden=YES;
        
        
        [self addSubview:_requestDenyView];
        
        
        
        
        _requestJoinHolderView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, SCREEN_WIDTH/2)];
        UILabel* requestJoinHolderLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/4, 30)];
        [requestJoinHolderLabel setText:LOCALIZE(@"要求加入")];
        [requestJoinHolderLabel setFont:SYSTEM_FONT_WITH_SIZE(18)];
        [requestJoinHolderLabel setTextColor:WHITE_COLOR];
        [requestJoinHolderLabel sizeToFit];
        requestJoinHolderLabel.center=CGPointMake(SCREEN_WIDTH/4, SCREEN_WIDTH*2/7+5);
        [_requestJoinHolderView addSubview:requestJoinHolderLabel];
        
        _requestOpenID=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/4, 35)];
        [_requestOpenID setFont:SYSTEM_FONT_WITH_SIZE(18)];
        [_requestOpenID setTextColor:WHITE_COLOR];
        [_requestJoinHolderView addSubview:_requestOpenID];

        
        _requestHeader = [[UIImageView alloc]initWithFrame:CGRectMake(5,0, 40, 40)];
        _requestHeader.layer.masksToBounds = YES;
        _requestHeader.layer.cornerRadius = 40/2.0;
        _requestHeader.center=CGPointMake(SCREEN_WIDTH/4, 40);
        [_requestHeader setBackgroundColor:WHITE_COLOR];
        //        [imageView setBackgroundColor:randomColor];
        
        [_requestJoinHolderView addSubview:_requestHeader];
        _requestJoinHolderView.hidden=YES;
        
        [self addSubview:_requestJoinHolderView];
        
        
        _applyYesBtn = [UIButton buttonWithType:UIButtonTypeCustom];//要求加入
        _applyYesBtn.frame = CGRectMake(SCREEN_WIDTH/7, SCREEN_WIDTH*4/11, 30, 30);
        [_applyYesBtn setBackgroundImage:[UIImage imageNamed:@"apply_yes"] forState:UIControlStateNormal];
        [_applyYesBtn setBackgroundImage:[UIImage imageNamed:@"apply_yes_down"] forState:UIControlStateHighlighted];
        [_applyYesBtn addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        
        _applyNoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _applyNoBtn.frame = CGRectMake(SCREEN_WIDTH*2/7, SCREEN_WIDTH*4/11, 30, 30);
        [_applyNoBtn setBackgroundImage:[UIImage imageNamed:@"apply_no"] forState:UIControlStateNormal];
        [_applyNoBtn setBackgroundImage:[UIImage imageNamed:@"apply_no_down"] forState:UIControlStateHighlighted];
        [_applyNoBtn addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [_requestJoinHolderView addSubview:_applyYesBtn];
        [_requestJoinHolderView addSubview:_applyNoBtn];
        
        
        _rollRightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        _rollRightBtn.frame = CGRectMake(SCREEN_WIDTH/2-35, SCREEN_WIDTH*2/11, 30, 30);
        [_rollRightBtn setBackgroundImage:[UIImage imageNamed:@"arrow_next"] forState:UIControlStateNormal];
        [_rollRightBtn setBackgroundImage:[UIImage imageNamed:@"arrow_next_down"] forState:UIControlStateHighlighted];
        [_rollRightBtn addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        _rollRightBtn.hidden=YES;
        
        _rollLeftBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        _rollLeftBtn.frame = CGRectMake(5, SCREEN_WIDTH*2/11, 30, 30);
        [_rollLeftBtn setBackgroundImage:[UIImage imageNamed:@"arrow_back"] forState:UIControlStateNormal];
        [_rollLeftBtn setBackgroundImage:[UIImage imageNamed:@"arrow_back_down"] forState:UIControlStateHighlighted];
        [_rollLeftBtn addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        _rollLeftBtn.hidden=YES;
        
        [_requestJoinHolderView addSubview:_rollLeftBtn];
        [_requestJoinHolderView addSubview:_rollRightBtn];
        

        
        [self addSubview:_mediaPlayer.glVideoOutputView];
        if (channelID!=1) {
            _mediaPlayer.glVideoOutputView.hidden=YES;
        }
        
        
        _openIDLabel.userInteractionEnabled=YES;
        [_openIDLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(panGesture:)]];
        
        _seatCoverView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, SCREEN_WIDTH/2)];
        [_seatCoverView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.7]];
        [self addSubview:_seatCoverView];
        _seatCoverView.hidden=YES;

        _kickBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        _kickBtn.frame = CGRectMake(SCREEN_WIDTH/2-24,10, 20, 20);
        [_kickBtn setBackgroundImage:[UIImage imageNamed:@"apply_no"] forState:UIControlStateNormal];
        [_kickBtn setBackgroundImage:[UIImage imageNamed:@"apply_no_down"] forState:UIControlStateHighlighted];
        [_kickBtn addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        _kickBtn.hidden=YES;
        
        
        _kickYesBtn=[UIButton buttonWithType:UIButtonTypeCustom];//踢除確認
        _kickYesBtn.frame = CGRectMake(SCREEN_WIDTH/7, SCREEN_WIDTH*3/11, 30, 30);
        [_kickYesBtn setBackgroundImage:[UIImage imageNamed:@"apply_yes"] forState:UIControlStateNormal];
        [_kickYesBtn setBackgroundImage:[UIImage imageNamed:@"apply_yes_down"] forState:UIControlStateHighlighted];
        [_kickYesBtn addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        _kickYesBtn.hidden=YES;
        
        _kickNoBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        _kickNoBtn.frame = CGRectMake(SCREEN_WIDTH*2/7, SCREEN_WIDTH*3/11, 30, 30);
        [_kickNoBtn setBackgroundImage:[UIImage imageNamed:@"apply_no"] forState:UIControlStateNormal];
        [_kickNoBtn setBackgroundImage:[UIImage imageNamed:@"apply_no_down"] forState:UIControlStateHighlighted];
        [_kickNoBtn addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        _kickNoBtn.hidden=YES;
        [self addSubview:_kickBtn];
        [self addSubview:_kickYesBtn];
        [self addSubview:_kickNoBtn];
        
    }
    return self;
}


-(void)didClickButton:(UIButton*)sender
{
    if([sender isEqual:_lockBtn]){
        if (_liveStreamMode==GROUP_HOSTER) {
            NSMutableDictionary *infoDict = [NSMutableDictionary dictionary];
            if (_lockBtn.selected==NO) {
                [self lockSeat];
                infoDict[@"locknums"]=[NSString stringWithFormat:@"%d",_channelID];
                [_delegate sendTcpRequest:LOCK_SEAT_ACTION infoDict:infoDict];
            }else{
                [self unlockSeat];
                infoDict[@"unlocknums"]=[NSString stringWithFormat:@"%d",_channelID];

                [_delegate sendTcpRequest:UNLOCK_SEAT_ACTION infoDict:infoDict];
            }
        }else if(_liveStreamMode==GROUP_WATCHER){
            if (_lockBtn.selected==NO) {
                NSMutableDictionary *infoDict = [NSMutableDictionary dictionary];
                _requestJoinWatcherView.hidden=NO;
                [self hideLockBtn];
                [_delegate sendTcpRequest:REQUEST_JOIN_ACTION infoDict:infoDict];
            }
        }
    }else if ([sender isEqual:_cancelRequestWatcherbtn]){
        _requestJoinWatcherView.hidden=YES;
        [self showLockBtn];
        NSMutableDictionary *infoDict = [NSMutableDictionary dictionary];
        [_delegate sendTcpRequest:CANCEL_JOIN_ACTION infoDict:infoDict];
    }else if ([sender isEqual:_denyConfirmBtn]){
        _requestDenyView.hidden=YES;
    }else if ([sender isEqual:_applyYesBtn]){
        [_delegate applyRequestYes];
    }else if ([sender isEqual:_applyNoBtn]){
        [_delegate applyRequestNo];
    }else if ([sender isEqual:_rollRightBtn]){
        [_delegate rollRightAction];
    }else if ([sender isEqual:_rollLeftBtn]){
        [_delegate rollLeftAction];
    }else if ([sender isEqual:_kickBtn]){
        if (_liveStreamMode==GROUP_HOSTER) {//kick
            [self kickWarning];
        }
    }else if ([sender isEqual:_kickYesBtn]){
        [self kickYesAction];
    }else if ([sender isEqual:_kickNoBtn]){
        [self kickNoAction];
    }
}

-(void)applyRequestYes:(GroupUserObject*)user
{
    _isPlaying=YES;
    _kickBtn.hidden=NO;
    _player.userID=user.userID;
    _player.picture=user.picture;
    _player.openID=user.openID;
    [_openIDLabel setText:_player.openID];
    _openIDLabel.hidden=NO;
    [_openIDLabel sizeToFit];
    [_openIDLabel setTextColor:WHITE_COLOR];
    _openIDLabel.frame=CGRectMake(_openIDLabel.frame.origin.x,_openIDLabel.frame.origin.y, _openIDLabel.frame.size.width+15, _openIDLabel.frame.size.height);
    [_openIDLabel setTextAlignment:NSTextAlignmentCenter];
    [self showGlView];
}




-(void)showGlView
{
    [self hideLockBtn];
    _kickBtn.hidden=NO;
    _requestJoinHolderView.hidden=YES;
    _requestDenyView.hidden=YES;
    _mediaPlayer.glVideoOutputView.hidden=NO;
}

-(void)hideGlview
{
    [self showLockBtn];
    _kickBtn.hidden=YES;
    _mediaPlayer.glVideoOutputView.hidden=YES;
}

-(void)showLockBtn
{
    _lockLabel.hidden=NO;
    _lockBtn.hidden=NO;
}

-(void)hideLockBtn
{
    _lockBtn.hidden=YES;
    _lockLabel.hidden=YES;
    
}
             
-(void)kickWarning
{
    _kickNoBtn.hidden=NO;
    _kickYesBtn.hidden=NO;
    _seatCoverView.hidden=NO;
    _kickConfirmLabel.hidden=NO;
}

-(void)kickYesAction
{
    _openIDLabel.hidden=YES;
    _kickConfirmLabel.hidden=YES;
    _kickNoBtn.hidden=YES;
    _kickYesBtn.hidden=YES;
    _seatCoverView.hidden=YES;
    _isPlaying=NO;
    [_delegate kickAction:_player channelID:_channelID];
    _player.userID=@"";
    if (_liveStreamMode==GROUP_PARTICIPANT) {
        _mediaPlayer.userStatus=GROUP_WATCHER;
    }
    [self hideGlview];
//    [_delegate kickAction];

}
-(void)kickNoAction
{
    _kickConfirmLabel.hidden=YES;
    _kickNoBtn.hidden=YES;
    _kickYesBtn.hidden=YES;
    _seatCoverView.hidden=YES;
}

-(void)showJoinRequest:(GroupUserObject *)user leftBtn:(BOOL)leftBtn rightBtn:(BOOL)rightBtn
{
    if (_lockBtn.selected==YES) {
        return;
    }
    [self hideLockBtn];
    _requestJoinHolderView.hidden=NO;
    if (leftBtn) {
        _rollLeftBtn.hidden=NO;
    }else{
        _rollLeftBtn.hidden=YES;
    }

    if (rightBtn) {
        _rollRightBtn.hidden=NO;
    }else{
        _rollRightBtn.hidden=YES;
    }
    [_requestOpenID setText:user.openID];
    [_requestOpenID sizeToFit];
    [_requestOpenID setTextAlignment:NSTextAlignmentCenter];
    _requestOpenID.center=CGPointMake(SCREEN_WIDTH/4, SCREEN_WIDTH*2/7-22);
    if(user.picture.length==0){
        [_requestHeader setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }else{
        [_requestHeader setImageWithURL:S3_THUMB_IMAGE_URL(user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }
    
}

-(void)showJoinRequestDeny
{
    _requestDenyView.hidden=NO;
}

-(void)lockSeat
{
    _lockBtn.selected=YES;
    [_lockBtn setBackgroundImage:[UIImage imageNamed:@"group_lock"] forState:UIControlStateNormal];
    [_lockLabel setText:LOCALIZE(@"Lock_now")];
    [_lockLabel setTextColor:WHITE_COLOR];
}

-(void)unlockSeat
{
    _lockBtn.selected=NO;
    [_lockBtn setBackgroundImage:[UIImage imageNamed:@"group_open"] forState:UIControlStateNormal];
    [_lockLabel setText:LOCALIZE(@"Open_for_Join")];
    [_lockLabel setTextColor:[UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1]];
}
@end
