//
//  LiveShowMediaView.h
//  story17
//
//  Created by POPO Chen on 10/27/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveShowMediaPlayer.h"
#import "GroupLiveConstants.h"
#import "GroupUserObject.h"
@protocol LiveShowMediaViewDelegate <NSObject>
@optional
-(void)sendTcpRequest:(BLAB_ACTION_TYPE)type infoDict:(NSDictionary*)infoDict;
-(void)applyRequestYes;
-(void)applyRequestNo;
-(void)kickAction:(GroupUserObject*)user channelID:(int)channelID;
-(void)reloadHolderRequestView;
-(void)rollLeftAction;
-(void)rollRightAction;
//-(void)requestJoinAction:(NSString*)openID picture:(NSString*)picture;
//-(void)rejectJoinAction:(NSString*)userID;
//-(void)cancelJoinAction:(NSString*)openID picture:(NSString*)picture;
//-(void)acceptJoinAction:(NSString*)userID channelID:(int)channel;
//-(void)lockSeatAction:(int)lockNums;
//-(void)unlockSeatAction:(int)unlockNums;
//-(void)kickOutAction:(NSString*)userID;

@end

@interface LiveShowMediaView : UIView


@property BOOL broadFirst;
@property (nonatomic, weak) id<LiveShowMediaViewDelegate> delegate;
@property (nonatomic, strong) LiveShowMediaPlayer* mediaPlayer;
@property (nonatomic) BOOL isPlaying;
@property (nonatomic,strong) GroupUserObject* player;
@property int32_t liveStreamMode;


@property (nonatomic, strong) UIView* bgView;
@property (nonatomic, strong) UIImageView *seatCoverView;

@property (nonatomic, strong) UIButton *lockBtn;
@property (nonatomic, strong) UILabel *lockLabel;

@property (nonatomic, strong) UIButton *rollRightBtn;
@property (nonatomic, strong) UIButton *rollLeftBtn;

@property (nonatomic, strong) UIButton *kickBtn;
@property (nonatomic, strong) UIButton *kickYesBtn;
@property (nonatomic, strong) UIButton *kickNoBtn;
@property (nonatomic, strong) UIButton *applyYesBtn;
@property (nonatomic, strong) UIButton *applyNoBtn;
@property (nonatomic, strong) UIButton *cancelRequestWatcherbtn;
@property (nonatomic, strong) UIButton *denyConfirmBtn;

@property (nonatomic, strong) UILabel *kickConfirmLabel;
@property (nonatomic, strong) UIView *requestJoinWatcherView;
@property (nonatomic, strong) UIView *requestJoinHolderView;

@property (nonatomic, strong) UIView *requestDenyView;

@property (nonatomic,strong) UIImageView *requestHeader;
@property (nonatomic,strong) UILabel *requestOpenID;



@property (nonatomic, strong) UILabel *openIDLabel;
@property int channelID;

-(id)initWithChannelID:(int)channelID liveShowMode:(int32_t)liveShowMode;
-(void)applyRequestYes:(GroupUserObject*)user;
-(void)showLockBtn;
-(void)hideLockBtn;
-(void)showJoinRequest:(GroupUserObject*)user leftBtn:(BOOL)leftBtn rightBtn:(BOOL)rightBtn;
-(void)showJoinRequestDeny;
-(void)showGlView;
-(void)hideGlview;
-(void)lockSeat;
-(void)unlockSeat;
@end
