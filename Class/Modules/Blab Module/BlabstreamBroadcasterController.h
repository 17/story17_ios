//
//  LivestreamBroadcasterController.h
//  story17
//
//  Created by POPO Chen on 7/30/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>
#import "Constant.h"
#import "PCMAudioPlayer.h"
#import "PCMAudioRecorder.h"
#import "MyOpusEncoder.h"
#import "GCDAsyncSocket.h"
#import "ChunkDataObject.h"
#import "MyScene.h"
#import "MyFilter.h"
#import "Constant.h"
#import "VP8VideoDecoder.h"
#import "VP8VideoEncoder.h"
#import "UDPSocket.h"
#import "LivestreamConstant.h"

/* TCP Command Codes */
#define COMMAND_CODE_NEW_CHUNK_BEGIN 1
#define COMMAND_CODE_CHUNK_DATA 2
#define COMMAND_CODE_START_CHUNK_UPLOAD 3

/* Data Chunk Types */
#define CHUNK_DATA_TYPE_AUDIO 0
#define CHUNK_DATA_TYPE_VIDEO 1

/* Chunking Params */
#define NUM_OF_AUDIO_SAMPLES_PER_CHUNK 20

/* Timer Duration */
#define LIVE_STREAM_KEEP_ALIVE_TIME 1.0

@protocol BlabBroadcasterDelegate <NSObject>
@optional
-(void)startBroadcast:(BOOL)success withLivestream:(LiveStreamObject*)livestream;
-(void)endlivestreamWithLivestreamInfo:(LiveStreamObject*)livestream;
-(void)broadcastKeepAliveFail:(int)failTimes;
-(void)broadcastReceiveUdpPacket:(LIVE_STREAM_PACKET_TYPE)packetType data:(NSData*)data;
-(void)updatelivestream:(LiveStreamObject*)livestream;
@end

@interface BlabstreamBroadcasterController : NSObject <AVCaptureVideoDataOutputSampleBufferDelegate,UDPSocketDelegate, GCDAsyncSocketDelegate,AudioRecorderDelegate>

@property (nonatomic, strong) NSString *broadcasterHexcolor;        //  Broadcaster hexColor (no use 20150730)
@property (nonatomic, strong) NSString *livestreamID;
@property (nonatomic, strong) NSString *publicIP;
@property (nonatomic) int audioOnly;

@property (nonatomic, strong) UIImage* bgImage;

/* Publisher Related */
@property (nonatomic, strong) GLKView* glVideoOutputView;
@property (nonatomic, weak) id<BlabBroadcasterDelegate> delegate;
@property (nonatomic, copy) void (^panRecogHandler)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location);
@property (nonatomic) int Count;
/* Camera Settings */
@property BOOL isFrontCamera;
@property BOOL isTorchOn;
@property float livestreamduration;
@property (nonatomic) float duration;
@property (nonatomic,strong) CTCallCenter *callCenter;

-(void) startLiveStreamWithLivestreamInfo:(NSDictionary*)infoDict;  //  Start Livestream Publisher
-(void) endLivestream;                                              //  End Livestreamming
-(void) setupCamera;
-(void) calDuration;
-(void) setupAudioRecorder;
//  Setup Camera When SwitchButton clicked

@end
