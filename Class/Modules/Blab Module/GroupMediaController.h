//
//  GroupMediaController.h
//  Story17
//
//  Created by POPO on 10/28/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GroupLiveConstants.h"
#import "GroupAudioFrame.h"
#import "GroupVideoFrame.h"
#import "PCMAUAudioPlayer.h"

@protocol GroupMediaControllerDelegate <NSObject>

@optional
//- (void)shouldPlayGroupAudioFrame:(GroupAudioFrame*)audioFrame;
//- (void)shouldPlayGroupVideoFrame:(GroupVideoFrame*)videoFrame;
//- (void)playAudioData:(NSData*)audioData inChannel:(GroupLiveChannelPosition)ch;
- (void)playVideoData:(GroupVideoFrame*)gVF;

@end

@interface GroupMediaController : NSObject <PCMAUAudioPlayerDelegate>

@property (assign) id <GroupMediaControllerDelegate> delegate;

- (void)start;
- (void)stop;

- (void)schedulePlayGroupAudioFrame:(GroupAudioFrame*)gAF;
- (void)schedulePlayGroupVideoFrame:(GroupVideoFrame*)gVF;

@end
