//
//  PaymentInfoViewController.h
//  Story17
//
//  Created by POPO on 2015/5/14.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TitleLabelView.h"
#import "MultiFileUploader.h"


@interface BlabSettingViewController : UIViewController <UITextFieldDelegate,UIPopoverControllerDelegate>
{
@private
    
}
@property (nonatomic)int choosedTypes;
@property (nonatomic,strong)UITextField* livetitle;
@property (nonatomic,strong)UILabel* tittleLabel;
@property (nonatomic,strong)UILabel* startTimeLabel;
@property (nonatomic,strong)UILabel* targettimeLabel;

@property (nonatomic,strong)UIButton* NowBtn;
@property (nonatomic,strong)UIButton* LaterBtn;
@property (nonatomic,strong)UIButton* ConfirmBtn;
@property (nonatomic,strong)UIDatePicker* datePicker;
@property (nonatomic,strong)NSLocale* datelocale;
@property (nonatomic,strong) UIPopoverController *popOverForDatePicker;
@property (nonatomic)BOOL publishing;
@property (nonatomic,strong)NSMutableArray* typeArrary;
@property (nonatomic,strong)UIButton* type1;
@property (nonatomic,strong)UIButton* type2;
@property (nonatomic,strong)UIButton* type3;
@property (nonatomic,strong)UIButton* type4;
@property (nonatomic,strong)UIButton* type5;
@property (nonatomic,strong)UIButton* type6;
@property (nonatomic,strong)UIButton* type7;
@property (nonatomic,strong)UIButton* type8;
@property (nonatomic,strong)UIButton* type9;
@end
