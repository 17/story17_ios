//
//  SettingsViewController.m
//  Story17
//
//  Created by York on 2015/5/5.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "SettingsViewController.h"
#import "EditProfileViewController.h"
#import "NotificationSettingsViewController.h"
#import "ChangePasswordViewController.h"
#import "Constant.h"
#import "FacebookManager.h"
#import "StatusBarColorApplyingActivityViewController.h"
#import "ListTableViewController.h"
#import <HelpShift/Helpshift.h>
#import "UserListViewController.h"
#import "GiftManageViewController.h"
#import "Branch.h"
#import "PhoneVerificationViewController.h"
#import "GuestViewController.h"
#import "RegisterV4ViewController.h"

@interface SettingsViewController ()

@property (nonatomic,strong) UIButton* logoutBtn;

@property (nonatomic,strong) UIScrollView* mainScrollView;

@property (nonatomic,strong) TitleLabelView* findLabel;
@property (nonatomic,strong) ArrowSwitchLabelView* findContactView;

@property (nonatomic,strong) TitleLabelView* accountLabel;
@property (nonatomic,strong) ArrowSwitchLabelView* editView;
@property (nonatomic,strong) ArrowSwitchLabelView* myPointView;
@property (nonatomic,strong) ArrowSwitchLabelView* changePassView;
@property (nonatomic,strong) ArrowSwitchLabelView* rebindPhoneView;
@property (nonatomic,strong) ArrowSwitchLabelView* currencyRateView;

@property (nonatomic,strong) TitleLabelView* settingsLabel;
@property (nonatomic,strong) ArrowSwitchLabelView* notifView;
@property (nonatomic,strong) ArrowSwitchLabelView* blackListView;
@property (nonatomic,strong) ArrowSwitchLabelView* savePhotoView;
@property (nonatomic,strong) ArrowSwitchLabelView* privacyView;
@property (nonatomic,strong) ArrowSwitchLabelView* followPrivacyView;
@property (nonatomic,strong) UILabel* followPrivacyIntroLabel;
@property (nonatomic,strong) UILabel* privacyIntroLabel;

@property (nonatomic) BOOL Helpshiftregister;
@property (nonatomic,strong) TitleLabelView* supportLabel;
@property (nonatomic,strong) ArrowSwitchLabelView* inviteView;
@property (nonatomic,strong) ArrowSwitchLabelView* reportView;
@property (nonatomic,strong) ArrowSwitchLabelView* FAQLabel;
@property (nonatomic,strong) ArrowSwitchLabelView* lougoutView;
@property (nonatomic,strong) ArrowSwitchLabelView* versionView;

@end

@implementation SettingsViewController

#define verticalMargin 5
#define titleLabelHeight 30

-(id)init
{
    self = [super init];
    if(self){
        DLog(@"init Retain count is %ld", CFGetRetainCount((__bridge CFTypeRef)self));
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    DLog(@" viewDidLoad init Retain count is %ld", CFGetRetainCount((__bridge CFTypeRef)self));

    
    [self addCustomNavigationBackButton];
    [self configureViewForIOS7];
    self.title = LOCALIZE(@"Settings");
    
    _mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
    [_mainScrollView setBackgroundColor:LIGHT_BG_COLOR];
    
    int nextY = verticalMargin;
//
//    _findLabel = [[TitleLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, titleLabelHeight)];
//    [_mainScrollView addSubview:_findLabel];
//    nextY += _findLabel.frame.size.height;
//    [_findLabel.titleLabel setText:@"Follow People"];
//    
//    _findContactView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
//    [_findContactView styleSettingsWithMode:WITH_ARROW];
//    [_findContactView.titleLabel setText:LOCALIZE(@"findContact")];
//    [_mainScrollView addSubview:_findContactView];
//    nextY += _findContactView.frame.size.height+verticalMargin;
    
    _accountLabel = [[TitleLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, titleLabelHeight)];
    [_mainScrollView addSubview:_accountLabel];
    nextY += _accountLabel.frame.size.height;
    [_accountLabel.titleLabel setText:LOCALIZE(@"Account")];

    _editView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_editView styleSettingsWithMode:WITH_ARROW];
    [_editView.titleLabel setText:LOCALIZE(@"user_profile_edit")];
    [_mainScrollView addSubview:_editView];
    nextY += _editView.frame.size.height+verticalMargin;
    
    _myPointView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_myPointView styleSettingsWithMode:WITH_ARROW];
    [_myPointView.titleLabel setText:LOCALIZE(@"user_point")];
//    UIImageView* redbadge=[[UIImageView alloc]initWithFrame:CGRectMake(25, _myPointView.titleLabel.frame.origin.y+_myPointView.titleLabel.frame.size.height+7, 5, 5)];
//    [redbadge setImage:[UIImage imageNamed:@"badge_red"]];
//    [_myPointView addSubview:redbadge];
//    
//    UILabel* firstbuy=[[UILabel alloc]initWithFrame:CGRectMake(31, _myPointView.titleLabel.frame.origin.y+_myPointView.titleLabel.frame.size.height+3, 150, 15)];
//    [firstbuy setText:@"首次儲值加贈1000點"];
//    [firstbuy setFont:SYSTEM_FONT_WITH_SIZE(14)];
//    [_myPointView addSubview:firstbuy];
//    [firstbuy setTextColor:RED_COLOR];
//    
    
    
    [_mainScrollView addSubview:_myPointView];
    nextY += _myPointView.frame.size.height+verticalMargin;
    
    _changePassView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_changePassView styleSettingsWithMode:WITH_ARROW];
    [_changePassView.titleLabel setText:LOCALIZE(@"changePassword")];
    [_mainScrollView addSubview:_changePassView];
    nextY += _changePassView.frame.size.height+verticalMargin;
    
    _rebindPhoneView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_rebindPhoneView styleSettingsWithMode:WITH_ARROW];
    [_rebindPhoneView.titleLabel setText:LOCALIZE(@"ReBindPhoneNumber")];
    [_mainScrollView addSubview:_rebindPhoneView];
    nextY += _rebindPhoneView.frame.size.height+verticalMargin;
    
    
    
    _currencyRateView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_currencyRateView styleSettingsWithMode:WITH_SUBTITLE];
    
    NSString* type = [[SINGLETON getCurrencyType] isEqualToString:@"¥"]?@"CNY":[SINGLETON getCurrencyType];
    [_currencyRateView.subTitleLabel setText:LOCALIZE(type)];
    [_currencyRateView.titleLabel setText:LOCALIZE(@"changeCurrencyRate")];
    [_mainScrollView addSubview:_currencyRateView];
    _currencyRateView.userInteractionEnabled = YES;
    
    __weak SettingsViewController* weakSelf = self;
    [_currencyRateView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        
        NSArray *listArray = [[SINGLETON currenctRateDict] allKeys];
        NSMutableArray *localizedArray = CREATE_MUTABLE_ARRAY;
        for(int i=0;i<[listArray count];i++){
            [localizedArray addObject:LOCALIZE(listArray[i])];
        }
        ListTableViewController* listTableView = [ListTableViewController new];
        listTableView.dataArray = localizedArray;
        NSString* type = [[SINGLETON getCurrencyType] isEqualToString:@"¥"]?@"CNY":[SINGLETON getCurrencyType];
        listTableView.selectedItem = LOCALIZE(type);
        listTableView.title = LOCALIZE(@"currency_rate");
        [listTableView setSelectionCallback:^(NSString * callbackStr) {
            
            int index = (int)[localizedArray indexOfObject:callbackStr];
            [DEFAULTS setObject:@1 forKey:CURRENCY_RATE_SETTED];
            [DEFAULTS setObject:listArray[index] forKey:CURRENCY_RATE];
            [DEFAULTS synchronize];
            [_currencyRateView.subTitleLabel setText:LOCALIZE(GET_DEFAULT(CURRENCY_RATE))];
            [NOTIFICATION_CENTER postNotificationName:RELOAD_SELF_INFO object:nil];

        }];
        [weakSelf.navigationController pushViewController:listTableView animated:YES];
    }]];

    nextY += _currencyRateView.frame.size.height+verticalMargin;
    
    _notifView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_notifView styleSettingsWithMode:WITH_ARROW];
    [_notifView.titleLabel setText:LOCALIZE(@"notification_settings")];
    [_mainScrollView addSubview:_notifView];
    nextY += _notifView.frame.size.height+verticalMargin;
    
    _blackListView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_blackListView styleSettingsWithMode:WITH_ARROW];
    [_blackListView.titleLabel setText:LOCALIZE(@"black_list")];
    [_mainScrollView addSubview:_blackListView];
    nextY += _blackListView.frame.size.height+verticalMargin;
    
    _savePhotoView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_savePhotoView styleSettingsWithMode:WITH_SWITCH];
    [_savePhotoView.titleLabel setText:LOCALIZE(@"savePhoto")];
    [_mainScrollView addSubview:_savePhotoView];
    nextY += _savePhotoView.frame.size.height+verticalMargin;
    
    _followPrivacyView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_followPrivacyView styleSettingsWithMode:WITH_SWITCH];
    [_followPrivacyView.titleLabel setText:LOCALIZE(@"followPrivacyMode")];
    [_mainScrollView addSubview:_followPrivacyView];
    nextY += _followPrivacyView.frame.size.height;
    
    _followPrivacyIntroLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, nextY, SCREEN_WIDTH-50, 50)];
    _followPrivacyIntroLabel.font = SYSTEM_FONT_WITH_SIZE(12);
    [_followPrivacyIntroLabel setText:LOCALIZE(@"followPrivacyIntro")];
    [_followPrivacyIntroLabel setTextColor:LIGHT_GRAY_COLOR];
    _followPrivacyIntroLabel.numberOfLines = 0;
    [_mainScrollView addSubview:_followPrivacyIntroLabel];
    nextY += _followPrivacyIntroLabel.frame.size.height+verticalMargin;
    
    _privacyView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_privacyView styleSettingsWithMode:WITH_SWITCH];
    [_privacyView.titleLabel setText:LOCALIZE(@"privacyMode")];
    [_mainScrollView addSubview:_privacyView];
    nextY += _privacyView.frame.size.height;

    _privacyIntroLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, nextY, SCREEN_WIDTH-50, 50)];
    _privacyIntroLabel.font = SYSTEM_FONT_WITH_SIZE(12);
    [_privacyIntroLabel setText:LOCALIZE(@"privacyIntro")];
    [_privacyIntroLabel setTextColor:LIGHT_GRAY_COLOR];
    _privacyIntroLabel.numberOfLines = 0;
    [_mainScrollView addSubview:_privacyIntroLabel];
    nextY += _privacyIntroLabel.frame.size.height+verticalMargin;
    
    _supportLabel = [[TitleLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, titleLabelHeight)];
    [_mainScrollView addSubview:_supportLabel];
    nextY += _supportLabel.frame.size.height;
    [_supportLabel.titleLabel setText:LOCALIZE(@"Support")];
    
    _inviteView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_inviteView styleSettingsWithMode:WITH_ARROW];
    [_inviteView.titleLabel setText:LOCALIZE(@"invite")];
    [_inviteView styleSettingsWithMode:WITH_NONE];
    [_mainScrollView addSubview:_inviteView];
    nextY += _inviteView.frame.size.height+verticalMargin;
    
    _reportView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_reportView styleSettingsWithMode:WITH_ARROW];
    [_reportView.titleLabel setText:LOCALIZE(@"report")];
    [_mainScrollView addSubview:_reportView];
    nextY += _reportView.frame.size.height+verticalMargin;
    
    _FAQLabel = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_FAQLabel styleSettingsWithMode:WITH_ARROW];
    [_FAQLabel.titleLabel setText:LOCALIZE(@"FAQ")];
    [_mainScrollView addSubview:_FAQLabel];
    nextY += _FAQLabel.frame.size.height+verticalMargin;
    
    
    _lougoutView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_lougoutView.titleLabel setText:LOCALIZE(@"logout")];
   // _lougoutView.titleLabel.font = BOLD_FONT_WITH_SIZE(16);
    [_lougoutView styleSettingsWithMode:WITH_NONE];
    [_mainScrollView addSubview:_lougoutView];
    nextY += _lougoutView.frame.size.height+verticalMargin;
    
    self.versionView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
//    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    [_versionView.titleLabel setText:[NSString stringWithFormat:LOCALIZE(@"Version %@"), version]];
    [_versionView styleSettingsWithMode:WITH_NONE];
    [_mainScrollView addSubview:_versionView];
    nextY += _versionView.frame.size.height+verticalMargin+20;
    
    [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, nextY)];
    [self.view addSubview:_mainScrollView];
    
    [_findContactView addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(findContactAction:)]];
    _findContactView.userInteractionEnabled = YES;
   
    [_inviteView addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(invitedFriend:)]];
    _inviteView.userInteractionEnabled = YES;
    
    [_editView addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(editAction:)]];
    _editView.userInteractionEnabled = YES;
    
    [_myPointView addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkPointAction:)]];
    _myPointView.userInteractionEnabled = YES;
    
    [_changePassView addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePasswordAction:)]];
    _changePassView.userInteractionEnabled = YES;
    
    [_rebindPhoneView addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(rebindPhoneView:)]];
    _rebindPhoneView.userInteractionEnabled = YES;
    
    
    [_notifView addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(notifAction:)]];
    
    [_blackListView addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(blackListAction)]];
    
    [_FAQLabel addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(FAQAction:)]];
    _FAQLabel.userInteractionEnabled = YES;
    

    [_reportView addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(reportAction:)]];
    _reportView.userInteractionEnabled = YES;
    
    [_lougoutView addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(logoutAction:)]];
    [_lougoutView.titleLabel setTextColor: [UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1]];
    _lougoutView.userInteractionEnabled = YES;
    
    [_savePhotoView addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(photoSavedMode:)]];
    _savePhotoView.userInteractionEnabled = YES;
    [_savePhotoView.switchView addTarget:self action:@selector(photoSavedMode:) forControlEvents:UIControlEventTouchUpInside];
   
    if([[GET_DEFAULT(SAVE_PHOTO) stringValue] isEqualToString:@"0"]){
        [_savePhotoView.switchView setOn:NO];
    }else{
        [_savePhotoView.switchView setOn:YES];
    }
    
    [_followPrivacyView addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(followPrivacyMode)]];
    _followPrivacyView.userInteractionEnabled = YES;
    [_followPrivacyView.switchView addTarget:self action:@selector(followPrivacyMode) forControlEvents:UIControlEventTouchUpInside];
        
    if ([GET_DEFAULT(FOLLOW_PRIVACY_MODE) isEqualToString:@"0"] || !GET_DEFAULT(FOLLOW_PRIVACY_MODE)) {
        [_followPrivacyView.switchView setOn:NO];
    } else {
        [_followPrivacyView.switchView setOn:YES];
    }
    
    [_privacyView addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(privacyMode:)]];
    _privacyView.userInteractionEnabled = YES;
    [_privacyView.switchView addTarget:self action:@selector(privacyMode:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([GET_DEFAULT(PRIVACY_MODE) isEqualToString:@"open"]) {
        [_privacyView.switchView setOn:NO];
    } else {
        [_privacyView.switchView setOn:YES];
    }
}

-(void)dealloc{

}

-(void)photoSavedMode:(id)sender
{
    if([[GET_DEFAULT(SAVE_PHOTO) stringValue] isEqualToString:@"0"]){
        [_savePhotoView.switchView setOn:YES animated:YES];
        [DEFAULTS setObject:@1 forKey:SAVE_PHOTO];
    }else{
        [_savePhotoView.switchView setOn:NO animated:YES];
        [DEFAULTS setObject:@0 forKey:SAVE_PHOTO];
    }
    
    [DEFAULTS synchronize];
}

-(void)followPrivacyMode
{
    if([GET_DEFAULT(FOLLOW_PRIVACY_MODE) isEqualToString:@"0"]){
        [_followPrivacyView.switchView setOn:YES animated:YES];
        [DEFAULTS setObject:@"1" forKey:FOLLOW_PRIVACY_MODE];
        [API_MANAGER updateUserInfo:@{@"followPrivacyMode":@"1"} fetchSelfInfo:NO completion:^(BOOL success) {}];
    }else{
        [_followPrivacyView.switchView setOn:NO animated:YES];
        [DEFAULTS setObject:@"0" forKey:FOLLOW_PRIVACY_MODE];
        [API_MANAGER updateUserInfo:@{@"followPrivacyMode":@"0"} fetchSelfInfo:NO completion:^(BOOL success) {}];
    }
    
    [DEFAULTS synchronize];
}

-(void)privacyMode:(id)sender
{
    if([GET_DEFAULT(PRIVACY_MODE) isEqualToString:@"open"]){
        [_privacyView.switchView setOn:YES animated:YES];
        [DEFAULTS setObject:@"private" forKey:PRIVACY_MODE];
        [API_MANAGER updateUserInfo:@{@"privacyMode":@"private"} fetchSelfInfo:NO completion:^(BOOL success) {}];
    }else{
        [_privacyView.switchView setOn:NO animated:YES];
        [DEFAULTS setObject:@"open" forKey:PRIVACY_MODE];
        [API_MANAGER updateUserInfo:@{@"privacyMode":@"open"} fetchSelfInfo:NO completion:^(BOOL success) {}];
    }
    
    [DEFAULTS synchronize];
}




-(void)findContactAction:(id)sender
{
    
}

-(void)inviteAction:(id)sender
{
    
}

-(void)editAction:(id)sender
{
    EditProfileViewController* editVC  = [EditProfileViewController new];
    [self.navigationController pushViewController:editVC animated:YES];
}

-(void)checkPointAction:(id)sender
{
    GiftManageViewController* giftManageVC = [GiftManageViewController new];
    [self.navigationController pushViewController:giftManageVC animated:YES];
}

-(void)changePasswordAction:(id)sender
{
    ChangePasswordViewController* changeVC = [ChangePasswordViewController new];
    [self.navigationController pushViewController:changeVC animated:YES];
}

-(void)rebindPhoneView:(id)sender
{
    PhoneVerificationViewController* rebindPhoneView = [PhoneVerificationViewController new];
    rebindPhoneView.status=REBIND_PHONE;
    [self.navigationController pushViewController:rebindPhoneView animated:YES];

}


-(void)notifAction:(id)sender
{
    NotificationSettingsViewController* notifVC  = [NotificationSettingsViewController new];
    [self.navigationController pushViewController:notifVC animated:YES];
}

-(void)blackListAction
{
    UserListViewController* userListVC  = [UserListViewController new];
    userListVC.mode = UserListViewModeBlockedUser;
    userListVC.hidesBottomBarWhenPushed = YES;
    userListVC.title = LOCALIZE(@"black_list");
    [self.navigationController pushViewController:userListVC animated:YES];
}

-(void)reportAction:(id)sender
{
//    [self sendMail];
    
 

    [DIALOG_MANAGER showLoadingView];

    [Helpshift setName:GET_DEFAULT(USER_OPEN_ID) andEmail:@""];
    [Helpshift setUserIdentifier:MY_USER_ID];

    [DIALOG_MANAGER hideLoadingView];
    
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *currentLanguage = [languages objectAtIndex:0];
    
    Helpshift *helpShift = [Helpshift sharedInstance];

    if ([currentLanguage isEqualToString:@"zh-TW"]) {
        [helpShift setSDKLanguage:@"zh-Hant"];
    }

    [helpShift  showConversation:self withOptions:nil];

}

-(void)FAQAction:(id)sender
{

    [DIALOG_MANAGER hideLoadingView];
    
    [Helpshift setName:GET_DEFAULT(USER_OPEN_ID) andEmail:@""];
    [Helpshift setUserIdentifier:MY_USER_ID];
    
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *currentLanguage = [languages objectAtIndex:0];
    
    Helpshift *helpShift = [Helpshift sharedInstance];

    if ([currentLanguage isEqualToString:@"zh-TW"]) {
        [helpShift setSDKLanguage:@"zh-Hant"];
    }

    [helpShift  showFAQs:self withOptions:nil];

}



-(void)sendMail {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.navigationBar.tintColor = [UIColor whiteColor];
        controller.mailComposeDelegate = self;
        
        [controller setToRecipients:[NSArray arrayWithObjects:@"support@machipopo.com", nil]];
        [controller setSubject:LOCALIZE(@"bug_report")];
        
        NSString* emailBody = [NSString stringWithFormat:LOCALIZE(@"Thanks for your report!\n\n\nBugs:\n\n\nSuggestion:\n\n\nModel: %@ IOS %@\n\n Version: %@\n\n ID: %@"),
                               [[UIDevice currentDevice] platformString],
                               [[UIDevice currentDevice] systemVersion],
                               CURRENT_VERSION,
                               MY_USER_ID];
        [controller setMessageBody:emailBody isHTML:NO];
        
        [controller becomeFirstResponder];
        [self presentViewController:controller animated:YES completion:nil];
        
    } else {
        [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"Notice") message:LOCALIZE(@"no available Email") buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
        }];
    }
}


-(void)logoutAction:(id)sender
{
    [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"confirm_logout") message:LOCALIZE(@"confirm_logout_msg") buttonText:LOCALIZE(@"OK") cancelable:YES withCompletion:^(BOOL okClicked) {
        if(okClicked){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [DIALOG_MANAGER showLoadingView];
                
                [API_MANAGER updateUserInfo:@{@"pushToken":@""}  fetchSelfInfo:NO  completion:^(BOOL success) {
                    if(success) {
                        
                        [SINGLETON clearUserDefaults];
                        [SINGLETON initializeDefaults];
                        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            
                            [DIALOG_MANAGER hideLoadingView];
                            [SINGLETON customizeInterface];
                            [PUBLISH_POST_HANDLER clearData];
                            [FACEBOOK_MANAGER logoutAction];
                            
                            Branch *branch = [Branch getInstance];
                            [branch logout];

                            GuestViewController *vc = [[GuestViewController alloc] init];
                            UINavigationController *startNav = [[UINavigationController alloc] initWithRootViewController:vc];
                            [[UIApplication sharedApplication] keyWindow].rootViewController = startNav;
                            [SINGLETON mainTabbarRelease];
                        });
                    } else {
                        
                        [DIALOG_MANAGER showNetworkFailToast];
                    
                    }
                }];
            });
        } // end of if oncliked
    }];

}


-(void) invitedFriend:(id)sender
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [DIALOG_MANAGER showActionSheetShareLink:ShareMediaTypeApp withObj:nil hasRestream:NO withCompletion:^(NSString *action){
            
            if (![action isEqualToString:@"cancel"]) {
                [EVENT_HANDLER addEventTracking:@"Share_App" withDict:@{@"channel":action}];
            }
        }];
        
    });
}


@end
