//
//  EditPostViewController.h
//  story17
//
//  Created by POPO Chen on 7/7/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserTitleView.h"
#import "PostObject.h"
#import "UIPlaceholderTextView.h"

@interface EditPostViewController : UIViewController

@property (nonatomic,strong) UserTitleView* userTitleView;
@property (nonatomic,strong) UIScrollView* scrollView;
@property (nonatomic,strong) UIImageView* postImageView;
@property (nonatomic,strong) UIImageView* videoImageView;
@property (nonatomic,strong) UIPlaceholderTextView* captionTextView;
@property(nonatomic, weak) PostObject* post;

-(void) setPost:(PostObject*)post;
@property (copy)void (^updateCompleteCallback)(PostObject* newPost);

@end
