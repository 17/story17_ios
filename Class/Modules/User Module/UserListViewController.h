//
//  UserListViewController.h
//  story17
//
//  Created by POPO Chen on 5/30/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserObject.h"
#import "SearchUserCell.h"
#import "RequestUserCell.h"

@interface UserListViewController : UIViewController <UITableViewDataSource,UITableViewDelegate, RequestUserCellDelegate, SearchUserDelegate>

@property (nonatomic,strong) UITableView* mainTableView;
@property (nonatomic,strong) NSMutableArray* users;
@property (nonatomic,strong) UserObject* targetUser;
@property (nonatomic,strong) PostObject* likesPost;
@property (nonatomic,strong) UIImageView* noDataImageView;


@property(nonatomic)BOOL isFirstFetching;
@property(nonatomic)BOOL noMoreData;
@property(nonatomic)BOOL isFetching;
@property(nonatomic)BOOL followedPeople;
@property (nonatomic,strong) UIActivityIndicatorView* progressView;

@property(nonatomic)BOOL isSendingRequest;

@property(nonatomic)int mode;
@property(nonatomic)int enterPageTimeStamp;
@property int visibleCellCount;
@end
