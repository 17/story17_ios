//
//  UserListViewController.m
//  story17
//
//  Created by POPO Chen on 5/30/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "UserListViewController.h"
#import "Constant.h"
#import "SVPullToRefresh.h"
#import "UserProfileViewController.h"
#import "RankTableViewCell.h"
#import "RequestUserCell.h"

@implementation UserListViewController

#define userListIdentify @"userListIdentify"
#define friendRequestIdentify @"friendRequestIdentify"
#define fetchNum 20

-(id)init
{
    self = [super init];
    if(self){
        _mode = FOLLOWER_MODE;
        _users = CREATE_MUTABLE_ARRAY;
        _noMoreData = false;
        _isFirstFetching = true;
        _isFetching = false;
        _followedPeople = false;
        _isSendingRequest = false;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    
    if (_mainTableView == nil)
    {
        
        _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
        _mainTableView.delegate = self;
        _mainTableView.dataSource = self;
        _mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_mainTableView registerClass:[SearchUserCell class] forCellReuseIdentifier:userListIdentify];
        [_mainTableView registerClass:[RequestUserCell class] forCellReuseIdentifier:friendRequestIdentify];
        
        _noDataImageView = [ThemeManager getNoDataImageView];
        
        _progressView = [UIActivityIndicatorView new];
        _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
        [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [_progressView setColor:MAIN_COLOR];
        
        [_mainTableView setBackgroundColor:LIGHT_BG_COLOR];
        [self.view addSubview:_mainTableView];
        [self.view addSubview:_progressView];
        [self.view addSubview:_noDataImageView];
        
        
        [NOTIFICATION_CENTER addObserver:self selector:@selector(refresh:) name:FOLLOW_USER_NOTIFICATION object:nil];
    }
    
    [self fetchDataWithRefresh:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (_mode==LIKER_MODE) {
        [EVENT_HANDLER addEventTracking:@"LeaveReceivedLikePage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp),@"cellCount":INT_TO_STRING(_visibleCellCount)}];
    }else if (_mode==FOLLOWER_MODE){
        [EVENT_HANDLER addEventTracking:@"LeaveFansPage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp),@"cellCount":INT_TO_STRING(_visibleCellCount)}];
 
    }else if (_mode==FOLLOWING_MODE){
        [EVENT_HANDLER addEventTracking:@"LeaveFollowingPage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp),@"cellCount":INT_TO_STRING(_visibleCellCount),@"follower":FOLLOWING_COUNT}];
    }
    if(_followedPeople){
        [NOTIFICATION_CENTER postNotificationName:RELOAD_SELF_INFO object:nil];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (_mode==LIKER_MODE) {
        [EVENT_HANDLER addEventTracking:@"EnterReceivedLikePage" withDict:@{}];
    }else if (_mode==FOLLOWER_MODE){
        [EVENT_HANDLER addEventTracking:@"EnterFansPage" withDict:@{}];
    }else if (_mode==FOLLOWING_MODE){
        [EVENT_HANDLER addEventTracking:@"EnterFollwingPage" withDict:@{}];

    }
    _enterPageTimeStamp=CURRENT_TIMESTAMP;
    _visibleCellCount=0;
    if(_mode == REQUEST_MODE){
        [API_MANAGER updateUserInfo:@{@"requestBadge":@"0"}  fetchSelfInfo:YES  completion:^(BOOL success) {
            if(success){
                [NOTIFICATION_CENTER postNotificationName:NEW_FRIEND_REQUEST object:nil];
            }
        }];
    }
}

-(void)dealloc
{
    [NOTIFICATION_CENTER removeObserver:self];
}

-(void)refresh:(id)sender
{
    [self fetchDataWithRefresh:YES];
}

- (void)fetchDataWithRefresh:(BOOL)refresh
{
    if(refresh){
        _noMoreData = false;
    }
    
    if(_isFetching || _noMoreData) return;
    
    if((_mode==FOLLOWER_MODE || _mode==FOLLOWING_MODE) && _targetUser==nil)
        return;
    
    if(_mode==LIKER_MODE && _likesPost==nil)
        return;
    
    UserObject* lastUserObj = NULL;
    int beforeTime = INT32_MAX;
    int offset = 0;
    
    if([_users count]>0){
        lastUserObj = _users[[_users count]-1];
        if(!refresh){
            offset = (int)[_users count];
        }
    }
    
    if(lastUserObj==NULL || refresh){
        beforeTime = INT32_MAX;
    }else{
        switch (_mode) {
            case UserListViewModeBlockedUser:
                beforeTime = lastUserObj.blockTime;
                break;
                
            default:
                beforeTime = lastUserObj.followTime;
                break;
        }
    }
    
    if(_isFirstFetching){
        [_progressView startAnimating];
        _isFirstFetching = false;
    }

    _isFetching = true;

    [self callApiWithMode:beforeTime offset:offset count:fetchNum  withCompletion:^(BOOL success, NSArray *users) {
      
        [_progressView stopAnimating];
        _isFetching = false;
        if(success){
            
            if(refresh){
                [_users removeAllObjects];
            }
            
            if([users count]==0 && [_users count]==0){
                _noDataImageView.hidden = NO;
            }else{
                _noDataImageView.hidden = YES;
            }
            
            [_users addObjectsFromArray:users];
            
            if([users count]<fetchNum){
                _noMoreData = true;
            }
            
            [_mainTableView reloadData];
        }
    }];
}

#pragma mark - API Action with callback
-(void)callApiWithMode:(int)beforeTime offset:(int)offset count:(int)count withCompletion:(void(^)(BOOL success,NSArray* dict))callback
{
    switch (_mode) {
        case FOLLOWER_MODE:
        {
            [API_MANAGER getFollower:_targetUser.userID beforeTime:beforeTime andCount:count withCompletion:^(BOOL success, NSArray *dict) {
                if(success){
                    callback(success,dict);
                }
            }];
            break;
        }
        case FOLLOWING_MODE:
        {
            [API_MANAGER getFollowing:_targetUser.userID beforeTime:beforeTime andCount:count withCompletion:^(BOOL success, NSArray *dict) {
                if(success){
                    callback(success,dict);
                }
            }];
            break;
        }
        case LIKER_MODE:
        {
            [API_MANAGER getPostLikers:_likesPost.postID offset:offset count:count withCompletion:^(BOOL success, NSArray *userObjects) {
                if(success){
                    DLog(@"getPostLikers:%lu",(unsigned long)[userObjects count]);
                    callback(success,userObjects);
                }else{
                    DLog(@"fail");
                }
            }];
            break;
        }
        case USER_LIKER_MODE:
        {
            [API_MANAGER getUserLikers:(NSString*)_targetUser.userID offset:offset count:count withCompletion:^(BOOL success, NSArray *userObjects) {
                if(success){
                    callback(success,userObjects);
                }
            }];
            break;
        }
        case REQUEST_MODE:
        {
            [API_MANAGER getFollowRequests:count beforeTime:beforeTime withCompletion:^(BOOL success, NSArray *userObjects) {
                if(success){
                    callback(success,userObjects);
                }
            }];
            break;
        }
        case UserListViewModeBlockedUser:
        {
            [API_MANAGER getBlockUserList:beforeTime count:200 withCompletion:^(BOOL success, NSArray *userObjects) {
                if (success) {
                    callback(YES, userObjects);
                } else {
                    DLog(@"ERR: getBlockUserList");
                }
            }];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_users count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    _visibleCellCount++;

    if(_mode==REQUEST_MODE){
        
        RequestUserCell* cell = [tableView dequeueReusableCellWithIdentifier:friendRequestIdentify];
        if(!cell){
            cell = [[RequestUserCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:friendRequestIdentify];
        }
        
        cell.delegate = self;
        UserObject* user = _users[indexPath.row];
        if(indexPath.row+fetchNum/2>[_users count]){
            [self fetchDataWithRefresh:NO];
        }
        [cell reloadCell:user];
     
        return cell;
    }
    
    SearchUserCell* cell = [tableView dequeueReusableCellWithIdentifier:userListIdentify];
    if(!cell){
        cell = [[SearchUserCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:userListIdentify];
    }
    
    cell.delegate = self;
    UserObject* user = _users[indexPath.row];
    cell.mode = _mode;
    [cell reloadCell:user];
    
    if(_mode==LIKER_MODE || _mode==USER_LIKER_MODE){
        cell.userTitleView.subTitle = [NSString stringWithFormat:@"%d",user.likeCount ];
        cell.heartImageView.hidden = NO;
        int length = (int)cell.userTitleView.subTitle.length;
        cell.heartImageView.frame = CGRectMake(65+length*7, 27, 20, 20);
    }
   
    if(indexPath.row+fetchNum/2>[_users count]){
        [self fetchDataWithRefresh:NO];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserObject* user = _users[indexPath.row];

    if([user.userID isEqualToString:MY_USER_ID])
        return;
    if (_mode==LIKER_MODE) {
        [EVENT_HANDLER addEventTracking:@"EnterProfileLikerPage" withDict:nil];
    }
    UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
    userCtrl.user = user;
    [userCtrl getUserWithUserID:userCtrl.user.userID];
    userCtrl.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:userCtrl animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_mode==REQUEST_MODE){
        return [RequestUserCell getCellHeigt];
    }
    return [UserTitleView getDefaultHeight];
}

-(void)didFollowUser:(UserObject*)user;
{
    _followedPeople = true;
    [SINGLETON reloadFollowStatus];
}

-(void)didClickUnblock:(SearchUserCell*)cell
{
    NSIndexPath *indexPath = [_mainTableView indexPathForCell:cell];
    
    if(indexPath==nil){
        return;
    }
    
    [_mainTableView beginUpdates];
    [_users removeObjectAtIndex:indexPath.row];
    [_mainTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [_mainTableView endUpdates];
}

#pragma mark RequestUserCellDelegate
-(void)didClickAcceptRequestButton:(RequestUserCell *)cell userID:(NSString *)userID
{
    if(_isSendingRequest == YES)
        return;
    
    _isSendingRequest = YES;
    [API_MANAGER acceptFollowRequest:userID withCompletion:^(BOOL success) {
        _isSendingRequest = NO;
        if(success){
            [self removeCellAnimation:cell];
            [DIALOG_MANAGER showCompleteToast];
        }else{
            [DIALOG_MANAGER showNetworkFailToast];
        }
    }];
}

-(void) didClickRejectRequestButton:(RequestUserCell*)cell userID:(NSString*)userID
{
    if(_isSendingRequest == YES)
        return;
    
    _isSendingRequest = YES;
    [API_MANAGER rejectFollowRequest:userID withCompletion:^(BOOL success) {
        _isSendingRequest = NO;
        if(success){
            
            [self removeCellAnimation:cell];
            [DIALOG_MANAGER showCompleteToast];
        }else{
            [DIALOG_MANAGER showNetworkFailToast];
        }
    }];
}

-(void)removeCellAnimation:(UITableViewCell*)cell
{
    if([_users count]!=1){
        [_mainTableView beginUpdates];
        NSIndexPath* indexPath = [[_mainTableView indexPathForCell:cell] copy];
        [_users removeObjectAtIndex:indexPath.row];
        [_mainTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        [_mainTableView endUpdates];
    }else{
        [DEFAULTS setObject:@0 forKey:UPDATE_NOTIF_TIME];
        [DEFAULTS synchronize];
        [self.navigationController popViewControllerAnimated:YES];
    }
}


@end
