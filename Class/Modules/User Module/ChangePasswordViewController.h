//
//  RegisterAccountViewController.h
//  tex
//
//  Created by POPO on 4/15/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageTextField.h"
#import "Constant.h"

@interface ChangePasswordViewController : UIViewController <UITextFieldDelegate>
{
@private
    ImageTextField* oldPasswordRow;
    ImageTextField* newPasswordRow;
    ImageTextField* confirmPasswordRow;
}

@property (nonatomic,strong) UIImageView* alertView;
@property (nonatomic,strong) UILabel* alertLabel;

@end
