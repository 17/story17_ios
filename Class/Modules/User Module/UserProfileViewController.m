//
//  UserProfileViewController.m
//  story17
//
//  Created by POPO Chen on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "UserProfileViewController.h"
#import "SettingsViewController.h"
#import "FindFriendViewController.h"
#import "Constant.h"
#import "PostGridCell.h"
#import "UserListCell.h"
#import "UserInfoCell.h"
#import "SVPullToRefresh.h"
#import "SinglePostViewController.h"
#import "NoDataCollectionViewCell.h"
#import "ListTableViewController.h"
#import "EditPostViewController.h"
#import "NotificationViewController.h"
#import "PrivacyModeCollectionViewCell.h"
#import "LoginHandler.h"
#import "UpAndDownAnimator.h"
#import "UpAndDownInteractive.h"

#define Padding 0
#define SuggestionCount 3

static NSString * const kReuseCellIdentifier = @"PostGridCell";

@interface UserProfileViewController() <UIViewControllerTransitioningDelegate, LiveStreamViewControllerDelegate>
@property (nonatomic, strong) UpAndDownInteractive *dismissInteractor;
@property (nonatomic, assign) BOOL enableInteractivePlayerTransitioning;
@property BOOL viewActive;
@end

@implementation UserProfileViewController

@synthesize user = _user;

- (id)init
{
    self = [super init];
    if (self)
    {
        _profileMode = USER_NORMAL;
        _viewActive=NO;
        _section = 4;
        _type = UserProfileTabSelectGrid;
        _suggestions = CREATE_MUTABLE_ARRAY;
        _posts = CREATE_MUTABLE_ARRAY;
        _likeposts = CREATE_MUTABLE_ARRAY;
        
        _isReLoad = false;
        _dismissNavbar = false;
        _isFetching = false;
        _noMoreData = false;
        _isFetchingSuggestion = NO;
        
//        [NOTIFICATION_CENTER addObserver:self selector:@selector(reloadUser:) name:FOLLOW_USER_NOTIFICATION object:nil];
        [NOTIFICATION_CENTER addObserver:self selector:@selector(removePost:) name:REMOVE_SELF_POST object:nil];
        [NOTIFICATION_CENTER addObserver:self selector:@selector(reloadUser:) name:RELOAD_SELF_DATA object:nil];
        [NOTIFICATION_CENTER addObserver:self selector:@selector(reloadUserHeader:) name:RELOAD_SELF_INFO object:nil];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViewForIOS7];
    
    self.dismissInteractor = [[UpAndDownInteractive alloc] init];
    self.enableInteractivePlayerTransitioning = NO;
    
    if (_collectionView == nil)
    {
        CGRect rect = self.view.bounds;
        rect.size.height -= STATUS_BAR_HEIGHT + NAVI_BAR_HEIGHT;
        
        _imgSize.width = (SCREEN_WIDTH - (2 * Padding)) / 3;
        _imgSize.height = _imgSize.width;
        
        _bgTapView = [[UIView alloc]initWithFrame:CGRectMake(0, -NAVI_BAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [self.view addSubview:_bgTapView];
        
        UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
        [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
        [layout setMinimumInteritemSpacing:Padding];
        [layout setMinimumLineSpacing:Padding];
        
        _collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:layout];
        [_collectionView setDataSource:self];
        [_collectionView setDelegate:self];
        [_collectionView registerClass:[UserProfileTitleCell class] forCellWithReuseIdentifier:@"TitleCell"];
        [_collectionView registerClass:[UserProfileTabCell class] forCellWithReuseIdentifier:@"TitleTab"];
        [_collectionView registerClass:[UserListCell class] forCellWithReuseIdentifier:@"UserListCell"];
        [_collectionView registerNib:[UINib nibWithNibName:kReuseCellIdentifier bundle:nil] forCellWithReuseIdentifier:kReuseCellIdentifier];
        [_collectionView registerClass:[UserInfoCell class] forCellWithReuseIdentifier:@"UserInfoCell"];
        [_collectionView registerClass:[PostListCell class] forCellWithReuseIdentifier:@"PostListCell"];
        [_collectionView registerClass:[NoDataCollectionViewCell class] forCellWithReuseIdentifier:@"NoDataCollectionViewCell"];
        [_collectionView registerClass:[PrivacyModeCollectionViewCell class] forCellWithReuseIdentifier:@"PrivacyModeCollectionViewCell"];

        [_collectionView setAlwaysBounceVertical:YES];
        [_collectionView setBackgroundColor:LIGHT_BG_COLOR];
        [self.view addSubview:_collectionView];
        
        __weak UserProfileViewController* ptr = self;
        [_collectionView addPullToRefreshWithActionHandler:^{[ptr fetchPostDataWithRefresh:YES];}];
        
        if(_user!=NULL && [_user.openID isEqualToString:GET_DEFAULT(USER_OPEN_ID)]){
            
//            UIButton* findFriendBtn = [ThemeManager findFriendBtn];
//            [findFriendBtn addTarget:self action:@selector(didClickFindFriend:) forControlEvents:UIControlEventTouchUpInside];
//            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:findFriendBtn];
            UIView *notifyView=[[UIView alloc]initWithFrame:CGRectMake(0, 10, 50, 50)];
            _badgeImageView = [[UIImageView alloc]initWithFrame:CGRectMake(30, 14, 5,5)];
            [_badgeImageView setImage:[UIImage imageNamed:@"badge_red"]];
            
            UIButton* notifyBtn = [ThemeManager notifyBtn];
            [notifyBtn addTarget:self action:@selector(didClickNotify:) forControlEvents:UIControlEventTouchUpInside];
//            [notifyBtn.imageView addSubview:_badgeImageView];
            [notifyView addSubview:_badgeImageView];
            [notifyView addSubview:notifyBtn];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:notifyView];
            
            _collectionView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT);

        }else{
            _backButton =  [self addCustomNavigationBackButtonCallback];
            [_backButton addTarget:self action:@selector(popoutViewController:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        if(!_isLocalUser){
            [NOTIFICATION_CENTER removeObserver:self];
        }
        

    }
    
   }

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
    [self reloadAllBadge];
    
    [NOTIFICATION_CENTER addObserver:self selector:@selector(setCellToPlay:) name:BUFFER_VIDEO_UPLADED object:nil];
    
    [self pullData:YES];
    
    if(_user==NULL){
        return;
    }
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if([GET_DEFAULT(UPDATE_USER_PROFILE) intValue] < CURRENT_TIMESTAMP-UPDATE_INTERVAL){
            [_collectionView.pullToRefreshView startAnimating];
            [self fetchPostDataWithRefresh:YES];
        }
    });
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [SINGLETON startVideoAVPlayerForCollectionView:_collectionView];
    
    /* Level 5 crash fixed */
    if(_isLocalUser){
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }else{
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [NOTIFICATION_CENTER removeObserver:self name:BUFFER_VIDEO_UPLADED object:nil];
    
    [_backButton setSelected:YES];
    
    if (_followAction) {
        _followAction=NO;
    }else{
        if (_user.isFollowing==1) {
            _followAction=YES;
        }
    }
    NSString * booleanString = (_followAction) ? @"True" : @"False";
    
    if (_user!=nil) {
        [EVENT_HANDLER addEventTracking:@"LeaveProfilePage" withDict:@{@"targetUserID":_user.userID,@"followAction":booleanString,@"gridCount":INT_TO_STRING(_gridCount),@"favorCount":INT_TO_STRING(_favorCount),@"listCount":INT_TO_STRING(_listCount)}];

    }
    
    NSArray *viewControllers = self.navigationController.viewControllers;
    if (viewControllers.count > 1 && [viewControllers objectAtIndex:viewControllers.count-2] == self) {
        
    } else if ([viewControllers indexOfObject:self] == NSNotFound) {
        
        if(_dismissNavbar){
            [self.navigationController setNavigationBarHidden:YES animated:YES];
        }
    }
    
    [SINGLETON stopAVPlayerForCollectionView:_collectionView];
}

-(void)dealloc
{
    [NOTIFICATION_CENTER removeObserver:self];
}

-(void)removePost:(NSNotification*)notif
{
    if([notif.object isKindOfClass:[PostObject class]]){
        if([_posts containsObject:notif.object]){
            [_posts removeObject:notif.object];
            [_collectionView reloadData];
            [self reloadUserHeader:nil];
        }
    }
}

-(void)popoutViewController:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    [_backButton setSelected:YES];
}


-(void)reloadUser:(id)sender
{
    if(![_user.userID isEqualToString:MY_USER_ID])
        return;
    
    [API_MANAGER getSelfInfo:^(BOOL success) {
        if(success){
            _user = [SINGLETON getSelfUserObject];
            [_collectionView reloadData];
        }
    }];
    [self fetchPostDataWithRefresh:YES];

}

-(void)reloadUserHeader:(id)sender
{
    if(![_user.userID isEqualToString:MY_USER_ID])
        return;
    
    [API_MANAGER getSelfInfo:^(BOOL success) {
        if(success){
            _user = [SINGLETON getSelfUserObject];
            dispatch_async(MAIN_QUEUE, ^{
                NSIndexPath* indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
                UICollectionViewCell* cell = [_collectionView cellForItemAtIndexPath:indexPath];
                if(![cell isEqual:[NSNull null]] && [cell isKindOfClass:[UserProfileTitleCell class]]){
                    [_collectionView reloadItemsAtIndexPaths:@[indexPath]];
                }
                indexPath= [NSIndexPath indexPathForItem:0 inSection:1];
                cell = [_collectionView cellForItemAtIndexPath:indexPath];
                if(![cell isEqual:[NSNull null]] && [cell isKindOfClass:[UserInfoCell class]]){
                    [_collectionView reloadItemsAtIndexPaths:@[indexPath]];
                }
                indexPath = [NSIndexPath indexPathForItem:0 inSection:2];
                cell = [_collectionView cellForItemAtIndexPath:indexPath];
                if(![cell isEqual:[NSNull null]] && [cell isKindOfClass:[UserInfoCell class]]){
                    [_collectionView reloadItemsAtIndexPaths:@[indexPath]];
                }
            });
            
        }
    }];
    
}

-(void)pullData:(BOOL)refresh
{
    if (![_user.userID isEqual:MY_USER_ID])
        return;
    
    [REVENUE_MANAGER pullDataWithCallback:refresh withCallback:^(BOOL success, NSArray* dataArray){

        if (!success){
            [DIALOG_MANAGER showNetworkFailToast];
            return;
        }
        
        if (![REVENUE_MANAGER eof]){
            [self pullData:NO];
        }
        else
        {
            [_collectionView reloadData];
        }
    }];
}

-(void)didClickSettings:(id)sender
{
    SettingsViewController* sVC = [SettingsViewController new];
    UINavigationController* setNav = [[UINavigationController alloc] initWithRootViewController:sVC];
    [self.navigationController presentViewController:setNav animated:YES completion:nil];
}

- (void)clickMore
{
    NSMutableArray* arr ;
    NSArray* dest = [NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil];
    NSMutableArray* admin = CREATE_MUTABLE_ARRAY;

    if(_user.isBlocked==1){
        arr = [NSMutableArray arrayWithObjects:LOCALIZE(@"unblock_user"), LOCALIZE(@"report_user"),LOCALIZE(@"ShareTo"),nil];
    }else{
        arr = [NSMutableArray arrayWithObjects:LOCALIZE(@"block_user"), LOCALIZE(@"report_user"),LOCALIZE(@"ShareTo"),nil];
    }

    if([GET_DEFAULT(IS_ADMIN) intValue]>=NORMAL_ADMIN){
        NSUInteger adminStartIndex = [arr indexOfObject:[arr lastObject]];
        
        [arr addObject:LOCALIZE(@"admin_freeze_action")];
        [admin addObject:[NSNumber numberWithUnsignedInteger:++adminStartIndex]];
        
        [arr addObject:LOCALIZE(@"admin_verify_action")];
        [admin addObject:[NSNumber numberWithUnsignedInteger:++adminStartIndex]];
        
        [arr addObject:LOCALIZE(@"recover_post")];
        [admin addObject:[NSNumber numberWithUnsignedInteger:++adminStartIndex]];

        [arr addObject:LOCALIZE(@"delete_profile_picture")];
        [admin addObject:[NSNumber numberWithUnsignedInteger:++adminStartIndex]];
        [arr addObject:LOCALIZE(@"send_openid_alert")];
        [admin addObject:[NSNumber numberWithUnsignedInteger:++adminStartIndex]];

    }
        
    [DIALOG_MANAGER showActionSheetDialogTitle:@"" options:arr destructiveIndexes:dest adminIndexes:admin cancelable:YES withCompletion:^(int selectedOption){
       
        if(selectedOption==0){
            // block
            [DIALOG_MANAGER showLoadingView];
            if(_user.isBlocked==1){
                [API_MANAGER unblockUserAction:_user.userID withCompletion:^(BOOL success) {
                    [DIALOG_MANAGER hideLoadingView];
                    if(success){
                        _user.isBlocked=0;
                        [DIALOG_MANAGER showCompleteToast];
                    }
                }];
            }else{
                [self blockUserAction:_user.userID withCallback:^(BOOL success) {
                    [DIALOG_MANAGER hideLoadingView];
                    if(success){
                        _user.isBlocked=1;
                        [DIALOG_MANAGER showCompleteToast];
                    }
                }];
            }
            
        }else if(selectedOption==1){
            // report
            
            ListTableViewController* listTableView = [ListTableViewController new];
            listTableView.dataArray = USER_REPORTED_REASON;
            listTableView.hidesBottomBarWhenPushed= YES;
            listTableView.title = LOCALIZE(@"Reason");
            [listTableView setSelectionCallback:^(NSString * callbackStr) {
                
                [DEFAULTS setObject:@0 forKey:UPDATE_FEED_TIME];
                [DEFAULTS setObject:@0 forKey:UPDATE_SEARCH_TIME];
                [DEFAULTS setObject:@0 forKey:UPDATE_USER_PROFILE];
                [DEFAULTS synchronize];
                
                [API_MANAGER reportUserAction:_user.userID withReason:callbackStr andMessage:@"UserProfile" withCompletion:^(BOOL success, NSString *message) {
                    if(success){
                        [DIALOG_MANAGER showCompleteToast];
                    }
                }];

            }];
            [self.navigationController pushViewController:listTableView animated:YES];
            
        }else if(selectedOption==2){
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

                [DIALOG_MANAGER showActionSheetShareLink:ShareMediaTypeUser withObj:_user hasRestream:NO withCompletion:^(NSString *action) {
                 
                    if (![action isEqualToString:@"cancel"]) {
                        [EVENT_HANDLER addEventTracking:@"Share_User" withDict:@{@"channel":action,@"targetUserID":_user.userID}];
                    }
                }];
            });

        }else if(selectedOption==3){
            
            // admin freeze user action
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self adminFreezeAction];
            });
            
        }else if(selectedOption==4){
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self adminVerifyAction];
            });

        }else if(selectedOption==5){
            
            if([arr[5] isEqualToString:LOCALIZE(@"recover_post")]){
                [API_MANAGER recoverPostAction:_user.userID withCompletion:^(BOOL success) {
                    if(success){
                        [DIALOG_MANAGER showCompleteToast];
                    }else{
                        [DIALOG_MANAGER showNetworkFailToast];
                    }
                }];
            }
            
        } else if(selectedOption == 6) {
            
            
            if([arr[6] isEqualToString:LOCALIZE(@"delete_profile_picture")]){
                [API_MANAGER adminDeleteProfilePictureAction:_user.userID withCompletion:^(BOOL success) {
                    if(success){
                        [DIALOG_MANAGER showCompleteToast];
                    }else{
                        [DIALOG_MANAGER showNetworkFailToast];
                    }
                }];
            }
            
        }else if(selectedOption ==11){
            
            if([arr[11] isEqualToString:LOCALIZE(@"send_openid_alert")]){
                
                
            }
            
        }
    }];
}

-(void) adminFreezeAction
{
    [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"Freeze_confirm") options:@[LOCALIZE(@"admin_ban_user"),LOCALIZE(@"admin_freeze_user")] destructiveIndexes:@[@0,@1] cancelable:YES withCompletion:^(int selectedOption) {
        if(selectedOption==0){
        
            
            [API_MANAGER banUserAction:_user.userID inLivestreamID:@"" absTime:0 withCompletion:^(BOOL success, NSString *message) {
                [DIALOG_MANAGER showCompleteToast];
            }];
        }else if(selectedOption==1){
            // freeze user
            [API_MANAGER freezeUserAction:_user.userID inLivestreamID:@"" absTime:0 withComletion:^(BOOL success,NSString* message) {
                [DIALOG_MANAGER showCompleteToast];
            }];
        }
    }];
}

-(void) adminVerifyAction
{
    
    NSMutableArray* arr = CREATE_MUTABLE_ARRAY ;

    if(_user.isVerified==1){
        [arr addObject:LOCALIZE(@"admin_verify_cancel_user")];
    }else{
        [arr addObject:LOCALIZE(@"admin_verify_user")];
    }
    
    if(_user.isChoice==1){
        [arr addObject:LOCALIZE(@"admin_choice_cancel_user")];
    }else{
        [arr addObject:LOCALIZE(@"admin_choice_user")];
    }
    
    if(_user.crossGreatWall==1){
        [arr addObject:LOCALIZE(@"crossGreatWall_off_control")];
    }else{
        [arr addObject:LOCALIZE(@"crossGreatWall_on_control")];
    }
    
    
    if(_user.isInternational==1){
        [arr addObject:LOCALIZE(@"international_off_control")];
    }else{
        [arr addObject:LOCALIZE(@"international_on_control")];
    }
    
    if(_user.isCelebrity==1){
        [arr addObject:LOCALIZE(@"celebrity_off_control")];
    }else{
        [arr addObject:LOCALIZE(@"celebrity_on_control")];
    }
    
    
    [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"Freeze_confirm") options:arr destructiveIndexes:@[@0,@1,@2,@3,@4] cancelable:YES withCompletion:^(int selectedOption) {
        
        
        if([arr[selectedOption] isEqualToString:LOCALIZE(@"admin_verify_user")]){
            [API_MANAGER verifyUserAction:_user.userID withCompletion:^(BOOL success, NSString *message) {
                [DIALOG_MANAGER showCompleteToast];
                _user.isVerified=1;
                [_collectionView reloadSections:[[NSIndexSet alloc] initWithIndex:0]];
            }];
        }else if([arr[selectedOption] isEqualToString:LOCALIZE(@"admin_verify_cancel_user")]){
            [API_MANAGER removeVerifiedUserAction:_user.userID withCompletion:^(BOOL success, NSString *message) {
                [DIALOG_MANAGER showCompleteToast];
                _user.isVerified=0;
                [_collectionView reloadSections:[[NSIndexSet alloc] initWithIndex:0]];
            }];
        }

        
        if([arr[selectedOption] isEqualToString:LOCALIZE(@"admin_choice_user")]){
        
            [API_MANAGER choiceUserAction:_user.userID withCompletion:^(BOOL success, NSString *message) {
                [DIALOG_MANAGER showCompleteToast];
                _user.isChoice=1;
                [_collectionView reloadSections:[[NSIndexSet alloc] initWithIndex:0]];
            }];
        }else if([arr[selectedOption] isEqualToString:LOCALIZE(@"admin_choice_cancel_user")]){
            [API_MANAGER removeChoiceUserAction:_user.userID withCompletion:^(BOOL success, NSString *message) {
                [DIALOG_MANAGER showCompleteToast];
                _user.isChoice=0;
                [_collectionView reloadSections:[[NSIndexSet alloc] initWithIndex:0]];
            }];
        }
        
        if([arr[selectedOption] isEqualToString:LOCALIZE(@"crossGreatWall_on_control")]){
            
            [API_MANAGER addCrossGreatWallFlag:_user.userID withCompletion:^(BOOL success, NSString *message) {
                if(success){
                    _user.crossGreatWall = 1;
                    [DIALOG_MANAGER showCompleteToast];
                }else{
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
            
        }else if([arr[selectedOption] isEqualToString:LOCALIZE(@"crossGreatWall_off_control")]){
            
            [API_MANAGER removeCrossGreatWallFlag:_user.userID withCompletion:^(BOOL success, NSString *message) {
                if(success){
                    _user.crossGreatWall = 0;
                    [DIALOG_MANAGER showCompleteToast];
                }else{
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
        }
        
        if([arr[selectedOption] isEqualToString:LOCALIZE(@"international_off_control")]){
            [API_MANAGER removeInternationalTagAction:_user.userID withCompletion:^(BOOL success, NSString *message) {
                if(success){
                    _user.isInternational = 0;
                    [DIALOG_MANAGER showCompleteToast];
                }
            }];
        }else if([arr[selectedOption] isEqualToString:LOCALIZE(@"international_on_control")]){
            [API_MANAGER internationalTagAction:_user.userID withCompletion:^(BOOL success, NSString *message) {
                if(success){
                    _user.isInternational = 1;
                    [DIALOG_MANAGER showCompleteToast];
                }
            }];
        }

        if([arr[selectedOption] isEqualToString:LOCALIZE(@"celebrity_off_control")]){
            [API_MANAGER removeCelebrityAction:_user.userID withCompletion:^(BOOL success, NSString *message) {
                if(success){
                    _user.isCelebrity = 0;
                    [DIALOG_MANAGER showCompleteToast];
                }
            }];
        }else if([arr[selectedOption] isEqualToString:LOCALIZE(@"celebrity_on_control")]){
            [API_MANAGER celebrityAction:_user.userID withCompletion:^(BOOL success, NSString *message) {
                if(success){
                    _user.isCelebrity = 1;
                    [DIALOG_MANAGER showCompleteToast];
                }
            }];
        }


    }];
}

- (void) adminLikeControlAction
{
    [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"admin_like_control") options:@[LOCALIZE(@"admin_clear_post_like_from_user"), LOCALIZE(@"admin_clear_post_like_by_user_received_like_table"),LOCALIZE(@"admin_recalculate_received_like_count"),LOCALIZE(@"admin_write_received_like_count_to_leaderboard_y"),LOCALIZE(@"admin_write_received_like_count_to_leaderboard_m"),LOCALIZE(@"admin_write_received_like_count_to_leaderboard_w"),LOCALIZE(@"admin_write_received_like_count_to_leaderboard_d"),LOCALIZE(@"admin_write_received_like_count_to_leaderboard_h")] destructiveIndexes:@[@0,@1,@2,@3,@4,@5,@6,@7] cancelable:YES withCompletion:^(int selectedOption) {
        switch (selectedOption) {
            case 0:
                [API_MANAGER clearPostLikeFromUser:_user.userID withCompletion:^(BOOL success) {
                    if (success) {
                        [DIALOG_MANAGER showCompleteToast];
                    }
                }];
                break;
            case 1:
                [API_MANAGER clearPostLikeByUserReceivedLikeTable:_user.userID withCompletion:^(BOOL success) {
                    if (success) {
                        [DIALOG_MANAGER showCompleteToast];
                    }
                }];
                break;
            case 2:
                [API_MANAGER recalculateUserReceivedLikeCount:_user.userID withCompletion:^(BOOL success) {
                    if (success) {
                        [DIALOG_MANAGER showCompleteToast];
                    }
                }];
                break;
            case 3:
                [API_MANAGER writeUserReceivedLikeCount:_user.userID toLeaderBoard:1 withCompletion:^(BOOL success) {
                    if (success) {
                        [DIALOG_MANAGER showCompleteToast];
                    }
                }];
                break;
            case 4:
                [API_MANAGER writeUserReceivedLikeCount:_user.userID toLeaderBoard:2 withCompletion:^(BOOL success) {
                    if (success) {
                        [DIALOG_MANAGER showCompleteToast];
                    }
                }];
                break;
            case 5:
                [API_MANAGER writeUserReceivedLikeCount:_user.userID toLeaderBoard:3 withCompletion:^(BOOL success) {
                    if (success) {
                        [DIALOG_MANAGER showCompleteToast];
                    }
                }];
                break;
            case 6:
                [API_MANAGER writeUserReceivedLikeCount:_user.userID toLeaderBoard:4 withCompletion:^(BOOL success) {
                    if (success) {
                        [DIALOG_MANAGER showCompleteToast];
                    }
                }];
                break;
            case 7:
                [API_MANAGER writeUserReceivedLikeCount:_user.userID toLeaderBoard:5 withCompletion:^(BOOL success) {
                    if (success) {
                        [DIALOG_MANAGER showCompleteToast];
                    }
                }];
                break;
        }
    }];
}

- (void)fetchPostDataWithRefresh:(BOOL)refresh
{
    
    NSMutableArray* postsArray;
    if(_type==UserProfileTabSelectFavor){
        postsArray = _likeposts;
    }else{
        postsArray = _posts;
    }
    
    if (refresh){
        [self pullData:YES];
        _noMoreData = false;
    }
    
    if(_user==NULL || _isFetching || _noMoreData){
        [_collectionView.pullToRefreshView stopAnimating];
        return;
    }
    
    _isReLoad = true;
    
    int lastTime = INT32_MAX;
    
    if ([postsArray count] > 0 && !refresh)
    {
        PostObject* obj = [postsArray lastObject];
        lastTime = obj.timestamp;
    }
    
    
    _isFetching = true;
    
    [self fetchDataBeforeTime:lastTime withCallback:^(BOOL success, NSArray *posts) {

        _isFetching = false;
        [_collectionView.pullToRefreshView stopAnimating];
        
        if(success){
            
            if(refresh){
                [postsArray removeAllObjects];
                [DEFAULTS setObject:INT_TO_NUMBER(CURRENT_TIMESTAMP) forKey:UPDATE_USER_PROFILE];
                [DEFAULTS synchronize];
            }
            
            if (posts != nil && [posts count] != 0)
            {
                NSMutableArray* postIDs = [[NSMutableArray alloc] init];
                for (PostObject* post in posts)
                    [postIDs addObject:post.postID];
                
            }
            
            if([posts count]==0){
                _noMoreData = true;
            }
            
            [postsArray addObjectsFromArray:posts];
            [_collectionView reloadData];
        }
        
        _isReLoad = false;
    
    }];
    
}

-(void)fetchDataBeforeTime:(int)lastTime withCallback:(void(^)(BOOL success,NSArray* posts))callback
{
    if(_type==UserProfileTabSelectFavor){
        [API_MANAGER getLikedPost:_user.userID beforeTime:lastTime count:18 withCompletion:^(BOOL success, NSArray *posts) {
            callback(success,posts);
        }];
    }else{
        
        /* filter reported User */
        if(![[[DatabaseManager sharedInstance] getReportRecord:@"userID"] containsObject:_user.userID]){
            [API_MANAGER getUserPost:_user.userID beforeTime:lastTime andCount:18 withCompletion:^(BOOL success,NSArray* posts){
                if(![[[DatabaseManager sharedInstance] getReportRecord:@"userID"] containsObject:_user.userID]){
                    callback(success,posts);
                }else{
                    callback(success,posts);
                }
            }];
        }else{
            callback(YES,@[]);
        }
    }
}

- (void)setUser:(UserObject *)user
{
    _user = user;
    _enterPageTimeStamp=CURRENT_TIMESTAMP;
    if (_user.isFollowing==1) {
        _followAction=YES;
    }else{
        _followAction=NO;
    }
    if([_user.userID isEqualToString:MY_USER_ID])
    {
    }else{
        if (user.userID!=nil) {
            [EVENT_HANDLER addEventTracking:@"EnterProfilePage" withDict:@{@"targetUserID":_user.userID}];
        }
    }
//    UserObject* selfObj = [SINGLETON getSelfUserObject];
    

    
    UIButton* findFriendBtn = [ThemeManager findFriendBtn];
    [findFriendBtn addTarget:self action:@selector(didClickFindFriend:) forControlEvents:UIControlEventTouchUpInside];

    [self setTitle:_user.openID];
    self.navigationController.title = @"";
    
    if (![LoginHandler isGuest]) {
        UIImage* image = nil;
        UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if (![_user.userID isEqualToString:MY_USER_ID])
        {
            _isLocalUser = false;
            image = [UIImage imageNamed:@"nav_more"];
            [btn addTarget:self action:@selector(clickMore) forControlEvents:UIControlEventTouchUpInside];
            [btn setFrame:CGRectMake(0, 0, 32, 32)];
        }
        else
        {
            //        _user = selfObj;
            _isLocalUser = true;
            image = [UIImage imageNamed:@"setting"];
            [btn addTarget:self action:@selector(didClickSettings:) forControlEvents:UIControlEventTouchUpInside];
            [btn setFrame:CGRectMake(0, 0, 32, 32)];
            //        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:findFriendBtn];
            _collectionView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT);
        }
        
        [btn setImage:image forState:UIControlStateNormal];
        [btn setImage:image forState:UIControlStateHighlighted];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    }

//    [self setTitle:_user.openID];
    [self fetchPostDataWithRefresh:YES];
    
}

-(void)getUserWithOpenID:(NSString*)openID
{
    
    if([openID isEqualToString:GET_DEFAULT(USER_OPEN_ID)]){
        _isLocalUser = YES;
        [self setUser:[SINGLETON getSelfUserObject]];
        return;
    }
    
    if(openID==nil)
        return;
    
    [DIALOG_MANAGER showLoadingView];
    [API_MANAGER getUserInfo:openID completion:^(BOOL success, UserObject *userObject) {
        [DIALOG_MANAGER hideLoadingView];
        if(success){
            
            if(userObject!=nil){
                
                [API_MANAGER isUserOnLiveStream:userObject.userID completion:^(BOOL success, int userIsOnLive, NSString *livestreamID) {
                    if(success){
                        if(userIsOnLive==1){
                            userObject.isLivestreamming = 1;
                            userObject.livestreamingID = livestreamID;
                        }
                        [self setUser:userObject];
                        [_collectionView reloadData];
                    }else{
                        [self setUser:userObject];
                        [_collectionView reloadData];
                    }
                }];

            }else{
                [DIALOG_MANAGER showNetworkFailToast];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }else{
            [DIALOG_MANAGER showNetworkFailToast];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
    }];
}

-(void)getUserWithUserID:(NSString*)userID
{
    
    if([userID isEqualToString:MY_USER_ID]){
        _isLocalUser = YES;
        [self setUser:[SINGLETON getSelfUserObject]];
        return;
    }
    
    [DIALOG_MANAGER showLoadingView];
    [API_MANAGER getUserInfoByUserID:userID completion:^(BOOL success, UserObject *userObject) {
        [DIALOG_MANAGER hideLoadingView];
        if(success){
            
            [API_MANAGER isUserOnLiveStream:userObject.userID completion:^(BOOL success, int userIsOnLive, NSString *livestreamID) {
                if(success){
                    if(userIsOnLive==1){
                        userObject.isLivestreamming = 1;
                        userObject.livestreamingID = livestreamID;
                    }
                    [self setUser:userObject];
                    [_collectionView reloadData];
                }else{
                    [self setUser:userObject];
                    [_collectionView reloadData];
                }
            }];
            
        }else{
            [DIALOG_MANAGER showNetworkFailToast];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
    }];
}


#pragma mark - data source
//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(8_0);
//

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if(_type==UserProfileTabSelectGrid){
        return UIEdgeInsetsMake(0, 2, 2, 2);
    }
    return UIEdgeInsetsMake(0, 0, 2, 0);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if(_section-1==(int)section){
        if(_type==UserProfileTabSelectGrid){
            return 1;
        }else{
            return 5;
        }
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray* postArray ;
    if(_type==UserProfileTabSelectFavor){
        postArray = _likeposts;
    }else{
        postArray = _posts;
    }
    
    CGSize size = CGSizeZero;
    if (_section == 4)
    {
        switch (indexPath.section) {
            case 0:
            {
                size = CGSizeMake(collectionView.frame.size.width-4, [UserProfileTitleCell getDefaultHeight:_user]);
                break;
            }
            case 1:
            {
                size = CGSizeMake(collectionView.frame.size.width-4, 0);
//                size = CGSizeMake(collectionView.frame.size.width-4, [UserInfoCell getDefaultHeightWithBio:_user]);
                break;
            }
            case 2:
            {
                size = CGSizeMake(collectionView.frame.size.width-4, [UserProfileTabCell getDefaultHeight]);
                break;
            }
                
            case 3:
            {
                if([postArray count]==0){
                    // noDataCell
                    size = CGSizeMake(collectionView.frame.size.width,80);
                }else{
                    size = _type == UserProfileTabSelectGrid ? CGSizeMake((SCREEN_WIDTH)/3-2,(SCREEN_WIDTH)/3-2) : CGSizeMake(SCREEN_WIDTH, [PostListCell getCellHeigthWithPost:postArray[indexPath.row]]);
                }
                break;
            }
                
            default:
                break;
        }
    }
    else if (_section == 5)
    {
        switch (indexPath.section) {
            case 0:
            {
                size = CGSizeMake(collectionView.frame.size.width-4, [UserProfileTitleCell getDefaultHeight:_user]);
                break;
            }
                
            case 1:
            {
                size.width = collectionView.frame.size.width-4;
                size.height = [UserListCell getDefaultHeight];
                break;
            }
            case 2:
            {
                size = CGSizeMake(collectionView.frame.size.width-4, 0);
//                size = CGSizeMake(collectionView.frame.size.width-4, [UserInfoCell getDefaultHeightWithBio:_user]);
                break;
            }
                
            case 3:
            {
                size = CGSizeMake(collectionView.frame.size.width-4, [UserProfileTabCell getDefaultHeight]);
                break;
            }
            case 4:
            {
                if([postArray count]==0){
                    // noDataCell
                    size = CGSizeMake(collectionView.frame.size.width-4,80);
                }else{
                    size = _type == UserProfileTabSelectGrid ? CGSizeMake((SCREEN_WIDTH)/3-2,(SCREEN_WIDTH)/3-2) : CGSizeMake(SCREEN_WIDTH, [PostListCell getCellHeigthWithPost:postArray[indexPath.row]]);
                }
                break;
            }
                
            default:
                break;
        }
    }
    
    return size;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if(_user==NULL){
        return 0;
    }
    return _section;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    NSMutableArray* postArray ;
    if(_type==UserProfileTabSelectFavor){
        postArray = _likeposts;
    }else{
        postArray = _posts;
    }
    
    int num = 0;
    if (_section == 4)
    {
        switch (section) {
            case 0:
                num = 1;
                break;
            case 1:
                num = 1;
                break;
            case 2:
                num = 1;
                break;
            case 3:
                num = [postArray count]==0?1:(int)[postArray count];
                break;
                
            default:
                break;
        }
    }
    else if (_section == 5)
    {
        switch (section) {
            case 0:
                num = 1;
                break;
            case 1:
                num = (int)[_suggestions count];
                break;
            case 2:
                num = 1;
                break;
            case 3:
                num = 1;
                break;
            case 4:
                num = [postArray count]==0?1:(int)[postArray count];
                break;
            default:
                break;
        }
    }
    
    return num;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray* postArray ;
    if(_type==UserProfileTabSelectFavor){
        postArray = _likeposts;
    }else{
        postArray = _posts;
    }
    
    if (indexPath.section == 0)
    {
        UserProfileTitleCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TitleCell" forIndexPath:indexPath];
        if (cell == nil)
            cell = [UserProfileTitleCell new];
        
        cell.navCtrl = self.navigationController;
        cell.isLocalUser = _isLocalUser;
        if(_isLocalUser){
            _user = [SINGLETON getSelfUserObject];
            cell.user = _user;
        }else{
            cell.user = _user;
        }
        
        __weak UserProfileViewController* ptr = self;
        cell.suggestionCallback = ^(){
            [ptr reloadWithSuggestion];
        };
        
        cell.giftDetailImageView.hidden = (!MY_USER_ID || [MY_USER_ID isEqualToString:@""]);
        
        [cell.livestreammingButton addTarget:self action:@selector(onOpenLiveStream:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    
    }else if((indexPath.section == 1 && _section == 4) || (indexPath.section == 2 && _section == 5)){
        
        UserInfoCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UserInfoCell" forIndexPath:indexPath];
        [cell reloadCell:_user];
        cell.navCtrl = self.navigationController;
        return cell;
        
    }else if ((indexPath.section == 2 && _section == 4) || (indexPath.section == 3 && _section == 5)){
        UserProfileTabCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TitleTab" forIndexPath:indexPath];
        if (cell == nil)
            cell = [[UserProfileTabCell alloc] initWithFrame:CGRectMake(0, 0, collectionView.frame.size.width, [UserProfileTabCell getDefaultHeight])];
        
        cell.delegate = self;
        return cell;
    }
    else if ((indexPath.section == 3 && _section == 4) || (indexPath.section == 4 && _section == 5))
    {
        if([postArray count]==0&&[_user isPrivacyMode]){
            PrivacyModeCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PrivacyModeCollectionViewCell" forIndexPath:indexPath];
            ;
            return cell;
        }
        if([postArray count]==0){
            PrivacyModeCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NoDataCollectionViewCell" forIndexPath:indexPath];
            ;
            return cell;
        }
        
        if (_type == UserProfileTabSelectGrid)
        {
            if(indexPath.row>=[_posts count]-9){
                [self fetchPostDataWithRefresh:NO];
            }
            
            PostGridCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:kReuseCellIdentifier forIndexPath:indexPath];
            
            if ([postArray count] > indexPath.row)
            {
                PostObject *post = postArray[indexPath.row];
                post.user = _user;
                cell.post = post;
            }
            return cell;
        }
        else if (_type == UserProfileTabSelectList)
        {

            if(indexPath.row>=[_posts count]-9){
                [self fetchPostDataWithRefresh:NO];
            }
            
            PostObject* postObj  = [_posts objectAtIndex:indexPath.row];
            NSString* identifier = @"PostListCell";
            
            if([postObj.type isEqual:@"video"]){
                identifier = postObj.type;
                [collectionView registerClass:[PostListCell class] forCellWithReuseIdentifier:identifier];
            }

            PostListCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            
            if (cell == nil){
                cell = [PostListCell new];
            }
            
            cell.navCtrl = self.navigationController;
            if ([postArray count] > indexPath.row)
                cell.post = postObj;
            
            cell.delegate = self;
            
            return cell;
        }else if (_type == UserProfileTabSelectFavor)
        {
            
            if(indexPath.row>=[_likeposts count]-9){
                [self fetchPostDataWithRefresh:NO];
            }
            
            PostListCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PostListCell" forIndexPath:indexPath];
        
            if ([postArray count] > indexPath.row)
                cell.post = [postArray objectAtIndex:indexPath.row];
            
            cell.delegate = self;
            cell.navCtrl = self.navigationController;
            return cell;
        }
    }else{
        UserListCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UserListCell" forIndexPath:indexPath];
        if (cell == nil)
            cell = [[UserListCell alloc] initWithFrame:CGRectMake(0, 0, collectionView.frame.size.width, [UserListCell getDefaultHeight])];
        
        if ([_suggestions count] > indexPath.row)
            cell.user = [_suggestions objectAtIndex:indexPath.row];
        return cell;
    }
    
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    id cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    if ((_type != UserProfileTabSelectGrid || ![cell isKindOfClass:[PostGridCell class]]) && _section==4 )
        return;
    
    if(indexPath.section==0)
        return;
    
    // avoid too deep
    if([self.navigationController.viewControllers count]>=6)
        return;
    
    if(_section==5 && indexPath.section==1){
        
        UserObject* user = _suggestions[indexPath.row];
        if([user.userID isEqualToString:MY_USER_ID])
            return;
        UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
        userCtrl.user = user;
        [userCtrl getUserWithUserID:userCtrl.user.userID];
        [self.navigationController pushViewController:userCtrl animated:YES];
        return;
    
    }
    
    if (indexPath.row > [_posts count] || indexPath==nil)
        return;

    SinglePostViewController* ctrl = [SinglePostViewController new];
    ctrl.hidesBottomBarWhenPushed = YES;
    ctrl.enterMode=EnterFromUserProfile;
    if ([_posts count]>0) {
        ctrl.post = [_posts objectAtIndex:indexPath.row];
        if(self.navigationController!=nil)
            [self.navigationController pushViewController:ctrl animated:YES];
    }
}

//-(void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    if([cell isKindOfClass:[PostListCell class]]) {
//        [((PostListCell*) cell) stopPlayer];
//    }
//}

#pragma mark - chage post style
- (void)tabClickWithType:(UserProfileTabType)type
{
    
    /* Change Tab stop running video */
    [SINGLETON stopAVPlayerForCollectionView:_collectionView];

    _type = type;
    switch (_type) {
        case UserProfileTabSelectGrid:
            _gridCount++;
            break;
        case UserProfileTabSelectFavor:
            _favorCount++;
            break;
        case UserProfileTabSelectList:
            _listCount++;
            break;
        default:
            break;
    }
    if([_likeposts count]==0 && _type == UserProfileTabSelectFavor){
        [self fetchPostDataWithRefresh:YES];
        return;
    }
    
    if (_section == 4)
        [_collectionView reloadSections:[[NSIndexSet alloc] initWithIndex:3]];
    else
        [_collectionView reloadSections:[[NSIndexSet alloc] initWithIndex:4]];
}

- (void)reloadWithSuggestion
{
    if (_section == 4)
    {
        if ([_suggestions count] == 0)
        {
            if(_isFetchingSuggestion)
                return;
            
            _isFetchingSuggestion = YES;
            [API_MANAGER getFriendSuggestion:_user.userID offset:0 count:SuggestionCount completion:^(BOOL success, NSArray* userObjects){
                
                [_suggestions removeAllObjects];
                
                if (!success || userObjects == nil || [userObjects count] == 0)
                    return;
                
                [_suggestions addObjectsFromArray:userObjects];
                [_collectionView performBatchUpdates:^{
                    _section = 5;
                    [_collectionView insertSections:[[NSIndexSet alloc] initWithIndex:1]];
                }completion:^(BOOL finished) {
                    [_collectionView reloadData];
                }];
                
                _isFetchingSuggestion = NO;
                
            }];
        }
        else
        {
            [_collectionView performBatchUpdates:^{
                _section = 5;
                [_collectionView insertSections:[[NSIndexSet alloc] initWithIndex:1]];
            } completion:^(BOOL finished) {
                [_collectionView reloadData];
            }];
        }
    }
    else if (_section == 5)
    {
        [_collectionView performBatchUpdates:^{
            _section = 4;
            [_collectionView deleteSections:[[NSIndexSet alloc] initWithIndex:1]];
//            [_collectionView reloadData];
        } completion:nil];
    }
}

-(void)didClickFindFriend:(id)sender
{
    FindFriendViewController* findFriendVC = [FindFriendViewController new];
    findFriendVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:findFriendVC animated:YES];
}

-(void)blockUserAction:(NSString*)userID withCallback:(void(^)(BOOL success))callback
{
    __block int index = 0;
    [API_MANAGER blockUserAction:userID withCompletion:^(BOOL success) {
        if(success){
            if(index==1){
                callback(YES);
            }else{
                index=1;
            }
        }else{
            callback(NO);
        }
    }];
    
    [API_MANAGER unfollowUserAction:userID withCompletion:^(BOOL success) {
        [NOTIFICATION_CENTER postNotificationName:FOLLOW_USER_NOTIFICATION object:nil];
        if(success){
            if(index==1){
                callback(YES);
            }else{
                index=1;
            }
        }else{
            callback(NO);
        }
    }];

}

#pragma mark - UserListDelegate
-(void)didClickFollow:(NSString*)userID
{
    
}

#pragma mark - PostCellDelegate
-(void)didClickPostImage:(UITapGestureRecognizer *)tapGesture withPostID:(NSString*)postID
{
    CGPoint touchPoint = [tapGesture locationInView:_bgTapView];
    DLog(@"%f %f",touchPoint.x,touchPoint.y);
    [DIALOG_MANAGER showHeartFromPoint:touchPoint];
}

//-(void)reloadCellPlayer:(PostListCell *)cell
//{
//    NSArray* viewControllers = self.navigationController.viewControllers;
//    if (viewControllers.count > 1 && [viewControllers objectAtIndex:viewControllers.count-1] == self) {
//        [SINGLETON setPlayerStart:@[cell] collectionView:_collectionView];
//    }
//}

-(void)didClickMore:(PostListCell*)cell
{
    [self didClickMoreDialog:cell collectionView:_collectionView dataArray:_posts];
}


# pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    DLog(@"scrollViewDidScroll");
    [SINGLETON startVideoAVPlayerForCollectionView:_collectionView];
    [LIKE_HANDLER likeBatchUpload];
}

//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    DLog(@"scrollViewDidEndDragging");
//
//    if(!_backButton.isSelected){
//        [SINGLETON setPlayerStart:[_collectionView visibleCells] collectionView:_collectionView];
//    }
//    _preVisibleCell = [_collectionView visibleCells];
//}

//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    DLog(@"scrollViewDidEndDecelerating");
//
//    if(!_backButton.isSelected){
//        [SINGLETON setPlayerStart:[_collectionView visibleCells] collectionView:_collectionView];
//    }
//    _preVisibleCell = [_collectionView visibleCells];
//}

/* handle buffer video callback */
-(void)setCellToPlay:(id)sender
{
//    [SINGLETON setPlayerStart:[_collectionView visibleCells] collectionView:_collectionView];
    [SINGLETON startVideoAVPlayerForCollectionView:_collectionView];
}


-(void)didClickNotify:(id)sender
{
    NotificationViewController * nVC=[[NotificationViewController alloc]init];
    nVC.hidesBottomBarWhenPushed = YES;
    nVC.enterMode=ENTER_MODE_SYS;
    [self.navigationController pushViewController:nVC animated:YES];
    
}

-(void)reloadAllBadge
{
    DLog(@"system log:%d",[GET_DEFAULT(SYSTEM_NOTIF_BADGE) intValue] );
    if( [GET_DEFAULT(SYSTEM_NOTIF_BADGE) intValue]!=0 ){
        self.badgeImageView.hidden = NO;
    }else{
        self.badgeImageView.hidden = YES;
    }
}
#pragma mark - UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    UpAndDownAnimator *animator = [[UpAndDownAnimator alloc] init];
    animator.transitionType = ModalAnimatedTransitioningTypeDismiss;
    return animator;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    UpAndDownAnimator *animator = [[UpAndDownAnimator alloc] init];
    animator.transitionType = ModalAnimatedTransitioningTypePresent;
    return animator;
}

- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForPresentation:(id<UIViewControllerAnimatedTransitioning>)animator
{
    return nil;
}

- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id<UIViewControllerAnimatedTransitioning>)animator
{
    if (_enableInteractivePlayerTransitioning) {
        return _dismissInteractor;
    }
    return nil;
}

#pragma mark - LiveStreamViewControllerDelegate

- (void)liveStreamViewControllerBeginInteractiveTransitioning:(LiveStreamViewController *)liveStreamViewController
{
    self.enableInteractivePlayerTransitioning = YES;
}

- (void)liveStreamViewController:(LiveStreamViewController *)liveStreamViewController updateInteractiveTransition:(CGFloat)percentComplete shouldComplete:(BOOL)shouldComplete
{
    [_dismissInteractor updateInteractiveTransition:percentComplete];
    _dismissInteractor.shouldComplete = shouldComplete;
}

- (BOOL)liveStreamViewControllerShouldCompleteInteractiveTransition:(LiveStreamViewController *)liveStreamViewController
{
    return _dismissInteractor.shouldComplete;
}

- (void)liveStreamViewController:(LiveStreamViewController *)liveStreamViewController completeInteractiveTransition:(BOOL)complete
{
    if (complete) {
        [_dismissInteractor finishInteractiveTransition];
    }
    else {
        [_dismissInteractor cancelInteractiveTransition];
    }
    
    self.enableInteractivePlayerTransitioning = NO;
}

#pragma mark - Actions

- (void)onOpenLiveStream:(id)sender
{
    LiveStreamViewController* vc = [SINGLETON liveStreamSharedManager];
    if (vc.canPresentView) {
        [API_MANAGER getLiveStreamInfo:_user.livestreamingID withCompletion:^(BOOL success, LiveStreamObject *liveStreamInfo) {
            if (vc.canPresentView) {
                if (success) {
                    vc.liveStreamMode = LiveStreamModeWatch;
                    vc.liveStream = liveStreamInfo;
                    vc.delegate = self;
                    vc.transitioningDelegate = self;
                    vc.modalTransitionStyle = UIModalPresentationCustom;
                    vc.modalPresentationStyle = UIModalPresentationFullScreen;
                    [self presentViewController:vc animated:YES completion:nil];
                }
                else {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }
        }];
    }
}

@end
