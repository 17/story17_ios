//
//  UserListCell.m
//  Story17
//
//  Created by POPO on 2015/5/8.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "UserListCell.h"
#import "LoginHandler.h"

@implementation UserListCell

@synthesize user = _user;

+ (float)getDefaultHeight
{
    return [UserTitleView getDefaultHeight];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
        [self setBackgroundColor:WHITE_COLOR];
        _titleView = [[UserTitleView alloc] init];
        [_titleView setFrame:self.bounds];
        [self addSubview:_titleView];
        
        _followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _followBtn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_followBtn addTarget:self action:@selector(didClickFollow:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_followBtn];
        
        CGRect rect;
        rect.origin.x = frame.size.width - FOLLOW_BTN_WIDTH-10;
        rect.origin.y = [UserTitleView getDefaultHeight]/2-FOLLOW_BTN_HEIGHT/2;
        rect.size.width = FOLLOW_BTN_WIDTH;
        rect.size.height = FOLLOW_BTN_HEIGHT;
        [_followBtn setFrame:rect];
        
    }
    
    return self;
}

- (void)setUser:(UserObject *)user
{
    _user = user;
    
    _titleView.title = user.openID;
    _titleView.subTitle = user.name;
    _titleView.image = user.picture;

    if(_user.isVerified==1){
        [_titleView verified:YES];
    }else{
        [_titleView verified:NO];
    }
    
    if(_user.isChoice==1){
        [_titleView choiced:YES];
    }else{
        [_titleView choiced:NO];
    }
    
    if(_user.crossGreatWall==1){
        [_titleView chinaFlag:YES];
    }else{
        [_titleView chinaFlag:NO];
    }
    
    if(_user.isInternational==1){
        [_titleView internationalFlag:YES];
    }else{
        [_titleView internationalFlag:NO];
    }

    
    if(_user.isFollowing==1){
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        [_followBtn setTitle:LOCALIZE(@"Following") forState:UIControlStateNormal];
        [_followBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    }else{
        
        if(_user.followRequestTime!=0){
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
            [_followBtn setTitle:LOCALIZE(@"Sended_request") forState:UIControlStateNormal];
            [_followBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
            
        }else{
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
            [_followBtn setTitle:LOCALIZE(@"+Follow") forState:UIControlStateNormal];
            [_followBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
            
        }
    }
}

-(void)didClickFollow:(id)sender
{
    if ([LoginHandler isGuest]) {
        [LoginHandler handleUserLogin];
        return;
    }
    
    if(_delegate!=NULL)
        [_delegate didClickFollow:_user.userID];
    
    [_followBtn playBounceAnimation];

    
    if(_user.isFollowing==1){
        _user.isFollowing=0;
        [self setUser:_user];
        [API_MANAGER unfollowUserAction:_user.userID withCompletion:^(BOOL success) {
        }];
    }else{
        if([GET_DEFAULT(FOLLOWING_COUNT) intValue] >= MAX_FOLLOWER_COUNT){
            [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"cannot_follow_user") message:[NSString stringWithFormat:LOCALIZE(@"max_followcount"),MAX_FOLLOWER_COUNT] buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                if(okClicked){
                    
                }
            }];
        }else{
            _user.isFollowing=1;
            [self setUser:_user];
            [API_MANAGER followUserAction:_user.userID withCompletion:^(BOOL success) {}];
        }
    }
}



@end
