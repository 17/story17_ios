//
//  NoDataCollectionViewCell.m
//  Story17
//
//  Created by York on 2015/5/31.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "NoDataCollectionViewCell.h"
#import "Constant.h"

@implementation NoDataCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        _noDataImageView = [ThemeManager getNoDataImageView];
        CGRect rect = _noDataImageView.frame;
        rect.origin.y = -20;
        _noDataImageView.frame = rect;
        _noDataImageView.hidden = NO;
        
        [self.contentView addSubview:_noDataImageView];
    }
    return self;
}

@end
