//
//  UserProfileTabCell.m
//  Story17
//
//  Created by POPO on 2015/5/12.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "UserProfileTabCell.h"
#import "Constant.h"

#define USerProfileTabHeight 40

@implementation UserProfileTabCell

+ (float)getDefaultHeight
{
    return USerProfileTabHeight;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _gridButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_gridButton addTarget:self action:@selector(clickGrid) forControlEvents:UIControlEventTouchUpInside];
        [_gridButton setImage:[UIImage imageNamed:@"profile_grid"] forState:UIControlStateNormal];
        [_gridButton setImage:[UIImage imageNamed:@"profile_grid_active"] forState:UIControlStateSelected];
        [_gridButton setSelected:YES];

        [self addSubview:_gridButton];
        
        _listButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_listButton addTarget:self action:@selector(clickList) forControlEvents:UIControlEventTouchUpInside];
        [_listButton setImage:[UIImage imageNamed:@"profile_list"] forState:UIControlStateNormal];
        [_listButton setImage:[UIImage imageNamed:@"profile_list_active"] forState:UIControlStateSelected];

        [self addSubview:_listButton];
        
        _favorButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_favorButton addTarget:self action:@selector(clickFavor) forControlEvents:UIControlEventTouchUpInside];
        [_favorButton setImage:[UIImage imageNamed:@"profile_like"] forState:UIControlStateNormal];
        [_favorButton setImage:[UIImage imageNamed:@"profile_like_active"] forState:UIControlStateSelected];

        [self addSubview:_favorButton];

        
        [self setFrame:frame];
    }
    
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if (frame.size.height <= 0 || frame.size.width <= 0)
        return;
    
    if (_gridButton == nil)
        return;
    
    float width = self.frame.size.width / 3;
    CGRect rect = CGRectMake(0, 0, width, USerProfileTabHeight);
    [_gridButton setFrame:rect];
    
    rect.origin.x += width;
    [_listButton setFrame:rect];
    
    rect.origin.x += width;
    
    [_favorButton setFrame:rect];
    
    UIView* separaterLine = [ThemeManager separaterLine];
    separaterLine.frame = CGRectMake(0, 0, SCREEN_WIDTH, 0.5);
    UIView* separaterLine2 = [ThemeManager separaterLine];
    separaterLine2.frame = CGRectMake(0, _listButton.frame.size.height-0.5, SCREEN_WIDTH, 0.5);
    UIView* verticalLine = [ThemeManager separaterLine];
    verticalLine.frame = CGRectMake(_listButton.frame.size.width, 10, 0.5, _gridButton.frame.size.height-20);
    UIView* verticalLine2 = [ThemeManager separaterLine];
    verticalLine2.frame = CGRectMake(2* _listButton.frame.size.width, 10, 0.5, _gridButton.frame.size.height-20);
    
    [self addSubview:separaterLine];
    [self addSubview:separaterLine2];
    [self addSubview:verticalLine];
    [self addSubview:verticalLine2];

}

- (void)setType:(UserProfileTabType)type
{
    _type = type;
    
    
    switch (_type) {
        case UserProfileTabSelectGrid:
        {
            if (_gridButton.layer.borderWidth == 1)
                return;
            
            [_gridButton playBounceAnimation];
            
            [_gridButton setSelected:YES];
            [_listButton setSelected:NO];
            [_favorButton setSelected:NO];

            break;
        }
            
        case UserProfileTabSelectList:
        {
            if (_listButton.layer.borderWidth == 1)
                return;
            
            [_listButton playBounceAnimation];
            
            [_gridButton setSelected:NO];
            [_listButton setSelected:YES];
            [_favorButton setSelected:NO];
            break;
        }
            
        case UserProfileTabSelectFavor:
        {
            if (_favorButton.layer.borderWidth == 1)
                return;
            
            [_favorButton playBounceAnimation];

            [_gridButton setSelected:NO];
            [_listButton setSelected:NO];
            [_favorButton setSelected:YES];
            break;
        }
            
        default:
            break;
    }
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(tabClickWithType:)])
        [self.delegate tabClickWithType:_type];
}

- (void)clickGrid
{
    self.type = UserProfileTabSelectGrid;
}

- (void)clickList
{
    self.type = UserProfileTabSelectList;
}

- (void)clickFavor
{
    self.type = UserProfileTabSelectFavor;
}

@end
