//
//  UserProfileCellTitel.h
//  Story17
//
//  Created by POPO on 2015/5/7.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserObject.h"
#import "MultiFileUploader.h"
#import <SafariServices/SafariServices.h>

@interface UserProfileTitleCell : UICollectionViewCell <UIImagePickerControllerDelegate, SFSafariViewControllerDelegate>
{
@private
    UIImageView* _userImage;
    UIImageView* _maskImage;

    UILabel* _followerLabel;
    UILabel* _followingLabel;
    UIButton* _suggestionBtn;

}


@property (nonatomic,strong) UIImageView* userImage;
@property (nonatomic,strong) UIImageView* editProfileImageView;
@property (nonatomic,strong) UIImageView* giftDetailImageView;


@property(nonatomic)bool isSelected;
@property(nonatomic)bool isLocalUser;
@property(nonatomic, weak) UserObject* user;
@property(nonatomic, weak) UINavigationController* navCtrl;
@property(nonatomic, copy)void (^suggestionCallback)(void);
@property (nonatomic,strong) MultiFileUploader* multiFileUploader;


@property (nonatomic,strong) UILabel* nameLabel;
@property (nonatomic,strong) UILabel* bioLabel;
@property (nonatomic,strong) STTweetLabel* websiteLabel;

@property (nonatomic,strong) UIButton* reviewButton;
@property (nonatomic,strong) UIButton* reviewProfileButton;
@property (nonatomic,strong) UIButton* postReviewButton;
@property (nonatomic,strong) UIButton* reportReviewButtopn;
@property (nonatomic,strong) UIButton* adminPaymentButton;

@property (nonatomic,strong) UIButton* userReportBtn;
@property (nonatomic,strong) UIButton* followBtn;
@property (nonatomic,strong) UIImageView* verifiedBadge;

/* For Admin Only */
@property (nonatomic,strong) UIImageView* chinaBadge;
@property (nonatomic,strong) UIImageView* choiceBadge;
@property (nonatomic,strong) UIImageView* internationalBadge;
@property (nonatomic,strong) UIImageView* celebrityBadge;


@property (nonatomic,strong) UIButton* heartButton;
@property (nonatomic,strong) UIButton* livestreammingButton;

@property (strong,nonatomic) AFHTTPRequestOperationManager *manager; // compose


+ (float)getDefaultHeight:(UserObject*)user;

@end
