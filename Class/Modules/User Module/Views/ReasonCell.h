//
//  ReasonCell.h
//  story17
//
//  Created by POPO Chen on 6/2/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReasonCell : UITableViewCell

@property (nonatomic,strong) UILabel* mainTextLabel;
+ (float)getHeightWithText:(NSString*)text;
-(void)reloadCell:(NSString*)text;

@end
