//
//  TitleLabelView.h
//  Story17
//
//  Created by POPO on 5/12/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface TitleLabelView : UIView

@property (nonatomic,strong) UILabel* titleLabel;

@end
