//
//  NoDataCollectionViewCell.m
//  Story17
//
//  Created by York on 2015/5/31.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "PrivacyModeCollectionViewCell.h"
#import "Constant.h"

@implementation PrivacyModeCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        _privacyImageView = [ThemeManager getNoDataImageView];
        [_privacyImageView setImage:[UIImage imageNamed:@"private"]];
        CGRect rect = _privacyImageView.frame;
        rect.origin.y = -20;
        _privacyImageView.frame = rect;
        _privacyImageView.frame= CGRectMake(_privacyImageView.frame.origin.x, _privacyImageView.frame.origin.y, _privacyImageView.frame.size.width/3*2, _privacyImageView.frame.size.height/3*2);
        _privacyImageView.center=CGPointMake(SCREEN_WIDTH/2,_privacyImageView.center.y);
        _privacyImageView.hidden = NO;
        UILabel* privacyRemind=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
        [privacyRemind setText:LOCALIZE(@"Privacy remind")];
        privacyRemind.numberOfLines=2;
        [privacyRemind sizeToFit];
        [privacyRemind setTextColor:GRAY_COLOR];
        privacyRemind.center=CGPointMake(_privacyImageView.center.x, _privacyImageView.frame.origin.y+_privacyImageView.frame.size.height+10);
        [self.contentView addSubview:privacyRemind];
        [self.contentView addSubview:_privacyImageView];
    }
    return self;
}

@end
