//
//  UserInfoCell.m
//  story17
//
//  Created by POPO Chen on 6/1/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "UserInfoCell.h"
#import "Constant.h"
#import "RevenueReportViewController.h"
#import "UserListViewController.h"

@implementation UserInfoCell

#define BioFontSize 14
#define UserProfileNameHeight 25
#define UserProfileWebsiteHeight 25
#define ButtonHeight 30

+ (float)getDefaultHeightWithBio:(UserObject*)user
{
    float height = 0.0f;
    if([user.userID isEqualToString:MY_USER_ID]){
        height += ButtonHeight+5;
    }

    CGSize stringSize = [user.bio getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-20,150) withFont:SYSTEM_FONT_WITH_SIZE(BioFontSize)];

    if([user.bio isEqualToString:@""]){
        height += UserProfileNameHeight+5;
    }else{
        height += UserProfileNameHeight + stringSize.height+5;
    }
    
    if(![user.website isEqualToString:@""]){

        CGSize webStringSize = [user.bio getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-20,150) withFont:SYSTEM_FONT_WITH_SIZE(BioFontSize)];

        height += webStringSize.height+5;
    }
    return height;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = BOLD_FONT_WITH_SIZE(16);
        [_nameLabel setTextColor:DARK_GRAY_COLOR];
        [self.contentView addSubview:_nameLabel];
        
        _bioLabel = [[UILabel alloc] init];
        _bioLabel.textAlignment = NSTextAlignmentLeft;
        _bioLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _bioLabel.numberOfLines = 0;
        [self.contentView addSubview:_bioLabel];
        
        _bioLabel.font = SYSTEM_FONT_WITH_SIZE(BioFontSize);
        [_bioLabel setTextColor:GRAY_COLOR];
        
        _userReportBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_userReportBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"]  resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
        [_userReportBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"]  resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        _userReportBtn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        [_userReportBtn setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
        
        _userReportBtn.hidden = YES;
        [_userReportBtn addTarget:self action:@selector(clickReport) forControlEvents:UIControlEventTouchDown];
        [self.contentView addSubview:_userReportBtn];

        _websiteLabel = [[STTweetLabel alloc] init];
        _websiteLabel.textAlignment = NSTextAlignmentLeft;
        [_websiteLabel setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(14)}
                      hotWord:STTweetHandle];
        [_websiteLabel setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(14)}
                      hotWord:STTweetHashtag];
        [_websiteLabel setAttributes:@{NSForegroundColorAttributeName: GRAY_COLOR,
                                NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(14)}];
        [self.contentView addSubview:_websiteLabel];
        
        [_websiteLabel setDetectionBlock:^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range) {
          
            
            [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"open_on_safari") message:string buttonText:LOCALIZE(@"OK") cancelable:YES withCompletion:^(BOOL okClicked) {
                if(okClicked){
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
                }
            }];
        }];

        _verifiedBadge = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"verified")];
        [self.contentView addSubview:_verifiedBadge];
        _verifiedBadge.hidden = YES;
        
        
        _heartView = [UIView new];
        _heartView.userInteractionEnabled = YES;
        [ _heartView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            
            if(![_user.userID isEqualToString:MY_USER_ID] && [GET_DEFAULT(IS_ADMIN) intValue]==0)
                return;
            
            UserListViewController* ulVC  = [UserListViewController new];
            ulVC.mode = USER_LIKER_MODE;
            ulVC.likesPost = nil;
            ulVC.targetUser = _user;
            ulVC.title =  [NSString stringWithFormat:LOCALIZE(@"UserLiker"),_user.openID];
            ulVC.hidesBottomBarWhenPushed = YES;
            [_navCtrl pushViewController:ulVC animated:YES];

        }]];
        
        [self.contentView addSubview:_heartView];
        
        _heartImageView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"all_like")];
        [_heartView addSubview:_heartImageView];
        
        _heartCountLabel = [UILabel new];
        _heartCountLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_heartCountLabel  setTextColor:MAIN_COLOR];
        [_heartView addSubview:_heartCountLabel];

    }
    
    return self;
}


-(void)reloadCell:(UserObject*)user
{
    _user = user;
    
    if([_user.userID isEqualToString:MY_USER_ID])
        _userReportBtn.hidden = NO;
    
    [_nameLabel setText:user.name];
    _bioLabel.text = user.bio;
    
//    DLog(@"%@",user.name);
    
    int nextY = 0;
    
    [_nameLabel setFrame:CGRectMake(10, nextY, SCREEN_WIDTH-20, UserProfileNameHeight)];
    _nameLabel.numberOfLines = 1;
    [_nameLabel sizeToFit];
    
    /* verify && heart setting */
    _verifiedBadge.frame = CGRectMake(_nameLabel.frame.origin.x+_nameLabel.frame.size.width+5, _nameLabel.frame.origin.y+2, 15, 15);
    if(_user.isVerified==1){
        _verifiedBadge.hidden = NO;
    }else{
        _verifiedBadge.hidden = YES;
    }
    
    [_heartCountLabel setText: [SINGLETON getCountNumNormalization:user.receivedLikeCount]];
    [_heartCountLabel sizeToFit];
    _heartCountLabel.frame = CGRectMake(25, 5, _heartCountLabel.frame.size.width, _heartCountLabel.frame.size.height);
    _heartImageView.frame = CGRectMake(5, 5, 15, 15);
    _heartView.frame = CGRectMake(SCREEN_WIDTH-_heartCountLabel.frame.size.width-35-10, _verifiedBadge.frame.origin.y-5, _heartCountLabel.frame.size.width+20+10, _heartCountLabel.frame.size.height+10);
    
    nextY += UserProfileNameHeight;

    CGSize stringSize = [user.bio getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-20,150) withFont:SYSTEM_FONT_WITH_SIZE(BioFontSize)];

    if([_user.bio isEqualToString:@""]){
        [_bioLabel setFrame:CGRectMake(10, nextY, SCREEN_WIDTH-20, 0)];
    }else{
        [_bioLabel setFrame:CGRectMake(10, nextY, SCREEN_WIDTH-20, stringSize.height)];
    }
    
    nextY += _bioLabel.frame.size.height+2;
    
    if([_user.website isEqualToString:@""]){
        [_websiteLabel setFrame:CGRectMake(10, nextY, SCREEN_WIDTH-20, 0)];
    }else {
        
        CGSize webStringSize = VIEW_SIZE([_websiteLabel suggestedFrameSizeToFitEntireStringConstrainedToWidth:(SCREEN_WIDTH-20)]);
        
        [_websiteLabel setFrame:CGRectMake(10, nextY, SCREEN_WIDTH-20, webStringSize.height)];
        _websiteLabel.text = _user.website;
    }
    
    nextY += _websiteLabel.frame.size.height+5;
    
    [_userReportBtn setTitle:[NSString stringWithFormat:LOCALIZE(@"todays_royalties"),[SINGLETON getCurrencyRate]*[REVENUE_MANAGER todayRevenue],[SINGLETON getCurrencyType]]  forState:UIControlStateNormal];
    _userReportBtn.frame = CGRectMake(10, nextY, SCREEN_WIDTH-20, ButtonHeight);
}

- (void)clickReport
{
    if (self.navCtrl == nil)
        return;
    
    static RevenueReportViewController* ctrl = nil;
    if (ctrl == nil)
        ctrl = [[RevenueReportViewController alloc] init];
    
    ctrl.user = _user;
    ctrl.hidesBottomBarWhenPushed = YES;
    [self.navCtrl pushViewController:ctrl animated:YES];
}


@end
