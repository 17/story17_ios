//
//  ReasonCell.m
//  story17
//
//  Created by POPO Chen on 6/2/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "ReasonCell.h"
#import "Constant.h"

@implementation ReasonCell

#define leftMargin 20
#define topMargin 10

+ (float)getHeightWithText:(NSString*)reason
{
    CGSize stringSize = [reason getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-2*leftMargin,SCREEN_HEIGHT) withFont:SYSTEM_FONT_WITH_SIZE(14)];

    if(stringSize.height+2*topMargin<50){
        return 50;
    }else{
        return 2*topMargin+stringSize.height;
    }
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self awakeFromNib];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)awakeFromNib {
    
    _mainTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH-10, 10)];
    _mainTextLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    _mainTextLabel.numberOfLines = 0;
    [_mainTextLabel setTextColor:DARK_GRAY_COLOR];
    [self.contentView addSubview:_mainTextLabel];
}

-(void)reloadCell:(NSString*)reason;
{
    CGSize stringSize = [reason getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-2*leftMargin,SCREEN_HEIGHT) withFont:SYSTEM_FONT_WITH_SIZE(14)];

    [_mainTextLabel setText:reason];
    if(stringSize.height+2*topMargin<50){
        _mainTextLabel.frame = CGRectMake(leftMargin, topMargin, stringSize.width, 50-2*topMargin);
    }else{
        _mainTextLabel.frame = CGRectMake(leftMargin, topMargin, stringSize.width, stringSize.height);
    }
}

@end
