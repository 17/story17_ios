//
//  TitleLabelView.m
//  Story17
//
//  Created by POPO on 5/12/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "TitleLabelView.h"

@implementation TitleLabelView

#define labelHeight 30

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, SCREEN_WIDTH, labelHeight)];
        [_titleLabel setTextColor:GRAY_COLOR];
        _titleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        [self addSubview:_titleLabel];
    }
    return self;
}

-(int) getViewHeight
{
    return labelHeight;
}

@end
