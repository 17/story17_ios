//
//  UserProfileTabCell.h
//  Story17
//
//  Created by POPO on 2015/5/12.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    UserProfileTabSelectGrid = 0,
    UserProfileTabSelectList = 1,
    UserProfileTabSelectFavor
} UserProfileTabType;

@protocol UserProfileTabCellDelegate <NSObject>

- (void)tabClickWithType:(UserProfileTabType)type;

@end

@interface UserProfileTabCell : UICollectionViewCell
{
@private
    UIButton* _gridButton;
    UIButton* _listButton;
    UIButton* _favorButton;
}

@property(nonatomic)UserProfileTabType type;
@property(nonatomic, weak)id<UserProfileTabCellDelegate> delegate;

+ (float)getDefaultHeight;

@end
