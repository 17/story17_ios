//
//  UserInfoCell.h
//  story17
//
//  Created by POPO Chen on 6/1/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserObject.h"
#import "STTweetLabel.h"

@interface UserInfoCell : UICollectionViewCell

@property (nonatomic,strong) UILabel* nameLabel;
@property (nonatomic,strong) UILabel* bioLabel;
@property (nonatomic,strong) STTweetLabel* websiteLabel;
@property (nonatomic,strong) UIButton* userReportBtn;
@property(nonatomic, strong)UIImageView* verifiedBadge;

@property (nonatomic,strong) UserObject* user;
@property(nonatomic, strong)UIImageView* heartImageView;
@property(nonatomic, strong)UILabel* heartCountLabel;
@property(nonatomic, strong)UIView* heartView;

@property(nonatomic, weak)UINavigationController* navCtrl;

+ (float)getDefaultHeightWithBio:(UserObject*)user;
-(void)reloadCell:(UserObject*)user;

@end
