//
//  UserProfileCellTitel.m
//  Story17
//
//  Created by POPO on 2015/5/7.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "UserProfileTitleCell.h"
#import "Constant.h"
#import "UIImageView+AFNetworking.h"
#import "EditProfileViewController.h"
#import "UserListCell.h"
#import "RevenueReportViewController.h"
#import "UserListViewController.h"
#import "LiveStreamViewController.h"
#import "LiveManageViewController.h"
#import "PostManageViewController.h"
#import "AdminPaymentViewController.h"
#import "ApiManager.h"
#import "GiftBoardViewController.h"
#import "LoginHandler.h"



#import "AFHTTPRequestOperationManager.h"
#import "AFHTTPRequestOperation.h"
#import "BNCEncodingUtils.h"


#define UserProfileHorizontalPadding 7
#define UserProfilePadding 7
#define UserProfileImageSize 75
#define UserLabelHeight 30
#define BioFontSize 14
#define LivestreamingLabelHeight 30

@implementation UserProfileTitleCell
@synthesize manager;


@synthesize user = _user;
@synthesize isLocalUser = _isLocalUser;
@synthesize suggestionCallback = _suggestionCallback;

+ (float)getDefaultHeight:(UserObject*)user
{
    float height = 0.0f;

    height += 7*UserProfilePadding+UserProfileImageSize;
    
    CGSize stringSize = [user.bio getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-20,150) withFont:SYSTEM_FONT_WITH_SIZE(BioFontSize)];

    UILabel* nameLabel = [[UILabel alloc] init];
    nameLabel.font = BOLD_FONT_WITH_SIZE(16);
    [nameLabel setText:user.name];
    [nameLabel sizeToFit];
    
    if([user.bio isEqualToString:@""]){
        height += nameLabel.frame.size.height;
    }else{
        height +=  nameLabel.frame.size.height + stringSize.height;
    }
    
    if(![user.website isEqualToString:@""]){
        
        CGSize webStringSize = [user.website getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-20,150) withFont:SYSTEM_FONT_WITH_SIZE(BioFontSize)];
        height += webStringSize.height;
    }
    
    if(user.isLivestreamming==1){
        height += UserProfilePadding + LivestreamingLabelHeight;
    }
    
    height += UserLabelHeight*2;
    
    return height;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        _isSelected = NO;
        
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImage)];
        _userImage = [[UIImageView alloc] init];
        [_userImage addGestureRecognizer:tap];
        [_userImage setUserInteractionEnabled:YES];
        [self.contentView addSubview:_userImage];
        _maskImage = [[UIImageView alloc] init];
        [self.contentView addSubview:_maskImage];
        [_maskImage setImage:[UIImage imageNamed:@"head_mask_gray"]];
        
        _followerLabel = [[UILabel alloc] init];
        [self setupLabel:_followerLabel];
        
        _followingLabel = [[UILabel alloc] init];
        [self setupLabel:_followingLabel];

        _reviewButton = [UIButton new];
        [_reviewButton setFrame:CGRectMake(10, 40, FOLLOW_BTN_WIDTH, FOLLOW_BTN_HEIGHT)];
        _reviewButton.titleLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_reviewButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
        [_reviewButton setTitle:LOCALIZE(@"review_livestream_title") forState:UIControlStateNormal];
        _reviewButton.layer.borderWidth = 0.5f;
        _reviewButton.layer.borderColor = [MAIN_COLOR CGColor];
        [_reviewButton addTarget:self action:@selector(gotoViewController:) forControlEvents:UIControlEventTouchUpInside];
    
        /******/
        _reviewProfileButton = [UIButton new];
        [_reviewProfileButton setFrame:CGRectMake(10+5+FOLLOW_BTN_WIDTH, 40, FOLLOW_BTN_WIDTH, FOLLOW_BTN_HEIGHT)];
        _reviewProfileButton.titleLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_reviewProfileButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
        [_reviewProfileButton setTitle:LOCALIZE(@"review_profile_pic_title") forState:UIControlStateNormal];
        _reviewProfileButton.layer.borderWidth = 0.5f;
        _reviewProfileButton.layer.borderColor = [MAIN_COLOR CGColor];
        [_reviewProfileButton addTarget:self action:@selector(gotoViewController:) forControlEvents:UIControlEventTouchUpInside];
        
        _postReviewButton = [UIButton new];
        [_postReviewButton setFrame:CGRectMake(10, 40+5+FOLLOW_BTN_HEIGHT, FOLLOW_BTN_WIDTH, FOLLOW_BTN_HEIGHT)];
        _postReviewButton.titleLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_postReviewButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
        [_postReviewButton setTitle:LOCALIZE(@"review_post_title") forState:UIControlStateNormal];
        _postReviewButton.layer.borderWidth = 0.5f;
        _postReviewButton.layer.borderColor = [MAIN_COLOR CGColor];
        [_postReviewButton addTarget:self action:@selector(gotoViewController:) forControlEvents:UIControlEventTouchUpInside];
        
        _reportReviewButtopn = [UIButton new];
        [_reportReviewButtopn setFrame:CGRectMake(10, 40+10+2*FOLLOW_BTN_HEIGHT, FOLLOW_BTN_WIDTH, FOLLOW_BTN_HEIGHT)];
        _reportReviewButtopn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_reportReviewButtopn setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
        [_reportReviewButtopn setTitle:LOCALIZE(@"review_report_title") forState:UIControlStateNormal];
        _reportReviewButtopn.layer.borderWidth = 0.5f;
        _reportReviewButtopn.layer.borderColor = [MAIN_COLOR CGColor];
        [_reportReviewButtopn addTarget:self action:@selector(gotoViewController:) forControlEvents:UIControlEventTouchUpInside];
        
        _adminPaymentButton = [UIButton new];
        [_adminPaymentButton setFrame:CGRectMake(SCREEN_WIDTH-FOLLOW_BTN_WIDTH-10, 20+15+FOLLOW_BTN_HEIGHT, FOLLOW_BTN_WIDTH, FOLLOW_BTN_HEIGHT)];
        _adminPaymentButton.titleLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_adminPaymentButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
        [_adminPaymentButton setTitle:@"分潤匯款" forState:UIControlStateNormal];
        _adminPaymentButton.layer.borderWidth = 0.5f;
        _adminPaymentButton.layer.borderColor = [MAIN_COLOR CGColor];
        [_adminPaymentButton addTarget:self action:@selector(gotoViewController:) forControlEvents:UIControlEventTouchUpInside];
//        [self.contentView addSubview:_adminPaymentButton];
        
        _followerLabel.userInteractionEnabled = YES;
        _followingLabel.userInteractionEnabled = YES;
        
        _editProfileImageView = [[UIImageView alloc]initWithImage:IMAGE_FROM_BUNDLE(@"edit")];
        _editProfileImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickEditAction)];
        [_editProfileImageView addGestureRecognizer:tapGesture];
        [self.contentView addSubview:_editProfileImageView];
        
        
        _giftDetailImageView = [[UIImageView alloc]initWithImage:IMAGE_FROM_BUNDLE(@"giftlist")];
        _giftDetailImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer* tapGesture1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickGiftAction)];
        [_giftDetailImageView addGestureRecognizer:tapGesture1];
        [self.contentView addSubview:_giftDetailImageView];
        
        
        
        /* user info */
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = BOLD_FONT_WITH_SIZE(16);
        [_nameLabel setTextColor:DARK_GRAY_COLOR];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_nameLabel];
        
        _bioLabel = [[UILabel alloc] init];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _bioLabel.numberOfLines = 0;
        [self.contentView addSubview:_bioLabel];
        
        _bioLabel.font = SYSTEM_FONT_WITH_SIZE(BioFontSize);
        _bioLabel.textAlignment = NSTextAlignmentCenter;
        [_bioLabel setTextColor:GRAY_COLOR];
        
        _userReportBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_userReportBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"]  resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
        [_userReportBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"]  resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        _userReportBtn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        [_userReportBtn setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    
        _userReportBtn.hidden = YES;
        [_userReportBtn addTarget:self action:@selector(clickReport) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_userReportBtn];
        
        _followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _followBtn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        _followBtn.hidden = YES;
        [_followBtn addTarget:self action:@selector(clickFollow) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_followBtn];
        
        _suggestionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_suggestionBtn setBackgroundImage:IMAGE_FROM_BUNDLE(@"btn_downward_down") forState:UIControlStateNormal];
        [_suggestionBtn setBackgroundImage:IMAGE_FROM_BUNDLE(@"btn_downward") forState:UIControlStateNormal];
        [_suggestionBtn addTarget:self action:@selector(clickSuggestion) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_suggestionBtn];

        _websiteLabel = [[STTweetLabel alloc] init];
        [_websiteLabel setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                       NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(14)}
                             hotWord:STTweetHandle];
        [_websiteLabel setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                       NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(14)}
                             hotWord:STTweetHashtag];
        [_websiteLabel setAttributes:@{NSForegroundColorAttributeName: GRAY_COLOR,
                                       NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(14)}];
        [self.contentView addSubview:_websiteLabel];
        
        [_websiteLabel setDetectionBlock:^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range) {
            [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"open_on_safari") message:string buttonText:LOCALIZE(@"OK") cancelable:YES withCompletion:^(BOOL okClicked) {
                NSString *myURL;
                myURL = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

                if(okClicked){
                    if (![string.lowercaseString hasPrefix:@"http://"]&&![string.lowercaseString hasPrefix:@"https://"]) {
                        
                        myURL = [NSString stringWithFormat:@"http://%@",string];
                    }else{
//                        myURL=string;
                    }
                    
                    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")){
                        SFSafariViewController *safariViewController = [[SFSafariViewController alloc]
                                                                        initWithURL:[NSURL URLWithString:myURL]
                                                                        entersReaderIfAvailable:YES];
                        
//                        safariViewController.delegate = self;
//                        [_navCtrl presentViewController:safariViewController animated:YES completion:nil];

                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [_navCtrl presentViewController:safariViewController animated:YES completion:nil];
                            
//                            UIViewController *rootViewController = [[
//                                                                     [UIApplication sharedApplication] keyWindow] rootViewController];
//                            
//                            [rootViewController presentViewController:safariViewController animated:YES completion: nil];
                        });
                    }else{
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
                    }
                }
            }];
        }];
        
        _websiteLabel.textAlignment = NSTextAlignmentCenter;
        
        _verifiedBadge = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"verified")];
        [self.contentView addSubview:_verifiedBadge];
        _verifiedBadge.hidden = YES;
        
        _choiceBadge = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"badge_add_choice")];
        [self.contentView addSubview:_choiceBadge];
        _choiceBadge.hidden = YES;
        
        _chinaBadge = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"badge_china")];
        [self.contentView addSubview:_chinaBadge];
        _chinaBadge.hidden = YES;
        
        _internationalBadge = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"badge_international")];
        [self.contentView addSubview:_internationalBadge];
        _internationalBadge.hidden = YES;
        
        _celebrityBadge = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"badge_star")];
        [self.contentView addSubview:_celebrityBadge];
        _celebrityBadge.hidden = YES;
        
        _heartButton = [ThemeManager getMainCircleBtn];
        [_heartButton setImage:IMAGE_FROM_BUNDLE(@"all_like") forState:UIControlStateNormal];
        [self.contentView addSubview:_heartButton];
        
        self.livestreammingButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.livestreammingButton setBackgroundImage:[[UIImage imageNamed:@"btn_greenline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
        [self.livestreammingButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
        [self.livestreammingButton.titleLabel setFont:SYSTEM_FONT_WITH_SIZE(14)];
        [self.livestreammingButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self.contentView addSubview:self.livestreammingButton];
        
        [self updateUI];
    }
    
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if (frame.size.width <= 0 || frame.size.height <= 0 || _userImage == nil)
        return;
    
    [self updateUI];
}

- (void)updateUI
{
    float nextY = UserProfilePadding;
    
    if(_user.isLivestreamming==1){
        self.livestreammingButton.frame = CGRectMake(5, UserProfilePadding, SCREEN_WIDTH-10, LivestreamingLabelHeight);
        [self.livestreammingButton setTitle:[NSString stringWithFormat:@"%@ %@",_user.openID,LOCALIZE(@"onLivestream_message")] forState:UIControlStateNormal];
        nextY += LivestreamingLabelHeight+UserProfilePadding;
        self.livestreammingButton.hidden = NO;
    }else{
        self.livestreammingButton.hidden = YES;
    }

    [_userImage setFrame:CGRectMake(SCREEN_WIDTH/2-UserProfileImageSize/2, nextY, UserProfileImageSize, UserProfileImageSize)];
//    _userImage.center = CGPointMake(SCREEN_WIDTH/2, UserProfilePadding+UserProfileImageSize/2);
    _maskImage.frame = _userImage.frame;
    
    _editProfileImageView.frame = CGRectMake(SCREEN_WIDTH-UserProfileHorizontalPadding-35-3, nextY, 30, 30);
    _giftDetailImageView.frame = CGRectMake(3, nextY, 30, 30);
    nextY += _userImage.frame.size.height + UserProfilePadding;
    
    
    
    [_nameLabel setText:_user.name];
    _nameLabel.numberOfLines = 1;
    [_nameLabel sizeToFit];
    _nameLabel.center = CGPointMake(SCREEN_WIDTH/2, nextY+_nameLabel.frame.size.height/2);
    nextY += UserProfilePadding+_nameLabel.frame.size.height;

    /* verify && heart setting */
    _verifiedBadge.frame = CGRectMake(_nameLabel.frame.origin.x+_nameLabel.frame.size.width+5, _nameLabel.frame.origin.y+2, 15, 15);
    _celebrityBadge.frame = CGRectMake(_verifiedBadge.frame.origin.x+_verifiedBadge.frame.size.width+5, _nameLabel.frame.origin.y+2, 15, 15);
    _chinaBadge.frame = CGRectMake(_celebrityBadge.frame.origin.x+_celebrityBadge.frame.size.width+5, _nameLabel.frame.origin.y+2, 15, 15);
    _internationalBadge.frame = CGRectMake(_chinaBadge.frame.origin.x+_chinaBadge.frame.size.width+5, _nameLabel.frame.origin.y+2, 15, 15);
    _choiceBadge.frame = CGRectMake(_internationalBadge.frame.origin.x+_internationalBadge.frame.size.width+5, _nameLabel.frame.origin.y+2, 15, 15);

    
    if(_user.isVerified==1){
        _verifiedBadge.hidden = NO;
    }else{
        _verifiedBadge.hidden = YES;
    }
    
    if(_user.isChoice==1 && [GET_DEFAULT(IS_ADMIN) intValue] >=1){
        _choiceBadge.hidden = NO;
    }else{
        _choiceBadge.hidden = YES;
    }
    
    if(_user.crossGreatWall==1  && [GET_DEFAULT(IS_ADMIN) intValue] >=1){
        _chinaBadge.hidden = NO;
    }else{
        _chinaBadge.hidden = YES;
    }
    
    if(_user.isInternational==1  && [GET_DEFAULT(IS_ADMIN) intValue] >=1){
        _internationalBadge.hidden = NO;
    }else{
        _internationalBadge.hidden = YES;
    }
    
    if(_user.isCelebrity==1  && ([_user.userID isEqualToString:MY_USER_ID] || [GET_DEFAULT(IS_ADMIN) intValue] >=1)){
        _celebrityBadge.hidden = NO;
    }else{
        _celebrityBadge.hidden = YES;
    }
    
    CGSize stringSize = [_user.bio getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-20,150) withFont:SYSTEM_FONT_WITH_SIZE(BioFontSize)];

    [_bioLabel setText:_user.bio];
    if([_user.bio isEqualToString:@""]){
        [_bioLabel setFrame:CGRectMake(10, nextY, SCREEN_WIDTH-20, 0)];
    }else{
        [_bioLabel setFrame:CGRectMake(10, nextY, SCREEN_WIDTH-20, stringSize.height)];
    }
    
    nextY += _bioLabel.frame.size.height+UserProfilePadding;
    
    if([_user.website isEqualToString:@""]){
        
        [_websiteLabel setFrame:CGRectMake(10, nextY, SCREEN_WIDTH-20, 0)];
    
    }else {
        
        CGSize webStringSize = VIEW_SIZE([_websiteLabel suggestedFrameSizeToFitEntireStringConstrainedToWidth:(SCREEN_WIDTH-20)]);

        [_websiteLabel setFrame:CGRectMake(10, nextY, SCREEN_WIDTH-20, webStringSize.height)];
        _websiteLabel.text = _user.website;
        _websiteLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    nextY += _websiteLabel.frame.size.height+UserProfilePadding;

    float labelWidth = ((SCREEN_WIDTH/2)-UserProfileHorizontalPadding*3)/2;

    _followerLabel.frame = CGRectMake(UserProfileHorizontalPadding, nextY, labelWidth, UserLabelHeight);
    _followingLabel.frame = CGRectMake(UserProfileHorizontalPadding+_followerLabel.frame.origin.x+_followerLabel.frame.size.width, nextY, labelWidth, UserLabelHeight);
    
    _heartButton.frame = CGRectMake(SCREEN_WIDTH/2, nextY, SCREEN_WIDTH/2-2*UserProfileHorizontalPadding, UserLabelHeight);
    [_heartButton setTitle:[SINGLETON getCountNumNormalization:_user.receivedLikeCount] forState:UIControlStateNormal];
    [_heartButton addTarget:self action:@selector(selectHeartButton) forControlEvents:UIControlEventTouchUpInside];


    [_heartButton setImageEdgeInsets:UIEdgeInsetsMake(0, CGRectGetWidth(_heartButton.frame)-35, 0, 0)];

    
    nextY += _heartButton.frame.size.height+UserProfilePadding;

    _userReportBtn.frame = CGRectMake(UserProfileHorizontalPadding, nextY, SCREEN_WIDTH-2*UserProfileHorizontalPadding-4, UserLabelHeight);
    [_userReportBtn setTitle:[NSString stringWithFormat:LOCALIZE(@"todays_royalties"),[SINGLETON getCurrencyRate]*[REVENUE_MANAGER todayRevenue],[SINGLETON getCurrencyType]]  forState:UIControlStateNormal];
//    DLog(@"userReportBtn:%@",_userReportBtn.titleLabel.text);
    
    _followBtn.frame = CGRectMake(UserProfileHorizontalPadding, nextY, SCREEN_WIDTH-3*UserProfileHorizontalPadding-30, UserLabelHeight);
    _suggestionBtn.frame = CGRectMake(SCREEN_WIDTH-UserProfileHorizontalPadding-30, nextY, 30, UserLabelHeight);

    if(_isLocalUser){

        _editProfileImageView.hidden = NO;
        _userReportBtn.hidden = NO;
        _followBtn.hidden = YES;
        _suggestionBtn.hidden = YES;
        
    }else{

        _editProfileImageView.hidden = YES;
        _userReportBtn.hidden = YES;
        _followBtn.hidden = NO;
        _suggestionBtn.hidden = NO;
    }

}

- (void)setupLabel:(UILabel *)label
{
    label.textAlignment = NSTextAlignmentCenter;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
    label.font = [UIFont systemFontOfSize:12];
    
    [self addSubview:label];
}

- (void)setUser:(UserObject *)user
{
    _user = user;
    
    if([_user.picture isEqualToString:@""]){
        [_userImage setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }else{
        [_userImage setImageWithURL:S3_FILE_URL(_user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }
    
    if([GET_DEFAULT(IS_ADMIN) intValue] >=1 && [user.userID isEqualToString:MY_USER_ID]){
        [self.contentView addSubview:_reviewButton];
        [self.contentView addSubview:_postReviewButton];
        [self.contentView addSubview:_reportReviewButtopn];
        [self.contentView addSubview:_reviewProfileButton];
    }
    
    NSString* followerCount = [SINGLETON getCountNumNormalization:_user.followerCount];
    NSString* followingCount = [SINGLETON getCountNumNormalization:_user.followingCount];

    _followerLabel.text = [NSString stringWithFormat:@"%@\n%@", followerCount, LOCALIZE(@"user_profile_followers")];
    _followingLabel.text = [NSString stringWithFormat:@"%@\n%@", followingCount, LOCALIZE(@"user_profile_following")];
    
    if ([GET_DEFAULT(IN_SANDBOX) isEqualToString:@"1"] || (_user.followPrivacyMode==1&&!_isLocalUser)) {
        
        [_followerLabel setTextColor:GRAY_COLOR];
        [_followingLabel setTextColor:GRAY_COLOR];
        
        
        /* Remove Gesture Recognizer */
        for(UIGestureRecognizer* gesture in [_followerLabel gestureRecognizers]){
            [_followerLabel removeGestureRecognizer:gesture];
        }
        
        for(UIGestureRecognizer* gesture in [_followingLabel gestureRecognizers]){
            [_followingLabel removeGestureRecognizer:gesture];
        }
        
    } else {
        
        [_followerLabel setTextColor:BLACK_COLOR];
        [_followingLabel setTextColor:BLACK_COLOR];

        [_followerLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            
            UserListViewController* ulVC  = [UserListViewController new];
            ulVC.mode = FOLLOWER_MODE;
            ulVC.targetUser = _user;
            ulVC.title = LOCALIZE(@"FOLLOWER");
            ulVC.hidesBottomBarWhenPushed = YES;
            [_navCtrl pushViewController:ulVC animated:YES];
            
        }]];
        
        [_followingLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            
            UserListViewController* ulVC  = [UserListViewController new];
            ulVC.mode = FOLLOWING_MODE;
            ulVC.targetUser = _user;
            ulVC.title = LOCALIZE(@"FOLLOWING");
            ulVC.hidesBottomBarWhenPushed = YES;
            [_navCtrl pushViewController:ulVC animated:YES];
        }]];
    }
    
    [self updateUI];
    [self reloadButton];
}

-(void)gotoViewController:(id)sender
{
    if([sender isEqual:_reviewButton]) {
        LiveManageViewController* liveVC = [LiveManageViewController new];
        [self.navCtrl presentViewController:NAV(liveVC) animated:YES completion:nil];
    } else if([ sender isEqual:_postReviewButton]) {
        PostManageViewController* postVC = [PostManageViewController new];
        [self.navCtrl presentViewController:NAV(postVC) animated:YES completion:nil];
    } else if([ sender isEqual:_adminPaymentButton]) {
        AdminPaymentViewController* adminPaymentVC = [AdminPaymentViewController new];
        [self.navCtrl presentViewController:NAV(adminPaymentVC) animated:YES completion:nil];
    }else if([ sender isEqual:_reportReviewButtopn]) {        
        PostManageViewController* postVC = [PostManageViewController new];
        postVC.reviewMode = REPORT_POST_MODE;
        [self.navCtrl presentViewController:NAV(postVC) animated:YES completion:nil];
    }else if([sender isEqual:_reviewProfileButton]){
        PostManageViewController* postVC = [PostManageViewController new];
        postVC.reviewMode = REVIEW_PROFILE_PICTURE;
        [self.navCtrl presentViewController:NAV(postVC) animated:YES completion:nil];
    }
}

- (void)setIsLocalUser:(bool)isLocalUser
{
    _isLocalUser = isLocalUser;
    if (_isLocalUser){
        [_suggestionBtn setHidden:true];
        _editProfileImageView.hidden = NO;
    }else{
        [_suggestionBtn setHidden:false];
        _editProfileImageView.hidden = YES;
    }
}

- (void)clickImage
{
    if (!_isLocalUser)
        return;
    
    [DIALOG_MANAGER showActionSheetDialogTitle:@"" options:@[LOCALIZE(@"take_picture"),LOCALIZE(@"albom")] destructiveIndexes:@[] cancelable:YES withCompletion:^(int selectedOption) {
        if(selectedOption==1){
            
            // 從相機膠卷選擇
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            [[UIBarButtonItem appearance] setBackgroundImage:nil  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
            [[UIBarButtonItem appearance] setBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
            
            imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
            imagePicker.delegate = (id) self;
            imagePicker.allowsEditing = YES;
            [self.navCtrl presentViewController:imagePicker animated:YES completion:^{
                
            }];
            

            
        }else if (selectedOption==0){
            
            // 照相
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
            imagePicker.cameraDevice=UIImagePickerControllerCameraDeviceFront;
            imagePicker.delegate = (id) self;
            imagePicker.allowsEditing = YES;
            [self.navCtrl presentViewController:imagePicker animated:YES completion:^{
                
            }];
            
        }else{
            
        }
        
    }];
}

- (void)clickEditAction
{
    EditProfileViewController* ctrl = nil;
    if (ctrl == nil)
    {
        ctrl = [[EditProfileViewController alloc] init];
        ctrl.hidesBottomBarWhenPushed = YES;
        [ctrl setTitle:@"Edit"];
    }
    



    [self.navCtrl pushViewController:ctrl animated:YES];
}

- (void)clickGiftAction
{
    if ([LoginHandler isGuest]) {
        [LoginHandler handleUserLogin];
        return;
    }
    
    GiftBoardViewController* ctrl = nil;
    if (ctrl == nil)
    {
        ctrl = [[GiftBoardViewController alloc] init];
        ctrl.hidesBottomBarWhenPushed = YES;
        ctrl.user=_user;
    }
    
    [self.navCtrl pushViewController:ctrl animated:YES];
}


-(void)clickFollow
{
    if ([LoginHandler isGuest]) {
        [LoginHandler handleUserLogin];
        return;
    }
    
    [_followBtn playBounceAnimation];
    
    if(_user.isFollowing==1){
        _user.isFollowing=0;
        _user.followRequestTime=0;
        [self setUser:_user];
        [API_MANAGER unfollowUserAction:_user.userID withCompletion:^(BOOL success) {}];
    }else{
        if(![_user isPrivacyMode]){
            if([GET_DEFAULT(FOLLOWING_COUNT) intValue] >= MAX_FOLLOWER_COUNT){
                [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"cannot_follow_user") message:[NSString stringWithFormat:LOCALIZE(@"max_followcount"),MAX_FOLLOWER_COUNT] buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                    if(okClicked){
                        
                    }
                }];
            }else{
                _user.isFollowing=1;
                [self setUser:_user];
                [API_MANAGER followUserAction:_user.userID withCompletion:^(BOOL success) {}];
            }
        }else{
            if(_user.followRequestTime==0){
                
                if([GET_DEFAULT(FOLLOWING_COUNT) intValue] >= MAX_FOLLOWER_COUNT){
                    [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"cannot_follow_user") message:[NSString stringWithFormat:LOCALIZE(@"max_followcount"),MAX_FOLLOWER_COUNT] buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                        if(okClicked){
                            
                        }
                    }];
                }else{
                    _user.followRequestTime = CURRENT_TIMESTAMP;
                    [self setUser:_user];
                    [API_MANAGER sendFollowRequest:_user.userID withCompletion:^(BOOL success) {
                        if(success){}
                    }];
                }
            }else{
                _user.followRequestTime = 0;
                [self setUser:_user];
                [API_MANAGER cancelFollowRequests:_user.userID withCompletion:^(BOOL success) {
                    if(success){}
                }];
            }
        }
    }
    [SINGLETON reloadFollowStatus];
    [NOTIFICATION_CENTER postNotificationName:FOLLOW_USER_NOTIFICATION object:nil];
}

- (void)clickSuggestion
{
    _isSelected = !_isSelected;
    
    [self reloadButton];
    
    if (_suggestionCallback != nil)
        _suggestionCallback();
}


# pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [SINGLETON customizeInterface];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        UIImage* imageTaken = [info valueForKey: UIImagePickerControllerEditedImage];

        if(picker.sourceType!=UIImagePickerControllerSourceTypePhotoLibrary){
            [SINGLETON saveImageWithWhiteBackgroundWithoutCompleteMessage:imageTaken];
        }
        
        [DIALOG_MANAGER showLoadingView];
        
        NSString* profilePictureFileName = [SINGLETON generateRandomFileNameWithExtension:@"jpg"];
        [SINGLETON saveImageWithThumbnailImage:imageTaken andFileName:profilePictureFileName];
        
        // Handle the result image here
        
        DLog(@"%@",profilePictureFileName);
        NSError *err = nil;
        NSData *data = [NSData dataWithContentsOfFile:GET_LOCAL_FILE_PATH(profilePictureFileName)
                                            options:NSDataReadingUncached
                                                error:&err];
        
        NSLog(@"File size is : %.2f MB",(float)data.length/1024.0f/1024.0f);
        
        self.multiFileUploader = [[MultiFileUploader alloc] initWithFileNameArray:@[profilePictureFileName, [NSString stringWithFormat:@"THUMBNAIL_%@", profilePictureFileName]]];
        
        [self.multiFileUploader startUploadWithCallback:^(NSArray *fileNameArray, int code , float progress) {
            
            if(code==MULTI_UPLOAD_SUCCESS) {
                // UPLOAD SUCCESS UPDATE USER INFO
                
                [API_MANAGER  updateUserInfo:@{@"picture":profilePictureFileName}  fetchSelfInfo:YES  completion:^(BOOL success) {
                    [DIALOG_MANAGER hideLoadingView];
                    if(success){
                        [_userImage setImageWithURL:S3_THUMB_IMAGE_URL(profilePictureFileName) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
                    }else{
                        [DIALOG_MANAGER showNetworkFailToast];
                    }
                }];
                
            } else if(code==MULTI_UPLOAD_FAIL) {
                [DIALOG_MANAGER hideLoadingView];
                [DIALOG_MANAGER showNetworkFailToast];
            } else if(code==PROGRESSING){
            
            }
        }];
    }];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [SINGLETON customizeInterface];
    [picker dismissViewControllerAnimated:YES completion:^{
    
    }];
}

- (void)clickReport
{
    if (self.navCtrl == nil)
        return;
    
    static RevenueReportViewController* ctrl = nil;
    if (ctrl == nil)
        ctrl = [[RevenueReportViewController alloc] init];
    
    ctrl.user = _user;
    ctrl.hidesBottomBarWhenPushed = YES;
    [self.navCtrl pushViewController:ctrl animated:YES];
}


-(void)selectHeartButton
{
    if(![_user.userID isEqualToString:MY_USER_ID] && [GET_DEFAULT(IS_ADMIN) intValue]==0)
    return;
    
    UserListViewController* ulVC  = [UserListViewController new];
    ulVC.mode = USER_LIKER_MODE;
    ulVC.likesPost = nil;
    ulVC.targetUser = _user;
    ulVC.title =  [NSString stringWithFormat:LOCALIZE(@"UserLiker"),_user.openID];
    ulVC.hidesBottomBarWhenPushed = YES;
    [_navCtrl pushViewController:ulVC animated:YES];

}

-(void)reloadButton
{
    
    if(_user.isFollowing==1){
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"]  resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"]  resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        [_followBtn setTitle:LOCALIZE(@"user_profile_unfollow") forState:UIControlStateNormal];
        [_followBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
        
        if(_isSelected){
            [_suggestionBtn setBackgroundImage:IMAGE_FROM_BUNDLE(@"btn_downward_active_down") forState:UIControlStateNormal];
            [_suggestionBtn setBackgroundImage:IMAGE_FROM_BUNDLE(@"btn_downward_active_down") forState:UIControlStateSelected];
        }else{
            [_suggestionBtn setBackgroundImage:IMAGE_FROM_BUNDLE(@"btn_downward_active") forState:UIControlStateNormal];
            [_suggestionBtn setBackgroundImage:IMAGE_FROM_BUNDLE(@"btn_downward_active") forState:UIControlStateSelected];
        }
    }else{
        
        if(_user.followRequestTime==0){
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"]  resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"]  resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
            [_followBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
            [_followBtn setTitle:LOCALIZE(@"user_profile_follow") forState:UIControlStateNormal];
        }else{
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"]  resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
            [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"]  resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
            [_followBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
            [_followBtn setTitle:LOCALIZE(@"user_profile_request") forState:UIControlStateNormal];
        }
        
        if(_isSelected){
            [_suggestionBtn setBackgroundImage:IMAGE_FROM_BUNDLE(@"btn_downward_down") forState:UIControlStateNormal];
            [_suggestionBtn setBackgroundImage:IMAGE_FROM_BUNDLE(@"btn_downward_down") forState:UIControlStateSelected];
        }else{
            [_suggestionBtn setBackgroundImage:IMAGE_FROM_BUNDLE(@"btn_downward") forState:UIControlStateNormal];
            [_suggestionBtn setBackgroundImage:IMAGE_FROM_BUNDLE(@"btn_downward") forState:UIControlStateSelected];
        }
    }
    
}





@end
