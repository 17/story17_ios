//
//  UserListCell.h
//  Story17
//
//  Created by POPO on 2015/5/8.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserTitleView.h"
#import "UserObject.h"


@protocol UserListCellDelegate <NSObject>
-(void)didClickFollow:(NSString*)userID;
@end

@interface UserListCell : UICollectionViewCell
{
@private
    UserTitleView* _titleView;
    UIButton* _followBtn;
}

@property(nonatomic, strong)UserObject* user;
@property (weak) id<UserListCellDelegate> delegate;

+ (float)getDefaultHeight;

@end
