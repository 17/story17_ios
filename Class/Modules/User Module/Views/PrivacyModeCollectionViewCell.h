//
//  NoDataCollectionViewCell.h
//  Story17
//
//  Created by York on 2015/5/31.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacyModeCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) UIImageView* privacyImageView;

@end
