//
//  RegisterAccountViewController.m
//  tex
//
//  Created by POPO on 4/15/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "ChangePasswordViewController.h"


@implementation ChangePasswordViewController

#define rowHeight  50
#define horizontalMargin  10
#define alertViewY SCREEN_HEIGHT-KEYBOARD_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT

-(id)init
{
    self = [super init];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:LOCALIZE(@"Done") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction)];
    
    self.title = LOCALIZE(@"changePassword");
    
    oldPasswordRow = [[ImageTextField alloc]initWithFrame:CGRectMake(horizontalMargin, 10, SCREEN_WIDTH-2*horizontalMargin, rowHeight)];
    newPasswordRow = [[ImageTextField alloc]initWithFrame:CGRectMake(horizontalMargin, rowHeight+10, SCREEN_WIDTH-2*horizontalMargin, rowHeight)];
    confirmPasswordRow = [[ImageTextField alloc]initWithFrame:CGRectMake(horizontalMargin, 10+rowHeight*2, SCREEN_WIDTH-2*horizontalMargin, rowHeight)];
    
    oldPasswordRow.mainTextField.placeholder = LOCALIZE(@"enter_oldpassword");
    oldPasswordRow.mainTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    newPasswordRow.mainTextField.placeholder = LOCALIZE(@"enter_newpassword");
    newPasswordRow.mainTextField.secureTextEntry = YES;
    confirmPasswordRow.mainTextField.placeholder = LOCALIZE(@"enter_confirmpassword");
    confirmPasswordRow.mainTextField.secureTextEntry = YES;
    
    [oldPasswordRow.headImage setImage:[UIImage imageNamed:@"profile_password"]];
    [newPasswordRow.headImage setImage:[UIImage imageNamed:@"profile_password"]];
    [confirmPasswordRow.headImage setImage:[UIImage imageNamed:@"profile_password"]];

    oldPasswordRow.mainTextField.delegate = self;
    newPasswordRow.mainTextField.delegate = self;
    confirmPasswordRow.mainTextField.delegate = self;
    
    [self.view addSubview:oldPasswordRow];
    [self.view addSubview:newPasswordRow];
    [self.view addSubview:confirmPasswordRow];
    
    _alertView = [ThemeManager getAlertImageView];
    _alertLabel = [UILabel new];
    _alertLabel.textAlignment = NSTextAlignmentCenter;
    _alertLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [_alertLabel setTextColor:DARK_GRAY_COLOR];
    
    [_alertView addSubview:_alertLabel];
    [self.view addSubview:_alertView];
    _alertView.frame = CGRectMake(0, alertViewY, SCREEN_WIDTH, _alertView.frame.size.height);
    _alertLabel.frame = CGRectMake(0, 0, _alertView.frame.size.width,_alertView.frame.size.height);

}

-(void) viewWillAppear:(BOOL)animated
{
    [oldPasswordRow.mainTextField becomeFirstResponder];
//    [self reloadUI];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [oldPasswordRow.mainTextField resignFirstResponder];
    [newPasswordRow.mainTextField resignFirstResponder];
    [confirmPasswordRow.mainTextField resignFirstResponder];
}

//-(void) reloadUI
//{
//    [oldPasswordRow.mainTextField setText:GET_DEFAULT(OPENID)];
//}

-(void)doneAction
{
    NSString* nameRegex = @"[A-Za-z0-9]+";
    NSPredicate* nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    NSString* alertStr = @"";
    
    if (oldPasswordRow.mainTextField.text.length < PASSWORD_MIN){
        alertStr = LOCALIZE(@"alert_password_wrong");
    } else if (newPasswordRow.mainTextField.text.length <PASSWORD_MIN){
        alertStr = [NSString stringWithFormat:LOCALIZE(@"alert_password_too_short"),PASSWORD_MIN];
        
    } else if(![nameTest evaluateWithObject:oldPasswordRow.mainTextField.text]){
        alertStr = LOCALIZE(@"alert_password_illegal");
        
    } else if (![newPasswordRow.mainTextField.text isEqualToString:confirmPasswordRow.mainTextField.text]){
        alertStr =LOCALIZE(@"alert_password_confirm_fail");
    }
    
    if(![alertStr isEqualToString:@""]){
        [self showAlertViewMessage:alertStr];
    }else{
        
        [DIALOG_MANAGER showLoadingView];
        [API_MANAGER changePassword:[oldPasswordRow.mainTextField.text MD5] newPassword:[newPasswordRow.mainTextField.text MD5] withCompletion:^(BOOL success, NSString *message) {
            [DIALOG_MANAGER hideLoadingView];
            if([message isEqualToString:@"wrong password"]){
                DLog(@"%@",message);
            }else{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
        }];
    }
}


# pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==oldPasswordRow.mainTextField){
        if(oldPasswordRow.mainTextField.text.length>=USERNAME_MAX && string.length!=0){
            return NO;
        }
    }else if(textField==newPasswordRow.mainTextField){
        if(newPasswordRow.mainTextField.text.length>=PASSWORD_MAX && string.length!=0){
            return NO;
        }
    }else{
        if(confirmPasswordRow.mainTextField.text.length>=PASSWORD_MAX && string.length!=0){
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if(textField==oldPasswordRow.mainTextField){
        [newPasswordRow.mainTextField becomeFirstResponder];
    }else if(textField==newPasswordRow.mainTextField){
        [confirmPasswordRow.mainTextField becomeFirstResponder];
    }else{
        [self doneAction];
    }
    return YES;
    
}

-(void) showAlertViewMessage:(NSString*)message
{
    [_alertView.layer removeAllAnimations];
    if(_alertView.frame.origin.y != SCREEN_HEIGHT-KEYBOARD_HEIGHT){
        _alertView.frame = CGRectMake(0, alertViewY, SCREEN_WIDTH, _alertView.frame.size.height);
    }
    
    [_alertLabel setText:message];
    _alertLabel.center = CGPointMake(SCREEN_WIDTH/2, _alertView.frame.size.height/2);
    
    [UIView animateWithDuration:0.4 animations:^{
        _alertView.frame = CGRectMake(0, alertViewY-_alertView.frame.size.height, SCREEN_WIDTH, _alertView.frame.size.height);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.4 delay:1.6 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            _alertView.frame = CGRectMake(0, alertViewY, SCREEN_WIDTH, _alertView.frame.size.height);
        } completion:nil];
    }];
}


@end
