//
//  EditPostViewController.m
//  story17
//
//  Created by POPO Chen on 7/7/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "EditPostViewController.h"
#import "Constant.h"
#import "UIImageView+AFNetworking.h"


@implementation EditPostViewController

#define leftMargin 10
#define textHeight 50

-(id)init
{
    self = [super init];
    if(self){
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    self.title = LOCALIZE(@"edit_post");
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:LOCALIZE(@"Done") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction)];

    _scrollView = [UIScrollView new];
//    _scrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    _scrollView.showsVerticalScrollIndicator = YES;
    [self.view addSubview:_scrollView];
    
    int nextY = 0.0;
    _userTitleView = [UserTitleView new];
    _userTitleView.frame = CGRectMake(0, 0, SCREEN_WIDTH, [UserTitleView getDefaultHeight]);
    [_scrollView addSubview:_userTitleView];
    
    nextY += _userTitleView.frame.size.height;

    float imageHeight = SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-KEYBOARD_HEIGHT-nextY-textHeight-10;

    _postImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, imageHeight)];
    _postImageView.layer.masksToBounds = YES;
    _postImageView.contentMode = UIViewContentModeScaleAspectFill;
    [_scrollView addSubview:_postImageView];
//    [_postImageView setBackgroundColor:RED_COLOR];
    
    _videoImageView  = [UIImageView new];
    _videoImageView.hidden = YES;
    [_videoImageView setImage:[UIImage imageNamed:@"video"]];
    _videoImageView.frame = CGRectMake(SCREEN_WIDTH-45, nextY+5, 40, 40);
    nextY += _postImageView.frame.size.height+5;
    [_scrollView addSubview:_videoImageView];
    
    
    _captionTextView = [[UIPlaceholderTextView alloc]initWithFrame:CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, textHeight)];
    _captionTextView.placeholder = LOCALIZE(@"enter_caption");
    _captionTextView.font = SYSTEM_FONT_WITH_SIZE(16);
    [_captionTextView setBackgroundColor:[UIColor clearColor]];
    [_captionTextView setTextColor:DARK_GRAY_COLOR];
    _captionTextView.autocorrectionType = UITextAutocorrectionTypeNo;
    [_captionTextView setNeedsDisplay];
    _captionTextView.keyboardType = UIKeyboardTypeTwitter;

    [_scrollView addSubview:_captionTextView];

    nextY += _captionTextView.frame.size.height+5;
    
    _scrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH, nextY);
    [_scrollView setContentSize:CGSizeMake(SCREEN_WIDTH, nextY)];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [_postImageView setImageWithURL:S3_FILE_URL(_post.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_s")];

//    [_postImageView my_setImageWithURL:_post.picture placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_s") isThumbnail:NO];
    [_captionTextView setText:_post.caption];
    _userTitleView.title = _post.user.openID;
    _userTitleView.subTitle = [SINGLETON getElaspsedTimeString:_post.timestamp];
    _userTitleView.image = _post.user.picture;
    [_captionTextView becomeFirstResponder];
    
    if([_post.type isEqualToString:@"video"]){
        _videoImageView.hidden = NO;
    }
    
    
    [NOTIFICATION_CENTER addObserver:self selector:@selector(keyboardNotificationHandler:) name: UIKeyboardWillChangeFrameNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [NOTIFICATION_CENTER removeObserver:self];
}

-(void)reloadUI:(float)keyboardHeight
{
    _scrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH,(SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-keyboardHeight) );
    [_scrollView setContentOffset:CGPointMake(0,_scrollView.contentSize.height-_scrollView.frame.size.height) animated:YES];
}

-(void) setPost:(PostObject*)post
{
    _post = post;
}

-(void)keyboardNotificationHandler: (NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [self reloadUI:keyboardFrame.size.height];
}



-(void)doneAction
{
    
    DLog(@"PostObject:%@",_post);

    /* UPDATE post and delegate */
    if(_post==nil)
        return;
    
    _post.caption = _captionTextView.text;
    
    NSDictionary* dict = @{@"postID":_post.postID,@"picture":_post.picture,@"caption":_captionTextView.text,@"userTags":TO_JSON(@[]),@"hashTags":TO_JSON(@[]),@"taggedUsers":TO_JSON(@[]),@"locationID":@"",@"locationName":@"",@"latitude":@"0.0",@"longitude":@"0.0",@"canComment":@"1",@"reachability":@"",@"type":_post.type,@"video":@""};
    
    [DIALOG_MANAGER showLoadingView];
    [API_MANAGER updatePost:dict withCompletion:^(BOOL success, NSString *message, PostObject *post) {
        [DIALOG_MANAGER hideLoadingView];
        if(success){
            _updateCompleteCallback(post);
            [DIALOG_MANAGER showCompleteToast];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [DIALOG_MANAGER showNetworkFailToast];
        }
    }];
    
}

@end
