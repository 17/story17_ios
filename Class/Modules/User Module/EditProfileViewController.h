//
//  EditProfileViewController.h
//  Story17
//
//  Created by POPO on 2015/5/8.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageTextField.h"
#import "TitleLabelView.h"
#import "NationTableViewController.h"

@interface EditProfileViewController : UIViewController <UITextFieldDelegate,UITextViewDelegate,UIScrollViewDelegate,NationTableDelegate>

@property (nonatomic,strong) UIScrollView* mainScrollView;

@property (nonatomic,strong) ImageTextField* openIDTextField;
@property (nonatomic,strong) ImageTextField* nameTextField;
@property (nonatomic,strong) ImageTextField* webTextField;
@property (nonatomic,strong) ImageTextField* infoTextField;

@property (nonatomic,strong) TitleLabelView* privateLabel;
@property (nonatomic,strong) ImageTextField* genderTextField;
@property (nonatomic,strong) ImageTextField* ageTextField;
@property (nonatomic,strong) ImageTextField* mailTextField;
@property (nonatomic,strong) ImageTextField* phoneTextField;

@property (nonatomic,strong) UIImageView* alertView;
@property (nonatomic,strong) UILabel* alertLabel;

@property (nonatomic,strong) NSMutableDictionary* updateDict;

@property (nonatomic) float keyboardHeight;
@property (nonatomic) float lastCellY;
@property (nonatomic) BOOL isFirst;

@end
