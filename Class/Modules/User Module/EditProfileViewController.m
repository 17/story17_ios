//
//  EditProfileViewController.m
//  Story17
//
//  Created by POPO on 2015/5/8.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "EditProfileViewController.h"
#import "Constant.h"

@implementation EditProfileViewController

#define rowHeight 45
#define margin 5
#define horizontalMargin 10

#define keyboardHeight 200
#define MIDDLE_WINDOW_HEIGHT (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-KEYBOARD_HEIGHT-STATUS_BAR_HEIGHT-32)
#define MAIN_SCROLL_WINDOW_SIZE (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)

#define max_length 250

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:LOCALIZE(@"Done") style:UIBarButtonItemStyleDone target:self action:@selector(checkingDone:)];

    self.title = LOCALIZE(@"user_profile_edit");
    _updateDict = CREATE_MUTABLE_DICTIONARY;

    [_updateDict setObject:GET_DEFAULT(USER_OPEN_ID) forKey:@"openID"];
    [_updateDict setObject:GET_DEFAULT(USER_NAME) forKey:@"name"];
    [_updateDict setObject:GET_DEFAULT(WEBSITE) forKey:@"website"];
    [_updateDict setObject:GET_DEFAULT(COUNTRY_CODE) forKey:@"countryCode"];
    [_updateDict setObject:GET_DEFAULT(BIO) forKey:@"bio"];
    [_updateDict setObject:GET_DEFAULT(GENDER) forKey:@"gender"];
    [_updateDict setObject:[GET_DEFAULT(AGE) stringValue] forKey:@"age"];
    [_updateDict setObject:GET_DEFAULT(EMAIL) forKey:@"email"];
    [_updateDict setObject:GET_DEFAULT(PHONE_NUM) forKey:@"phoneNumber"];
    
    _isFirst = YES;
    _keyboardHeight = 0.0f;
    
    [self setup];
    [self reloadUI];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [NOTIFICATION_CENTER removeObserver:self];
}

-(void)checkingDone:(id)sender
{

    if([_openIDTextField.mainTextField.text isEqualToString:GET_DEFAULT(USER_OPEN_ID)]){
        [self doneAction];
    }else{
        
        /* Do not check modify time */
        NSString* nameRegex = @"[A-Za-z0-9_.]+";
        NSPredicate* nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
        NSString* alertStr = @"";
        
        if (_openIDTextField.mainTextField.text.length < USERNAME_MIN){
            alertStr =[NSString stringWithFormat:LOCALIZE(@"alert_openid_too_short"),USERNAME_MIN];
        } else if(![nameTest evaluateWithObject:_openIDTextField.mainTextField.text]){
            alertStr = LOCALIZE(@"alert_openid_illegal");
        }
        int days=0;
        
        days=((86400*90)-(CURRENT_TIMESTAMP-[GET_DEFAULT(OPENID_MODIFYTIME) intValue]))/86400;

        if([alertStr isEqualToString:@""]){
            if (CURRENT_TIMESTAMP-[GET_DEFAULT(OPENID_MODIFYTIME) intValue]<86400*90) {
                DLog(@"[GET_DEFAULT(OPENID_MODIFYTIME) intValue]=%d",[GET_DEFAULT(OPENID_MODIFYTIME) intValue]);

                [self showAlertViewMessage:[NSString stringWithFormat:LOCALIZE(@"OpenID_3month"),days]];
                return;
            }

            [self.view endEditing:YES];
            [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"OpenID_cant_edit") message:nil buttonText:LOCALIZE(@"OK") cancelable:YES withCompletion:^(BOOL okClicked) {
                if (!okClicked) {
                    return;
                }else{
                    [API_MANAGER checkOpenIDAvailable:_openIDTextField.mainTextField.text completion:^(BOOL success, NSString *message) {
                        if(success){
                            [DIALOG_MANAGER showLoadingView];
                            [API_MANAGER updateOpenID:_openIDTextField.mainTextField.text completion:^(BOOL success) {
                                if(success){
                                    [DEFAULTS setObject:_openIDTextField.mainTextField.text  forKey:USER_OPEN_ID]; // #openID
                                    [self reloadUI];
                                    [self doneAction];
                                }
                            }];
                        }else{
                            if([message isEqualToString:@"open_id_not_available"]){
                                [self showAlertViewMessage:LOCALIZE(@"alert_openid_used")];
                            }else{
                                [DIALOG_MANAGER showNetworkFailToast];
                            }
                        }
                    }];
                }
            }];
           

        }else{
            // show alert view too short
            [self showAlertViewMessage:alertStr];

        }
    }
}

-(void)doneAction
{
    
    [_updateDict setObject:_nameTextField.mainTextField.text forKey:@"name"];
    [_updateDict setObject:_webTextField.mainTextField.text forKey:@"website"];
    [_updateDict setObject:_infoTextField.growTextView.text forKey:@"bio"];
    [_updateDict setObject:_ageTextField.mainTextField.text forKey:@"age"];
    [_updateDict setObject:_mailTextField.mainTextField.text forKey:@"email"];
    [_updateDict setObject:_phoneTextField.mainTextField.text forKey:@"phoneNumber"];
    
    [DEFAULTS setObject:_updateDict[@"name"] forKey:USER_NAME];
    [DEFAULTS setObject:_updateDict[@"website"] forKey:WEBSITE];
    [DEFAULTS setObject:_updateDict[@"bio"] forKey:BIO];
    [DEFAULTS setObject:_updateDict[@"gender"] forKey:GENDER];
    [DEFAULTS setObject:_updateDict[@"email"] forKey:EMAIL];
    [DEFAULTS setObject:@([_updateDict[@"age"] intValue]) forKey:AGE];
//    [DEFAULTS setObject:_updateDict[@"phoneNumber"] forKey:PHONE_NUM];
//    [DEFAULTS setObject:_updateDict[@"countryCode"] forKey:COUNTRY_CODE];
    [_updateDict setObject:[NSString stringWithFormat:@"%@%@",_updateDict[@"countryCode"],_updateDict[@"phoneNumber"]] forKey:@"phoneWithCountryCode"];
    [DEFAULTS synchronize];

    [DIALOG_MANAGER showLoadingView];
    [API_MANAGER updateUserInfo:_updateDict fetchSelfInfo:YES completion:^(BOOL success) {
        [DIALOG_MANAGER hideLoadingView];
        if(success){
            [NOTIFICATION_CENTER postNotificationName:RELOAD_SELF_INFO object:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [DIALOG_MANAGER showNetworkFailToast];
        }
    }];
}


-(void)setup
{
    
    _mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, MAIN_SCROLL_WINDOW_SIZE)];
    _mainScrollView.scrollEnabled = YES;
    _mainScrollView.alwaysBounceVertical = YES;
    _mainScrollView.showsHorizontalScrollIndicator = NO;
    _mainScrollView.showsVerticalScrollIndicator = YES;
    
    _openIDTextField = [self getImageTextFiledWithY];
    _openIDTextField.mainTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [_openIDTextField.headImage setImage:IMAGE_FROM_BUNDLE(@"profile_id")];

    
    _nameTextField = [self getImageTextFiledWithY];
    [_nameTextField.headImage setImage:IMAGE_FROM_BUNDLE(@"profile_username")];

    _webTextField = [self getImageTextFiledWithY];
    [_webTextField.headImage setImage:IMAGE_FROM_BUNDLE(@"profile_web")];
    _webTextField.mainTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;

    _infoTextField = [self getImageTextFiledWithY];
    [_infoTextField.headImage setImage:IMAGE_FROM_BUNDLE(@"profile_info")];
    [_infoTextField changeToGroupTextMode];
    _infoTextField.growTextView.delegate  =self;
    
    _privateLabel = [TitleLabelView new];
    [_privateLabel.titleLabel setText:LOCALIZE(@"Account")];
    
    
    _genderTextField = [self getImageTextFiledWithY];
    _genderTextField.mainTextField.enabled = NO;
    [_genderTextField.headImage setImage:IMAGE_FROM_BUNDLE(@"profile_gender")];
    [_genderTextField addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showPickerAction:)]];
    _genderTextField.userInteractionEnabled = YES;
    
    _ageTextField = [self getImageTextFiledWithY];
    _ageTextField.mainTextField.enabled = NO;
    [_ageTextField.headImage setImage:IMAGE_FROM_BUNDLE(@"profile_birthday")];
    [_ageTextField addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showPickerAction:)]];
    _ageTextField.userInteractionEnabled = YES;
    
    _mailTextField = [self getImageTextFiledWithY];
    [_mailTextField.headImage setImage:IMAGE_FROM_BUNDLE(@"profile_mail")];
    _mailTextField.mainTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    _phoneTextField = [self getImageTextFiledWithY];
    [_phoneTextField.headImage setImage:IMAGE_FROM_BUNDLE(@"profile_phone")];
    _phoneTextField.mainTextField.keyboardType = UIKeyboardTypeNumberPad;
    [_phoneTextField changeToPhoneMode];
    [_phoneTextField.countryCodeLabel setText:[NSString stringWithFormat:@"+%@",GET_DEFAULT(COUNTRY_CODE)]];
    [_phoneTextField.countryCodeLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickNation:)]];
    _phoneTextField.countryCodeLabel.userInteractionEnabled = YES;

    [_openIDTextField.mainTextField setText:_updateDict[@"openID"]];
    [_nameTextField.mainTextField setText:_updateDict[@"name"]];
    [_webTextField.mainTextField setText:_updateDict[@"website"]];
    [_infoTextField.growTextView setText:_updateDict[@"bio"]];
    [_infoTextField.countLabel setText:[NSString stringWithFormat:@"%lu",max_length-_infoTextField.growTextView.text.length]];

    [_genderTextField.mainTextField setText:LOCALIZE(_updateDict[@"gender"])];
    
    [_mailTextField.mainTextField setText:_updateDict[@"email"]];
    [_phoneTextField.mainTextField setText:_updateDict[@"phoneNumber"]];
    
    [_mainScrollView addSubview:_privateLabel];

    [_mainScrollView addSubview:_openIDTextField];
    [_mainScrollView addSubview:_nameTextField];
    [_mainScrollView addSubview:_webTextField];
    [_mainScrollView addSubview:_infoTextField];
    [_mainScrollView addSubview:_genderTextField];
    [_mainScrollView addSubview:_ageTextField];
    [_mainScrollView addSubview:_mailTextField];
//    [_mainScrollView addSubview:_phoneTextField];
    
    [_mainScrollView setBackgroundColor:LIGHT_BG_COLOR];
    
    [self.view addSubview:_mainScrollView];
    
    
    _alertView = [ThemeManager getAlertImageView];
    _alertLabel = [UILabel new];
    _alertLabel.textAlignment = NSTextAlignmentCenter;
    _alertLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [_alertLabel setTextColor:DARK_GRAY_COLOR];
    
    [_alertView addSubview:_alertLabel];
    [self.view addSubview:_alertView];
    _alertView.frame = CGRectMake(0, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT, SCREEN_WIDTH, _alertView.frame.size.height);
    _alertLabel.frame = CGRectMake(0, 0, _alertView.frame.size.width,_alertView.frame.size.height);
    
}

-(void)reloadUI
{
    /* reload data && position animation */
    
    [_genderTextField.mainTextField setText:LOCALIZE(_updateDict[@"gender"])];
//    [_ageTextField.mainTextField setText:_updateDict[@"age"]];
    
    int nextY = 20;
    
    _openIDTextField.mainTextField.placeholder = LOCALIZE(@"REGISTER_ENTER_USERNAME");
    _openIDTextField.frame = CGRectMake(horizontalMargin, nextY, SCREEN_WIDTH-2*horizontalMargin, rowHeight);
    nextY += rowHeight+margin;

    
    _nameTextField.mainTextField.placeholder = LOCALIZE(@"enter_name");
    _nameTextField.frame = CGRectMake(horizontalMargin, nextY, SCREEN_WIDTH-2*horizontalMargin, rowHeight);
    nextY += rowHeight+margin;
    
    _webTextField.mainTextField.placeholder = LOCALIZE(@"enter_website");
    _webTextField.frame = CGRectMake(horizontalMargin, nextY, SCREEN_WIDTH-2*horizontalMargin, rowHeight);
    nextY += rowHeight+margin;

    CGSize stringSize = [_infoTextField.growTextView.text getSizeWithConstrainSize:CGSizeMake(_infoTextField.growTextView.frame.size.width,500) withFont:_infoTextField.growTextView.font];

    float contentSizeHeight = _isFirst?stringSize.height+20:_infoTextField.growTextView.contentSize.height;
    int numLines = MAX(1, (contentSizeHeight - _infoTextField.growTextView.textContainerInset.top - _infoTextField.growTextView.textContainerInset.bottom) / _infoTextField.growTextView.font.lineHeight);
   
    
    [_infoTextField.countLabel setText:[NSString stringWithFormat:@"%lu",max_length-_infoTextField.growTextView.text.length]];
    _infoTextField.growTextView.placeholder = LOCALIZE(@"enter_bio");

    _infoTextField.growTextView.frame = CGRectMake(_infoTextField.growTextView.frame.origin.x, _infoTextField.growTextView.frame.origin.y, _infoTextField.growTextView.frame.size.width, rowHeight+_infoTextField.growTextView.font.lineHeight*(numLines-1));
    _infoTextField.frame = CGRectMake(horizontalMargin, nextY, SCREEN_WIDTH-2*horizontalMargin, _infoTextField.growTextView.frame.size.height+10);
    _infoTextField.countLabel.frame = CGRectMake(SCREEN_WIDTH-80, _infoTextField.frame.size.height-30, _infoTextField.countLabel.frame.size.width, _infoTextField.countLabel.frame.size.height);
    _infoTextField.bgImage.frame = CGRectMake(0, 0, SCREEN_WIDTH-20, _infoTextField.growTextView.frame.size.height);
    _infoTextField.countLabel.textAlignment = NSTextAlignmentCenter;
    nextY += _infoTextField.frame.size.height;
    

    _privateLabel.frame = CGRectMake(horizontalMargin, nextY, SCREEN_WIDTH-2*horizontalMargin, 30);
    _privateLabel.titleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    nextY += _privateLabel.frame.size.height;
    
    _genderTextField.mainTextField.placeholder = LOCALIZE(@"enter_gender");
    _genderTextField.frame = CGRectMake(horizontalMargin, nextY, SCREEN_WIDTH-2*horizontalMargin, rowHeight);
    nextY += rowHeight+margin;
    
    
    _ageTextField.mainTextField.placeholder = LOCALIZE(@"enter_age");
    if(![_updateDict[@"age"] isEqualToString:@"0"]){
        [_ageTextField.mainTextField setText:_updateDict[@"age"]];
    }else{
        [_ageTextField.mainTextField setText:@""];
    }
    
    _ageTextField.frame = CGRectMake(horizontalMargin, nextY, SCREEN_WIDTH-2*horizontalMargin, rowHeight);
    nextY += rowHeight+margin;
    
    _mailTextField.mainTextField.placeholder = LOCALIZE(@"enter_email");
    _mailTextField.frame = CGRectMake(horizontalMargin, nextY, SCREEN_WIDTH-2*horizontalMargin, rowHeight);
    nextY += rowHeight+margin;
    
    _phoneTextField.mainTextField.placeholder = LOCALIZE(@"enter_phoneNumber");
    _phoneTextField.frame = CGRectMake(horizontalMargin, nextY, SCREEN_WIDTH-2*horizontalMargin, rowHeight);
    
    nextY += rowHeight+margin;
    
    _lastCellY = nextY;
    
    [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, _lastCellY)];
    
    _isFirst = NO;

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(keyboardNotificationHandler:) name: UIKeyboardDidChangeFrameNotification object:nil];

    if([GET_DEFAULT(COUNTRY_CODE) isEqualToString:@""]){
        
        [SINGLETON detectIpGeoLocation:^(BOOL success, NSDictionary *jsonDict) {
            if(success){
                NSString* countryCode = jsonDict[@"countryCode"];
                NSString* phoneCode = [SINGLETON countryCode2PhoneCode][countryCode];
                
                // in 17app, countryCode this means phoneCode
                [_updateDict setObject:phoneCode forKey:@"countryCode"];
                [_phoneTextField.countryCodeLabel setText:[NSString stringWithFormat:@"+%@",phoneCode]];
            }
        }];
        
//        [API_MANAGER getCountry:^(BOOL success, NSString *country) {
//            if(success){
//                NSString* countryCode = [SINGLETON countryName2countryCode][country];
//                [_updateDict setObject:countryCode forKey:@"countryCode"];
//                [_phoneTextField.countryCodeLabel setText:[NSString stringWithFormat:@"+%@",countryCode]];
//            }
//        }];
    }
}


-(ImageTextField*)getImageTextFiledWithY
{
    ImageTextField* imageTextField = [[ImageTextField alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, rowHeight)];
    imageTextField.mainTextField.delegate = self;
    return imageTextField;
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView;
{
    [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, _lastCellY+KEYBOARD_HEIGHT+NAVI_BAR_HEIGHT)];
}

- (void)textViewDidChange:(UITextView *)textView
{
    [self reloadUI];
    [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, _lastCellY+KEYBOARD_HEIGHT+NAVI_BAR_HEIGHT)];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@""])
        return YES;
    
    if(textView.text.length>=max_length)
        return NO;
    
    return YES;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, _lastCellY+KEYBOARD_HEIGHT+NAVI_BAR_HEIGHT)];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIView* superView = textField.superview;
    
    if([superView isKindOfClass:[ImageTextField class]]){
        [self scrollViewToY:superView.frame.origin.y];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //nickname 20 characters
    if(textField==_nameTextField.mainTextField){
        if(textField.text.length>=USERNAME_MAX && string.length!=0){
            return NO;
        }
    }else if(textField==_openIDTextField.mainTextField){
        if(textField.text.length>=USERNAME_MAX&&range.length==0){
            return NO;
        }
    }
    return YES;
}

#pragma mark - animation
-(void)showPickerAction:(id)sender
{
    [self.view endEditing:YES];
    
    NSMutableArray* dataArray = CREATE_MUTABLE_ARRAY;
    
    UITapGestureRecognizer* tapGesture = sender;
    int offset = 0;
    if([tapGesture.view isEqual:_genderTextField]){
        
        dataArray = [@[LOCALIZE(@"male"),LOCALIZE(@"female")] mutableCopy];
        int index = (int)[dataArray indexOfObject:LOCALIZE(_updateDict[@"gender"])];
        offset = (index>0 && index<[dataArray count])?index:0;
    }else{
        for(int i=AGE_MIN;i<=AGE_MAX;i++){
            [dataArray addObject:INT_TO_STRING(i)];
        }
        int index = (int)[dataArray indexOfObject:_updateDict[@"age"]];
        offset = (index>0 && index<[dataArray count])?index:0;
    }
    
    if([tapGesture.view isKindOfClass:[ImageTextField class]]){
        [self scrollViewToY:tapGesture.view.frame.origin.y];
    }
    
    [DIALOG_MANAGER showPickerDialogWithItems:dataArray offset:offset withCompletion:^(BOOL done, int selectedIndex) {
        
        _alertView.frame = CGRectMake(0, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT, SCREEN_WIDTH, _alertView.frame.size.height);

        if(done){
            if([tapGesture.view isEqual:_genderTextField]){
                if(selectedIndex==0){
                    [_updateDict setObject:@"male" forKey:@"gender"];
                }else{
                    [_updateDict setObject:@"female" forKey:@"gender"];
                }
            }else{
                [_updateDict setObject:[dataArray objectAtIndex:selectedIndex] forKey:@"age"];
            }
            [self reloadUI];
        }
        [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, _lastCellY)];
        [_mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        
    }];
}


-(void)scrollViewToY:(float)scrollY
{
    float ylocation = scrollY-MIDDLE_WINDOW_HEIGHT/3;

    CGPoint superViewPoint = CGPointMake(0,0);
    if(ylocation>0){
        if(ylocation+MIDDLE_WINDOW_HEIGHT > _lastCellY){
            superViewPoint = CGPointMake(0, _lastCellY-MIDDLE_WINDOW_HEIGHT);
        }else{
            superViewPoint = CGPointMake(0, ylocation);
        }
    }
    [_mainScrollView setContentOffset:superViewPoint animated:YES];
}


-(void)didClickNation:(id)sender
{
    NationTableViewController* nVC = [NationTableViewController new];
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:nVC];
    nVC.delegate = self;
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}



-(NSString*)validPhoneNumer:(NSString*)inputPhoneNumber
{
    NSString* validPhone = [inputPhoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@"(" withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@")" withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@"+" withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    NSString* firstIndex = [validPhone substringToIndex:1];
    if([firstIndex isEqualToString:@"0"]){
        validPhone = [validPhone substringWithRange:NSMakeRange(1, [validPhone length]-1)];
    }
    return validPhone;
}

#pragma mark - NationTableDelegate
-(void) didSelectNations:(NSString*)nation code:(NSString*)code;
{
    [_phoneTextField.countryCodeLabel setText:[NSString stringWithFormat:@"+%@",code]];
    _updateDict[@"countryCode"] = code;
}

-(void) showAlertViewMessage:(NSString*)message
{
    [_alertView.layer removeAllAnimations];
    
    if(_alertView.frame.origin.y != SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-keyboardHeight){
        _alertView.frame = CGRectMake(0, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-_keyboardHeight, SCREEN_WIDTH,  _alertView.frame.size.height);
    }
    
    [_alertLabel setText:message];
    _alertLabel.center = CGPointMake(SCREEN_WIDTH/2, _alertView.frame.size.height/2);
    
    [UIView animateWithDuration:0.4 animations:^{
        _alertView.frame = CGRectMake(0, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-_keyboardHeight- _alertView.frame.size.height, SCREEN_WIDTH,  _alertView.frame.size.height);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.4 delay:1.6 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            _alertView.frame = CGRectMake(0, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-_keyboardHeight, SCREEN_WIDTH,  _alertView.frame.size.height);
        } completion:nil];
    }];
}

-(void)keyboardNotificationHandler:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    _keyboardHeight = keyboardFrame.size.height;
    _alertView.frame = CGRectMake(0, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-_keyboardHeight, SCREEN_WIDTH,  _alertView.frame.size.height);

}

@end
