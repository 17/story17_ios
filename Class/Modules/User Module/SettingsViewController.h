//
//  SettingsViewController.h
//  Story17
//
//  Created by York on 2015/5/5.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArrowSwitchLabelView.h"
#import "TitleLabelView.h"

@interface SettingsViewController : UIViewController<MFMailComposeViewControllerDelegate>

@end
