#import "Constant.h"

@interface UserObject : NSObject

/* Basic Info */
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *openID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *picture;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *coverPhoto;
@property (nonatomic, strong) NSString *website;
@property (nonatomic, strong) NSString *bio;
@property (nonatomic, strong) NSString *phoneNumber;

@property (nonatomic) int32_t age;
@property (nonatomic) int32_t adsOn;
@property (nonatomic) int32_t isVerified;
@property (nonatomic) int32_t crossGreatWall;
@property (nonatomic) int32_t isCelebrity;

@property (nonatomic) int32_t ishiddenFromLiveStream;

@property (nonatomic) int32_t isChoice;
@property (nonatomic) int32_t followerCount;
@property (nonatomic) int32_t followingCount;
@property (nonatomic) int32_t postCount;
@property (nonatomic) int32_t repostCount;
@property (nonatomic) int32_t likePostCount;
@property (nonatomic) int32_t likeCount;  // like count for certain post ( Count after migration )
@property (nonatomic) int32_t receivedLikeCount;  // like count for certain post ( Count before migration )
@property (nonatomic) int32_t isInternational;


@property (nonatomic) int32_t enterTime;   // livestream enter time
@property (nonatomic) int32_t followTime;
@property (nonatomic) int32_t blockTime;
@property (nonatomic) int32_t lastLogin; 

@property (nonatomic) int32_t openIDModifyTime;

@property (nonatomic) int32_t likeBadge;
@property (nonatomic) int32_t commentBadge;

@property (nonatomic) int32_t isFollowing;
@property (nonatomic) int32_t isBlocked;
@property (nonatomic) int32_t isLivestreamming;
@property (nonatomic, strong) NSString *livestreamingID;

@property (nonatomic, strong) NSString *privacyMode;
@property (nonatomic) int32_t followPrivacyMode;

@property (nonatomic) int32_t followRequestTime;

+(UserObject *)getUserWithDict:(NSDictionary *)userInfoDict;

-(BOOL)isPrivacyMode;


@end
