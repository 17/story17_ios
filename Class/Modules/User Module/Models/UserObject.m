 #import "UserObject.h"

@implementation UserObject


+(UserObject *)getUserWithDict:(NSDictionary *)userInfoDict
{
    
    if (userInfoDict[@"userID"] == nil || [userInfoDict[@"userID"] isKindOfClass:[NSNull class]] || [userInfoDict[@"userID"] isEqualToString:@""]) {
        return nil;
    }

    
    UserObject* user = [[UserObject alloc] init];
    
    user.userID = userInfoDict[@"userID"];
    user.openID = userInfoDict[@"openID"];
    user.name = userInfoDict[@"name"];
    user.picture = userInfoDict[@"picture"];
    user.coverPhoto = userInfoDict[@"coverPhoto"];
    user.phoneNumber = userInfoDict[@"phoneNumber"];
    user.website = userInfoDict[@"website"];
    user.gender = userInfoDict[@"gender"];
    user.bio= userInfoDict[@"bio"];

    user.isCelebrity = [userInfoDict[@"isCelebrity"] intValue];

    user.isInternational = [userInfoDict[@"isInternational"] intValue];
    user.age = [userInfoDict[@"age"] intValue];
    user.isVerified = [userInfoDict[@"isVerified"] intValue];
    user.crossGreatWall = [userInfoDict[@"crossGreatWall"] intValue];
    
    user.adsOn = [userInfoDict[@"adsOn"] intValue];
    user.isChoice = [userInfoDict[@"isChoice"] intValue];
    user.followerCount = [userInfoDict[@"followerCount"] intValue];
    user.followingCount = [userInfoDict[@"followingCount"] intValue];
    user.postCount = [userInfoDict[@"postCount"] intValue];
    user.repostCount = [userInfoDict[@"repostCount"] intValue];
    user.likePostCount = [userInfoDict[@"likePostCount"] intValue];
    user.privacyMode = userInfoDict[@"privacyMode"];
    user.receivedLikeCount = [userInfoDict[@"receivedLikeCount"] intValue];
    user.followPrivacyMode = [userInfoDict[@"followPrivacyMode"] intValue];
    
    user.isLivestreamming = 0;
    user.livestreamingID = @"";
    user.ishiddenFromLiveStream = 0;
    
    if(userInfoDict[@"isFollowing"]!=nil && [userInfoDict[@"isFollowing"] isKindOfClass:[NSNull class]]==NO) {
        user.isFollowing = [userInfoDict[@"isFollowing"] intValue];
    }
    
    if(userInfoDict[@"isBlocked"]!=nil && [userInfoDict[@"isBlocked"] isKindOfClass:[NSNull class]]==NO) {
        user.isBlocked = [userInfoDict[@"isBlocked"] intValue];
    }
    
    if(userInfoDict[@"followTime"]!=nil && [userInfoDict[@"followTime"] isKindOfClass:[NSNull class]]==NO) {
        user.followTime = [userInfoDict[@"followTime"] intValue];
    }
    
    if(userInfoDict[@"blockTime"]!=nil && [userInfoDict[@"blockTime"] isKindOfClass:[NSNull class]]==NO) {
        user.blockTime = [userInfoDict[@"blockTime"] intValue];
    }
   
    if(userInfoDict[@"enterTime"]!=nil && [userInfoDict[@"enterTime"] isKindOfClass:[NSNull class]]==NO) {
        user.enterTime = [userInfoDict[@"enterTime"] intValue];
    }
    
    if(userInfoDict[@"likeBadge"]!=nil && [userInfoDict[@"likeBadge"] isKindOfClass:[NSNull class]]==NO) {
        user.likeBadge = [userInfoDict[@"likeBadge"] intValue];
    }

    if(userInfoDict[@"commentBadge"]!=nil && [userInfoDict[@"commentBadge"] isKindOfClass:[NSNull class]]==NO) {
        user.commentBadge = [userInfoDict[@"commentBadge"] intValue];
    }

    if(userInfoDict[@"likeCount"]!=nil && [userInfoDict[@"likeCount"] isKindOfClass:[NSNull class]]==NO) {
        user.likeCount = [userInfoDict[@"likeCount"] intValue];
    }
    
    
    if(userInfoDict[@"openIDModifyTime"]!=nil && [userInfoDict[@"openIDModifyTime"] isKindOfClass:[NSNull class]]==NO) {
        user.openIDModifyTime = [userInfoDict[@"openIDModifyTime"] intValue];
    }
    
    if(userInfoDict[@"lastLogin"]!=nil && [userInfoDict[@"lastLogin"] isKindOfClass:[NSNull class]]==NO) {
        user.lastLogin = [userInfoDict[@"lastLogin"] intValue];
    }

    if(userInfoDict[@"followRequestTime"]!=nil && [userInfoDict[@"followRequestTime"] isKindOfClass:[NSNull class]]==NO) {
        user.followRequestTime = [userInfoDict[@"followRequestTime"] intValue];
    }
    
    return user;
}

-(BOOL)isPrivacyMode
{
    if([_privacyMode isEqualToString:@"private"]){
        return YES;
    }else{
        return NO;
    }
}


@end
