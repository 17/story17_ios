//
//  UserProfileViewController.h
//  story17
//
//  Created by POPO Chen on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserObject.h"
#import "UserProfileTitleCell.h"
#import "UserProfileTabCell.h"
#import "UserListCell.h"
#import "PostListCell.h"
#import "PostObject.h"

typedef enum {
    USER_SUGGESTIONING=0,
    USER_NORMAL
} USERP_RROFILE_UI_MODE;


@interface UserProfileViewController: UIViewController <UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UserProfileTabCellDelegate,PostCellDelegate>
{
@private
    bool _isLocalUser;
    bool _isReLoad;
    
    CGSize _imgSize;
    NSMutableArray* _posts;
    NSMutableArray* _likeposts;
    UserProfileTabType _type;
    
    NSMutableArray* _suggestions;
    
    int _section;
}

@property USERP_RROFILE_UI_MODE profileMode;

@property(nonatomic, strong)UserObject* user;
@property(nonatomic, strong)UIView* bgTapView;
@property(nonatomic, strong)UICollectionView* collectionView;
@property (nonatomic,strong) UIButton* backButton;

@property(nonatomic)BOOL dismissNavbar;
@property(nonatomic)BOOL isFetching;
@property(nonatomic)BOOL noMoreData;
@property(nonatomic)BOOL isFetchingSuggestion;
@property (nonatomic, strong) UIImageView* badgeImageView;

@property (nonatomic) CGFloat lastContentOffset;
@property int enterPageTimeStamp;
@property BOOL followAction;
@property int gridCount;
@property int listCount;
@property int favorCount;

-(void)getUserWithOpenID:(NSString*)openID;
-(void)getUserWithUserID:(NSString*)userID;

@end
