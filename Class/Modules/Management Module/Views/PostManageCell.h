//
//  LiveManageCell.h
//  Story17
//
//  Created by POPO on 9/30/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveStreamObject.h"

@class PostManageCell;

typedef enum {
    REVIEW_POST_CELL_MODE=0,
    REPORT_POST_CELL_MODE,
    REVIEW_PROFILE_CELL_MODE
} POST_CELL_MANAGER_MODE;

@protocol PostManageCellDelegate <NSObject>
-(void)removeItem:(PostManageCell*)cell;
@end

@interface PostManageCell : UICollectionViewCell

@property POST_CELL_MANAGER_MODE reviewMode;
@property (nonatomic, weak) PostObject* post;
@property (nonatomic, weak) UserObject* user;
@property (nonatomic, weak) UINavigationController* navCtrl;
@property (nonatomic, weak) id<PostManageCellDelegate> delegate;

+ (float)getHeightFromLivestream;

@end