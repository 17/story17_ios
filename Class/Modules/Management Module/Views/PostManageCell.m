//
//  PostManageCell.m
//  Story17
//
//  Created by POPO on 9/30/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "PostManageCell.h"
#import "Constant.h"
#import "PostCellTitle.h"
#import "UserProfileViewController.h"
#import "LiveStreamViewController.h"

@interface PostManageCell()

#define CONTROL_VIEW_EDGE_MARGIN 10
#define CAPTION_LABEL_HEIGHT 20

@property (nonatomic, strong) PostCellTitle* postTitleView;
@property (nonatomic,strong) UIImageView* postImageView;
@property (nonatomic, strong) UILabel* captionLabel;
@property (nonatomic, strong) UIView* controlView;
@property (nonatomic, strong) UILabel* userControlLabel;
@property (nonatomic, strong) UIButton* userGhostButton;
@property (nonatomic, strong) UIButton* userFreezeButton;
@property (nonatomic, strong) UIButton* userVerifyButton;
@property (nonatomic, strong) UILabel* livestreamControlLabel;
@property (nonatomic, strong) UIButton* livestreamHideButton;
@property (nonatomic, strong) UIButton* livestreamPassButton;

@property (nonatomic, strong) UIActivityIndicatorView* loadingView;

@end

@implementation PostManageCell

+ (float)getHeightFromLivestream
{
    return [PostCellTitle getDefaultHeight]*2.8+CAPTION_LABEL_HEIGHT+SCREEN_WIDTH;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        CGFloat width4Col = (frame.size.width-5*CONTROL_VIEW_EDGE_MARGIN)/4;
        CGFloat width3Col = (frame.size.width-width4Col-4*CONTROL_VIEW_EDGE_MARGIN)/2;
        
        _postTitleView = [[PostCellTitle alloc] init];
        [_postTitleView maskUserImage:NO];
        CGRect rect = CGRectMake(0, 0, frame.size.width, [PostCellTitle getDefaultHeight]);
        _postTitleView.frame = rect;
        _postTitleView.viewsLabel.hidden = NO;
        _postTitleView.moneyLabel.hidden = NO;
        
        self.postImageView = [[UIImageView alloc] init];
//        self.postImageView.userInteractionEnabled = YES;
//        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickPostImage:)];
//        [self.postImageView addGestureRecognizer:tapGesture];
        
        [self.contentView addSubview:self.postImageView];

        rect.origin.x = 0;
        rect.origin.y =  60;
        rect.size.width = frame.size.width;
        rect.size.height = frame.size.width;
        [self.postImageView setFrame:rect];
        
        
        UITapGestureRecognizer* postTitleGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickTitle)];
        [_postTitleView addGestureRecognizer:postTitleGesture];
        [_postTitleView setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer* userImgGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickTitle)];
        [_postTitleView.userImage addGestureRecognizer:userImgGesture];
        [_postTitleView.userImage setUserInteractionEnabled:YES];
        [self.contentView addSubview:_postTitleView];
        
        _captionLabel = [[UILabel alloc] init];
        _captionLabel.frame = CGRectMake(_postTitleView.userImage.frame.origin.x+_postTitleView.userImage.frame.size.width+10, CAPTION_LABEL_HEIGHT+10, SCREEN_WIDTH, CAPTION_LABEL_HEIGHT);
        [_captionLabel setTextAlignment:NSTextAlignmentLeft];
        _captionLabel.font = SYSTEM_FONT_WITH_SIZE(16);
        [_captionLabel setTextColor:GRAY_COLOR];
        [self.contentView addSubview:_captionLabel];
        
        _controlView = [[UIView alloc] init];
        rect.origin.y = [PostCellTitle getDefaultHeight]+CAPTION_LABEL_HEIGHT+SCREEN_WIDTH;
        rect.size.width = frame.size.width;
        rect.size.height = [PostCellTitle getDefaultHeight]*1.8;
        _controlView.frame = rect;
        _controlView.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:239.0/255.0 alpha:1.0];
        [self.contentView addSubview:_controlView];
        
        rect.origin.x = CONTROL_VIEW_EDGE_MARGIN;
        rect.origin.y = CONTROL_VIEW_EDGE_MARGIN;
        rect.size.width = width4Col;
        rect.size.height = (rect.size.height-4*CONTROL_VIEW_EDGE_MARGIN)/2;
        _userControlLabel = [[UILabel alloc] init];
        _userControlLabel.frame = rect;
        [_userControlLabel setTextAlignment:NSTextAlignmentCenter];
        _userControlLabel.font = SYSTEM_FONT_WITH_SIZE(16);
        [_userControlLabel setTextColor:GRAY_COLOR];
        [_userControlLabel setText:@"認證個人"];
        [_controlView addSubview:_userControlLabel];
        
        //        rect.origin.x = width4Col+2*CONTROL_VIEW_EDGE_MARGIN;
        //        _userGhostButton = [UIButton buttonWithType:UIButtonTypeCustom];
        //        _userGhostButton.frame = rect;
        //        _userGhostButton.titleLabel.font = SYSTEM_FONT_WITH_SIZE(16);
        //        [_userGhostButton setTitle:@"Ghost" forState:UIControlStateNormal];
        //        [_userGhostButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
        //        [_userGhostButton setBackgroundColor:BLACK_COLOR];
        //        [_userGhostButton addTarget:self action:@selector(didClickObj:) forControlEvents:UIControlEventTouchUpInside];
        //        [_controlView addSubview:_userGhostButton];
        
        //        rect.origin.x = 2*width4Col+3*CONTROL_VIEW_EDGE_MARGIN;
        
        rect.origin.x = width4Col+2*CONTROL_VIEW_EDGE_MARGIN;
        rect.size.width = width3Col;
        _userFreezeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _userFreezeButton.frame = rect;
        _userFreezeButton.titleLabel.font = SYSTEM_FONT_WITH_SIZE(16);
        [_userFreezeButton setTitle:@"Freeze" forState:UIControlStateNormal];
        [_userFreezeButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
        [_userFreezeButton setBackgroundColor:[UIColor colorWithRed:225.0/255.0 green:35.0/255.0 blue:40.0/255.0 alpha:1.0]];
        [_userFreezeButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        [_controlView addSubview:_userFreezeButton];
        
        rect.origin.x = 3*width4Col+4*CONTROL_VIEW_EDGE_MARGIN;
        rect.origin.x = width4Col+width3Col+3*CONTROL_VIEW_EDGE_MARGIN;
        _userVerifyButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _userVerifyButton.frame = rect;
        _userVerifyButton.titleLabel.font = SYSTEM_FONT_WITH_SIZE(16);
        [_userVerifyButton setTitle:@"Verified" forState:UIControlStateNormal];
        [_userVerifyButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
        [_userVerifyButton setBackgroundColor:[UIColor colorWithRed:115.0/255.0 green:200.0/255.0 blue:155.0/255.0 alpha:1.0]];
        [_userVerifyButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        [_controlView addSubview:_userVerifyButton];
        
        rect.origin.x = CONTROL_VIEW_EDGE_MARGIN;
        rect.origin.y = rect.size.height+3*CONTROL_VIEW_EDGE_MARGIN;
        rect.size.width = width4Col;
        _livestreamControlLabel = [[UILabel alloc] init];
        _livestreamControlLabel.frame = rect;
        [_livestreamControlLabel setTextAlignment:NSTextAlignmentCenter];
        _livestreamControlLabel.font = SYSTEM_FONT_WITH_SIZE(16);
        [_livestreamControlLabel setTextColor:GRAY_COLOR];
        [_livestreamControlLabel setText:@"此張照片"];
        [_controlView addSubview:_livestreamControlLabel];
        
        rect.origin.x = width4Col+2*CONTROL_VIEW_EDGE_MARGIN;
        rect.size.width = width3Col;
        _livestreamHideButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _livestreamHideButton.frame = rect;
        _livestreamHideButton.titleLabel.font = SYSTEM_FONT_WITH_SIZE(16);
        [_livestreamHideButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
        [_livestreamHideButton setBackgroundColor:BLACK_COLOR];
        [_livestreamHideButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        [_livestreamHideButton setTitle:@"Hide" forState:UIControlStateNormal];

        [_controlView addSubview:_livestreamHideButton];

        rect.origin.x = width4Col+width3Col+3*CONTROL_VIEW_EDGE_MARGIN;
        _livestreamPassButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _livestreamPassButton.frame = rect;
        _livestreamPassButton.titleLabel.font = SYSTEM_FONT_WITH_SIZE(16);
        [_livestreamPassButton setTitle:@"Pass" forState:UIControlStateNormal];
        [_livestreamPassButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
        [_livestreamPassButton setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:180.0/255.0 blue:200.0/255.0 alpha:1.0]];
        [_livestreamPassButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        [_controlView addSubview:_livestreamPassButton];

    }
    
    return self;
}

-(void)setUser:(UserObject *)user
{
    _user = user;
    
    [_livestreamHideButton setTitle:@"Remove" forState:UIControlStateNormal];
    [_livestreamPassButton setTitle:@"Pass" forState:UIControlStateNormal];

    self.postTitleView.title = _user.openID;
    
    self.postTitleView.image = _user.picture;
    
    //    self.postTitleView.views = _post.totalViewTime;
    [_postTitleView.viewsLabel setText:[NSString stringWithFormat:@"%d %@", _user.followerCount, LOCALIZE(@"user_profile_followers")]];
    //    [_postTitleView.moneyLabel setText:[NSString stringWithFormat:LOCALIZE(@"watching_viewer"),_post.liveViewerCount]];
    
    if ([_post.caption isEqualToString:@""]) {
        [_captionLabel setText:@"沒有標題"];
    } else {
        [_captionLabel setText:_post.caption];
    }
    [self.postImageView setImageWithURL:S3_FILE_URL(_user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
    
    if(_user.isVerified==1) {
        [_userVerifyButton setTitle:@"unVerified" forState:UIControlStateNormal];
        [_userVerifyButton setBackgroundColor:WHITE_COLOR];
        [_userVerifyButton setTitleColor:[UIColor colorWithRed:115.0/255.0 green:200.0/255.0 blue:155.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [_postTitleView verified:YES];
    } else {
        [_userVerifyButton setTitle:@"Verified" forState:UIControlStateNormal];
        [_userVerifyButton setBackgroundColor:[UIColor colorWithRed:115.0/255.0 green:200.0/255.0 blue:155.0/255.0 alpha:1.0]];
        [_userVerifyButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
        [_postTitleView verified:NO];
    }
    
    if(_user.isChoice==1){
        [_postTitleView choiced:YES];
    }else{
        [_postTitleView choiced:NO];
    }
    
    if(_user.crossGreatWall==1){
        [_postTitleView chinaFlag:YES];
    }else{
        [_postTitleView chinaFlag:NO];
    }
    
    if(_user.isInternational==1){
        [_postTitleView internationalFlag:YES];
    }else{
        [_postTitleView internationalFlag:NO];
    }
}



-(void)setPost:(PostObject *)post
{
    _post = post;
    
    if(_reviewMode==REVIEW_POST_CELL_MODE){
        [_livestreamHideButton setTitle:@"Hide" forState:UIControlStateNormal];
    }else{
        [_livestreamHideButton setTitle:@"Process" forState:UIControlStateNormal];
        [_livestreamPassButton removeFromSuperview];
    }
    
//    if(_post.liveStreamIDInt!=post.liveStreamIDInt) {
//        _loadingView.alpha = 1.0;
//    }
//    
//    self.postTitleView.timestamp = _post.beginTime;
    
    self.postTitleView.title = _post.user.openID;

    //    self.postTitleView.image = _post.picture;
    
    self.postTitleView.image = _post.user.picture;

//    self.postTitleView.views = _post.totalViewTime;
    [_postTitleView.viewsLabel setText:[NSString stringWithFormat:@"%d %@", _post.user.followerCount, LOCALIZE(@"user_profile_followers")]];
//    [_postTitleView.moneyLabel setText:[NSString stringWithFormat:LOCALIZE(@"watching_viewer"),_post.liveViewerCount]];
    
    if ([_post.caption isEqualToString:@""]) {
        [_captionLabel setText:@"沒有標題"];
    } else {
        [_captionLabel setText:_post.caption];
    }
    [self.postImageView setImageWithURL:S3_FILE_URL(_post.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];

    if(_post.user.isVerified==1) {
        [_userVerifyButton setTitle:@"unVerified" forState:UIControlStateNormal];
        [_userVerifyButton setBackgroundColor:WHITE_COLOR];
        [_userVerifyButton setTitleColor:[UIColor colorWithRed:115.0/255.0 green:200.0/255.0 blue:155.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [_postTitleView verified:YES];
    } else {
        [_userVerifyButton setTitle:@"Verified" forState:UIControlStateNormal];
        [_userVerifyButton setBackgroundColor:[UIColor colorWithRed:115.0/255.0 green:200.0/255.0 blue:155.0/255.0 alpha:1.0]];
        [_userVerifyButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
        [_postTitleView verified:NO];
    }
}

- (void)didClickTitle
{
    
    UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
    
    if(_reviewMode == REVIEW_PROFILE_CELL_MODE){
        [userCtrl getUserWithOpenID:_user.openID];
    }else{
        [userCtrl getUserWithOpenID:_post.user.openID];
    }
    userCtrl.hidesBottomBarWhenPushed = YES;
    [self.navCtrl pushViewController:userCtrl animated:YES];
}

- (void)didClickUserImage
{
//    LiveStreamViewController* vc = [SINGLETON liveStreamSharedManager];
//    vc.liveStreamMode = LIVE_STREAM_MODE_WATCH;
//    vc.livestreamID = [_livestream.liveStreamID copy];
//    [self.navCtrl presentViewController:NAV(vc) animated:YES completion:nil];
}

- (void)didClickButton:(UIButton*)btn
{
    
    [btn playBounceAnimation];
    
    if ([btn isEqual:_userGhostButton]) {
        
//        [API_MANAGER hideUserFromLiveAction:_livestream.user.userID hideUserValue:1 withCompletion:^(BOOL success) {
//            if (success) {
//                //                _livestream.user.ishiddenFromLiveStream = 1;
//                [DIALOG_MANAGER showCompleteToast];
//            } else {
//                [DIALOG_MANAGER showNetworkFailToast];
//            }
//        }];
    } else if ([btn isEqual:_userFreezeButton]) {
        
        [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"Freeze_confirm") options:@[LOCALIZE(@"OK")] destructiveIndexes:@[@0,@1] cancelable:YES withCompletion:^(int selectedOption) {
            if(selectedOption==0) {
                [API_MANAGER freezeUserAction:_post.user.userID inLivestreamID:@"" absTime:0 withComletion:^(BOOL success, NSString* message) {
                    if (success) {
                        [self didClickButton:_livestreamHideButton];
                    } else {
                        [DIALOG_MANAGER showNetworkFailToast];
                    }
                }];
            }
        }];
        
    } else if ([btn isEqual:_userVerifyButton]) {
        if (_post.user.isVerified) {
        
            [API_MANAGER removeVerifiedUserAction:_post.user.userID withCompletion:^(BOOL success, NSString *message) {
                if (success) {
                    [DIALOG_MANAGER showCompleteToast];
                    _post.user.isVerified = 0;
                    [_userVerifyButton setTitle:@"Verified" forState:UIControlStateNormal];
                    [_userVerifyButton setBackgroundColor:[UIColor colorWithRed:115.0/255.0 green:200.0/255.0 blue:155.0/255.0 alpha:1.0]];
                    [_userVerifyButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
                    [_postTitleView verified:NO];
                } else {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
        
        } else {
        
            [API_MANAGER verifyUserAction:_post.user.userID withCompletion:^(BOOL success, NSString *message) {
                if (success) {
                    [DIALOG_MANAGER showCompleteToast];
                    [_userVerifyButton setTitle:@"unVerified" forState:UIControlStateNormal];
                    [_userVerifyButton setBackgroundColor:WHITE_COLOR];
                    [_userVerifyButton setTitleColor:[UIColor colorWithRed:115.0/255.0 green:200.0/255.0 blue:155.0/255.0 alpha:1.0] forState:UIControlStateNormal];
                    _post.user.isVerified = 1;
                    [_postTitleView verified:YES];
                } else {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
        
        }
    } else if ([btn isEqual:_livestreamHideButton]) {
       
        if(_reviewMode==REVIEW_POST_CELL_MODE){
            
            [API_MANAGER hideExplorePicture:_post.postID withCompletion:^(BOOL success) {
                if (success) {
                    if(_delegate){
                        [_delegate removeItem:self];
                    }
                    [DIALOG_MANAGER showCompleteToast];
                } else {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
            
        }else if(_reviewMode == REPORT_POST_CELL_MODE){
            
            [API_MANAGER processReportedPostAction:_post.postID withCompletion:^(BOOL success) {
                if (success) {
                    if(_delegate){
                        [_delegate removeItem:self];
                    }
                    [DIALOG_MANAGER showCompleteToast];
                } else {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
        }else if(_reviewMode == REVIEW_PROFILE_CELL_MODE){
            
            // delete profile picture action
            
            [API_MANAGER adminDeleteProfilePictureAction:_user.userID withCompletion:^(BOOL success) {
                if (success) {
                    if(_delegate){
                        [_delegate removeItem:self];
                    }
                    [DIALOG_MANAGER showCompleteToast];
                } else {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
        }
    
    } else if ([btn isEqual:_livestreamPassButton]) {
       
        if(_reviewMode==REVIEW_POST_CELL_MODE){
            
            [API_MANAGER passExplorePicture:_post.postID withCompletion:^(BOOL success) {
                if (success) {
                    if(_delegate){
                        [_delegate removeItem:self];
                    }
                    [DIALOG_MANAGER showCompleteToast];
                } else {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
            
        }else if(_reviewMode == REVIEW_PROFILE_CELL_MODE){
            
            // process review profile action
            [API_MANAGER processProfilePictureReview:_user.userID withCompletion:^(BOOL success) {
                if (success) {
                    if(_delegate){
                        [_delegate removeItem:self];
                    }
                    [DIALOG_MANAGER showCompleteToast];
                } else {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
        }

        
    }
}





@end
