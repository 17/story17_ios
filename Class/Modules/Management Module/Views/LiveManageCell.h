//
//  LiveManageCell.h
//  Story17
//
//  Created by POPO on 9/30/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveStreamObject.h"

@class LiveManageCell;

@protocol LiveManageCellDelegate <NSObject>
-(void)removeItem:(LiveManageCell*)cell;
@end

@interface LiveManageCell : UICollectionViewCell

@property (nonatomic, weak) LiveStreamObject* livestream;
@property (nonatomic, weak) UINavigationController* navCtrl;
@property (nonatomic, weak) id<LiveManageCellDelegate> delegate;

+ (float)getHeightFromLivestream;

@end