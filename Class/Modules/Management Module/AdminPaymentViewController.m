//
//  AdminPaymentViewController.m
//  Story17
//
//  Created by POPO on 10/22/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "AdminPaymentViewController.h"
#import "Constant.h"

@interface AdminPaymentViewController()

@property (nonatomic, strong) UIDatePicker *datePicker;

@property (nonatomic, strong) UITextField* targetOpenIDTextField;
@property (nonatomic, strong) UITextField* remitDateTextField;
@property (nonatomic, strong) UITextField* amountTextField;
@property (nonatomic, strong) UITextField* bankNameTextField;
@property (nonatomic, strong) UITextField* accountNumberTextField;

@property (nonatomic, strong) NSDate* remitDate;

@property (nonatomic, strong) UIButton* sendPayPalButton;
@property (nonatomic, strong) UIButton* sendTWBankButton;
@property (nonatomic, strong) UIButton* sendBankButton;

@end

@implementation AdminPaymentViewController

-(void) viewDidLoad
{
    UIToolbar *dateToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    UIBarButtonItem *finish = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(finishDatePicker)];
    dateToolBar.items = [NSArray arrayWithObject:finish];
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done)];
    toolBar.items = [NSArray arrayWithObject:done];

    _targetOpenIDTextField = [[UITextField alloc] initWithFrame:CGRectMake(40, 100, 200, 50)];
    [_targetOpenIDTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _targetOpenIDTextField.delegate = self;
    _targetOpenIDTextField.borderStyle =  UITextBorderStyleRoundedRect;
    _targetOpenIDTextField.placeholder = @"請輸入收款者open ID";
    _targetOpenIDTextField.inputAccessoryView = toolBar;
    [self.view addSubview:_targetOpenIDTextField];
    
    _datePicker = [[UIDatePicker alloc]init];
    _datePicker.timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    _remitDateTextField = [[UITextField alloc] initWithFrame:CGRectMake(40, 200, 200, 50)];
    [_remitDateTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _remitDateTextField.delegate = self;
    _remitDateTextField.inputView = _datePicker;
    _remitDateTextField.borderStyle =  UITextBorderStyleRoundedRect;
    _remitDateTextField.placeholder = @"請選擇匯款日期";
    _remitDateTextField.inputAccessoryView = dateToolBar;
    [self.view addSubview:_remitDateTextField];
    
    _amountTextField = [[UITextField alloc] initWithFrame:CGRectMake(40, 300, 200, 50)];
    [_amountTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _amountTextField.delegate = self;
    _amountTextField.borderStyle =  UITextBorderStyleRoundedRect;
    _amountTextField.placeholder = @"請輸入匯款金額";
    _amountTextField.inputAccessoryView = toolBar;
    [self.view addSubview:_amountTextField];
    
    _bankNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(40, 400, 200, 50)];
    [_bankNameTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _bankNameTextField.delegate = self;
    _bankNameTextField.borderStyle =  UITextBorderStyleRoundedRect;
    _bankNameTextField.placeholder = @"請輸入收款機構";
    _bankNameTextField.inputAccessoryView = toolBar;
    [self.view addSubview:_bankNameTextField];
    
    _accountNumberTextField = [[UITextField alloc] initWithFrame:CGRectMake(40, 500, 200, 50)];
    [_accountNumberTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _accountNumberTextField.delegate = self;
    _accountNumberTextField.borderStyle =  UITextBorderStyleRoundedRect;
    _accountNumberTextField.placeholder = @"請輸入收款帳號";
    _accountNumberTextField.inputAccessoryView = toolBar;
    [self.view addSubview:_accountNumberTextField];
    
    _sendTWBankButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/10, 600, 90, 50)];
    [_sendTWBankButton setEnabled:NO];
    [_sendTWBankButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [_sendTWBankButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    [_sendTWBankButton setTitle:@"台灣銀行" forState:UIControlStateNormal];
    [_sendTWBankButton addTarget:self action:@selector(send:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_sendTWBankButton];
    
    _sendPayPalButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2-50, 600, 90, 50)];
    [_sendPayPalButton setEnabled:NO];
    [_sendPayPalButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [_sendPayPalButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    [_sendPayPalButton setTitle:@"PayPal" forState:UIControlStateNormal];
    [_sendPayPalButton addTarget:self action:@selector(send:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_sendPayPalButton];
    
    _sendBankButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-100-SCREEN_WIDTH/10, 600, 90, 50)];
    [_sendBankButton setEnabled:NO];
    [_sendBankButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [_sendBankButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    [_sendBankButton setTitle:@"海外銀行" forState:UIControlStateNormal];
    [_sendBankButton addTarget:self action:@selector(send:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_sendBankButton];
}

-(void)textFieldDidChange:(UITextField*)textField {
    
    if (_targetOpenIDTextField.text.length>0 && _remitDateTextField.text.length>0 && _amountTextField.text.length>0) {
        
        if (_accountNumberTextField.text.length>0 && _bankNameTextField.text.length>0) {
            _sendBankButton.enabled = YES;
            _sendTWBankButton.enabled = YES;
        }
        _sendPayPalButton.enabled = YES;
        
    } else {
        
        _sendPayPalButton.enabled = NO;
    }
}

- (void)send:(id)sender
{
    NSString* message;
    NSString* year = [NSString stringWithFormat:@"%ld", (long)_remitDate.year];
    NSString* month = [NSString stringWithFormat:@"%ld", (long)_remitDate.month];
    NSString* day = [NSString stringWithFormat:@"%ld", (long)_remitDate.day];

    if ([sender isEqual:_sendPayPalButton]) {
        _bankNameTextField.text = @"PayPal";
        message = [NSString stringWithFormat:@"親愛的%@您好，\n您9月份的分潤金額，一共%@美元，已於%@/%@/%@匯款至您的PayPal帳戶，請前往查看。\n提醒您，分潤金額需扣除銀行匯費，實際提領金額以帳戶收到的金額為主。\n\n註：個人頁面內的未領分潤金額，會在下次更新後自動扣除此次提領金額。", _targetOpenIDTextField.text, _amountTextField.text, year, month, day];

    } else if ([sender isEqual:_sendTWBankButton]) {
        message = [NSString stringWithFormat:@"親愛的%@您好，\n您9月份的分潤金額，一共%@台幣，已於%@/%@/%@匯款至您提供的銀行帳戶，請前往查看。\n提醒您，下個月開始僅提供PayPal支付，不會再使用銀行匯款方式，若您的PayPal帳戶未設定，請儘速前往設定。\n\n註：個人頁面內的未領分潤金額，會在下次更新後自動扣除此次提領金額。", _targetOpenIDTextField.text, _amountTextField.text, year, month, day];
    } else if ([sender isEqual:_sendBankButton]) {
        message = [NSString stringWithFormat:@"親愛的%@您好，\n您9月份的分潤金額，一共%@美元，已於%@/%@/%@匯款至您的PayPal帳戶，請前往查看。\n提醒您，分潤金額需扣除銀行匯費與匯差，實際提領金額以帳戶收到的金額為主。\n\n註：1. 個人頁面內的未領分潤金額，會在下次更新後自動扣除此次提領金額。\n　　2. 下個月開始僅提供PayPal支付，不會再使用銀行匯款方式，若您的PayPal帳戶未設定，請儘速前往設定。", _targetOpenIDTextField.text, _amountTextField.text, year, month, day];
    }
    [API_MANAGER balancePayment:_targetOpenIDTextField.text amount:_amountTextField.text toBankName:_bankNameTextField.text account:_accountNumberTextField.text inYear:year month:month day:day withMessage:message withCompletion:^(BOOL success) {
        if (success) {
            [DIALOG_MANAGER showCompleteToast];
        } else {
            [DIALOG_MANAGER showNetworkFailToast];
        }
    }];
}

-(void) finishDatePicker
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    _remitDateTextField.text = [NSString stringWithFormat:@"%@",[formatter stringFromDate:_datePicker.date]];
    _remitDate = _datePicker.date;
    
    [self.view endEditing:YES];
}

-(void) done
{
    [self.view endEditing:YES];
}

@end
