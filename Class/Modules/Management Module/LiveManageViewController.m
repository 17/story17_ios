//
//  LiveManageViewController.m
//  Story17
//
//  Created by POPO on 10/1/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "LiveManageViewController.h"
#import "SVPullToRefresh.h"

@interface LiveManageViewController()

#define LiveManaegeCellKey @"LiveManageCellKey"
#define fetchNum 12

@property (nonatomic, strong) UICollectionView* liveManageCollectionView;
@property (nonatomic, strong) NSMutableArray* liveStreamArray;
@property (nonatomic, strong) UIActivityIndicatorView* progressView;
@property (nonatomic) BOOL isFirstFetching;
@property (nonatomic) BOOL isFetching;
@property (nonatomic) int medulo;
@property (nonatomic) int devisor;
@property (nonatomic, strong) UIBarButtonItem* rightBarItem;
@property (nonatomic, strong) NSString* fetchMode;


@end

@implementation LiveManageViewController

#define SHARD_NUMBER 50

- (id)init
{
    self = [super init];
    if (self) {
        _medulo = 1;
        _devisor = 1;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    
    _fetchMode = @"hot";
    
    self.title = LOCALIZE(@"review_livestream_title");
    
    _liveStreamArray = CREATE_MUTABLE_ARRAY;
    __weak LiveManageViewController* weakSelf = self;
    
    _rightBarItem =[[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"%d/%d",_medulo,_devisor] style:UIBarButtonItemStyleDone target:self action:@selector(pickerAction:)];
    self.navigationItem.rightBarButtonItem = _rightBarItem;

    
    if (_liveManageCollectionView == nil) {
        UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
        [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
        
        _liveManageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT) collectionViewLayout:layout];
        [_liveManageCollectionView setBackgroundColor:LIGHT_BG_COLOR];
        _liveManageCollectionView.delegate = self;
        _liveManageCollectionView.dataSource = self;
        [_liveManageCollectionView registerClass:[LiveManageCell class] forCellWithReuseIdentifier:LiveManaegeCellKey];
        [_liveManageCollectionView setAlwaysBounceVertical:YES];
        [_liveManageCollectionView addPullToRefreshWithActionHandler:^{
            [weakSelf fetchData:weakSelf.fetchMode];
        }];
        [self.view addSubview:_liveManageCollectionView];
        
        _progressView = [UIActivityIndicatorView new];
        _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
        [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [_progressView setColor:MAIN_COLOR];
        [self.view addSubview:_progressView];
        
        _isFirstFetching = true;
        _isFetching = false;
    }
    
//    [self fetchData];
    
    [self pickerAction:nil];
}

-(void)reloadItemBarTitle
{
    [_rightBarItem setTitle:[NSString stringWithFormat:@"%d/%d",_medulo,_devisor]];
}

-(void)pickerAction:(id)sender
{
    NSMutableArray* rangeItem = CREATE_MUTABLE_ARRAY;
    for(int i=1;i<=SHARD_NUMBER;i++){
        [rangeItem addObject:[NSString stringWithFormat:@"%d",i]];
    }
    int lowOffset = (int)[rangeItem indexOfObject:[NSString stringWithFormat:@"%d",_medulo]];
    int rightOffset = (int)[rangeItem indexOfObject:[NSString stringWithFormat:@"%d",_devisor]];
    
    [DIALOG_MANAGER showRangePickerDialogWithItems:[rangeItem copy] rightItem:[rangeItem copy] lowOffset:lowOffset highOffset:rightOffset withCompletion:^(BOOL done, int selectedLowIndex, int selectedHighIndex) {
        if(done){
            _medulo = [rangeItem[selectedLowIndex] intValue];
            _devisor = [rangeItem[selectedHighIndex] intValue];
            [self reloadItemBarTitle];
            [self fetchData:@""];
        }
    }];
}

- (void)fetchData:(NSString*)mode
{
    if(_isFetching)
        return;

    if(_isFirstFetching){
        [_progressView startAnimating];
    }else{
        [_liveManageCollectionView.pullToRefreshView startAnimating];
    }
    
    _isFetching = YES;
    
    [API_MANAGER getLiveStreamsForReview:GET_DEFAULT(IP_COUNTRY) beforeTime:CURRENT_TIMESTAMP count:50 fetchMode:mode modulo:_medulo-1 devisor:_devisor withCompletion:^(BOOL success, NSArray *liveStreams) {
        _isFetching = NO;
        [_liveManageCollectionView.pullToRefreshView stopAnimating];
        [_progressView stopAnimating];
        [DIALOG_MANAGER hideLoadingView];
        _isFirstFetching = false;
        
        if (success) {
            [_liveStreamArray removeAllObjects];
            [_liveStreamArray addObjectsFromArray:liveStreams];
            [_liveManageCollectionView reloadData];
            [_liveManageCollectionView scrollsToTop];
        }
    }];
}

#pragma mark - UICollectionView DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(3, 0, 0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return  CGSizeMake(SCREEN_WIDTH,0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10.0f;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    return CGSizeMake(SCREEN_WIDTH, [LiveManageCell getHeightFromLivestream]);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _liveStreamArray != nil ? [_liveStreamArray count] : 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
   
        LiveStreamObject* liveObj = [_liveStreamArray objectAtIndex:indexPath.row];
    
        LiveManageCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:LiveManaegeCellKey forIndexPath:indexPath];
            
        if (cell == nil)
        {
            cell = [[LiveManageCell alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,100)];
        }
        cell.delegate = self;
        cell.navCtrl = self.navigationController;
        cell.livestream = liveObj;  // reloadUI Action
            
        return cell;
}

#pragma mark LiveManagerCellDelegate
-(void)removeItem:(LiveManageCell*)cell
{
    [_liveStreamArray removeObject:cell.livestream];
    
    [_liveManageCollectionView performBatchUpdates:^{
        
        NSIndexPath* indexPath = [_liveManageCollectionView indexPathForCell:cell];
        [_liveManageCollectionView deleteItemsAtIndexPaths:@[indexPath]];
        
    } completion:^(BOOL finished) {

        [_liveManageCollectionView reloadData];
    }];
}

@end
