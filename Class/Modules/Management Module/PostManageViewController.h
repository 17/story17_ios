//
//  LiveManageViewController.h
//  Story17
//
//  Created by POPO on 10/1/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostManageCell.h"

typedef enum {
    REVIEW_POST_MODE=0,
    REPORT_POST_MODE,
    REVIEW_PROFILE_PICTURE
} POST_MANAGER_MODE;


@interface PostManageViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,PostManageCellDelegate>

@property POST_MANAGER_MODE reviewMode;

@end
