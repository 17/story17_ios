//
//  PaymentObject.m
//  story17
//
//  Created by York on 5/3/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "PaymentObject.h"

@implementation PaymentObject

+(PaymentObject*) getPaymentWithDict:(NSDictionary*)dict
{
    PaymentObject* payment = [PaymentObject new];

    payment.year = [dict[@"year"] intValue];
    payment.month = [dict[@"month"] intValue];
    payment.amount = [dict[@"amount"] intValue];
    payment.status = dict[@"status"];
    payment.timestamp = [dict[@"timestamp"] intValue];
    payment.bankName = dict[@"bankName"];
    payment.bankAddress = dict[@"bankAddress"];
    payment.accountNumber = dict[@"accountNumber"];
    payment.beneficiaryName = dict[@"beneficiaryName"];
    payment.swiftCode = [dict[@"swiftCode"] intValue];
    payment.routingNumber = [dict[@"routingNumber"] intValue];
    payment.bankCountry = dict[@"bankCountry"];
    payment.idCardNumber = [dict[@"idCardNumber"] intValue];
    payment.address = dict[@"address"];
    payment.bankCode = [dict[@"bankCode"] intValue];
    payment.bankBranchCode = [dict[@"bankBranchCode"] intValue];

    return payment;
}


@end
