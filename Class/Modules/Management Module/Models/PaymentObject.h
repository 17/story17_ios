//
//  PaymentObject.h
//  story17
//
//  Created by York on 5/3/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentObject : NSObject

@property(nonatomic) int year;
@property(nonatomic) int month;
@property(nonatomic) float amount;
@property(nonatomic,strong) NSString* status;
@property(nonatomic) int timestamp;
@property(nonatomic,strong) NSString* bankName;
@property(nonatomic,strong) NSString* bankAddress;
@property(nonatomic,strong) NSString* accountNumber;
@property(nonatomic,strong) NSString* beneficiaryName;
@property(nonatomic) int swiftCode;
@property(nonatomic) int routingNumber;
@property(nonatomic,strong) NSString* bankCountry;
@property(nonatomic) int idCardNumber;
@property(nonatomic,strong) NSString* address;
@property(nonatomic) int bankCode;
@property(nonatomic) int bankBranchCode;

+(PaymentObject*) getPaymentWithDict:(NSDictionary*)dict;

@end
