//
//  GiftManageViewController.h
//  Story17
//
//  Created by POPO on 11/10/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IAPManager.h"
#import <SafariServices/SafariServices.h>



typedef NS_ENUM(NSInteger, GiftManagerMode) {
    GiftManagerModeNone = -1,
    GiftManagerModePurchase = 0,
    GiftManagerModeUsageRecord = 1
};

@interface GiftManageViewController : UIViewController<UIScrollViewDelegate,UITableViewDataSource, UITableViewDelegate,IAPManagerDelegate1,SFSafariViewControllerDelegate>

@property(nonatomic,strong) NSArray* btnArray;
@property(nonatomic,strong) NSArray* tableViewArray;
@property (nonatomic,strong) NSMutableArray* purchaseRecordArray;
@property (nonatomic,strong) NSMutableArray* useRecordArray;  /* Only on selfNotif */
@property (nonatomic,strong) UIActivityIndicatorView* progressView;
@property (nonatomic,strong) UIActivityIndicatorView* progressView2;
@property (nonatomic,strong) UIImageView* noDataImageView;
@property (nonatomic,strong) UIImageView* noDataImageView2;
@property (nonatomic,strong) NSArray* tableDataArray;
@property (nonatomic,strong) NSArray* noDataArray;
@property (nonatomic,strong) NSArray* progressArray;
@property (nonatomic,strong) UIView* topContainerView;
@property (nonatomic,strong) UILabel* nowPointLabel;
@property (nonatomic,strong) UIImageView* pointImageView;
@property(nonatomic, weak) UINavigationController* navCtrl;

@property BOOL isFirstFetching;
@property BOOL isFetching;
@property BOOL noMoreData;
@end
