//
//  NotificationViewController.h
//  story17
//
//  Created by POPO Chen on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserTitleView.h"
#import "UserObject.h"
@interface GiftBoardViewController : UIViewController <UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) UIView* mainView;

/* mode indicator */
@property(nonatomic,strong) UIView* modeSelectorContainer;
@property(nonatomic,strong) UIImageView* modeIndicatorImageView;

/* mode btn array */
@property(nonatomic,strong) NSArray* btnArray;
@property(nonatomic,strong) UIButton* dayBtn;
@property(nonatomic,strong) UIButton* totalBtn;
@property(nonatomic,strong) UIButton* monthBtn;

/* main scrollview */
@property(nonatomic,strong) UIScrollView* mainScrollView;
@property(nonatomic,strong) NSArray* tableViewArray;
@property(nonatomic,strong) UITableView* dayTableView;
@property(nonatomic,strong) UITableView* totalTableView;
@property(nonatomic,strong) UITableView* monthTableView;

/* data array */
@property (nonatomic,strong) NSArray* notifArray;

@property (nonatomic,strong) NSMutableArray* dayArray;
@property (nonatomic,strong) NSMutableArray* totalArray;
@property (nonatomic,strong) NSMutableArray* monthArray;


@property (nonatomic,strong) NSMutableArray* monthUsers;
@property (nonatomic,strong) NSMutableArray* daysUsers;
@property (nonatomic,strong) NSMutableArray* weekUsers;



/* progress view */
@property (nonatomic,strong) NSArray* progressArray;
@property (nonatomic,strong) UIActivityIndicatorView* progressView;
@property (nonatomic,strong) UIActivityIndicatorView* progressView2;
@property (nonatomic,strong) UIActivityIndicatorView* progressView3;

/* no data view */
@property (nonatomic,strong) NSArray* noDataArray;
@property (nonatomic,strong) UIImageView* noDataImageView;
@property (nonatomic,strong) UIImageView* noDataImageView2;
@property (nonatomic,strong) UIImageView* noDataImageView3;
@property (nonatomic,strong) UserObject* user;

@property  int viewMode;    // PHOTO_LEFT PHOTO_RIGHT

@property BOOL isFirstFetching;
@property BOOL isFetching;
@property BOOL noMoreData;


@end
