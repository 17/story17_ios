//
//  LivestreamGiftLeaderboardObject.m
//  Story17
//
//  Created by Racing on 2015/12/15.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "LiveStreamGiftLeaderboardObject.h"
#import "ContributorObject.h"

@implementation LiveStreamGiftLeaderboardObject

+ (LiveStreamGiftLeaderboardObject *)getLiveStreamLeaderboardWithResult:(NSDictionary *)results
{
    LiveStreamGiftLeaderboardObject *obj = [[LiveStreamGiftLeaderboardObject alloc] init];
    obj.myPoint = [results[@"myPoint"] intValue];
    obj.totalPoint = [results[@"totalPoint"] intValue];
    NSMutableArray *contributors = [NSMutableArray array];
    
    for (NSDictionary *contributorDict in results[@"rank"]) {
        [contributors addObject:[ContributorObject getContributorWithResult:contributorDict]];
    }
    
    obj.contributors = contributors;
    
    return obj;
}

@end
