//
//  GiftLeaderboardViewController.h
//  Story17
//
//  Created by Racing on 2015/12/14.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LiveStreamObject;

@interface GiftLeaderboardViewController : UIViewController
@property (strong, nonatomic) LiveStreamObject *liveStreamObj;
@end
