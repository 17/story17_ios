//
//  NotificationViewController.m
//  story17
//
//  Created by POPO Chen on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "GiftBoardViewController.h"
#import "Constant.h"
#import "SVPullToRefresh.h"
#import "GiftBoardCell.h"
#import "UserListViewController.h"
#import "UserTitleView.h"
#import "UserProfileViewController.h"
@interface GiftBoardViewController()

@property BOOL needUpdate;
@property (nonatomic,strong) NSMutableArray* updatedArray;

@end

@implementation GiftBoardViewController

#define btnWidth SCREEN_WIDTH/2
#define scrollViewHeight SCREEN_HEIGHT-2*NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT
#define fetchNum 20
#define sectionHeight 25

-(id)init
{
    self = [super init];
    if(self){
        
    }
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];

    self.navigationController.title = @"";
    
    _dayArray = CREATE_MUTABLE_ARRAY;
    _totalArray = CREATE_MUTABLE_ARRAY;
    _monthArray = CREATE_MUTABLE_ARRAY;
    
    _monthUsers = CREATE_MUTABLE_ARRAY;
    _daysUsers = CREATE_MUTABLE_ARRAY;
    _weekUsers = CREATE_MUTABLE_ARRAY;
    
    _updatedArray = CREATE_MUTABLE_ARRAY;
    _needUpdate = YES;
    
    _isFirstFetching = true;
    
    [self setup];
    
    [self fetchData:true withMode:-1];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    self.title = [NSString stringWithFormat:LOCALIZE(@"My_gift_leader_board"),_user.openID];
    

    
    if([GET_DEFAULT(UPDATE_GIFT_BOARD_LOG_TIME) intValue] < CURRENT_TIMESTAMP-UPDATE_INTERVAL){
        
        _needUpdate = YES;
        [_updatedArray removeAllObjects];
        
        /* reload current mode */
        [self fetchData:true withMode:-1];
        
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    /* Level 5 crash fixed */
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

-(void)dealloc
{
    [NOTIFICATION_CENTER removeObserver:self];
}

//-(void)fetchNotif:(id)sender
//{
//    
//    NSNotification* s = sender;
//    NSString* pushType = s.name;
//    
//    if([pushType isEqualToString:NEW_FOLLOW]){
//        [self fetchData:YES withMode:0];
//        [self fetchData:YES withMode:1];
//    }else if( [pushType isEqualToString:NEW_COMMENT]){
//        [self fetchData:YES withMode:0];
//        [self fetchData:YES withMode:1];
//    }else if( [pushType isEqualToString:NEW_POST_TAG]){
//        [self fetchData:YES withMode:0];
//        [self fetchData:YES withMode:1];
//    }else if( [pushType isEqualToString:NEW_COMMENT_TAG]){
//        [self fetchData:YES withMode:0];
//        [self fetchData:YES withMode:1];
//    }else if( [pushType isEqualToString:NEW_FRIEND_JOIN_FROM_CONTACTS]){
//        [self fetchData:YES withMode:0];
//        [self fetchData:YES withMode:1];
//    }else if( [pushType isEqualToString:NEW_FRIEND_JOIN_FROM_FB]){
//        [self fetchData:YES withMode:0];
//        [self fetchData:YES withMode:1];
//    }else if( [pushType isEqualToString:NEW_SYSTEM_NOTIF]){
//        [self fetchData:YES withMode:2];
//    }
//}


-(void)setup
{
    _viewMode = SELF_NOTIF;
//    
//    UIButton* findFriendBtn = [ThemeManager findFriendBtn];
//    [findFriendBtn addTarget:self action:@selector(didClickFindFriend:) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:findFriendBtn];
//    
    _mainView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
    
    _dayBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2, 0, btnWidth, NAVI_BAR_HEIGHT)];
    _dayBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [_dayBtn setTitle:LOCALIZE(@"leaderboard_day") forState:UIControlStateNormal];
    [_dayBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_dayBtn setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
    [_dayBtn addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];
    [_dayBtn setSelected:YES];
    
    _totalBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, btnWidth, NAVI_BAR_HEIGHT)];
    _totalBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [_totalBtn setTitle:LOCALIZE(@"leaderboard_total") forState:UIControlStateNormal];
    [_totalBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_totalBtn setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
    [_totalBtn addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _monthBtn = [[UIButton alloc] initWithFrame:CGRectMake(2*SCREEN_WIDTH/3, 0, btnWidth, NAVI_BAR_HEIGHT)];
    _monthBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [_monthBtn setTitle:LOCALIZE(@"leaderboard_month") forState:UIControlStateNormal];
    [_monthBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_monthBtn setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
    [_monthBtn addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnArray = @[_totalBtn,_dayBtn,_monthBtn];
    
    _modeSelectorContainer = [ThemeManager maintabBackgroundImageView];
    _modeSelectorContainer.frame = CGRectMake(0, 0, SCREEN_WIDTH, NAVI_BAR_HEIGHT);
    _modeSelectorContainer.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
    [_modeSelectorContainer setBackgroundColor:[UIColor clearColor]];
    
    _modeIndicatorImageView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"bar")];
    _modeIndicatorImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH/2, 3);
    _modeIndicatorImageView.layer.masksToBounds = YES;
    _modeIndicatorImageView.center = CGPointMake(SCREEN_WIDTH/4, NAVI_BAR_HEIGHT-3);
    [_modeSelectorContainer addSubview:_modeIndicatorImageView];
    
    [_modeSelectorContainer addSubview:_dayBtn];
    [_modeSelectorContainer addSubview:_totalBtn];
//    [_modeSelectorContainer addSubview:_monthBtn];
    
    _mainScrollView = [ThemeManager getBouncedScrollView];
    _mainScrollView.frame = CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, scrollViewHeight);
    _mainScrollView.delegate = self;
    [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH*2, _mainScrollView.frame.size.height)];
    
    
    /* CollectionView Setting */
    
    __weak GiftBoardViewController *weakSelf = self;
    _totalTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, scrollViewHeight) style:UITableViewStylePlain];
    _totalTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_totalTableView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData:TRUE withMode:-1];
    }];
    
    _dayTableView = [[UITableView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, scrollViewHeight)];
    _dayTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_dayTableView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData:TRUE withMode:-1];
    }];
    _monthTableView = [[UITableView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*2, 0, SCREEN_WIDTH, scrollViewHeight) style:UITableViewStylePlain];
    _monthTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_monthTableView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData:TRUE withMode:-1];
    }];
    [self tableViewSettings:_totalTableView];
    [self tableViewSettings:_dayTableView];
    [self tableViewSettings:_monthTableView];
    
    _tableViewArray = @[_totalTableView,_dayTableView,_monthTableView];
    _notifArray = @[_totalArray,_dayArray,_monthArray];
    
    
    /* no data view & progress view settings */
    
    _noDataImageView = [ThemeManager getNoDataImageView];
    _noDataImageView2 = [ThemeManager getNoDataImageView];
    _noDataImageView3 = [ThemeManager getNoDataImageView];
    
    CGRect rect = _noDataImageView.frame;
    _noDataImageView.frame = rect;
    rect.origin.x += SCREEN_WIDTH;
    _noDataImageView2.frame = rect;
    rect.origin.x += SCREEN_WIDTH;
    _noDataImageView3.frame = rect;
    
    _noDataArray = @[_noDataImageView,_noDataImageView2,_noDataImageView3];
    
    _progressView = [UIActivityIndicatorView new];
    _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView setColor:MAIN_COLOR];
    
    _progressView2 = [UIActivityIndicatorView new];
    _progressView2.frame = CGRectMake(SCREEN_WIDTH/2-15+SCREEN_WIDTH, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView2 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView2 setColor:MAIN_COLOR];
    
    _progressView3 = [UIActivityIndicatorView new];
    _progressView3.frame = CGRectMake(SCREEN_WIDTH/2-15+2*SCREEN_WIDTH, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView3 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView3 setColor:MAIN_COLOR];
    
    _progressArray = @[_progressView,_progressView2,_progressView3];
    
    [_mainScrollView addSubview:_dayTableView];
    [_mainScrollView addSubview:_totalTableView];
    [_mainScrollView addSubview:_monthTableView];
    
    [_mainScrollView addSubview:_noDataImageView];
    [_mainScrollView addSubview:_noDataImageView2];
    [_mainScrollView addSubview:_noDataImageView3];
    
    [_mainScrollView addSubview:_progressView];
    [_mainScrollView addSubview:_progressView2];
    [_mainScrollView addSubview:_progressView3];
    
    [_mainView addSubview:_mainScrollView];
    [_mainView addSubview:_modeSelectorContainer];
    [self.view addSubview:_mainView];
    
}

-(void)fetchData:(BOOL)refresh withMode:(int)viewMode
{
    if(refresh){
        _noMoreData = false;
    }
    if(_isFetching || _noMoreData) return;
    
    int modeChange = viewMode==-1?_viewMode:viewMode;
    
    /* current view */
    NSMutableArray* notifArray = _notifArray[modeChange];
    UILabel* noDataView = _noDataArray[modeChange];
    UITableView* tableView = _tableViewArray[modeChange];
    UIActivityIndicatorView* progressView = _progressArray[modeChange];
    int currentMode = modeChange;
    
    /* Handle updated */
    if(refresh){
        if(![_updatedArray containsObject:INT_TO_STRING(currentMode)]){
            [_updatedArray addObject:INT_TO_STRING(currentMode)];
        }
        
        if((int)[_updatedArray count]==3){
            [_updatedArray removeAllObjects];
            _needUpdate = NO;
        }
        

    }
    
    
   
    
//    if([notifArray count]>0){
//        lastPostObject = notifArray[[notifArray count]-1];
//        if(!refresh){
//            offset = (int)[notifArray count];
//        }
//    }
//    
//    if(lastPostObject==NULL || refresh){
//        beforeTime = INT32_MAX;
//    }else{
//        beforeTime = lastPostObject.timestamp;
//    }
//    
    _isFetching = true;
    
    if((viewMode==-1 || refresh) && !_isFirstFetching){
        [tableView.pullToRefreshView startAnimating];
    }
    
    if(_isFirstFetching){
        [progressView startAnimating];
        _isFirstFetching = false;
    }
    
    if (modeChange==0) {
        int32_t offset=0;
        if (refresh) {
            offset=0;
            [_totalArray removeAllObjects];

        }else{
            offset=(int32_t)[_totalArray count];
        }
        [API_MANAGER getGiftLeaderboardType:GiftLeaderboardRecordTypeTotal fromUser:_user.userID withCount:20 andOffset:offset withCompletion:^(BOOL success, NSArray *giftLeaderboardRecords) {
            
            
            
            [progressView stopAnimating];
            [tableView.pullToRefreshView stopAnimating];
            _isFetching = false;
            
    
            
            if(success){
                DLog(@"Stop");
                
                if(refresh){
                    //                [DEFAULTS setObject:INT_TO_NUMBER(CURRENT_TIMESTAMP) forKey:UPDATE_NOTIF_TIME];
                    [DEFAULTS synchronize];
                }
                
                [_totalArray addObjectsFromArray:giftLeaderboardRecords];
                
                if([giftLeaderboardRecords count]<fetchNum){
                    _noMoreData = true;
                }
                
                // 如果跑到一半換 mode 本來這個 collectionview reload就變成另一個 to avoid wrong reoload
                if(currentMode==modeChange){
                    DLog(@"reload");
                    [tableView reloadData];
                }
                if([_totalArray count]==0){
                    noDataView.hidden = NO;
                }else{
                    noDataView.hidden = YES;
                }
            }
            
        }];

    }else{
        int32_t offset=0;
        if (refresh) {
            offset=0;
            [_dayArray removeAllObjects];

        }else{
            offset=(int32_t)[_dayArray count];
        }
        [API_MANAGER getGiftLeaderboardType:GiftLeaderboardRecordTypeDay fromUser:_user.userID withCount:20 andOffset:(int32_t)[_dayArray count] withCompletion:^(BOOL success, NSArray *giftLeaderboardRecords) {
            
            
            
            [progressView stopAnimating];
            [tableView.pullToRefreshView stopAnimating];
            _isFetching = false;
            
     
            if(success){
                DLog(@"Stop");
                
                if(refresh){
                    [DEFAULTS synchronize];
                }
                
                [notifArray addObjectsFromArray:giftLeaderboardRecords];
        
                
                if([giftLeaderboardRecords count]<fetchNum){
                    _noMoreData = true;
                }
                if([_dayArray count]==0){
                    noDataView.hidden = NO;
                }else{
                    noDataView.hidden = YES;
                }
                // 如果跑到一半換 mode 本來這個 collectionview reload就變成另一個 to avoid wrong reoload
                if(currentMode==modeChange){
                    DLog(@"reload");
                    [tableView reloadData];
                }
                
            }
            
        }];

    }
    
    
    }


#pragma mark - UITableViewDelegagte
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [UserTitleView getDefaultHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GiftLeaderboardRecordObject* giftLeaderBoard;
    NSLog(@"%lu",(unsigned long)[_notifArray[_viewMode] count]);
    NSLog(@"%ld",(long)indexPath.row);

    if ([_notifArray[_viewMode] count]>indexPath.row) {
        
          giftLeaderBoard = [_notifArray[_viewMode] objectAtIndex:indexPath.row];
        
        if([giftLeaderBoard.user.userID isEqualToString:MY_USER_ID]){
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            [cell shake:5 withDelta:4 speed:0.05 shakeDirection:ShakeDirectionHorizontal];
            return;
        }
        
        UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
        userCtrl.user = giftLeaderBoard.user;
        [userCtrl getUserWithUserID:userCtrl.user.userID];
        [self.navigationController pushViewController:userCtrl animated:YES];
    }

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([tableView isEqual:_totalTableView]){
//        DLog(@"%d %lu %lu",_viewMode, (unsigned long)[_followNotifArray count],(unsigned long)[(NSArray*)_notifArray[_viewMode] count]);
        return [_totalArray count];
    }else if([tableView isEqual:_dayTableView]){
//        DLog(@"%d  %lu %lu",_viewMode, (unsigned long)[_selfNotifArray count],(unsigned long)[(NSArray*)_notifArray[_viewMode] count]);
        return [_dayArray count];
    }else{
//        DLog(@"%d  %lu %lu",_viewMode, (unsigned long)[_systemNotifArray count],(unsigned long)[(NSArray*)_notifArray[_viewMode] count]);
        return [_monthArray count];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, sectionHeight)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-20, sectionHeight)];
    [label setTextColor:DARK_GRAY_COLOR];
    [label setFont: BOLD_FONT_WITH_SIZE(14) ];
    label.textAlignment = NSTextAlignmentLeft;
    [view setBackgroundColor:LIGHT_GRAY_COLOR];
    [label setText:@"message"];
    
    [view addSubview:label];
    [view setAlpha:0.8];
    
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView isEqual:_totalTableView]){
        
        GiftBoardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotifCell"];
     
        if ([_notifArray[0] count]!=0){
        GiftLeaderboardRecordObject* giftLeaderBoard;
        giftLeaderBoard = [_notifArray[0] objectAtIndex:indexPath.row];
        
        [cell setUser:giftLeaderBoard.user withGiftpoint:[NSString stringWithFormat:@"%d",giftLeaderBoard.point]];
        }
        if(cell==nil){
            
            cell = [[GiftBoardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NotifCell"];

        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if(indexPath.row>[_totalArray count]-fetchNum/3){
            [self fetchData:false withMode:0];
        }
        
        return cell;
    }else if( [tableView isEqual:_dayTableView]){
        
            GiftBoardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FriendNotifCell"];
        if ([_notifArray[1] count]!=0) {
            GiftLeaderboardRecordObject* giftLeaderBoard;
            giftLeaderBoard = [_notifArray[1] objectAtIndex:indexPath.row];
            [cell setUser:giftLeaderBoard.user withGiftpoint:[NSString stringWithFormat:@"%d",giftLeaderBoard.point]];
        }
   

            if(cell==nil){
                cell = [[GiftBoardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FriendNotifCell"];

            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            if(indexPath.row>[_dayArray count]-fetchNum/3){
                [self fetchData:false withMode:1];
            }
            
            return cell;
            
        
    }else{
        GiftBoardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SystemNotifCell"];
        if ([_notifArray[2] count]!=0){
        UserObject* user;
        user = [_notifArray[2] objectAtIndex:indexPath.row];
            [cell setUser:user withGiftpoint:@"1234"];
        }
        if(cell==nil){
            cell = [[GiftBoardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SystemNotifCell"];

        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if(indexPath.row>[_monthArray count]-fetchNum/3){
            [self fetchData:false withMode:2];
        }
        return cell;
    }
}

#pragma mark - API Action with callback
//-(void)callApiWithMode:(int)mode andBeforeTime:(int)beforeTime withCompletion:(void(^)(BOOL success,NSArray* dict))callback
//{
//    if(mode==FOLLOWER_NOTIF){
//        [API_MANAGER getFriendNotif:beforeTime andCount:fetchNum withCompletion:^(BOOL success, NSArray *dict) {
//            callback(success,dict);
//        }];
//    }else if(mode==SELF_NOTIF){
//        [API_MANAGER getNotif:beforeTime andCount:fetchNum withCompletion:^(BOOL success, NSArray *dict) {
//            callback(success,dict);
//        }];
//    }else if(mode==SYSTEM_NOTIF){
//        [API_MANAGER getSystemNotif:beforeTime andCount:fetchNum withCompletion:^(BOOL success, NSArray *dict) {
//            callback(success,dict);
//            [API_MANAGER readSystemNotifWithCompletion:^(BOOL success) {}];
//        }];
//    }
//    
//}

-(void)changeModeAction:(id)sender
{
    DLog(@"change mode");
    if([sender isEqual:_totalBtn]){
        [self switchToMode:SELF_NOTIF animated:YES];
    }else if ([sender isEqual:_dayBtn]){
        [self switchToMode:FOLLOWER_NOTIF animated:YES];
        [API_MANAGER readNotif:^(BOOL success) {}];
    }else if([sender isEqual:_monthBtn]){
        [self switchToMode:SYSTEM_NOTIF animated:YES];
    }
}

-(void) switchToMode:(int)mode animated:(BOOL) animated
{
    if(_viewMode==mode) {
        return;
    }
    _viewMode = mode;
    NSMutableArray* notifs = _notifArray[mode];
    
    if(_needUpdate){
        if((int)[notifs count]==0){
            _isFirstFetching = true;
        }
        _isFetching = false;
        [self fetchData:true withMode:_viewMode];
    }
    if(_viewMode==SELF_NOTIF){
        [_dayBtn setSelected:NO];
        [_totalBtn setSelected:YES];
        [_monthBtn setSelected:NO];
    }else if(_viewMode==FOLLOWER_NOTIF){
        [_dayBtn setSelected:YES];
        [_totalBtn setSelected:NO];
        [_monthBtn setSelected:NO];
    }else if(_viewMode==SYSTEM_NOTIF){
        [_dayBtn setSelected:NO];
        [_totalBtn setSelected:NO];
        [_monthBtn setSelected:YES];
    }
    [self reloadUIAnimated:animated];
}

-(void) reloadUIAnimated:(BOOL)animated
{
    for (int mode=0; mode < 3; mode++) {
        UIButton* modeButton = _btnArray[mode];
        UIScrollView *tableView = _tableViewArray[mode];
        
        if(mode == _viewMode) {
            
            tableView.scrollsToTop = YES;
            modeButton.userInteractionEnabled = NO;
            
            [_mainScrollView scrollRectToVisible:CGRectMake(SCREEN_WIDTH*mode, 0, SCREEN_WIDTH, _mainScrollView.frame.size.height) animated:YES];
            
            if(animated) {
                [UIView animateWithDuration:0.3 animations:^{
                    _modeIndicatorImageView.center = CGPointMake((SCREEN_WIDTH/4)+(SCREEN_WIDTH/2)*mode, NAVI_BAR_HEIGHT-1);
                }];
            } else {
                _modeIndicatorImageView.center = CGPointMake((SCREEN_WIDTH/4)+(SCREEN_WIDTH/2)*mode, NAVI_BAR_HEIGHT-1);
            }
            
        }else {
            
            tableView.scrollsToTop = NO;
            modeButton.userInteractionEnabled = YES;
        }
    }
}


#pragma mark UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // to avoid refresh action go this way
    if([_tableViewArray containsObject:scrollView]) return;
    
    if(scrollView.contentOffset.x==0) {
        [self switchToMode:0 animated:YES];
    }else if(scrollView.contentOffset.x==SCREEN_WIDTH) {
        [self switchToMode:1 animated:YES];
    }else if(scrollView.contentOffset.x==SCREEN_WIDTH*2) {
        [self switchToMode:2 animated:YES];
    }
}


/* UI Settings */
-(void) tableViewSettings:(UITableView*)tableView
{
    tableView.delegate = self;
    tableView.dataSource = self;
    
    if([tableView isEqual:_dayTableView]){
        
        [tableView registerClass:[GiftBoardCell class] forCellReuseIdentifier:@"FriendNotifCell"];
        
    } else if([tableView isEqual:_totalTableView]){
        
        [tableView registerClass:[GiftBoardCell class] forCellReuseIdentifier:@"NotifCell"];
        
    }else{
        [tableView registerClass:[GiftBoardCell class] forCellReuseIdentifier:@"SystemNotifCell"];
    }
}

//-(void)didClickFindFriend:(id)sender
//{
//    FindFriendViewController* findFriendVC = [FindFriendViewController new];
//    findFriendVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:findFriendVC animated:YES];
//}


@end
