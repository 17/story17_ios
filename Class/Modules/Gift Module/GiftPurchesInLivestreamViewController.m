//
//  GiftPurchesInLivestreamViewController.m
//  Story17
//
//  Created by POPO on 2015/11/13.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GiftPurchesInLivestreamViewController.h"
#import "Constant.h"
#define leftmargin 0.08*SCREEN_WIDTH
#define imageHeight 0.1733*SCREEN_WIDTH
#define imageMargin 0.032*SCREEN_WIDTH
#define labelHeight 20
#define bottomposition 135

@implementation GiftPurchesInLivestreamViewController
- (id)initWithArray:(NSArray *)gift
{
    self = [super init];
    
    if(self){
        _giftArray=CREATE_MUTABLE_ARRAY;
        //        _giftArray=[NSMutableArray arrayWithArray:g];
    }
    return self;
}

-(id)init
{
    self=[super init];
    if (self) {
        //        _giftArray=CREATE_MUTABLE_ARRAY;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //    _purchaseManager = [IAPManager new];
    _purchaseManager=[SINGLETON iapManager];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _purchaseManager.delegate1=self;
    
    _mainScrollView = [ThemeManager getBouncedScrollView];
    _mainScrollView.delegate = self;
    _mainScrollView.pagingEnabled = NO;
    [self.view addSubview:_mainScrollView];
    UILabel* aaa=[[UILabel alloc]init];
    aaa.frame=CGRectMake(20, 100, 200, 100);
    [aaa setBackgroundColor:RED_COLOR];
    [aaa setText:[NSString stringWithFormat:@"%lu",(unsigned long)[_giftArray count]]];
    _progressView = [UIActivityIndicatorView new];
    _progressView.frame=CGRectMake(SCREEN_WIDTH/9*8/2-15, 150, 30, 30);
    [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [_progressView setColor:WHITE_COLOR];
    [self.view addSubview:_progressView];
    [_progressView startAnimating];
    
    [API_MANAGER getGiftListWithCompletion:^(BOOL success, NSArray *gifts) {
        [_progressView stopAnimating];
        _giftArray=[gifts copy];
        int row;
        if ([_giftArray count]>4*4) {
            row=4;
        }else{
            row=(int)[_giftArray count]/4+1;
        }
        if (IS_IPHONE_4) {
            _mainScrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH/9*8, (imageHeight+labelHeight+labelHeight+10)*row-80);
        }else{
            _mainScrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH/9*8, (imageHeight+labelHeight+labelHeight+10)*row-20);
        }
        
        float nextRowY=0;
        float nowRowY=0;
        for (int index=0;index<[gifts count]; index++) {
//        for (int index=0;index<[_giftArray count]; index++) {
        if (index%4==0&&index>0) {
                nowRowY=nowRowY+nextRowY+(imageHeight+labelHeight)+3;
                nextRowY=0;
            }
            GiftObject* a=[gifts objectAtIndex:index];
//            GiftObject* a=[_giftArray objectAtIndex:index];

            UIButton* gift=[[UIButton alloc]initWithFrame:CGRectMake(10, 10, imageHeight, imageHeight)];
            gift.center=CGPointMake(leftmargin+20+(imageHeight+imageMargin)*(index%4), imageHeight/2+10+nowRowY);
            if (FILE_EXIST(a.picture)) {
                [gift setBackgroundImage:[UIImage imageWithContentsOfFile:GET_LOCAL_FILE_PATH(a.picture)] forState:UIControlStateNormal];
            }
//            DLog(@"%@",[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)objectAtIndex:0]);
            gift.tag=index;
            [gift addTarget:self action:@selector(didClickGift:) forControlEvents:UIControlEventTouchUpInside];
            [self.mainScrollView addSubview:gift];
            
            UILabel* giftName=[[UILabel alloc]initWithFrame:CGRectMake(20, 20, 0.1733*SCREEN_WIDTH+imageMargin, labelHeight)];
//            [giftName setLineBreakMode:NSLineBreakByWordWrapping];
            [giftName setFont:SYSTEM_FONT_WITH_SIZE(14)];
            [giftName setText:[NSString stringWithFormat:@"%@",a.name]];
            giftName.numberOfLines=2;
            [giftName setAdjustsFontSizeToFitWidth:YES];

            CGSize size=[giftName sizeThatFits:CGSizeMake(0.1733*SCREEN_WIDTH+10, 50)];


//            [giftName sizeToFit];
            [giftName setFrame:CGRectMake(0, 0, size.width, size.height)];
            
            
//            [giftName sizeThatFits:CGSizeMake(0.1733*SCREEN_WIDTH+10, 50)];
            [giftName setTextColor:WHITE_COLOR];
            giftName.textAlignment=NSTextAlignmentCenter;
            giftName.center=CGPointMake(gift.frame.origin.x+imageHeight/2, gift.frame.origin.y+giftName.frame.size.height/2+imageHeight);
            if (giftName.frame.size.height>nextRowY) {
                nextRowY=giftName.frame.size.height;
            }
            [self.mainScrollView addSubview:giftName];
            
            UILabel* giftCharge=[[UILabel alloc]initWithFrame:CGRectMake(20, 20,0.1733*SCREEN_WIDTH+10 , 15)];
            [giftCharge setText:[NSString stringWithFormat:@"%d",a.point]];
            //            [giftCharge sizeToFit];
            [giftCharge setTextColor:WHITE_COLOR];
            [giftCharge setFont:SYSTEM_FONT_WITH_SIZE(14)];
            giftCharge.textAlignment=NSTextAlignmentCenter;
            giftCharge.center=CGPointMake(gift.frame.origin.x+imageHeight/2, giftName.frame.size.height+giftCharge.frame.size.height/2+giftName.frame.origin.y);
            [self.mainScrollView addSubview:giftCharge];
            
            
        }
        
        //    [aaa setText:[NSString stringWithFormat:@"%f",nowRowY]];
        //    [self.view addSubview:aaa];
        
        
        [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH/9*8, nowRowY+imageHeight+nextRowY+labelHeight+10)];
        
        
        UIView* line1=[ThemeManager separaterLine];
        if (IS_IPHONE_4) {
            [line1 setFrame:CGRectMake(5, _mainScrollView.frame.size.height, SCREEN_WIDTH/9*8-10, 0.5f)];
        }else{
            [line1 setFrame:CGRectMake(5, row*(imageHeight+labelHeight+labelHeight+10)-20, SCREEN_WIDTH/9*8-10, 0.5f)];

        }
        [self.view addSubview:line1];
        
        
        UILabel* nowpoint=[[UILabel alloc]initWithFrame:CGRectMake(20, 20, 20, 20)];
        [nowpoint setText:LOCALIZE(@"now_point")];
        [nowpoint setFont:SYSTEM_FONT_WITH_SIZE(18)];

        [nowpoint sizeToFit];
        [nowpoint setTextColor:WHITE_COLOR];
        if (IS_IPHONE_4) {
            nowpoint.center=CGPointMake(SCREEN_WIDTH/6*2, _mainScrollView.frame.size.height+20);
        }else{
            nowpoint.center=CGPointMake(SCREEN_WIDTH/6*2, (imageHeight+10+labelHeight+labelHeight)*row+17-10);
        }
        nowpoint.frame=CGRectMake(10, nowpoint.frame.origin.y, nowpoint.frame.size.width, nowpoint.frame.size.height);
        [self.view addSubview:nowpoint];
        
        _heart=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"giftpoint_5"]];
        [_heart setFrame:CGRectMake(0, 0, 13, 15)];
        if (IS_IPHONE_4) {
            _heart.center=CGPointMake(10+nowpoint.frame.size.width+11,nowpoint.center.y);
        }else{
            _heart.center=CGPointMake(10+nowpoint.frame.size.width+11, (imageHeight+10+labelHeight+labelHeight)*row+17-10);
        }
        [self.view addSubview:_heart];
        
        _nowPointNumber=[[UILabel alloc]initWithFrame:CGRectMake(20, 20, 20, 20)];
        [_nowPointNumber setFont:SYSTEM_FONT_WITH_SIZE(16)];
        
        [_nowPointNumber setTextColor:WHITE_COLOR];
        
        [_nowPointNumber setText:[NSString stringWithFormat:@"%@",GET_DEFAULT(MY_POINT)]] ;
        [_nowPointNumber sizeToFit];
        if (IS_IPHONE_4) {
            _nowPointNumber.center=CGPointMake(_heart.frame.origin.x+25 +_nowPointNumber.frame.size.width/2, nowpoint.center.y);
        }else{
            _nowPointNumber.center=CGPointMake(_heart.frame.origin.x+25 +_nowPointNumber.frame.size.width/2, (imageHeight+10+labelHeight+labelHeight)*row+17-10);
        }
        [self.view addSubview:_nowPointNumber];
        
        if (IS_IPHONE_4) {
            _buyPoint=[[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/9*8-30, nowpoint.frame.origin.y-3, 75, 30)];
        }else{
            _buyPoint=[[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/9*8-80, nowpoint.frame.origin.y-3, 75, 40)];
        }
        [_buyPoint setBackgroundImage:[[UIImage imageNamed:@"btn_yellow"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
        [_buyPoint setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
        [_buyPoint setTitle:LOCALIZE(@"Refill") forState:UIControlStateNormal];
        _buyPoint.titleLabel.font=[UIFont boldSystemFontOfSize:16];
        _buyPoint.center=CGPointMake(SCREEN_WIDTH/9*8-62, nowpoint.center.y);
        [_buyPoint addTarget:self action:@selector(showProduct) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_buyPoint];
    }];
    
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_nowPointNumber setText:[NSString stringWithFormat:@"%@",GET_DEFAULT(MY_POINT)]] ;
    [_nowPointNumber sizeToFit];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _purchaseManager.delegate1=nil;
    
    for(UIView* subView in [self.view subviews]){
        [subView removeFromSuperview];
    }
    //    [self.view removeFromSuperview];
}

-(void)pointRefresh
{
    
    [_nowPointNumber setText:GET_DEFAULT(MY_POINT)];
    [_nowPointNumber sizeToFit];
    _nowPointNumber.center=CGPointMake(_heart.frame.origin.x+25 +_nowPointNumber.frame.size.width/2, bottomposition);
    
}

-(void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - actions

-(void)didClickGift:(UIButton*)sender
{
    
    dispatch_async(MAIN_QUEUE, ^{
        UserGiftObject* gift = [UserGiftObject new];
        gift.gift = [_giftArray objectAtIndex:sender.tag];
        gift.user = [SINGLETON getSelfUserObject];
        
        if ([GET_DEFAULT(MY_POINT) intValue]>=gift.gift.point) {
            [DIALOG_MANAGER showGiftConfirm:gift.gift withCompletion:^(BOOL okclick) {
                if (okclick) {
                    //                    [_delegate giftAnimatePreviewStop];
                    [_delegate buyAgift:gift notEnough:NO];
                    [self dismissViewControllerAnimated:YES completion:^{
                        
                    }];
                    
                }else{
                    //                    [_delegate giftbuyViewPopOver];
                }
            }];
        } else {
            [DIALOG_MANAGER showLoadingView];
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
            
            [_delegate buyAgift:gift notEnough:YES];
            
        }
        
    });
}

-(void)showProduct
{
    [DIALOG_MANAGER showLoadingView];
    [self dismissViewControllerAnimated:YES completion:^{
        [_purchaseManager getProductList];
    }];
}

@end
