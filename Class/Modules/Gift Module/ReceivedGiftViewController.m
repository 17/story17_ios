//
//  ReceivedGiftViewController.m
//  Story17
//
//  Created by Racing on 2015/12/23.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "ReceivedGiftViewController.h"
#import "GiftHistoryViewController.h"
#import "GiftLeaderboardViewController.h"
#import "Constant.h"
#import "LiveStreamObject.h"
#import "MyScene.h"

@interface ReceivedGiftViewController () <UIScrollViewDelegate, GiftHistoryViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIView *streamerModeView;
@property (strong, nonatomic) IBOutlet UIView *watcherModeView;
@property (strong, nonatomic) IBOutlet UIButton *leaderboardButton;
@property (strong, nonatomic) IBOutlet UIButton *historyButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *indicatorView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *indicatorLC;
@property (strong, nonatomic) MyScene* animateScene;
@property (strong, nonatomic) SKView* skView;
@property (strong, nonatomic) NSArray *tabButtons;
@property (strong, nonatomic) NSArray *subViewControllers;

- (IBAction)onBack:(id)sender;
- (IBAction)onLeaderboard:(id)sender;
- (IBAction)onHistory:(id)sender;

@end

@implementation ReceivedGiftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _contentView.layer.cornerRadius = 10;
    _contentView.clipsToBounds = YES;
    
    if ([_liveStreamObj.user.userID isEqualToString:MY_USER_ID]) {
        // Streamer UI
        _titleLabel.text = LOCALIZE(@"Received Gift");
        
        self.tabButtons = @[_leaderboardButton, _historyButton];
        
        [_leaderboardButton setTitle:LOCALIZE(@"Leaderboard") forState:UIControlStateNormal];
        [_historyButton setTitle:LOCALIZE(@"Gift Box") forState:UIControlStateNormal];
        
        _skView = [[SKView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _skView.allowsTransparency = YES;
        _skView.backgroundColor = [UIColor clearColor];
        _skView.userInteractionEnabled = YES;
        _skView.hidden = YES;
        [self.view addSubview:_skView];
        
        _animateScene = [MyScene sceneWithSize:_skView.bounds.size];
        _animateScene.scaleMode = SKSceneScaleModeResizeFill;
        [_skView presentScene:_animateScene];
        
        GiftLeaderboardViewController *leaderboardVC = [[GiftLeaderboardViewController alloc] init];
        leaderboardVC.liveStreamObj = _liveStreamObj;
        GiftHistoryViewController *historyVC = [[GiftHistoryViewController alloc] init];
        historyVC.liveStreamObj = _liveStreamObj;
        historyVC.delegate = self;
        
        self.subViewControllers = @[leaderboardVC, historyVC];
        
        self.streamerModeView.hidden = NO;
        self.watcherModeView.hidden = YES;
    }
    else {
        // Watcher UI
        _titleLabel.text = LOCALIZE(@"Gift Leaderboard");
        
        GiftLeaderboardViewController *leaderboardVC = [[GiftLeaderboardViewController alloc] init];
        leaderboardVC.liveStreamObj = _liveStreamObj;
        
        self.subViewControllers = @[leaderboardVC];
        
        self.streamerModeView.hidden = YES;
        self.watcherModeView.hidden = NO;
    }

}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([_liveStreamObj.user.userID isEqualToString:MY_USER_ID]) {
        CGFloat width = CGRectGetWidth(_scrollView.frame);
        CGFloat height = CGRectGetHeight(_scrollView.frame);
        _scrollView.contentSize = CGSizeMake(_tabButtons.count * width, height);
        
        [_subViewControllers enumerateObjectsUsingBlock:^(UIViewController *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.view.frame = CGRectMake(idx*width, 0, width, height);
            [_scrollView addSubview:obj.view];
            
            [self addChildViewController:obj];
        }];
    }
    else {
        if (_subViewControllers.count <= 0) {
            return;
        }
        
        UIViewController *vc = _subViewControllers[0];

        vc.view.frame = CGRectMake(0, 0, CGRectGetWidth(_watcherModeView.frame), CGRectGetHeight(_watcherModeView.frame));
        [_watcherModeView addSubview:vc.view];
        [self addChildViewController:vc];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollToIndex:(NSUInteger)index
{
    UIViewController *vc = _subViewControllers[index];
    [_scrollView scrollRectToVisible:vc.view.frame animated:YES];
    
    [_tabButtons enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.selected = (idx == index) ? YES : NO;
    }];
}

-(void)showGiftAnimate:(GiftObject *)gift
{
    [_skView presentScene:_animateScene];
    _skView.hidden = NO;

    NSMutableArray* giftAnimates = [NSMutableArray array];
    
    for (int i = 1; i <= gift.numOfImages; i++) {
        NSString *rootPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
        NSRange dirNameRange = NSMakeRange(0, gift.archiveFileName.length-4);
        NSString *path = [NSString stringWithFormat:@"%@/animatePng/%@/%@_%d", rootPath, [gift.archiveFileName substringWithRange:dirNameRange], gift.giftID, i];
        
        if ([[NSFileManager defaultManager] isReadableFileAtPath:path]) {
            [giftAnimates addObject:path];
            DLog(@"path exist :%@",path);
        }
        else {
            DLog(@"path not exist :%@",path);
            break;
        }
        //        DLog(@"==== %d", gift.gift.frameDuration);
    }
    
    [_animateScene showAnimate:CGPointMake(SCREEN_WIDTH, SCREEN_HEIGHT)
                      imgarray:giftAnimates
               perFramDuraiton:(float)gift.frameDuration/1000
                startPoint:0 withCompletion:^(BOOL Done) {
                    _skView.hidden = YES;
                }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    _indicatorLC.constant = scrollView.contentOffset.x / _tabButtons.count;
    [_indicatorView layoutIfNeeded];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSUInteger currIndex = scrollView.contentOffset.x / _scrollView.frame.size.width;
    [_tabButtons enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.selected = (idx == currIndex) ? YES : NO;
    }];
}

#pragma mark - GiftHistoryViewControllerDelegate

- (void)giftHistoryViewController:(GiftHistoryViewController *)giftHistoryViewController didClickGift:(GiftObject *)gift
{
    [self showGiftAnimate:gift];
}

#pragma mark - Actions

- (IBAction)onBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onLeaderboard:(id)sender {
    [self scrollToIndex:0];
}

- (IBAction)onHistory:(id)sender {
    [self scrollToIndex:1];
}
@end
