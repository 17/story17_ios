//
//  LivestreamGiftLeaderboardObject.h
//  Story17
//
//  Created by Racing on 2015/12/15.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LiveStreamGiftLeaderboardObject : NSObject
@property (assign, nonatomic) int myPoint;
@property (assign, nonatomic) int totalPoint;
@property (copy, nonatomic) NSArray *contributors;

+ (LiveStreamGiftLeaderboardObject *)getLiveStreamLeaderboardWithResult:(NSDictionary*)results;
@end
