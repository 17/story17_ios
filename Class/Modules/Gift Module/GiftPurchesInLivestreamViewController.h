//
//  GiftPurchesInLivestreamViewController.h
//  Story17
//
//  Created by POPO on 2015/11/13.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

//
//  EditProfileViewController.h
//  Story17
//
//  Created by POPO on 2015/5/8.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IAPManager.h"
#import "UserGiftObject.h"
@protocol GiftPurchaseInLive <NSObject>
-(void)buyAgift:(UserGiftObject*)usergift notEnough:(BOOL)notEnough;
-(void)showAnimate:(GiftObject*)gift dismiss:(BOOL)dismiss;
-(void)giftbuyViewPopOver;
-(void)giftAnimatePreviewStop;
@end

@interface GiftPurchesInLivestreamViewController : UIViewController<IAPManagerDelegate1,UIScrollViewDelegate>
@property (nonatomic, strong) IAPManager* purchaseManager;
@property (nonatomic, weak) id<GiftPurchaseInLive> delegate;
@property (nonatomic,strong) UILabel* nowPointNumber;
@property (nonatomic,strong) UIImageView * heart;
@property (nonatomic,strong) NSArray* giftArray;
@property (nonatomic,strong) UIScrollView* mainScrollView;
@property (nonatomic,strong) UIActivityIndicatorView* progressView;
@property (nonatomic,strong) UIButton* buyPoint;

-(id)initWithArray:(NSArray*)gift;
-(void)dismiss;

@end
