//
//  GiftLeaderboardViewController.m
//  Story17
//
//  Created by Racing on 2015/12/14.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "GiftLeaderboardViewController.h"
#import "GiftLeaderboardTableViewCell.h"
#import "Constant.h"
#import "UIImage+AFNetworking.h"
#import "ContributorObject.h"

static NSString * const kReuseCellFirst = @"GiftLeaderboardTableViewCell";
static NSString * const kReuseCellSecond = @"GiftLeaderboardSecondTableViewCell";
static NSString * const kReuseCellThird = @"GiftLeaderboardThirdTableViewCell";
static NSString * const kReuseCellSmall = @"GiftLeaderboardSmallTableViewCell";
static CGFloat const kLargeCellHeight = 150;
static CGFloat const kMidCellHeight = 120;
static CGFloat const kSmallCellHeight = 60;
static CGFloat const kQueryThreshold = 120;
static int const kQueryCount = 10;

@interface GiftLeaderboardViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *targetLabel;
@property (strong, nonatomic) IBOutlet UILabel *contributionLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalContributionLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *watcherHeader;
@property (strong, nonatomic) IBOutlet UIView *streamerHeader;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) NSMutableArray *contributors;
@property (strong, nonatomic) LiveStreamGiftLeaderboardObject *liveStreamGiftLeaderboard;
@property (assign, nonatomic) int currentOffset;

@end

@implementation GiftLeaderboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_tableView registerNib:[UINib nibWithNibName:kReuseCellFirst bundle:nil] forCellReuseIdentifier:kReuseCellFirst];
    [_tableView registerNib:[UINib nibWithNibName:kReuseCellSecond bundle:nil] forCellReuseIdentifier:kReuseCellSecond];
    [_tableView registerNib:[UINib nibWithNibName:kReuseCellThird bundle:nil] forCellReuseIdentifier:kReuseCellThird];
    [_tableView registerNib:[UINib nibWithNibName:kReuseCellSmall bundle:nil] forCellReuseIdentifier:kReuseCellSmall];
    
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _profileImageView.layer.cornerRadius = _profileImageView.frame.size.width / 2;
    _profileImageView.clipsToBounds = YES;
    
    self.contributors = [NSMutableArray array];
    
    self.currentOffset = 0;
    
    [self queryLiveStreamLeaderboardWithCount:kQueryCount andOffset:_currentOffset];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertContributorsAtRange:(NSRange)range
{
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (NSUInteger i = range.location; i < range.location+range.length; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPaths addObject:indexPath];
    }
    
    [_tableView beginUpdates];
    [_tableView insertRowsAtIndexPaths:[indexPaths copy] withRowAnimation:UITableViewRowAnimationNone];
    [_tableView endUpdates];
}

- (void)showEmptyTips
{
    // Display a message when the table is empty
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    
    messageLabel.text = LOCALIZE(@"Livestreamer doesn't receive any gifts.");
    messageLabel.textColor = [UIColor colorFromHexString:@"#999999"];
    messageLabel.font = [UIFont systemFontOfSize:14];
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    [messageLabel sizeToFit];
    
    _tableView.backgroundView = messageLabel;
}

- (void)queryLiveStreamLeaderboardWithCount:(int)count andOffset:(int)offset
{
    [API_MANAGER getLiveStreamGiftLeaderboardByUserId:GET_DEFAULT(USER_ID) liveStreamId:_liveStreamObj.liveStreamID withCount:count andOffset:offset withCompletion:^(BOOL success, LiveStreamGiftLeaderboardObject *liveStreamGiftLeaderboard) {
        if (success) {
            self.liveStreamGiftLeaderboard = liveStreamGiftLeaderboard;
            int contributorCount = (int)liveStreamGiftLeaderboard.contributors.count;
            self.currentOffset = offset + contributorCount;
            
            if ([_liveStreamObj.user.userID isEqualToString:GET_DEFAULT(USER_ID)]) {
                [self setupTotalContribution];
                self.streamerHeader.hidden = NO;
            }
            else {
                [self setupProfile];
                self.watcherHeader.hidden = NO;

            }
            
            if (_contributors.count > 0 && contributorCount > 0) {
                // Second time query
                
                [_contributors addObjectsFromArray:liveStreamGiftLeaderboard.contributors];
                [self insertContributorsAtRange:NSMakeRange(offset, contributorCount)];
            }
            else if (contributorCount > 0){
                // First time query
                
                [_contributors addObjectsFromArray:liveStreamGiftLeaderboard.contributors];
                [_tableView reloadData];
            }
            else if (_contributors.count == 0) {
                // No query result
                
                [self showEmptyTips];
            }
            
            [_activityIndicator stopAnimating];
        }
    }];
}

- (void)setupProfile
{
    [_profileImageView setImageWithURL:S3_THUMB_IMAGE_URL(GET_DEFAULT(PICTURE)) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    
    NSDictionary *attrs = @{NSFontAttributeName:[UIFont systemFontOfSize:14],
                            NSForegroundColorAttributeName:[UIColor colorFromHexString:@"#999999"]
                            };
    NSDictionary *subAttrs = @{
                               NSFontAttributeName:[UIFont boldSystemFontOfSize:14],
                               NSForegroundColorAttributeName:[UIColor colorFromHexString:@"#4d4d4d"]
                               };
    
    NSString *localizedString = [NSString stringWithFormat:LOCALIZE(@"I gave %@"), _liveStreamObj.user.openID, _liveStreamGiftLeaderboard.myPoint];
    NSRange pointRange = [localizedString rangeOfString:[NSString stringWithFormat:@"%@", _liveStreamObj.user.openID]];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:localizedString attributes:attrs];
    [attributedText setAttributes:subAttrs range:pointRange];
    [_targetLabel setAttributedText:attributedText];
    
    
    float myPoint = (float)_liveStreamGiftLeaderboard.myPoint;
    float totalPoint = (float)_liveStreamGiftLeaderboard.totalPoint;
    
    if (myPoint > 0) {
        float percentage = myPoint / totalPoint * 100;
        
        if (percentage < 1) {
            _contributionLabel.text = [NSString stringWithFormat:@"%.2f%%", percentage];
        }
        else {
            _contributionLabel.text = [NSString stringWithFormat:@"%d%%", (int)percentage];
        }
        
    }
    else {
        _contributionLabel.text = @"0%";
    }

}

- (void)setupTotalContribution
{
    NSDictionary *attrs = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
    NSDictionary *subAttrs = @{
                               NSFontAttributeName:[UIFont boldSystemFontOfSize:20],
                               NSForegroundColorAttributeName:[UIColor colorFromHexString:@"#283774"]
                               };
    
    NSString *localizedString = [NSString stringWithFormat:LOCALIZE(@"Total %d"), _liveStreamGiftLeaderboard.totalPoint];
    NSRange pointRange = [localizedString rangeOfString:[NSString stringWithFormat:@"%d", _liveStreamGiftLeaderboard.totalPoint]];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:localizedString attributes:attrs];
    [attributedText setAttributes:subAttrs range:pointRange];
    [_totalContributionLabel setAttributedText:attributedText];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContributorObject *contributor = _contributors[indexPath.row];
    UserObject *user = contributor.user;
    float point = (float)contributor.point;
    float totalPoint = (float)_liveStreamGiftLeaderboard.totalPoint;
    
    GiftLeaderboardTableViewCell *cell;
    if (indexPath.row == 0) {
        cell = (GiftLeaderboardTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:kReuseCellFirst];
    }
    else if (indexPath.row == 1) {
        cell = (GiftLeaderboardTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:kReuseCellSecond];
    }
    else if (indexPath.row == 2) {
        cell = (GiftLeaderboardTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:kReuseCellThird];
    }
    else {
        cell = (GiftLeaderboardTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:kReuseCellSmall];
    }
    
    [cell.profileImageView setImageWithURL:S3_THUMB_IMAGE_URL(user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    cell.nameLabel.text = user.openID;

    if ([_liveStreamObj.user.userID isEqualToString:GET_DEFAULT(USER_ID)]) {
        cell.contributionLabel.text = [NSString stringWithFormat:@"%d", (int)point];
    }
    else {
        if (point > 0) {
            float percentage = point / totalPoint * 100;
            
            if (percentage < 1) {
                cell.contributionLabel.text = [NSString stringWithFormat:@"%.2f%%", percentage];
            }
            else {
                cell.contributionLabel.text = [NSString stringWithFormat:@"%d%%", (int)percentage];
            }
            
        }
        else {
            cell.contributionLabel.text = @"0%";
        }
        
    }

    
    cell.rankLabel.text = [NSString stringWithFormat:@"NO.%d", (int)indexPath.row+1];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _contributors.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 || indexPath.row == 1) {
        return kLargeCellHeight;
    }
    else if (indexPath.row == 2) {
        return kMidCellHeight;
    }
    else {
        return kSmallCellHeight;
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat actualPosition = scrollView.contentOffset.y;
    CGFloat contentHeight = scrollView.contentSize.height - _tableView.frame.size.height;
    if (actualPosition >= contentHeight - kQueryThreshold && _contributors.count >= _currentOffset) {
        
        int count = _currentOffset;
        self.currentOffset += kQueryCount;
        [self queryLiveStreamLeaderboardWithCount:kQueryCount andOffset:count];
    }
}


@end
