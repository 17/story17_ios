//
//  GiftManageViewController.m
//  Story17
//
//  Created by POPO on 11/10/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "GiftManageViewController.h"
#import "Constant.h"
#import "SVPullToRefresh.h"
#import "PointGainObject.h"
#import "GiftRecordCell.h"
#import "PointUsageLogObject.h"

@interface GiftManageViewController ()

#define purchaseCellIdentifier @"purchaseCellIdentifier"
#define usageCellIdentifier @"usageCellIdentifier"

#define introSectionHeight 50
#define btnHeight 60
//#define freePointBarHeight 35
#define freePointBarHeight 0

#define scrollViewHeight SCREEN_HEIGHT-introSectionHeight-btnHeight-freePointBarHeight-NAVI_BAR_HEIGHT
#define topViewHeight 30

@property BOOL needUpdate;
@property (nonatomic, strong) IAPManager* purchaseManager;
@property (nonatomic, strong) UIScrollView* mainScrollView;
@property (nonatomic, strong) UITableView* purchaseTableView;
@property (nonatomic, strong) UITableView* useTabelView;
@property (nonatomic, strong) NSMutableArray* pointGainObjArr;
@property (nonatomic, strong) NSMutableArray* pointUsageObjArr;
@property (nonatomic, strong) UIImageView* pointIntroImageView;
@property (nonatomic, strong) UILabel* pointIntroLabel;
@property (nonatomic, strong) UIImageView* nowPointImageView;
@property (nonatomic, strong) UIButton* purchaseButton;
@property (nonatomic,strong) UIButton* purchaseRecordBtn;
@property (nonatomic,strong) UIButton* usePointRecordBtn;
@property GiftManagerMode viewMode;
@property (nonatomic,strong) UIImageView* modeIndicatorImageView;
@end

@implementation GiftManageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addCustomNavigationBackButton];
    [self configureViewForIOS7];
    self.title = LOCALIZE(@"user_point");
    [self.view setBackgroundColor:[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0f]];
    _purchaseManager = [SINGLETON iapManager];
    _purchaseManager.delegate1=self;
    _pointIntroImageView = [[UIImageView alloc] initWithFrame:CGRectMake(35, 5, 30, 33)];
    _pointIntroImageView.image = [UIImage imageNamed:@"point_my"];
    [self.view addSubview:_pointIntroImageView];
    
    _pointIntroLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, introSectionHeight/2-10, 100, 20)];
    _pointIntroLabel.text = LOCALIZE(@"now_point");
    [self.view addSubview:_pointIntroLabel];
    
    _isFirstFetching=YES;
    
//    _purchaseButton = [ThemeManager getMainCircleBtn];
    _purchaseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _purchaseButton.frame = CGRectMake(SCREEN_WIDTH-50-20, introSectionHeight/2-20, 60, 40);
    _purchaseButton.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [_purchaseButton setTitle:LOCALIZE(@"purchase") forState:UIControlStateNormal];
    [_purchaseButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    [_purchaseButton setBackgroundImage:[[UIImage imageNamed:@"btn_green_2"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [_purchaseButton setBackgroundImage:[[UIImage imageNamed:@"btn_green_2_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
//    [_purchaseButton setBackgroundColor:RED_COLOR];
    [_purchaseButton addTarget:self action:@selector(didClickPurchase:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_purchaseButton];
//
    _nowPointLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    [_nowPointLabel setText:[NSString stringWithFormat:@"%@",GET_DEFAULT(MY_POINT)]];
    _nowPointLabel.font=SYSTEM_FONT_WITH_SIZE(14);
    _nowPointLabel.textColor=DARK_GRAY_COLOR;
    [_nowPointLabel sizeToFit];
    _nowPointLabel.frame=CGRectMake(_purchaseButton.frame.origin.x-_nowPointLabel.frame.size.width-10, introSectionHeight/2-10, _nowPointLabel.frame.size.width, _nowPointLabel.frame.size.height);
    [self.view addSubview:_nowPointLabel];
    
    _pointImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_nowPointLabel.frame.origin.x-25, introSectionHeight/2-10, 13, 15)];
    _pointImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"giftpoint_%d",rand()%7+1]];
    [self.view addSubview:_pointImageView];
    
    
    
    UIView* getFreePointView=[[UIView alloc]initWithFrame:CGRectMake(0, introSectionHeight, SCREEN_WIDTH, freePointBarHeight)];
    [getFreePointView setBackgroundColor:BLACK_COLOR];
    [self.view addSubview:getFreePointView];
    
    UIImageView* redbadge=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"badge_red"]];
    redbadge.frame=CGRectMake(SCREEN_WIDTH*2/5-15, 15, 7, 7);
    [getFreePointView addSubview:redbadge];
    
    UILabel* getFreePointLabel=[[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*2/5, 8, 150, 20 )];
    [getFreePointLabel setText:LOCALIZE(@"get_free_point")];
    [getFreePointLabel setTextColor:WHITE_COLOR];
    [getFreePointLabel setFont:SYSTEM_FONT_WITH_SIZE(16)];
    [getFreePointView addSubview:getFreePointLabel];
    getFreePointView.hidden=YES;
    UIView* Hline=[[UIView alloc]initWithFrame:CGRectMake(0, introSectionHeight, SCREEN_WIDTH, 2)];
    [Hline setBackgroundColor:WHITE_COLOR];
    [self.view addSubview:Hline];
//    float banner=0;
    
    
    _purchaseRecordBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, introSectionHeight+freePointBarHeight, SCREEN_WIDTH/2, btnHeight)];
    _purchaseRecordBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [_purchaseRecordBtn setTitle:LOCALIZE(@"purchase_log") forState:UIControlStateNormal];
    [_purchaseRecordBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_purchaseRecordBtn setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
    [_purchaseRecordBtn addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_purchaseRecordBtn];
    [_purchaseRecordBtn setSelected:YES];
    [_purchaseRecordBtn setBackgroundColor:WHITE_COLOR];
    
    _usePointRecordBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2, introSectionHeight+freePointBarHeight, SCREEN_WIDTH/2, btnHeight)];

    _usePointRecordBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [_usePointRecordBtn setTitle:LOCALIZE(@"use_log") forState:UIControlStateNormal];
    [_usePointRecordBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_usePointRecordBtn setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
    [_usePointRecordBtn addTarget:self action:@selector(changeModeAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_usePointRecordBtn];
    [_usePointRecordBtn setBackgroundColor:WHITE_COLOR];

    _modeIndicatorImageView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"bar")];
    _modeIndicatorImageView.frame = CGRectMake(0, introSectionHeight+freePointBarHeight+btnHeight-5, SCREEN_WIDTH/2, 5);
    _modeIndicatorImageView.layer.masksToBounds = YES;
    [self.view addSubview:_modeIndicatorImageView];
    
    _btnArray = @[_purchaseRecordBtn,_usePointRecordBtn];

    _mainScrollView = [ThemeManager getBouncedScrollView];
    _mainScrollView.frame = CGRectMake(0, introSectionHeight+btnHeight+freePointBarHeight, SCREEN_WIDTH, scrollViewHeight);
    _mainScrollView.delegate = self;
    [_mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH*2, _mainScrollView.frame.size.height)];

    _pointGainObjArr = CREATE_MUTABLE_ARRAY;
    _pointUsageObjArr = CREATE_MUTABLE_ARRAY;
    
//    for (int i=0; i<12; i++) {
//        PointGainObject* a = [PointGainObject getPointGainWithDict:nil];
//        [_pointGainObjArr addObject:a];
//    }

    
//    for (int i=0; i<2; i++) {
//        PointUsageLogObject* a = [PointUsageLogObject getPointUsageLogWithDict:nil];
//        a.receiverUser = [SINGLETON getSelfUserObject];
//        [_pointUsageObjArr addObject:a];
//    }

    __weak GiftManageViewController *weakSelf = self;
    _purchaseTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, scrollViewHeight-topViewHeight) style:UITableViewStylePlain];
    _purchaseTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _purchaseTableView.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0f];
    [_purchaseTableView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData:YES withMode:GiftManagerModePurchase];
    }];
    [_mainScrollView addSubview:_purchaseTableView];
    
    _topContainerView=[[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, topViewHeight)];
    
    [_topContainerView setBackgroundColor:[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0f]];
    UILabel* purchaseItemName=[[UILabel alloc]initWithFrame:CGRectMake(20, 5, 50, 20)];
    [purchaseItemName setText:LOCALIZE(@"buy_item")];
    purchaseItemName.font=SYSTEM_FONT_WITH_SIZE(14);
    [purchaseItemName sizeToFit];
    [purchaseItemName setTextColor:BLACK_COLOR];
    [_topContainerView addSubview:purchaseItemName];

    UILabel* receiverNameLabel=[[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*2/5, 5, 50, 20)];
    [receiverNameLabel setText:LOCALIZE(@"receiver_gift")];
    receiverNameLabel.font=SYSTEM_FONT_WITH_SIZE(14);
    [receiverNameLabel sizeToFit];
    [receiverNameLabel setTextColor:BLACK_COLOR];
    [_topContainerView addSubview:receiverNameLabel];
    
    [_mainScrollView addSubview:_topContainerView];
    
    
    
    
    _useTabelView = [[UITableView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH, topViewHeight, SCREEN_WIDTH, scrollViewHeight-topViewHeight-20) style:UITableViewStylePlain];
    _useTabelView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _useTabelView.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0f];
    [_useTabelView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData:YES withMode:GiftManagerModeUsageRecord];
    }];
    [_mainScrollView addSubview:_useTabelView];
    
    

    [self tableViewSettings:_purchaseTableView];
    [self tableViewSettings:_useTabelView];
    _tableViewArray = @[_purchaseTableView, _useTabelView];
    _tableDataArray = @[_pointGainObjArr, _pointUsageObjArr];
    [self.view addSubview:_mainScrollView];

    
    _noDataImageView = [ThemeManager getNoDataImageView];
    _noDataImageView2 = [ThemeManager getNoDataImageView];

    [_noDataImageView setImage:[UIImage imageNamed:@"nodata_buy"]];
    [_noDataImageView2 setImage:[UIImage imageNamed:@"nodata_buy"]];
    _noDataImageView2.frame = CGRectMake(SCREEN_WIDTH/2-165, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-280, 330, 330);
    _noDataImageView.frame = CGRectMake(SCREEN_WIDTH/2-165, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-280, 330, 330);

    CGRect rect = _noDataImageView.frame;
    _noDataImageView.frame = rect;
    rect.origin.x += SCREEN_WIDTH;
    _noDataImageView2.frame = rect;
    
    _noDataArray = @[_noDataImageView,_noDataImageView2];
    _progressView = [UIActivityIndicatorView new];
    _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView setColor:MAIN_COLOR];
    
    _progressView2 = [UIActivityIndicatorView new];
    _progressView2.frame = CGRectMake(SCREEN_WIDTH/2-15+SCREEN_WIDTH, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView2 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView2 setColor:MAIN_COLOR];
    _progressArray = @[_progressView,_progressView2];
    [_mainScrollView addSubview:_noDataImageView];
    [_mainScrollView addSubview:_noDataImageView2];
    [_mainScrollView addSubview:_progressView];
    [_mainScrollView addSubview:_progressView2];
    
    [self fetchData:true withMode:GiftManagerModeNone];

}

-(void)fetchData:(BOOL)refresh withMode:(GiftManagerMode)viewMode
{
    // todo
    /* update _pointPurchaseObjArr */
    if(refresh){
        _noMoreData = false;
    }

    if(_isFetching || _noMoreData) return;


    // getPointUsageLogFromUser

    int beforeTime = INT32_MAX;

    GiftManagerMode modeChange = (viewMode==GiftManagerModeNone)?_viewMode:viewMode;
    NSMutableArray* logArray = _tableDataArray[modeChange];
    UILabel* noDataView = _noDataArray[modeChange];
    UITableView* tableView = _tableViewArray[modeChange];
    UIActivityIndicatorView* progressView = _progressArray[modeChange];
    PointGainObject* lastPostObject;
    PointUsageLogObject* lastlogObject;
    
    
  
    
    if (modeChange==GiftManagerModePurchase) {
        
        if([logArray count]>0){
            lastPostObject = logArray[[logArray count]-1];
            
        }
        
        if(lastPostObject==NULL || refresh){
            beforeTime = INT32_MAX;
        }else{
            beforeTime = lastPostObject.timestamp;
        }
        if((viewMode==-1 || refresh) && !_isFirstFetching){
            [tableView.pullToRefreshView startAnimating];
        }
    }else{
        if([logArray count]>0){
            lastlogObject = logArray[[logArray count]-1];
            
        }
        
        if(lastlogObject==NULL || refresh){
            beforeTime = INT32_MAX;
        }else{
            beforeTime = lastlogObject.timestamp;
        }
        if((viewMode==-1 || refresh) && !_isFirstFetching){
            [tableView.pullToRefreshView startAnimating];
        }
   
    }
    if(_isFirstFetching){
        [progressView startAnimating];
        _isFirstFetching = false;
    }
    _isFetching = true;

    switch (modeChange) {
        case GiftManagerModePurchase:
        {
            
            [API_MANAGER getPointGainLogFromUser:[SINGLETON getSelfUserObject].userID beforeTime:beforeTime withCount:20 withCompletion:^(BOOL success, NSArray *pointGainLogs) {
                
            
                [tableView.pullToRefreshView stopAnimating];
                [progressView stopAnimating];
                if ([pointGainLogs count]==0&&[logArray count]==0) {
                    noDataView.hidden=NO;
                }else{
                    noDataView.hidden=YES;
                }
                if (success) {

                    if(refresh){
//                        tableView.hidden=NO;
                        [logArray removeAllObjects];
                        [logArray addObjectsFromArray:pointGainLogs];

                    }else{
                        [logArray addObjectsFromArray:pointGainLogs];
                    }
                    [tableView reloadData];

                    if ([pointGainLogs count]<20) {
                        _noMoreData=true;
                    }
                } else {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
                _isFetching=NO;
            }];
        }
            break;
            
        /* update _pointUsageObjArr */
        case GiftManagerModeUsageRecord:
        {
            [API_MANAGER getPointUsageLogFromUser:[SINGLETON getSelfUserObject].userID beforeTime:beforeTime withCount:20 withCompletion:^(BOOL success, NSArray *pointUsageLogs) {
                [progressView stopAnimating];
                if ([pointUsageLogs count]==0&&[logArray count]==0) {
                    noDataView.hidden=NO;
                }else{
                    noDataView.hidden=YES;
                }
                _isFetching=NO;
                [tableView.pullToRefreshView stopAnimating];

                if (success) {
                    if(refresh){
//                        tableView.hidden=NO;

                        [logArray removeAllObjects];
                        [logArray addObjectsFromArray:pointUsageLogs];

                    }else{
                        [logArray addObjectsFromArray:pointUsageLogs];
                    }

                    [tableView reloadData];
                    if ([pointUsageLogs count]<20) {
                        _noMoreData=true;
                    }
                } else {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
        }
            break;
            
        default:
            break;
    }

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:_purchaseTableView]) {
        return [_pointGainObjArr count];
    } else {
        return [_pointUsageObjArr count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:_purchaseTableView]) {
        GiftRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:purchaseCellIdentifier];
        PointGainObject *purchaseLog = [_pointGainObjArr objectAtIndex:indexPath.row];

        if (cell == nil) {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:purchaseCellIdentifier];
            cell = [[GiftRecordCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:purchaseCellIdentifier];

        }
        cell.userInteractionEnabled = NO;

        switch (purchaseLog.type) {
            case PointGainTypePurchase:
                cell.itemLabel.text = LOCALIZE(@"point_purchase");
                break;
            case PointGainTypeFree:
                cell.itemLabel.text = LOCALIZE(@"FreeGaint");
                break;
            case PointGainTypeUnknow:
                cell.itemLabel.text = @"Unknow";
            default:
                break;
        }
        cell.pointLabel.text=[NSString stringWithFormat:@"%d",purchaseLog.point];
        cell.targetUserImageView.hidden=YES;
        cell.targetUserLabel.hidden=YES;
        //            [cell.textLabel setTextColor:GRAY_COLOR];
//        purchaseLog.timestamp=CURRENT_TIMESTAMP;
        NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
        [_formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:purchaseLog.timestamp];

        DLog(@"%@",[_formatter stringFromDate:date]);
        cell.dateLabel.text = [_formatter stringFromDate:date];
        [cell setUser:@"RTT" targetUserPicture:@"123"];

//        cell.dateLabel.text = [NSString stringWithFormat:@"%d", purchaseLog.timestamp];
        //            [cell.itemLabel setTextColor:GRAY_COLOR];
        //            cell.pointLabel.text = @"30";
        if(indexPath.row>[_pointGainObjArr count]-20/3){
            [self fetchData:false withMode:GiftManagerModePurchase];
        }
        return cell;
    } else {
        GiftRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:usageCellIdentifier];
        if (cell == nil) {
            //            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:purchaseCellIdentifier];
            cell = [[GiftRecordCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:usageCellIdentifier];
        }
            cell.userInteractionEnabled = NO;

            PointUsageLogObject *pointUsageLog = [_pointUsageObjArr objectAtIndex:indexPath.row];
            cell.itemLabel.text = pointUsageLog.receivedGiftUser.gift.name;
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:pointUsageLog.timestamp];

//            pointUsageLog.timestamp=CURRENT_TIMESTAMP;
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setDateFormat:@"yyyy-MM-dd"];
           DLog(@"%@",[_formatter stringFromDate:date]);
           cell.dateLabel.text = [_formatter stringFromDate:date];
        cell.pointLabel.text=[NSString stringWithFormat:@"%d",pointUsageLog.point];

//            cell.dateLabel.text = [NSString stringWithFormat:@"%d", pointUsageLog.timestamp];
            [cell.targetUserLabel setText:pointUsageLog.receivedGiftUser.user.openID];
            //            [cell.itemLabel setTextColor:GRAY_COLOR];
            //            cell.pointLabel.text = @"30";
        [cell setUser:@"RTT" targetUserPicture:pointUsageLog.receivedGiftUser.user.picture];
      
        if(indexPath.row>[_pointUsageObjArr count]-20/3){
            [self fetchData:false withMode:GiftManagerModeUsageRecord];
        }
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 65;
}


- (void)didClickPurchase:(id)sender
{
    [DIALOG_MANAGER showLoadingView];
    _purchaseManager.remainingWarning=NO;
    DLog(@"%@",GET_DEFAULT(GIFTMODULESTATE));
    if ([GET_DEFAULT(GIFTMODULESTATE) isEqual:@1]) {
        [_purchaseManager getProductList];
    }else if([GET_DEFAULT(GIFTMODULESTATE) isEqual:@2]){
        [DIALOG_MANAGER hideLoadingView];
        [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"pleaseupdateNewApp") message:@"" buttonText:LOCALIZE(@"goToappstore") cancelable:NO withCompletion:^(BOOL okClicked) {
            if (okClicked) {
                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")){
                    SFSafariViewController *safariViewController = [[SFSafariViewController alloc]
                                                                    initWithURL:[NSURL URLWithString:GET_DEFAULT(UPDATE_APPLINK)]
                                                                    entersReaderIfAvailable:YES];
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [_navCtrl presentViewController:safariViewController animated:YES completion:nil];
                                       });
                }else{
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:GET_DEFAULT(UPDATE_APPLINK)]];
                }

            }
        }];
    }else if([GET_DEFAULT(GIFTMODULESTATE) isEqual:@0]){
        [DIALOG_MANAGER hideLoadingView];
        [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"giftmoduleClose") message:@"" buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
            if (okClicked) {
                
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _purchaseManager.delegate1=nil;
    
    if([GET_DEFAULT(UPDATE_NOTIF_TIME) intValue] < CURRENT_TIMESTAMP-UPDATE_INTERVAL){
        
        _needUpdate = YES;
//        [_updatedArray removeAllObjects];
        
        /* reload current mode */
        [self fetchData:true withMode:-1];
    }
    
}

-(void)changeModeAction:(id)sender
{
    if([sender isEqual:_purchaseRecordBtn]){
        [self switchToMode:GiftManagerModePurchase animated:YES];
    }else if ([sender isEqual:_usePointRecordBtn]){
        [self switchToMode:GiftManagerModeUsageRecord animated:YES];
  }
}

-(void) switchToMode:(GiftManagerMode)mode animated:(BOOL) animated
{
    if(_viewMode==mode) {
        return;
    }
    _viewMode = mode;
    NSMutableArray* notifs = _tableDataArray[mode];
    
    if(1){
        if((int)[notifs count]==0){
            _isFirstFetching = true;
        }
        _isFetching = false;
        [self fetchData:true withMode:_viewMode];
    }
    switch (_viewMode) {
        case GiftManagerModePurchase:
        {
            [_purchaseRecordBtn setSelected:YES];
            [_usePointRecordBtn setSelected:NO];
            break;
        }
        case GiftManagerModeUsageRecord:
        {
            [_purchaseRecordBtn setSelected:NO];
            [_usePointRecordBtn setSelected:YES];
            break;
        }

            
        default:
            break;
    }
    
    [self reloadUIAnimated:animated];
}


-(void) reloadUIAnimated:(BOOL)animated
{
    for (int mode=0; mode < 2; mode++) {
        UIButton* modeButton = _btnArray[mode];
        UIScrollView *tableView = _tableViewArray[mode];
        
        if(mode == _viewMode) {
            
            tableView.scrollsToTop = YES;
            modeButton.userInteractionEnabled = NO;
            
            [_mainScrollView scrollRectToVisible:CGRectMake(SCREEN_WIDTH*mode, 0, SCREEN_WIDTH, _mainScrollView.frame.size.height) animated:YES];
            
            if(animated) {
                [UIView animateWithDuration:0.2 animations:^{
                    if (mode==GiftManagerModePurchase) {
                        _modeIndicatorImageView.frame = CGRectMake(0, introSectionHeight+freePointBarHeight+btnHeight-5, SCREEN_WIDTH/2, 5);

                    }else{
                        _modeIndicatorImageView.frame = CGRectMake(SCREEN_WIDTH/2, introSectionHeight+freePointBarHeight+btnHeight-5, SCREEN_WIDTH/2, 5);

                    }

                    
                }];
            } else {
              
                
            }
            
        }else {
            
            tableView.scrollsToTop = NO;
            modeButton.userInteractionEnabled = YES;
        }
    }
}


-(void) tableViewSettings:(UITableView*)tableView
{
    tableView.delegate = self;
    tableView.dataSource = self;
//    tableView.userInteractionEnabled = NO;

//    
//    if([tableView isEqual:_purchaseTableView]){
//        
//        [tableView registerClass:[GiftRecordCell class] forCellReuseIdentifier:purchaseCellIdentifier];
//        
//    } else if([tableView isEqual:_useTabelView]){
//        
//        [tableView registerClass:[GiftRecordCell class] forCellReuseIdentifier:usageCellIdentifier];
//        
//    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // to avoid refresh action go this way
    if([_tableViewArray containsObject:scrollView]) return;
    
    if(scrollView.contentOffset.x==0) {
        [self switchToMode:GiftManagerModePurchase animated:YES];
    }else if(scrollView.contentOffset.x==SCREEN_WIDTH) {
        [self switchToMode:GiftManagerModeUsageRecord animated:YES];
    }
}


-(void)pointRefresh
{
    [_nowPointLabel setText:[NSString stringWithFormat:@"%@",GET_DEFAULT(MY_POINT)]];
    
    [_nowPointLabel sizeToFit];
    _nowPointLabel.frame=CGRectMake(_purchaseButton.frame.origin.x-_nowPointLabel.frame.size.width-10, introSectionHeight/2-10, _nowPointLabel.frame.size.width, _nowPointLabel.frame.size.height);
        _pointImageView.frame = CGRectMake(_nowPointLabel.frame.origin.x-25, introSectionHeight/2-10, 13, 15);
}


-(void)dealloc
{
    
}


@end
