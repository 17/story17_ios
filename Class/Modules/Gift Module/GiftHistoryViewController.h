//
//  GiftHistoryViewController.h
//  Story17
//
//  Created by Racing on 2015/12/23.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GiftHistoryViewController;
@class GiftObject;
@protocol GiftHistoryViewControllerDelegate <NSObject>
- (void)giftHistoryViewController:(GiftHistoryViewController *)giftHistoryViewController didClickGift:(GiftObject *)gift;

@end

@class LiveStreamObject;
@interface GiftHistoryViewController : UIViewController
@property (strong, nonatomic) LiveStreamObject *liveStreamObj;
@property (weak, nonatomic) id<GiftHistoryViewControllerDelegate> delegate;

@end
