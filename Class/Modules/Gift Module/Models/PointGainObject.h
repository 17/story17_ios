//
//  PointGainObject.h
//  Story17
//
//  Created by POPO on 11/12/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ProductObject;

@interface PointGainObject : NSObject

typedef NS_ENUM(NSInteger, PointGainType) {
    PointGainTypePurchase,
    PointGainTypeFree,
    PointGainTypeUnknow
};

@property (nonatomic, strong) ProductObject* product;
@property (nonatomic) int32_t point;
@property (nonatomic) int32_t timestamp;
@property (nonatomic) PointGainType type;

+ (PointGainObject*)getPointGainWithDict:(NSDictionary *)pointGainDict;

@end
