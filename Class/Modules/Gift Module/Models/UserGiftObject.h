//
//  UserGiftObject.h
//  Story17
//
//  Created by POPO on 11/25/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserObject;
@class GiftObject;

@interface UserGiftObject : NSObject

@property (nonatomic, strong) UserObject* user;
@property (nonatomic, strong) GiftObject* gift;
@property (nonatomic) int32_t timestamp;

+ (UserGiftObject*)getUserGiftWitDict:(NSDictionary*)userGiftInfoDict;

@end