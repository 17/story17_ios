//
//  GiftObject.m
//  Story17
//
//  Created by POPO on 11/20/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "GiftObject.h"
#import "Constant.h"
#import "SSZipArchive.h"

@interface GiftObject()


@end

@implementation GiftObject

+ (GiftObject*)getGiftWitDict:(NSDictionary*)giftInfoDict
{
    GiftObject* gift = [GiftObject new];
    
    gift.giftID = giftInfoDict[@"giftID"];
    gift.giftToken=[giftInfoDict[@"giftToken"] intValue];
    gift.point = [giftInfoDict[@"point"] intValue];
    gift.name = giftInfoDict[@"name"];
    gift.icon = giftInfoDict[@"icon"];
    gift.picture = giftInfoDict[@"picture"];
    gift.width = [giftInfoDict[@"width"] intValue];
    gift.height = [giftInfoDict[@"height"] intValue];
    gift.frameDuration = [giftInfoDict[@"frameDuration"] intValue];
    gift.numOfImages = [giftInfoDict[@"numOfImages"] intValue];
    
    gift.soundTrack = giftInfoDict[@"soundTrack"];
    gift.sequence = [giftInfoDict[@"sequence"] intValue];
    gift.archiveFileName = giftInfoDict[@"archiveFileName"];
    
    gift.leaderboardIcon = giftInfoDict[@"leaderboardIcon"];
    if(gift.archiveFileName==NULL){
        return NULL;
    }
    
    dispatch_async(GLOBAL_QUEUE, ^{
//        DLog(@"啦圖片前%@",gift.archiveFileName);
        if (!FILE_EXIST(gift.picture)) {
                NSData *urlData = [NSData dataWithContentsOfURL:S3_FILE_URL(gift.picture)];
                
                if (urlData) {
                    [urlData writeToFile:GET_LOCAL_FILE_PATH(gift.picture) atomically:YES];
                }
        }
        
        dispatch_async(GLOBAL_QUEUE, ^{
            //        DLog(@"啦圖片前%@",gift.archiveFileName);
            DLog(@"%@",GET_LOCAL_FILE_PATH(@"animatePng"));
            if (![gift.soundTrack isEqualToString:@""]) {
                if (!FILE_EXIST(gift.soundTrack)) {
                    NSData *urlData = [NSData dataWithContentsOfURL:S3_FILE_URL(gift.soundTrack)];
                    if (urlData) {
                        [urlData writeToFile:GET_LOCAL_FILE_PATH(gift.soundTrack) atomically:YES];
                    }
                }
            }else{
                DLog(@"soundTrack is nil");
  
            }
        });

    });

    return gift;
}

+ (void)getGiftZipFile:(NSArray*)gifts index:(int)index firstCatch:(BOOL)firstCatch
{
//    dispatch_async(GLOBAL_QUEUE, ^{
    
    if([gifts count] <= index)
        return;
    
    GiftObject* gift=[gifts objectAtIndex:index];

    
        if (!FILE_EXIST(gift.archiveFileName)) {
    
                if (GET_LOCAL_FILE_PATH(gift.archiveFileName)) {
//                    DLog(@"開始DownloadZip%@",gift.archiveFileName);
                    NSData *urlData = [NSData dataWithContentsOfURL:S3_FILE_URL(gift.archiveFileName)];
//                    DLog(@"DownloadZip%@",gift.archiveFileName);

//                    DLog(@"開始寫data%@",gift.archiveFileName);
                    if (urlData) {
    
                        [urlData writeToFile:GET_LOCAL_FILE_PATH(gift.archiveFileName) atomically:YES];
                    }
//                    DLog(@"寫完data%@",gift.archiveFileName);
    
                }
        }


    
        if (![[DEFAULTS arrayForKey:GIFT_ZIP] containsObject:gift.archiveFileName]) {
    
    
            DLog(@"unzip%@",gift.archiveFileName);
            DLog(@"%@",GET_LOCAL_FILE_PATH(@"animatePng"));
            if ([SSZipArchive unzipFileAtPath:GET_LOCAL_FILE_PATH(gift.archiveFileName) toDestination:GET_LOCAL_FILE_PATH(@"animatePng")]) {
                DLog(@"unzip 完成%@",gift.archiveFileName);
                
                NSMutableArray *giftZiped = [NSMutableArray arrayWithArray:[DEFAULTS arrayForKey:GIFT_ZIP]];
                [giftZiped addObject:gift.archiveFileName];
                [DEFAULTS setObject:giftZiped forKey:GIFT_ZIP];
                [DEFAULTS synchronize];
                DLog(@"%@",[DEFAULTS arrayForKey:GIFT_ZIP]);
                dispatch_async(dispatch_get_main_queue(), ^{

                    [NOTIFICATION_CENTER postNotificationName:GIFT_DOWNLOAD_COMPLETE object:nil];
                });
                if (index+1<[gifts count]&&firstCatch&&firstCatch&&[GET_DEFAULT(NOTLIVESTREAMING)isEqualToString:@"1"]) {
                    [self getGiftZipFile:gifts index:index+1 firstCatch:firstCatch];
                }
            } else {
                if (FILE_EXIST(gift.archiveFileName)) {
//                    DLog(@"檔案存在");
                }else{
                    DLog(@"檔案不存在");
                }
                DELETE_FILE(GET_LOCAL_FILE_PATH(gift.archiveFileName));
//                [self getGiftZipFile:gifts index:index firstCatch:firstCatch];
                if (index+1<[gifts count]&&firstCatch&&[GET_DEFAULT(NOTLIVESTREAMING)isEqualToString:@"1"]) {

                    [self getGiftZipFile:gifts index:index+1 firstCatch:firstCatch];
                }
            }
        }else{
            
//            DLog(@"%@",[DEFAULTS arrayForKey:GIFT_ZIP]);
            if (index+1<[gifts count]&&firstCatch&&[GET_DEFAULT(NOTLIVESTREAMING)isEqualToString:@"1"]) {
                [self getGiftZipFile:gifts index:index+1 firstCatch:firstCatch];
            }
        }
}


+ (void)getGiftZipFile:(GiftObject*)gift
{
    
    
    if (!FILE_EXIST(gift.archiveFileName)) {
        
        if (GET_LOCAL_FILE_PATH(gift.archiveFileName)) {
            //                    DLog(@"開始DownloadZip%@",gift.archiveFileName);
            NSData *urlData = [NSData dataWithContentsOfURL:S3_FILE_URL(gift.archiveFileName)];
            //                    DLog(@"DownloadZip%@",gift.archiveFileName);
            
            //                    DLog(@"開始寫data%@",gift.archiveFileName);
            if (urlData) {
                
                [urlData writeToFile:GET_LOCAL_FILE_PATH(gift.archiveFileName) atomically:YES];
            }
            //                    DLog(@"寫完data%@",gift.archiveFileName);
            
        }
    }
    
    
    
    if (![[DEFAULTS arrayForKey:GIFT_ZIP] containsObject:gift.archiveFileName]) {
        
        
        DLog(@"unzip%@",gift.archiveFileName);
        DLog(@"%@",GET_LOCAL_FILE_PATH(@"animatePng"));
        if ([SSZipArchive unzipFileAtPath:GET_LOCAL_FILE_PATH(gift.archiveFileName) toDestination:GET_LOCAL_FILE_PATH(@"animatePng")]) {
            DLog(@"unzip 完成%@",gift.archiveFileName);
            
            NSMutableArray *giftZiped = [NSMutableArray arrayWithArray:[DEFAULTS arrayForKey:GIFT_ZIP]];
            [giftZiped addObject:gift.archiveFileName];
            [DEFAULTS setObject:giftZiped forKey:GIFT_ZIP];
            [DEFAULTS synchronize];
            DLog(@"%@",[DEFAULTS arrayForKey:GIFT_ZIP]);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [NOTIFICATION_CENTER postNotificationName:GIFT_DOWNLOAD_COMPLETE object:nil];
            });
        } else {
            if (FILE_EXIST(gift.archiveFileName)) {
                //                    DLog(@"檔案存在");
            }else{
                DLog(@"檔案不存在");
            }
            DELETE_FILE(GET_LOCAL_FILE_PATH(gift.archiveFileName));
            //                [self getGiftZipFile:gifts index:index firstCatch:firstCatch];
        }
    }else{
        
        //            DLog(@"%@",[DEFAULTS arrayForKey:GIFT_ZIP]);
    }
}

@end
