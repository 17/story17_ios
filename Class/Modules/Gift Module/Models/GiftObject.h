//
//  GiftObject.h
//  Story17
//
//  Created by POPO on 11/20/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

@interface GiftObject : NSObject

@property (nonatomic, copy) NSString* giftID;
@property (nonatomic) int32_t point;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* icon;
@property (nonatomic, copy) NSString* picture;
@property (nonatomic, copy) NSString* soundTrack;
@property (nonatomic) int32_t sequence;
@property (nonatomic, copy) NSString* archiveFileName;
@property (nonatomic) int32_t width;
@property (nonatomic) int32_t height;
@property (nonatomic) int32_t frameDuration;
@property (nonatomic) int32_t numOfImages;
@property (nonatomic) int32_t giftToken;
@property (copy, nonatomic) NSString *leaderboardIcon;

+ (GiftObject*)getGiftWitDict:(NSDictionary*)giftInfoDict;
+ (void)getGiftZipFile:(NSArray*)gifts index:(int)index firstCatch:(BOOL)firstCatch;
+ (void)getGiftZipFile:(GiftObject*)gift;
@end
