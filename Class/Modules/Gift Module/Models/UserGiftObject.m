//
//  UserGiftObject.m
//  Story17
//
//  Created by POPO on 11/25/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "UserGiftObject.h"

#import "GiftObject.h"
#import "UserObject.h"

@implementation UserGiftObject

+ (UserGiftObject*)getUserGiftWitDict:(NSDictionary*)userGiftInfoDict;
{
    UserGiftObject* userGift = [UserGiftObject new];
    userGift.gift = [GiftObject getGiftWitDict:userGiftInfoDict[@"giftInfo"]];
    userGift.user = [UserObject getUserWithDict:userGiftInfoDict[@"userInfo"]];
    userGift.timestamp = [userGiftInfoDict[@"timestamp"] intValue];

    return userGift;
}

@end
