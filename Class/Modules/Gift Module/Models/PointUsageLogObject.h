//
//  GiftObject.h
//  Story17
//
//  Created by POPO on 11/17/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserGiftObject;

@interface PointUsageLogObject : NSObject

typedef NS_ENUM(NSInteger, PointUsageLogType) {
    PointUsageLogTypePurchaseGift,
    PointUsageLogTypeUnkown
};

@property (nonatomic) PointUsageLogType type;
@property (nonatomic) int32_t point;
@property (nonatomic, strong) UserGiftObject* receivedGiftUser;
@property (nonatomic) int32_t timestamp;

+ (PointUsageLogObject*)getPointUsageLogWithDict:(NSDictionary *)pointUsageLogDict;


@end
