//
//  ContributorObject.h
//  Story17
//
//  Created by Racing on 2015/12/15.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserObject;

@interface ContributorObject : NSObject
@property (strong, nonatomic) UserObject *user;
@property (assign, nonatomic) int point;

+ (ContributorObject *)getContributorWithResult:(NSDictionary*)results;
@end
