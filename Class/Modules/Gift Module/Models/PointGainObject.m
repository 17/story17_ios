//
//  PointGainObject.m
//  Story17
//
//  Created by POPO on 11/12/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "PointGainObject.h"

#import "ProductObject.h"

@implementation PointGainObject

+ (PointGainObject*)getPointGainWithDict:(NSDictionary *)pointGainDict
{
    PointGainObject* pointGain = [PointGainObject new];
    
    if ([pointGainDict[@"type"] isEqualToString:@"freePoint"]) {
        pointGain.type = PointGainTypeFree;
    } else if ([pointGainDict[@"type"] isEqualToString:@"purchase"]) {
        pointGain.type = PointGainTypePurchase;
    } else {
        pointGain.type = PointGainTypeUnknow;
    }
    pointGain.point = [pointGainDict[@"point"] intValue];
    pointGain.timestamp = [pointGainDict[@"timestamp"] intValue];
    pointGain.product = [ProductObject getProductWithDict:pointGainDict[@"productInfo"]];
    
    return pointGain;
}

@end