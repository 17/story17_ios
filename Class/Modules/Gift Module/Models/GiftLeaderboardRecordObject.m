//
//  GiftLeaderboardRecordObject.m
//  Story17
//
//  Created by POPO on 11/26/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "GiftLeaderboardRecordObject.h"

#import "UserObject.h"

@implementation GiftLeaderboardRecordObject

+ (GiftLeaderboardRecordObject*)getGiftLeaderboardRecordWitDict:(NSDictionary*)giftLeaderboardRecordInfoDict
{
    GiftLeaderboardRecordObject* giftLeaderboardRecord = [GiftLeaderboardRecordObject new];
    
    giftLeaderboardRecord.user = [UserObject getUserWithDict:giftLeaderboardRecordInfoDict[@"userInfo"]];
    giftLeaderboardRecord.point = [giftLeaderboardRecordInfoDict[@"point"] intValue];
    giftLeaderboardRecord.revenue = [giftLeaderboardRecordInfoDict[@"revenue"] floatValue];
    
    return giftLeaderboardRecord;
}

@end
