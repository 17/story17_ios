//
//  PointUsageLogObject.m
//  Story17
//
//  Created by POPO on 11/17/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "PointUsageLogObject.h"

#import "UserGiftObject.h"

@implementation PointUsageLogObject

+ (PointUsageLogObject*)getPointUsageLogWithDict:(NSDictionary *)pointUsageLogDict
{
    PointUsageLogObject* pointUsageLog = [PointUsageLogObject new];
    
    if ([pointUsageLogDict[@"type"] isEqualToString:@"purchaseGift"]) {
        pointUsageLog.type = PointUsageLogTypePurchaseGift;
    } else {
        pointUsageLog.type = PointUsageLogTypeUnkown;
    }
    pointUsageLog.point = [pointUsageLogDict[@"point"] intValue];;
    pointUsageLog.receivedGiftUser = [UserGiftObject getUserGiftWitDict:pointUsageLogDict];
    pointUsageLog.timestamp = [pointUsageLogDict[@"timestamp"] intValue];

    return pointUsageLog;
}


@end
