//
//  ContributorObject.m
//  Story17
//
//  Created by Racing on 2015/12/15.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "ContributorObject.h"
#import "UserObject.h"

@implementation ContributorObject

+ (ContributorObject *)getContributorWithResult:(NSDictionary*)results
{
    ContributorObject *obj = [[ContributorObject alloc] init];
    obj.user = [UserObject getUserWithDict:results[@"userInfo"]];
    obj.point = [results[@"point"] intValue];
    
    return obj;
}
@end
