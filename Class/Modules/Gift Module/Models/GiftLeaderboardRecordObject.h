//
//  GiftLeaderboardRecordObject.h
//  Story17
//
//  Created by POPO on 11/26/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class UserObject;

@interface GiftLeaderboardRecordObject : NSObject

typedef NS_ENUM(NSInteger, GiftLeaderboardRecordType) {
    GiftLeaderboardRecordTypeTotal,
    GiftLeaderboardRecordTypeYear,
    GiftLeaderboardRecordTypeMonth,
    GiftLeaderboardRecordTypeWeek,
    GiftLeaderboardRecordTypeDay
};

@property (nonatomic, strong) UserObject* user;
@property (nonatomic) int32_t point;
@property (nonatomic) float revenue;

+ (GiftLeaderboardRecordObject*)getGiftLeaderboardRecordWitDict:(NSDictionary*)giftLeaderboardRecordInfoDict;

@end
