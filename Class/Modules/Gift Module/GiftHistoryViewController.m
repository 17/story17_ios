//
//  GiftHistoryViewController.m
//  Story17
//
//  Created by Racing on 2015/12/23.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "GiftHistoryViewController.h"
#import "Constant.h"
#import "UserGiftObject.h"
#import "GiftHistoryTableViewCell.h"
#import "LiveStreamObject.h"

static NSString * const kReuseCell = @"GiftHistoryTableViewCell";
static CGFloat const kQueryThreshold = 120;
static int const kQueryCount = 10;

@interface GiftHistoryViewController () <UITableViewDataSource, UITableViewDelegate> {
    struct {
        unsigned int didClickGift : 1;
    } _delegateFlags;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) NSMutableArray *records;
@property (assign, nonatomic) int currentOffset;

@end

@implementation GiftHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_tableView registerNib:[UINib nibWithNibName:kReuseCell bundle:nil] forCellReuseIdentifier:kReuseCell];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.records = [NSMutableArray array];
    [self queryHistoryWithCount:kQueryCount andOffset:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertContributorsAtRange:(NSRange)range
{
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (NSUInteger i = range.location; i < range.location+range.length; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPaths addObject:indexPath];
    }
    
    [_tableView beginUpdates];
    [_tableView insertRowsAtIndexPaths:[indexPaths copy] withRowAnimation:UITableViewRowAnimationNone];
    [_tableView endUpdates];
}

- (void)showEmptyTips
{
    // Display a message when the table is empty
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    
    messageLabel.text = LOCALIZE(@"Livestreamer doesn't receive any gifts.");
    messageLabel.textColor = [UIColor colorFromHexString:@"#999999"];
    messageLabel.font = [UIFont systemFontOfSize:14];
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    [messageLabel sizeToFit];
    
    _tableView.backgroundView = messageLabel;
}

- (void)queryHistoryWithCount:(int)count andOffset:(int)offset
{
    [API_MANAGER getGiftHistoryInLiveStream:_liveStreamObj.liveStreamID withCount:count andOffset:offset withCompletion:^(BOOL success, NSArray *gifts) {
        if (success) {
            int recordCount = (int)gifts.count;
            self.currentOffset = offset + recordCount;
            
            
            if (_records.count > 0 && recordCount > 0) {
                // Second time query
                
                [_records addObjectsFromArray:gifts];
                [self insertContributorsAtRange:NSMakeRange(offset, recordCount)];
            }
            else if (recordCount > 0){
                // First time query
                
                [_records addObjectsFromArray:gifts];
                [_tableView reloadData];
            }
            else if (_records.count == 0) {
                // No query result
                
                [self showEmptyTips];
            }
            
        }
        
        [_activityIndicator stopAnimating];
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserGiftObject *userGift = _records[indexPath.row];
    UserObject *user = userGift.user;
    GiftObject *gift = userGift.gift;
    
    GiftHistoryTableViewCell *cell = (GiftHistoryTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:kReuseCell];
    
    [cell.profileImageView setImageWithURL:S3_THUMB_IMAGE_URL(user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    cell.nameLabel.text = user.openID;
    [cell.giftThumbImageView setImageWithURL:S3_FILE_URL(gift.leaderboardIcon) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _records.count;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserGiftObject *userGift = _records[indexPath.row];
    GiftObject *gift = userGift.gift;
    
    if (_delegateFlags.didClickGift) {
        [_delegate giftHistoryViewController:self didClickGift:gift];
    }
    
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat actualPosition = scrollView.contentOffset.y;
    CGFloat contentHeight = scrollView.contentSize.height - _tableView.frame.size.height;
    if (actualPosition >= contentHeight - kQueryThreshold && _records.count >= _currentOffset) {
        
        int count = _currentOffset;
        self.currentOffset += kQueryCount;
        [self queryHistoryWithCount:kQueryCount andOffset:count];
    }
}

#pragma mark - Override

- (void)setDelegate:(id<GiftHistoryViewControllerDelegate>)delegate
{
    _delegate = delegate;
    _delegateFlags.didClickGift = [_delegate respondsToSelector:@selector(giftHistoryViewController:didClickGift:)];
}

@end
