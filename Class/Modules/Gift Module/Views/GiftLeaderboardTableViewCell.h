//
//  GiftLeaderboardTableViewCell.h
//  Story17
//
//  Created by Racing on 2015/12/14.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiftLeaderboardTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *bigRankMarkImageView;
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *contributionLabel;
@property (strong, nonatomic) IBOutlet UILabel *rankLabel;

@end
