//
//  GiftRecordCell.h
//  Story17
//
//  Created by POPO on 11/10/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PointGainObject.h"

@interface GiftRecordCell : UITableViewCell

@property (nonatomic) PointGainType pointType;
@property (nonatomic, strong) UILabel* itemLabel;
@property (nonatomic, strong) UILabel* dateLabel;
@property (nonatomic, strong) UIImageView* targetUserImageView;
@property (nonatomic, strong) UILabel* targetUserLabel;
@property (nonatomic, strong) UIImageView* pointImageView;
@property (nonatomic, strong) UILabel* pointLabel;
@property (nonatomic, strong) UILabel* year;
@property (nonatomic, strong) UILabel* monthDay;
@property (nonatomic, strong) UILabel* giftName;


@property (nonatomic,strong) NSString* targetUserPicture;

@property (nonatomic,strong) UIImageView* maskImage;

-(void)setUser:(NSString*)targetOpenID targetUserPicture:(NSString*)targetUserPicture;
-(void)setGiftReceiveUI:(NSString*)targetUserPicture timeStampYear:(NSString*)timeStampYear timeStampMonth:(NSString*)timeStampMonth giftName:(NSString*)giftName;
@end
