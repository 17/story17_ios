//
//  UserListCell.h
//  Story17
//
//  Created by POPO on 2015/5/8.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserTitleView.h"
#import "UserObject.h"

@interface GiftBoardCell : UITableViewCell
{
@private
    UserTitleView* _titleView;
    UIButton* _followBtn;
    UIImageView* giftImage;
    UILabel* giftPoint;
}

@property(nonatomic, strong)UserObject* user;

+ (float)getDefaultHeight;
- (void)setUser:(UserObject *)user withGiftpoint:(NSString*)Point;

@end
