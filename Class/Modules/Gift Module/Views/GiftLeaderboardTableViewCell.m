//
//  GiftLeaderboardTableViewCell.m
//  Story17
//
//  Created by Racing on 2015/12/14.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "GiftLeaderboardTableViewCell.h"

@implementation GiftLeaderboardTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    _profileImageView.layer.cornerRadius = _profileImageView.frame.size.width / 2;
    _profileImageView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
