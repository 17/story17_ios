//
//  UserListCell.m
//  Story17
//
//  Created by POPO on 2015/5/8.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "GiftBoardCell.h"

@implementation GiftBoardCell

@synthesize user = _user;

+ (float)getDefaultHeight
{
    return [UserTitleView getDefaultHeight];
}

-(void) awakeFromNib
{
    _titleView = [[UserTitleView alloc] init];
    [_titleView setFrame:self.bounds];
    [self.contentView addSubview:_titleView];
    [self setBackgroundColor:WHITE_COLOR];
    giftImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"giftpoint_%d",rand()%7+1]]];
    [giftImage setFrame:CGRectMake(SCREEN_WIDTH-140, 5, 15, 15)];
    giftImage.center=CGPointMake(SCREEN_WIDTH-15, [UserTitleView getDefaultHeight]/2);
    giftPoint=[[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-190, 35, 10, 10)];
    [giftPoint setTextColor:GRAY_COLOR];
    UIView* separaterLine = [ThemeManager separaterLine];
    separaterLine.frame = CGRectMake(0, [UserTitleView getDefaultHeight]-0.5, SCREEN_WIDTH, 0.5);

    [self.contentView addSubview:separaterLine];
    [self.contentView addSubview:giftPoint];
    [self.contentView addSubview:giftImage];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self awakeFromNib];
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        

        
        _followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _followBtn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_followBtn addTarget:self action:@selector(didClickFollow:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_followBtn];
        
        CGRect rect;
        rect.origin.x = frame.size.width - FOLLOW_BTN_WIDTH-10;
        rect.origin.y = [UserTitleView getDefaultHeight]/2-FOLLOW_BTN_HEIGHT/2;
        rect.size.width = FOLLOW_BTN_WIDTH;
        rect.size.height = FOLLOW_BTN_HEIGHT;
        [_followBtn setFrame:rect];
        
    }
    
    return self;
}

- (void)setUser:(UserObject *)user withGiftpoint:(NSString *)Point
{

    _user = user;
//    [giftPoint setText:[NSString stringWithFormat:@"%d",rand()%50000000]];
    [giftPoint setText:Point];
    [giftPoint sizeToFit];
    giftPoint.center=CGPointMake(SCREEN_WIDTH-30-giftPoint.frame.size.width/2, [UserTitleView getDefaultHeight]/2);
    
    _titleView.title = user.openID;
    _titleView.subTitle = user.name;
    _titleView.image = user.picture;
    
    if(_user.isVerified==1){
        [_titleView verified:YES];
    }else{
        [_titleView verified:NO];
    }
    
    if(_user.isChoice==1){
        [_titleView choiced:YES];
    }else{
        [_titleView choiced:NO];
    }
    
    if(_user.crossGreatWall==1){
        [_titleView chinaFlag:YES];
    }else{
        [_titleView chinaFlag:NO];
    }
    
    if(_user.isInternational==1){
        [_titleView internationalFlag:YES];
    }else{
        [_titleView internationalFlag:NO];
    }

}


@end
