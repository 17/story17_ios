//
//  GiftHistoryTableViewCell.h
//  Story17
//
//  Created by Racing on 2015/12/23.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiftHistoryTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UIImageView *giftThumbImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end
