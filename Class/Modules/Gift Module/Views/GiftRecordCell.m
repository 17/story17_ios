//
//  GiftRecordCell.m
//  Story17
//
//  Created by POPO on 11/10/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "GiftRecordCell.h"
#import "Constant.h"

@interface GiftRecordCell()

#define INSET 10
#define TEXT_WIDTH 100
#define TEXT_HEIGHT 20

@end

@implementation GiftRecordCell

//- (id)initWithFrame:(CGRect)frame
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier
{
//    self = [super initWithFrame:frame];
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView setBackgroundColor:WHITE_COLOR];
        
        _itemLabel = [[UILabel alloc] init];
        _itemLabel.font = SYSTEM_FONT_WITH_SIZE(16);
        [_itemLabel setTextColor:GRAY_COLOR];
        _itemLabel.frame = CGRectMake(20, INSET, TEXT_WIDTH, TEXT_HEIGHT);
        [self.contentView addSubview:_itemLabel];
        
        _dateLabel = [[UILabel alloc] init];
        _dateLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_dateLabel setTextColor:GRAY_COLOR];
        _dateLabel.frame = CGRectMake(20, INSET+_itemLabel.frame.size.height+5, 150, TEXT_HEIGHT);
//        [_dateLabel sizeToFit];
        [self.contentView addSubview:_dateLabel];
        
        _targetUserImageView = [[UIImageView alloc] init];
        _maskImage = [[UIImageView alloc] init];
        [_maskImage setImage:[UIImage imageNamed:@"head_mask"]];
        
        CGRect rect = CGRectMake(SCREEN_WIDTH/5*2-25, 10, 40, 40);
        [_targetUserImageView setFrame:rect];
        [_maskImage setFrame:rect];
        [self.contentView addSubview:_targetUserImageView];
        [self.contentView addSubview:_maskImage];
        if (_targetUserPicture==nil) {
            _targetUserPicture=@"";
        }



        

        _targetUserLabel = [[UILabel alloc] init];
        _targetUserLabel.font = SYSTEM_FONT_WITH_SIZE(16);
        [_targetUserLabel setTextColor:GRAY_COLOR];
        _targetUserLabel.frame = CGRectMake(SCREEN_WIDTH/3+45,20,  150, TEXT_HEIGHT);
        [self.contentView addSubview:_targetUserLabel];
        
        
        _pointLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH/5, 40)];
        _pointLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        _pointLabel.frame = CGRectMake(SCREEN_WIDTH-INSET-_pointLabel.frame.size.width, 20, _pointLabel.frame.size.width, _pointLabel.frame.size.height);



        [_pointLabel setTextColor:GRAY_COLOR];
        [self.contentView addSubview:_pointLabel];
        
        
        _pointImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"giftpoint_%d",rand()%7+1]] ];
        _pointImageView.frame = CGRectMake(SCREEN_WIDTH-INSET-_pointLabel.frame.size.width-20-4, self.frame.size.height/2, 13, 15);
        [self.contentView addSubview:_pointImageView];
        UIView* lineView = [UIView new];
        [lineView setBackgroundColor:[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0f]];
        [lineView setFrame:CGRectMake(0, 60, SCREEN_WIDTH, 5)];
        [self.contentView addSubview:lineView];
    
        
        
        _year=[[UILabel alloc]initWithFrame:CGRectMake(20, 10, 60, 20)];
        _year.font = SYSTEM_FONT_WITH_SIZE(18);
        [self.contentView addSubview:_year];
        [_year setTextColor:GRAY_COLOR];
        _monthDay=[[UILabel alloc]initWithFrame:CGRectMake(20, 35, 60, 20)];
        _monthDay.font = SYSTEM_FONT_WITH_SIZE(18);
        [self.contentView addSubview:_monthDay];
        [_monthDay setTextColor:GRAY_COLOR];
        
        
        _giftName=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, 60, 20)];
        _giftName.font = SYSTEM_FONT_WITH_SIZE(14);
        [self.contentView addSubview:_giftName];
    }
    
    return self;
}


-(void)setUser:(NSString *)targetOpenID targetUserPicture:(NSString *)targetUserPicture
{
    if([targetUserPicture isEqualToString:@""]){
        [_targetUserImageView setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }else{
        [_targetUserImageView setImageWithURL:S3_THUMB_IMAGE_URL(targetUserPicture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }
    
    [_pointLabel sizeToFit];
    _pointLabel.frame = CGRectMake(SCREEN_WIDTH-INSET-_pointLabel.frame.size.width, _pointLabel.frame.origin.y, _pointLabel.frame.size.width, _pointLabel.frame.size.height);
    _pointImageView.frame = CGRectMake(SCREEN_WIDTH-INSET-_pointLabel.frame.size.width-20-4, _pointImageView.frame.origin.y, 13, 15);
}

-(void)setGiftReceiveUI:targetUserPicture timeStampYear:(NSString *)timeStampYear timeStampMonth:(NSString *)timeStampMonth giftName:(NSString *)giftName
{
    if([targetUserPicture isEqualToString:@""]){
        [_targetUserImageView setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }else{
        [_targetUserImageView setImageWithURL:S3_THUMB_IMAGE_URL(targetUserPicture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }
    [_monthDay setText:timeStampMonth];
    [_year setText:timeStampYear];
    if (IS_IPHONE_4) {
        _pointLabel.frame=CGRectMake(_targetUserLabel.frame.origin.x+_targetUserLabel.frame.size.width+10, _targetUserLabel.frame.origin.y, _pointLabel.frame.size.width, _pointLabel.frame.size.height);
    }else{
//        _pointLabel.frame=CGRectMake(_pointLabel.frame.origin.x, _targetUserLabel.frame.origin.y*9/10, _pointLabel.frame.size.width, _pointLabel.frame.size.height);
        _pointLabel.frame=CGRectMake(_pointLabel.frame.origin.x, 5, _pointLabel.frame.size.width,55);

        _pointLabel.numberOfLines=3;
        [_pointLabel sizeThatFits:CGSizeMake(_pointLabel.frame.size.width, _pointLabel.frame.size.height)];
    }
    [_pointLabel setText:giftName];
}


@end
