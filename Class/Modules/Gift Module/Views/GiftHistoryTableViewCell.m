//
//  GiftHistoryTableViewCell.m
//  Story17
//
//  Created by Racing on 2015/12/23.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "GiftHistoryTableViewCell.h"

@implementation GiftHistoryTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    _profileImageView.layer.cornerRadius = CGRectGetWidth(_profileImageView.frame) / 2;
    _profileImageView.clipsToBounds = YES;
    
    _giftThumbImageView.layer.cornerRadius = 5;
    _giftThumbImageView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
