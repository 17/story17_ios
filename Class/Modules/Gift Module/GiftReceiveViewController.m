  //
//  GiftReceiveViewController.m
//  Story17
//
//  Created by POPO on 2015/11/27.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GiftReceiveViewController.h"
#import "GiftRecordCell.h"
#import "SVPullToRefresh.h"
#import "Constant.h"
@interface GiftReceiveViewController ()
@property (nonatomic,strong)UITableView* receiveGiftTableView;
@property (nonatomic,strong)NSMutableArray* receiveGiftArray;
@property BOOL isFirstFetching;
@property BOOL isFetching;
@property BOOL noMoreData;
@property (nonatomic,strong) UIActivityIndicatorView* progressView;
@property (nonatomic,strong) UIImageView* noDataImageView;



@end
#define scrollViewHeight SCREEN_HEIGHT-NAVI_BAR_HEIGHT
#define topViewHeight 40
#define giftReceiveCellId @"giftReceiveCellId"


@implementation GiftReceiveViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.title=LOCALIZE(@"received_gift");
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    _isFirstFetching=YES;
    _isFetching=NO;
    _noMoreData=NO;
    UIView* topView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, topViewHeight)  ];
    UILabel* date=[[UILabel alloc]initWithFrame:CGRectMake(20, 10, 40, 20)];
    [date setText:LOCALIZE(@"date")];
    [date setFont:SYSTEM_FONT_WITH_SIZE(16)];
    [date setTextColor:BLACK_COLOR];
    [date setTextAlignment:NSTextAlignmentCenter];
    [topView addSubview:date];
    
    UILabel* sender=[[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/3, 10, 70, 20)];
    [sender setText:LOCALIZE(@"sender_gift")];
    [sender setFont:SYSTEM_FONT_WITH_SIZE(16)];
    [sender setTextColor:BLACK_COLOR];
    [sender setTextAlignment:NSTextAlignmentCenter];
    [topView addSubview:sender];
    
    
    UILabel* gift=[[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/4*3+20, 10, 40, 20)];
    [gift setText:LOCALIZE(@"gift")];
    [gift setFont:SYSTEM_FONT_WITH_SIZE(16)];
    [gift setTextColor:BLACK_COLOR];
    [gift setTextAlignment:NSTextAlignmentCenter];
    [topView addSubview:gift];
    
    
    [self.view addSubview:topView];
    
    
    
    
    
    _receiveGiftArray=CREATE_MUTABLE_ARRAY;
    _receiveGiftTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, topViewHeight, SCREEN_WIDTH, scrollViewHeight-topViewHeight) style:UITableViewStylePlain];
    __weak GiftReceiveViewController *weakSelf = self;
    _receiveGiftTableView.delegate=self;
    _receiveGiftTableView.dataSource=self;
    _receiveGiftTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//    _receiveGiftTableView
    [self.view addSubview:_receiveGiftTableView];
    [_receiveGiftTableView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData:YES ];
    }];

    
    _noDataImageView = [ThemeManager getNoDataImageView];

    [self.view addSubview:_noDataImageView];
    
    
    _progressView = [UIActivityIndicatorView new];
    _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView setColor:MAIN_COLOR];
    

    [self.view addSubview:_progressView];
    
    
    [self fetchData:YES];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_receiveGiftArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GiftRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:giftReceiveCellId];
    UserGiftObject *receiveLog = [_receiveGiftArray objectAtIndex:indexPath.row];
    
    if (cell == nil) {
        //            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:purchaseCellIdentifier];
        cell = [[GiftRecordCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:giftReceiveCellId];
        
    }
    cell.userInteractionEnabled = NO;
    

    cell.itemLabel.hidden=YES;
    cell.pointImageView.hidden=YES;
//    cell.pointLabel.text=[NSString stringWithFormat:@"%d",receiveLog.gift.point];
    //            [cell.textLabel setTextColor:GRAY_COLOR];
//    purchaseLog.timestamp=CURRENT_TIMESTAMP;
    NSDateFormatter *_formatterYear=[[NSDateFormatter alloc]init];
    
    NSDateFormatter *_formatterMonth=[[NSDateFormatter alloc]init];

    [_formatterMonth setDateFormat:@"MM-dd"];
    [_formatterYear setDateFormat:@"YYYY"];

    NSDate *date = [NSDate dateWithTimeIntervalSince1970:receiveLog.timestamp];
    cell.dateLabel.hidden =YES;
    
    
    [cell.targetUserLabel setText:receiveLog.user.openID];

    DLog(@"%@",[_formatterMonth stringFromDate:date]);
    [cell setGiftReceiveUI:receiveLog.user.picture timeStampYear:[_formatterYear stringFromDate:date] timeStampMonth:[_formatterMonth stringFromDate:date] giftName:receiveLog.gift.name];
    
    //        cell.dateLabel.text = [NSString stringWithFormat:@"%d", purchaseLog.timestamp];
    //            [cell.itemLabel setTextColor:GRAY_COLOR];
    //            cell.pointLabel.text = @"30";
    if(indexPath.row>[_receiveGiftArray count]-20/3){
        [self fetchData:false ];
    }
    return cell;
    
}
-(void)fetchData:(BOOL)refresh
{
    if (refresh) {
        _noMoreData=NO;
    }
    
    if(_isFetching || _noMoreData) return;

    UserGiftObject* lastObject;
    if ([_receiveGiftArray count]>0) {
        lastObject=_receiveGiftArray[[_receiveGiftArray count]-1];
    }
    
    int32_t beforeTime = INT32_MAX;
    if(lastObject==NULL || refresh){
        beforeTime = INT32_MAX;
    }else{
        beforeTime = lastObject.timestamp;
    }
//    if( !_isFirstFetching){
//        [_receiveGiftTableView.pullToRefreshView startAnimating];
//    }
//    
    if(_isFirstFetching){
        [_progressView startAnimating];
        _isFirstFetching = false;
    }
    [API_MANAGER getReceivedGiftLog:GET_DEFAULT(USER_ID) beforeTime:beforeTime withCount:20 withCompletion:^(BOOL success, NSArray *receiveGiftsLog) {
        [_receiveGiftTableView.pullToRefreshView stopAnimating];
        [_progressView stopAnimating];
        if (success) {
     
            if ([receiveGiftsLog count]<20) {
                _noMoreData=YES;
            }
            if (refresh) {
                [_receiveGiftArray removeAllObjects];
                [_receiveGiftArray addObjectsFromArray:receiveGiftsLog];
            }else{
                [_receiveGiftArray addObjectsFromArray:receiveGiftsLog];
            }
            
            if ([_receiveGiftArray count]==0) {
                _noDataImageView.hidden=NO;
            }else{
                _noDataImageView.hidden=YES;
            }
        }
        
        [_receiveGiftTableView reloadData];
    }];
}

@end
