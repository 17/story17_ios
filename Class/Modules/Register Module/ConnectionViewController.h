//
//  ConnectionViewController.h
//  story17
//
//  Created by POPO Chen on 5/20/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ConnectionStatus){
    FBConnnectionStatus,
    ContactConnectionStatus
};

@interface ConnectionViewController : UIViewController
@property (assign, nonatomic) ConnectionStatus status;

@end
