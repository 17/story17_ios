//
//  ConnectionViewController.m
//  story17
//
//  Created by POPO Chen on 5/20/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "ConnectionViewController.h"
#import "ConnectionUserViewController.h"
#import "Constant.h"
#import "UserSuggestionViewController.h"
#import "UIImage+Helper.h"
#import "ImageTextField.h"
#import "NationTableViewController.h"
#import "CustomNavbarView.h"

#define coverPhotoHeight 0
#define leftMargin SCREEN_WIDTH/8
#define btnHeight [ImageTextField getDefaultHeight]
#define labelHeight 30
#define arrowWidth 12

#define description_font_size 14

#define alertHeight SCREEN_HEIGHT-KEYBOARD_HEIGHT

@interface ConnectionViewController() <NationTableDelegate, FacebookManagerDelegate>
@property (strong, nonatomic) CustomNavbarView *navBar;
@property (strong, nonatomic) UIImageView *bgImageView;

@property (strong, nonatomic) UIScrollView *mainScrollView;

@property (strong, nonatomic) UIImageView *coverPhoto;

@property (strong, nonatomic) UIButton *countryButton;
@property (strong, nonatomic) ImageTextField *phoneImageTextField;

@property (strong, nonatomic) UIButton *connectionBtn;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *descriptionLabel;

@property (strong, nonatomic) UIButton *secondConnectionBtn;
@property (strong, nonatomic) UILabel *secondTitleLabel;
@property (strong, nonatomic) UILabel *secondDescriptionLabel;

@property (strong, nonatomic) NSMutableArray *fbFriendIDArray;

@property (strong, nonatomic) UIImageView *alertView;
@property (strong, nonatomic) UILabel *alertLabel;

@end

@implementation ConnectionViewController

- (id)init
{
    self = [super init];
    if (self != nil){
        _status = ContactConnectionStatus;
    }
    
    return self;
}


-(void)dealloc
{
    [NOTIFICATION_CENTER removeObserver:self];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.title = LOCALIZE(@"Connect_contact");
    
    [self configureViewForIOS7];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:LOCALIZE(@"skip") style:UIBarButtonItemStyleDone target:self action:@selector(skipAction:)];
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    
    [self.view setBackgroundColor:WHITE_COLOR];
    
    int nextY = STATUS_BAR_HEIGHT;
    
    _navBar = [[CustomNavbarView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, NAVI_BAR_HEIGHT)];
    [_navBar.titleLabel setTextColor:BLACK_COLOR];
    [_navBar.backBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [_navBar.nextBtn addTarget:self action:@selector(skipAction:) forControlEvents:UIControlEventTouchUpInside];
    [_navBar.nextBtn setTitle:LOCALIZE(@"Next") forState:UIControlStateNormal];
    _navBar.backBtn.hidden = YES;
    _navBar.titleLabel = LOCALIZE(@"Connect_contact");

    [_navBar setBackgroundColor:WHITE_COLOR];
    nextY += _navBar.frame.size.height+10;
    
    UIImage* screenImg = [UIImage imageByApplyingBlurToImage:[UIImage imageNamed:@"bg5"] withRadius:SCREEN_WIDTH/32 tintColor:[UIColor colorWithRed:255.f green:255.f blue:255.f alpha:0.2f] saturationDeltaFactor:1.f maskImage:nil];
    _bgImageView = [[UIImageView alloc]initWithImage:screenImg];
    _bgImageView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, SCREEN_HEIGHT-nextY);
    _bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    [self.view addSubview:_bgImageView];
    [self.view addSubview:_navBar];

    
    _coverPhoto = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, coverPhotoHeight)];
    [_coverPhoto setImage:[UIImage imageNamed:@"pic_fb"]];
    _coverPhoto.contentMode = UIViewContentModeScaleAspectFill;
    
    [self.view addSubview:_coverPhoto];
    
    
    _mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, _navBar.frame.size.height+STATUS_BAR_HEIGHT, 2*SCREEN_WIDTH, SCREEN_HEIGHT-_navBar.frame.size.height-STATUS_BAR_HEIGHT)];
    [_mainScrollView setBackgroundColor:LIGHT_BG_COLOR];
    [_mainScrollView setBackgroundColor:[UIColor clearColor]];
    
    nextY = 10;

    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH+leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, labelHeight)];
    [_titleLabel setTextColor:DARK_GRAY_COLOR];
    [_titleLabel setText:LOCALIZE(@"find_friend_fb")];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
//    nextY += _titleLabel.frame.size.height+10;
    nextY += 15;
    
    _descriptionLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH+leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, labelHeight)];
    [_descriptionLabel setTextColor:GRAY_COLOR];
    [_descriptionLabel setText:LOCALIZE(@"find_friend_fb_description")];
    _descriptionLabel.font = SYSTEM_FONT_WITH_SIZE(description_font_size);
    
    CGSize stringSize = [_descriptionLabel.text getSizeWithConstrainSize:CGSizeMake(_descriptionLabel.frame.size.width,90) withFont:SYSTEM_FONT_WITH_SIZE(description_font_size)];

    _descriptionLabel.frame = CGRectMake(3*SCREEN_WIDTH/2-stringSize.width/2, _descriptionLabel.frame.origin.y, stringSize.width, stringSize.height);
    _descriptionLabel.textAlignment = NSTextAlignmentCenter;
    _descriptionLabel.numberOfLines = 0;
    
    nextY += _descriptionLabel.frame.size.height+25;
    
    _connectionBtn = [ThemeManager getRegisterControlBtn];
    _connectionBtn.frame = CGRectMake(SCREEN_WIDTH+10, nextY, SCREEN_WIDTH-20, btnHeight);
    nextY += _connectionBtn.frame.size.height;
    
    [_connectionBtn setTitle:LOCALIZE(@"Connection_fb") forState:UIControlStateNormal];
    [_connectionBtn addTarget:self action:@selector(connectionAction:) forControlEvents:UIControlEventTouchUpInside];

    
    nextY = 10;
    
    _secondTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, labelHeight)];
    [_secondTitleLabel setTextColor:DARK_GRAY_COLOR];
    [_secondTitleLabel setText:LOCALIZE(@"find_friend_contact")];
    _secondTitleLabel.textAlignment = NSTextAlignmentCenter;

    nextY += 10;

    _secondDescriptionLabel = [[UILabel alloc]initWithFrame:CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, labelHeight)];
    [_secondDescriptionLabel setTextColor:GRAY_COLOR];
    [_secondDescriptionLabel setText:LOCALIZE(@"find_friend_contact_description")];
    _secondDescriptionLabel.font = SYSTEM_FONT_WITH_SIZE(description_font_size);
    
    CGSize stringSize2 = [_descriptionLabel.text getSizeWithConstrainSize:CGSizeMake(_secondDescriptionLabel.frame.size.width,90) withFont:SYSTEM_FONT_WITH_SIZE(description_font_size)];

    _secondDescriptionLabel.frame = CGRectMake(SCREEN_WIDTH/2-stringSize2.width/2, _secondDescriptionLabel.frame.origin.y, stringSize2.width, stringSize2.height);
    _secondDescriptionLabel.textAlignment = NSTextAlignmentCenter;
    _secondDescriptionLabel.numberOfLines = 0;
    nextY += _secondDescriptionLabel.frame.size.height+15;

    _countryButton = [[UIButton alloc]initWithFrame:CGRectMake(10, nextY, 80, 30)];
    [_countryButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [_countryButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
    [_countryButton setImage:IMAGE_FROM_BUNDLE(@"downward_black") forState:UIControlStateNormal];
    [_countryButton addTarget:self action:@selector(didClickNation:) forControlEvents:UIControlEventTouchUpInside];
    [_countryButton setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
    _countryButton.titleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [_countryButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_countryButton sizeToFit];
    _countryButton.frame = CGRectMake(_countryButton.frame.origin.x, _countryButton.frame.origin.y, _countryButton.frame.size.width+30, _countryButton.frame.size.height);
    [_countryButton setImageEdgeInsets:UIEdgeInsetsMake(0, CGRectGetWidth(_countryButton.frame)-25, 0, 0)];
    [_countryButton setEnabled:NO];
    
    nextY += _countryButton.frame.size.height+5;
    
    _phoneImageTextField = [[ImageTextField alloc]initWithFrame:CGRectMake(10, nextY, SCREEN_WIDTH-20, btnHeight)];
    _phoneImageTextField.mainTextField.keyboardType = UIKeyboardTypeNumberPad;
    [_phoneImageTextField.headImage setImage:[UIImage imageNamed:@"profile_phone"]];
    _phoneImageTextField.mainTextField.placeholder = LOCALIZE(@"Phone_number_optional");
    [_phoneImageTextField.mainTextField setTextColor:BLACK_COLOR];
    [_phoneImageTextField.bgImage setImage:[[UIImage imageNamed:@"btn_greenline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]];
    [_phoneImageTextField changeToPhoneMode];
    
    [_phoneImageTextField.mainTextField setEnabled:NO];
    [_phoneImageTextField.mainTextField setText:GET_DEFAULT(PHONE_NUM)];
    NSLog(@"電話號碼: %@",GET_DEFAULT(PHONE_NUM));
    
    [_phoneImageTextField.countryCodeLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickNation:)]];
    _phoneImageTextField.countryCodeLabel.userInteractionEnabled = YES;
    _phoneImageTextField.mainTextField.font = SYSTEM_FONT_WITH_SIZE(16);
    
    nextY += _phoneImageTextField.frame.size.height+5;

    _secondConnectionBtn = [ThemeManager getRegisterControlBtn];
    _secondConnectionBtn.frame = CGRectMake(10, nextY, SCREEN_WIDTH-20, [ImageTextField getDefaultHeight]);
    nextY += _secondConnectionBtn.frame.size.height;
    
    [_secondConnectionBtn setTitle:LOCALIZE(@"Connection") forState:UIControlStateNormal];
    [_secondConnectionBtn addTarget:self action:@selector(connectionAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_mainScrollView addSubview:_connectionBtn];

    
    [_mainScrollView addSubview:_descriptionLabel];
    [_mainScrollView addSubview:_countryButton];
    [_mainScrollView addSubview:_phoneImageTextField];
    [_mainScrollView addSubview:_secondDescriptionLabel];
    [_mainScrollView addSubview:_secondConnectionBtn];
    
    [self.view addSubview:_mainScrollView];
    
    _alertView = [ThemeManager getAlertImageView];
    _alertLabel = [UILabel new];
    _alertLabel.textAlignment = NSTextAlignmentCenter;
    _alertLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [_alertLabel setTextColor:DARK_GRAY_COLOR];
    
//    [_alertView addSubview:_alertLabel];
//    [self.view addSubview:_alertView];
    _alertView.frame = CGRectMake(0, alertHeight, SCREEN_WIDTH, _alertView.frame.size.height);
    _alertLabel.frame = CGRectMake(0, 0, _alertView.frame.size.width,_alertView.frame.size.height);

    NSString* countryCode = GET_DEFAULT(COUNTRY_CODE);
    NSString* phoneCode = [SINGLETON countryCode2PhoneCode][countryCode];
    [_phoneImageTextField.countryCodeLabel setText:[NSString stringWithFormat:@"+%@",phoneCode]];
    [_countryButton setTitle:LOCALIZE(countryCode) forState:UIControlStateNormal];
    [_countryButton sizeToFit];
    _countryButton.frame = CGRectMake(_countryButton.frame.origin.x, _countryButton.frame.origin.y, _countryButton.frame.size.width+30, _countryButton.frame.size.height);
    [_countryButton setImageEdgeInsets:UIEdgeInsetsMake(0, CGRectGetWidth(_countryButton.frame)-25, 0, 0)];

    DLog(@"countryCode:%@ phoneCode:%@",countryCode,phoneCode);
    
    if([countryCode isEqualToString:@""]){
        [SINGLETON detectIpGeoLocation:^(BOOL success, NSDictionary *jsonDict) {
            if(success){
                NSString* countryCode = jsonDict[@"countryCode"];
                NSString* phoneCode = [SINGLETON countryCode2PhoneCode][countryCode];
                [_phoneImageTextField.countryCodeLabel setText:[NSString stringWithFormat:@"+%@",phoneCode]];
                [_countryButton setTitle:LOCALIZE(countryCode) forState:UIControlStateNormal];
                [_countryButton sizeToFit];
                _countryButton.frame = CGRectMake(_countryButton.frame.origin.x, _countryButton.frame.origin.y, _countryButton.frame.size.width+30, _countryButton.frame.size.height);
                [_countryButton setImageEdgeInsets:UIEdgeInsetsMake(0, CGRectGetWidth(_countryButton.frame)-25, 0, 0)];
                
            }
        }];
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];

    if(_status==FBConnnectionStatus){
        [_mainScrollView setContentOffset:CGPointMake(SCREEN_WIDTH, 0) animated:NO];
    }else if(_status==ContactConnectionStatus){
        [_phoneImageTextField.mainTextField becomeFirstResponder];
        [_mainScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    }
}

-(void)connectionAction:(id)sender
{
    
    
//    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"currencyRate" ofType:@"plist"]];
//    DLog(@"%@",dictionary[@"quotes"]);
    
    if([sender isEqual:_connectionBtn]){
        
        [self showFBPermissionAlert];
        
    }else if( [sender isEqual:_secondConnectionBtn]){
    
        if([_phoneImageTextField.mainTextField.text isEqualToString:@""]){

            /* Show Alert if not fill Phone number */
            [self showAlertViewMessage:LOCALIZE(@"PLZ_ENTER_PHONE_BEFORE_CONNECTION")];
            
        }else{

            /* Update PhoneNumber */
            [DEFAULTS setObject:[self validPhoneNumer:_phoneImageTextField.mainTextField.text] forKey:PHONE_NUM];
            [DEFAULTS setObject:[_phoneImageTextField.countryCodeLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""] forKey:COUNTRY_CODE];
            [DEFAULTS synchronize];

            NSString* totalPhoneNum = [NSString stringWithFormat:@"%@%@",GET_DEFAULT(COUNTRY_CODE),GET_DEFAULT(PHONE_NUM)];
            [API_MANAGER updateUserInfo:@{@"phoneNumber":GET_DEFAULT(PHONE_NUM),@"countryCode":GET_DEFAULT(COUNTRY_CODE),@"phoneWithCountryCode":totalPhoneNum} fetchSelfInfo:NO completion:^(BOOL success) {}];
            
            ConnectionUserViewController* userVC = [ConnectionUserViewController new];
            [userVC.navBar.titleLabel setText:LOCALIZE(@"Connect_contact")];
            UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:userVC];
            userVC.type = UserFromContactBook;
            
            [self.navigationController presentViewController:nav animated:YES completion:^{
                
                #ifdef INTERNATION_VERSION
                    [self modeTransform2ContactMode];
                #else
                    UserSuggestionViewController* userSugVC = [UserSuggestionViewController new];
                    [self.navigationController pushViewController:userSugVC animated:YES];
                #endif
            }];
            
        }
        
    }
}


-(void)didClickNation:(id)sender
{
    NationTableViewController* nVC = [NationTableViewController new];
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:nVC];
    nVC.delegate = self;
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}



-(void)skipAction:(id)sender
{
    
    if(_status==FBConnnectionStatus){
        [EVENT_HANDLER addEventTracking:@"SkipFindFriendWithFB" withDict:nil];
        UserSuggestionViewController* userSugVC = [UserSuggestionViewController new];
        [self.navigationController pushViewController:userSugVC animated:YES];
        
    }else if(_status==ContactConnectionStatus){
        [EVENT_HANDLER addEventTracking:@"SkipFindFrienWithContact" withDict:nil];
        #ifdef INTERNATIONAL_VERSION
            [_phoneImageTextField.mainTextField resignFirstResponder];
            [self modeTransform2ContactMode];
        #else
            UserSuggestionViewController* userSugVC = [UserSuggestionViewController new];
            [self.navigationController pushViewController:userSugVC animated:YES];
        #endif
    
    }
}

-(void)modeTransform2ContactMode
{
    _alertView.hidden = YES;
    _status = FBConnnectionStatus;
    [_mainScrollView setContentOffset:CGPointMake(SCREEN_WIDTH, 0) animated:YES];
    [self.navBar.titleLabel setText:LOCALIZE(@"Connect_facebook")];
    [_coverPhoto setImage:[UIImage imageNamed:@"pic_contacts"]];

}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}


#pragma mark - Contact Book Related

- (void)showFBPermissionAlert
{
    if (![FACEBOOK_MANAGER isLoginned]) {
        [FACEBOOK_MANAGER setDelegate:self];
        [FACEBOOK_MANAGER loginAction:self];
        
    }
    else {
        [self loginSuccessHandler];
    }
}


-(void)loginSuccessHandler
{
    
    ConnectionUserViewController* userVC = [ConnectionUserViewController new];
    [userVC.navBar.titleLabel setText:LOCALIZE(@"Connect_facebook")];
    UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:userVC];
    userVC.type = UserFromFacebook;
    [self.navigationController presentViewController:nav animated:YES completion:nil];

    UserSuggestionViewController* userSugVC = [UserSuggestionViewController new];
    [self.navigationController pushViewController:userSugVC animated:YES];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if(error!=nil) {
//             [DIALOG_MANAGER showNetworkFailToast];
             return ;
         }
         NSString* facebookID = result[@"id"];
         NSString* fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
         
         [API_MANAGER updateUserInfo:@{@"facebookID":facebookID,@"facebookAccessToken":fbAccessToken} fetchSelfInfo:NO completion:^(BOOL success) {
         }];
     }];
}

- (void)loginFailHandler
{
    
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) didSelectNations:(NSString*)nation code:(NSString*)code;
{
    [_phoneImageTextField.countryCodeLabel setText:[NSString stringWithFormat:@"+%@",code]];
    [_countryButton setTitle:LOCALIZE(nation) forState:UIControlStateNormal];
    [_countryButton sizeToFit];
    _countryButton.frame = CGRectMake(_countryButton.frame.origin.x, _countryButton.frame.origin.y, _countryButton.frame.size.width+30, _countryButton.frame.size.height);
    [_countryButton setImageEdgeInsets:UIEdgeInsetsMake(0, CGRectGetWidth(_countryButton.frame)-25, 0, 0)];

    [DEFAULTS setObject:code forKey:COUNTRY_CODE];
    [DEFAULTS synchronize];
}

-(NSString*)validPhoneNumer:(NSString*)inputPhoneNumber
{
    NSString* validPhone = [inputPhoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@"(" withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@")" withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@"+" withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    NSString* firstIndex = [validPhone substringToIndex:1];
    if([firstIndex isEqualToString:@"0"]){
        validPhone = [validPhone substringWithRange:NSMakeRange(1, [validPhone length]-1)];
    }
    return validPhone;
}

-(void) showAlertViewMessage:(NSString*)message
{
    [_alertView.layer removeAllAnimations];
    if(_alertView.frame.origin.y != SCREEN_HEIGHT-alertHeight){
        _alertView.frame = CGRectMake(0, alertHeight, SCREEN_WIDTH, _alertView.frame.size.height);
    }
    
    [_alertLabel setText:message];
    _alertLabel.center = CGPointMake(SCREEN_WIDTH/2, _alertView.frame.size.height/2);
    
    [UIView animateWithDuration:0.4 animations:^{
        _alertView.frame = CGRectMake(0, alertHeight-_alertView.frame.size.height, SCREEN_WIDTH, _alertView.frame.size.height);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.4 delay:1.6 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            _alertView.frame = CGRectMake(0, alertHeight, SCREEN_WIDTH, _alertView.frame.size.height);
        } completion:nil];
    }];
}

#pragma mark - FacebookManagerDelegate

- (void)facebookManager:(FacebookManager *)facebookManager didLoginComplete:(BOOL)success
{
    if (success) {
        [self loginSuccessHandler];
    }
    else {
        [self loginFailHandler];
    }
}

@end
