//
//  UserListViewController.h
//  story17
//
//  Created by POPO Chen on 5/20/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectionHeaderView.h"
#import "UserListCell.h"
#import "CustomNavbarView.h"


typedef enum{
    UserFromFacebook = 0,
    UserFromContactBook =1
} UserTypeStatus;


@interface ConnectionUserViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, ConnectionHeaderDelegate,UserListCellDelegate>

@property (nonatomic,strong) CustomNavbarView* navBar;
@property (nonatomic,strong) UIImageView* bgImageView;

@property (nonatomic,strong) UICollectionView* mainCollectionView;
@property (nonatomic,strong) UIImageView* noDataImageView;
@property (nonatomic,strong) ConnectionHeaderView* headerView;
@property (nonatomic) UserTypeStatus type;

@property (nonatomic,strong) NSMutableArray* userArray;        // user object for facebook
@property (nonatomic,strong) NSMutableArray* fbFriendIDArray;  // friend facebook id
@property (nonatomic,strong) NSMutableArray* fbUploadData; 

@property (nonatomic,strong) NSMutableArray* contactUsers;     // user object for contact

@property (nonatomic,strong) NSMutableArray* followedUserIDs;  // followed users

@property (nonatomic,strong) NSMutableArray* privacyUserIDs;  // followed users
@property (nonatomic,strong) NSMutableArray* privacyfollowedUserIDs;  // followed users

@end
