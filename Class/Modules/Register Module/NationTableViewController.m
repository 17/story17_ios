//
//  NationTableViewController.m
//  Dare
//
//  Created by POPO on 11/20/14.
//  Copyright (c) 2014 POPO_INNOVATION. All rights reserved.
//

#import "NationTableViewController.h"
#import "Constant.h"

@implementation NationTableViewController
@synthesize mode;

#define nationIdentify @"nationIdentify"

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    self.title = LOCALIZE(@"Nation");
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];

    self.searchArray = CREATE_MUTABLE_ARRAY;
    
    UISearchBar *searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
    searchBar.delegate = self;
    searchBar.placeholder = LOCALIZE(@"search_nation");
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"country" ascending:sigpending];
    NSArray *sortDescriptors=[NSArray arrayWithObject:sortDescriptor];
 
    _countryArray=[[NSMutableArray alloc]initWithArray:[[SINGLETON countryData] sortedArrayUsingDescriptors:sortDescriptors]];
#ifdef INTERNATIONAL_VERSION
#else
    
    for (int i=0; i<[[SINGLETON countryData]count]; i++) {
        NSDictionary* dict =  CREATE_MUTABLE_DICTIONARY;
        dict=[_countryArray objectAtIndex:i];
        if ([dict[@"country"]isEqualToString:@"CN"]) {
            [_countryArray exchangeObjectAtIndex:i withObjectAtIndex:0];
        }
    }
#endif
    self.searchDisplay = [[UISearchDisplayController  alloc] initWithSearchBar:searchBar contentsController:self];
    self.searchDisplay.active = NO;
    self.searchDisplay.delegate = self;
    self.searchDisplay.searchResultsDelegate=self;
    self.searchDisplay.searchResultsDataSource = self;
    [self.searchDisplay.searchResultsTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:nationIdentify];
    self.searchDisplay.searchResultsTableView.scrollEnabled = YES;
    
    self.mode = self.mode!=1?0:1;
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, SCREEN_WIDTH, SCREEN_HEIGHT-44-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:nationIdentify];
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:searchBar];
    
    for (UIView *subView in (SYSTEM_VERSION_LESS_THAN(@"7.0")?searchBar.subviews:[[searchBar.subviews objectAtIndex:0] subviews])) {
        if ([subView isKindOfClass:[UITextField class]]) {
            UITextField *searchField = (UITextField *)subView;
            searchField.font = SYSTEM_FONT_WITH_SIZE(16.0);
        }
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView == self.searchDisplayController.searchResultsTableView){
        return [self.searchArray count];
    } else{
        return [_countryArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nationIdentify forIndexPath:indexPath];
    if (cell == nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nationIdentify];
    }
    NSInteger row = [indexPath row];
    NSDictionary* dict =  CREATE_MUTABLE_DICTIONARY;
    
    if(tableView == self.searchDisplayController.searchResultsTableView){
        dict = [self.searchArray objectAtIndex:row];
    } else{
        dict = [_countryArray objectAtIndex:row];
    }
    
    NSString* countryName = [dict objectForKey:@"country"];
//    UIImage* img = [UIImage imageNamed: [SINGLETON getCountryImageFileName:countryName]];
    
    [cell.textLabel setText:[NSString stringWithFormat:@"%@ (+%@)",LOCALIZE(countryName),[dict objectForKey:@"countryCode"]]];
    [cell.textLabel setFont:BOLD_FONT_WITH_SIZE(18)];
    [cell.textLabel setTextColor:[UIColor darkGrayColor]];
//    cell.imageView.image = img;
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  45.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSMutableDictionary* dict = CREATE_MUTABLE_DICTIONARY;
    if(tableView == self.searchDisplayController.searchResultsTableView){
        dict = [self.searchArray objectAtIndex:indexPath.row];
    } else{
        dict = [_countryArray objectAtIndex:indexPath.row];
    }
    
    NSString* countryName = [dict objectForKey:@"country"];
    NSString* countryCode = [dict objectForKey:@"countryCode"];
    
    
    if(self.mode==0){
        [self.delegate didSelectNations:countryName code:countryCode];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
 //       [DIALOG_MANAGER showLoadingView];
        
//        [API_MANAGER updateUserInfo:@{@"countryCode":countryCode} completion:^(BOOL success) {
//            if(success){
//                [DEFAULTS setObject:countryCode forKey:COUNTRY_CODE];
//                [DEFAULTS synchronize];
//                [self.delegate didSelectNations:countryName code:countryCode];
//                [self.navigationController popViewControllerAnimated:YES];
//            }else{
//                [DIALOG_MANAGER showNetworkFailToast];
//            }
//        }];
      
    }
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    tableView.rowHeight = 45.0f; // or some other height
}

# pragma mark - UISearchDisplayDelegate
-(void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    [self.searchArray removeAllObjects];
}
- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView  {
    tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT);
}

//-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
//{
//    [self.searchArray removeAllObjects];
//    [self.tableView reloadData];
//
//}


-(BOOL) searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self.searchArray removeAllObjects];
    
    NSString* query = LOCALIZE(searchString);
    DLog(@"%@",query);
    

    
    
    for(NSDictionary* dict in [SINGLETON countryData]){
        NSString* countryName = [LOCALIZE([dict objectForKey:@"country"]) lowercaseString];
        NSString* a=[NSString stringWithFormat:@"%@ (+%@) %@",countryName,[dict objectForKey:@"countryCode"],[dict[@"country"] lowercaseString]];

        NSString* lowercaseQuery = [query lowercaseString];
        NSRange search = [a rangeOfString:lowercaseQuery];
        
        DLog(@"%@ %@",lowercaseQuery,countryName);
        
        if(search.location!=NSNotFound){
                [self.searchArray addObject:dict];
        }
    }
    //[self.searchDisplay.searchResultsTableView reloadData];
    return YES;
}


// IOS bug fix
- (void)searchDisplayController:(UISearchDisplayController *)controller didHideSearchResultsTableView:(UITableView *)tableView {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide) name:UIKeyboardWillHideNotification object:nil];
}

- (void) keyboardWillHide {
    UITableView *tableView = [[self searchDisplayController] searchResultsTableView];
    [tableView setContentInset:UIEdgeInsetsZero];
    [tableView setScrollIndicatorInsets:UIEdgeInsetsZero];
    tableView.scrollEnabled = YES;
}
@end
