//
//  StartV3ViewController.h
//  Story17
//
//  Created by POPO on 8/5/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "WeiboSDK.h"
#import "FacebookManager.h"

@interface StartV3ViewController : UIViewController <TencentSessionDelegate,FacebookManagerDelegate>

@end
