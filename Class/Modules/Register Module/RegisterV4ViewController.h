//
//  RegisterV3ViewController.h
//  Story17
//
//  Created by POPO on 2015/8/6.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageTextField.h"

typedef NS_ENUM(NSUInteger, REGISTER_V4_MODE){
    USER_NAME_MODE_V4,
    FACEBOOK_MODE_V4,
    PASSWORD_MODE_V4,
    FULLNAME_MODE_V4,
    USER_IMAGE_MODE_V4,
};

typedef NS_ENUM(NSUInteger, REGISTER_TYPE){
    MSG_MODE,
    FB_MODE,
    WECHAT_MODE,
    WEIBO_MODE,
    QQ_MODE,
};

@class ImageTextField;
@interface RegisterV4ViewController : UIViewController
@property (nonatomic) REGISTER_V4_MODE registerMode;
@property (nonatomic) REGISTER_TYPE registerType;
@property (nonatomic, strong) ImageTextField* imageTextField;
@property (nonatomic, strong) UIButton* backButton;

@end
