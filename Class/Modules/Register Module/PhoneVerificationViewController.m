//
//  ConnectionViewController.m
//  story17
//
//  Created by POPO Chen on 5/20/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "PhoneVerificationViewController.h"
#import "ImageTextField.h"
#import "NationTableViewController.h"
#import "CustomNavbarView.h"
#import "ConnectionUserViewController.h"
#import "Constant.h"
#import "UserSuggestionViewController.h"
#import "UIImage+Helper.h"
#import "RegisterV4ViewController.h"
#import <GTFramework/GTFramework.h>
#import "AFNetworking.h"
#import "SettingsViewController.h"
#import "LoginV4ViewController.h"
#import "NBPhoneNumberUtil.h"


@interface PhoneVerificationViewController() <NationTableDelegate, UITextFieldDelegate, UINavigationBarDelegate, GTManageDelegate>
@property (nonatomic,strong) UIScrollView* mainScrollView;

@property (nonatomic,strong) ImageTextField* usernameTextView;
@property (nonatomic,strong) ImageTextField* pwdTextView;
@property (nonatomic,strong) UIButton* confirmPwdBtn;

@property (nonatomic,strong) NSString *requestID;
@property (nonatomic,strong) NSString *resetPasswordToken;
@property (nonatomic,strong) UIButton* countryButton;
@property (nonatomic,strong) ImageTextField* phoneImageTextField;
@property (nonatomic, strong) ImageTextField* usernameTextField;

@property (nonatomic,strong) UILabel* verificationLabel;
@property (nonatomic,strong) ImageTextField* verificationCodeTextView;

@property (nonatomic,strong) UIButton* confirmButton;
@property (nonatomic,strong) UIButton* resendButton;

@property (nonatomic,strong) UIButton* sendVerifyCodeBtn;
@property (nonatomic,strong) UILabel* phoneVerifyMessage;

@property (nonatomic,strong) NSMutableArray* fbFriendIDArray;

/* Alert View */
@property (nonatomic,strong) UIImageView* alertView;
@property (nonatomic,strong) UILabel* alertLabel;

/* UI View */
@property (nonatomic,strong) CustomNavbarView* navBar;
@property (nonatomic, strong) UIImageView* bgImageView;

@property (nonatomic,strong) NSTimer* resendCodeTimer;

@property (nonatomic, strong) GTManager *gtManager;

@property (assign, nonatomic) CGRect alertFrame;

@property (nonatomic, strong) NSString* nation;

@end

@implementation PhoneVerificationViewController

#define coverPhotoHeight 0
#define leftMargin 8
#define btnHeight [ImageTextField getDefaultHeight]
#define labelHeight 30
#define arrowWidth 12

#define description_font_size 14

#define BUTTON_MARGIN_X 10

#define coolDownTime 60

- (id)init
{
    self = [super init];
    if (self != nil) {
        _status = PHONE_MODE;
    }
    
    return self;
}

- (void)dealloc
{
    [NOTIFICATION_CENTER removeObserver:self];
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [self configureViewForIOS7];

    int nextY = STATUS_BAR_HEIGHT;
    
    _navBar = [[CustomNavbarView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, NAVI_BAR_HEIGHT)];
    [_navBar.titleLabel setTextColor:BLACK_COLOR];
    [_navBar.backBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [_navBar.nextBtn addTarget:self action:@selector(skipAction) forControlEvents:UIControlEventTouchUpInside];
    _navBar.nextBtn.hidden = YES;
    [_navBar setTitleLabel:LOCALIZE(@"PHONE_VERIFICATION")];
    [_navBar setBackgroundColor:WHITE_COLOR];
    nextY += _navBar.frame.size.height+10;
    
    UIImage* screenImg = [UIImage imageByApplyingBlurToImage:[UIImage imageNamed:@"bg5"] withRadius:SCREEN_WIDTH/32 tintColor:[UIColor colorWithRed:255.f green:255.f blue:255.f alpha:0.2f] saturationDeltaFactor:1.f maskImage:nil];
    _bgImageView = [[UIImageView alloc]initWithImage:screenImg];
    _bgImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
    _bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    if (_status != REBIND_PHONE)
    {
        [self.navigationController setNavigationBarHidden:YES];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
        [self.view setBackgroundColor:WHITE_COLOR];
        [self.view addSubview:_bgImageView];
        [self.view addSubview:_navBar];
        
    }
    else if (_status == REBIND_PHONE)
    {
        UIButton* backButton  = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setImage:[SINGLETON loadCachedImage:@"nav_arrow_back" withMethod:LOAD_FROM_BUNDLE] forState:UIControlStateNormal];
        [backButton setImage:[SINGLETON loadCachedImage:@"nav_arrow_back" withMethod:LOAD_FROM_BUNDLE] forState:UIControlStateHighlighted];
        [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
        backButton.frame = CGRectMake(0.0, 0.0, 40, 40);
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        self.title = LOCALIZE(@"ReBindPhoneNumber");
      
        
    }
    
    nextY = 20;
    
    _mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, _navBar.frame.size.height, 3*SCREEN_WIDTH, SCREEN_HEIGHT- _navBar.frame.size.height)];
    [_mainScrollView setBackgroundColor:[UIColor clearColor]];
    
    /* Verification code module */

    _verificationLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH+leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, labelHeight)];
    [_verificationLabel setTextColor:GRAY_COLOR];
    _verificationLabel.font = SYSTEM_FONT_WITH_SIZE(description_font_size);

    if (_status == RESET_PHONE_MODE)
    {
        [_verificationLabel setText:LOCALIZE(@"reset_verification_message")];
    }
    else if (_status == PHONE_MODE)
    {
        [_verificationLabel setText:[NSString stringWithFormat:LOCALIZE(@"verification_message"),@"+886",@"0980167563"]];
    }
    else if (_status == REBIND_PHONE)
    {
        [_verificationLabel setText:[NSString stringWithFormat:LOCALIZE(@"verification_message"),@"+886",@"0980167563"]];
    }
    
    _verificationLabel.textColor = DARK_GRAY_COLOR_TWO;
    
    CGSize stringSize = [_verificationLabel.text getSizeWithConstrainSize:CGSizeMake(_verificationLabel.frame.size.width,90) withFont:SYSTEM_FONT_WITH_SIZE(description_font_size)];
    
    _verificationLabel.frame = CGRectMake(3*SCREEN_WIDTH/2-stringSize.width/2, _verificationLabel.frame.origin.y, stringSize.width, stringSize.height);
    _verificationLabel.textAlignment = NSTextAlignmentCenter;
    _verificationLabel.numberOfLines = 0;
    
    nextY += _verificationLabel.frame.size.height+15;
    

    _verificationCodeTextView = [[ImageTextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH+10, nextY, SCREEN_WIDTH-20, btnHeight)];
    _verificationCodeTextView.mainTextField.keyboardType = UIKeyboardTypeNumberPad;
    [_verificationCodeTextView.bgImage setImage:[[UIImage imageNamed:@"btn_greenline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]];
    [_verificationCodeTextView.mainTextField setTextColor:DARK_GRAY_COLOR_TWO];

    nextY += _verificationCodeTextView.frame.size.height+15;

    
    _confirmButton = [ThemeManager getRegisterNextStepBtn];
    _confirmButton.frame = CGRectMake(SCREEN_WIDTH+10, nextY, SCREEN_WIDTH-20, btnHeight);
    
    nextY += _confirmButton.frame.size.height+10;
    
    [_confirmButton setTitle:LOCALIZE(@"REGISTER_CONFIRM") forState:UIControlStateNormal];
    [_confirmButton addTarget:self action:@selector(connectionAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _resendButton = [[UIButton alloc]init];
    _resendButton.frame = CGRectMake(SCREEN_WIDTH+10, nextY, SCREEN_WIDTH-20, btnHeight);
    [_resendButton setTitleColor:LIGHT_GRAY_COLOR_TWO forState:UIControlStateNormal];
    
    nextY += _resendButton.frame.size.height;
    
    [_resendButton addTarget:self action:@selector(resendVerificationCode:) forControlEvents:UIControlEventTouchUpInside];
    
    /* Mobile Info module */
    nextY = 30 + BUTTON_MARGIN_X;
    
    _usernameTextField = [[ImageTextField alloc] initWithFrame:CGRectMake(BUTTON_MARGIN_X, nextY, SCREEN_WIDTH, [ImageTextField getDefaultHeight])];
    _usernameTextField.mainTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _usernameTextField.mainTextField.tintColor = BLACK_COLOR;
    [_usernameTextField.bgImage setImage:[IMAGE_FROM_BUNDLE(@"btn_grayline") resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]];
    [_usernameTextField.mainTextField setTextColor:BLACK_COLOR];
    
    _usernameTextField.mainTextField.delegate = self;
    [_usernameTextField.headImage setImage:[UIImage imageNamed:@"lod_username"]];
    _usernameTextField.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"REGISTER_ENTER_USERNAME") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];
    
    if (_status == RESET_PHONE_MODE)
    {
        [_mainScrollView addSubview:_usernameTextField];
        nextY += BUTTON_MARGIN_X+_usernameTextField.frame.size.height;
        _verificationCodeTextView.headImage.image = [UIImage imageNamed:@"lod_code"];
        _verificationCodeTextView.mainTextField.placeholder = LOCALIZE(@"enter_resetcode");
        [_resendButton setTitle:LOCALIZE(@"resend_verification_code") forState:UIControlStateNormal];
        
    }
    else if (_status == PHONE_MODE || _status == REBIND_PHONE)
    {
        _verificationCodeTextView.headImage.image = [UIImage imageNamed:@"lod_code"];
        _verificationCodeTextView.mainTextField.placeholder = LOCALIZE(@"enter_verification_code");
        
        if(_status == REBIND_PHONE){
            nextY = 0;
        }
    }

    _countryButton = [[UIButton alloc]initWithFrame:CGRectMake(10, nextY, 80, 30)];
    [_countryButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [_countryButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
    [_countryButton setImage:IMAGE_FROM_BUNDLE(@"downward_black") forState:UIControlStateNormal];
    [_countryButton addTarget:self action:@selector(didClickNation:) forControlEvents:UIControlEventTouchUpInside];
    [_countryButton setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
    _countryButton.titleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [_countryButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_countryButton sizeToFit];
    _countryButton.frame = CGRectMake(_countryButton.frame.origin.x, _countryButton.frame.origin.y, _countryButton.frame.size.width+30, _countryButton.frame.size.height);
    [_countryButton setImageEdgeInsets:UIEdgeInsetsMake(0, CGRectGetWidth(_countryButton.frame)-25, 0, 0)];
    
    nextY += _countryButton.frame.size.height+5;
    
    _phoneImageTextField = [[ImageTextField alloc]initWithFrame:CGRectMake(10, nextY, SCREEN_WIDTH-20, btnHeight)];
    _phoneImageTextField.mainTextField.keyboardType = UIKeyboardTypeNumberPad;
    [_phoneImageTextField.headImage setImage:[UIImage imageNamed:@"lod_phone"]];
    _phoneImageTextField.mainTextField.placeholder = LOCALIZE(@"Phone_number");
    [_phoneImageTextField.mainTextField setTextColor:BLACK_COLOR];
    [_phoneImageTextField.bgImage setImage:[[UIImage imageNamed:@"btn_greenline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]];
    [_phoneImageTextField changeToPhoneMode];
    
    [_phoneImageTextField.countryCodeLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickNation:)]];
    _phoneImageTextField.countryCodeLabel.userInteractionEnabled = YES;
    _phoneImageTextField.mainTextField.font = SYSTEM_FONT_WITH_SIZE(16);
    _phoneImageTextField.countryCodeLabel.textColor = DARK_GRAY_COLOR_TWO;
    _phoneImageTextField.verticalLine.backgroundColor = DARK_GRAY_COLOR_TWO;
    
    if (_status == REBIND_PHONE)
    {
        _phoneImageTextField.mainTextField.text = [DEFAULTS objectForKey:PHONE_NUM];
    }
    
    nextY += _phoneImageTextField.frame.size.height+5;
    
    _sendVerifyCodeBtn = [ThemeManager getRegisterControlBtn];
    _sendVerifyCodeBtn.frame = CGRectMake(10, nextY, SCREEN_WIDTH-20, [ImageTextField getDefaultHeight]);
    nextY += _sendVerifyCodeBtn.frame.size.height+20;
    
    [_sendVerifyCodeBtn setTitle:LOCALIZE(@"send_verification_code") forState:UIControlStateNormal];
    [_sendVerifyCodeBtn addTarget:self action:@selector(connectionAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _phoneVerifyMessage = [[UILabel alloc]initWithFrame:CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, labelHeight)];
    [_phoneVerifyMessage setTextColor:GRAY_COLOR];
    _phoneVerifyMessage.font = SYSTEM_FONT_WITH_SIZE(description_font_size);
    
    if (_status == RESET_PHONE_MODE) {
        [_phoneVerifyMessage setText:LOCALIZE(@"phone_verification_message")];
    }
    else if (_status == PHONE_MODE) {
        [_phoneVerifyMessage setText:LOCALIZE(@"phone_verification_message")];
    }
    else if (_status == REBIND_PHONE) {
        [_phoneVerifyMessage setText:LOCALIZE(@"phone_verification_message")];
    }
    
    CGSize stringSize2 = [_phoneVerifyMessage.text getSizeWithConstrainSize:CGSizeMake(_phoneVerifyMessage.frame.size.width,90) withFont:SYSTEM_FONT_WITH_SIZE(description_font_size)];
    
    _phoneVerifyMessage.frame = CGRectMake(SCREEN_WIDTH/2-stringSize2.width/2, _phoneVerifyMessage.frame.origin.y, stringSize2.width, stringSize2.height);
    _phoneVerifyMessage.textAlignment = NSTextAlignmentCenter;
    _phoneVerifyMessage.numberOfLines = 0;
    nextY += _phoneVerifyMessage.frame.size.height+15;
    _phoneVerifyMessage.textColor = DARK_GRAY_COLOR_TWO;

    
    /* Reset Password module */
    
    nextY = 30 + BUTTON_MARGIN_X;
    
    _usernameTextView = [[ImageTextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*2+10, nextY, SCREEN_WIDTH-20, btnHeight)];
    _usernameTextView.mainTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _usernameTextView.mainTextField.tintColor = GRAY_COLOR;
    [_usernameTextView.bgImage setImage:[IMAGE_FROM_BUNDLE(@"btn_grayline") resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]];
    [_usernameTextView.mainTextField setTextColor:BLACK_COLOR];
    
    _usernameTextView.mainTextField.delegate = self;
    [_usernameTextView.headImage setImage:[UIImage imageNamed:@"lod_username"]];
    _usernameTextView.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"REGISTER_ENTER_USERNAME") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];
    
    nextY += _usernameTextView.frame.size.height+5;

    
    _pwdTextView = [[ImageTextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*2+10, nextY, SCREEN_WIDTH-20, btnHeight)];
    _pwdTextView.mainTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _pwdTextView.mainTextField.tintColor = BLACK_COLOR;
    [_pwdTextView.bgImage setImage:[IMAGE_FROM_BUNDLE(@"btn_grayline") resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]];
    [_pwdTextView.mainTextField setTextColor:BLACK_COLOR];
    
    _pwdTextView.mainTextField.delegate = self;
    [_pwdTextView.headImage setImage:[UIImage imageNamed:@"lod_password"]];
    _pwdTextView.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"REGISTER_ENTER_PWD") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];

    nextY += _pwdTextView.frame.size.height+5;

    _confirmPwdBtn = [ThemeManager getRegisterNextStepBtn];
    _confirmPwdBtn.frame = CGRectMake(2*SCREEN_WIDTH+10, nextY, SCREEN_WIDTH-20, btnHeight);
    nextY += _confirmPwdBtn.frame.size.height+10;
    
    [_confirmPwdBtn setTitle:LOCALIZE(@"REGISTER_CONFIRM") forState:UIControlStateNormal];
    [_confirmPwdBtn addTarget:self action:@selector(connectionAction:) forControlEvents:UIControlEventTouchUpInside];

    [_mainScrollView addSubview:_confirmButton];
    [_mainScrollView addSubview:_resendButton];

    [_mainScrollView addSubview:_verificationCodeTextView];
    
    [_mainScrollView addSubview:_verificationLabel];
    [_mainScrollView addSubview:_countryButton];
    [_mainScrollView addSubview:_phoneImageTextField];

    [_mainScrollView addSubview:_phoneVerifyMessage];
    [_mainScrollView addSubview:_sendVerifyCodeBtn];

    [_mainScrollView addSubview:_usernameTextView];
    [_mainScrollView addSubview:_pwdTextView];
    [_mainScrollView addSubview:_confirmPwdBtn];

    
    if (_status == RESET_PHONE_MODE)
    {
        [_phoneVerifyMessage removeFromSuperview];
    }
    
    [self.view addSubview:_mainScrollView];
    
    _alertView = [ThemeManager getAlertImageView];
    _alertLabel = [UILabel new];
    _alertLabel.textAlignment = NSTextAlignmentCenter;
    _alertLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [_alertLabel setTextColor:DARK_GRAY_COLOR];
    [_alertView addSubview:_alertLabel];
    
    [self detectIPGeoLocation];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [NOTIFICATION_CENTER addObserver:self
                            selector:@selector(keyboardDidShow:)
                                name:UIKeyboardDidShowNotification
                              object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(keyboardNotificationHandler:) name: UIKeyboardDidChangeFrameNotification object:nil];

 
    if (_status != REBIND_PHONE) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        
    }
    else {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        [_phoneImageTextField.mainTextField becomeFirstResponder];
    }

    
    
    if (_status == VERIFY_MODE || _status == RESET_VERIFY_MODE) {
        [EVENT_HANDLER addEventTracking:@"EnterBindPhonePage" withDict:nil];
        [_mainScrollView setContentOffset:CGPointMake(SCREEN_WIDTH, 0) animated:NO];
        
    }
    else if (_status == PHONE_MODE || _status == RESET_PHONE_MODE) {
        
        [_phoneImageTextField.mainTextField becomeFirstResponder];
        [_mainScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [NOTIFICATION_CENTER removeObserver:self name:UIKeyboardDidChangeFrameNotification object:nil];
    [NOTIFICATION_CENTER removeObserver:self name:UIKeyboardDidShowNotification object:nil];

    if (_resendCodeTimer != nil) {
        
        [_resendCodeTimer invalidate];
        _resendCodeTimer = nil;
        
    }
}


- (void)viewDidDisappear:(BOOL)animated
{
    [NOTIFICATION_CENTER removeObserver:self
                                   name:UIKeyboardDidShowNotification
                                 object:nil];
}



- (NSString *)humanTestTypeString:(HumanTestType)humanTestType
{
    switch (humanTestType) {
        case HumanTestTypeGetResetPasswordToken:
        {
            return @"getResetPasswordToken";
        }
            break;
            
        case HumanTestTypeSendPhoneVerificationCode:
        {
            return @"sendPhoneVerificationCode";
        }
            break;
    }
}

- (void)callHumanTestMappingMethodWithType:(HumanTestType)humanTestType
{
    switch (humanTestType) {
        case HumanTestTypeGetResetPasswordToken:
        {
            [self getResetPasswordTokenAPI];
        }
            break;
            
        case HumanTestTypeSendPhoneVerificationCode:
        {
            [self checkPhoneNumberUsage];
        }
            break;
    }
}

#pragma mark - UI


- (void)resendCountDown:(id)sender
{
    
    
    int lastVerifyTime = [GET_DEFAULT(LAST_VERIFICATION_TIME) intValue];
    int currentTime = CURRENT_TIMESTAMP;
    
    
    
    if (currentTime-lastVerifyTime < coolDownTime)
    {
        NSString *temp = [NSString stringWithFormat:LOCALIZE(@"After %@ resend verification code."),[self timeFormatted:coolDownTime-(currentTime-lastVerifyTime)]];
        [_resendButton setTitle:temp forState:UIControlStateNormal];
    }
    else {
        
        if(_status == RESET_VERIFY_MODE)
        {
            [_resendButton setTitle:LOCALIZE(@"resend_verification") forState:UIControlStateNormal];
        }
        else if(_status == VERIFY_MODE)
        {
            [_resendButton setTitle:LOCALIZE(@"resend_verification") forState:UIControlStateNormal];
            
            
            NSArray *objects = [[NSArray alloc] initWithObjects:self.resendButton.titleLabel.textColor, [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
            NSArray *keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
            
            NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
            
            NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:self.resendButton.titleLabel.text attributes:linkAttributes];
            
            [self.resendButton.titleLabel setAttributedText:attributedString];
            
        }
        else if (_status == REBIND_PHONE_ENTER_VERFICODE)
        {
            [_resendButton setTitle:LOCALIZE(@"resend_verification") forState:UIControlStateNormal];
            
            NSArray *objects = [[NSArray alloc] initWithObjects:self.resendButton.titleLabel.textColor, [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
            NSArray *keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
            
            NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
            
            NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:self.resendButton.titleLabel.text attributes:linkAttributes];
            
            [self.resendButton.titleLabel setAttributedText:attributedString];
            
        }
        
        if (_resendCodeTimer != nil)
        {
            [_resendCodeTimer invalidate];
            _resendCodeTimer = nil;
        }
        [_resendButton setSelected:NO];
    }
    
}


- (void)resignKeyboard
{
    if (_usernameTextField.mainTextField.isFirstResponder)
    {
        [_usernameTextField.mainTextField resignFirstResponder];
    }
    else if (_pwdTextView.mainTextField.isFirstResponder)
    {
        [_pwdTextView.mainTextField resignFirstResponder];
    }
    else if (_phoneImageTextField.mainTextField.isFirstResponder)
    {
        [_phoneImageTextField.mainTextField resignFirstResponder];
    }
    else if (_usernameTextView.mainTextField.isFirstResponder)
    {
        [_usernameTextView.mainTextField resignFirstResponder];
    }
    else if (_verificationCodeTextView.mainTextField.isFirstResponder)
    {
        [_verificationCodeTextView.mainTextField resignFirstResponder];
    }
}
#pragma mark - Notifications

- (void)keyboardDidShow:(NSNotification*)notification
{
//    NSDictionary *keyboardInfo = [notification userInfo];
//    NSValue *keyboardFrameEnd = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
//    CGRect keyboardFrameEndRect = [keyboardFrameEnd CGRectValue];
//    _alertView.frame = CGRectMake(0,
//                                  keyboardFrameEndRect.origin.y + CGRectGetHeight(_alertView.frame),
//                                  SCREEN_WIDTH,
//                                  -CGRectGetHeight(_alertView.frame));
//    self.alertFrame = _alertView.frame;
}

-(void)keyboardNotificationHandler:(NSNotification*)notification
{
    
    NSDictionary *keyboardInfo = [notification userInfo];
    NSValue *keyboardFrameEnd = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameEndRect = [keyboardFrameEnd CGRectValue];
//    NSLog(@"鍵盤的位置： %@",NSStringFromCGRect(keyboardFrameEndRect));

    _alertView.frame = CGRectMake(0,
                                  keyboardFrameEndRect.origin.y,
                                  SCREEN_WIDTH,
                                  CGRectGetHeight(_alertView.frame));
    
    if(_status == PHONE_MODE)
    {
        _alertView.frame = CGRectMake(0,
                                      keyboardFrameEndRect.origin.y,
                                      SCREEN_WIDTH,
                                      CGRectGetHeight(_alertView.frame));
    }
    
    self.alertFrame = _alertView.frame;

}
#pragma mark - Alert View

- (void)showAlertViewOfSendVerification
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LOCALIZE(@"YourEnterPhoneNumber") message:[NSString stringWithFormat:@"+%@ %@",GET_DEFAULT(COUNTRY_CODE),GET_DEFAULT(PHONE_NUM)] preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:LOCALIZE(@"OK")
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [DIALOG_MANAGER showLoadingViewQicker];
                                                          [_phoneImageTextField.mainTextField becomeFirstResponder];
                                                          [self humanTestIfNeededWithType:HumanTestTypeSendPhoneVerificationCode];
                                                          
                                                      }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:LOCALIZE(@"Cancel")
                                                        style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction *action) {
                                                          [_phoneImageTextField.mainTextField becomeFirstResponder];
                                                      }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


- (void)showAlertViewMessage:(NSString*)message
{
    [_alertView.layer removeAllAnimations];
    [self.view addSubview:_alertView];
    
    _alertLabel.frame = _alertView.bounds;
    [_alertLabel setText:message];
    _alertLabel.center = CGPointMake(SCREEN_WIDTH / 2, CGRectGetHeight(_alertView.frame) / 2);
    
    _alertView.alpha = 0;
//    NSLog(@"對話位置的起點：%@ alertFrame : %@ ",NSStringFromCGRect(_alertView.frame),NSStringFromCGRect(_alertFrame));
    
    [UIView animateWithDuration:0.4 animations:^{
        
        if (_status == REBIND_PHONE || _status == REBIND_PHONE_ENTER_VERFICODE )
        {
            _alertView.frame = CGRectOffset(_alertFrame, 0, -CGRectGetHeight(_alertFrame)-60);
            _alertView.alpha = 1;
//            NSLog(@"對話位置中間停留點 in ：%@",NSStringFromCGRect(_alertView.frame));
            

        }else
        {
            _alertView.frame = CGRectOffset(_alertFrame, 0, -CGRectGetHeight(_alertFrame));
            _alertView.alpha = 1;
        }
//        NSLog(@"對話位置中間停留點：%@",NSStringFromCGRect(_alertView.frame));
        
    } completion:^(BOOL finished){
        if (finished)
        {
            [UIView animateWithDuration:0.1 delay:0.9 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
                _alertView.frame = CGRectOffset(_alertFrame, 0, CGRectGetHeight(_alertFrame));
                _alertView.alpha = 0;
//                NSLog(@"對話的位置終點：%@",NSStringFromCGRect(_alertView.frame));
                
            } completion:^(BOOL finished) {
                if (finished) {
                    [_alertView removeFromSuperview];
                    
                }
            }];
        }
        
    }];
}



- (void)showAlertViewOfResetPasswordError:(NSString*)message
{
    
    //    [self resignKeyboard];
    
    NSString *content;
    
    if ([message isEqualToString:@"new_password_confirm_error"])
    {
        content = LOCALIZE(@"new_password_confirm_error");
    }
    else if([message isEqualToString:@"resetPasswordToken_timeout"] || [message isEqualToString:@"resetPasswordToken_used"])
    {
        content = LOCALIZE(@"Reset password timeout.\nPlease Try again.");
    }
    else if([message isEqualToString:@"maximum_count_exceeds"])
    {
        content = LOCALIZE(@"Exceed maximum trials,once in 6 hours");
    }
    else
    {
        [DIALOG_MANAGER showNetworkFailToast];
        return;
    }
    
    [DIALOG_MANAGER hideLoadingView];

    
    [self showAlertViewMessage:content];
    
    
}

#pragma mark - Mode Change

- (void)modeTransform2ResetPwd
{
    [_navBar.titleLabel setText:LOCALIZE(@"reset_new_password")];
    _status = RESET_PASSWORD;
    [_usernameTextView.mainTextField becomeFirstResponder];
    [_mainScrollView setContentOffset:CGPointMake(2*SCREEN_WIDTH, 0) animated:YES];
    
    [_usernameTextView.headImage setImage:[UIImage imageNamed:@"lod_password"]];
    _usernameTextView.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"REGISTER_ENTER_PWD") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];
    
    [_pwdTextView.headImage setImage:[UIImage imageNamed:@"lod_password"]];
    _pwdTextView.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"confirm_new_password") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];
    
    _usernameTextView.mainTextField.keyboardType = UIKeyboardTypeASCIICapable;
    _pwdTextView.mainTextField.keyboardType = UIKeyboardTypeASCIICapable;

    [self passwordSecurityCheck];
}


- (void)modeTransform2ResetVerifyMode
{
    _status = RESET_VERIFY_MODE;
    [_navBar.titleLabel setText:LOCALIZE(@"enter_resetcode")];
    self.resendCodeTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(resendCountDown:) userInfo:nil repeats:YES];
    [_verificationCodeTextView.mainTextField becomeFirstResponder];
    [_mainScrollView setContentOffset:CGPointMake(SCREEN_WIDTH, 0) animated:YES];
    [DEFAULTS setObject:INT_TO_STRING(CURRENT_TIMESTAMP) forKey:LAST_VERIFICATION_TIME];
    [DEFAULTS synchronize];

    
    [self passwordSecurityCheck];

}

- (void)modeTransform2VerifyMode
{
    [_navBar.titleLabel setText:LOCALIZE(@"enter_verification_code")];
    
    if (_status == REBIND_PHONE)
    {
        _status = REBIND_PHONE_ENTER_VERFICODE;
        self.title = LOCALIZE(@"enter_verification_code");
    }
    else
    {
        _status = VERIFY_MODE;
    }
    
    self.resendCodeTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(resendCountDown:) userInfo:nil repeats:YES];
    [_resendButton setSelected:YES];
    [_verificationLabel setText:[NSString stringWithFormat:LOCALIZE(@"verification_message"),GET_DEFAULT(COUNTRY_CODE),GET_DEFAULT(PHONE_NUM)]];
    [_verificationCodeTextView.mainTextField becomeFirstResponder];
    [_mainScrollView setContentOffset:CGPointMake(SCREEN_WIDTH, 0) animated:YES];
    
    [DEFAULTS setObject:INT_TO_STRING(CURRENT_TIMESTAMP) forKey:LAST_VERIFICATION_TIME];
    [DEFAULTS synchronize];

    
    [self passwordSecurityCheck];

}

- (void)modeTransform2ResetPhoneMode
{
    [_navBar.titleLabel setText:LOCALIZE(@"PHONE_VERIFICATION")];
    [_phoneImageTextField.mainTextField becomeFirstResponder];
    [_mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    
    
    [self passwordSecurityCheck];

}

- (void)modeTransform2PhoneMode
{
    [_phoneImageTextField.mainTextField becomeFirstResponder];
    [_navBar.titleLabel setText:LOCALIZE(@"PHONE_VERIFICATION")];
    [_mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [self passwordSecurityCheck];

}

#pragma mark - Tool

- (void)didSelectNations:(NSString*)nation code:(NSString*)code;
{
    [_phoneImageTextField.countryCodeLabel setText:[NSString stringWithFormat:@"+%@",code]];
    [_countryButton setTitle:LOCALIZE(nation) forState:UIControlStateNormal];
    [_countryButton sizeToFit];
    _countryButton.frame = CGRectMake(_countryButton.frame.origin.x, _countryButton.frame.origin.y, _countryButton.frame.size.width+30, _countryButton.frame.size.height);
    [_countryButton setImageEdgeInsets:UIEdgeInsetsMake(0, CGRectGetWidth(_countryButton.frame)-25, 0, 0)];
    
    _nation = nation;
    
    [DEFAULTS setObject:code forKey:COUNTRY_CODE];
    [DEFAULTS synchronize];
    
    [self passwordSecurityCheck];

}



- (void) detectIPGeoLocation
{
    DLog(@"detectIPGeolocation");
    
    if(_status==REBIND_PHONE){
        
        _nation = GET_DEFAULT(PHONE_TWO_DIGIT);

        NSString* phoneCode = [SINGLETON countryCode2PhoneCode][_nation];
        [_phoneImageTextField.countryCodeLabel setText:[NSString stringWithFormat:@"+%@",phoneCode]];
        [_countryButton setTitle:LOCALIZE(_nation) forState:UIControlStateNormal];
        [_countryButton sizeToFit];
        _countryButton.frame = CGRectMake(_countryButton.frame.origin.x, _countryButton.frame.origin.y, _countryButton.frame.size.width+30, _countryButton.frame.size.height);
        [_countryButton setImageEdgeInsets:UIEdgeInsetsMake(0, CGRectGetWidth(_countryButton.frame)-25, 0, 0)];
           
    }else{
        
        [SINGLETON detectIpGeoLocation:^(BOOL success, NSDictionary *jsonDict) {
            
            if(success){
                
                DLog(@"detectIPGeolocation success");
                _nation = jsonDict[@"countryCode"];
            } else {
                DLog(@"detectIPGeolocation fail");
#ifdef INTERNATIONAL_VERSION
                _nation = @"TW";
#else
                _nation = @"CN";
#endif
            }
#ifdef INTERNATIONAL_VERSION
#else
            _nation = @"CN";
#endif
            NSString* phoneCode = [SINGLETON countryCode2PhoneCode][_nation];
            [_phoneImageTextField.countryCodeLabel setText:[NSString stringWithFormat:@"+%@",phoneCode]];
            [_countryButton setTitle:LOCALIZE(_nation) forState:UIControlStateNormal];
            [_countryButton sizeToFit];
            _countryButton.frame = CGRectMake(_countryButton.frame.origin.x, _countryButton.frame.origin.y, _countryButton.frame.size.width+30, _countryButton.frame.size.height);
            [_countryButton setImageEdgeInsets:UIEdgeInsetsMake(0, CGRectGetWidth(_countryButton.frame)-25, 0, 0)];
            
            // in 17app, countryCode this means phoneCode
            //        DLog(@"_nation:%@",_nation);
            //        DLog(@"phoneCode:%@",phoneCode);
            
            if([GET_DEFAULT(COUNTRY_CODE) isEqualToString:@""]){
                [DEFAULTS setObject:_nation forKey:COUNTRY_CODE];
                [DEFAULTS synchronize];
            }
        }];
        
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

- (NSString*)validPhoneNumer:(NSString*)inputPhoneNumber
{
    if(inputPhoneNumber.length==0)
        return @"";
    
    NSString* validPhone = [inputPhoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@"(" withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@")" withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@"+" withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
    validPhone = [validPhone stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    NSString* firstIndex = [validPhone substringToIndex:1];
    if([firstIndex isEqualToString:@"0"]){
        validPhone = [validPhone substringWithRange:NSMakeRange(1, [validPhone length]-1)];
    }
    return validPhone;
}



- (NSString *)timeFormatted:(int)totalSeconds
{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}


-(void)passwordSecurityCheck
{
    switch (_status)
    {
        case RESET_PASSWORD:
            _usernameTextView.mainTextField.secureTextEntry = YES;
            _pwdTextView.mainTextField.secureTextEntry = YES;
            break;
            
        default:
            _usernameTextView.mainTextField.secureTextEntry = NO;
            _pwdTextView.mainTextField.secureTextEntry = NO;
            break;
         
    }
}

#pragma mark - API

- (void)humanTestIfNeededWithType:(HumanTestType)humanTestType
{
    
    [API_MANAGER checkHumanTestWithType:[self humanTestTypeString:humanTestType] completion:^(BOOL success, BOOL isNeeded) {
        if (success)
        {
            if (isNeeded)
            {
                [self requestGTestWithType:humanTestType];
            }
            else
            {
                [self callHumanTestMappingMethodWithType:humanTestType];
            }
        }
        else
        {
            [DIALOG_MANAGER hideLoadingView];
        }
    }];
}

-(void)checkPhoneNumberUsage
{
    NSString *countryCode = GET_DEFAULT(COUNTRY_CODE);
    NSString *phoneNumber = GET_DEFAULT(PHONE_NUM);
    
    NSString* type =@"";
    
    if(_status==PHONE_MODE)
    {
        type = @"register";
        
    }else if(_status==RESET_PHONE_MODE)
    {
        type = @"resetPassword";
        
    }else if(_status==REBIND_PHONE)
    {
        type = @"changePhoneNumber";
        
    }else
    {
        return;
    }
    
    
    if(_status != RESET_PHONE_MODE)
    {
        if(_status == PHONE_MODE)
        {
            [DIALOG_MANAGER showLoadingView];
            [API_MANAGER isPhoneNumberUsed:countryCode andPhoneNum:phoneNumber completion:^(BOOL success, NSString *message,BOOL isUsed) {
                
                [DIALOG_MANAGER hideLoadingView];
                if(success)
                {
                    if(isUsed)
                    {
                        [self showAlertViewMessage:LOCALIZE(@"phone_number_used")];
                    }else
                    {
                        [self sendVerifactionAPI:type];
                    }
                }else
                {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
            
        }else if(_status == REBIND_PHONE)
        {
            [self sendVerifactionAPI:type];
        }
        
    }else
    {
        [DIALOG_MANAGER showLoadingView];

        [API_MANAGER isPhoneNumberOpenIDPaired:countryCode localPhoneNumber:phoneNumber openID:GET_DEFAULT(USER_OPEN_ID) completion:^(BOOL success, NSString *message, BOOL isPaired) {
            [DIALOG_MANAGER hideLoadingView];

            if(success)
            {
                if(isPaired)
                {
                    [self sendVerifactionAPI:type];
                }else
                {
                    [self showAlertViewMessage:LOCALIZE(@"Phone number is not consistent.")];
                }
            }else
            {
                if([message isEqualToString:@""])
                {
                    [DIALOG_MANAGER showNetworkFailToast];
                }else
                {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
                
            }
        }];
    }
}

- (void)sendVerifactionAPI:(NSString*)type
{
    NSString *countryCode = GET_DEFAULT(COUNTRY_CODE);
    NSString *phoneNumber = GET_DEFAULT(PHONE_NUM);
 
    [API_MANAGER sendPhoneVerificationCode:countryCode phoneNumber:phoneNumber type:type completion:^(BOOL success, NSString *message, NSString *requestId) {
        
        [DIALOG_MANAGER hideLoadingView];
        if (success)
        {
            if (_status == RESET_PHONE_MODE)
            {
                [self modeTransform2ResetVerifyMode];
            }
            else if (_status == PHONE_MODE)
            {
                [self modeTransform2VerifyMode];
            }
            else if (_status == REBIND_PHONE)
            {
                [self modeTransform2VerifyMode];
            }
            
            _requestID = requestId;
            
            DLog(@"request id - %@", requestId);
        }
        else
        {
            DLog(@"sendPhoneVerificationCode failed - %@", message);
            [DIALOG_MANAGER hideLoadingView];
            
        
            if ([message isEqualToString:@"ip_rate_limit_exceeds"])
            {
                [self requestGTestWithType:HumanTestTypeSendPhoneVerificationCode];
            }
            else if([message isEqualToString:@"phoneNumber_used"])
            {
                [self showAlertViewMessage:LOCALIZE(@"phoneNumber_used")];
            }
            else if([message isEqualToString:@"openID_phoneNumber_not_paired"])
            {
                [self showAlertViewMessage:LOCALIZE(@"openID_phoneNumber_not_paired")];
            }
            else
            {
                [self showAlertViewMessage:LOCALIZE(@"Please try again later.")];
            }
        }
    }];
    
    
}

- (void)registerByPhoneActionAPI
{
    
    [DIALOG_MANAGER showLoadingView];
    
    NSString *countryCode = GET_DEFAULT(COUNTRY_CODE);
    NSString *phoneNumber = GET_DEFAULT(PHONE_NUM);
    NSString *password =  GET_DEFAULT(PASSWORD);
    NSString *openID =  GET_DEFAULT(USER_OPEN_ID);
    NSString *requestID = _requestID;
    NSString *verificationCode = _verificationCodeTextView.mainTextField.text;
    
    NSDictionary* dict;
    
    
    if([GET_DEFAULT(LOGIN_MODE) isEqualToString:FACEBOOK_LOGIN_MODE])
    {
        
        dict = @{@"facebookID":GET_DEFAULT(FACEBOOK_ID),@"facebookAccessToken":GET_DEFAULT(FACEBOOK_TOKEN)};
        
    }else if([GET_DEFAULT(LOGIN_MODE) isEqualToString:WECHAT_LOGIN_MODE])
    {

        dict = @{@"wechatID":GET_DEFAULT(WECHAT_USER_ID),@"wechatAccessToken":GET_DEFAULT(WECHAT_TOKEN)};

    }else if([GET_DEFAULT(LOGIN_MODE) isEqualToString:WEIBO_LOGIN_MODE])
    {

        dict = @{@"weiboID":GET_DEFAULT(WEIBO_USER_ID),@"weiboAccessToken":GET_DEFAULT(WEIBO_ACCESSTOKEN)};

    }else if([GET_DEFAULT(LOGIN_MODE) isEqualToString:QQ_LOGIN_MODE])
    {

        dict = @{@"qqID":GET_DEFAULT(QQ_ID),@"qqAccessToken":GET_DEFAULT(QQ_TOKEN)};

    }else if([GET_DEFAULT(LOGIN_MODE) isEqualToString:MSG_LOGIN_MODE])
    {

        dict = @{};

    }
    
    
    [API_MANAGER registerByPhoneAction:openID password:password requestID:requestID countryCode:countryCode phoneNumber:phoneNumber verificationCode:verificationCode phoneCountry:_nation optionalDict:dict completion:^(BOOL success, NSString *message, NSString *accesToken, NSDictionary *userInfo) {
        
        [DIALOG_MANAGER hideLoadingView];

        if (success)
        {
            [EVENT_HANDLER addEventTracking:@"BindPhone" withDict:@{@"phoneNumber":[NSString stringWithFormat:@"+%@%@",countryCode,phoneNumber]}];
            [EVENT_HANDLER addEventTracking:@"SuccessfulRegister" withDict:nil];

          if(_status == VERIFY_MODE)
          {
                [self gotoRegisterV4ViewController];
          }
            
        }else
        {
//            [self resignKeyboard];

            NSString *content;
            
            if ([message isEqualToString:@"verificationCode_error"])
            {
                content = LOCALIZE(@"verification_verificationCode_error_message");
                
            }
            else if([message isEqualToString:@"verificationCode_timeout"])
            {
                content = LOCALIZE(@"Verification code was inactivated.");
            }
            else
            {
                [DIALOG_MANAGER showNetworkFailToast];
                return;
            }
            
            
            [DIALOG_MANAGER hideLoadingView];
            [self showAlertViewMessage:content];
        }
    }];

}

-(void)changePhoneNumberAPI
{
    [DIALOG_MANAGER showLoadingView];
    
    NSString *countryCode = GET_DEFAULT(COUNTRY_CODE);
    NSString *phoneNumber = GET_DEFAULT(PHONE_NUM);
    NSString *openID =  GET_DEFAULT(USER_OPEN_ID);
    NSString *requestID = _requestID;
    NSString *verificationCode = _verificationCodeTextView.mainTextField.text;
    
    
    [API_MANAGER changePhoneNumber:openID requestID:requestID countryCode:countryCode phoneNumber:phoneNumber verificationCode:verificationCode phoneCountry:_nation completion:^(BOOL success, NSString *message) {
        [DIALOG_MANAGER hideLoadingView];
        
        if (success)
        {
            [DIALOG_MANAGER showCompleteToast];
            [DEFAULTS setObject:phoneNumber forKey:PHONE_NUM];
            
            NSString *openID2 =  GET_DEFAULT(USER_OPEN_ID);

            [self gobackToSettingViewController];
        }
        else
        {
            [self resignKeyboard];
            [_phoneImageTextField.mainTextField becomeFirstResponder];

            NSString *content;
            
            if ([message isEqualToString:@"verificationCode_error"])
            {
                content = LOCALIZE(@"verification_verificationCode_error_message");
            }
            else if([message isEqualToString:@"verificationCode_timeout"])
            {
                content = LOCALIZE(@"Verification code was inactivated.");
            }
            else if([message isEqualToString:@"maximum_count_exceeds"])
            {
                content = LOCALIZE(@"Exceed maximum trials,once in 6 hours");
            }
            else if([message isEqualToString:@"verificationCode_used"])
            {
                content = LOCALIZE(@"Verification code used.Please resend SMS.");
            }
            else
            {
                [DIALOG_MANAGER showNetworkFailToast];
                return;
            }
            
            
            [DIALOG_MANAGER hideLoadingView];
            
            [self showAlertViewMessage:content];
        }
    }];
    
}


- (void)getResetPasswordTokenAPI
{
    
    NSString *countryCode = GET_DEFAULT(COUNTRY_CODE);
    NSString *phoneNumber = GET_DEFAULT(PHONE_NUM);
    NSString *openID =  GET_DEFAULT(USER_OPEN_ID);
    NSString *requestID = _requestID;
    NSString *verificationCode = _verificationCodeTextView.mainTextField.text;

    [API_MANAGER getResetPasswordToken:countryCode openID:openID requestID:requestID phoneNumber:phoneNumber verificationCode:verificationCode withCompletion:^(BOOL success, NSString *message, NSString *resetPasswordToken) {
        
        if (success) {
            [DIALOG_MANAGER hideLoadingView];
            
            DLog(@"sendPhoneVerificationCode success - %@", message);

            [self modeTransform2ResetPwd];
            _resetPasswordToken = resetPasswordToken;
            
            DLog(@"Reset password token - %@", resetPasswordToken);
        }
        else {
            DLog(@"sendPhoneVerificationCode failed - %@", message);
            
            if ([message isEqualToString:@"ip_rate_limit_exceeds"])
            {
                [self requestGTestWithType:HumanTestTypeGetResetPasswordToken];
            }
            else
            {
//                [self resignKeyboard];

                NSString *content;
                
                if ([message isEqualToString:@"verificationCode_error"]){
                    
                    content = LOCALIZE(@"verification_verificationCode_error_message");
                    
                }else if([message isEqualToString:@"verificationCode_timeout"]){
                    
                    content = LOCALIZE(@"Verification code was inactivated.");
                }else if ([message isEqualToString:@"openID_phoneNumber_not_paired"]){
                    
                    content = LOCALIZE(@"Incorrect number, please input the phone number you signed up.");
                }else if ([message isEqualToString:@"verificationCode_used"]){
                    
                    content = LOCALIZE(@"Verification code used.Please resend SMS.");
                }
                else{
                    [DIALOG_MANAGER showNetworkFailToast];
                    return;
                }
                
                
                [DIALOG_MANAGER hideLoadingView];
                
                [self showAlertViewMessage:content];
        
            }
        }
        
    }];
   
}

- (void)resetPasswordAPI
{
    
    [DIALOG_MANAGER showLoadingView];


    NSString *newPassword = _pwdTextView.mainTextField.text;
    NSString *confirmNewPassword = _usernameTextView.mainTextField.text;
    
    [API_MANAGER resetPassword:_resetPasswordToken newPassword:newPassword confirmNewPassword:confirmNewPassword withCompletion:^(BOOL success, NSString *message) {
        
        [DIALOG_MANAGER hideLoadingView];

        if (success) {
            
            [DIALOG_MANAGER showCompleteToast];

            [self gobackToLoginV4ViewController];
            
        }else {
            [self showAlertViewOfResetPasswordError:message];
        }
    }];

}

-(void)loginActionAPI{
    
    NSDictionary *loginDic = [[NSDictionary alloc]init];
    
    NSString *password =  GET_DEFAULT(PASSWORD);
    NSString *openID =  GET_DEFAULT(USER_OPEN_ID);
    
    loginDic = @{@"password":password ,@"openID":openID};
    
    [API_MANAGER loginAction:loginDic completion:^(BOOL success, NSString *message) {
        
        if(success)
        {
//      [self.navigationController dismissViewControllerAnimated:NO completion:nil];
        [SINGLETON setupMainTabBarViewController];

        }
    }];
}


#pragma mark - Navigation

- (void)gotoRegisterV4ViewController
{
    RegisterV4ViewController *regVC = [RegisterV4ViewController new];
    regVC.registerMode = FULLNAME_MODE_V4;
    [self.navigationController pushViewController:regVC animated:YES];
}

- (void)gobackToSettingViewController
{
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[SettingsViewController class]]) {
            [self.navigationController popToViewController:aViewController animated:NO];
        }
    }
}

- (void)gobackToLoginV4ViewController
{
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[LoginV4ViewController class]]) {
            [self.navigationController popToViewController:aViewController animated:NO];
        }
    }
}


#pragma mark - Override

- (GTManager *)gtManager
{
    if (!_gtManager) {
        _gtManager = [GTManager sharedGTManger];
        [_gtManager debugModeEnable:NO];
        [_gtManager setGTDelegate:self];
        _gtManager.backgroundAlpha = 0.2;
        _gtManager.cornerViewShadow = NO;
        _gtManager.colorWithHexInt = 0x0a0a0a;
        
        NSString *language = [[[NSBundle mainBundle] preferredLocalizations] firstObject];
        if ([language isEqualToString:@"zh-Hant"]) {
            [_gtManager languageSwitch:LANGTYPE_ZH_TW];
        }
        else if ([language isEqualToString:@"zh-Hans"]) {
            [_gtManager languageSwitch:LANGTYPE_ZH_CN];
        }
        else {
            [_gtManager languageSwitch:LANGTYPE_EN_US];
        }

    }
    return _gtManager;
}

#pragma mark - Geetest

- (void)requestGTestWithType:(HumanTestType)humanTestType
{
    __weak __typeof(self) weakSelf = self;
    
    NSURL *requestGTestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@getHumanTestParams", SERVER_IP]];
    
    [self.gtManager requestCustomServerForGTest:requestGTestURL
                              timeoutInterval:15.0
                           withHTTPCookieName:@"msid"
                                      options:GTDefaultSynchronousRequest
                            completionHandler:^(NSString *gt_captcha_id, NSString *gt_challenge, NSNumber *gt_success_code) {
                                NSLog(@"sessionID === %@",self.gtManager.sessionID);
                                
                                [DIALOG_MANAGER hideLoadingView];
                                [self resignKeyboard];

                                if ([gt_success_code intValue] == 1) {
                                    
                                    if (gt_captcha_id.length == 32) {
                                        
                                        [weakSelf.gtManager openGTViewAddFinishHandler:^(NSString *code, NSDictionary *result, NSString *message) {
                                            if ([code isEqualToString:@"1"]) {
                                                
                                                [weakSelf seconderyValidate:code result:result message:message type:humanTestType];
                                            } else {
                                                NSLog(@"client captcha failed:\ncode :%@ message:%@ result:%@", code, message, result);
                                            }
                                            
                                        } closeHandler:^{
                                            NSLog(@"close geetest");
                                            
                                        } animated:YES];
                                        
                                    }
                                    else {
                                        NSLog(@"invalid geetest ID, please set right ID");
                                    }
                                }
                                else {
                                    // @TODO Failback solution
                                    
                                    UIAlertView *warning = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                                                      message:@"Geetest server connection failed."
                                                                                     delegate:self
                                                                            cancelButtonTitle:@"OK"
                                                                            otherButtonTitles:nil, nil];
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [warning show];
                                    });
                                    NSLog(@"Geetest server connection failed.");
                                    
                                }
                            }];
}

- (void)seconderyValidate:(NSString *)code result:(NSDictionary *)result message:(NSString *)message type:(HumanTestType)humanTestType
{
    if (code && result) {
        
        [DIALOG_MANAGER showLoadingView];
        
        [API_MANAGER humanTestSeconderyValidateWithType:[self humanTestTypeString:humanTestType] result:result completion:^(BOOL success) {

            if (success) {
                [self callHumanTestMappingMethodWithType:humanTestType];
            }
            else {
                [DIALOG_MANAGER hideLoadingView];

//                [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"Verify failed") message:LOCALIZE(@"Please try again later.") buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
//                    
//                }];
                
                [self showAlertViewMessage:LOCALIZE(@"Please try again later.") ];
            }
        }];
    }
}

#pragma mark - GTManageDelegate

- (void)GTNetworkErrorHandler:(NSError *)error
{
    NSLog(@"GTNetWork Error: %@",error.localizedDescription);
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"网络错误"
                                                         message:error.localizedDescription
                                                        delegate:self
                                               cancelButtonTitle:@"确定"
                                               otherButtonTitles:nil, nil];
    dispatch_async(dispatch_get_main_queue(), ^{

        [errorAlert show];
    });
}

#pragma mark - Check Phone Validity

-(void)blockInvalidPhoneNumber:(NSString*)phoneNumber nation:(NSString*)nation
{
    if ([self checkValidPhone:phoneNumber nation:_nation])
    {
//        NSLog(@"電話號碼驗證格式已過%@ %@" ,_nation , phoneNumber);
        [self showAlertViewOfSendVerification];
    }else
    {
        [self showAlertViewMessage:LOCALIZE(@"Invalid phone number format.")];
//        NSLog(@"電話號碼驗證格式錯誤: %@ %@" ,_nation , phoneNumber);
    }
}

-(BOOL)checkValidPhone:(NSString*)phoneNumber nation:(NSString*)nation
{
    
    //Google SDK 可判斷 valid phone number
    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
    NSError *anError = nil;
    NBPhoneNumber *myNumber = [phoneUtil parse:phoneNumber
                                 defaultRegion:_nation
                                         error:&anError];
    
    //電話號碼長度一定要超過 6 碼
    if (phoneNumber.length > 6)
    {
        if ([phoneUtil isValidNumber:myNumber])
        {
            return true;
        }
    }

   
    return false;
}




#pragma mark - Actions

- (void)connectionAction:(id)sender
{
    
    
    if ([sender isEqual:_confirmButton])
    {
        if (_status == RESET_VERIFY_MODE)
        {
            // reset password mode
            [self humanTestIfNeededWithType:HumanTestTypeGetResetPasswordToken];
        }
        else if(_status == VERIFY_MODE)
        {
            /* Register and fill info */
            [self registerByPhoneActionAPI];
        }
        else if (_status == REBIND_PHONE_ENTER_VERFICODE)
        {
            [self changePhoneNumberAPI];
        }
        
    }
    else if ([sender isEqual:_sendVerifyCodeBtn]) //傳送驗證碼
    {
        if (_phoneImageTextField.mainTextField.text.length == 0)
        {
            [self showAlertViewMessage:LOCALIZE(@"phone_should_not_empty")];
            return;
        }

        if(_status == RESET_PHONE_MODE)
        {
            if (_usernameTextField.mainTextField.text.length == 0)
            {
                [self showAlertViewMessage:LOCALIZE(@"username_should_not_empty")];
                return;
            }
            
            [API_MANAGER checkOpenIDAvailable:_usernameTextField.mainTextField.text completion:^(BOOL success, NSString *message) {
                if (success)
                {
                    [self showAlertViewMessage:LOCALIZE(@"username_is_not_availabel")];
                    return;
                }
                else
                {
                    int lastVerifyTime = [GET_DEFAULT(LAST_VERIFICATION_TIME) intValue];
                    int currentTime = CURRENT_TIMESTAMP;
                    if(currentTime-lastVerifyTime > coolDownTime)
                    {
                        [DEFAULTS setObject:INT_TO_STRING(CURRENT_TIMESTAMP) forKey:LAST_VERIFICATION_TIME];
                        NSString *temp = [NSString stringWithFormat:LOCALIZE(@"After %@ resend verification code."),[self timeFormatted:coolDownTime-(currentTime-lastVerifyTime)]];
                        [_resendButton setTitle:temp forState:UIControlStateNormal];

                        
                    }
                    
                    [DEFAULTS setObject:[self validPhoneNumer:_phoneImageTextField.mainTextField.text] forKey:PHONE_NUM];
                    [DEFAULTS setObject:_usernameTextField.mainTextField.text forKey:USER_OPEN_ID];
                    NSString *countryCode = [_phoneImageTextField.countryCodeLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
                    NSString *phoneNumber = [self validPhoneNumer:_phoneImageTextField.mainTextField.text];
                    [DEFAULTS setObject:phoneNumber forKey:PHONE_NUM];
                    [DEFAULTS setObject:countryCode forKey:COUNTRY_CODE];
                    [DEFAULTS synchronize];
                    
                    [self blockInvalidPhoneNumber:phoneNumber nation:_nation];
            
                }
            }];
            
        }
        else if (_status == PHONE_MODE || _status == REBIND_PHONE)
        {
            
            [DEFAULTS setObject:[self validPhoneNumer:_phoneImageTextField.mainTextField.text] forKey:PHONE_NUM];
            
            [DEFAULTS setObject:INT_TO_STRING(CURRENT_TIMESTAMP) forKey:LAST_VERIFICATION_TIME];
            
            
            NSString *temp = [NSString stringWithFormat:LOCALIZE(@"After %@ resend verification code."),[self timeFormatted:coolDownTime]];
            [_resendButton setTitle:temp forState:UIControlStateNormal];

            [DEFAULTS synchronize];
            if (_status == PHONE_MODE)
            {
                [_verificationLabel setText:[NSString stringWithFormat:LOCALIZE(@"verification_message"),GET_DEFAULT(COUNTRY_CODE),GET_DEFAULT(PHONE_NUM)]];
            }
            else if (_status == REBIND_PHONE)
            {
                [_verificationLabel setText:[NSString stringWithFormat:LOCALIZE(@"verification_message"),GET_DEFAULT(COUNTRY_CODE),GET_DEFAULT(PHONE_NUM)]];
            }
            

            
            NSString *countryCode = [_phoneImageTextField.countryCodeLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
            NSString *phoneNumber = [self validPhoneNumer:_phoneImageTextField.mainTextField.text];
            
            [DEFAULTS setObject:phoneNumber forKey:PHONE_NUM];
            [DEFAULTS setObject:countryCode forKey:COUNTRY_CODE];
            NSString* totalPhoneNum = [NSString stringWithFormat:@"%@%@",GET_DEFAULT(COUNTRY_CODE),GET_DEFAULT(PHONE_NUM)];
            [DEFAULTS setObject:totalPhoneNum forKey:TOTAL_PHONE_NUM];
            [DEFAULTS synchronize];
            
            
            [self blockInvalidPhoneNumber:phoneNumber nation:_nation];
            
        }
        
    }
    else if ([sender isEqual:_confirmPwdBtn]) //重設密碼按鈕
    {
        
        NSString *newPassword = _pwdTextView.mainTextField.text;
        NSString *confirmNewPassword = _usernameTextView.mainTextField.text;
        
        if (_usernameTextView.mainTextField.text.length < 5)
        {
            [self showAlertViewMessage:LOCALIZE(@"Password should longer than five characters")];
            return;
        }
        
        if (_usernameTextView.mainTextField.text.length > 20) {
            [self showAlertViewMessage:LOCALIZE(@"Password should smaller than twenty characters")];
            return;
        }
        
        if (![newPassword isEqualToString:confirmNewPassword])
        {
            [self showAlertViewMessage:LOCALIZE(@"new_password_confirm_error")];
            return;
        }
     
        [self resetPasswordAPI];
    }
}

- (void)didClickNation:(id)sender
{
    NationTableViewController* nVC = [NationTableViewController new];
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:nVC];
    nVC.delegate = self;
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)skipAction:(id)sender
{
    
    if (_status == VERIFY_MODE)
    {
        UserSuggestionViewController* userSugVC = [UserSuggestionViewController new];
        [self.navigationController pushViewController:userSugVC animated:YES];
        
    }
    else if (_status == PHONE_MODE)
    {
        [_phoneImageTextField.mainTextField resignFirstResponder];
        [self modeTransform2VerifyMode];
    }
}

- (void)resendVerificationCode:(id)sender
{
    if (_resendButton.isSelected)
    {
        return;
    }
    /* Block quick clicks */
    [self humanTestIfNeededWithType:HumanTestTypeSendPhoneVerificationCode];
    
    NSString *temp = [NSString stringWithFormat:LOCALIZE(@"After %@ resend verification code."),[self timeFormatted:coolDownTime]];
    [_resendButton setTitle:temp forState:UIControlStateNormal];
    
    self.resendCodeTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(resendCountDown:) userInfo:nil repeats:YES];
    [_resendButton setSelected:YES];
    [DEFAULTS setObject:INT_TO_STRING(CURRENT_TIMESTAMP) forKey:LAST_VERIFICATION_TIME];
    [DEFAULTS synchronize];
}


- (void)backAction
{
    if (_status == PHONE_MODE || _status == RESET_PHONE_MODE)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(_status == VERIFY_MODE)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LOCALIZE(@"smsissending") message:LOCALIZE(@"confirmtoback") preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:LOCALIZE(@"OK")
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action) {
                                                    [self modeTransform2PhoneMode];
                                                    _status = PHONE_MODE;
                                                }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:LOCALIZE(@"Cancel")
                                                  style:UIAlertActionStyleCancel
                                                handler:^(UIAlertAction *action) {
                                                    
                                                }]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if(_status == RESET_VERIFY_MODE)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LOCALIZE(@"smsissending") message:LOCALIZE(@"confirmtoback") preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:LOCALIZE(@"OK")
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action) {
                                                    [self modeTransform2ResetPhoneMode];
                                                    _status = RESET_PHONE_MODE;
                                                }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:LOCALIZE(@"Cancel")
                                                  style:UIAlertActionStyleCancel
                                                handler:^(UIAlertAction *action) {
                                                    
                                                }]];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else if(_status == RESET_PASSWORD)
    {
        [self modeTransform2ResetVerifyMode];
        _status = RESET_VERIFY_MODE;
        
    }
    else if(_status == REBIND_PHONE)
    {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else if(_status == REBIND_PHONE_ENTER_VERFICODE)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LOCALIZE(@"smsissending") message:LOCALIZE(@"confirmtoback") preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:LOCALIZE(@"OK")
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action) {
                                                    [self modeTransform2PhoneMode];
                                                    self.title=LOCALIZE(@"ReBindPhoneNumber");
                                                    _status = REBIND_PHONE;
                                                }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:LOCALIZE(@"Cancel")
                                                  style:UIAlertActionStyleCancel
                                                handler:^(UIAlertAction *action) {
                                                    
                                                }]];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
}

# pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
   
    if (_status == RESET_PASSWORD) {
       
        if (textField == _pwdTextView.mainTextField) {
            if(_pwdTextView.mainTextField.text.length >= PASSWORD_MAX && string.length!=0) {
                return NO;
            }
        }
      
        if (textField == _usernameTextView.mainTextField) {
            if(_usernameTextView.mainTextField.text.length >= PASSWORD_MAX && string.length!=0) {
                return NO;
            }
        }

        
    }

    return YES;
}


@end
