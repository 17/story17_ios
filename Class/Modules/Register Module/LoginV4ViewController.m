//
//  LoginV3ViewController.m
//  Story17
//
//  Created by POPO on 8/5/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "LoginV4ViewController.h"
#import "Constant.h"
#import "ImageTextField.h"
#import "CustomNavbarView.h"
#import "UIImage+Helper.h"
#import "RegisterV4ViewController.h"
#import "PhoneVerificationViewController.h"
#import "WeiboSDK.h"
#import "WXApiManager.h"

#define BUTTON_MARGIN_X 10
#define IMAGE_WIDTH 100
#define ALERT_VIEW_Y SCREEN_HEIGHT-KEYBOARD_HEIGHT

@interface LoginV4ViewController() <UIScrollViewDelegate, FacebookManagerDelegate>

@property (nonatomic,strong) CustomNavbarView* navBar;
@property (nonatomic, strong) UIImageView* bgImageView;
@property (nonatomic, strong) UIButton* facebookLoginButton;
@property (nonatomic, strong) UIButton* wechatLoginButton;
@property (nonatomic, strong) UIButton* weiboLoginButton;
@property (nonatomic, strong) UIButton* qqLoginButton;


@property (nonatomic, strong) ImageTextField* usernameTextField;
@property (nonatomic, strong) ImageTextField* pwdTextField;
@property (nonatomic, strong) UILabel* resetPwdLabel;
@property (nonatomic, strong) UIButton* doneButton;

// alert
@property (nonatomic, strong) UIImageView* alertView;
@property (nonatomic, strong) UILabel* alertLabel;

@property (nonatomic, strong) NSString* fbAccessToken;
@property (nonatomic, strong) NSString* facebookID;
@property (nonatomic, strong) NSString* myProfilePicture;

@property (assign, nonatomic) CGRect alertFrame;

@end

@implementation LoginV4ViewController

- (id)init
{
    self = [super init];
    if (self) {
        _loginMode = LOGIN_V4_MODE_LOGIN;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];

    [self.view setBackgroundColor:WHITE_COLOR];
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    scrollView.contentSize = self.view.bounds.size;
    scrollView.bounces = YES;
    scrollView.alwaysBounceVertical = YES;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    
    int nextY = STATUS_BAR_HEIGHT ;
    
    _navBar = [[CustomNavbarView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, NAVI_BAR_HEIGHT)];
    [_navBar.titleLabel setTextColor:BLACK_COLOR];
    [_navBar.backBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [_navBar.nextBtn addTarget:self action:@selector(skipAction:) forControlEvents:UIControlEventTouchUpInside];
    _navBar.nextBtn.hidden = YES;
    [_navBar setBackgroundColor:WHITE_COLOR];
    nextY += _navBar.frame.size.height+10;
    
    UIImage* screenImg = [UIImage imageByApplyingBlurToImage:[UIImage imageNamed:@"bg5"] withRadius:SCREEN_WIDTH/32 tintColor:[UIColor colorWithRed:255.f green:255.f blue:255.f alpha:0.2f] saturationDeltaFactor:1.f maskImage:nil];
    _bgImageView = [[UIImageView alloc]initWithImage:screenImg];
    _bgImageView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, SCREEN_HEIGHT-nextY);
    _bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    [scrollView addSubview:_bgImageView];
    [scrollView addSubview:_navBar];

    
    nextY += 5;
    
    _usernameTextField = [[ImageTextField alloc] initWithFrame:CGRectMake(BUTTON_MARGIN_X, nextY, SCREEN_WIDTH, [ImageTextField getDefaultHeight])];
    _usernameTextField.mainTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _usernameTextField.mainTextField.tintColor = BLACK_COLOR;
    [_usernameTextField.bgImage setImage:[IMAGE_FROM_BUNDLE(@"btn_grayline") resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]];
    [_usernameTextField.mainTextField setTextColor:BLACK_COLOR];
    
    _usernameTextField.mainTextField.delegate = self;
    [scrollView addSubview:_usernameTextField];
    
    nextY += BUTTON_MARGIN_X+_usernameTextField.frame.size.height;
    
    _pwdTextField = [[ImageTextField alloc] initWithFrame:CGRectMake(BUTTON_MARGIN_X, nextY, SCREEN_WIDTH, [ImageTextField getDefaultHeight])];
    _pwdTextField.mainTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _pwdTextField.mainTextField.tintColor = BLACK_COLOR;
    [_pwdTextField.bgImage setImage:[IMAGE_FROM_BUNDLE(@"btn_grayline") resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]];
    [_pwdTextField.mainTextField setTextColor:BLACK_COLOR];
    _pwdTextField.mainTextField.delegate = self;
    [scrollView addSubview:_pwdTextField];
    
    nextY += BUTTON_MARGIN_X+_pwdTextField.frame.size.height+5;
    
    if (_loginMode == LOGIN_V4_MODE_LOGIN) {
        
        _resetPwdLabel = [UILabel new];
        _resetPwdLabel.numberOfLines = 1;
        _resetPwdLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        _resetPwdLabel.textAlignment = NSTextAlignmentCenter;
        _resetPwdLabel.text = LOCALIZE(@"REGISTER_TITLE_RESET_PWD");
        [_resetPwdLabel setTextColor:BLACK_COLOR];
        [_resetPwdLabel sizeToFit];
        _resetPwdLabel.center = CGPointMake(SCREEN_WIDTH/2, nextY+_resetPwdLabel.frame.size.height/2);
        _resetPwdLabel.hidden = NO;
        _resetPwdLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *resetPwdTapGesture =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchResetPwdLabel:)];
        [_resetPwdLabel addGestureRecognizer:resetPwdTapGesture];
        [scrollView addSubview:_resetPwdLabel];
        //
        nextY += BUTTON_MARGIN_X+_resetPwdLabel.frame.size.height+5;
    }
    
    _doneButton = [ThemeManager getRegisterControlBtn];
    _doneButton.center = CGPointMake(SCREEN_WIDTH/2, nextY+_doneButton.frame.size.height/2);
    [_doneButton addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [_doneButton.titleLabel setFont:BOLD_FONT_WITH_SIZE(18)];

    [scrollView addSubview:_doneButton];
   
    
    nextY += _doneButton.frame.size.height + 10;
    
    UILabel *orLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2 - 15, nextY, 30, 20)];
    orLabel.text = LOCALIZE(@"or");
    orLabel.textAlignment = NSTextAlignmentCenter;
    [scrollView addSubview:orLabel];
    
    UILabel *leftLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2 - 15 - SCREEN_WIDTH*(85)/(320) -10 , nextY + 10, SCREEN_WIDTH*(85)/(320), 1)];
    leftLineLabel.backgroundColor = LIGHT_GRAY_COLOR;
    [scrollView addSubview:leftLineLabel];

    UILabel *rightLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(leftLineLabel.frame.origin.x + leftLineLabel.frame.size.width + 10 + orLabel.frame.size.width + 10 , nextY + 10, SCREEN_WIDTH*(85)/(320), 1)];
    rightLineLabel.backgroundColor = LIGHT_GRAY_COLOR;
    [scrollView addSubview:rightLineLabel];
   
    
    nextY += orLabel.frame.size.height + 10;
    
    
    // alert view
    _alertView = [ThemeManager getAlertImageView];
    _alertLabel = [UILabel new];
    _alertLabel.textAlignment = NSTextAlignmentCenter;
    _alertLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [_alertLabel setTextColor:DARK_GRAY_COLOR];
    
    [_alertView addSubview:_alertLabel];
    
    if (_loginMode == LOGIN_V4_MODE_LOGIN) {
        
        [_doneButton setTitle:LOCALIZE(@"login") forState:UIControlStateNormal];
        [_usernameTextField.headImage setImage:[UIImage imageNamed:@"lod_username"]];
        [_pwdTextField.headImage setImage:[UIImage imageNamed:@"lod_password"]];
        _usernameTextField.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"REGISTER_ENTER_USERNAME") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];
        _pwdTextField.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"REGISTER_ENTER_PWD") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];
        self.title = LOCALIZE(@"login");
        _pwdTextField.mainTextField.secureTextEntry = YES;
        [_navBar.titleLabel setText:LOCALIZE(@"login")];
        
        
        _qqLoginButton = [ThemeManager getRegisterCircleBtn];
        _qqLoginButton.center = CGPointMake(SCREEN_WIDTH/2, nextY+30);
        [_qqLoginButton setImage:[UIImage imageNamed:@"register_qq"] forState:UIControlStateNormal];
        [_qqLoginButton bk_addEventHandler:^(id sender) {
            
            [[LoginHandler sharedInstance] loginWithType:qqLoginMode withViewController:self];
            
//            [_tencentOAuth authorize:[NSArray arrayWithObjects:@"get_user_info", @"get_simple_userinfo", @"add_t", nil] inSafari:NO];
        
        } forControlEvents:UIControlEventTouchUpInside];
        
        _wechatLoginButton = [ThemeManager getRegisterCircleBtn];
        _wechatLoginButton.center = CGPointMake(3*SCREEN_WIDTH/4+_wechatLoginButton.frame.size.width/4, nextY+30);
        [_wechatLoginButton setImage:[UIImage imageNamed:@"register_wechat"] forState:UIControlStateNormal];
        [_wechatLoginButton bk_addEventHandler:^(id sender) {

            [[LoginHandler sharedInstance] loginWithType:wechatLoginMode withViewController:self];
            
        } forControlEvents:UIControlEventTouchUpInside];

        
        _weiboLoginButton = [ThemeManager getRegisterCircleBtn];
        _weiboLoginButton.center = CGPointMake(SCREEN_WIDTH/4-_weiboLoginButton.frame.size.width/4, nextY+30);
        [_weiboLoginButton setImage:[UIImage imageNamed:@"register_weibo"] forState:UIControlStateNormal];
        [_weiboLoginButton bk_addEventHandler:^(id sender) {
            
            [self resignKeyboard];
            [[LoginHandler sharedInstance] loginWithType:weiboLoginMode withViewController:self];

        } forControlEvents:UIControlEventTouchUpInside];
        
        
        
        _facebookLoginButton = [ThemeManager getRegisterControlBtn];
        
        _facebookLoginButton.center = CGPointMake(SCREEN_WIDTH/2,nextY+_facebookLoginButton.frame.size.height/2);
        [_facebookLoginButton setTitle:LOCALIZE(@"REGISTER_FB_LOGIN") forState:UIControlStateNormal];
        [_facebookLoginButton.titleLabel setFont:BOLD_FONT_WITH_SIZE(18)];
        [_facebookLoginButton setImage:[UIImage imageNamed:@"lod_facebook"] forState:UIControlStateNormal];
        
        [_facebookLoginButton bk_addEventHandler:^(id sender) {
            
            [FACEBOOK_MANAGER loginAction:self];
            [FACEBOOK_MANAGER setDelegate:self];
            
        } forControlEvents:UIControlEventTouchUpInside];
        

//        _facebookLoginButton = [ThemeManager getRegisterCircleBtn];
//        _facebookLoginButton.center = CGPointMake(7*SCREEN_WIDTH/8, nextY+30);
//        [_facebookLoginButton setImage:[UIImage imageNamed:@"register_facebook"] forState:UIControlStateNormal];
//        
//        [_facebookLoginButton bk_addEventHandler:^(id sender) {
//            
//            [FACEBOOK_MANAGER setDelegate:self];
//            [FACEBOOK_MANAGER loginAction:self];
//            
//            
//            
//        } forControlEvents:UIControlEventTouchUpInside];
//        
        #ifdef  INTERNATIONAL_VERSION
            [scrollView addSubview:_facebookLoginButton];
        #else
            [scrollView addSubview:_wechatLoginButton];
            [scrollView addSubview:_qqLoginButton];
            [scrollView addSubview:_weiboLoginButton];
        #endif


    } else if (_loginMode == LOGIN_V4_MODE_FORGET_PASSWORD) {
        
        [_doneButton setTitle:LOCALIZE(@"login") forState:UIControlStateNormal];
        [_usernameTextField.headImage setImage:[UIImage imageNamed:@"lod_username"]];
        [_pwdTextField.headImage setImage:[UIImage imageNamed:@"lod_email"]];
        _usernameTextField.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"REGISTER_ENTER_USERNAME") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];
        _pwdTextField.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"REGISTER_ENTER_EMAIL") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];
        self.title = LOCALIZE(@"forgetPassword");
        [_navBar.titleLabel setText:LOCALIZE(@"forgetPassword")];

    }
   
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [LoginHandler sharedInstance].delegate = self;
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    [NOTIFICATION_CENTER addObserver:self
                            selector:@selector(keyboardDidShow:)
                            name:UIKeyboardDidShowNotification
                            object:nil];
    
    [_usernameTextField.mainTextField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    NSArray *viewControllers = self.navigationController.viewControllers;
    if (viewControllers.count > 1 && [viewControllers objectAtIndex:viewControllers.count-2] == self) {
        
    } else if ([viewControllers indexOfObject:self] == NSNotFound) {
        
        if(_loginMode==LOGIN_V4_MODE_LOGIN){
            [self resignKeyboard];
        }
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [NOTIFICATION_CENTER removeObserver:self name:UIKeyboardDidShowNotification object:nil];
}

- (void)resignKeyboard
{
    if (_usernameTextField.mainTextField.isFirstResponder) {
        [_usernameTextField.mainTextField resignFirstResponder];
    }
    else if (_pwdTextField.mainTextField.isFirstResponder) {
        [_pwdTextField.mainTextField resignFirstResponder];
    }
}

- (void)loginWithDict:(NSDictionary*)dict
{
    if ([dict[@"openID"] isEqualToString:@""])
    {
        [_doneButton setSelected:NO];
        [self showAlertViewMessage:LOCALIZE(@"Please enter user name")];
    }
    else if ([dict[@"password"] isEqualToString:@""])
    {
        [_doneButton setSelected:NO];
        [self showAlertViewMessage:LOCALIZE(@"Please enter password")];
    }
    else
    {
        [DIALOG_MANAGER showLoadingView];
        
        [API_MANAGER loginAction2:dict completion:^(BOOL success, NSString *message) {
            
            [DIALOG_MANAGER hideLoadingView];
            [_doneButton setSelected:NO];
            
            if(success) {
                
                if([message isEqualToString:@"password_wrong"]) {
                    
                    [self showAlertViewMessage:LOCALIZE(@"wrong_password")];
                
                }else{
                    
                    [DEFAULTS setObject:dict[@"password"] forKey:@"PASSWORD"];
                    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
                    [SINGLETON setupMainTabBarViewController];
                }
            } else {
                
                DLog(@"message=%@", message);
                if([message isEqualToString:@"password_wrong"]) {
                    
                    [self showAlertViewMessage:LOCALIZE(@"wrong_password")];
                  
                }
                else if([message isEqualToString:@"freeze"]) {
                    
                    [self showAlertViewMessage:LOCALIZE(@"is_freezed")];
                    
                }
                else {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }
        }];
    }
}

-(void)forgetPasswordAction
{
    NSString* openID = _usernameTextField.mainTextField.text;
    NSString* email = _pwdTextField.mainTextField.text;
    
    if(![openID isEqualToString:@""] && ![email isEqualToString:@""]){
        
        [DIALOG_MANAGER showLoadingView];
        [API_MANAGER recoverPassword:openID email:email completion:^(BOOL success, NSString *message) {
            
            [DIALOG_MANAGER hideLoadingView];
            [_doneButton setSelected:NO];
            
            if(success){
                [DIALOG_MANAGER showCompleteToast];
                [DIALOG_MANAGER showActionSheetOKDialogTitle:@"" message:LOCALIZE(@"reset_password_complete") buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                
            }else{
                [DIALOG_MANAGER showActionSheetOKDialogTitle:@"" message:LOCALIZE(@"reset_password_alert") buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                    [_pwdTextField.mainTextField becomeFirstResponder];
                }];
            }
        }];
    }
}

#pragma mark - alert message
- (void)showAlertViewMessage:(NSString*)message
{
    [_alertView.layer removeAllAnimations];
    [self.view addSubview:_alertView];

    _alertLabel.frame = _alertView.bounds;
    
    [_alertLabel setText:message];
    _alertLabel.center = CGPointMake(SCREEN_WIDTH / 2, CGRectGetHeight(_alertView.frame) / 2);
    
    _alertView.alpha = 0;
    [UIView animateWithDuration:0.4 animations:^{
        _alertView.frame = CGRectOffset(_alertFrame, 0, -CGRectGetHeight(_alertFrame));
        _alertView.alpha = 1;
        
    } completion:^(BOOL finished) {
        if (finished)
        {
            [UIView animateWithDuration:0.4 delay:1.6 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
                _alertView.frame = CGRectOffset(_alertFrame, 0, CGRectGetHeight(_alertFrame));
                _alertView.alpha = 0;
                
            } completion:^(BOOL finished)
            {
                if (finished) {
                    [_alertView removeFromSuperview];
                    
                }
            }];
        }

    }];
}

#pragma mark - FacebookManagerDelegate

- (void)facebookManager:(FacebookManager *)facebookManager didLoginComplete:(BOOL)success
{
    if (success)
    {
        [self loginSuccessHandler];
    }
    else
    {
        [self loginFailHandler];
    }
}


# pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _usernameTextField.mainTextField)
    {
        [_pwdTextField.mainTextField becomeFirstResponder];
    }
    else if (textField == _pwdTextField.mainTextField)
    {
        [self doneAction:nil];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _usernameTextField.mainTextField)
    {
        if(_usernameTextField.mainTextField.text.length >= USERNAME_MAX && string.length != 0)
        {
            return NO;
        }
    }
    else if(textField == _pwdTextField.mainTextField && _loginMode != LOGIN_V4_MODE_FORGET_PASSWORD)
    {
        if(_pwdTextField.mainTextField.text.length >= PASSWORD_MAX && string.length!=0)
        {
            return NO;
        }
    }
    return YES;
}

#pragma mark - Notifications
- (void)loginSuccessHandler
{
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link,gender, first_name, last_name, picture.type(large), email, birthday"}]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (error) {
             [DIALOG_MANAGER showNetworkFailToast];
             return ;
         }
         
         NSString* email = result[@"email"];
         NSString* name = result[@"name"];
         NSString* gender = result[@"gender"];
         _facebookID = result[@"id"];
         _fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
         
         if(_fbAccessToken == nil || name == nil || _facebookID == nil) {
             [DIALOG_MANAGER hideLoadingView];
             [DIALOG_MANAGER showSadToastWithMessage:LOCALIZE(@"fb_login_reject_prompt")];
             return ;
         }
         
         if (email == nil) {
             email = @"";
         }
         if (gender == nil) {
             gender = @"";
         }
         
         NSString* profileImageFileName = [SINGLETON generateRandomFileNameWithExtension:@"png"];
         _myProfilePicture = profileImageFileName;
         
         [API_MANAGER checkFacebookIDAvailable:_facebookID completion:^(BOOL success, NSString *message) {
             if (success) {
                 
                 DLog(@"facebook register");
                 
                 // get profile photo
                 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                 NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"myFacebookPic"];
                 
                 AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                 manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                 
                 AFHTTPRequestOperation *op = [manager GET:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", _facebookID] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                     
                     //                        NSString* profileImageFileName = [SINGLETON generateRandomFileNameWithExtension:@"png"];
                     [SINGLETON saveImageWithThumbnailImage:[UIImage imageWithContentsOfFile:path] andFileName:profileImageFileName];
                     self.multiFileUploader = [[MultiFileUploader alloc] initWithFileNameArray:@[profileImageFileName, [NSString stringWithFormat:@"THUMBNAIL_%@", profileImageFileName]]];
                     
                     [self.multiFileUploader startUploadWithCallback:^(NSArray *fileNameArray, int code, float progress) {
                         if(code==MULTI_UPLOAD_SUCCESS) {
                             [DIALOG_MANAGER hideLoadingView];
                         } else if(code==MULTI_UPLOAD_FAIL) {
                             [DIALOG_MANAGER showNetworkFailToast];
                             return ;
                         }
                     }];
                 } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     DELETE_FILE(GET_LOCAL_FILE_PATH(path));
                 }];
                 
                 op.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
                 [op start];
                 
                 [DEFAULTS setObject:[[email componentsSeparatedByString:@"@"] objectAtIndex:0] forKey:USER_OPEN_ID];
                 [DEFAULTS setObject:email forKey:EMAIL];
                 [DEFAULTS setObject:gender forKey:GENDER];
                 [DEFAULTS setObject:profileImageFileName forKey:CACHE_PICTURE];
                 [DEFAULTS setObject:_facebookID forKey:FACEBOOK_ID];
                 [DEFAULTS setObject:_fbAccessToken forKey:FACEBOOK_TOKEN];
                 [DEFAULTS synchronize];
                 
                 /* RegisterInfo */
                 RegisterV4ViewController* regVC = [RegisterV4ViewController new];
                 regVC.registerType = FB_MODE;
                 regVC.registerMode = USER_NAME_MODE_V4;
                 regVC.backButton.hidden = YES;
                 regVC.imageTextField.mainTextField.text = name;
                 [self.navigationController pushViewController:regVC animated:YES];
                 
             } else
             {
                 DLog(@"facebook login");
                 [self loginWithDict:@{@"facebookID": _facebookID, @"facebookToken": _fbAccessToken}];
             }
         }];
     }];
}

- (void)loginFailHandler
{
    
}

- (void)keyboardDidShow:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameEnd = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameEndRect = [keyboardFrameEnd CGRectValue];
    _alertView.frame = CGRectMake(0,
                                  keyboardFrameEndRect.origin.y + CGRectGetHeight(_alertView.frame),
                                  SCREEN_WIDTH,
                                  -CGRectGetHeight(_alertView.frame));
    
    self.alertFrame = _alertView.frame;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self resignKeyboard];
}


#pragma mark LoginHandlerDelegate
- (void) loginSuccess:(BOOL)success loginMode:(loginMode)loginMode userid:(NSString*)userid token:(NSString*)token
{
    if( loginMode == weiboLoginMode ){
    
        if(success){
            
            [DIALOG_MANAGER showLoadingView];
            [API_MANAGER checkWeiboIDAvailable:userid completion:^(BOOL success, NSString *message) {
                [DIALOG_MANAGER hideLoadingView];
                
                if (success) {
                    [[LoginHandler sharedInstance] getUserInfo:weiboLoginMode];
                } else {
                    DLog(@"weibo login");
                    [self loginWithDict:@{@"weiboID": userid,@"weiboAccessToken":token}];
                }
            }];

            
        }else{
        
        }
        
    }else if(loginMode == wechatLoginMode){
    
        if(success){
            [DIALOG_MANAGER showLoadingView];

            [API_MANAGER checkWechatIDAvailable:userid completion:^(BOOL success, NSString *message) {

                [DIALOG_MANAGER hideLoadingView];
                if(success){

                    [[LoginHandler sharedInstance] getUserInfo:wechatLoginMode];
                   
                }else{
                    // wechat login
                    [self loginWithDict:@{@"wechatID": userid,@"wechatAccessToken":token}];
                }
            }];
        }
    }else if( loginMode == qqLoginMode )
    {
        if(success)
        {
            [DIALOG_MANAGER showLoadingView];
            [API_MANAGER checkQQIDAvailable:userid completion:^(BOOL success, NSString *message) {
                
                [DIALOG_MANAGER hideLoadingView];
                if (success)
                {
                    [[LoginHandler sharedInstance] getUserInfo:qqLoginMode];
                }else
                {
                    [self loginWithDict:@{@"qqID":userid,@"qqAccessToken":token}];
                }
            }];
            
        }
    }
}

- (void) requestUserInfo:(BOOL)success loginMode:(loginMode)loginMode
{
    /* RegisterInfo */
    RegisterV4ViewController* regVC = [RegisterV4ViewController new];
    
    if( loginMode == weiboLoginMode ){
        
        regVC.registerType = WEIBO_MODE;

    }else if(loginMode == wechatLoginMode){

        regVC.registerType = WECHAT_MODE;

    }else if(loginMode == qqLoginMode){
        
        regVC.registerType = QQ_MODE;
        
    }
    
    DLog(@"push view controller %ld username:%@",loginMode,GET_DEFAULT(USER_OPEN_ID));
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        regVC.imageTextField.mainTextField.text = GET_DEFAULT(USER_OPEN_ID);
        [self.navigationController pushViewController:regVC animated:YES];
    });


}


#pragma mark - Actions

-(void)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)touchResetPwdLabel:(id)sender
{
    
    PhoneVerificationViewController *conVC = [PhoneVerificationViewController new];
    conVC.status = RESET_PHONE_MODE;
    [self.navigationController pushViewController:conVC animated:YES];
    
}

-(void)doneAction:(id)sender
{

    if (_usernameTextField.mainTextField.text.length == 0)
    {
        [self showAlertViewMessage:LOCALIZE(@"Please enter user name")];
        return;
    }
    if (_pwdTextField.mainTextField.text.length == 0)
    {
        [self showAlertViewMessage:LOCALIZE(@"Please enter password")];
        return;
    }
    if(_doneButton.isSelected)
    {
        return;
    }
    
    [_doneButton setSelected:YES];
    
    if(_loginMode == LOGIN_V4_MODE_LOGIN)
    {
        [self loginWithDict:@{@"openID": _usernameTextField.mainTextField.text, @"password": [_pwdTextField.mainTextField.text MD5]}];
    }
    else if (_loginMode == LOGIN_V4_MODE_FORGET_PASSWORD)
    {
        [self forgetPasswordAction];
    }
}

@end
