//
//  UserListViewController.m
//  story17
//
//  Created by POPO Chen on 5/20/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "ConnectionUserViewController.h"
#import "Constant.h"
#import <AddressBook/AddressBook.h>
#import "ContactsData.h"
#import "UIImage+Helper.h"

@implementation ConnectionUserViewController

#define Padding 2

- (id)init
{
    self = [super init];
    if (self != nil){
    }
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self configureViewForIOS7];
//    [self addCustomNavigationBackButton];
    
    _fbFriendIDArray = CREATE_MUTABLE_ARRAY;
    _userArray = CREATE_MUTABLE_ARRAY;
    _fbUploadData = CREATE_MUTABLE_ARRAY;

    _contactUsers = CREATE_MUTABLE_ARRAY;
    
    _followedUserIDs = CREATE_MUTABLE_ARRAY;
    _privacyfollowedUserIDs = CREATE_MUTABLE_ARRAY;   // Privacy mode friend
    _privacyUserIDs = CREATE_MUTABLE_ARRAY; // Privacy user
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:LOCALIZE(@"Next") style:UIBarButtonItemStyleDone target:self action:@selector(nextAction:)];
    [self.navigationController setNavigationBarHidden:YES];
    [self.view setBackgroundColor:WHITE_COLOR];
    
    int nextY = STATUS_BAR_HEIGHT;
    
    _navBar = [[CustomNavbarView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, NAVI_BAR_HEIGHT)];
    [_navBar.titleLabel setTextColor:BLACK_COLOR];
    [_navBar.backBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [_navBar.nextBtn addTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
    [_navBar.nextBtn setTitle:LOCALIZE(@"Next") forState:UIControlStateNormal];

    _navBar.backBtn.hidden = YES;
    [_navBar setBackgroundColor:WHITE_COLOR];
    nextY += _navBar.frame.size.height+10;
    
    if(_type==UserFromContactBook){
        _navBar.titleLabel = LOCALIZE(@"Connect_contact");
    }else if(_type==UserFromFacebook){
        _navBar.titleLabel = LOCALIZE(@"Connect_facebook");
    }
    
//    UIImage* screenImg = [UIImage imageByApplyingBlurToImage:[UIImage imageNamed:@"bg5"] withRadius:SCREEN_WIDTH/32 tintColor:[UIColor colorWithRed:255.f green:255.f blue:255.f alpha:0.2f] saturationDeltaFactor:1.f maskImage:nil];
//    _bgImageView = [[UIImageView alloc]initWithImage:screenImg];
    _bgImageView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, SCREEN_HEIGHT-nextY);
    _bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    [self.view addSubview:_bgImageView];
    [self.view addSubview:_navBar];

    float headerHeight = [ConnectionHeaderView getDefaultHeaderHeight];
    _headerView = [[ConnectionHeaderView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, headerHeight)];
    _headerView.hidden = YES;
    _headerView.delegate = self;
    [_headerView setBackgroundColor:[UIColor clearColor]];
    nextY += headerHeight;
    
    UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [layout setMinimumInteritemSpacing:Padding];
    [layout setMinimumLineSpacing:Padding];
    
    _mainCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, SCREEN_HEIGHT-nextY) collectionViewLayout:layout];
    [_mainCollectionView registerClass:[UserListCell class] forCellWithReuseIdentifier:@"UserListCell"];
    [_mainCollectionView setDataSource:self];
    [_mainCollectionView setDelegate:self];
    _mainCollectionView.scrollsToTop = YES;
    _mainCollectionView.alwaysBounceVertical = YES;
    [_mainCollectionView setBackgroundColor:[UIColor clearColor]];

    _noDataImageView = [ThemeManager getNoDataImageView];
    _noDataImageView.hidden = YES;
    
    [self.view addSubview:_headerView];
    [self.view addSubview:_mainCollectionView];
    [self.view addSubview:_noDataImageView];
}

-(void)viewWillAppear:(BOOL)animated
{
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];

    if(_type==UserFromFacebook){
    
        [self loadFBFriendWithOffset:@""];
    
    }else if(_type==UserFromContactBook){
        
        if([[CONNECTION_MANAGER getPhoneNumerForSearch] count]==0){
            [CONNECTION_MANAGER showPermissionAlert:^(BOOL success, NSString *message) {
                [self fetchContactDataWithJson:TO_JSON([CONNECTION_MANAGER getPhoneNumerForSearch]) method:@"contacts"];
            }];
            return;
        }
        [self fetchContactDataWithJson:TO_JSON([CONNECTION_MANAGER getPhoneNumerForSearch]) method:@"contacts"];

    }
}

-(void)fetchContactDataWithJson:(NSString*)jsonStr method:(NSString*)method
{
    [DIALOG_MANAGER showLoadingView];
    [API_MANAGER findFriends:jsonStr method:method completion:^(BOOL success, NSArray *userObjects) {
        [DIALOG_MANAGER hideLoadingView];
        if(success){

            _headerView.hidden = NO;
            
            int friendCount = 0;
            
            if(_type==UserFromContactBook){

                [_contactUsers removeAllObjects];
                
                for(UserObject* user in userObjects){
                    [_contactUsers addObject:user];
                    if(user.isFollowing==1){
                        [_followedUserIDs addObject:user.userID];
                    }
                    if([user isPrivacyMode]){
                        [_privacyUserIDs addObject:user.userID];
                    }
                }
                
                friendCount = (int)[_contactUsers count];

            }else if(_type==UserFromFacebook){
                
                [_userArray removeAllObjects];
            
                for(UserObject* user in userObjects){
                    [_userArray addObject:user];
                    if(user.isFollowing==1){
                        [_followedUserIDs addObject:user.userID];
                    }
                    if([user isPrivacyMode]){
                        [_privacyUserIDs addObject:user.userID];
                    }
                }
                
                friendCount = (int)[_userArray count];
            }
            
            if(([method isEqualToString:@"contacts"] && [_contactUsers count]==0) || ([method isEqualToString:@"facebook"] && [_userArray count]==0)){
                _noDataImageView.hidden = NO;
            }else{
                _noDataImageView.hidden = YES;
            }

            
            [_headerView.titleLabel setText:[NSString stringWithFormat:LOCALIZE(@"connection_header_title"),friendCount]];
            [_mainCollectionView reloadData];
        }
    }];
}

#pragma mark - UICollectionView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(_type==UserFromContactBook){
        return [_contactUsers count];
    }else if(_type==UserFromFacebook){
        return [_userArray count];
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UserListCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UserListCell" forIndexPath:indexPath];

    cell.delegate = self;
    
    if (cell == nil){
        cell = [[UserListCell alloc] initWithFrame:CGRectMake(0, 0, collectionView.frame.size.width, [UserListCell getDefaultHeight])];
    }
    
    if(_type==UserFromContactBook){
        
        UserObject* user;
        user = [_contactUsers objectAtIndex:indexPath.row];

        if([_followedUserIDs containsObject:user.userID]){
            user.isFollowing=1;
        }else{
            user.isFollowing=0;
            
            if([_privacyfollowedUserIDs containsObject:user.userID]){
                user.followRequestTime = CURRENT_TIMESTAMP;
            }else{
                user.followRequestTime = 0;
            }
        }
        
        NSString* phone = user.phoneNumber;
        NSString* realName = [CONNECTION_MANAGER getRealNameByPhone:phone];

        user.name = realName;
        cell.user=user;

    }else if(_type==UserFromFacebook){
        
        UserObject* user = [_userArray objectAtIndex:indexPath.row];
        
        if([_followedUserIDs containsObject:user.userID]){
            user.isFollowing=1;
        }else{
            user.isFollowing=0;
            
            if([_privacyfollowedUserIDs containsObject:user.userID]){
                user.followRequestTime = CURRENT_TIMESTAMP;
            }else{
                user.followRequestTime = 0;
            }
            
        }
        cell.user = user;

    }
    return cell;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeZero;
    size.width = collectionView.frame.size.width;
    size.height = [UserListCell getDefaultHeight];
    return size;
}

#pragma mark - Facebook
-(void)loadFBFriendWithOffset:(NSString*)urlString
{
    
    NSString* url;
    
    if([urlString isEqualToString:@""]){
        url = @"me/friends";
    }else{
        url = urlString;
        url = [url substringFromIndex:32]; //remove the 'https://graph.facebook.com/v2.3/' for using FBSDKGraphRequest
    }
    
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:url parameters:@{@"fields": @"id"} HTTPMethod:@"GET"]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (error) {
             [DIALOG_MANAGER showNetworkFailToast];
             return ;
         }
         
         if([urlString isEqualToString:@""]){
             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
              startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                  NSString* facebookID = result[@"id"];
                  NSString* fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
                  [API_MANAGER updateUserInfo:@{@"facebookID":facebookID,@"facebookAccessToken":fbAccessToken} fetchSelfInfo:NO completion:^(BOOL success) {
                  }];
              }];
         }
         
         NSArray* fbFriends = result[@"data"];
         NSString* nextPage = result[@"paging"][@"next"];
         
         for(NSDictionary* dict in fbFriends){
             [_fbUploadData addObject:dict];
             [_fbFriendIDArray addObject:dict[@"id"]];
         }
         
         if(nextPage !=nil){
             
             [self loadFBFriendWithOffset:nextPage];
             
         }else{
             
             NSString* facebookName =[NSString stringWithFormat:@"%@ %@",result[@"first_name"],result[@"last_name"]];
     
             [self fetchContactDataWithJson:TO_JSON(_fbFriendIDArray) method:@"facebook"];
             
             if([GET_DEFAULT(FACEBOOK_LAST_UPDATE) intValue]>CURRENT_TIMESTAMP-86400){
                 return;
             }
             
             [DIALOG_MANAGER showLoadingView];
             [API_MANAGER uploadFacebookFriends:facebookName facebookIDs:TO_JSON(_fbUploadData) withCompletion:^(BOOL success, NSString *message) {
                 [DIALOG_MANAGER hideLoadingView];
                 if(success){
                     [DEFAULTS setObject:INT_TO_NUMBER(CURRENT_TIMESTAMP) forKey:FACEBOOK_LAST_UPDATE];
                     [DEFAULTS synchronize];
                 }
             }];
         }
         
     }]; 
    
}

#pragma mark - ConnectionHeaderDelegate
-(void)didClickFollowAll
{
    NSArray* userArray;
    
    if(_type==UserFromContactBook){
        userArray = _contactUsers;
    }else if(_type==UserFromFacebook){
        userArray = _userArray;
    }
    
    for(UserObject* user in userArray){
        if(![_followedUserIDs containsObject:user.userID]){
            if([user isPrivacyMode]){
                [_privacyfollowedUserIDs addObject:user.userID];
            }else{
                [_followedUserIDs addObject:user.userID];
            }
        }
    }
    [_mainCollectionView reloadData];
}

#pragma mark - UserListCellDelegate
-(void)didClickFollow:(NSString*)userID
{
    if(![_followedUserIDs containsObject:userID]){
    
        if([_privacyUserIDs containsObject:userID]){
            [_privacyfollowedUserIDs addObject:userID];
        }else{
            [_followedUserIDs addObject:userID];
        }
    }else{
        [_followedUserIDs removeObject:userID];
    }
}

-(void)nextAction:(id)sender
{
    if([_followedUserIDs count]==0){
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    
    [DIALOG_MANAGER showLoadingView];
    [API_MANAGER followAllAction:TO_JSON(_followedUserIDs) withCompletion:^(BOOL success) {
        [DIALOG_MANAGER hideLoadingView];
        if(success){
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    
    for(NSString* userID in _privacyfollowedUserIDs){
        [API_MANAGER sendFollowRequest:userID withCompletion:^(BOOL success) {}];
    }
}

@end
