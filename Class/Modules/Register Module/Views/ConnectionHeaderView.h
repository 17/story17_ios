//
//  ConnectionHeaderView.h
//  story17
//
//  Created by POPO Chen on 5/20/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ConnectionHeaderDelegate <NSObject>
-(void)didClickFollowAll;
@end

@interface ConnectionHeaderView : UIView

@property (nonatomic,strong) UILabel* titleLabel;
@property (nonatomic,strong) UIButton* followAllBtn;

@property (weak) id<ConnectionHeaderDelegate> delegate;

+ (float)getDefaultHeaderHeight;


@end
