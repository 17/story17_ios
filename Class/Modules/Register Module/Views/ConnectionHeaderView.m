//
//  ConnectionHeaderView.m
//  story17
//
//  Created by POPO Chen on 5/20/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "ConnectionHeaderView.h"

#import "Constant.h"

@implementation ConnectionHeaderView

#define headerHeight 45
#define topMargin (headerHeight-FOLLOW_BTN_HEIGHT)/2
#define leftMargin 10
#define btnWidth 30

+ (float)getDefaultHeaderHeight
{
    return headerHeight;
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self setBackgroundColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:0.4]];
        
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(leftMargin, topMargin, SCREEN_WIDTH/2, btnWidth)];
        _titleLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_titleLabel setTextColor:DARK_GRAY_COLOR];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        
        _followAllBtn = [ThemeManager getGreenLineBtn];
        _followAllBtn.frame = CGRectMake(SCREEN_WIDTH-leftMargin-FOLLOW_BTN_WIDTH, topMargin, FOLLOW_BTN_WIDTH, FOLLOW_BTN_HEIGHT);
        [_followAllBtn addTarget:self action:@selector(didClickFollowAll:) forControlEvents:UIControlEventTouchUpInside];
        [_followAllBtn setTitle:LOCALIZE(@"+FollowAll") forState:UIControlStateNormal];
        
        [self addSubview:_titleLabel];
        [self addSubview:_followAllBtn];
    }
    return self;
}

-(void)didClickFollowAll:(id)sender
{
    [_followAllBtn playBounceAnimation];
    [_delegate didClickFollowAll];
}


@end
