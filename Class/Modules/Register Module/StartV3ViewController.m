//
//  StartV3ViewController.m
//  Story17
//
//  Created by POPO on 8/5/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "StartV3ViewController.h"
#import "Constant.h"
#import "FacebookManager.h"
#import <TwitterKit/TwitterKit.h>
#import "WeiboSDK.h"
#import "WXApiManager.h"
#import "MultiFileUploader.h"

#define BUTTON_MARGIN_X 10
#define IMAGE_PADDING_X 40

@interface StartV3ViewController()<WXApiManagerDelegate>

// background login animation
@property (nonatomic, strong) NSMutableArray* loginAnimationImages;
@property (nonatomic, strong) UIScrollView* loginAnimationScrollView;
@property NSInteger animationImageCount;
@property (nonatomic, strong) UIImageView* upperImageView;
@property (nonatomic, strong) UIImageView* lowerImageView;

@property (nonatomic, strong) UIImageView* logoImageView;
@property (nonatomic, strong) UIButton* registerButton;

@property (nonatomic, strong) UIButton* qqLoginButton;
@property (nonatomic, strong) UIButton* weiboLoginButton;
@property (nonatomic, strong) UIButton* wechatLoginButton;


@property (nonatomic, strong) UILabel* loginRemindLabel;
@property (nonatomic, strong) UILabel* loginLabel;
@property (nonatomic, strong) UILabel* agreementLabel;
@property (nonatomic, strong) TencentOAuth* tencentOAuth;

@property (nonatomic, strong) NSString* qqID;
@property (nonatomic, strong) NSString* myProfilePicture;
@property (nonatomic, strong) MultiFileUploader* multiFileUploader;

// alert
@property (nonatomic, strong) UIImageView* alertView;
@property (nonatomic, strong) UILabel* alertLabel;

@end

@implementation StartV3ViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    
    [SINGLETON clearUserDefaults];
    [SINGLETON initializeDefaults];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    NSArray *viewControllers = self.navigationController.viewControllers;
    
    if (viewControllers.count > 1 && [viewControllers objectAtIndex:viewControllers.count-2] == self) {
        [NOTIFICATION_CENTER removeObserver:self];
    }
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
}

- (void)loginBackgrouindAnimationRound:(int)round
{
    int nextRound = (round%_animationImageCount)? round+1: 1;

    [_upperImageView setImage:[UIImage imageNamed:[_loginAnimationImages objectAtIndex:round-1]]];
    _upperImageView.alpha = 1;
    [_lowerImageView setImage:[UIImage imageNamed:[_loginAnimationImages objectAtIndex:nextRound-1]]];

    // move image
    [UIView animateWithDuration:3.0 animations:^{
        _loginAnimationScrollView.contentOffset = CGPointMake(IMAGE_PADDING_X, 0);
    } completion:^(BOOL finished) {
        _loginAnimationScrollView.contentOffset = CGPointMake(0, 0);
        [self loginBackgrouindAnimationRound:nextRound];
    }];
    
    // fade out upper image
    [UIView animateWithDuration:0.5 delay:2.5 options:UIViewAnimationOptionCurveLinear animations:^{
        _upperImageView.alpha = 0;
    } completion:^(BOOL finished) {
    }];
}

- (void) viewDidLoad
{
    [super viewDidLoad];

    // init
    [self configureViewForIOS7];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    _tencentOAuth =  [[TencentOAuth alloc] initWithAppId:QQ_APP_ID andDelegate:self];
    [WXApiManager sharedManager].delegate = self;

//    [NOTIFICATION_CENTER addObserver:self selector:@selector(loginSuccessHandler) name:FACEBOOK_LOGIN_SUCCESS_NOTIFICATION object:nil];
//    [NOTIFICATION_CENTER addObserver:self selector:@selector(loginFailHandler) name:FACEBOOK_LOGIN_FAIL_NOTIFICATION object:nil];
    
    [NOTIFICATION_CENTER addObserver:self selector:@selector(loginWeiboSuccessHandler) name:WEIBO_LOGIN_SUCCESS_NOTIFICATION object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(loginWeiboFailHandler) name:WEIBO_LOGIN_FAIL_NOTIFICATION object:nil];
    
//    _igWebViewVC = [IGWebViewController new];
    
    // background login animation
    _loginAnimationImages = [NSMutableArray arrayWithObjects:@"bg5", @"bg6", @"bg7", @"bg8", nil];
    _animationImageCount = [_loginAnimationImages count];
    _upperImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH+IMAGE_PADDING_X, SCREEN_HEIGHT)];
    _lowerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH+IMAGE_PADDING_X, SCREEN_HEIGHT)];
    _loginAnimationScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _loginAnimationScrollView.showsHorizontalScrollIndicator = NO;
    _loginAnimationScrollView.showsVerticalScrollIndicator = NO;
    _loginAnimationScrollView.scrollEnabled = NO;
    _loginAnimationScrollView.contentSize = CGSizeMake(SCREEN_WIDTH+IMAGE_PADDING_X, SCREEN_HEIGHT);
    _loginAnimationScrollView.contentOffset = CGPointMake(0, 0);
    [self.view addSubview:_lowerImageView];
    [self.view addSubview:_loginAnimationScrollView];
    [_loginAnimationScrollView addSubview:_upperImageView];
    
    // logo
    _logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lodin_icon"]];
    _logoImageView.frame = CGRectMake(0, 0, _logoImageView.bounds.size.width*1.5, _logoImageView.bounds.size.height*1.5);
    _logoImageView.center = CGPointMake(SCREEN_WIDTH/2, STATUS_BAR_HEIGHT+_logoImageView.bounds.size.height);
    [self.view addSubview:_logoImageView];

    
    // button
    _registerButton = [ThemeManager getRegisterControlBtn];
    _registerButton.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/24*7);
    [_registerButton setTitle:LOCALIZE(@"REGISTER_ACCOUNT") forState:UIControlStateNormal];
    [_registerButton.titleLabel setFont:BOLD_FONT_WITH_SIZE(18)];
    [_registerButton bk_addEventHandler:^(id sender) {

//        RegisterV3ViewController* regVC = [RegisterV3ViewController new];
//        [self.navigationController pushViewController:regVC animated:YES];
        
    } forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:_registerButton];

    
    _qqLoginButton = [ThemeManager getRegisterControlBtn];
    _qqLoginButton.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/24*8);
    [_qqLoginButton setTitle:LOCALIZE(@"REGISTER_QQ_LOGIN") forState:UIControlStateNormal];
    [_qqLoginButton.titleLabel setFont:BOLD_FONT_WITH_SIZE(18)];
    [_qqLoginButton setImage:[UIImage imageNamed:@"login_qq"] forState:UIControlStateNormal];
    
    [_qqLoginButton bk_addEventHandler:^(id sender) {
        
        [_tencentOAuth authorize:[NSArray arrayWithObjects:@"get_user_info", @"get_simple_userinfo", @"add_t", nil] inSafari:NO];
        
    } forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_qqLoginButton];

    _wechatLoginButton = [ThemeManager getRegisterControlBtn];
    _wechatLoginButton.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/24*10);
    [_wechatLoginButton setTitle:LOCALIZE(@"REGISTER_WECHAT_LOGIN") forState:UIControlStateNormal];
    [_wechatLoginButton.titleLabel setFont:BOLD_FONT_WITH_SIZE(18)];
    [_wechatLoginButton setImage:[UIImage imageNamed:@"login_wechat"] forState:UIControlStateNormal];
    [_wechatLoginButton bk_addEventHandler:^(id sender) {
        
        NSString *scope = @"snsapi_userinfo";
        NSString *state = @"xxx";
        
        SendAuthReq* req = [[SendAuthReq alloc] init];
        req.scope = scope;
        req.state = state;
        
//        [WXApi sendAuthReq:req
//                   viewController:self
//                         delegate:[WXApiManager sharedManager]];
        
    } forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_wechatLoginButton];

    
    _weiboLoginButton = [ThemeManager getRegisterControlBtn];
    _weiboLoginButton.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/24*12);
    [_weiboLoginButton setTitle:LOCALIZE(@"REGISTER_WEIBO_LOGIN") forState:UIControlStateNormal];
    [_weiboLoginButton.titleLabel setFont:BOLD_FONT_WITH_SIZE(18)];
    [_weiboLoginButton setImage:[UIImage imageNamed:@"register_weibo"] forState:UIControlStateNormal];
    
    [_weiboLoginButton bk_addEventHandler:^(id sender) {
        
        WBAuthorizeRequest *request = [WBAuthorizeRequest request];
        request.redirectURI = WEIBO_REDIRECT_URL;
        request.scope = @"all";
        request.userInfo = @{@"SSO_From": @"SendMessageToWeiboViewController",
                             @"Other_Info_1": [NSNumber numberWithInt:123],
                             @"Other_Info_2": @[@"obj1", @"obj2"],
                             @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
        [WeiboSDK sendRequest:request];

    } forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_weiboLoginButton];

    
    _loginRemindLabel = [UILabel new];
    _loginRemindLabel.numberOfLines = 1;
    _loginRemindLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    _loginRemindLabel.textAlignment = NSTextAlignmentCenter;
    _loginRemindLabel.text = LOCALIZE(@"REGISTER_HAVE_ACCOUNT");
    [_loginRemindLabel setTextColor:BLACK_COLOR];
    [_loginRemindLabel sizeToFit];
    
    _loginLabel = [UILabel new];
    _loginLabel.numberOfLines = 1;
    _loginLabel.font = BOLD_FONT_WITH_SIZE(16);
    _loginLabel.textAlignment = NSTextAlignmentCenter;
    _loginLabel.text = LOCALIZE(@"REGISTER_LOGIN");
    [_loginLabel setTextColor:BLACK_COLOR];
    [_loginLabel sizeToFit];
    _loginLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *loginTapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchLoginLabel)];
    [_loginLabel addGestureRecognizer:loginTapGesture];
    
    float screenWhiteSpace = (SCREEN_WIDTH-_loginRemindLabel.bounds.size.width-_loginLabel.bounds.size.width-10)/2;
    _loginRemindLabel.center = CGPointMake(screenWhiteSpace+_loginRemindLabel.bounds.size.width/2, SCREEN_HEIGHT/24*14);
    [self.view addSubview:_loginRemindLabel];
    [self.view addSubview:_loginLabel];
    
    // for easy to click this label
    _loginLabel.frame = CGRectMake(0, 0, _loginLabel.bounds.size.width+10, _loginLabel.bounds.size.height+10);
    _loginLabel.center = CGPointMake(screenWhiteSpace+_loginRemindLabel.bounds.size.width+10+_loginLabel.bounds.size.width/2, SCREEN_HEIGHT/24*14);
    
    _agreementLabel = [UILabel new];
    _agreementLabel.numberOfLines = 0;
    _agreementLabel.font = SYSTEM_FONT_WITH_SIZE(10);
    _agreementLabel.textAlignment = NSTextAlignmentCenter;
    _agreementLabel.text = LOCALIZE(@"REGISTER_AGREEMENT_WARNING");
    [_agreementLabel setTextColor:[UIColor colorWithRed:66.0f/255.0f green:66.0f/255.0f blue:66.0f/255.0f alpha:1.0f]];
    _agreementLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH-30, 40);
    _agreementLabel.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/24*15+5);
    
    [self.view addSubview:_agreementLabel];
    
    [self loginBackgrouindAnimationRound:1];
}

#pragma mark - TencentSessionDelegate
- (void)getUserInfoResponse:(APIResponse*) response
{
    // register process
    DLog(@"nickname:%@",response.jsonResponse[@"nickname"]);
    DLog(@"figureurl_qq_2:%@",response.jsonResponse[@"figureurl_qq_2"]);
    DLog(@"response:%@",response.jsonResponse);

    NSString* name = response.jsonResponse[@"nickname"];
    NSString* pictureUrl = response.jsonResponse[@"figureurl_qq_2"];
    NSString* gender = response.jsonResponse[@"gender"];
    
//    DLog(@"Gender:%@",gender);
    
    // get profile photo
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"myQQPic.png"];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    AFHTTPRequestOperation *op = [manager GET:pictureUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {

        NSString* profileImageFileName = [SINGLETON generateRandomFileNameWithExtension:@"png"];
        [SINGLETON saveImageWithThumbnailImage:[UIImage imageWithContentsOfFile:path] andFileName:profileImageFileName];
        self.multiFileUploader = [[MultiFileUploader alloc] initWithFileNameArray:@[profileImageFileName,[NSString stringWithFormat:@"THUMBNAIL_%@",profileImageFileName]]];
        
        [self.multiFileUploader startUploadWithCallback:^(NSArray *fileNameArray, int code, float progress) {
            if(code==MULTI_UPLOAD_SUCCESS) {
                DLog(@"%@",profileImageFileName);
                [DIALOG_MANAGER hideLoadingView];
            } else if(code==MULTI_UPLOAD_FAIL) {
//                [DIALOG_MANAGER showNetworkFailToast];
//                return ;
            }
        }];
        
        if([gender isEqualToString:@"男"]){
            [DEFAULTS setObject:@"male" forKey:GENDER];
        }else if([gender isEqualToString:@"女"]){
            [DEFAULTS setObject:@"female" forKey:GENDER];
        }
        
        [DEFAULTS setObject:name forKey:USER_OPEN_ID];
        [DEFAULTS setObject:name forKey:USER_NAME];
        [DEFAULTS setObject:profileImageFileName forKey:CACHE_PICTURE];
        [DEFAULTS setObject:_qqID forKey:QQ_ID];
        [DEFAULTS synchronize];
        
        /* RegisterInfo */
//        RegisterV3ViewController* regVC = [RegisterV3ViewController new];
//        regVC.registerMode = QQ_MODE;
//        regVC.backButton.hidden = YES;
//        regVC.imageTextField.mainTextField.text = name;
//        [self.navigationController pushViewController:regVC animated:YES];

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        DLog(@"error:%@",error);
        [DIALOG_MANAGER showNetworkFailToast];
        DELETE_FILE(GET_LOCAL_FILE_PATH(path));
    }];

    op.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    [op start];
}

-(void)tencentDidLogin
{
    
    if (_tencentOAuth.accessToken && 0 != [_tencentOAuth.accessToken length])
    {
        
        DLog(@"登录完成:%@ %@",_tencentOAuth.accessToken,[_tencentOAuth openId]);
        _qqID = [_tencentOAuth openId];
//        NSString* accessToken = _tencentOAuth.accessToken;
        
        [API_MANAGER checkQQIDAvailable:_qqID completion:^(BOOL success, NSString *message) {
            
            if (success) {
                [_tencentOAuth getUserInfo];
            }else{
                [self loginWithDict:@{@"qqID": _qqID,}];
            }
        }];
    }
    else
    {
        DLog(@"登录不成功 没有获取accesstoken");
    }
}

-(void)tencentDidNotLogin:(BOOL)cancelled
{
    if (cancelled)
    {
        DLog(@"用户取消登录");
    }
    else
    {
        DLog(@"登录失败");
    }
} 

-(void)tencentDidNotNetWork
{
    DLog(@"无网络连接，请设置网络");
    [DIALOG_MANAGER showNetworkFailToast];
}

- (void)touchLoginLabel
{
//    LoginV3ViewController* loginVC = [LoginV3ViewController new];
//    [self.navigationController pushViewController:loginVC animated:YES];
}

#pragma mark - WXApiManagerDelegate

- (void)managerDidRecvAuthResponse:(SendAuthResp *)response {
    
    if(response.errCode==0){
        // Auth Success
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code",WECHAT_APP_ID,WECHAT_APP_KEY,response.code]]];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setHTTPMethod:@"GET"];
                
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
            if(error){
            
            }else{
            
                NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                NSDictionary *deserializedData = [requestReply mj_JSONObject];

                NSString* accessToken = deserializedData[@"access_token"];
                NSString* wechatID = deserializedData[@"openid"];
                
                NSMutableURLRequest *userInfoRequest = [[NSMutableURLRequest alloc] init];
                [userInfoRequest setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",accessToken,wechatID]]];
                [userInfoRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                [userInfoRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
                [userInfoRequest setHTTPMethod:@"GET"];
                
                DLog(@"request:%@",[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",accessToken,wechatID]);
                
                NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
                [[session dataTaskWithRequest:userInfoRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    if(error){
                        
                    }else{
                        NSString *requestReply2 = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                        NSString* trimString = [requestReply2 stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
                        NSDictionary *deserializedData2 = [trimString mj_JSONObject];
                        NSString* picture = deserializedData2[@"headimgurl"];
                        NSString* profileImageFileName = [SINGLETON generateRandomFileNameWithExtension:@"png"];
                        NSString* name = deserializedData2[@"nickname"] ;
                        name = [NSString stringWithCString:[name cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
                        
                        NSString* gender = @"female";
                        if([deserializedData2[@"sex"] intValue]==1){
                            gender=@"male";
                        }
                        
                        [DIALOG_MANAGER showLoadingView];
                        
                        [API_MANAGER checkWechatIDAvailable:wechatID completion:^(BOOL success, NSString *message) {

                            [DIALOG_MANAGER hideLoadingView];
                            if(success){
                                
                                // wechat Register
                                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"myFacebookPic.png"];
                                
                                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                                manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                                
                                AFHTTPRequestOperation *op = [manager GET:picture parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                    
                                    //                        NSString* profileImageFileName = [SINGLETON generateRandomFileNameWithExtension:@"png"];
                                    [SINGLETON saveImageWithThumbnailImage:[UIImage imageWithContentsOfFile:path] andFileName:profileImageFileName];
                                    self.multiFileUploader = [[MultiFileUploader alloc] initWithFileNameArray:@[profileImageFileName, [NSString stringWithFormat:@"THUMBNAIL_%@", profileImageFileName]]];
                                    
                                    [self.multiFileUploader startUploadWithCallback:^(NSArray *fileNameArray, int code, float progress) {
                                        if(code==MULTI_UPLOAD_SUCCESS) {
                                            [DIALOG_MANAGER hideLoadingView];
                                        } else if(code==MULTI_UPLOAD_FAIL) {
                                            [DIALOG_MANAGER showNetworkFailToast];
                                            return ;
                                        }
                                    }];
                                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                    DELETE_FILE(GET_LOCAL_FILE_PATH(path));
                                }];
                                
                                op.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
                                [op start];
                                
                                [DEFAULTS setObject:@"" forKey:USER_OPEN_ID];
                                [DEFAULTS setObject:name forKey:USER_NAME];
                                [DEFAULTS setObject:gender forKey:GENDER];
                                [DEFAULTS setObject:profileImageFileName forKey:CACHE_PICTURE];
                                [DEFAULTS setObject:wechatID forKey:WECHAT_USER_ID];
                                [DEFAULTS synchronize];
                                
                                /* RegisterInfo */
//                                RegisterV3ViewController* regVC = [RegisterV3ViewController new];
//                                regVC.registerMode = WECHAT_MODE;
//                                regVC.backButton.hidden = YES;
//                                regVC.imageTextField.mainTextField.text = name;
//                                [self.navigationController pushViewController:regVC animated:YES];
                                
                            }else{
                                // wechat login
                                [self loginWithDict:@{@"wechatID": wechatID}];
                            }
                        }];
                    }
                    
                }] resume];
            }
        }] resume];

    }
    
}

#pragma mark - notification
-(void) loginWeiboSuccessHandler
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://api.weibo.com/2/users/show.json?access_token=%@&uid=%@",GET_DEFAULT(WEIBO_ACCESSTOKEN),GET_DEFAULT(WEIBO_USER_ID)]]];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSDictionary *deserializedData = [requestReply mj_JSONObject];
        
        NSString* name = deserializedData[@"name"];
        NSString* picture = deserializedData[@"profile_image_url"];
        NSString* weiboID = deserializedData[@"id"];
        NSString* profileImageFileName = [SINGLETON generateRandomFileNameWithExtension:@"png"];
        NSString* gender = @"female";
        name = [NSString stringWithCString:[name cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];

        if([deserializedData[@"gender"] isEqualToString:@"m"]){
            gender = @"male";
        }
        
        
        [DIALOG_MANAGER showLoadingView];
        [API_MANAGER checkWeiboIDAvailable:weiboID completion:^(BOOL success, NSString *message) {
           
            [DIALOG_MANAGER hideLoadingView];
            
            if (success) {
                
                // get profile photo
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"myFacebookPic.png"];
                
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                
                AFHTTPRequestOperation *op = [manager GET:picture parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    //                        NSString* profileImageFileName = [SINGLETON generateRandomFileNameWithExtension:@"png"];
                    [SINGLETON saveImageWithThumbnailImage:[UIImage imageWithContentsOfFile:path] andFileName:profileImageFileName];
                    self.multiFileUploader = [[MultiFileUploader alloc] initWithFileNameArray:@[profileImageFileName, [NSString stringWithFormat:@"THUMBNAIL_%@", profileImageFileName]]];
                    
                    [self.multiFileUploader startUploadWithCallback:^(NSArray *fileNameArray, int code, float progress) {
                        if(code==MULTI_UPLOAD_SUCCESS) {
                            [DIALOG_MANAGER hideLoadingView];
                        } else if(code==MULTI_UPLOAD_FAIL) {
                            [DIALOG_MANAGER showNetworkFailToast];
                            return ;
                        }
                    }];
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    DELETE_FILE(GET_LOCAL_FILE_PATH(path));
                }];
                
                op.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
                [op start];
                
                [DEFAULTS setObject:name forKey:USER_OPEN_ID];
                [DEFAULTS setObject:name forKey:USER_NAME];
                [DEFAULTS setObject:gender forKey:GENDER];
                [DEFAULTS setObject:profileImageFileName forKey:CACHE_PICTURE];
                [DEFAULTS setObject:weiboID forKey:WEIBO_USER_ID];
                [DEFAULTS synchronize];
                
                /* RegisterInfo */
//                RegisterV3ViewController* regVC = [RegisterV3ViewController new];
//                regVC.registerMode = WEIBO_MODE;
//                regVC.backButton.hidden = YES;
//                regVC.imageTextField.mainTextField.text = name;
//                [self.navigationController pushViewController:regVC animated:YES];
                
            } else {
                DLog(@"facebook login");
                [self loginWithDict:@{@"weiboID": weiboID}];
            }
        }];
        
    }] resume];
}

- (void)loginWeiboFailHandler
{
    DLog(@"loginWeiboFailHandler");
}

#pragma mark - login
- (void)loginWithDict:(NSDictionary*)dict
{
    [DIALOG_MANAGER showLoadingView];
    [API_MANAGER loginAction2:dict completion:^(BOOL success, NSString *message) {
        [DIALOG_MANAGER hideLoadingView];
        if(success) {
            if([message isEqualToString:@"password_wrong"]) {
                
                [self showAlertViewMessage:LOCALIZE(@"wrong_password")];
                
            }else{
                [SINGLETON setupMainTabBarViewController];
            }
            
        } else {
            DLog(@"message=%@", message);
            if([message isEqualToString:@"password_wrong"]) {
                
                [self showAlertViewMessage:LOCALIZE(@"wrong_password")];
                
            } else if([message isEqualToString:@"freeze"]) {
                
                [self showAlertViewMessage:LOCALIZE(@"is_freezed")];
                
            } else {
                [DIALOG_MANAGER showNetworkFailToast];
            }
        }
    }];
}

#pragma mark - alert message
- (void)showAlertViewMessage:(NSString*)message
{
    [_alertView.layer removeAllAnimations];
    [_alertView addSubview:_alertLabel];
    [self.view addSubview:_alertView];
    
    _alertView.frame = CGRectMake(0, SCREEN_HEIGHT-KEYBOARD_HEIGHT, SCREEN_WIDTH, _alertView.frame.size.height);
    _alertLabel.frame = CGRectMake(0, 0, _alertView.frame.size.width,_alertView.frame.size.height);
    if(_alertView.frame.origin.y != SCREEN_HEIGHT-KEYBOARD_HEIGHT){
        _alertView.frame = CGRectMake(0, SCREEN_HEIGHT-KEYBOARD_HEIGHT, SCREEN_WIDTH, _alertView.frame.size.height);
    }
    
    [_alertLabel setText:message];
    _alertLabel.center = CGPointMake(SCREEN_WIDTH/2, _alertView.frame.size.height/2);
    
    [UIView animateWithDuration:0.4 animations:^{
        DLog(@"_alertView H=%f", _alertView.frame.size.height);
        DLog(@"message =%@", message);
        
        _alertView.frame = CGRectMake(0, SCREEN_HEIGHT-KEYBOARD_HEIGHT-_alertView.frame.size.height, SCREEN_WIDTH, _alertView.frame.size.height);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.4 delay:1.6 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            _alertView.frame = CGRectMake(0, SCREEN_HEIGHT-KEYBOARD_HEIGHT, SCREEN_WIDTH, _alertView.frame.size.height);
        } completion:^(BOOL finished) {
            [_alertLabel removeFromSuperview];
            [_alertView removeFromSuperview];
        }];
    }];
}

#pragma WBHttpRequestDelegate
- (void)request:(WBHttpRequest *)request didFinishLoadingWithResult:(NSString *)result
{
    NSString *title = nil;
    UIAlertView *alert = nil;
    
    title = NSLocalizedString(@"收到网络回调", nil);
    alert = [[UIAlertView alloc] initWithTitle:title
                                       message:[NSString stringWithFormat:@"%@",result]
                                      delegate:nil
                             cancelButtonTitle:NSLocalizedString(@"确定", nil)
                             otherButtonTitles:nil];
    [alert show];
}

- (void)request:(WBHttpRequest *)request didFailWithError:(NSError *)error;
{
    NSString *title = nil;
    UIAlertView *alert = nil;
    
    title = NSLocalizedString(@"请求异常", nil);
    alert = [[UIAlertView alloc] initWithTitle:title
                                       message:[NSString stringWithFormat:@"%@",error]
                                      delegate:nil
                             cancelButtonTitle:NSLocalizedString(@"确定", nil)
                             otherButtonTitles:nil];
    [alert show];
}

#pragma FacebookManagerDelegate
- (void)facebookManager:(FacebookManager *) facebookManager didLoginComplete:(BOOL)success
{
    
}

@end
