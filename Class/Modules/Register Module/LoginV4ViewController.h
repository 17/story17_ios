//
//  LoginV3ViewController.h
//  Story17
//
//  Created by POPO on 8/5/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "LoginHandler.h"


typedef NS_ENUM(NSInteger, LOGIN_V4_MODE){
    LOGIN_V4_MODE_LOGIN,
    LOGIN_V4_MODE_FORGET_PASSWORD,
};

@interface LoginV4ViewController : UIViewController <UITextFieldDelegate,LoginHandlerDelegate>
@property LOGIN_V4_MODE loginMode;
//@property (nonatomic, strong) TencentOAuth* tencentOAuth;
@property (nonatomic, strong) MultiFileUploader* multiFileUploader;

@end
