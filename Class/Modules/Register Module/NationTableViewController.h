//
//  NationTableViewController.h
//  Dare
//
//  Created by POPO on 11/20/14.
//  Copyright (c) 2014 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NationTableDelegate
-(void) didSelectNations:(NSString*)nation code:(NSString*)code;
@end

@interface NationTableViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchDisplayDelegate>


@property (weak) id<NationTableDelegate> delegate;
@property (nonatomic,strong) UISearchDisplayController* searchDisplay;

@property (nonatomic,strong) UITableView* tableView;
@property int mode;
@property (nonatomic,strong) NSMutableArray* searchArray;
@property (nonatomic,strong) NSMutableArray* countryArray;

@end
