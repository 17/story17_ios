//
//  RegisterV3ViewController.m
//  Story17
//
//  Created by POPO on 2015/8/6.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "RegisterV4ViewController.h"
#import "CustomNavbarView.h"
#import "Constant.h"
#import "ConnectionViewController.h"
#import "FacebookManager.h"
#import "UIImage+Helper.h"
#import "PhoneVerificationViewController.h"
#import "MultiFileUploader.h"

@interface RegisterV4ViewController() <UITextFieldDelegate, UIImagePickerControllerDelegate>

@property (nonatomic,strong) CustomNavbarView* navBar;
@property (nonatomic, strong) UIImageView* bgImageView;

/* register starting view */
@property (nonatomic, strong) UIButton* registerButton;
@property (nonatomic, strong) UIButton* fbRegisterButton;
@property (nonatomic, strong) UILabel* registerLabel;

@property (nonatomic, strong) MultiFileUploader* multiFileUploader;
@property (nonatomic, strong) NSString* myProfilePicture;
@property (nonatomic, strong) UILabel* emailRemindLabel;
@property (nonatomic, strong) UIButton* doneButton;

@property (nonatomic, strong) UIButton* profilePhotoButton;
@property (nonatomic, strong) UIImageView* alertView;
@property (nonatomic, strong) UILabel* alertLabel;

@property (assign, nonatomic) CGRect alertFrame;

@end

@implementation RegisterV4ViewController

#define BUTTON_MARGIN_X 10
#define IMAGE_WIDTH 100
#define USER_IMAGE_WIDTH 85

- (id)init
{
    self = [super init];
    if(self){
        _registerMode = USER_NAME_MODE_V4;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (![[EVENT_HANDLER readNowStatus]isEqualToString:@"phone"]&&![[EVENT_HANDLER readNowStatus] isEqualToString:@"pwd"]) {
        [EVENT_HANDLER setNowStatus:@"openID"];
    }
//    _backButton = [self addCustomNavigationBackButtonCallback];
//    [_backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [self configureViewForIOS7];

    [self.view setBackgroundColor:WHITE_COLOR];
    
    int nextY = STATUS_BAR_HEIGHT;

    _navBar = [[CustomNavbarView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, NAVI_BAR_HEIGHT)];
    [_navBar.titleLabel setTextColor:BLACK_COLOR];
    [_navBar.backBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [_navBar.nextBtn addTarget:self action:@selector(skipAction:) forControlEvents:UIControlEventTouchUpInside];
    _navBar.nextBtn.hidden = YES;
    [_navBar setBackgroundColor:WHITE_COLOR];
    nextY += _navBar.frame.size.height+10;

    UIImage* screenImg = [UIImage imageByApplyingBlurToImage:[UIImage imageNamed:@"bg5"] withRadius:SCREEN_WIDTH/32 tintColor:[UIColor colorWithRed:255.f green:255.f blue:255.f alpha:0.2f] saturationDeltaFactor:1.f maskImage:nil];
    _bgImageView = [[UIImageView alloc]initWithImage:screenImg];
    _bgImageView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, SCREEN_HEIGHT-nextY);
    _bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    [self.view addSubview:_bgImageView];
    [self.view addSubview:_navBar];
    

    // alert view
    _alertView = [ThemeManager getAlertImageView];
    _alertLabel = [UILabel new];
    _alertLabel.textAlignment = NSTextAlignmentCenter;
    _alertLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [_alertLabel setTextColor:DARK_GRAY_COLOR];
    [_alertView addSubview:_alertLabel];
    
    
    self.profilePhotoButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2-USER_IMAGE_WIDTH/2, nextY, USER_IMAGE_WIDTH, USER_IMAGE_WIDTH)];
    self.profilePhotoButton.imageView.contentMode  = UIViewContentModeScaleAspectFill;
    [self.profilePhotoButton addTarget:self action:@selector(pickProfilePictureAction) forControlEvents:UIControlEventTouchUpInside];
    [self.profilePhotoButton setImage:IMAGE_FROM_BUNDLE(@"profile_photo") forState:UIControlStateNormal];
    [self.view addSubview:self.profilePhotoButton];
    _profilePhotoButton.hidden = YES;
    
    _imageTextField = [[ImageTextField alloc] initWithFrame:CGRectMake(BUTTON_MARGIN_X, nextY, SCREEN_WIDTH, [ImageTextField getDefaultHeight])];
    _imageTextField.mainTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _imageTextField.mainTextField.tintColor = BLACK_COLOR;
    [_imageTextField.bgImage setImage:[IMAGE_FROM_BUNDLE(@"btn_grayline") resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]];
    [_imageTextField.mainTextField setTextColor:BLACK_COLOR];
    
    _imageTextField.mainTextField.delegate = self;
    [self.view addSubview:_imageTextField];
    
    nextY += BUTTON_MARGIN_X+_imageTextField.frame.size.height;
    
    _doneButton = [ThemeManager getRegisterControlBtn];
    [_doneButton setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
    [_doneButton setTitle:LOCALIZE(@"REGISTER_NEXT_STEP") forState:UIControlStateNormal];
    _doneButton.center = CGPointMake(SCREEN_WIDTH/2, nextY+_doneButton.frame.size.height/2);
    [_doneButton addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_doneButton];
    
    if (_registerMode==USER_NAME_MODE_V4) {
        
        self.title = LOCALIZE(@"REGISTER_TITLE_USERNAME");
        _navBar.titleLabel = LOCALIZE(@"REGISTER_TITLE_USERNAME");
        _imageTextField.mainTextField.keyboardType = UIKeyboardTypeASCIICapable;


        [_imageTextField.headImage setImage:[UIImage imageNamed:@"lod_username"]];
        _imageTextField.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"REGISTER_ENTER_USERNAME") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];
        _imageTextField.mainTextField.text = GET_DEFAULT(USER_OPEN_ID);
        _alertView.hidden = NO;
        
    }
    else if (_registerMode==FACEBOOK_MODE_V4) {
        
        _navBar.titleLabel = LOCALIZE(@"REGISTER_TITLE_USERNAME");

        self.title = LOCALIZE(@"REGISTER_TITLE_USERNAME");
        [_imageTextField.headImage setImage:[UIImage imageNamed:@"lod_username"]];
        _imageTextField.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"REGISTER_ENTER_USERNAME") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];
        _imageTextField.mainTextField.text = GET_DEFAULT(USER_OPEN_ID);
        _alertView.hidden = NO;
        
    }
    else if (_registerMode==PASSWORD_MODE_V4) {
        
        self.title = LOCALIZE(@"REGISTER_TITLE_PWD");
        _navBar.titleLabel = LOCALIZE(@"REGISTER_TITLE_PWD");
        _imageTextField.mainTextField.keyboardType = UIKeyboardTypeASCIICapable;

        [_imageTextField.headImage setImage:[UIImage imageNamed:@"lod_password"]];
        _imageTextField.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"REGISTER_ENTER_PWD") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];
        _alertView.hidden = NO;
        
    }
    else if (_registerMode==FULLNAME_MODE_V4) {
        
        _backButton.hidden = YES;
        self.title = LOCALIZE(@"REGISTER_TITLE_NICKNAME");
        _navBar.titleLabel = LOCALIZE(@"REGISTER_TITLE_NICKNAME");
        _navBar.backBtn.hidden = YES;
        _navBar.nextBtn.hidden = NO;
        
        [_imageTextField.headImage setImage:[UIImage imageNamed:@"lod_fullname"]];
        _imageTextField.mainTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZE(@"REGISTER_ENTER_NICKNAME") attributes:@{NSForegroundColorAttributeName: GRAY_COLOR}];
        _imageTextField.mainTextField.text = GET_DEFAULT(USER_NAME);
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:LOCALIZE(@"skip") style:UIBarButtonItemStyleDone target:self action:@selector(skipAction:)];
        

        
    }else if(_registerMode==USER_IMAGE_MODE_V4){
        
        _backButton.hidden = YES;
        self.title = LOCALIZE(@"REGISTER_TITLE_PICTURE");
        _navBar.titleLabel = LOCALIZE(@"REGISTER_TITLE_PICTURE");
        _navBar.backBtn.hidden = YES;
        _navBar.nextBtn.hidden = NO;
        
        _profilePhotoButton.hidden = NO;

        _imageTextField.hidden = YES;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:LOCALIZE(@"skip") style:UIBarButtonItemStyleDone target:self action:@selector(skipAction:)];
        [self setProfileImage];
        _doneButton.center = CGPointMake(SCREEN_WIDTH/2, _profilePhotoButton.frame.origin.y+_profilePhotoButton.frame.size.height+_doneButton.frame.size.height);
        
    }
    
    /* enter username mode decide action */
    if(_registerMode == USER_NAME_MODE_V4){
        if(_registerType==FB_MODE){
            [DEFAULTS setObject:FACEBOOK_LOGIN_MODE forKey:LOGIN_MODE];
        }else if(_registerType == WECHAT_MODE){
            [DEFAULTS setObject:WECHAT_LOGIN_MODE forKey:LOGIN_MODE];
        }else if (_registerType == WEIBO_MODE){
            [DEFAULTS setObject:WEIBO_LOGIN_MODE forKey:LOGIN_MODE];
        }else if(_registerType == QQ_MODE){
            [DEFAULTS setObject:QQ_LOGIN_MODE forKey:LOGIN_MODE];
        }else{
            [DEFAULTS setObject:MSG_LOGIN_MODE forKey:LOGIN_MODE];
        }
        
        [DEFAULTS synchronize];
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    [NOTIFICATION_CENTER addObserver:self
                            selector:@selector(keyboardDidShow:)
                                name:UIKeyboardDidShowNotification
                              object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(keyboardNotificationHandler:) name: UIKeyboardDidChangeFrameNotification object:nil];

    
    if (self.isMovingToParentViewController == NO&&_registerMode==PASSWORD_MODE_V4)
    {
        [EVENT_HANDLER setNowStatus:@"phone"];
    }
    
    if (_registerMode == USER_NAME_MODE_V4) {
        [EVENT_HANDLER addEventTracking:@"StartRegister" withDict:nil];
    }
    [_doneButton setSelected:NO];

    if(_registerMode == USER_NAME_MODE_V4 || _registerMode == FACEBOOK_MODE_V4){
//        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
    
    if(_registerMode == FACEBOOK_MODE_V4){
        _backButton.hidden = YES;
        [EVENT_HANDLER addEventTracking:@"ClickFbRegisterButton" withDict:@{}];
    }
    
    if (_registerMode != USER_IMAGE_MODE_V4) {
        [_imageTextField.mainTextField becomeFirstResponder];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    
    NSArray *viewControllers = self.navigationController.viewControllers;
    if (viewControllers.count > 1 && [viewControllers objectAtIndex:viewControllers.count-2] == self) {
        
        
    } else if ([viewControllers indexOfObject:self] == NSNotFound) {
        
        if(_registerMode == USER_NAME_MODE_V4){
            [DEFAULTS setObject:@"" forKey:USER_OPEN_ID];
            [DEFAULTS setObject:@"" forKey:COUNTRY_CODE];
            [DEFAULTS setObject:@"" forKey:PHONE_NUM];
            [DEFAULTS synchronize];

        }
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [NOTIFICATION_CENTER removeObserver:self
                                   name:UIKeyboardDidShowNotification
                                 object:nil];
    [NOTIFICATION_CENTER removeObserver:self name:UIKeyboardDidChangeFrameNotification object:nil];

}

- (void)resignKeyboard
{
    if (_imageTextField.mainTextField.isFirstResponder) {
        [_imageTextField.mainTextField resignFirstResponder];
    }
}

#pragma mark - Notifications

- (void)keyboardDidShow:(NSNotification*)notification
{
//    NSDictionary* keyboardInfo = [notification userInfo];
//    NSValue* keyboardFrameEnd = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
//    CGRect keyboardFrameEndRect = [keyboardFrameEnd CGRectValue];
//    _alertView.frame = CGRectMake(0,
//                                  keyboardFrameEndRect.origin.y + CGRectGetHeight(_alertView.frame),
//                                  SCREEN_WIDTH,
//                                  -CGRectGetHeight(_alertView.frame));
//    
//    self.alertFrame = _alertView.frame;
}

-(void)keyboardNotificationHandler:(NSNotification*)notification
{
    NSDictionary *keyboardInfo = [notification userInfo];
    NSValue *keyboardFrameEnd = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameEndRect = [keyboardFrameEnd CGRectValue];
    _alertView.frame = CGRectMake(0,
                                  keyboardFrameEndRect.origin.y ,
                                  SCREEN_WIDTH,
                                  CGRectGetHeight(_alertView.frame));
    self.alertFrame = _alertView.frame;
    
}

#pragma mark - profile photo related and UIImagePickerControllerDelegate

-(void)pickProfilePictureAction
{
    [DIALOG_MANAGER showActionSheetDialogTitle:@"" options:@[LOCALIZE(@"take_picture"),LOCALIZE(@"albom")] destructiveIndexes:@[] cancelable:YES withCompletion:^(int selectedOption) {
        if(selectedOption == 1){
            // 從相機膠卷選擇
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            [imagePicker.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
            [[UIBarButtonItem appearance] setBackgroundImage:nil  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
            
            [[UIBarButtonItem appearance] setBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
            
            imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
            imagePicker.delegate = (id) self;
            imagePicker.allowsEditing = YES;
            
            [self presentViewController:imagePicker animated:YES completion:^{
                
            }];
        }else if (selectedOption==0){
            // 照相
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
            imagePicker.cameraDevice=UIImagePickerControllerCameraDeviceFront;
            imagePicker.delegate = (id) self;
            imagePicker.allowsEditing = YES;
            [self presentViewController:imagePicker animated:YES completion:^{
                
            }];
            
        }else{
            
        }
        
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [SINGLETON customizeInterface];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        UIImage* imageTaken = [info valueForKey: UIImagePickerControllerEditedImage];
        
        if(picker.sourceType!=UIImagePickerControllerSourceTypePhotoLibrary){
            [SINGLETON saveImageWithWhiteBackgroundWithoutCompleteMessage:imageTaken];
        }
        
        [DIALOG_MANAGER showLoadingView];
        
        NSString* profilePictureFileName = [SINGLETON generateRandomFileNameWithExtension:@"jpg"];
        [SINGLETON saveImageWithThumbnailImage:imageTaken andFileName:profilePictureFileName];
        
        // Handle the result image here
        
        
        _multiFileUploader = [[MultiFileUploader alloc] initWithFileNameArray:@[profilePictureFileName, [NSString stringWithFormat:@"THUMBNAIL_%@", profilePictureFileName]]];
        
        [_multiFileUploader startUploadWithCallback:^(NSArray *fileNameArray, int code , float progress) {
            
            if(code==MULTI_UPLOAD_SUCCESS) {
                
                [DEFAULTS setObject: fileNameArray[0] forKey:CACHE_PICTURE];
                [DEFAULTS synchronize];
                
                
                dispatch_async(MAIN_QUEUE, ^{
                    [DIALOG_MANAGER hideLoadingView];
                    [self setProfileImage];
                    
                });
                
                
            } else if(code==MULTI_UPLOAD_FAIL) {
                [DIALOG_MANAGER showNetworkFailToast];
            }
        }];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [SINGLETON customizeInterface];
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)setProfileImage
{
    _myProfilePicture = GET_DEFAULT(CACHE_PICTURE);
    
    if(_myProfilePicture != nil && _myProfilePicture.length >0 && FILE_EXIST(_myProfilePicture)) {
        UIImage *profileImage = [UIImage imageWithContentsOfFile:GET_LOCAL_FILE_PATH(_myProfilePicture)];
        if (profileImage == nil) {
        }
        
        [_profilePhotoButton setImage:[UIImage imageWithContentsOfFile:GET_LOCAL_FILE_PATH(self.myProfilePicture)] forState:UIControlStateNormal];
        _profilePhotoButton.imageView.layer.cornerRadius = USER_IMAGE_WIDTH/2;
        _profilePhotoButton.imageView.layer.masksToBounds = YES;
        _profilePhotoButton.imageView.layer.borderWidth = 2.0f;
        _profilePhotoButton.imageView.layer.borderColor = [WHITE_COLOR CGColor];
    } else {
        [_profilePhotoButton setImage:IMAGE_FROM_BUNDLE(@"profile_photo") forState:UIControlStateNormal];
        _profilePhotoButton.imageView.layer.cornerRadius = 0.0f;
        _profilePhotoButton.imageView.layer.masksToBounds = NO;
    }
}

- (void)moveToMode:(REGISTER_V4_MODE)mode
{
    RegisterV4ViewController* regVC = [RegisterV4ViewController new];
    [EVENT_HANDLER setNowStatus:@"pwd"];
    regVC.registerMode = mode;
    [self.navigationController pushViewController:regVC animated:YES];
}

# pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self doneAction:nil];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    switch (_registerMode)
    {
        case USER_NAME_MODE_V4:
            if(_imageTextField.mainTextField.text.length >= USERNAME_MAX && string.length!=0) {
                return NO;
            }
            break;
        case FACEBOOK_MODE_V4:
            if(_imageTextField.mainTextField.text.length >= USERNAME_MAX && string.length!=0) {
                return NO;
            }
            break;
        case PASSWORD_MODE_V4:
            if(_imageTextField.mainTextField.text.length >= PASSWORD_MAX && string.length!=0) {
                return NO;
            }
            break;
        case FULLNAME_MODE_V4:
            if(_imageTextField.mainTextField.text.length >= PASSWORD_MAX && string.length!=0) {
                return NO;
            }
            break;
        case USER_IMAGE_MODE_V4:
            if(_imageTextField.mainTextField.text.length >= PASSWORD_MAX && string.length!=0) {
                return NO;
            }
            break;
    }
    

    
    return YES;
}



#pragma mark - alert message
- (void)showAlertViewMessage:(NSString*)message
{
    [_alertView.layer removeAllAnimations];
    [self.view addSubview:_alertView];
    
    _alertLabel.frame = _alertView.bounds;
    [_alertLabel setText:message];
    _alertLabel.center = CGPointMake(SCREEN_WIDTH / 2, CGRectGetHeight(_alertView.frame) / 2);
    
    _alertView.alpha = 0;
    [UIView animateWithDuration:0.4 animations:^{
        _alertView.frame = CGRectOffset(_alertFrame, 0, -CGRectGetHeight(_alertFrame));
        _alertView.alpha = 1;
//        NSLog(@"對話的位置中間停留：%@",NSStringFromCGRect(_alertView.frame));

        
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView animateWithDuration:0.4 delay:1.6 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
                _alertView.frame = CGRectOffset(_alertFrame, 0, CGRectGetHeight(_alertFrame));
                _alertView.alpha = 0;
                
            } completion:^(BOOL finished) {
                if (finished) {
                    [_alertView removeFromSuperview];
                    
                }
            }];
        }

    }];
}

#pragma mark - Actions

- (void)backAction:(id)sender
{
    if (_registerMode==USER_NAME_MODE_V4) {
        [EVENT_HANDLER addEventTracking:@"QuitRegister" withDict:@{@"lastStep":[EVENT_HANDLER readNowStatus]}];
        [EVENT_HANDLER setNowStatus:@""];
    }

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)doneAction:(id)sender
{
    NSString *nameRegex = @"[A-Za-z0-9_.]+";
    NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    NSString *alertStr = @"";
    NSString *inputString;
    
    if(_doneButton.isSelected)
        return;
    
    [_doneButton setSelected:YES];
    
    if(_registerMode == USER_NAME_MODE_V4)
    {
        
        inputString = _imageTextField.mainTextField.text;
        
        if (inputString.length < USERNAME_MIN)
        {
            alertStr = [NSString stringWithFormat:LOCALIZE(@"REGISTER_ALERT_USERNAME_TOO_SHORT"),USERNAME_MIN];
        } else if(![nameTest evaluateWithObject:inputString])
        {
            alertStr = LOCALIZE(@"REGISTER_ALERT_USERNAME_ILLEGAL");
        }
        
        if(![alertStr isEqualToString:@""])
        {
            [_doneButton setSelected:NO];
            [self showAlertViewMessage:alertStr];
            return;
        }
        
        [DIALOG_MANAGER showLoadingView];
        [API_MANAGER checkOpenIDAvailable:[inputString lowercaseString] completion:^(BOOL success, NSString *message) {
            
            [DIALOG_MANAGER hideLoadingView];
            
            if (success)
            {
                
                [DEFAULTS setObject:[inputString lowercaseString] forKey:USER_OPEN_ID];
                [DEFAULTS synchronize];
                [self moveToMode:PASSWORD_MODE_V4];
                
            } else
            {
                [_doneButton setSelected:NO];
                if ([message isEqualToString:@"open_id_not_available"])
                {
                    [self showAlertViewMessage:LOCALIZE(@"REGISTER_ALERT_USERNAME_USED")];
                } else
                {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }
        }];
        
        
    } else if(_registerMode == FACEBOOK_MODE_V4){
        
        
        inputString = _imageTextField.mainTextField.text;
        
        if (inputString.length < USERNAME_MIN) {
            alertStr =[NSString stringWithFormat:LOCALIZE(@"REGISTER_ALERT_USERNAME_TOO_SHORT"),USERNAME_MIN];
        } else if(![nameTest evaluateWithObject:inputString]) {
            alertStr = LOCALIZE(@"REGISTER_ALERT_USERNAME_ILLEGAL");
        }
        
        if(![alertStr isEqualToString:@""]) {
            [_doneButton setSelected:NO];
            [self showAlertViewMessage:alertStr];
            return;
        }
        
        [DIALOG_MANAGER showLoadingView];
        [API_MANAGER checkOpenIDAvailable:[inputString lowercaseString] completion:^(BOOL success, NSString *message) {
            
            [DIALOG_MANAGER hideLoadingView];
            
            if (success) {
                
                [DEFAULTS setObject:[inputString lowercaseString] forKey:USER_OPEN_ID];
                [DEFAULTS synchronize];
                
                [API_MANAGER registerAction2:@{@"openID":[DEFAULTS objectForKey:USER_OPEN_ID] ,@"picture":[DEFAULTS objectForKey:CACHE_PICTURE],@"email":[DEFAULTS objectForKey:EMAIL],@"gender":[DEFAULTS objectForKey:GENDER], @"facebookID": [DEFAULTS objectForKey:FACEBOOK_ID]} completion:^(BOOL success, NSString *message) {
                    if (success) {
                        ConnectionViewController* conVC = [ConnectionViewController new];
                        [self.navigationController pushViewController:conVC animated:YES];
                    }else {
                        [_doneButton setSelected:NO];
                        [DIALOG_MANAGER showNetworkFailToast];
                    }
                }];
                
            } else {
                
                [_doneButton setSelected:NO];
                if ([message isEqualToString:@"open_id_not_available"]) {
                    [self showAlertViewMessage:LOCALIZE(@"REGISTER_ALERT_USERNAME_USED")];
                } else {
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }
        }];
    }else if(_registerMode == PASSWORD_MODE_V4){
        
        if (_imageTextField.mainTextField.text.length < PASSWORD_MIN) {
            [self showAlertViewMessage:[NSString stringWithFormat:LOCALIZE(@"REGISTER_ALERT_PWD_TOO_SHORT"),PASSWORD_MIN]];
            [_doneButton setSelected:NO];
        } else {
            
            /* To Phone Verification  */
            PhoneVerificationViewController* conVC = [PhoneVerificationViewController new];
            conVC.status = PHONE_MODE;
            [self.navigationController pushViewController:conVC animated:YES];
            
            NSString *md5Pwd = [_imageTextField.mainTextField.text MD5];
            [DEFAULTS setObject:md5Pwd forKey:PASSWORD];
            [DEFAULTS synchronize];
            
        }
    }else if(_registerMode == FULLNAME_MODE_V4){
        
        /* RegisterInfo */
        [API_MANAGER updateUserInfo:@{@"name":_imageTextField.mainTextField.text} fetchSelfInfo:NO completion:^(BOOL success){}];
        [self moveToMode:USER_IMAGE_MODE_V4];
        
    }else if(_registerMode == USER_IMAGE_MODE_V4){
        
        [API_MANAGER updateUserInfo:@{@"picture":GET_DEFAULT(CACHE_PICTURE)}  fetchSelfInfo:NO  completion:^(BOOL success) { }];
        ConnectionViewController* conVC = [ConnectionViewController new];
        [self.navigationController pushViewController:conVC animated:YES];
        
        
    }
}

- (void)skipAction:(id)sender
{
    if (_registerMode == FULLNAME_MODE_V4) {
        [EVENT_HANDLER addEventTracking:@"SkipEnterFullName" withDict:nil];
        /* RegisterInfo */
        [self moveToMode:USER_IMAGE_MODE_V4];
        
    }
    else if (_registerMode == USER_IMAGE_MODE_V4) {
        [EVENT_HANDLER addEventTracking:@"SkipSetProfilePicture" withDict:nil];
        ConnectionViewController* conVC = [ConnectionViewController new];
        [self.navigationController pushViewController:conVC animated:YES];
        
    }
}

@end
