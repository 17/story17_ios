//
//  ConnectionViewController.h
//  story17
//
//  Created by POPO Chen on 5/20/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, PhoneVerificationStatus) {
    PHONE_MODE = 0,
    VERIFY_MODE,

    RESET_PHONE_MODE,
    RESET_VERIFY_MODE,
    RESET_PASSWORD,
    
    REBIND_PHONE,
    REBIND_PHONE_ENTER_VERFICODE
};

typedef NS_ENUM(NSUInteger, HumanTestType) {
    HumanTestTypeSendPhoneVerificationCode,
    HumanTestTypeGetResetPasswordToken
};



@interface PhoneVerificationViewController : UIViewController
@property (nonatomic) PhoneVerificationStatus status;

@end
