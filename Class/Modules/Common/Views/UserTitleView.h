//
//  UserTitleView.h
//  Story17
//
//  Created by POPO on 2015/5/6.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserTitleView : UIView
{
@protected
    UILabel* _userTitle;
    UILabel* _userSubTitle;
    UIImageView* _userImage;
    UIImageView* _maskImage;
}

@property(nonatomic, strong)NSString* title;
@property(nonatomic, strong)NSString* subTitle;
@property(nonatomic, strong)NSString* image;
@property(nonatomic, strong)UIImageView* verifiedBadge;
@property(nonatomic, strong)UIImageView* choiceBadge;
@property(nonatomic, strong)UIImageView* internationalBadge;
@property(nonatomic, strong)UIImageView* chinaBadge;

+ (float)getDefaultHeight;
+ (float)getHeightFromSubstring:(NSString*)subTitleString;
- (void)maskUserImage:(BOOL)isMask;

- (void)verified:(BOOL)setBool;
- (void)choiced:(BOOL)setBool;
- (void)chinaFlag:(BOOL)setBool;
- (void)internationalFlag:(BOOL)setBool;

@end
