//
//  ArrowSwitchLabelView.m
//  Story17
//
//  Created by POPO on 5/12/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "ArrowSwitchLabelView.h"
#import "Constant.h"

@implementation ArrowSwitchLabelView

#define viewHeight 45

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setBackgroundColor:WHITE_COLOR];
        _arrowImageView = [[UIImageView alloc] init];
        [self addSubview:_arrowImageView];
        
        _selectedImageView = [[UIImageView alloc]init];
        _selectedImageView.hidden = YES;
        [self addSubview:_selectedImageView];
        
        _titleLabel = [[UILabel alloc] init];
        [self addSubview:_titleLabel];
        
        _switchView = [[UISwitch alloc] init];
        [_switchView setOnTintColor:MAIN_COLOR];
        [self addSubview:_switchView];
        
        _subTitleLabel = [UILabel new];
        _subTitleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        [_subTitleLabel setTextColor:GRAY_COLOR];
        [self addSubview:_subTitleLabel];
        _subTitleLabel.hidden = YES;
        _subTitleLabel.textAlignment = NSTextAlignmentRight;
        
        [self setup];
    }
    return self;
}

- (void)setup
{
    _titleLabel.frame = CGRectMake(25, 5, SCREEN_WIDTH-70, 35);
    [_titleLabel setTextColor:DARK_GRAY_COLOR];
    _titleLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    [_arrowImageView setImage:[UIImage imageNamed:@"nav_arrow_next"]];
    _arrowImageView.frame = CGRectMake(SCREEN_WIDTH-30, 15, 15, 15);
    [_selectedImageView setImage:[UIImage imageNamed:@"check_circle"]];
    _selectedImageView.frame = CGRectMake(SCREEN_WIDTH-30, 15, 15, 15);
    _switchView.frame = CGRectMake(SCREEN_WIDTH-70, 7, 35, 60);
    
    _subTitleLabel.frame = CGRectMake(SCREEN_WIDTH/2, 5, SCREEN_WIDTH/2-10, 35);
}

-(void)styleSettingsWithMode:(int)mode
{
    if(mode==WITH_ARROW){
        _switchView.hidden = YES;
        _selectedImageView.hidden = YES;
    }else if(mode==WITH_NONE){
        _switchView.hidden = YES;
        _arrowImageView.hidden = YES;
        _selectedImageView.hidden = YES;
    }else if(mode==WITH_SWITCH){
        _arrowImageView.hidden = YES;
        _selectedImageView.hidden = YES;
    }else if(mode==IS_SELECTED){
        _selectedImageView.hidden = NO;
    }else if(mode==WITH_SUBTITLE){
        _switchView.hidden = YES;
        _selectedImageView.hidden = YES;
        _subTitleLabel.hidden = NO;
        _arrowImageView.hidden = YES;
    }
}


-(int) getViewHeight
{
    return viewHeight;
}

@end
