//
//  ImageTextField.m
//  tex
//
//  Created by POPO on 4/15/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "ImageTextField.h"
#import "ThemeManager.h"

@implementation ImageTextField

@synthesize headImage;
@synthesize mainTextField;
@synthesize growTextView;

#define headerImageWidth 22
#define headerImageLeftMargin 12
#define ViewHeight 45.0

+(float)getDefaultHeight
{
    return ViewHeight;
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    
    headImage = [[UIImageView alloc]initWithFrame:CGRectMake(headerImageLeftMargin, frame.size.height/2-11, headerImageWidth, headerImageWidth)];
    [headImage setImage:[UIImage imageNamed:@"icon_write"]];
    
    mainTextField = [[UITextField alloc]initWithFrame:CGRectMake(headerImageLeftMargin+headerImageWidth+10, 0, SCREEN_WIDTH-(headerImageLeftMargin+headerImageWidth+10)-20, frame.size.height)];
    [mainTextField setFont:SYSTEM_FONT_WITH_SIZE(14)];
    mainTextField.textAlignment = NSTextAlignmentLeft;
    [mainTextField setTextColor:GRAY_COLOR];
    [mainTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [mainTextField setTintColor:MAIN_COLOR];
    
    growTextView = [[UIPlaceholderTextView alloc]initWithFrame:CGRectMake(headerImageLeftMargin+headerImageWidth+10, 5, SCREEN_WIDTH-(headerImageLeftMargin+headerImageWidth+10)-20, 30)];
    [growTextView setFont:SYSTEM_FONT_WITH_SIZE(14)];
    growTextView.textAlignment = NSTextAlignmentLeft;
    [growTextView setAutocorrectionType:UITextAutocorrectionTypeNo];
    growTextView.hidden = YES;
    [growTextView setBackgroundColor:[UIColor clearColor]];
    [growTextView setTextColor:GRAY_COLOR];
    
    _countLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-100, 30, 90, 20)];
    _countLabel.font = SYSTEM_FONT_WITH_SIZE(10);
    _countLabel.textAlignment = NSTextAlignmentRight;
    [_countLabel setTextColor:GRAY_COLOR];
    _countLabel.hidden = YES;
    
    _bgImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-20, ViewHeight)];
    [_bgImage setImage:[[UIImage imageNamed:@"input_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]];
    
    
    [self addSubview:_bgImage];
    [self addSubview:headImage];
    [self addSubview:mainTextField];
    [self addSubview:_countLabel];
    [self addSubview:growTextView];
    
    
    /* Phone Label users */
    _countryCodeLabel = [[UILabel alloc]initWithFrame:CGRectMake(headerImageLeftMargin+headerImageWidth+10, 0, 60, frame.size.height)];
    _countryCodeLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [_countryCodeLabel setTextColor:DARK_GRAY_COLOR];
    _countryCodeLabel.textAlignment = NSTextAlignmentCenter;
    
    _verticalLine = [ThemeManager separaterLine];
    _verticalLine.frame = CGRectMake(_countryCodeLabel.frame.origin.x+_countryCodeLabel.frame.size.width, 10, 0.5, frame.size.height-20);
    _countryCodeLabel.hidden = YES;
    _verticalLine.hidden = YES;
    [self addSubview:_countryCodeLabel];
    [self addSubview:_verticalLine];
    
    self.layer.masksToBounds = YES;
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    return self;
}

-(void)changeToGroupTextMode
{
    _countLabel.hidden = NO;
    growTextView.hidden = NO;
    mainTextField.hidden = YES;
}

-(void)changeToPhoneMode
{
    _countryCodeLabel.hidden = NO;
    _verticalLine.hidden = NO;
    mainTextField.frame = CGRectMake(_verticalLine.frame.origin.x+_verticalLine.frame.size.width+10, 0, SCREEN_WIDTH-_verticalLine.frame.origin.x+_verticalLine.frame.size.width-30, mainTextField.frame.size.height);

}


@end
