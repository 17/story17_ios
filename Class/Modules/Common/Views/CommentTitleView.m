//
//  CommentTitleView.m
//  story17
//
//  Created by POPO Chen on 5/28/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "CommentTitleView.h"

#import "Constant.h"
#import "UIImageView+AFNetworking.h"
#import "UserProfileViewController.h"
#import "BrowsePostsViewController.h"

#define UserTitleImageSize 45
#define SubTitleSize 14


@implementation CommentTitleView

@synthesize image = _image;

+ (float)getHeightFromSubstring:(NSString*)subTitleString
{
    
    CGSize rStringSize = [subTitleString getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-(UserTitleImageSize + 15),SCREEN_HEIGHT) withFont:SYSTEM_FONT_WITH_SIZE(SubTitleSize)];
    CGSize stringSize = VIEW_SIZE(rStringSize);
    return stringSize.height+15+UserTitleImageSize/2+1;
}

+ (float)getDefaultHeight
{
    return UserTitleImageSize + 10;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        _userImage = [[UIImageView alloc] init];
        [self addSubview:_userImage];
        
        _maskImage = [[UIImageView alloc] init];
        [self addSubview:_maskImage];
        
        [_maskImage setImage:[UIImage imageNamed:@"head_mask"]];
        
        _userTitle = [[UILabel alloc] init];
        _userTitle.font = SYSTEM_FONT_WITH_SIZE(14);
        [_userTitle setTextColor:MAIN_COLOR];
        [self addSubview:_userTitle];
        
        _userSubTitle = [[STTweetLabel alloc] init];
        [_userSubTitle setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                       NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(SubTitleSize)}
                             hotWord:STTweetHandle];
        [_userSubTitle setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                       NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(SubTitleSize)}
                             hotWord:STTweetHashtag];
        [_userSubTitle setAttributes:@{NSForegroundColorAttributeName: DARK_GRAY_COLOR,
                                       NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(SubTitleSize)}];
        
        _userSubTitle.userInteractionEnabled = YES;
        [self addSubview:_userSubTitle];
        
        [_userSubTitle setAttributes:@{NSForegroundColorAttributeName:DARK_GRAY_COLOR, NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:SubTitleSize]}];
        
        _bottomLine = [ThemeManager separaterLine];
        [self addSubview:_bottomLine];
        
    }
    
    return self;
}

- (void)setFrame:(CGRect)frame
{
    
    [super setFrame:frame];
    
    if (frame.size.width <= 0 || frame.size.height <= 0)
        return;
    
    if (_userImage == nil)
        return;
    
    CGRect rect = CGRectMake(5, 5, UserTitleImageSize, UserTitleImageSize);
    [_userImage setFrame:rect];
    [_maskImage setFrame:rect];
    _userImage.userInteractionEnabled = YES;
    
    rect.origin.x += rect.size.width + 5;
    rect.size.height = UserTitleImageSize/ 2;
    rect.size.width = frame.size.width - (UserTitleImageSize + 5);
    [_userTitle setFrame:rect];
    
    rect.origin.y = rect.size.height + rect.origin.y;
    [_userSubTitle setFrame:rect];
    
    CGSize stringSize = VIEW_SIZE([_userSubTitle suggestedFrameSizeToFitEntireStringConstrainedToWidth:(SCREEN_WIDTH-(UserTitleImageSize + 15))]);

    _userSubTitle.numberOfLines = 0;
    _userSubTitle.frame = CGRectMake(_userTitle.frame.origin.x, _userSubTitle.frame.origin.y,stringSize.width, stringSize.height);
    _bottomLine.frame = CGRectMake(0, _userSubTitle.frame.origin.y + _userSubTitle.frame.size.height+10, SCREEN_WIDTH, 0.5);
}

- (void)setTitle:(NSString *)title
{
    _userTitle.text = title;
}

- (NSString *)title
{
    return _userTitle.text;
}

- (void)setUserSubTitle:(NSString *)subTitle
{
    _userSubTitle.text = subTitle;    
    
    CGSize rStringSize = [subTitle getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-(UserTitleImageSize + 15),SCREEN_HEIGHT) withFont:SYSTEM_FONT_WITH_SIZE(SubTitleSize)];

    
    CGSize stringSize = VIEW_SIZE(rStringSize);

    _userSubTitle.numberOfLines = 0;
    _userSubTitle.frame = CGRectMake(_userTitle.frame.origin.x, _userSubTitle.frame.origin.y,stringSize.width, stringSize.height);
    _bottomLine.frame = CGRectMake(0, _userSubTitle.frame.origin.y + _userSubTitle.frame.size.height+9, SCREEN_WIDTH, 0.5);
}

- (NSString *)subTitle
{
    return _userSubTitle.text;
}

- (void)setImage:(NSString *)image
{
    
    if(image==NULL)
        return;
    
    _image = image;
    
    if(![image isEqualToString:@""]){
        [_userImage setImageWithURL:S3_THUMB_IMAGE_URL(image) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];

//        [_userImage my_setImageWithURL:image placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s") isThumbnail:YES];
    }else{
        [_userImage setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }
    
}

- (NSString *)image
{
    return _image;
}


@end
