//
//  UserTitleView.m
//  Story17
//
//  Created by POPO on 2015/5/6.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "UserTitleView.h"
#import "Constant.h"

#define UserTitleImageSize 50
#define SubTitleSize 14

@implementation UserTitleView

@synthesize image = _image;

+ (float)getHeightFromSubstring:(NSString*)subTitleString
{
    CGSize stringSize = [subTitleString getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-(UserTitleImageSize + 15),SCREEN_HEIGHT) withFont:SYSTEM_FONT_WITH_SIZE(SubTitleSize)];

    return stringSize.height+15+UserTitleImageSize/2;
}

+ (float)getDefaultHeight
{
    return UserTitleImageSize + 10;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        _userImage = [[UIImageView alloc] init];
        [self addSubview:_userImage];
        
        _maskImage = [[UIImageView alloc] init];
        [_maskImage setImage:[UIImage imageNamed:@"head_mask"]];
        [self addSubview:_maskImage];
        
        _userTitle = [[UILabel alloc] init];
        _userTitle.font = SYSTEM_FONT_WITH_SIZE(14);
        [_userTitle setTextColor:MAIN_COLOR];
        [self addSubview:_userTitle];
        
        _userSubTitle = [[UILabel alloc] init];
        _userSubTitle.font = SYSTEM_FONT_WITH_SIZE(SubTitleSize);
        [_userSubTitle setTextColor:DARK_GRAY_COLOR];
        [self addSubview:_userSubTitle];
        
        _verifiedBadge = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"verified")];
        [self addSubview:_verifiedBadge];
        _verifiedBadge.hidden = YES;
        
        _choiceBadge = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"badge_add_choice")];
        [self addSubview:_choiceBadge];
        _choiceBadge.hidden = YES;
        
        _chinaBadge = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"badge_china")];
        [self addSubview:_chinaBadge];
        _chinaBadge.hidden = YES;
        
        _internationalBadge = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"badge_international")];
        [self addSubview:_internationalBadge];
        _internationalBadge.hidden = YES;
        
    }
    
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    if (frame.size.width <= 0 || frame.size.height <= 0)
        return;
    
    if (_userImage == nil)
        return;
    
    CGRect rect = CGRectMake(5, 5, UserTitleImageSize, UserTitleImageSize);
    [_userImage setFrame:rect];
    [_maskImage setFrame:rect];
    
    rect.origin.x += rect.size.width + 5;
    rect.size.height = UserTitleImageSize/ 2;
    rect.size.width = frame.size.width - (UserTitleImageSize + 5);
    [_userTitle setFrame:rect];
    
    rect.origin.y = rect.size.height + rect.origin.y;
    [_userSubTitle setFrame:rect];

    CGSize stringSize = [_userSubTitle.text getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-(UserTitleImageSize + 15),SCREEN_HEIGHT) withFont:SYSTEM_FONT_WITH_SIZE(SubTitleSize)];

    _userSubTitle.numberOfLines = 0;
    _userSubTitle.frame = CGRectMake(_userSubTitle.frame.origin.x, _userSubTitle.frame.origin.y,stringSize.width, stringSize.height);
    
}

- (void)setTitle:(NSString *)title
{
    _userTitle.text = title;
    [_userTitle sizeToFit];
    _verifiedBadge.frame = CGRectMake(_userTitle.frame.origin.x+_userTitle.frame.size.width, _userTitle.frame.origin.y, 15, 15);
    _choiceBadge.frame = CGRectMake(_verifiedBadge.frame.origin.x+_verifiedBadge.frame.size.width, _userTitle.frame.origin.y, 15, 15);
    _chinaBadge.frame = CGRectMake(_choiceBadge.frame.origin.x+_choiceBadge.frame.size.width, _userTitle.frame.origin.y, 15, 15);
    _internationalBadge.frame = CGRectMake(_chinaBadge.frame.origin.x+_chinaBadge.frame.size.width, _userTitle.frame.origin.y, 15, 15);

}

- (NSString *)title
{
    return _userTitle.text;
}

- (void)setSubTitle:(NSString *)subTitle
{
    _userSubTitle.text = subTitle;

    CGSize stringSize = [subTitle getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-(UserTitleImageSize + 15),SCREEN_HEIGHT) withFont:SYSTEM_FONT_WITH_SIZE(SubTitleSize)];

    _userSubTitle.numberOfLines = 0;
    _userSubTitle.frame = CGRectMake(_userSubTitle.frame.origin.x, _userSubTitle.frame.origin.y,stringSize.width, stringSize.height);

}

- (NSString *)subTitle
{
    return _userSubTitle.text;
}

- (void)setImage:(NSString *)image
{
//    if(image==NULL)
//        return;
    
    _image = image;
    if([image isEqualToString:@""]){
        [_userImage setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }else{
        
        [_userImage setImageWithURL:S3_THUMB_IMAGE_URL(image) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
        
//        [_userImage setImageWithURL:S3_THUMB_IMAGE_URL(image) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
//        [_userImage my_setImageWithURL:image placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s") isThumbnail:YES];
    }

}

- (NSString *)image
{
    return _image;
}


-(void)verified:(BOOL)setBool
{
    if(setBool==true){
        _verifiedBadge.hidden = NO;
    }else{
        _verifiedBadge.hidden = YES;
    }
}

-(void)choiced:(BOOL)setBool
{
    if(setBool==true && [GET_DEFAULT(IS_ADMIN) intValue] >=1){
        _choiceBadge.hidden = NO;
    }else{
        _choiceBadge.hidden = YES;
    }
}

-(void)chinaFlag:(BOOL)setBool
{
    if(setBool==true && [GET_DEFAULT(IS_ADMIN) intValue] >=1){
        _chinaBadge.hidden = NO;
    }else{
        _chinaBadge.hidden = YES;
    }
}


-(void)internationalFlag:(BOOL)setBool
{
    if(setBool==true && [GET_DEFAULT(IS_ADMIN) intValue] >=1){
        _internationalBadge.hidden = NO;
    }else{
        _internationalBadge.hidden = YES;
    }
}


- (void)maskUserImage:(BOOL)isMask
{
    if (isMask) {
        _maskImage.hidden = NO;
    } else {
        _maskImage.hidden = YES;
    }
}


@end
