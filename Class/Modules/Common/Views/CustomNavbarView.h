//
//  CustomNavbarView.h
//  story17
//
//  Created by POPO Chen on 5/22/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavbarView : UIView

@property (nonatomic,strong) UIButton* backBtn;
@property (nonatomic,strong) UIButton* nextBtn;
@property (nonatomic,strong) UILabel* titleLabel;

-(void)setTitleLabel:(NSString *)titleStr;

@end
