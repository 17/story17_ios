//
//  CustomNavbarView.m
//  story17
//
//  Created by POPO Chen on 5/22/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "CustomNavbarView.h"
#import "Constant.h"

@implementation CustomNavbarView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _titleLabel = [UILabel new];
        _titleLabel.frame = CGRectMake(SCREEN_WIDTH/2-100, 0, 200, NAVI_BAR_HEIGHT);
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [_titleLabel setTextColor:WHITE_COLOR];
        _titleLabel.font =  BOLD_FONT_WITH_SIZE(18);
        
        _backBtn = [UIButton new];
        _backBtn.frame = CGRectMake(5, 0, NAVI_BAR_HEIGHT*2, NAVI_BAR_HEIGHT);
        [_backBtn setImage:[UIImage imageNamed:@"nav_arrow_back_black"] forState:UIControlStateNormal];
//        [_backBtn setBackgroundColor:TIFFINY_BLUE_COLOR];
        _backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 40);

        _nextBtn = [UIButton new];
        _nextBtn.frame = CGRectMake(SCREEN_WIDTH-60-10, 0, 60, NAVI_BAR_HEIGHT);
        [_nextBtn setTitle:LOCALIZE(@"skip") forState:UIControlStateNormal];
        _nextBtn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(16);
        [_nextBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
        [_nextBtn setTitleColor:BLACK_COLOR forState:UIControlStateHighlighted];
        
        [self addSubview:_backBtn];
        [self addSubview:_nextBtn];
        [self addSubview:_titleLabel];
    }
    return self;
}

-(void)setTitleLabel:(NSString *)titleStr
{
    [_titleLabel setText:titleStr];
}

-(void)setNextBtn:(NSString *)btnStr
{
    [_nextBtn setTitle:btnStr forState:UIControlStateNormal];
}

@end
