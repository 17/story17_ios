//
//  ArrowSwitchLabelView.h
//  Story17
//
//  Created by POPO on 5/12/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

//typedef enum
//{
//    ViewWithArrow = 0,
//    ViewWithSwitch = 1,
//    ViewWithNone
//} ArrowSwithLabelViewType;
//

@interface ArrowSwitchLabelView : UIView

@property (nonatomic,strong) UILabel* titleLabel;
@property (nonatomic,strong) UIImageView* arrowImageView;
@property (nonatomic,strong) UISwitch* switchView;
@property (nonatomic,strong) UIImageView* selectedImageView;
@property (nonatomic,strong) UILabel* subTitleLabel;

-(void)styleSettingsWithMode:(int)mode;
-(int) getViewHeight;

@end
