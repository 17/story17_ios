//
//  ImageTextField.h
//  tex
//
//  Created by POPO on 4/15/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "UIPlaceholderTextView.h"

@interface ImageTextField : UIView

@property (nonatomic,strong) UIImageView* headImage;
@property (nonatomic,strong) UITextField* mainTextField;
@property (nonatomic,strong) UIImageView* bgImage;

@property (nonatomic,strong) UIPlaceholderTextView* growTextView;
@property (nonatomic,strong) UILabel* countLabel;
@property (nonatomic,strong) UIImageView* textBGImageView;

/* for phone only */
@property (nonatomic,strong) UILabel* countryCodeLabel;
@property (nonatomic,strong) UIView* verticalLine;


+(float)getDefaultHeight;
-(void)changeToGroupTextMode;
-(void)changeToPhoneMode;

@end
