//
//  CommentTitleView.h
//  story17
//
//  Created by POPO Chen on 5/28/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STTweetLabel.h"

@interface CommentTitleView : UIView

@property(nonatomic, strong)UILabel* userTitle;
@property(nonatomic, strong)UIImageView* maskImage;
@property(nonatomic, strong)UIView* bottomLine;

@property(nonatomic, strong)NSString* title;
@property(nonatomic, strong)NSString* image;
@property(nonatomic, strong)STTweetLabel* userSubTitle;
@property(nonatomic, strong)UIImageView* userImage;

+ (float)getDefaultHeight;
+ (float)getHeightFromSubstring:(NSString*)subTitleString;

//@property(nonatomic, weak)UINavigationController* navCtrl;


@end
