//
//  DatabaseManager.h
//  story17
//
//  Created by POPO Chen on 6/23/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMDatabasePool.h"
#import "FMDatabaseQueue.h"


@protocol DownLoadVideoDelegate <NSObject>


@end

@interface DatabaseManager : NSObject

typedef void (^CallbackWithChatThreads)(BOOL success, NSArray* chatThreads);
typedef void (^CallbackWithMessages)(BOOL success, NSArray* messageObjects);
typedef void (^CallbackWithCount)(int count);

+ (id)sharedInstance;
-(void)createCatchVideoTable;
-(void)insertCacheVideo:(NSString*)videoUUID;
-(void)refreshVideoCacheData;

-(void)insertReportRecord:(NSString*)paramID type:(NSString*)type;
-(NSArray*)getReportRecord:(NSString*)reportType;

-(BOOL)fileExist:(NSString*)videoName;
-(BOOL)isDownloading:(NSString*)videoName;

-(void)downloadVideo:(NSString*)video withCallback:(void(^)(BOOL success,NSString* status))callback;
-(void)removeCacheVideoInfo;

@end
