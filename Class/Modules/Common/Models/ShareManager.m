//
//  ShareManager.m
//  Story17
//
//  Created by York on 2016/1/8.
//  Copyright © 2016年 POPO_INNOVATION. All rights reserved.
//

#import "ShareManager.h"
#import "QQApiInterface.h"

@implementation ShareManager

-(id)init
{
    self = [super init];
    if(self){
    }
    return self;
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


-(void)shareFacebookWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:[UIColor blackColor]];
        
        SLComposeViewController *mySocialComposeView = [[SLComposeViewController alloc] init];
        mySocialComposeView = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [mySocialComposeView setInitialText:@"一起來玩17"];
        if ([shareUrl isEqualToString:@""]) {
            
        }
        NSURL *myURL = [[NSURL alloc] initWithString:shareUrl];
        [mySocialComposeView addURL: myURL];
        
        // 插入圖片
        UIImage *myImage = [UIImage imageNamed:@"share_17"];
        [mySocialComposeView addImage:myImage];
        
        // 呼叫建立的SocialComposeView
        [[self topMostController] presentViewController:mySocialComposeView animated:YES completion:^{
        }];
        
        // 訊息成功送出與否的之後處理
        [mySocialComposeView setCompletionHandler:^(SLComposeViewControllerResult result){
        }];
    }   else {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:LOCALIZE(@"Notice") message:LOCALIZE(@"Please_set_up_your_FB_Acc") delegate:nil cancelButtonTitle:LOCALIZE(@"ok") otherButtonTitles:nil, nil];
        [av show];
    }
}

-(void)shareTwitterWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage
{
    TWTRComposer *composer = [[TWTRComposer alloc] init];

    [composer setText:shareUrl];
    [composer setImage:[UIImage imageNamed:@"share_17"]];

    // Called from a UIViewController
    [composer showFromViewController:[self topMostController] completion:^(TWTRComposerResult result) {
    }];
}


-(void)shareInstagramWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage
{
    UIImage* image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:shareImage]]];

    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if([[UIApplication sharedApplication] canOpenURL:instagramURL]) //check for App is install or not
    {
        NSData *imageData = UIImagePNGRepresentation(image); //convert image into .png format.
        NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
        NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
        NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"insta.igo"]]; //add our image to the path
        [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
        NSLog(@"image saved");

        CGRect rect = CGRectMake(0 ,0 , 0, 0);
        UIGraphicsBeginImageContextWithOptions([self topMostController].view.bounds.size, [self topMostController].view.opaque, 0.0);
        [[self topMostController].view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIGraphicsEndImageContext();
        NSString *fileNameToSave = [NSString stringWithFormat:@"Documents/insta.igo"];
        NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:fileNameToSave];
        NSLog(@"jpg path %@",jpgPath);
        NSString *newJpgPath = [NSString stringWithFormat:@"file://%@",jpgPath];
        NSLog(@"with File path %@",newJpgPath);
        NSURL *igImageHookFile = [[NSURL alloc]initFileURLWithPath:newJpgPath];
        NSLog(@"url Path %@",igImageHookFile);

        self.documentController.UTI = @"com.instagram.exclusivegram";
        self.documentController = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
        self.documentController=[UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
        NSString *caption = @"#Your Text"; //settext as Default Caption
        self.documentController.annotation=[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",caption],@"InstagramCaption", nil];
        [self.documentController presentOpenInMenuFromRect:rect inView: [self topMostController].view animated:YES];
    }
    else
    {
        NSLog (@"Instagram not found");
    }
}

-(void)shareLineWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage
{
    NSString * urlWhats = [NSString stringWithFormat:@"line://msg/text/%@",shareUrl];
    NSURL *appURL = [NSURL URLWithString:urlWhats];
    if ([[UIApplication sharedApplication] canOpenURL: appURL]) {
        [[UIApplication sharedApplication] openURL: appURL];
    }
    else { //如果使用者沒有安裝，連結到App Store
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:LOCALIZE(@"Notice") message:LOCALIZE(@"Not_install_line") delegate:self cancelButtonTitle:LOCALIZE(@"ok") otherButtonTitles:nil];
        [alert show];
    }
}


-(void)shareWechatWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage
{
    //if the Weixin app is not installed, show an error
    if (![WXApi isWXAppInstalled]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:LOCALIZE(@"Please_install_wechat_App") delegate:nil cancelButtonTitle:LOCALIZE(@"ok") otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    //create a message object
    WXMediaMessage *message = [WXMediaMessage message];
    
    WXImageObject *ext = [WXImageObject object];
    message.mediaObject = ext;
    message.title=@"一起來玩17";
    message.description=@"點這網址一起來";
    //                [message setThumbImage:[UIImage imageNamed:@"share_17"]];
    
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = YES;
    req.text = shareUrl;
    req.scene = WXSceneSession;
    req.message=message;
    //                [WXApi sendReq:req];
    if (![WXApi sendReq:req]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:LOCALIZE(@"Please_install_wechat_App") delegate:nil cancelButtonTitle:LOCALIZE(@"ok") otherButtonTitles: nil];
        [alert show];
        
    }
}

-(void)shareMomentsWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage
{
    if (![WXApi isWXAppInstalled]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:LOCALIZE(@"Please_install_wechat_App") delegate:nil cancelButtonTitle:LOCALIZE(@"ok") otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    //create a message object
    WXMediaMessage *message = [WXMediaMessage message];
    
    WXImageObject *ext = [WXImageObject object];
    message.mediaObject = ext;
    message.title=@"一起來玩17";
    message.description=@"點這網址一起來";
    
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = YES;
    req.text = shareUrl;
    req.scene = WXSceneTimeline;
    req.message=message;
    //                [WXApi sendReq:req];
    if (![WXApi sendReq:req]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:LOCALIZE(@"Please_install_wechat_App") delegate:nil cancelButtonTitle:LOCALIZE(@"ok") otherButtonTitles: nil];
        [alert show];
    }

}


-(void)shareWeiboWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage
{
    // 判斷社群網站的服務是否可用
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeSinaWeibo]) {
        
        SLComposeViewController *mySocialComposeView = [[SLComposeViewController alloc] init];
        mySocialComposeView = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeSinaWeibo];
        
        [mySocialComposeView setInitialText:@"一起來玩17"];
        
        NSURL *myURL = [[NSURL alloc] initWithString:shareUrl];
        [mySocialComposeView addURL: myURL];
        
        // 插入圖片
        UIImage *myImage = [UIImage imageNamed:@"share_17"];
        [mySocialComposeView addImage:myImage];
        
        // 呼叫建立的SocialComposeView
        [[self topMostController] presentViewController:mySocialComposeView animated:YES completion:^{
        }];
        
        // 訊息成功送出與否的之後處理
        [mySocialComposeView setCompletionHandler:^(SLComposeViewControllerResult result){
        }];
    }   else {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:LOCALIZE(@"Notice") message:LOCALIZE(@"Please_set_up_your_weibo_Acc") delegate:nil cancelButtonTitle:LOCALIZE(@"ok") otherButtonTitles:nil, nil];
        [av show];
    }

}


-(void)shareWhatsappWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage
{
    NSString * msg = shareUrl;
    NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=%@",msg];
    NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    } else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:LOCALIZE(@"Notice") message:LOCALIZE(@"Not_install_Whatsapp") delegate:self cancelButtonTitle:LOCALIZE(@"ok") otherButtonTitles:nil];
        [alert show];
    }
}

-(void)shareMessageWithUrl:(NSString*)shareUrl
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = shareUrl;
        controller.messageComposeDelegate = self;
        [[self topMostController] presentViewController:controller animated:YES completion:NULL];
    }
}

-(void)shareMailWithUrl:(NSString*)shareUrl
{
    NSString *emailTitle = @"一起來玩17";
    // Email Content
    NSString *messageBody = shareUrl;
    // To address
    //            NSArray *toRecipents = [NSArray arrayWithObject:@"support@appcoda.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    //            [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    if ([MFMailComposeViewController canSendMail]) {
        [[self topMostController] presentViewController:mc animated:YES completion:NULL];
    }
}

-(void)shareQQWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage
{
    QQApiTextObject* txtObj = [QQApiTextObject objectWithText:shareUrl];
    SendMessageToQQReq* req = [SendMessageToQQReq reqWithContent:txtObj];
    QQApiSendResultCode sent = [QQApiInterface sendReq:req];
    [self handleSendResult:sent];
}

-(void)shareQzoneWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage withObj:(id)obj
{
    NSURL *previewURL = [NSURL URLWithString:shareImage];
    NSURL* url = [NSURL URLWithString:shareUrl];
    
    if([obj isKindOfClass:[UserObject class]]){
        UserObject* user = obj;
        shareText = [NSString stringWithFormat:LOCALIZE(@"SHARE_USER"), user.openID, @""];
        previewURL = S3_THUMB_IMAGE_URL(user.picture);
        
    }else if([obj isKindOfClass:[PostObject class]]){
        
        PostObject* post = obj;
        if ([post.type isEqualToString:@"video"]) {
            shareText = [NSString stringWithFormat:LOCALIZE(@"SHARE_VIDEO_POST"), post.user.openID , @""];
        } else {
            shareText = [NSString stringWithFormat:LOCALIZE(@"SHARE_PHOTO_POST"), post.user.openID , @""];
        }
        previewURL = S3_THUMB_IMAGE_URL(post.picture);
        
    }else if([obj isKindOfClass:[LiveStreamObject class]]){
        LiveStreamObject* livestream = obj;
        shareText = [NSString stringWithFormat:LOCALIZE(@"SHARE_LIVE_STREAM"), livestream.user.openID , @""];
        previewURL = S3_THUMB_IMAGE_URL(livestream.user.picture);
        
    }else{
        
        /* share app */
        DLog(@"Share app");
    }
    
    //            DLog(@"shareText:%@",shareText);
    //            DLog(@"previewURL:%@",previewURL);
    //            DLog(@"url:%@",url);
    //            DLog(@"shareImage:%@",shareImage);
    
    QQApiNewsObject* imgObj = [QQApiNewsObject objectWithURL:url title:shareText description:shareText previewImageURL:previewURL];
    [imgObj setTitle:shareText ? : @""];
    [imgObj setCflag:kQQAPICtrlFlagQZoneShareOnStart];
    SendMessageToQQReq* req = [SendMessageToQQReq reqWithContent:imgObj];
    QQApiSendResultCode sent = [QQApiInterface sendReq:req];
    [self handleSendResult:sent];

}

-(void)copyUrl:(NSString *)shareUrl
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.URL = [NSURL URLWithString:shareUrl];
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:LOCALIZE(@"Notice") message:LOCALIZE(@"Already_copy") delegate:nil cancelButtonTitle:LOCALIZE(@"ok") otherButtonTitles:nil, nil];
    [av show];
}

- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}

#pragma mark  -  MFMessageComposeViewControllerDelegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
}


- (void)handleSendResult:(QQApiSendResultCode)sendResult
{
    switch (sendResult)
    {
        case EQQAPIAPPNOTREGISTED:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"App未注册" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];
            
            break;
        }
        case EQQAPIMESSAGECONTENTINVALID:
        case EQQAPIMESSAGECONTENTNULL:
        case EQQAPIMESSAGETYPEINVALID:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"发送参数错误" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];
            
            break;
        }
        case EQQAPIQQNOTINSTALLED:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"未安装手Q" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];
            
            break;
        }
        case EQQAPIQQNOTSUPPORTAPI:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"API接口不支持" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];
            
            break;
        }
        case EQQAPISENDFAILD:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"发送失败" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];
            
            break;
        }
        default:
        {
            break;
        }
    }
}

@end
