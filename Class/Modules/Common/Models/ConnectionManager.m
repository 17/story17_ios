//
//  FacebookManager.m
//  story17
//
//  Created by POPO Chen on 5/20/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "ConnectionManager.h"
#import "Constant.h"
#import <AddressBook/AddressBook.h>
#import "ContactsData.h"

@implementation ConnectionManager

- (id)init
{
    self = [super init];
    if (self)
    {
        /* Contact Status Related */
        _contactsArray = CREATE_MUTABLE_ARRAY;
        _fullNameArray = CREATE_MUTABLE_ARRAY;
        _phoneNumerArray = CREATE_MUTABLE_ARRAY;
        _numberMapNameDict = CREATE_MUTABLE_DICTIONARY;
        _nameMapNumberDict = CREATE_MUTABLE_DICTIONARY;
        _getImageFromFullName = CREATE_MUTABLE_DICTIONARY;
        
        /* Contact Detail Related */
        _addedFriends = CREATE_MUTABLE_ARRAY;
        _unAddedFriends = CREATE_MUTABLE_ARRAY;
        _unPlayedFriends = CREATE_MUTABLE_ARRAY;
        
    }
    
    return self;
}

//-(void) showFBPermissionAlert
//{
//    if (![FACEBOOK_MANAGER isLoginned]) {
//        
//        [FACEBOOK_MANAGER loginAction:self];
//        
//    }else{
//        [self loginSuccessHandler];
//    }
//}

-(void)loginSuccessHandler
{
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if(error!=nil) {
             [DIALOG_MANAGER showNetworkFailToast];
             return ;
         }
         NSString* facebookID = result[@"id"];
         NSString* fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
         
         _fbName = result[@"id"];
         
         [API_MANAGER updateUserInfo:@{@"facebookID":facebookID,@"facebookAccessToken":fbAccessToken} fetchSelfInfo:NO completion:^(BOOL success) {
         }];
     }];

}



#pragma mark - AddressBook
-(NSArray *)getAllContacts
{
    NSMutableArray* contactList = [[NSMutableArray alloc] init];
    CFErrorRef *error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    
    for (int i=0;i < nPeople;i++) {
        ContactsData *contacts = [ContactsData new];
        
        ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
        
        //get First Name
        CFStringRef firstName = (CFStringRef)ABRecordCopyValue(person,kABPersonFirstNameProperty);
        contacts.firstNames = [(__bridge NSString*)firstName copy];
        
        if (firstName != NULL) {
            CFRelease(firstName);
        }
        
        
        //get Last Name
        CFStringRef lastName = (CFStringRef)ABRecordCopyValue(person,kABPersonLastNameProperty);
        contacts.lastNames = [(__bridge NSString*)lastName copy];
        
        if (lastName != NULL) {
            CFRelease(lastName);
        }
        
        
        if (!contacts.firstNames) {
            contacts.firstNames = @"";
        }
        
        if (!contacts.lastNames) {
            contacts.lastNames = @"";
        }
        
        contacts.fullName = [NSString stringWithFormat:@"%@ %@", contacts.firstNames, contacts.lastNames];
        
        // get contacts picture, if pic doesn't exists, show standart one
        CFDataRef imgData = ABPersonCopyImageData(person);
        NSData *imageData = (__bridge NSData *)imgData;
        contacts.image = [UIImage imageWithData:imageData];
        
        if (imgData != NULL) {
            CFRelease(imgData);
        }
        
        
        if (!contacts.image) {
            contacts.image = [UIImage imageNamed:@"icon"];
        }
        
        
        //get Phone Numbers
        NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
        ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);
        
        for(CFIndex i=0; i<ABMultiValueGetCount(multiPhones); i++) {
            @autoreleasepool {
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                NSString *phoneNumber = CFBridgingRelease(phoneNumberRef);
                if (phoneNumber != nil)[phoneNumbers addObject:phoneNumber];
                //NSLog(@"All numbers %@", phoneNumbers);
            }
        }
        
        if (multiPhones != NULL) {
            CFRelease(multiPhones);
        }
        
        [contacts setNumbers:phoneNumbers];
        
        //get Contact email
        NSMutableArray *contactEmails = [NSMutableArray new];
        ABMultiValueRef multiEmails = ABRecordCopyValue(person, kABPersonEmailProperty);
        
        for (CFIndex i=0; i<ABMultiValueGetCount(multiEmails); i++) {
            @autoreleasepool {
                CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
                NSString *contactEmail = CFBridgingRelease(contactEmailRef);
                if (contactEmail != nil)[contactEmails addObject:contactEmail];
                // NSLog(@"All emails are:%@", contactEmails);
            }
        }
        
        if (multiPhones != NULL) {
            CFRelease(multiEmails);
        }
        
        [contacts setEmails:contactEmails];
        
        [contactList addObject:contacts];
        
        
        //        NSMutableDictionary *dOfPerson=[NSMutableDictionary dictionary];
        //
        //        ABRecordRef ref = CFArrayGetValueAtIndex(allPeople,i);
        //
        //        //For username and surname
        //        ABMultiValueRef phones =(__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonPhoneProperty));
        //
        //        CFStringRef firstName, lastName;
        //        firstName = ABRecordCopyValue(ref, kABPersonFirstNameProperty);
        //        lastName  = ABRecordCopyValue(ref, kABPersonLastNameProperty);
        //        [dOfPerson setObject:[NSString stringWithFormat:@"%@ %@", firstName, lastName] forKey:@"name"];
        //
        //        //For Email ids
        //        ABMutableMultiValueRef eMail  = ABRecordCopyValue(ref, kABPersonEmailProperty);
        //        if(ABMultiValueGetCount(eMail) > 0) {
        //            [dOfPerson setObject:(__bridge NSString *)ABMultiValueCopyValueAtIndex(eMail, 0) forKey:@"email"];
        //
        //        }
        //
        //        //For Phone number
        //        NSString* mobileLabel;
        //
        //        for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++) {
        //            mobileLabel = (__bridge NSString*)ABMultiValueCopyLabelAtIndex(phones, j);
        //            if([mobileLabel isEqualToString:(NSString *)kABPersonPhoneMobileLabel])
        //            {
        //                [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, j) forKey:@"Phone"];
        //            }
        //            else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneIPhoneLabel])
        //            {
        //                [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, j) forKey:@"Phone"];
        //                break ;
        //            }
        //            
        //        }
        //        [contactList addObject:dOfPerson];
        
    }
    NSLog(@"Contacts = %@",contactList);
    return [contactList copy];
}


-(void) showPermissionAlert:(void(^)(BOOL success,NSString* message))callback
{
    ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
        if (!granted){
            [DIALOG_MANAGER showActionSheetOKDialogTitle:@"Cannot Add Contact" message:@"You must give the app permission to add the contact first." buttonText:@"OK" cancelable:NO withCompletion:^(BOOL okClicked) {
            }];
            return;
        }
        [self loadingContact:^(BOOL success) {
            callback(YES,@"Done");
        }];
        return;
    });
}


-(void)loadingContact:(void(^)(BOOL success))callback
{
    DLog(@"loadingContact");
    
    dispatch_async(GLOBAL_QUEUE, ^{
        NSMutableArray* allContacts = CREATE_MUTABLE_ARRAY;
        allContacts = [[self getAllContacts] mutableCopy];
        DLog(@"finished");
        
        for(ContactsData* contact in allContacts){
            
            NSString* fullName = [NSString stringWithFormat:@"%@%@",contact.firstNames,contact.lastNames ];
            fullName = [fullName stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSMutableArray* phones = CREATE_MUTABLE_ARRAY;
            if(contact.image!=nil)
                _getImageFromFullName[fullName] = contact.image;
            
            if([self validName:fullName]){
                
                phones = [[self validPhoneNumber:contact.numbers] copy];
                
                for(NSString* valied in phones){
                    [_contactsArray addObject:valied];
                    NSString* countryCode = GET_DEFAULT(COUNTRY_CODE);
                    
                    _numberMapNameDict[[NSString stringWithFormat:@"%@%@",countryCode,valied]] = fullName;
                    _numberMapNameDict[valied] = fullName;
                    [_phoneNumerArray addObject:valied];

                }
                
                if([_nameMapNumberDict objectForKey:fullName]==nil){
                    
                    [_fullNameArray addObject:fullName];
                    
                    if([phones count]!=0 && fullName.length!=0){
                        _nameMapNumberDict[fullName] = phones;
                    }
                    
                    
                } else{
                    // to handle different person but same name
                    //[_numberMapNumberDict[fullName] addObject:contact.numbers];
                }
                
            }
        }
        
        dispatch_async(MAIN_QUEUE, ^{
            callback(YES);
        });
    });

    
    
    
//    [DIALOG_MANAGER showLoadingView];
    
//    dispatch_async(GLOBAL_QUEUE, ^{
//        NSMutableArray* allContacts = CREATE_MUTABLE_ARRAY;
//        allContacts = [[self getAllContacts] mutableCopy];
//        
//        for(ContactsData* contact in allContacts){
//            
//            NSString* fullName = [NSString stringWithFormat:@"%@%@",contact.firstNames,contact.lastNames ];
//            fullName = [fullName stringByReplacingOccurrencesOfString:@" " withString:@""];
//            NSMutableArray* phones = CREATE_MUTABLE_ARRAY;
//            if(contact.image!=nil)
//                _getImageFromFullName[fullName] = contact.image;
//            
//            if([self validName:fullName]){
//                phones = [[self validPhoneNumber:contact.numbers] copy];
//                
//                for(NSString* valied in phones){
//                    [_contactsArray addObject:valied];
//                    NSString* countryCode = GET_DEFAULT(COUNTRY_CODE);
//                    
//                    _numberMapNameDict[[NSString stringWithFormat:@"%@%@",countryCode,valied]] = fullName;
//                    _numberMapNameDict[valied] = fullName;
//                    [_phoneNumerArray addObject:valied];
//                }
//                
//                if([_nameMapNumberDict objectForKey:fullName]==nil){
//                    
//                    [_fullNameArray addObject:fullName];
//                    
//                    if([phones count]!=0 && fullName.length!=0){
//                        _nameMapNumberDict[fullName] = phones;
//                    }
//                    
//                    
//                } else{
//                    // to handle different person but same name
//                    //[_numberMapNumberDict[fullName] addObject:contact.numbers];
//                }
//                
//            }
//        }
//        dispatch_async(MAIN_QUEUE, ^{
//            callback(YES);
//        });
//    });
}

-(NSArray*) validPhoneNumber:(NSArray*)phoneNum
{
    NSMutableArray* phones = CREATE_MUTABLE_ARRAY;
    for(NSString* phone in phoneNum){
        NSString* validPhone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
        validPhone = [validPhone stringByReplacingOccurrencesOfString:@"(" withString:@""];
        validPhone = [validPhone stringByReplacingOccurrencesOfString:@")" withString:@""];
        validPhone = [validPhone stringByReplacingOccurrencesOfString:@"+" withString:@""];
        validPhone = [validPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
        validPhone = [validPhone stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        if ([validPhone length]>1) {
            NSString* firstIndex = [validPhone substringToIndex:1];
            if([firstIndex isEqualToString:@"0"]){
                validPhone = [validPhone substringWithRange:NSMakeRange(1, [validPhone length]-1)];
            }
            [phones addObject:validPhone];
        }

    }
    
    return phones;
}


-(BOOL) validName:(NSString*)fullName
{
    NSString* nospaceName  = [fullName stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"@#"] invertedSet];
    set = [set invertedSet];
    
    if ([nospaceName rangeOfCharacterFromSet:set].location != NSNotFound || nospaceName.length<=1) {
        return false;
    }
    
    return true;
}

#pragma mark - geter
-(NSArray*)getPhoneNumerForSearch
{
    return [_phoneNumerArray copy];
}

-(NSString*)getRealNameByPhone:(NSString*)phone
{
    return [_numberMapNameDict objectForKey:phone];
}

@end

