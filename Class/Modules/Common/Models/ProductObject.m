//
//  ProductObject.m
//  Story17
//
//  Created by POPO on 11/17/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "ProductObject.h"

@implementation ProductObject

+ (ProductObject*)getProductWithDict:(NSDictionary *)productInfoDict
{
    ProductObject* product = [[ProductObject alloc] init];
    
    product.sequence = [productInfoDict[@"sequence"] intValue];
    product.productID = productInfoDict[@"productID"];
    product.price = [productInfoDict[@"price"] floatValue];
    product.point = [productInfoDict[@"point"] intValue];
    product.bonusAmount = [productInfoDict[@"bonusAmount"] intValue];
    product.bonusPercentage = [productInfoDict[@"bonusPercentage"] intValue];
    product.name = productInfoDict[@"name"];

    return product;
}

@end
