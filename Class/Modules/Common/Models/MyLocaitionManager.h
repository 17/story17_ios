//
//  MyLocaitionManager.h
//  story17
//
//  Created by POPO Chen on 7/16/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

@protocol UpdateLocationDelegate <NSObject>
-(void)finishUpdateLocation:(BOOL)success localName:(NSString*)localName geo:(NSDictionary*)geo;
-(void)googlePlaceCallback:(BOOL)success;
@end

@interface MyLocaitionManager : NSObject <CLLocationManagerDelegate>

+(MyLocaitionManager*) sharedInstance;

@property (strong,nonatomic) NSMutableArray *locationObjArray;
@property (nonatomic,weak) id<UpdateLocationDelegate> delegate;

@property(nonatomic, readonly) BOOL isUpdating;
@property(nonatomic, readonly) BOOL eof;
@property(nonatomic, readonly) int lastUpdateTime;

-(BOOL) locationEnable;
-(NSDictionary*)getNowLocation;
-(void) updateLocation;

-(void) googleSearchPlace:(NSString *)name withCallback:(void (^)(BOOL succcess,NSArray* locObjArray))callback;

@end


