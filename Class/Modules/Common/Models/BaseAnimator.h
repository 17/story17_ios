//
//  BaseAnimator.h
//  Story17
//
//  Created by Racing on 2015/11/30.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ModalAnimatedTransitioningType) {
    ModalAnimatedTransitioningTypePresent,
    ModalAnimatedTransitioningTypeDismiss
};

@interface BaseAnimator : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic) ModalAnimatedTransitioningType transitionType;

- (void)animatePresentingInContext:(id<UIViewControllerContextTransitioning>)transitionContext toVC:(UIViewController *)toVC fromVC:(UIViewController *)fromVC;
- (void)animateDismissingInContext:(id<UIViewControllerContextTransitioning>)transitionContext toVC:(UIViewController *)toVC fromVC:(UIViewController *)fromVC;

@end
