#import "Constant.h"

@class FacebookManager;
@protocol FacebookManagerDelegate <NSObject>
- (void)facebookManager:(FacebookManager *) facebookManager didLoginComplete:(BOOL)success;
@end

@interface FacebookManager : NSObject
@property (strong, nonatomic) FBSDKLoginManager* login;
@property (weak, nonatomic) id<FacebookManagerDelegate> delegate;

+ (FacebookManager*)sharedInstance;
- (void)loginAction:(UIViewController*)viewController;
- (void)logoutAction;
- (BOOL)isLoginned;

@end
