#import <sys/utsname.h>
#import <objc/runtime.h>
#import <mach/mach_time.h>
#import <ifaddrs.h>
#import <arpa/inet.h>

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

#import <AudioUnit/AudioUnit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreFoundation/CoreFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreData/CoreData.h>
#import <AudioToolbox/AudioServices.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>
#import <StoreKit/StoreKit.h>
#import <GLKit/GLKit.h>
#import <ImageIO/ImageIO.h>
#import <Accelerate/Accelerate.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import <MessageUI/MessageUI.h>
#import "Singleton.h"
#import "ApiManager.h"
#import "NSDate+Utilities.h"
#import "NSString+Helper.h"
#import "Flurry.h"
#import "Mobclick.h"
#import "SSKeychain.h"
#import "Reachability.h"
#import "MJExtension.h"
#import "BlocksKit+UIKit.h"
#import "ThemeManager.h"
#import "MBProgressHUD.h"
#import "UIViewController+Helper.h"
#import "NSData+Base64.h"
#import "DialogManager.h"
#import <GLKit/GLKit.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <CoreTelephony/CoreTelephonyDefines.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "NSString+Helper.h"
#import "ThemeManager.h"
#import "STTweetLabel.h"
#import "UIDevice-Hardware.h"
#import "PublishPostHandler.h"
#import "UIButton+Animation.h"
#import "UIView+Animator.h"
#import "DatabaseManager.h"
#import "MyLocaitionManager.h"
#import "LikesHandler.h"
#import "EventHandler.h"
#import "UIColor+Helper.h"
#import "FacebookManager.h"
#import <TwitterKit/TwitterKit.h>
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import <Social/Social.h>
#import "WXApiObject.h"
#import "WXApi.h"
#import "UILabel+Helper.h"
#import <Partytrack/Partytrack.h>
#import "OL113Tool.h"

#define CURRENT_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

#define LOCALIZE(identifier) NSLocalizedString(identifier,nil)


#define INTERNATIONAL_VERSION 1

//#define DEV_MODE 1
//#define SANDBOX_MODE 1
//#define TRACE_LOG 1
//#define INFO_LOG 1
//#define WARNING_LOG 1
//#define ERROR_LOG 1

#ifdef INTERNATIONAL_VERSION
    #define BUNDLE @"com.machipopo.story17"
    #define SHARED_URL @"17.media"
    #define LIVE_STREAM_TCP_SERVER_IP @"livebroadcast.17app.co"
    #define APP_ID @"988259048"
    #define BUCKET_URL @"http://cdn.17app.co/"
    #define SERVER_IP @"http://api-dsa.17app.co/"
    #define UPLOAD_IP @"http://api-dsa.17app.co/"
    #define WECHAT_APP_ID @"wx1ddcb15e0e6ca5d4"
    #define BRANCH_PLIST_KEY @"key_live_jnoGVUcr1JYKDipj8teqnmcmxti27Rjl"
    #define UMENG_KEY @"56066ecb67e58eb6a00034f4"
    #define FLURRY_API_KEY @"54X2WM6YC4XG737W86MB"
    #define PARTYTRACK_APP_ID 5258
    #define PARTYTRACK_API_KEY @"a5a887a2faf55557c227b53e46f708a4"
    #define OL113_APP_KEY @"98bb719812f3f7a40a4498e474297454"
#else
    #define BUNDLE @"cn.media17.17app"
    #define SHARED_URL @"share.media17.cn"
    #define LIVE_STREAM_TCP_SERVER_IP @"livebroadcast.media17.cn"
    #define APP_ID @"1059688912"
    #define BUCKET_URL @"http://cdn.media17.cn/"
    #define SERVER_IP @"http://api.media17.cn/"
    #define UPLOAD_IP @"http://api.media17.cn/"
    #define WECHAT_APP_ID @"wxc48d4e480a3d0c0e"
    #define BRANCH_PLIST_KEY @"key_live_efcQ942quLGyNXNePFJoKkdmszjAr2rX"
    #define UMENG_KEY @"566fd02b67e58ef84a00210f"
    #define FLURRY_API_KEY @"G46VQN77SBMN99S49P99"
    #define PARTYTRACK_APP_ID 5258
    #define PARTYTRACK_API_KEY @"a5a887a2faf55557c227b53e46f708a4"
    #define OL113_APP_KEY @"98bb719812f3f7a40a4498e474297454"
#endif




#define LIVE_CHUNK_CDN_URL @"http://livecdn.media17.cn/"

//#define SERVER_IP @"http://54.222.129.24:8001/"
//#define BUCKET_URL @"http://cdn.17app.co/"
//#define skyeye @"http://skyeye.17app.co:8000"
//#define SERVER_IP @"http://52.11.159.139:8000/"
//#define LIVE_STREAM_TCP_SERVER_IP @"52.11.159.139"

//#define WECHAT_APP_ID @"wx1ddcb15e0e6ca5d4"


#	ifdef ERROR_LOG
#       define ErrorLog(format, ...) NSLog(@"<Error> " format @" [" __FILE__ @":%i]", ##__VA_ARGS__, __LINE__)
#	else
#		define ErrorLog(format, ...)
#	endif

#	ifdef WARNING_LOG
#       define WarningLog(format, ...) NSLog(@"<Warning> " format @" [" __FILE__ @":%i]", ##__VA_ARGS__, __LINE__)
#       define ErrorLog(format, ...) NSLog(@"<Error> " format @" [" __FILE__ @":%i]", ##__VA_ARGS__, __LINE__)
#	else
#		define WarningLog(format, ...)
#	endif

#	ifdef INFO_LOG
#       define InfoLog(format, ...) NSLog(@"<Info> " format @" [" __FILE__ @":%i]", ##__VA_ARGS__, __LINE__)
#       define WarningLog(format, ...) NSLog(@"<Warning> " format @" [" __FILE__ @":%i]", ##__VA_ARGS__, __LINE__)
#       define ErrorLog(format, ...) NSLog(@"<Error> " format @" [" __FILE__ @":%i]", ##__VA_ARGS__, __LINE__)
#	else
#		define InfoLog(format, ...)
#	endif

#	ifdef TRACE_LOG
#		define DebugTrace(format, ...) NSLog(@"<Trace>: " format @" [" __FILE__ @":%i]", ##__VA_ARGS__, __LINE__)
#       define InfoLog(format, ...) NSLog(@"<Info> " format @" [" __FILE__ @":%i]", ##__VA_ARGS__, __LINE__)
#       define WarningLog(format, ...) NSLog(@"<Warning> " format @" [" __FILE__ @":%i]", ##__VA_ARGS__, __LINE__)
#       define ErrorLog(format, ...) NSLog(@"<Error> " format @" [" __FILE__ @":%i]", ##__VA_ARGS__, __LINE__)
#	else
#		define DebugTrace(format, ...)
#	endif

#   ifdef SANDBOX_MODE
#       define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#       define DebugTrace(format, ...) NSLog(@"<Trace>: " format @" [" __FILE__ @":%i]", ##__VA_ARGS__, __LINE__)
#       define InfoLog(format, ...) NSLog(@"<Info> " format @" [" __FILE__ @":%i]", ##__VA_ARGS__, __LINE__)
#       define WarningLog(format, ...) NSLog(@"<Warning> " format @" [" __FILE__ @":%i]", ##__VA_ARGS__, __LINE__)
#       define ErrorLog(format, ...) NSLog(@"<Error> " format @" [" __FILE__ @":%i]", ##__VA_ARGS__, __LINE__)
#   else
#       define DLog(...)
#   endif

#define NSLOG DLog(@"%@:%@",[self class],NSStringFromSelector(_cmd));

/* Admin Level */
#define NORMAL_ADMIN 1
#define SUPER_ADMIN 2
#define PRIMO_ADMIN 3

//#define SUPER_ADMIN 4
//#define PRIMO_ADMIN 5

#define QQ_APP_ID @"1104946300"
#define QQ_APP_KEY @"Z0GjhA8rRqadZY68"

//#define WECHAT_APP_ID @"wxc48d4e480a3d0c0e"
//#define WECHAT_APP_KEY @"d4624c36b6795d1d99dcf0547af5443d"


#define LOGIN_MODE @"LOGIN_MODE"
#define MSG_LOGIN_MODE @"MSG_LOGIN_MODE"
#define WECHAT_LOGIN_MODE @"WECHAT_LOGIN_MODE"
#define WEIBO_LOGIN_MODE @"WEIBO_LOGIN_MODE"
#define QQ_LOGIN_MODE @"QQ_LOGIN_MODE"
#define FACEBOOK_LOGIN_MODE @"FACEBOOK_LOGIN_MODE"

#define WECHAT_APP_KEY @"d4624c36b6795d1d99dcf0547af5443d"
#define WEIBO_APP_ID @"3776632650"
#define WEIBO_APP_KEY @"a02f0e655a80fc467ebf35864cacc8aa"
#define WEIBO_REDIRECT_URL @"http://17app.co"

#define FACEBOOK_APP_ID @"898257960220808"

#define PUBLISH_POST_NOTIFICATION @"PUBLISH_POST_NOTIFICATION"
#define REFRESH_FEEDS @"REFRESH_FEEDS"

#define SocialLoginNotification @"SocialLoginNotification"
#define LoginNotification @"LoginNotification"
#define SignUpNotification @"SignUpNotification"

#define MAX_TRANSFER_TIMEOUT 10
#define MAX_FOLLOWER_COUNT 5000
#define MAX_IPS_COUNT 3

#define S3_FILE_URLSTRING(fileName) [BUCKET_URL stringByAppendingString:[NSString stringWithFormat:@"%@", fileName]]
#define S3_THUMBNAIL_FILE_URLSTRING(fileName) [BUCKET_URL stringByAppendingString:[NSString stringWithFormat:@"THUMBNAIL_%@", fileName]]
#define S3_FILE_URL(fileName) [NSURL URLWithString:S3_FILE_URLSTRING(fileName)]
#define S3_THUMB_IMAGE_URL(fileName) [NSURL URLWithString:S3_THUMBNAIL_FILE_URLSTRING(fileName)]

#define NAV(vc) [[UINavigationController alloc] initWithRootViewController:vc]
#define EQUAL_STRING(str1,str2) [str1 isEqualToString:str2]

#define ENTER_FOREGROUND_NOTIFICATION @"ENTER_FOREGROUND_NOTIFICATION"
#define END_LIVE_PREVIEW_NOTIFICATION @"END_LIVE_PREVIEW_NOTIFICATION"

#define PaypalSign_IP @"https://www.paypal.com/signup/account"
#define AlipaySign_IP @"https://www.alipay.com/user/reg_select.htm"

#define LIVE_STREAM_UDP_SERVER_IP @"liveudp.17app.co"
#define BLAB_STREAM_SERVER_IP @"52.89.78.83"


#define INSTAGRAM_CLIENT_ID @"8a3f7bbecb5b405c87c2ae846f58bb6d"
#define INSTAGRAM_REDIRECT_URI @"http://api.media17app.com"

#define APP_SHARE_URL @"http://goo.gl/G5L0Z4"

#define IP_COUNTRY @"IP_COUNTRY"
#define CURRENT_CITY @"CURRENT_CITY"
#define IN_SANDBOX @"IN_SANDBOX"
#define IS_FESTIVAL @"IS_FESTIVAL"

#define STRINGIZE(x) #x
#define STRINGIZE2(x) STRINGIZE(x)
#define SHADER_STRING(text) @ STRINGIZE2(text)

#define RANDOM_FLOAT(MIN, MAX) ((arc4random()%RAND_MAX)/(RAND_MAX*1.0))*(MAX-MIN)+MIN

#define MAX_LIVE_COMMENT 100

/* Load Cached Image Methods */
#define LOAD_FROM_BUNDLE 1000
#define LOAD_FROM_DOCUMENTS 2000
#define IMAGE_FROM_BUNDLE(fileName) [SINGLETON loadCachedImage:fileName withMethod:LOAD_FROM_BUNDLE]
#define IMAGE_FROM_DOC(fileName) [SINGLETON loadCachedImage:fileName withMethod:LOAD_FROM_DOCUMENTS]
#define RESIZABLE_IMAGE_FROM_BUNDLE(fileName, insets) [SINGLETON loadCachedImage:fileName withEdgeInset:insets withMethod:LOAD_FROM_BUNDLE]
#define RESIZABLE_IMAGE_FROM_DOC(fileName, insets) [SINGLETON loadCachedImage:fileName withEdgeInset:insets withMethod:LOAD_FROM_DOCUMENTS]

#define NETWORK_STATUS [[Reachability reachabilityForInternetConnection] currentReachabilityStatus]
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define ON_HOTSPOT  [[UIApplication sharedApplication] statusBarFrame].size.height==40? YES:NO
#define CURRENT_DEVICE_ORIENTATION [[UIDevice currentDevice]orientation]
#define BUNDLE_ID [[NSBundle mainBundle] bundleIdentifier]

#define MY_USER_ID [DEFAULTS objectForKey: USER_ID]
#define MY_ACCESS_TOKEN [DEFAULTS objectForKey: ACCESS_TOKEN]

#define GET_DEFAULT(KEY) [[[Singleton sharedInstance] defaults] objectForKey:KEY]

/* System Info Keys */
#define LAST_PLAY_NOTIFICATION_SOUND_TIME @"LAST_PLAY_NOTIFICATION_SOUND_TIME"
#define RETAIN_COUNT(VC) (CFGetRetainCount((__bridge CFTypeRef)VC))

/* View Constants */
#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height

#define TIME_BACKGROUND @"TIME_BACKGROUND"
#define TIME_FOREGROUND @"TIME_FOREGROUND"

#define NAVI_BAR_HEIGHT 44.0
#define TOOL_BAR_HEIGHT 44.0
#define TAB_BAR_HEIGHT 49.0
#define HOT_SPOT_HEIGHT [[UIApplication sharedApplication] statusBarFrame].size.height>=40?20:0
#define STATUS_BAR_HEIGHT [UIApplication sharedApplication].statusBarFrame.size.height
#define KEYBOARD_HEIGHT (SCREEN_HEIGHT==736?226.0:216.0)  // iphone keyboard size

/* Network Status */
#define IS_USING_4G_NETWORK [[[CTTelephonyNetworkInfo new] currentRadioAccessTechnology] isEqualToString:CTRadioAccessTechnologyLTE]
#define IS_USING_WIFI_NETWORK [[Reachability reachabilityForInternetConnection] currentReachabilityStatus]==ReachableViaWiFi
#define IS_NETWORK_AVAILABLE [[Reachability reachabilityForInternetConnection] currentReachabilityStatus]!=NotReachable

/* UI Related */
#define GridHMargin 2
#define GridImageWidth (SCREEN_WIDTH-4*GridHMargin)/3

/* UI Size Related */
#define FOLLOW_BTN_WIDTH 70
#define FOLLOW_BTN_HEIGHT 30

#define VIEW_SIZE(SIZE) CGSizeMake(ceil(SIZE.width), ceil(SIZE.height))
#define VIEW_WIDTH(WIDTH) ceil(WIDTH)
#define VIEW_HEIGHT(HEIGHT) ceil(HEIGHT)

/* Register Parameter */
#define USERNAME_MIN 4
#define USERNAME_MAX 20
#define PASSWORD_MIN 5
#define PASSWORD_MAX 20
#define AGE_MIN 12
#define AGE_MAX 80
#define ID_CARD_LENGTH 20
#define PAYMENT_NAME_LENFTH 50

// Notification View Mode
#define SELF_NOTIF 0
#define FOLLOWER_NOTIF 1
#define SYSTEM_NOTIF 2

/* Update Counter */
#define UPDATE_FEED_TIME @"UPDATE_FEED_TIME"
#define UPDATE_SEARCH_TIME @"UPDATE_SEARCH_TIME"
#define UPDATE_NOTIF_TIME @"UPDATE_NOTIF_TIME"
#define UPDATE_GIFT_MANAGE_LOG_TIME @"UPDATE_GIFT_MANAGE_LOG_TIME"
#define UPDATE_GIFT_BOARD_LOG_TIME @"UPDATE_GIFT_BOARD_LOG_TIME"

#define UPDATE_USER_PROFILE @"UPDATE_USER_PROFILE"

#define SYSTEM_FONT_WITH_SIZE(SIZE) [UIFont systemFontOfSize:SIZE]
#define BOLD_FONT_WITH_SIZE(SIZE) [UIFont boldSystemFontOfSize:SIZE]

// Upload Callback Codes
#define UPLOAD_FAIL 100
#define UPLOAD_SUCCESS 200
#define UPLOAD_PROGRESS 300
#define MULTI_UPLOAD_FAIL 400
#define MULTI_UPLOAD_SUCCESS 500
#define PROGRESSING 600

// Search View Status
#define PHOTO_LEFT 0
#define PHOTO_RIGHT 1

#define SEARCH_LEFT 0 
#define SEARCH_RIGHT 1

// Notification View Mode
typedef NS_ENUM(NSInteger, NotificationViewMode) {
    NotificationViewModeSelf = 0,
    NotificationViewModeFollower,
    NotificationViewModeSystem
};

// Revenue View Mode
#define WEEKLY_REVENUE 0
#define MONTHLY_REVENUE 1
#define YEARLY_REVENUE 2

/* User List View Mode */
typedef NS_ENUM(NSInteger, UserListViewMode) {
    UserListViewModeFollower = 0,
    UserListViewModeFollowing,
    UserListViewModePostLiker,
    UserListViewModeRequest,
    UserListViewModeUserLiker,
    UserListViewModeBlockedUser
};

#define FOLLOWER_MODE 100
#define FOLLOWING_MODE 200
#define LIKER_MODE 300
#define REQUEST_MODE 500
#define USER_LIKER_MODE 400


#define UPLOAD_IMAGE_TASK_EMPTY 0
//#define UPLOAD_IMAGE_FAIL 100
//#define UPLOAD_IMAGE_SUCCESS 200
//#define UPLOAD_IMAGE_PROGRSSING 300

// Settings View Mode
#define WITH_ARROW 100
#define WITH_SWITCH 200
#define WITH_NONE 300
#define IS_SELECTED 400
#define WITH_SUBTITLE 500
//Gift Download complete
#define GIFT_DOWNLOAD_COMPLETE @"Gift_DOWNLOAD_COMPLETE"


// Notification Cell Type
#define POST_LIKE @"postLike"
#define POST_COMMENT @"postComment"
#define REPOST @"repost"
#define POST_TAG @"postTag"
#define COMMENT_TAG @"commentTag"
#define NEW_FRIEND_JOIN_FB @"newFBFriendJoin"
#define NEW_FRIEND_JOIN_CONTACT @"newContactsFriendJoin"
#define NEW_FRIEND_FIRST_POST @"newFriendFirstPost"
#define NEW_POST @"newPost"
#define NEW_FOLLOW_TYPE @"follow"

#define QQ_ID @"QQ_ID"
#define WECHAT_USER_ID @"WECHAT_USER_ID"
#define WECHAT_TOKEN @"WECHAT_TOKEN"
#define QQ_TOKEN @"QQ_TOKEN"

#define WEIBO_LOGIN_SUCCESS_NOTIFICATION @"WEIBO_LOGIN_SUCCESS_NOTIFICATION"
#define WEIBO_LOGIN_FAIL_NOTIFICATION @"WEIBO_LOGIN_FAIL_NOTIFICATION"
#define WEIBO_ACCESSTOKEN @"WEIBO_ACCESSTOKEN"
#define WEIBO_USER_ID @"WEIBO_USER_ID"

#define FOLLOW_IN_NOTIFYCELL @"FOLLOW_IN_NOTIFYCELL"

#define NEW_SYSTEM_NOTIF @"NEW_SYSTEM_NOTIF"
#define NEW_COMMENT_TAG @"NEW_COMMENT_TAG"
#define NEW_POST_TAG @"NEW_POST_TAG"
#define NEW_COMMENT @"NEW_COMMENT"
#define NEW_FOLLOW @"NEW_FOLLOW"
#define NEW_LIKE @"NEW_LIKE"
#define NEW_LIVE_STREAM @"NEW_LIVE_STREAM"
#define NEW_LIVE_RESTREAM @"NEW_LIVE_RESTREAM"
#define NEW_FRIEND_REQUEST @"NEW_FRIEND_REQUEST"

#define NEW_FRIEND_JOIN_FROM_FB @"NEW_FRIEND_JOIN_FROM_FB"
#define NEW_FRIEND_JOIN_FROM_CONTACTS @"NEW_FRIEND_JOIN_FROM_CONTACTS"
#define NEW_FRIEND_JOIN_FROM_TWITTER @"NEW_FRIEND_JOIN_FROM_TWITTER"
#define NEW_FRIEND_JOIN_FROM_IG @"NEW_FRIEND_JOIN_FROM_IG"
#define NEW_FRIEND_JOIN_FROM_WECHAT @"NEW_FRIEND_JOIN_FROM_WECHAT"
#define NEW_FRIEND_JOIN_FROM_WEIBO @"NEW_FRIEND_JOIN_FROM_WEIBO"
#define NEW_FRIEND_JOIN_FROM_QQ @"NEW_FRIEND_JOIN_FROM_QQ"


#define PUSH_OFF @"off"
#define PUSH_FOLLOWER_ONLT @"followerOnly"
#define PUSH_EVERYONE @"everyone"

#define FOLLOW_USER_NOTIFICATION  @"FOLLOW_USER_NOTIFICATION"

#define RELOAD_SELF_DATA  @"RELOAD_SELF_DATA"
#define RELOAD_SELF_INFO  @"RELOAD_SELF_INFO"
#define REMOVE_SELF_POST @"REMOVE_SELF_POST"

#define IS_IPHONE_6_PLUS  SCREEN_HEIGHT==736?YES:NO
#define IS_IPHONE_6  SCREEN_WIDTH==375?YES:NO
#define IS_IPHONE_4  (SCREEN_HEIGHT<=480?YES:NO)

#define NOTIF_FROM_BACKGROUND [GET_DEFAULT(TIME_FOREGROUND) intValue]<[GET_DEFAULT(TIME_BACKGROUND) intValue]

/* Colors */
#define TIFFINY_BLUE_COLOR [UIColor colorWithRed:132.0/255.0 green:217.0/255.0 blue:213.0/255.0 alpha:1.0]
#define MAIN_COLOR [UIColor blackColor]
#define ALERT_TEXT_COLOR [UIColor blackColor]

#define DARK_MAIN_COLOR [UIColor colorWithRed:110.0/255.0 green:182.0/255.0 blue:180.0/255.0 alpha:1.0]
#define LIGHT_MAIN_COLOR [UIColor colorWithRed:250.0/255.0 green:254.0/255.0 blue:255.0/255.0 alpha:1.0]
#define ALPHA_MAIN_COLOR [UIColor colorWithRed:132.0/255.0 green:217.0/255.0 blue:213.0/255.0 alpha:0.7]
#define LIGHT_BG_COLOR [UIColor colorWithRed:250.0/255.0 green:250.0/255.0 blue:250.0/255.0 alpha:1.0]
#define DARK_BG_COLOR [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0]

#define GRAY_COLOR [UIColor colorWithRed:133.0/255.0 green:133.0/255.0 blue:133.0/255.0 alpha:1.0]
#define DARK_GRAY_COLOR [UIColor colorWithRed:86/255.0 green:86/255.0 blue:86/255.0 alpha:1.0]
#define LIGHT_GRAY_COLOR [UIColor colorWithRed:178.0/255.0 green:178.0/255.0 blue:178.0/255.0 alpha:1.0]
#define LINE_GRAY_COLOR [UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1.0]
#define PINK_COLOR [UIColor colorWithRed:255.0/255.0 green:175.0/255.0 blue:187.0/255.0 alpha:1.0]
#define DARK_GRAY_COLOR_TWO [UIColor colorWithRed:35.0/255.0 green:24.0/255.0 blue:21.0/255.0 alpha:1.0]
#define LIGHT_GRAY_COLOR_TWO [UIColor colorWithRed:113.0/255.0 green:113.0/255.0 blue:113.0/255.0 alpha:1.0]



#define ORANGE_COLOR [UIColor colorWithRed:251.0/255.0 green:176.0/255.0 blue:59.0/255.0 alpha:1.0]
#define CAMERA_GRAY_BG_COLOR [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0]
#define WHITE_COLOR [UIColor whiteColor]
#define BLACK_COLOR [UIColor blackColor]
#define RED_COLOR [UIColor redColor]
#define WARN_COLOR [UIColor colorWithRed:236.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0]
#define WARN_TEXT_COLOR [UIColor colorWithRed:118.0/255.0 green:228.0/255.0 blue:226.0/255.0 alpha:1.0]


#define COMMENT_TEXT_COLOR1 [UIColor colorWithRed:182.0/255.0 green:0.0/255.0 blue:150.0/255.0 alpha:1.0]
#define COMMENT_TEXT_COLOR2 [UIColor colorWithRed:14.0/255.0 green:77.0/255.0 blue:178.0/255.0 alpha:1.0]
#define COMMENT_TEXT_COLOR3 [UIColor colorWithRed:255.0/255.0 green:130.0/255.0 blue:190.0/255.0 alpha:1.0]
#define COMMENT_TEXT_COLOR4 [UIColor colorWithRed:238.0/255.0 green:125.0/255.0 blue:53.0/255.0 alpha:1.0]
#define COMMENT_TEXT_COLOR5 [UIColor colorWithRed:0.0/255.0 green:182.0/255.0 blue:191.0/255.0 alpha:1.0]
#define COMMENT_TEXT_COLOR6 [UIColor colorWithRed:221.0/255.0 green:73.0/255.0 blue:73.0/255.0 alpha:1.0]
#define COMMENT_TEXT_COLOR7 [UIColor colorWithRed:117.0/255.0 green:192.0/255.0 blue:82.0/255.0 alpha:1.0]

#define DIALOG_TITLE_TEXT_COLOR [UIColor colorWithRed:135.0/255.0 green:135.0/255.0 blue:135.0/255.0 alpha:1.0]
#define DIALOG_CONTENT_TEXT_COLOR [UIColor colorWithRed:179.0/255.0 green:179.0/255.0 blue:179.0/255.0 alpha:1.0]

#define CAM_BG_COLOR [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0]
#define TRANS_CAM_BG_COLOR [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.8]

/* Global Singletons */
#define SINGLETON [Singleton sharedInstance]
#define FACEBOOK_MANAGER [FacebookManager sharedInstance]

#define NOTIFICATION_CENTER [NSNotificationCenter defaultCenter]
#define DEFAULTS [[Singleton sharedInstance] defaults]
#define FILE_MANAGER [NSFileManager defaultManager]
#define API_MANAGER [[Singleton sharedInstance] apiManager]
#define REVENUE_MANAGER [[Singleton sharedInstance] revenueDataManager]
#define CONNECTION_MANAGER [[Singleton sharedInstance] connectionManager]
#define DIALOG_MANAGER [DialogManager sharedInstance]
#define PUBLISH_POST_HANDLER [PublishPostHandler sharedInstance]
#define DB_MANAGER [DatabaseManager sharedInstance]
#define LOCATION_MANAGER [MyLocaitionManager sharedInstance]
#define LIKE_HANDLER [LikesHandler sharedInstance]
#define EVENT_HANDLER [EventHandler sharedInstance]

#define kGOOGLE_API_KEY @"AIzaSyAKlHp4D2iNadQ8B-yjzCnLU_3s9zsSBdU"
#define SearchRadius 500

/* Handy Creator Macro */
#define CREATE_MUTABLE_DICTIONARY [[NSMutableDictionary alloc] init]
#define CREATE_MUTABLE_ARRAY [[NSMutableArray alloc] init]
#define CREATE_MUTABLE_STRING [[NSMutableString alloc] init]
#define CREATE_MUTABLE_DATA [[NSMutableData alloc] init]
#define CREATE_MUTABLE_SET [[NSMutableSet alloc] init]
#define CREATE_MUTABLE_ORDERED_SET [[NSMutableOrderedSet alloc] init]

#define MAIN_QUEUE dispatch_get_main_queue()
#define GLOBAL_QUEUE dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)


#define TO_JSON(DICT_OR_ARRAY) [SINGLETON modelToJsonString:DICT_OR_ARRAY]
#define JSONKIT_TO_JSON(DICT_OR_ARRAY) [DICT_OR_ARRAY JSONStringWithOptions:JKSerializeOptionEscapeUnicode error:nil]

#define NETWORK_TIMEOUT 10.0

#define FORMAT_STRING(src, params...) [NSString stringWithFormat:src, params]
#define FLOAT_TO_NUMBER(x) [NSNumber numberWithFloat:x]
#define INT_TO_NUMBER(x) [NSNumber numberWithInt:x]
#define FLOAT_TO_STRING(x) [NSString stringWithFormat:@"%f", x]
#define INT_TO_STRING(x) [NSString stringWithFormat:@"%d", x]
#define DOUBLE_TO_STRING(x) [NSString stringWithFormat:@"%f", x]
#define LONGLONG_TO_STRING(x) [NSString stringWithFormat:@"%lld", x]
#define NSINTEGER_TO_STRING(x) [NSString stringWithFormat: @"%ld",x]
#define ARC4RANDOM_MAX 0x100000000
#define RANDOM_FLOAT(MIN, MAX) ((arc4random()%RAND_MAX)/(RAND_MAX*1.0))*(MAX-MIN)+MIN
#define RANDOM_INT(MIN ,MAX) (arc4random()%MAX+MIN)

/* Handy File Locator and File Exist Checker */
#define DOCUMENTS_DIRECTORY [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]

/* Move data to cache dir  */
#define CACHE_DIRECTORY [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)objectAtIndex:0]
#define GET_LOCAL_FILE_PATH(FILE_NAME) [CACHE_DIRECTORY stringByAppendingPathComponent:FILE_NAME]

#define GET_BUNDLE_FILE_PATH(FILE_NAME) [[NSBundle mainBundle] pathForResource:FILE_NAME ofType:@""]
#define FILE_EXIST(FILE_NAME) [FILE_MANAGER fileExistsAtPath:GET_LOCAL_FILE_PATH(FILE_NAME)]
#define SAVE_DATA_TO_DISK(DATA,FILE_PATH) [DATA writeToFile:[DOCUMENTS_DIRECTORY stringByAppendingPathComponent:FILE_PATH] atomically:YES];
#define DELETE_FILE(FILE_PATH) [[NSFileManager defaultManager] removeItemAtPath:FILE_PATH error:nil]
//#define GET_CACHE_FILE_PATH(FILE_NAME) [CACHE_DIRECTORY stringByAppendingPathComponent:FILE_NAME]


#define GET_IMAGES_ARRAY(IMAGE_NAME) @[IMAGE_NAME, [NSString stringWithFormat:@"THUMBNAIL_%@", IMAGE_NAME]]
#define GET_VIDEO_ARRAY(IMAGE_NAME,VIDEO_NAME) @[IMAGE_NAME, [NSString stringWithFormat:@"THUMBNAIL_%@", IMAGE_NAME],VIDEO_NAME]

#define CURRENT_TIMESTAMP (int) ([[NSDate date] timeIntervalSince1970])
#define UPDATE_INTERVAL 600

/* Image Sizes */
#define NORMAL_IMAGE_COMPRESSION_TARGET_SIZE 1166400.0f
#define THUMBNAIL_IMAGE_COMPRESSION_TARGET_SIZE 409600.0f

/* Device Version Checker */
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

/* gift keys*/
#define GIFTMODULESTATE @"GIFTMODULESTATE"
#define GIFT_ZIP @"GIFT_ZIP"
#define NOTLIVESTREAMING @"NOTLIVESTREAMING"
/* User Info Keys */
#define USER_ID @"USER_ID"
#define SESSION_ID @"SESSION_ID"
#define APP_OPEN_TIMES @"APP_OPEN_TIMES"
#define MY_POINT @"MY_POINT"
#define USER_IP @"USER_IP"
#define IS_ON_LIVESTREAMING @"IS_ON_LIVESTREAMING"
#define IS_FIRST_HELPSHIFT @"IS_FIRST_HELPSHIFT"
#define MY_USER_IP @"MY_USER_IP"

#define USER_OPEN_ID @"USER_OPEN_ID"
#define FACEBOOK_ID @"FACEBOOK_ID"
#define FACEBOOK_TOKEN @"FACEBOOK_TOKEN"
#define USER_NAME @"USER_NAME"
#define TOKEN @"TOKEN"
#define DEVICE_ID @"DEVICE_ID"
#define ACCESS_TOKEN @"ACCESS_TOKEN"
#define SAVE_PHOTO @"SAVE_PHOTO"
#define IS_FREEZED @"IS_FREEZED"
#define PASSWORD @"PASSWORD"
#define AGE @"AGE"
#define PICTURE @"PICTURE"
#define CACHE_PICTURE @"CACHE_PICTURE"
#define PRIVACY_MODE @"PRIVACY_MODE"
#define FOLLOW_PRIVACY_MODE @"FOLLOW_PRIVACY_MODE"
#define BIO @"BIO"
#define WEBSITE @"WEBSITE"
#define IS_VERIFIED @"IS_VERIFIED"
#define FOLLOWER_COUNT @"FOLLOWER_COUNT"
#define FOLLOWING_COUNT @"FOLLOWING_COUNT"
#define POST_COUNT @"POST_COUNT"
#define REPOST_COUNT @"REPOST_COUNT"
#define LIKED_POST_COUNT @"LIKED_POST_COUNT"
#define EMAIL @"EMAIL"
#define IS_ADMIN @"IS_ADMIN"
#define ALIPAYPHONE @"ALIPAYPHONE"
#define PAYPALEMAIL @"PAYPALEMAIL"
#define VERIFIEDPHONE @"VERIFIEDPHONENUMBER"
#define OPENID_REJECTED @"OPENID_REJECTED"

#define CURRENCY_RATE @"CURRENCY_RATE"
#define CURRENCY_RATE_SETTED @"CURRENCY_RATE_SETTED"
#define PUSH_LIKE @"PUSH_LIKE"
#define PUSH_COMMENT @"PUSH_COMMENT"
#define PUSH_FOLLOW @"PUSH_FOLLOW"
#define PUSH_TAG @"PUSH_TAG"
#define PUSH_FRIEND_JOIN @"PUSH_FRIEND_JOIN"
#define PUSH_FRIEND_REQUEST @"PUSH_FRIEND_REQUEST"
#define PUSH_FRIEND_FIRST_POST @"PUSH_FRIEND_FIRST_POST"
#define PUSH_FRIEND_REQUEST @"PUSH_FRIEND_REQUEST"
#define PUSH_SYSTEM_NOTIF @"PUSH_SYSTEM_NOTIF"
#define PUSH_FOLLOW_REQUEST @"PUSH_FOLLOW_REQUEST"
#define PUSH_LIVESTREAM_NOTIF @"PUSH_LIVESTREAM_NOTIF"
#define PUSH_RESTREAM_NOTIF @"PUSH_RESTREAM_NOTIF"
#define PUSH_SCHEDULE_NOTIF @"PUSH_SCHEDULE_NOTIF"
#define UPDATE_APPLINK @"UPDATE_APPLINK"

#define NOTIF_BADGE @"NOTIF_BADGE"
#define SYSTEM_NOTIF_BADGE @"SYSTEM_NOTIF_BADGE"
#define MESSGAE_BADGE @"MESSGAE_BADGE"
#define REQUEST_BADGE @"REQUEST_BADGE"
#define COMMENT_BADGE @"COMMENT_BADGE"
#define LIKE_BADGE @"LIKE_BADGE"

#define BANK_NAME @"BANK_NAME"
#define BANK_ADDRESS @"BANK_ADDRESS"
#define ACCOUNT_NUMBER @"ACCOUNT_NUMBER"
#define BENEFICIARY_NAME @"BENEFICIARY_NAME"
#define SWIFT_CODE @"SWIFT_CODE"
#define ROUNTING_NUMBER @"ROUNTING_NUMBER"
#define BANK_COUNTRY @"BANK_COUNTRY"
#define ID_CARD_NUMBER @"ID_CARD_NUMBER"
#define ID_CARD_NAME @"ID_CARD_NAME"
#define BANK_CODE @"BANK_CODE"
#define BANK_BRANCHCODE @"BANK_BRANCHCODE"
#define BRANCH_NAME @"BRANCH_NAME"
#define ID_CARD_FRONT_PICTURE @"ID_CARD_FRONT_PICTURE"
#define ID_CAR_BACK_PICTURE @"ID_CAR_BACK_PICTURE"
#define PASSBOOK_PICTURE @"PASSBOOK_PICTURE"
#define BANK_ACCOUNT_VERIFIED @"BANK_ACCOUNT_VERIFIED"
#define TOTAL_REVENUE_EARNED @"TOTAL_REVENUE_EARNED"
#define TOTAL_REVENUE_PAYED @"TOTAL_REVENUE_PAYED"
#define RECEIVED_LIKE_COUNT @"RECEIVED_LIKE_COUNT"
#define OPENID_MODIFYTIME @"OPENID_MODIFYTIME"
#define REMOVE_CACHED_INFO @"REMOVE_CACHED_INFO"
//#define USER_PHONE_NUMBER @"phoneNumber"

// register related keys
#define COUNTRY_CODE @"COUNTRY_CODE" // in 17app, this means PHONE_CODE
#define PHONE_TWO_DIGIT @"PHONE_TWO_DIGIT" // in 17app, this means PHONE_CODE
#define TOTAL_PHONE_NUM @"TOTAL_PHONE_NUM"
#define PHONE_NUM @"PHONE_NUM"
#define VERIFIED_CODE @"VERIFIED_CODE"
#define GENDER @"GENDER"
#define LAST_VERIFICATION_TIME @"LAST_VERIFICATION_TIME"

/* Find Friend Related */
#define CONTACT_LAST_UPDATE @"CONTACT_LAST_UPDATE"
#define FACEBOOK_LAST_UPDATE @"FACEBOOK_LAST_UPDATE"

// Filter Keys
#define NORMAL_FILTER @"NORMAL_FILTER"
#define RICH_FILTER @"RICH_FILTER"
#define WARM_FILTER @"WARM_FILTER"
#define SOFT_FILTER @"SOFT_FILTER"
#define ROSE_FILTER @"ROSE_FILTER"
#define MORNING_FILTER @"MORNING_FILTER"
#define SUNSHINE_FILTER @"SUNSHINE_FILTER"
#define SUNSET_FILTER @"SUNSET_FILTER"
#define COOL_FILTER @"COOL_FILTER"
#define FREEZE_FILTER @"FREEZE_FILTER"
#define OCEAN_FILTER @"OCEAN_FILTER"
#define DREAM_FILTER @"DREAM_FILTER"
#define VIOLET_FILTER @"VIOLET_FILTER"
#define MELLOW_FILTER @"MELLOW_FILTER"
#define BLEAK_FILTER @"BLEAK_FILTER"
#define MEMORY_FILTER @"MEMORY_FILTER"
#define PURE_FILTER @"PURE_FILTER"
#define CALM_FILTER @"CALM_FILTER"
#define AUTUMN_FILTER @"AUTUMN_FILTER"
#define FANTASY_FILTER @"FANTASY_FILTER"
#define FREEDOM_FILTER @"FREEDOM_FILTER"
#define MILD_FILTER @"MILD_FILTER"
#define PRAIRIE_FILTER @"PRAIRIE_FILTER"
#define DEEP_FILTER @"DEEP_FILTER"
#define GLOW_FILTER @"GLOW_FILTER"
#define MEMOIR_FILTER @"MEMOIR_FILTER"
#define MIST_FILTER @"MIST_FILTER"
#define VIVID_FILTER @"VIVID_FILTER"
#define CHILL_FILTER @"CHILL_FILTER"
#define PINKY_FILTER @"PINKY_FILTER"
#define ADVENTURE_FILTER @"ADVENTURE_FILTER"

/* System Info Keys */
#define LAST_PLAY_NOTIFICATION_SOUND_TIME @"LAST_PLAY_NOTIFICATION_SOUND_TIME"
#define NETWORK_ERROR @"NETWORK_ERROR"

#define CAMERA_FLASH_MODE @"CAMERA_FLASH_MODE"
#define CAMERA_FLASH_MODE_ON @"CAMERA_FLASH_MODE_ON"
#define CAMERA_FLASH_MODE_OFF @"CAMERA_FLASH_MODE_OFF"
#define CAMERA_FLASH_MODE_AUTO @"CAMERA_FLASH_MODE_AUTO"

#define CAMERA_GRID_MODE @"CAMERA_GRID_MODE"
#define CAMERA_GRID_MODE_ON @"CAMERA_GRID_MODE_ON"
#define CAMERA_GRID_MODE_OFF @"CAMERA_GRID_MODE_OFF"

//#define ADMIN_DELETE_POST_REASON @[LOCALIZE(@"reject_post_detail_1"),LOCALIZE(@"reject_post_detail_2"),LOCALIZE(@"reject_post_detail_3"),LOCALIZE(@"reject_post_detail_4")]
#define ADMIN_DELETE_POST_REASON @[@"reject_post_detail_1",@"reject_post_detail_2",@"reject_post_detail_3",@"reject_post_detail_4"]

#define REPORTED_REASON @[LOCALIZE(@"i_dont_like"),LOCALIZE(@"garbage_message"),LOCALIZE(@"bad_to_others"),LOCALIZE(@"not_good_for")]
#define USER_REPORTED_REASON @[LOCALIZE(@"i_dont_like_user"),LOCALIZE(@"i_dont_like_user_search"),LOCALIZE(@"user_violate")]

#define REPORTED_COMMENT_REASON @[LOCALIZE(@"i_dont_like_comment"),LOCALIZE(@"this_comment_is_spam_scam"),LOCALIZE(@"puts_people_at_risk"),LOCALIZE(@"comment_should_not_be_in_17")]

/* Post Manager Kyes */
#define UPLOAD_FAILED_FILES @"UPLOAD_FAILED_FILES" // nsdefault json
#define DOWNLOAD_VIDEO_FILES @"DOWNLOAD_VIDEO_FILES"
#define BUFFER_VIDEO_UPLADED @"BUFFER_VIDEO_UPLADED"
#define POST_CAN_UPLOAD 100
#define POST_CANNOT_UPLOAD 200

//#define BACK_DERTAIL @[@[@"",@"[]"],@[@"",@"[]"],@[]@,@[]]
//#define BACK_DERTAIL @[@[@"",@"[]"],@[@"",@"[]"],@[]@,@[]]

/* LIVE STREAM */
#define LOCAL_PORT 10000
#define DEFAULT_UDP_TIMEOUT 10000
#define LIVE_STREAM_SERVER_PORT 10000

#define ABSOLUTE_TIME mach_absolute_time()

/* Live Streaming */
#define USE_LIBVPX 1

#define FRAME_WIDTH 640
#define FRAME_HEIGHT 360
#define LIVE_STREAM_FRAME_RATE 30

#define FULL_HD_LIVESTREAM_VIDEO_BIT_RATE 1536000
#define VERY_HIGH_LIVESTREAM_VIDEO_BIT_RATE 1024000
#define HIGH_LIVESTREAM_VIDEO_BIT_RATE 768000
#define MEDIUM_LIVESTREAM_VIDEO_BIT_RATE 512000
//#define LOW_LIVESTREAM_VIDEO_BIT_RATE 64000
#define LOW_LIVESTREAM_VIDEO_BIT_RATE 128000
#define VERY_LOW_LIVESTREAM_VIDEO_BIT_RATE 256000



#define FULL_HD_LIVESTREAM_AUDIO_BIT_RATE 96000
#define HIGH_LIVESTREAM_AUDIO_BIT_RATE 64000
#define MEDIUM_LIVESTREAM_AUDIO_BIT_RATE 48000
#define LOW_LIVESTREAM_AUDIO_BIT_RATE 16000

#define LIVESTREAM_QUALITY @"LIVESTREAM_QUALITY"
//#define LIVESTREAM_QUALITY_FULL_HD 1
#define LIVESTREAM_QUALITY_VERY_HIGH 1
#define LIVESTREAM_QUALITY_HIGH 2
#define LIVESTREAM_QUALITY_MEDIUM 3
#define LIVESTREAM_QUALITY_LOW 4
#define LIVESTREAM_QUALITY_VERY_LOW 5

/* Movie */
#define MOVIE_WIDTH 480
#define MOVIE_HEIGHT 480
#define MOVIE_FRAME_RATE 30
#define MOVIE_GOP_SIZE 15
#define MOVIE_VIDEO_BIT_RATE 1024000
#define MOVIE_AUDIO_BIT_RATE 96000
#define MOVIE_AUDIO_SAMPLE_RATE 44100
#define MOVIE_MIN_RECORD_TIME 3.0
#define MOVIE_MAX_RECORD_TIME 15.0
#define MOVIE_FILE_NAME @"movie.mp4"

#define NEED_AUTO_FOCUS_THRESHOLD 0.05f
#define START_AUTO_FOCUS_THRESHOLD 0.01f

/* Error Code */
#define DECODE_SUCCESS 0
#define DECODE_FAIL -1

/* Packet */
#define AUDIO_PACKET_DATA_LENGTH 50
#define PACKET_HEADER_LENGTH 1
#define ROTATION_0 0
#define ROTATION_90 1
#define ROTATION_180 2
#define ROTATION_270 3