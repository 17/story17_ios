#import "Constant.h"
#import "DialogManager.h"
#import "BlocksKit.h"
#import "BlocksKit+UIKit.h"
#import "UIGestureRecognizer+BlocksKit.h"
#import "UIImageView+AFNetworking.h"
#import "Branch.h"
#import "ProductObject.h"

static NSTimeInterval const kDialogFadeDuration = 0.3f;

@implementation DialogManager

@synthesize dialogBgView;
@synthesize pickerView;
@synthesize pickerMode;
@synthesize pickerItems;
@synthesize loadingBgView;
@synthesize loadingView;
@synthesize pickerRightItems;

+(DialogManager*) sharedInstance
{
    static dispatch_once_t once;
    
    static DialogManager *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        _onlyOneTap=NO;
        _shareManager = [ShareManager new];
    }
    
    return self;
}

# pragma mark - Dialog
-(UIView*) getWindowView
{
    UIWindow *statusBarWindow = (UIWindow *)[UIApplication sharedApplication].keyWindow;
    return statusBarWindow;
}

-(UIView*) getStatusBarWindowView
{
    UIWindow *statusBarWindow = (UIWindow *)[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"];
    return statusBarWindow;
}

-(void) removeDialogImmediately
{
    if(dialogBgView!=nil) {
        [dialogBgView removeFromSuperview];
        dialogBgView = nil;
    }
}

-(void) removePopImmediately
{
    if(_popImageView!=nil) {
        [_popImageView.layer removeAllAnimations];
        [_popImageView removeFromSuperview];
        _popImageView = nil;
    }
}

# pragma mark - Loading View
-(void) showLoadingView
{
    [DIALOG_MANAGER hideLoadingView];
    
    dialogBgView = [[UIView alloc] initWithFrame:self.getStatusBarWindowView.frame];
    loadingBgView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"loading2")];
    loadingView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"loading1")];
    [self.getStatusBarWindowView addSubview:dialogBgView];
    [[self getStatusBarWindowView] addSubview:loadingBgView];
    [[self getStatusBarWindowView] addSubview:loadingView];

    loadingBgView.center = CGPointMake(self.getStatusBarWindowView.center.x, self.getStatusBarWindowView.center.y-20);
    loadingView.center = CGPointMake(self.getStatusBarWindowView.center.x, self.getStatusBarWindowView.center.y-20);
    
    // rotation
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 * 1 * 1.5];
    rotationAnimation.duration = 1.5;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 10000;
    [loadingView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    [UIView animateWithDuration:0.6 delay:kDialogFadeDuration options:(UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat) animations:^{
        loadingBgView.transform = CGAffineTransformMakeScale(0.85, 0.85);
    } completion:^(BOOL finished) {
        
    }];

    loadingBgView.alpha = 0.0;
    loadingView.alpha = 0.0;
    
    [UIView animateWithDuration:kDialogFadeDuration animations:^{
        loadingView.alpha = 1.0;
        loadingBgView.alpha = 1.0;
    } completion:^(BOOL finished) {
    
    }];
    
    [dialogBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [DIALOG_MANAGER hideLoadingView];
    }]];
}


-(void) showLoadingViewQicker
{
    [DIALOG_MANAGER hideLoadingView];
    
    dialogBgView = [[UIView alloc] initWithFrame:self.getStatusBarWindowView.frame];
    loadingBgView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"loading2")];
    loadingView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"loading1")];
    [self.getStatusBarWindowView addSubview:dialogBgView];
    [[self getStatusBarWindowView] addSubview:loadingBgView];
    [[self getStatusBarWindowView] addSubview:loadingView];
    
    loadingBgView.center = CGPointMake(self.getStatusBarWindowView.center.x, self.getStatusBarWindowView.center.y-20);
    loadingView.center = CGPointMake(self.getStatusBarWindowView.center.x, self.getStatusBarWindowView.center.y-20);
    
    // rotation
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 * 1 * 1.5];
    rotationAnimation.duration = 1.5;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 10000;
    [loadingView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    [UIView animateWithDuration:0.6 delay:0.01f options:(UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat) animations:^{
        loadingBgView.transform = CGAffineTransformMakeScale(0.85, 0.85);
    } completion:^(BOOL finished) {
        
    }];
    
    loadingBgView.alpha = 0.0;
    loadingView.alpha = 0.0;
    
    [UIView animateWithDuration:0.01f animations:^{
        loadingView.alpha = 1.0;
        loadingBgView.alpha = 1.0;
    } completion:^(BOOL finished) {
        
    }];
    
    [dialogBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [DIALOG_MANAGER hideLoadingView];
    }]];
}

-(void) hideLoadingView
{
    [self removeDialogImmediately];
    
    UIView* currentLoadingBgView = loadingBgView;
    UIView* currentLoadingView = loadingView;

    if(currentLoadingView) {
        [UIView animateWithDuration:kDialogFadeDuration animations:^{
            currentLoadingView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [currentLoadingView removeFromSuperview];
        }];
    }
    
    if(currentLoadingBgView) {
        [UIView animateWithDuration:kDialogFadeDuration animations:^{
            currentLoadingBgView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [currentLoadingBgView removeFromSuperview];
        }];
    }

    loadingBgView = nil;
    loadingView = nil;
}

# pragma mark - Toast
- (void)showCompleteToast
{
    [self showHappyToastWithMessage:LOCALIZE(@"Completed!")];
}

- (void)showHappyToastWithMessage:(NSString*) message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[self getWindowView] animated:YES];
        hud.userInteractionEnabled=NO;
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"happy")];
        hud.animationType = MBProgressHUDAnimationFade;
        hud.detailsLabelText = message;
        hud.detailsLabelFont = BOLD_FONT_WITH_SIZE(18);
        [hud hide:YES afterDelay:1.0];
    });
}

- (void)showSadToastWithMessage:(NSString*) message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[self getWindowView] animated:YES];
        hud.userInteractionEnabled=NO;
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"sad")];
        hud.animationType = MBProgressHUDAnimationFade;
        hud.labelText = message;
        [hud hide:YES afterDelay:1.0];
    });
}

-(void) showNetworkFailToast
{
    [DIALOG_MANAGER hideLoadingView];
    [EVENT_HANDLER addEventTracking:@"NetWorkUnstable" withDict:nil];
    [EVENT_HANDLER addNetworkUnstableCount];
    dispatch_async(dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[self getWindowView] animated:YES];
        hud.userInteractionEnabled=NO;
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"sad")];
        hud.animationType = MBProgressHUDAnimationFade;
        hud.detailsLabelText = LOCALIZE(@"Network Unstable");
        hud.detailsLabelFont=BOLD_FONT_WITH_SIZE(18);
        [hud hide:YES afterDelay:1.0];
    });
}

# pragma mark - Picker
-(void) showPickerDialogWithItems:(NSArray *)items offset:(int)offset withCompletion:(void (^)(BOOL done, int selectedIndex)) callback
{
    [self removeDialogImmediately];
    
    self.itemPickerCallback = callback;
    pickerItems = items;
    pickerMode = PICKER_MODE_ITEM;
    [self showPickerDialog];
    
    [pickerView selectRow:offset inComponent:0 animated:YES];
}

-(void) showRangePickerDialogWithItems:(NSArray *)items rightItem:(NSArray*)rightItem lowOffset:(int)lowOffset highOffset:(int)highOffset withCompletion:(void (^)(BOOL done, int selectedLowIndex, int selectedHighIndex)) callback
{
    [self removeDialogImmediately];
    
    self.rangePickerCallback = callback;
    pickerItems = items;
    pickerRightItems = rightItem;
    pickerMode = PICKER_MODE_RANGE;
    
    [self showPickerDialog];
    
    [pickerView selectRow:lowOffset inComponent:0 animated:YES];
    [pickerView selectRow:highOffset inComponent:1 animated:YES];
}

-(void) showPickerDialog
{
    
    dialogBgView = [self getTopBgView];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.0];
    
    pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake((dialogBgView.frame.size.width-SCREEN_WIDTH)/2, SCREEN_HEIGHT-216, SCREEN_WIDTH, 216)];
    pickerView.backgroundColor = [UIColor whiteColor];
    pickerView.dataSource = self;
    pickerView.delegate = self;
    [dialogBgView addSubview:pickerView];
    [pickerView reloadAllComponents];
    
    // add tool bar
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake((dialogBgView.frame.size.width-SCREEN_WIDTH)/2, SCREEN_HEIGHT-216-44, SCREEN_WIDTH, 44)];
    toolBar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *startSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil] ;
    startSpace.width = 10.0;
    UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePickerDialogAction)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil] ;
    UIBarButtonItem *cancelButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelPickerDialogAction)];
    UIBarButtonItem *endSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil] ;
    endSpace.width = 10.0;
    
    [toolBar setItems:@[startSpace, cancelButtonItem, flexSpace, doneButtonItem, endSpace]];
    
    UIView* separatorLine = [[UIView alloc] initWithFrame:CGRectMake(0, 43.5, SCREEN_WIDTH, 0.5)];
    separatorLine.backgroundColor = [UIColor colorWithRed:125.0/255.0 green:115.0/255.0 blue:140.0/255.0 alpha:0.3];
    [toolBar addSubview:separatorLine];
    
    UILabel* labelAll=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, 30)];
    UILabel* labelTh=[[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2, 30)];
    [labelAll setText:LOCALIZE(@"THman")];
    [labelTh setText:LOCALIZE(@"Allman")];

    [labelAll setTextAlignment:NSTextAlignmentCenter];
    [labelAll sizeToFit];
    labelAll.center=CGPointMake(SCREEN_WIDTH/4+20, 43.5+15);
    [labelTh setTextAlignment:NSTextAlignmentCenter];
    [labelTh sizeToFit];

    labelTh.center=CGPointMake(SCREEN_WIDTH*3/4, 43.5+15);
    
    if(pickerMode == PICKER_MODE_RANGE){
        [toolBar addSubview:labelAll];
        [toolBar addSubview:labelTh];
    }
    
    [dialogBgView addSubview:toolBar];
    
    dialogBgView.center = CGPointMake(dialogBgView.frame.size.width/2, dialogBgView.frame.size.height/2+216);
    
    [UIView animateWithDuration:kDialogFadeDuration animations:^{
        dialogBgView.center = CGPointMake(dialogBgView.frame.size.width/2, dialogBgView.frame.size.height/2);
    }];
}

-(void) hidePickerDialog
{
    [UIView animateWithDuration:0.15 animations:^{
        dialogBgView.center = CGPointMake([self getWindowView].frame.size.width/2, [self getWindowView].frame.size.height/2+216);
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [dialogBgView removeFromSuperview];
        dialogBgView = nil;
        
        pickerItems = nil;
        pickerView = nil;
        self.itemPickerCallback = nil;
        self.rangePickerCallback = nil;
    });
}

-(void) donePickerDialogAction
{
    if(pickerMode==PICKER_MODE_ITEM) {
        self.itemPickerCallback(YES, (int) [pickerView selectedRowInComponent:0]);
    } else if(pickerMode==PICKER_MODE_RANGE) {
        self.rangePickerCallback(YES, (int) [pickerView selectedRowInComponent:0], (int) [pickerView selectedRowInComponent:1]);
    }
    
    [self hidePickerDialog];
}

-(void) cancelPickerDialogAction
{
    if(pickerMode==PICKER_MODE_ITEM) {
        self.itemPickerCallback(NO, 0);
    } else if(pickerMode==PICKER_MODE_RANGE) {
        self.rangePickerCallback(NO, 0, 0);
    }
    
    [self hidePickerDialog];
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if(pickerMode==PICKER_MODE_ITEM) {
        return 1;
    } else if(pickerMode==PICKER_MODE_RANGE) {
        return 2;
    }
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(pickerMode==PICKER_MODE_ITEM) {
        return pickerItems.count;
    } else if(pickerMode==PICKER_MODE_RANGE) {
        if(component ==0){
            return pickerItems.count;
        }else{
            return pickerRightItems.count;
        }
    }
    return pickerItems.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(pickerMode==PICKER_MODE_ITEM) {
        return LOCALIZE(pickerItems[row]);
    } else if(pickerMode==PICKER_MODE_RANGE) {
        if(component ==0){
            return LOCALIZE(pickerRightItems[row]);
        }else{
            return LOCALIZE(pickerRightItems[row]);
        }
    }
    return LOCALIZE(pickerItems[row]);
}

-(void) sizeToFitHeight:(UILabel*) label maxWidth:(float) maxWidth
{
    CGSize size =  [label.text boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName: label.font}
                                            context:nil].size;
    CGRect labelFrame = label.frame;
    labelFrame.size.height = size.height;
    label.frame = labelFrame;
}


#pragma mark ShowHeartFunction
-(void) showHeartFromPoint:(CGPoint) startPoint
{
    UIImageView* heartImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    heartImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"colorheart_%i", (int) arc4random_uniform(28)+1]];
    
    UIImageView* catImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    catImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"cat_%i", (int) arc4random_uniform(8)+1]];
    
    UIImageView* bearImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    bearImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"bear_%i", (int) arc4random_uniform(8)+1]];
    
    UIImageView* rabbitImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    rabbitImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"rabbit_%i", (int) arc4random_uniform(8)+1]];
    
    UIImageView* bubbleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    bubbleImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"bubble_%i", (int) arc4random_uniform(10)+1]];
    
    
    UIImageView* spImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    spImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"money_%i", (int) arc4random_uniform(9)+1]];
    
    UIImageView* spImageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    spImageView2.image = [UIImage imageNamed:[NSString stringWithFormat:@"new_year_%i", (int) arc4random_uniform(7)+1]];
    
    
    UIImageView* presentImageView = heartImageView;
    
    if([SINGLETON isSpecialMode]){
        presentImageView = spImageView;
    }

    if((int)(arc4random_uniform(5))==1){
        
        if([SINGLETON isSpecialMode]){
            presentImageView = spImageView2;
        }else{
            int random = (int)(arc4random_uniform(4));
            if(random==0){
                presentImageView = bubbleImageView;
            }else if(random==1){
                presentImageView = catImageView;
            }else if(random==2){
                presentImageView = bearImageView;
            }else if(random==3){
                presentImageView = rabbitImageView;
            }
        }
    }
    
    [[self getWindowView] addSubview:presentImageView];
    presentImageView.center = startPoint;

    float totalAnimationTime = RANDOM_FLOAT(5.0, 6.0);
    float offsetX1 = RANDOM_FLOAT(-40.0, 40.0);
    float offsetX2 = RANDOM_FLOAT(-20.0, 20.0);
    float offsetY = RANDOM_FLOAT(-220.0, -180.0);

    // scale up
    presentImageView.transform = CGAffineTransformMakeScale(0.0, 0.0);

    [UIView animateWithDuration:kDialogFadeDuration animations:^{
        float finalScale = RANDOM_FLOAT(0.9, 1.1);
        
        presentImageView.transform = CGAffineTransformMakeScale(finalScale, finalScale);
    }];
    
    [UIView animateWithDuration:totalAnimationTime/2.0 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        presentImageView.center = CGPointMake(presentImageView.center.x+offsetX1, presentImageView.center.y+offsetY);
        presentImageView.alpha = 0.5;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:totalAnimationTime/2.0 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            presentImageView.center = CGPointMake(presentImageView.center.x+offsetX2, presentImageView.center.y+offsetY);
            presentImageView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [heartImageView removeFromSuperview];
        }];
    }];

    // random rotation
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:3.0 animations:^{
            presentImageView.transform = CGAffineTransformMakeRotation(RANDOM_FLOAT(-M_PI_4*0.75, M_PI_4*0.75));
        }];
    });
}


-(void) showSameHeartFromPoint:(CGPoint) startPoint selectedColor:(int)color
{
    if(color<1 || color >28)
        return;
    
    UIImageView* heartImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    NSString* imageName = [NSString stringWithFormat:@"colorheart_%i",color];
    heartImageView.image = [UIImage imageNamed:imageName];
    
    UIImageView* presentImageView = heartImageView;
    
    [[self getWindowView] addSubview:presentImageView];
    presentImageView.center = startPoint;
    
    float totalAnimationTime = RANDOM_FLOAT(2.5, 3.5);
    float offsetX1 = RANDOM_FLOAT(-40.0, 40.0);
    float offsetX2 = RANDOM_FLOAT(-20.0, 20.0);
    float offsetY = RANDOM_FLOAT(-220.0, -180.0);
    
    // scale up
    presentImageView.transform = CGAffineTransformMakeScale(0.0, 0.0);
    
    [UIView animateWithDuration:kDialogFadeDuration animations:^{
        float finalScale = RANDOM_FLOAT(0.9, 1.1);
        presentImageView.transform = CGAffineTransformMakeScale(finalScale, finalScale);
    }];
    
    [UIView animateWithDuration:totalAnimationTime/2.0 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
    
        presentImageView.center = CGPointMake(presentImageView.center.x+offsetX1, presentImageView.center.y+offsetY);
        presentImageView.alpha = 0.5;
   
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:totalAnimationTime/2.0 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            presentImageView.center = CGPointMake(presentImageView.center.x+offsetX2, presentImageView.center.y+offsetY);
            presentImageView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [heartImageView removeFromSuperview];
        }];
    }];
    
    // random rotation
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:3.0 animations:^{
            presentImageView.transform = CGAffineTransformMakeRotation(RANDOM_FLOAT(-M_PI_4*0.75, M_PI_4*0.75));
        }];
    });
}

-(void) showSingleEmitterHeartFromPoint:(CGPoint) startPoint selectedColor:(int)color
{
    if(color<1 || color >28)
        return;
    
    UIImageView* heartImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    NSString* imageName = [NSString stringWithFormat:@"colorheart_%i",color];
    heartImageView.image = [UIImage imageNamed:imageName];
    
    UIImageView* catImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    catImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"cat_%i", (int) arc4random_uniform(8)+1]];
    
    UIImageView* bearImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    bearImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"bear_%i", (int) arc4random_uniform(8)+1]];
    
    UIImageView* rabbitImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    rabbitImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"rabbit_%i", (int) arc4random_uniform(8)+1]];
    
    UIImageView* bubbleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    bubbleImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"bubble_%i", (int) arc4random_uniform(10)+1]];
    
    UIImageView* presentImageView = heartImageView;
    if((int)(arc4random_uniform(5))==1){
        int random = (int)(arc4random_uniform(4));
        if(random==0){
            presentImageView = bubbleImageView;
        }else if(random==1){
            presentImageView = catImageView;
        }else if(random==2){
            presentImageView = bearImageView;
        }else if(random==3){
            presentImageView = rabbitImageView;
        }
    }
    
    [[self getWindowView] addSubview:presentImageView];
    presentImageView.center = startPoint;
    
    float totalAnimationTime = RANDOM_FLOAT(2.0, 3.0);
    float offsetX1 = RANDOM_FLOAT(-50.0, 50.0);
    float offsetX2 = RANDOM_FLOAT(-20.0, 20.0);
    float offsetY = RANDOM_FLOAT(-320.0, -280.0);
    float rotationDegree = RANDOM_FLOAT(-90.0, 90.0);

    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(presentImageView.center.x, presentImageView.center.y-20)];
    // Crash if loss this , CGPathRef can not find point
    [path addCurveToPoint:CGPointMake(presentImageView.center.x+offsetX1+offsetX2, presentImageView.center.y+2*offsetY) controlPoint1:CGPointMake(presentImageView.center.x+offsetX1, presentImageView.center.y+offsetY/2) controlPoint2:CGPointMake(presentImageView.center.x+offsetX1+offsetX2, presentImageView.center.y+offsetY)];
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    animation.path = path.CGPath;
    animation.duration = totalAnimationTime;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    
    CABasicAnimation * rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotationAnimation.fromValue = [NSNumber numberWithFloat:0];
    rotationAnimation.toValue = [NSNumber numberWithFloat:((rotationDegree*M_PI)/180)];
    rotationAnimation.duration = totalAnimationTime;
    [rotationAnimation setBeginTime:0.0];
    rotationAnimation.removedOnCompletion = NO;
    rotationAnimation.fillMode = kCAFillModeForwards;
    
    CABasicAnimation *scaleAnim = [CABasicAnimation animationWithKeyPath:@"transform"];
    scaleAnim.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    scaleAnim.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)];
    scaleAnim.removedOnCompletion = NO;
    scaleAnim.fillMode = kCAFillModeForwards;
    
    CABasicAnimation *fade = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fade.fromValue = @(1.0f);
    fade.toValue = @(0.0f);
    fade.duration = totalAnimationTime;
    fade.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    fade.removedOnCompletion = NO;
    fade.fillMode = kCAFillModeForwards;
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    [group setDuration:totalAnimationTime+kDialogFadeDuration];
    [group setAnimations:[NSArray arrayWithObjects:animation,scaleAnim,fade,rotationAnimation, nil]];
    
    [presentImageView.layer addAnimation:group forKey:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(totalAnimationTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [presentImageView removeFromSuperview];
    });

    
}



-(void) showEmitterHeartFromPoint:(CGPoint) startPoint selectedColor:(int)color
{
    
//    UIImageView* heartImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
//    heartImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"colorheart_%i",color]];

    float multiplier = 0.25f;
    
    //Create the emitter layer
    CAEmitterLayer* emitter = [CAEmitterLayer layer];
    emitter.emitterPosition = startPoint;
    emitter.emitterMode = kCAEmitterLayerOutline;
    emitter.emitterShape = kCAEmitterLayerCircle;
    emitter.renderMode = kCAEmitterLayerAdditive;
    emitter.emitterSize = CGSizeMake(100 * multiplier, 0);
    
    //Create the emitter cell
    CAEmitterCell* particle = [CAEmitterCell emitterCell];
    particle.scale=0.05;
    particle.emissionLongitude = M_PI;
    particle.birthRate = multiplier * 100.0;
    particle.lifetime = multiplier*30;
    particle.lifetimeRange = multiplier * 4.0f;
    particle.velocity = 300;
    particle.velocityRange = 400;
    particle.emissionRange = 5.5;
    particle.scaleSpeed = 0.05; // was 0.3
    particle.alphaRange = 0.02;
    particle.alphaSpeed = 0.5;
    
    //particle.color = [[COOKBOOK_PURPLE_COLOR colorWithAlphaComponent:0.5f] CGColor];
    particle.contents = (__bridge id)([UIImage imageNamed:[NSString stringWithFormat:@"colorheart_%i",color]].CGImage);
    particle.name = @"particle";
    
    emitter.emitterCells = [NSArray arrayWithObject:particle];
    [[self getWindowView].layer addSublayer:emitter];
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    // emitter.emitterPosition = pt;
    [CATransaction commit];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [emitter removeFromSuperlayer];
    });
    
}


-(void) dialogAnimationUp:(int)range
{
    [UIView animateWithDuration:0.2 animations:^{
        dialogBgView.frame = CGRectMake(dialogBgView.frame.origin.x, dialogBgView.frame.origin.y
                                        -range, dialogBgView.frame.size.width, dialogBgView.frame.size.height);
    }];
}


#pragma mark ShowActionSheetFunction
-(void) showActionSheetDialogTitle:(NSString*) title options:(NSArray*) options destructiveIndexes:(NSArray*) destructiveIndexes cancelable:(BOOL) cancelable withCompletion: (void (^)(int selectedOption)) callback
{
    [self showActionSheetDialogTitle:title options:options destructiveIndexes:destructiveIndexes adminIndexes:nil cancelable:cancelable withCompletion:callback];
}

-(void) showActionSheetDialogTitle:(NSString*) title options:(NSArray*) options destructiveIndexes:(NSArray*) destructiveIndexes adminIndexes:(NSArray*)adminIndexes cancelable:(BOOL) cancelable withCompletion: (void (^)(int selectedOption)) callback
{
    [self removeDialogImmediately];

    dialogBgView = [self getTopBgView];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
    dialogBgView.alpha = 0.0f;
    dialogBgView.userInteractionEnabled = YES;
    
    int leftMargin = 15;
    int topMargin =7 ;
    int btnHeight =45 ;
    int nextY = topMargin;
    float animationDuration = kDialogFadeDuration;

    UIImageView* dialogBoxView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"dialog"] resizableImageWithCapInsets:UIEdgeInsetsMake(24, 24, 24, 24)]];
    
    dialogBoxView.userInteractionEnabled = YES;
    [dialogBgView addSubview:dialogBoxView];
    
    /* Caucluate Box Height */
    float boxHeight = topMargin;
    if(![title isEqualToString:@""]){
        boxHeight += btnHeight+topMargin;
    }
    boxHeight += [options count]*(btnHeight+topMargin);
    
    if(cancelable){
        boxHeight += 3*topMargin +btnHeight;
    }
    
//    dialogBoxView.frame = CGRectMake(0, DEVICE_SIZE.height, DEVICE_SIZE.width, boxHeight);
    dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);

    
    /* Add Buttons */
    if(![title isEqualToString:@""]){
        
        UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, btnHeight)];
        titleLabel.text = title;
        titleLabel.textColor = DIALOG_TITLE_TEXT_COLOR;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.font = SYSTEM_FONT_WITH_SIZE(18);
        titleLabel.numberOfLines = 2;
        titleLabel.minimumScaleFactor = 0.0;
        titleLabel.adjustsFontSizeToFitWidth = YES;
        titleLabel.userInteractionEnabled = NO;
        [dialogBoxView addSubview:titleLabel];
        
        nextY += titleLabel.frame.size.height;
    }
    
    nextY += topMargin;
    
    int index = 0;
    
    for(NSString* option in options) {
        
        BOOL isDestructive = [destructiveIndexes containsObject:[NSNumber numberWithInt:index]];
        BOOL isAdminBtn = [adminIndexes containsObject:[NSNumber numberWithInt:index]];

        UIButton* optionButton;
        
        if (isAdminBtn) {
            optionButton = [self getGMButton];
        } else {
            if (isDestructive) {
                optionButton = [self getDestructiveButton];
            } else {
                optionButton = [self getNormalButton];
            }
        }
        
        optionButton.frame = CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, btnHeight);

        [optionButton setTitle:option forState:UIControlStateNormal];
        optionButton.tag = index;
        [dialogBoxView addSubview:optionButton];
        
        [optionButton bk_addEventHandler:^(id sender) {
            
            [UIView animateWithDuration:animationDuration animations:^{
                
                dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);

                dialogBgView.alpha = 0.0f;
            }];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [dialogBgView removeFromSuperview];
                dialogBgView = nil;
            });
            
            callback(index);
        } forControlEvents:UIControlEventTouchUpInside];
        
        nextY += optionButton.frame.size.height + topMargin;
        index++;
    }
    
    nextY+=5;
    
    UIButton* cancelButton = [self getCancelButton];
    cancelButton.frame = CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, btnHeight);

    
    if(cancelable) {
        [dialogBoxView addSubview:cancelButton];
        nextY += topMargin + cancelButton.frame.size.height;
    }
    
    [cancelButton bk_addEventHandler:^(id sender) {
        [UIView animateWithDuration:animationDuration animations:^{
            
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            
            dialogBgView.alpha = 0.0f;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
        
    } forControlEvents:UIControlEventTouchUpInside];

    
    [dialogBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [UIView animateWithDuration:animationDuration animations:^{
            
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            
            dialogBgView.alpha = 0.0f;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
    }]];
    
    [UIView animateWithDuration:animationDuration animations:^{
        
        dialogBgView.alpha = 1.0f;
        dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT-boxHeight, SCREEN_WIDTH, boxHeight);
        
    }];
}


-(void) showActionSheetDialogTitleV2:(NSString*) title options:(NSArray*) options destructiveIndexes:(NSArray*) destructiveIndexes adminIndexes:(NSArray*)adminIndexes cancelable:(BOOL) cancelable withCompletion: (void (^)(int selectedOption)) callback
{
    [self removeDialogImmediately];
    
    dialogBgView = [self getTopBgView];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
    dialogBgView.alpha = 0.0f;
    dialogBgView.userInteractionEnabled = YES;
    
    int leftMargin = 15;
    int topMargin =7 ;
    int btnHeight =45 ;
    int nextY = topMargin;
    float animationDuration = kDialogFadeDuration;
    
    UIImageView* dialogBoxView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"dialog"] resizableImageWithCapInsets:UIEdgeInsetsMake(24, 24, 24, 24)]];
    
    dialogBoxView.userInteractionEnabled = YES;
    [dialogBgView addSubview:dialogBoxView];
    
    /* Caucluate Box Height */
    float boxHeight = topMargin;
    if(![title isEqualToString:@""]){
        boxHeight += btnHeight+topMargin;
    }
    boxHeight += [options count]*(btnHeight+topMargin);
    
    if(cancelable){
        boxHeight += 5*topMargin +btnHeight;
    }
    
    //    dialogBoxView.frame = CGRectMake(0, DEVICE_SIZE.height, DEVICE_SIZE.width, boxHeight);
    dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
    
    
    /* Add Buttons */
    if(![title isEqualToString:@""]){
        
        UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, btnHeight)];
        titleLabel.text = title;
        titleLabel.textColor = DIALOG_TITLE_TEXT_COLOR;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.font = SYSTEM_FONT_WITH_SIZE(18);
        titleLabel.numberOfLines = 2;
        titleLabel.minimumScaleFactor = 0.0;
        titleLabel.adjustsFontSizeToFitWidth = YES;
        titleLabel.userInteractionEnabled = NO;
        [dialogBoxView addSubview:titleLabel];
        
        nextY += titleLabel.frame.size.height;
    }
    
    nextY += topMargin;
    
    int index = 0;
    
    for(NSString* option in options) {
        
        BOOL isDestructive = [destructiveIndexes containsObject:[NSNumber numberWithInt:index]];
        BOOL isAdminBtn = [adminIndexes containsObject:[NSNumber numberWithInt:index]];
        
        UIButton* optionButton;
        
        if (isAdminBtn) {
            optionButton = [self getGMButton];
        } else {
            if (isDestructive) {
                optionButton = [self getDestructiveButton];
            } else {
                optionButton = [self getNormalButton];
            }
        }
        if ([option isEqualToString:@"Phone register"]) {
            optionButton=[self getCancelButton];
        }else if ([option isEqualToString:@"Use Facebook to Sign Up"]){
            [optionButton setImage: [UIImage imageNamed:@"lod_facebook"] forState:UIControlStateNormal];

        }
        
        optionButton.frame = CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, btnHeight);
        
        [optionButton setTitle:LOCALIZE(option) forState:UIControlStateNormal];
        optionButton.tag = index;
        [dialogBoxView addSubview:optionButton];
        
        [optionButton bk_addEventHandler:^(id sender) {
            
            [UIView animateWithDuration:animationDuration animations:^{
                
                dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
                
                dialogBgView.alpha = 0.0f;
            }];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [dialogBgView removeFromSuperview];
                dialogBgView = nil;
            });
            
            callback(index);
        } forControlEvents:UIControlEventTouchUpInside];
        
        nextY += optionButton.frame.size.height + topMargin;
        index++;
    }
    
    nextY+=5+2*topMargin;
    
    UIButton* cancelButton = [self getNormalButton];
    cancelButton.frame = CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, btnHeight);
    [cancelButton setTitle:LOCALIZE(@"Cancel") forState:UIControlStateNormal];
    
    if(cancelable) {
        [dialogBoxView addSubview:cancelButton];
        nextY += topMargin + cancelButton.frame.size.height;
    }
    
    [cancelButton bk_addEventHandler:^(id sender) {
        [UIView animateWithDuration:animationDuration animations:^{
            
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            
            dialogBgView.alpha = 0.0f;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    [dialogBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [UIView animateWithDuration:animationDuration animations:^{
            
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            
            dialogBgView.alpha = 0.0f;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
    }]];
    
    [UIView animateWithDuration:animationDuration animations:^{
        
        dialogBgView.alpha = 1.0f;
        dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT-boxHeight, SCREEN_WIDTH, boxHeight);
        
    }];
}

-(void) showActionSheetOKDialogTitle:(NSString*)title message:(NSString*)message buttonText:(NSString*)buttonText cancelable:(BOOL)cancelable withCompletion: (void (^)(BOOL okClicked)) callback
{
    [self removeDialogImmediately];
    
    dialogBgView = [self getTopBgView];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
    dialogBgView.alpha = 0.0f;

    int topMargin =7 ;
    int leftMargin =15 ;
    int btnHeight =45 ;
    int nextY = topMargin;
    float animationDuration = kDialogFadeDuration;
    
    UIImageView* dialogBoxView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"dialog"] resizableImageWithCapInsets:UIEdgeInsetsMake(24, 24, 24, 24)]];
    
    dialogBoxView.userInteractionEnabled = YES;
    [dialogBgView addSubview:dialogBoxView];

    /* Add Title */
    if(![title isEqualToString:@""]){
        
        UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, btnHeight-10)];
        titleLabel.text = title;
        titleLabel.textColor = DARK_GRAY_COLOR;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.font = BOLD_FONT_WITH_SIZE(18);
        titleLabel.numberOfLines = 2;
        titleLabel.minimumScaleFactor = 0.0;
        titleLabel.adjustsFontSizeToFitWidth = YES;
        titleLabel.userInteractionEnabled = NO;
        [dialogBoxView addSubview:titleLabel];
        
        nextY += titleLabel.frame.size.height;
    }
    
    
    /* Add SubTitle */

    if(![message isEqualToString:@""]){

        UILabel* subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, btnHeight)];
        subtitleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        subtitleLabel.textAlignment = NSTextAlignmentCenter;
        [subtitleLabel setText:message];
        
        CGSize stringSize = [message getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-2*leftMargin,200) withFont:subtitleLabel.font];

        subtitleLabel.frame = CGRectMake(SCREEN_WIDTH/2-stringSize.width/2, nextY, stringSize.width, stringSize.height);
        [dialogBoxView addSubview:subtitleLabel];
        
        nextY += subtitleLabel.frame.size.height+topMargin;
    }
    
    nextY+= topMargin;
    
    /* Add OK Button */
    
    UIButton* okButton = [self getDestructiveButton];
    okButton.frame = CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, btnHeight);
    [okButton setTitle:buttonText forState:UIControlStateNormal];
    okButton.titleLabel.font = BOLD_FONT_WITH_SIZE(18);
    nextY += btnHeight+topMargin;
    [dialogBoxView addSubview:okButton];

    UIButton* cancelButton = [self getCancelButton];
    cancelButton.frame = CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, btnHeight);
    
    
    if(cancelable) {
        [dialogBoxView addSubview:cancelButton];
        nextY += topMargin + cancelButton.frame.size.height;
        
    }
    
    float boxHeight = nextY;
    
    dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
    
    [okButton bk_addEventHandler:^(id sender) {
        
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
        
        callback(YES);
    } forControlEvents:UIControlEventTouchUpInside];
    
    if(cancelable){
        [dialogBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            callback(NO);
            [UIView animateWithDuration:animationDuration animations:^{
                dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
                dialogBgView.alpha = 0.0f;
            }];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [dialogBgView removeFromSuperview];
                dialogBgView = nil;
            });
        }]];
    }
    
    [cancelButton bk_addEventHandler:^(id sender) {
        callback(NO);
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    [UIView animateWithDuration:animationDuration animations:^{
        dialogBgView.alpha = 1.0f;
        dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT-boxHeight, SCREEN_WIDTH, boxHeight);
    }];
}



-(void) showActionSheetUserDialog:(UserObject*)user isPublisher:(BOOL)isPublisher isLivestreamer:(BOOL)isLivestreamer withCompletion: (void (^)(UserDialogAction selectedAction)) callback
{
    [self removeDialogImmediately];
    
    dialogBgView = [self getTopBgView];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
    dialogBgView.alpha = 0.0f;
    int leftMargin = 15;
    int topMargin =7 ;
    int btnHeight =35 ;
    float animationDuration = kDialogFadeDuration;
    float imageWidth = 60.0f;
    float idHeight = 30.0f;
    float reportbthHeight=18;
    
    UIImageView* dialogBoxView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"dialog"] resizableImageWithCapInsets:UIEdgeInsetsMake(22, 22, 22, 2)]];
    
    dialogBoxView.userInteractionEnabled = YES;
    [dialogBgView addSubview:dialogBoxView];
    
    UIActivityIndicatorView* progressView = [UIActivityIndicatorView new];
    [progressView setTintColor:MAIN_COLOR];
    [progressView setColor:MAIN_COLOR];
    [dialogBoxView addSubview:progressView];

    int nextY = topMargin;
    
    UIButton* reportbth = [ThemeManager getGraylineBtn];
    [reportbth setTitle:LOCALIZE(@"livereport") forState:UIControlStateNormal];
    
    /* Add Buttons */
    if(![user.userID isEqualToString:MY_USER_ID]){
        [reportbth setTitleColor:GRAY_COLOR forState:UIControlStateNormal];
        reportbth.frame = CGRectMake(leftMargin, nextY+4, SCREEN_HEIGHT*1/12+5, reportbthHeight);
        nextY=topMargin+reportbthHeight+12;
        [dialogBoxView addSubview:reportbth];
        [progressView startAnimating];

    }else{
        nextY=topMargin;
    }
    
    UIImageView* userImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, nextY, imageWidth, imageWidth)];
    if(user!=nil){
        [userImageView setImageWithURL:S3_FILE_URL(user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];

//        [userImageView my_setImageWithURL:user.picture placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s") isThumbnail:NO];
    }else{
        [userImageView setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }
    [dialogBoxView addSubview:userImageView];
    userImageView.layer.masksToBounds = YES;
    userImageView.layer.cornerRadius = imageWidth/2.0f;
    
    UILabel* openIDLabel = [[UILabel alloc]initWithFrame:CGRectMake(20+imageWidth, nextY, SCREEN_WIDTH-20-imageWidth-10, idHeight)];
    if(user!=nil){
        [openIDLabel setText:user.openID];
    }else{
        [openIDLabel setText:LOCALIZE(@"user_not_exist")];
    }
    [openIDLabel setTextColor:MAIN_COLOR];
    openIDLabel.font = BOLD_FONT_WITH_SIZE(16);
    [dialogBoxView addSubview:openIDLabel];

    nextY += idHeight;
    
    UILabel* bioLabel = [[UILabel alloc]initWithFrame:CGRectMake(20+imageWidth, nextY, SCREEN_WIDTH-20-imageWidth-10, 0)];
    if(user!=nil){
        if(![user.bio isEqualToString:@""]){
            [bioLabel setText:user.bio];
            bioLabel.numberOfLines = 0;
            bioLabel.font = SYSTEM_FONT_WITH_SIZE(14);
            [bioLabel setTextColor:DARK_GRAY_COLOR];
            [dialogBoxView addSubview:bioLabel];
        }
    }else{
        [bioLabel setText:LOCALIZE(@"user_not_exist")];
    }
    [bioLabel sizeToFit];

    if (bioLabel.frame.size.height==0&&![user.userID isEqualToString:MY_USER_ID]) {
        openIDLabel.center=CGPointMake(14*SCREEN_WIDTH/18+5,topMargin+imageWidth/2+nextY/2 );
        userImageView.center=CGPointMake(14*SCREEN_WIDTH/18-openIDLabel.frame.size.width/2-imageWidth/2-5,topMargin+imageWidth/2+nextY/2);
    }
    
    float boxHeight = 0.0f;
    
    nextY += bioLabel.frame.size.height+3;

    if(![user.userID isEqualToString:MY_USER_ID] && user!=nil){

        /* Caucluate Box Height */
        if (bioLabel.frame.size.height>btnHeight) {
            boxHeight = nextY+btnHeight+topMargin+5+topMargin+reportbthHeight;
        }else{
            boxHeight = nextY+btnHeight+topMargin+btnHeight+topMargin-bioLabel.frame.size.height+10+reportbthHeight;
        }
    
        
        UIButton* resButton = [ThemeManager getGreenLineBtn];
        resButton.frame = CGRectMake(0, nextY, SCREEN_WIDTH/2-30, btnHeight);
        resButton.layer.cornerRadius = btnHeight/2.0f;
        if(bioLabel.frame.size.height>btnHeight)
        {
            resButton.center = CGPointMake(5*SCREEN_WIDTH/18, nextY+btnHeight/2+topMargin+15);
        }else if(bioLabel.frame.size.height==0){
            resButton.center = CGPointMake(5*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin+15);
        }else{
            resButton.center = CGPointMake(5*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin-bioLabel.frame.size.height/2);
        }
        [resButton setTitle:LOCALIZE(@"@Response") forState:UIControlStateNormal];
        [dialogBoxView addSubview:resButton];
        
        [resButton bk_addEventHandler:^(id sender) {
            [UIView animateWithDuration:animationDuration animations:^{
                dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
                dialogBgView.alpha = 0.0f;
            } completion:^(BOOL finished) {
                if (finished) {
                    [dialogBgView removeFromSuperview];
                    dialogBgView = nil;
                }
            }];
            
            callback(UserDialogActionResponse);
            
        } forControlEvents:UIControlEventTouchUpInside];
        
      
        UIButton* banButton = [ThemeManager getGreenLineBtn];
        banButton.frame = CGRectMake(0, nextY, SCREEN_WIDTH/2-30, btnHeight);
        banButton.layer.cornerRadius = btnHeight/2.0f;
        if(bioLabel.frame.size.height>btnHeight)
        {
            banButton.center = CGPointMake(5*SCREEN_WIDTH/18, nextY+btnHeight/2+topMargin+15);
        }else if(bioLabel.frame.size.height==0){
            banButton.center = CGPointMake(5*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin+15);
        }else{
            banButton.center = CGPointMake(5*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin-bioLabel.frame.size.height/2);
        }
        [banButton setTitle:LOCALIZE(@"block") forState:UIControlStateNormal];
        
        [reportbth bk_addEventHandler:^(id sender) {
            
            [UIView animateWithDuration:animationDuration animations:^{
                dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
                dialogBgView.alpha = 0.0f;
            } completion:^(BOOL finished) {
                if (finished) {
                    [dialogBgView removeFromSuperview];
                    dialogBgView = nil;
                }
            }];
            
            callback(UserDialogActionReport);
            
        } forControlEvents:UIControlEventTouchUpInside];
        
        
        UIButton* followBtn = [ThemeManager getGreenLineBtn];
        followBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH/2-30, btnHeight);
        if(bioLabel.frame.size.height>btnHeight)
        {
            followBtn.center = CGPointMake(13*SCREEN_WIDTH/18, nextY+btnHeight/2+topMargin+15);
        }else if(bioLabel.frame.size.height==0){
            followBtn.center = CGPointMake(13*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin+15);
        }else{
            followBtn.center = CGPointMake(13*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin-bioLabel.frame.size.height/2);
        }
        
        followBtn.layer.cornerRadius = btnHeight/2.0f;
        [followBtn setTitle:LOCALIZE(@"+Follow") forState:UIControlStateNormal];
        
        [dialogBoxView addSubview:followBtn];

        if(isPublisher){
            resButton.hidden = YES;
            
            [dialogBoxView addSubview:banButton];
            
//            followBtn.center = CGPointMake(13*SCREEN_WIDTH/18, nextY+btnHeight/2);
//            followBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH-40, btnHeight);

            if(bioLabel.frame.size.height>btnHeight)
            {
                followBtn.center = CGPointMake(13*SCREEN_WIDTH/18, nextY+btnHeight/2+topMargin+15);
            }else if(bioLabel.frame.size.height==0){
                followBtn.center = CGPointMake(13*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin+15);
            }else{
                followBtn.center = CGPointMake(13*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin-bioLabel.frame.size.height/2);
            }
        }
        reportbth.alpha=0;
        openIDLabel.alpha=0;
        userImageView.alpha=0;
        bioLabel.alpha=0;
        followBtn.alpha=0;
        resButton.alpha=0;
        banButton.alpha=0;

        [API_MANAGER getUserInfo:user.openID completion:^(BOOL success, UserObject *userObject) {
            if(success){

                [UIView animateWithDuration:0.5f animations:^{
                    reportbth.alpha=1;
                    openIDLabel.alpha=1;
                    userImageView.alpha=1;
                    bioLabel.alpha=1;
                    followBtn.alpha=1;
                    resButton.alpha=1;
                    banButton.alpha=1;
                    
                }];

                
                [progressView stopAnimating];
                
                [banButton bk_addEventHandler:^(id sender) {
                    

                    [UIView animateWithDuration:animationDuration animations:^{
                        dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
                        dialogBgView.alpha = 0.0f;
                    } completion:^(BOOL finished) {
                        if (finished) {
                            [dialogBgView removeFromSuperview];
                            dialogBgView = nil;

                        }
                    }];
                    
                    if (userObject.isBlocked>0) {
                        callback(UserDialogActionUnblock);
                    }
                    else {
                        callback(UserDialogActionBlock);
                    }
                    
                } forControlEvents:UIControlEventTouchUpInside];
                

                
                
                [followBtn bk_addEventHandler:^(id sender) {

                    [UIView animateWithDuration:animationDuration animations:^{
                        dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
                        dialogBgView.alpha = 0.0f;
                    } completion:^(BOOL finished) {
                        if (finished) {
                            [dialogBgView removeFromSuperview];
                            dialogBgView = nil;
                        }
                    }];
                    
                    if (userObject.isFollowing>0) {
                        callback(UserDialogActionUnfollow);
                    }
                    else {
                        if (![userObject isPrivacyMode]) {
                            callback(UserDialogActionFollow);
                        }
                        else {
                            if (userObject.followRequestTime==0) {
                                callback(UserDialogActionFollow);
                            }
                            else {
                                callback(UserDialogActionCancelFollowRequest);
                            }
                        }
                    }
                    
                } forControlEvents:UIControlEventTouchUpInside];

                
                if(userObject.isFollowing>0){
                    //            followBtn.hidden = YES;
                    
                    [followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    [followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
                    [followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
                    
                    [followBtn setTitle:LOCALIZE(@"user_profile_following") forState:UIControlStateNormal];
                    
                    //            resButton.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2);
                }
                else if ([userObject isPrivacyMode] && userObject.followRequestTime != 0) {
                    [followBtn setTitle:LOCALIZE(@"Sended_request") forState:UIControlStateNormal];
                }
                
                
                if(userObject.isBlocked>0){
                    //            followBtn.hidden = YES;
                    [banButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    
                    [banButton setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
                    [banButton setBackgroundImage:[[UIImage imageNamed:@"btn_green_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
                    
                    [banButton setTitle:LOCALIZE(@"blocked") forState:UIControlStateNormal];
                    
                    //            resButton.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2);
                }
                
                
                if(isLivestreamer)
                {
                    
                    resButton.hidden=YES;
                    followBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH-40, btnHeight);
                    
                    if(bioLabel.frame.size.height>btnHeight)
                    {
                        followBtn.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2+topMargin+15);
                    }else if(bioLabel.frame.size.height==0){
                        followBtn.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2+imageWidth/2+topMargin+15);
                    }else{
                        followBtn.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2+imageWidth/2+topMargin-bioLabel.frame.size.height/2);
                    }
                }
            }
        }];
        
    }else{
        boxHeight = ( nextY+topMargin ) < (imageWidth +2*topMargin)?(imageWidth +2*topMargin):( nextY+topMargin );
    }
    
    dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);

    
    
    UIButton* cancelButton = [UIButton new];
    cancelButton.frame = CGRectMake(SCREEN_WIDTH-leftMargin-btnHeight, topMargin, btnHeight, btnHeight);
    [cancelButton setImage:IMAGE_FROM_BUNDLE(@"nav_cancel_black") forState:UIControlStateNormal];
    
    [cancelButton bk_addEventHandler:^(id sender) {
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    [dialogBoxView addSubview:cancelButton];
    progressView.center = CGPointMake(SCREEN_WIDTH/2, boxHeight/2);

    [dialogBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [UIView animateWithDuration:animationDuration animations:^{
            
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
    }]];
    
    [UIView animateWithDuration:animationDuration animations:^{
        dialogBgView.alpha = 1.0f;
        dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT-boxHeight, SCREEN_WIDTH, boxHeight);

    }];
}

-(void)showRegisterActionSheetWithCompletion:(void (^)(NSString* action))callback    // qq , wechat , weibo , facebook , msg
{
    [self removeDialogImmediately];
    
    CGFloat elementMargin = 8.0f;
    CGFloat elementHeight = 40.0f;
    CGFloat btnHeight = 50.0f;
    CGFloat labelHeight = 30.0f;
    float animationDuration = kDialogFadeDuration;
    
    CGFloat leftMargin = (SCREEN_WIDTH-4*btnHeight)/5;
    
    dialogBgView = [self getTopBgView];
    dialogBgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    dialogBgView.alpha = 0.0f;
    dialogBgView.userInteractionEnabled = YES;
    
    UIImageView *dialogBoxView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"dialog"] resizableImageWithCapInsets:UIEdgeInsetsMake(22, 22, 22, 2)]];
    
    dialogBoxView.userInteractionEnabled = YES;
    [dialogBgView addSubview:dialogBoxView];
    
    float boxHeight = btnHeight + labelHeight + 4*elementMargin + elementHeight + labelHeight + elementMargin;
    dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
    
    UITapGestureRecognizer *tapBackgroundGR = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [UIView animateWithDuration:kDialogFadeDuration animations:^{
            dialogBgView.alpha = 0.0f;
            dialogBoxView.transform = CGAffineTransformIdentity;
            
        } completion:^(BOOL finished) {
            if (finished) {
                [dialogBgView removeFromSuperview];
            }
        }];
    }];
    [dialogBgView addGestureRecognizer:tapBackgroundGR];
    
    float nextY = 2*elementMargin;
    
    UILabel *registerLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, elementMargin, SCREEN_WIDTH, labelHeight)];
    registerLabel.textAlignment = NSTextAlignmentCenter;
    registerLabel.text = LOCALIZE(@"Choose register way");
    registerLabel.textColor = BLACK_COLOR;
    [dialogBoxView addSubview:registerLabel];
    
    nextY = nextY + labelHeight + elementMargin;
    
    
    UIButton *weiboBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, btnHeight, btnHeight)];
    UIButton *qqButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, btnHeight, btnHeight)];
    UIButton *facebookBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, btnHeight, btnHeight)];
    UIButton *wechatBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, btnHeight, btnHeight)];
    UIButton *msgBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, btnHeight, btnHeight)];
    
    [dialogBoxView addSubview:weiboBtn];
    [dialogBoxView addSubview:qqButton];
    //    [dialogBoxView addSubview:facebookBtn];
    [dialogBoxView addSubview:wechatBtn];
    [dialogBoxView addSubview:msgBtn];
    
    weiboBtn.tag = 0;
    qqButton.tag = 1;
    facebookBtn.tag = 2;
    wechatBtn.tag = 3;
    msgBtn.tag = 4;
    
    [weiboBtn setBackgroundImage:[UIImage imageNamed:@"register_weibo"] forState:UIControlStateNormal];
    [weiboBtn setBackgroundImage:[UIImage imageNamed:@"register_weibo_down"] forState:UIControlStateHighlighted];
    
    [qqButton setBackgroundImage:[UIImage imageNamed:@"register_qq"] forState:UIControlStateNormal];
    [qqButton setBackgroundImage:[UIImage imageNamed:@"register_qq_down"] forState:UIControlStateHighlighted];
    
    [facebookBtn setBackgroundImage:[UIImage imageNamed:@"register_facebook"] forState:UIControlStateNormal];
    [facebookBtn setBackgroundImage:[UIImage imageNamed:@"register_facebook_down"] forState:UIControlStateHighlighted];
    
    [wechatBtn setBackgroundImage:[UIImage imageNamed:@"register_wechat"] forState:UIControlStateNormal];
    [wechatBtn setBackgroundImage:[UIImage imageNamed:@"register_wechat_down"] forState:UIControlStateHighlighted];
    
    [msgBtn setBackgroundImage:[UIImage imageNamed:@"register_phone"] forState:UIControlStateNormal];
    [msgBtn setBackgroundImage:[UIImage imageNamed:@"register_message_down"] forState:UIControlStateHighlighted];
    

    //    nextY += btnHeight + labelHeight + elementMargin;
    //    facebookBtn.center = CGPointMake(SCREEN_WIDTH/5, nextY+btnHeight/2);

    msgBtn.center = CGPointMake(leftMargin+btnHeight/2, nextY+btnHeight/2);
    weiboBtn.center = CGPointMake(2*leftMargin+3*btnHeight/2, nextY+btnHeight/2);
    qqButton.center = CGPointMake(3*leftMargin+5*btnHeight/2, nextY+btnHeight/2);
    wechatBtn.center= CGPointMake(4*leftMargin+7*btnHeight/2, nextY+btnHeight/2);
    
    nextY += btnHeight + labelHeight + elementMargin;
    
    //    weiboBtn.center = CGPointMake((facebookBtn.center.x+wechatBtn.center.x)/2, weiboBtn.center.y);
    //    qqButton.center = CGPointMake((msgBtn.center.x+wechatBtn.center.x)/2, qqButton.center.y);
    
    UILabel* weiboLabel = [UILabel new];
    [weiboLabel setText:@"Weibo"];
    [weiboLabel sizeToFit];
    weiboLabel.center = CGPointMake(weiboBtn.center.x, weiboBtn.frame.origin.y+weiboBtn.frame.size.height+labelHeight/2);
    [dialogBoxView addSubview:weiboLabel];
    
    UILabel* qqLabel = [UILabel new];
    [qqLabel setText:@"QQ"];
    [qqLabel sizeToFit];
    qqLabel.center = CGPointMake(qqButton.center.x, qqButton.frame.origin.y+qqButton.frame.size.height+labelHeight/2);
    [dialogBoxView addSubview:qqLabel];
    
    UILabel* facebookLabel = [UILabel new];
    [facebookLabel setText:@"Facebook"];
    [facebookLabel sizeToFit];
    facebookLabel.center = CGPointMake(facebookBtn.center.x, facebookBtn.frame.origin.y+facebookBtn.frame.size.height+labelHeight/2);
    //    [dialogBoxView addSubview:facebookLabel];
    
    UILabel* wechatLabel = [UILabel new];
    [wechatLabel setText:@"Wechat"];
    [wechatLabel sizeToFit];
    wechatLabel.center = CGPointMake(wechatBtn.center.x, wechatBtn.frame.origin.y+wechatBtn.frame.size.height+labelHeight/2);
    [dialogBoxView addSubview:wechatLabel];
    
    UILabel* msgLabel = [UILabel new];
    [msgLabel setText:@"Phone"];
    [msgLabel sizeToFit];
    msgLabel.center = CGPointMake(msgBtn.center.x, msgBtn.frame.origin.y+msgBtn.frame.size.height+labelHeight/2);
    [dialogBoxView addSubview:msgLabel];
    
    NSArray* btnArray = @[weiboBtn,qqButton,facebookBtn,msgBtn,wechatBtn];
    
    for(UIButton* btn in btnArray){
        
        [btn bk_addEventHandler:^(id sender) {
            
            if(btn.tag == 0){
                callback(@"weibo");
            }else if(btn.tag ==1){
                callback(@"qq");
            }else if(btn.tag ==2){
                callback(@"facebook");
            }else if(btn.tag ==3){
                callback(@"wechat");
            }else if(btn.tag ==4){
                callback(@"msg");
            }
            
            [UIView animateWithDuration:animationDuration animations:^{
                
                //                dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
                dialogBgView.alpha = 0.0f;
            }];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [dialogBgView removeFromSuperview];
                dialogBgView = nil;
            });
            
        } forControlEvents:UIControlEventTouchUpInside];
    }
    
    UIButton* cancelButton = [self getCancelButton];
    cancelButton.frame = CGRectMake(elementMargin, nextY, SCREEN_WIDTH-2*elementMargin, elementHeight);
    
    [cancelButton bk_addEventHandler:^(id sender) {
        [UIView animateWithDuration:animationDuration animations:^{
            
            //            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    [dialogBoxView addSubview:cancelButton];
    
    [UIView animateWithDuration:animationDuration animations:^{
        dialogBgView.alpha = 1.0f;
        dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT-boxHeight, SCREEN_WIDTH, boxHeight);
    }];
    
}

- (void)showActionSheetShareLink:(ShareMediaType)type withObj:(id)obj hasRestream:(BOOL)hasRestream withCompletion:(void (^)(NSString* action))callback
{
    [self removeDialogImmediately];
    BOOL cancelable = YES;
    NSString *title = LOCALIZE(@"Share_to");
    __block NSString *shareUrl=@"";
    __block NSString *shareText=@"";
    __block NSString *shareImage=@"";
    
    if (type == ShareMediaTypeApp) {
        shareUrl = APP_SHARE_URL;
        shareText = [NSString stringWithFormat:LOCALIZE(@"SHARE_BODY"), GET_DEFAULT(USER_OPEN_ID), APP_SHARE_URL];
    }
    else {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self getShareLinkType:type withObj:obj completion:^(NSString *url, NSString *shareText, NSString *shareImgPath, NSError *error) {
                shareUrl = url;
                shareText = shareText;
                shareImage = shareImgPath;
            }];
        });
    }
    
    NSArray* shareStrArray;
    NSArray* shareImgArray;
    
    #ifdef INTERNATIONAL_VERSION
        shareStrArray = [NSArray arrayWithObjects:@"17", @"Facebook", @"Twitter", @"Weibo", @"Wechat", @"Line", @"WhatsApp", @"CopyLink", @"Mail", @"Message",nil];
        shareImgArray = [NSArray arrayWithObjects:@"share_17", @"share_fb", @"share_twitter", @"share_weibo", @"share_wechat", @"share_line", @"share_whatsapp", @"share_copy", @"share_mail", @"share_message", @"share_wechat",nil];
    #else
        shareStrArray=[NSArray arrayWithObjects:@"17",@"Weibo",@"Wechat",@"moment",@"QQ",@"Qzone",@"CopyLink", nil];
        shareImgArray=[NSArray arrayWithObjects:@"share_17",@"share_weibo",@"share_wechat",@"share_moments",@"share_qq",@"share_qzone",@"share_copy", nil];
    #endif
    
    dialogBgView = [self getTopBgView];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
    dialogBgView.alpha = 0.0f;
    dialogBgView.userInteractionEnabled = YES;
    
    int leftMargin = 35;
    int topMargin = 4 ;
    int btnHeight = 45 ;
    int nextY = 0;
    int imageHeight = (SCREEN_WIDTH - 5 * leftMargin) / 4;
    int labelHeight = 30;
    float animationDuration = kDialogFadeDuration;

    _shareLiveView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"dialog"] resizableImageWithCapInsets:UIEdgeInsetsMake(22, 22, 22, 22)]];
    
    _shareLiveView.userInteractionEnabled = YES;
    [dialogBgView addSubview:_shareLiveView];
    
    /* Caucluate Box Height */
    float boxHeight = topMargin;
    
    if(![title isEqualToString:@""]){
        boxHeight += btnHeight+topMargin;
    }

    if (type == ShareMediaTypeLive) {
        boxHeight += (imageHeight + labelHeight + 2 * topMargin) * (ceil([shareStrArray count]/4.0));
    }
    else {
        boxHeight += (imageHeight + labelHeight + 2 * topMargin) * (ceil(([shareStrArray count]-1) /4.0));
    }
    
    if (cancelable) {
        boxHeight += topMargin + btnHeight;
    }
    
    _shareBoxHeight = boxHeight;
    _shareLiveView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
    
    /* Add Buttons */
    if (![title isEqualToString:@""]) {
        
        UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, nextY, SCREEN_WIDTH-2*leftMargin, btnHeight)];
        titleLabel.text = title;
        titleLabel.textColor = DIALOG_TITLE_TEXT_COLOR;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.font = SYSTEM_FONT_WITH_SIZE(18);
        titleLabel.numberOfLines = 2;
        titleLabel.minimumScaleFactor = 0.0;
        titleLabel.adjustsFontSizeToFitWidth = YES;
        titleLabel.userInteractionEnabled = NO;
        [_shareLiveView addSubview:titleLabel];
        nextY += titleLabel.frame.size.height;
    }

    int nextX = leftMargin;
    int insertIndex = 0;
    
    for (int index = (type != ShareMediaTypeLive) ? 1 : 0; index < [shareStrArray count]; index++) {
        
        UIButton *imageBtn = [[UIButton alloc]initWithFrame:CGRectMake(nextX, nextY, imageHeight, imageHeight)];
        imageBtn.tag = index;
        
        if (index == 0 && hasRestream == YES) {
        
            [imageBtn setBackgroundImage:[UIImage imageNamed:@"share_17off"] forState:UIControlStateNormal];
            [imageBtn setBackgroundImage:[UIImage imageNamed:@"share_17off_down"] forState:UIControlStateHighlighted];
        
        } else {
            
            [imageBtn setBackgroundImage:[UIImage imageNamed:shareImgArray[index]] forState:UIControlStateNormal];
            [imageBtn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_down", shareImgArray[index]]] forState:UIControlStateHighlighted];

            [imageBtn bk_addEventHandler:^(id sender) {
                
                callback(shareImgArray[index]);
                if ([shareUrl isEqualToString:@""]) {
                    [self showLoadingView];
                    [self getShareLinkType:type withObj:obj completion:^(NSString *url, NSString *shareText, NSString *shareImgPath, NSError *error) {
                        [self hideLoadingView];
                        [self didClickShareButton:imageBtn shareUrl:url shareText:shareText shareImage:shareImgPath withObj:obj];
                    }];
                }
                else {
                    [self didClickShareButton:imageBtn shareUrl:shareUrl  shareText:shareText shareImage:shareImage withObj:obj];
                }
                
                if ([shareUrl isEqualToString:@"error"]) {
                    [self removeDialogImmediately];
                    [self showNetworkFailToast];
                    return;
                }
                [UIView animateWithDuration:animationDuration animations:^{
                    
                    _shareLiveView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
                    dialogBgView.alpha = 0.0f;
                }];
            } forControlEvents:UIControlEventTouchUpInside];
        }

        /* add label */
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(nextX, nextY+imageHeight , imageHeight, labelHeight)];
        label.font = [UIFont systemFontOfSize:14];
        [label setText:shareStrArray[index]];
        [label setTextColor:GRAY_COLOR];
        [label sizeToFit];
        label.textAlignment = NSTextAlignmentCenter;
        label.center = CGPointMake(imageBtn.center.x, label.center.y+4);
        
        [_shareLiveView addSubview:imageBtn];
        [_shareLiveView addSubview:label];

        nextX += imageHeight + leftMargin;
        insertIndex++;
        
        if (insertIndex % 4 == 0) {
            nextY += imageHeight + topMargin + labelHeight;
            nextX = leftMargin;
        }
    }
    
    nextY += (imageHeight + labelHeight);
    nextY += 5;

    UIButton *cancelButton = [self getCancelButton];
    cancelButton.frame = CGRectMake(leftMargin, nextY, SCREEN_WIDTH - 2 * leftMargin, btnHeight);
    
    if (cancelable) {
        [_shareLiveView addSubview:cancelButton];
        nextY += topMargin + cancelButton.frame.size.height;
    }
    
    [cancelButton bk_addEventHandler:^(id sender) {
        [UIView animateWithDuration:animationDuration animations:^{
            
            _shareLiveView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            
            dialogBgView.alpha = 0.0f;
            callback(@"cancel");
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    [dialogBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [UIView animateWithDuration:animationDuration animations:^{
            
            _shareLiveView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            
            dialogBgView.alpha = 0.0f;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
    }]];
    
    [UIView animateWithDuration:animationDuration animations:^{
        
        dialogBgView.alpha = 1.0f;
        _shareLiveView.frame = CGRectMake(0, SCREEN_HEIGHT - boxHeight, SCREEN_WIDTH, boxHeight);
    }];
}


-(void) showActionSheetUserDialogcommentver:(UserObject*)user isPublisher:(BOOL)isPublisher isLivestreamer:(BOOL)isLivestreamer withCompletion: (void (^)(NSString* selectedAction)) callback
{
    [self removeDialogImmediately];
    
    dialogBgView = [self getTopBgView];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
    dialogBgView.alpha = 0.0f;
    int leftMargin = 15;
    int topMargin =7 ;
    int btnHeight =35 ;
    float animationDuration = kDialogFadeDuration;
    float imageWidth = 60.0f;
    float idHeight = 30.0f;
    float reportbthHeight=18;
    
    UIImageView* dialogBoxView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"dialog"] resizableImageWithCapInsets:UIEdgeInsetsMake(22, 22, 22, 2)]];
    
    dialogBoxView.userInteractionEnabled = YES;
    [dialogBgView addSubview:dialogBoxView];
    
    
    int nextY = topMargin;
    
    UIButton* reportbth = [ThemeManager getGraylineBtn];
    [reportbth setTitle:LOCALIZE(@"livereport") forState:UIControlStateNormal];
    
    /* Add Buttons */
    if(![user.userID isEqualToString:MY_USER_ID]){
        [reportbth setTitleColor:GRAY_COLOR forState:UIControlStateNormal];
        reportbth.frame = CGRectMake(leftMargin, nextY+4, SCREEN_HEIGHT*1/12+5, reportbthHeight);
        nextY=topMargin+reportbthHeight+12;
        [dialogBoxView addSubview:reportbth];
        
    }else{
        nextY=topMargin;
    }
    
    
    UIButton* adminbtn = [ThemeManager getGraylineBtn];
    [adminbtn setTitle:LOCALIZE(@"adminButton") forState:UIControlStateNormal];
    if([GET_DEFAULT(IS_ADMIN) intValue] >=1&&![user.userID isEqualToString:MY_USER_ID]){
        [adminbtn setTitleColor:RED_COLOR forState:UIControlStateNormal];
        adminbtn.frame = CGRectMake(leftMargin+SCREEN_HEIGHT*1/12+5+50, nextY-reportbthHeight-8-4, SCREEN_WIDTH/3, reportbthHeight+15+4);
        nextY=topMargin+reportbthHeight+12;
        
        [dialogBoxView addSubview:adminbtn];
        
    }
    
    
    UIImageView* userImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, nextY, imageWidth, imageWidth)];
    if(user!=nil){
        [userImageView setImageWithURL:S3_THUMB_IMAGE_URL(user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
//        [userImageView my_setImageWithURL:user.picture placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s") isThumbnail:YES];
    }else{
        [userImageView setImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
    }
    [dialogBoxView addSubview:userImageView];
    userImageView.layer.masksToBounds = YES;
    userImageView.layer.cornerRadius = imageWidth/2.0f;
    
    UILabel* openIDLabel = [[UILabel alloc]initWithFrame:CGRectMake(20+imageWidth, nextY, SCREEN_WIDTH-20-imageWidth-10, idHeight)];
    if(user!=nil){
        [openIDLabel setText:user.openID];
    }else{
        [openIDLabel setText:LOCALIZE(@"user_not_exist")];
    }
    [openIDLabel setTextColor:MAIN_COLOR];
    openIDLabel.font = BOLD_FONT_WITH_SIZE(16);
    [dialogBoxView addSubview:openIDLabel];
    
    nextY += idHeight;
    
    UILabel* bioLabel = [[UILabel alloc]initWithFrame:CGRectMake(20+imageWidth, nextY, SCREEN_WIDTH-20-imageWidth-10, 0)];
    if(user!=nil){
        if(![user.bio isEqualToString:@""]){
            [bioLabel setText:user.bio];
            bioLabel.numberOfLines = 0;
            bioLabel.font = SYSTEM_FONT_WITH_SIZE(14);
            [bioLabel setTextColor:DARK_GRAY_COLOR];
            [dialogBoxView addSubview:bioLabel];
        }
    }else{
        [bioLabel setText:LOCALIZE(@"user_not_exist")];
    }
    [bioLabel sizeToFit];
    
    if (bioLabel.frame.size.height==0&&![user.userID isEqualToString:MY_USER_ID]) {
        openIDLabel.center=CGPointMake(14*SCREEN_WIDTH/18+5,topMargin+imageWidth/2+nextY/2 );
        userImageView.center=CGPointMake(14*SCREEN_WIDTH/18-openIDLabel.frame.size.width/2-imageWidth/2-5,topMargin+imageWidth/2+nextY/2);
    }
    
    float boxHeight = 0.0f;
    
    nextY += bioLabel.frame.size.height+3;
    
    if(![user.userID isEqualToString:MY_USER_ID] && user!=nil){
        
        /* Caucluate Box Height */
        if (bioLabel.frame.size.height>btnHeight) {
            boxHeight = nextY+btnHeight+topMargin+5+topMargin+reportbthHeight;
        }else{
            boxHeight = nextY+btnHeight+topMargin+btnHeight+topMargin-bioLabel.frame.size.height+10+reportbthHeight;
        }
        
        
        UIButton* resButton = [ThemeManager getGreenLineBtn];
        resButton.frame = CGRectMake(0, nextY, SCREEN_WIDTH/2-30, btnHeight);
        resButton.layer.cornerRadius = btnHeight/2.0f;
        if(bioLabel.frame.size.height>btnHeight)
        {
            resButton.center = CGPointMake(5*SCREEN_WIDTH/18, nextY+btnHeight/2+topMargin+15);
        }else if(bioLabel.frame.size.height==0){
            resButton.center = CGPointMake(5*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin+15);
        }else{
            resButton.center = CGPointMake(5*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin-bioLabel.frame.size.height/2);
        }
        [resButton setTitle:LOCALIZE(@"@Response") forState:UIControlStateNormal];
        [dialogBoxView addSubview:resButton];
        
        [resButton bk_addEventHandler:^(id sender) {
            [UIView animateWithDuration:animationDuration animations:^{
                dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
                dialogBgView.alpha = 0.0f;
            } completion:^(BOOL finished) {
                if (finished) {
                    [dialogBgView removeFromSuperview];
                    dialogBgView = nil;
                }
            }];
            
            callback(@"response");
            
        } forControlEvents:UIControlEventTouchUpInside];
        
        
        UIButton* banButton = [ThemeManager getGreenLineBtn];
        banButton.frame = CGRectMake(0, nextY, SCREEN_WIDTH/2-30, btnHeight);
        banButton.layer.cornerRadius = btnHeight/2.0f;
        if(bioLabel.frame.size.height>btnHeight)
        {
            banButton.center = CGPointMake(5*SCREEN_WIDTH/18, nextY+btnHeight/2+topMargin+15);
        }else if(bioLabel.frame.size.height==0){
            banButton.center = CGPointMake(5*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin+15);
        }else{
            banButton.center = CGPointMake(5*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin-bioLabel.frame.size.height/2);
        }
        [banButton setTitle:LOCALIZE(@"block") forState:UIControlStateNormal];
        
        
        
        
        
        [reportbth bk_addEventHandler:^(id sender) {
            
            [UIView animateWithDuration:animationDuration animations:^{
                dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
                dialogBgView.alpha = 0.0f;
            } completion:^(BOOL finished) {
                if (finished) {
                    [dialogBgView removeFromSuperview];
                    dialogBgView = nil;
                }
            }];
            callback(@"report");

        } forControlEvents:UIControlEventTouchUpInside];
        
        
        UIButton* followBtn = [ThemeManager getGreenLineBtn];
        followBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH/2-30, btnHeight);
        if(bioLabel.frame.size.height>btnHeight)
        {
            followBtn.center = CGPointMake(13*SCREEN_WIDTH/18, nextY+btnHeight/2+topMargin+15);
        }else if(bioLabel.frame.size.height==0){
            followBtn.center = CGPointMake(13*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin+15);
        }else{
            followBtn.center = CGPointMake(13*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin-bioLabel.frame.size.height/2);
        }
        
        followBtn.layer.cornerRadius = btnHeight/2.0f;
        [followBtn setTitle:LOCALIZE(@"+Follow") forState:UIControlStateNormal];
        
        [dialogBoxView addSubview:followBtn];
        
        
        
        if(isPublisher){
            resButton.hidden = YES;
            
            [dialogBoxView addSubview:banButton];
            //             followBtn.center = CGPointMake(13*SCREEN_WIDTH/18, nextY+btnHeight/2);
            
            //            followBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH-40, btnHeight);
            
            if(bioLabel.frame.size.height>btnHeight)
            {
                followBtn.center = CGPointMake(13*SCREEN_WIDTH/18, nextY+btnHeight/2+topMargin+15);
            }else if(bioLabel.frame.size.height==0){
                followBtn.center = CGPointMake(13*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin+15);
            }else{
                followBtn.center = CGPointMake(13*SCREEN_WIDTH/18, nextY+btnHeight/2+imageWidth/2+topMargin-bioLabel.frame.size.height/2);
            }
        }

        
        [followBtn bk_addEventHandler:^(id sender) {

            [UIView animateWithDuration:animationDuration animations:^{
                dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
                dialogBgView.alpha = 0.0f;
            } completion:^(BOOL finished) {
                if (finished) {
                    [dialogBgView removeFromSuperview];
                    dialogBgView = nil;
                }
            }];
            
            if (user.isFollowing>0) {
                callback(@"unfollow");
            }
            else {
                callback(@"follow");
            }
            
        } forControlEvents:UIControlEventTouchUpInside];
        
        
                [banButton bk_addEventHandler:^(id sender) {
                    
                    [UIView animateWithDuration:animationDuration animations:^{
                        dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
                        dialogBgView.alpha = 0.0f;
                    } completion:^(BOOL finished) {
                        if (finished) {
                            [dialogBgView removeFromSuperview];
                            dialogBgView = nil;
                        }
                    }];
                    if (user.isBlocked>0) {
                        callback(@"unblock");
                    }
                    else {
                        callback(@"block");
                    }
                    
                } forControlEvents:UIControlEventTouchUpInside];
                
                
        
        [banButton bk_addEventHandler:^(id sender) {
            
            [UIView animateWithDuration:animationDuration animations:^{
                dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
                dialogBgView.alpha = 0.0f;
            } completion:^(BOOL finished) {
                if (finished) {
                    [dialogBgView removeFromSuperview];
                    dialogBgView = nil;
                }
            }];
            
            if (user.isBlocked>0) {
                callback(@"unblock");
            }
            else {
                callback(@"block");
            }
            
        } forControlEvents:UIControlEventTouchUpInside];
        

                
                if(user.isFollowing>0){
                    //            followBtn.hidden = YES;
                    
                    [followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    [followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
                    [followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
                    
                    [followBtn setTitle:LOCALIZE(@"user_profile_following") forState:UIControlStateNormal];
                    
                    //            resButton.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2);
                }
                
                
                if(user.isBlocked>0){
                    //            followBtn.hidden = YES;
                    [banButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    
                    [banButton setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
                    [banButton setBackgroundImage:[[UIImage imageNamed:@"btn_green_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
                    
                    [banButton setTitle:LOCALIZE(@"blocked") forState:UIControlStateNormal];
                    
                    //            resButton.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2);
                }
                
                
                if(isLivestreamer)
                {
                    
                    resButton.hidden=YES;
                    followBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH-40, btnHeight);
                    
                    if(bioLabel.frame.size.height>btnHeight)
                    {
                        followBtn.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2+topMargin+15);
                    }else if(bioLabel.frame.size.height==0){
                        followBtn.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2+imageWidth/2+topMargin+15);
                    }else{
                        followBtn.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2+imageWidth/2+topMargin-bioLabel.frame.size.height/2);
                    }
                    
                }
        
    }else{
        boxHeight = ( nextY+topMargin ) < (imageWidth +2*topMargin)?(imageWidth +2*topMargin):( nextY+topMargin );
    }
    
    
    
    
    dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
    
    
    
    UIButton* cancelButton = [UIButton new];
    cancelButton.frame = CGRectMake(SCREEN_WIDTH-leftMargin-btnHeight, topMargin, btnHeight, btnHeight);
    [cancelButton setImage:IMAGE_FROM_BUNDLE(@"nav_cancel_black") forState:UIControlStateNormal];
    
    [cancelButton bk_addEventHandler:^(id sender) {
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    [dialogBoxView addSubview:cancelButton];
    
    if([GET_DEFAULT(IS_ADMIN) intValue] >=1 &&![user.userID isEqualToString:MY_USER_ID]){

    [adminbtn bk_addEventHandler:^(id sender) {
        
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        } completion:^(BOOL finished) {
            if (finished) {
                [dialogBgView removeFromSuperview];
                dialogBgView = nil;
            }
        }];
        
        callback(@"admin");

        
    } forControlEvents:UIControlEventTouchUpInside];
    
    [dialogBoxView addSubview:adminbtn];
    
}


    [dialogBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [UIView animateWithDuration:animationDuration animations:^{
            
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
    }]];
    
    [UIView animateWithDuration:animationDuration animations:^{
        dialogBgView.alpha = 1.0f;
        dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT-boxHeight, SCREEN_WIDTH, boxHeight);
        
    }];
}


-(void) showActionSheetLiveRemind:(NSString*)openID showedText:(NSString*)showedText  withCompletion: (void (^)(BOOL onClick))callback
{

    /* Handle Feedview animation */
    float verticalMove = 0.0f;
    UIWindow *statusBarWindow = (UIWindow *)[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"];
    if(statusBarWindow.frame.origin.y!=0){
        verticalMove = -8;
    }
    
    UIView* topView = [self getWindowView];
 
    UIImageView* dialogBoxView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"dialog3"] resizableImageWithCapInsets:UIEdgeInsetsMake(24, 24, 24, 24)]];
    dialogBoxView.userInteractionEnabled = YES;
    [topView addSubview:dialogBoxView];

    /* UI */
    UIActivityIndicatorView* progressView = [UIActivityIndicatorView new];
    [progressView setTintColor:MAIN_COLOR];
    [progressView setColor:MAIN_COLOR];
    [progressView startAnimating];
    
    
    UIImageView* userImageView = [[UIImageView alloc]initWithFrame:CGRectMake(15, STATUS_BAR_HEIGHT, 38, 38)];
    UILabel* detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(45, STATUS_BAR_HEIGHT, SCREEN_WIDTH-50, 38)];
    
    userImageView.layer.masksToBounds = YES;
    userImageView.layer.cornerRadius = userImageView.frame.size.height/2;
    userImageView.alpha  =0.0f;
    
    if(openID!=nil){
        [detailLabel setText:showedText];
    }
    detailLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [detailLabel setTextColor:MAIN_COLOR];
    detailLabel.userInteractionEnabled = YES;
    detailLabel.textAlignment = NSTextAlignmentCenter;
    
    [dialogBoxView addSubview:progressView];
    [dialogBoxView addSubview:detailLabel];
    [dialogBoxView addSubview:userImageView];
    
    float boxHeight = 45+STATUS_BAR_HEIGHT;
    dialogBoxView.frame = CGRectMake(0, -boxHeight, SCREEN_WIDTH, boxHeight+STATUS_BAR_HEIGHT);

    progressView.center = userImageView.center;
    
    if(openID!=nil){
        [API_MANAGER getUserInfo:openID completion:^(BOOL success, UserObject *userObject) {
            [progressView stopAnimating];
            if(success){
                if(![userObject.picture isEqualToString:@""]){
                    [userImageView setImageWithURL:S3_THUMB_IMAGE_URL(userObject.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_s")];
//                    [userImageView my_setImageWithURL:(userObject.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_s") isThumbnail:YES];
                }else{
                    [userImageView setImage:IMAGE_FROM_BUNDLE(@"placehold_s")];
                }
                
                progressView.hidden = YES;
                [UIView animateWithDuration:kDialogFadeDuration animations:^{
                    userImageView.alpha = 1.0f;
                }];
            }else{
                
            }
        }];
    }
    

    UITapGestureRecognizer* tapRecog = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        
        callback(YES);
        [UIView animateWithDuration:kDialogFadeDuration animations:^{
            dialogBoxView.frame = CGRectMake(0, -boxHeight+verticalMove , SCREEN_WIDTH, boxHeight);
        } completion:^(BOOL finished) {
            [dialogBoxView removeFromSuperview];
        }];
    }];

    [dialogBoxView addGestureRecognizer:tapRecog];
    dialogBoxView.userInteractionEnabled = YES;

    [UIView animateWithDuration:kDialogFadeDuration delay:0.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        dialogBoxView.frame = CGRectMake(0, verticalMove, SCREEN_WIDTH, boxHeight);
    } completion:^(BOOL finished) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if(dialogBoxView==nil)
                return;
            [UIView animateWithDuration:kDialogFadeDuration delay:0.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
                dialogBoxView.frame = CGRectMake(0, -boxHeight+verticalMove, SCREEN_WIDTH, boxHeight);
            } completion:^(BOOL finished) {
                [dialogBoxView removeFromSuperview];
            }];
        });
    }];

}

-(void) showActionSheetLivestreamDetail:(LiveStreamObject*)livestream  withCompletion: (void (^)(NSString* selectedAction)) callback
{
    [self removeDialogImmediately];
    
    dialogBgView = [self getTopBgView];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
    dialogBgView.alpha = 0.0f;
    
//    int leftMargin = 15;
    int topMargin =7 ;
    int btnHeight =35 ;
    
    float animationDuration = kDialogFadeDuration;
    float textLabelHeight = 30.0f;
    float dataLabelHeight = 60.0f;
    
    UIImageView* dialogBoxView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"dialog"] resizableImageWithCapInsets:UIEdgeInsetsMake(24, 24, 24, 24)]];
    
    dialogBoxView.userInteractionEnabled = YES;
    [dialogBgView addSubview:dialogBoxView];
    
    int nextY = topMargin;
    /* Add Label */
    
    UILabel* totalViewLabel = [self getDataLabel:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [totalViewLabel setText:INT_TO_STRING(livestream.viewerCount)];
    [totalViewLabel sizeToFit];
    totalViewLabel.center = CGPointMake(SCREEN_WIDTH/4+10, nextY + totalViewLabel.frame.size.height/2);
    [dialogBoxView addSubview:totalViewLabel];
    
    UILabel* totalLikeLabel = [self getDataLabel:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [totalLikeLabel setText:INT_TO_STRING(livestream.receivedLikeCount)];
    [totalLikeLabel sizeToFit];
    totalLikeLabel.center = CGPointMake(3*SCREEN_WIDTH/4-10, nextY + totalLikeLabel.frame.size.height/2);
    [dialogBoxView addSubview:totalLikeLabel];
    
    nextY += totalLikeLabel.frame.size.height+3;
    
    UILabel* viewsLabel = [self getTextLabel:CGRectMake(0, nextY, 0, textLabelHeight)];
    [viewsLabel setText:LOCALIZE(@"Total_Views")];
    [viewsLabel sizeToFit];
    viewsLabel.center = CGPointMake(totalViewLabel.center.x, nextY + viewsLabel.frame.size.height/2);
    [dialogBoxView addSubview:viewsLabel];

    UILabel* likesLabel = [self getTextLabel:CGRectMake(0, nextY, 0, textLabelHeight)];
    [likesLabel setText:LOCALIZE(@"Total_Likes")];
    [likesLabel sizeToFit];
    likesLabel.center = CGPointMake(totalLikeLabel.center.x, nextY + likesLabel.frame.size.height/2);
    [dialogBoxView addSubview:likesLabel];
    
    nextY += viewsLabel.frame.size.height+topMargin;
    
    UILabel* totalTimeLabel = [self getDataLabel:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [totalTimeLabel setText:[SINGLETON getTimeFormatBySecond:livestream.totalViewTime]];
    totalTimeLabel.numberOfLines = 1;
    [totalTimeLabel setMinimumScaleFactor:8.0/25];
//    totalTimeLabel.minimumFontSize = 8.;
    totalTimeLabel.adjustsFontSizeToFitWidth = YES;
    [totalTimeLabel sizeToFit];
    if(totalTimeLabel.frame.size.width>SCREEN_WIDTH/2){
        totalTimeLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH/2, textLabelHeight);
    }
    totalTimeLabel.center = CGPointMake(totalViewLabel.center.x, nextY + totalTimeLabel.frame.size.height/2);
    [dialogBoxView addSubview:totalTimeLabel];

    UILabel* durationLabel = [self getDataLabel:CGRectMake(0, nextY, 0, dataLabelHeight)];
    [durationLabel setText:[SINGLETON getTimeFormatBySecond:livestream.duration]];
    [durationLabel sizeToFit];
    durationLabel.center = CGPointMake(totalLikeLabel.center.x, nextY + durationLabel.frame.size.height/2);
    [dialogBoxView addSubview:durationLabel];
    
    nextY += totalTimeLabel.frame.size.height+3;
    
    UILabel* timesLabel = [self getTextLabel:CGRectMake(0, nextY, 0, textLabelHeight)];
    [timesLabel setText:LOCALIZE(@"Total_Duration")];
    [timesLabel sizeToFit];
    timesLabel.center = CGPointMake(totalViewLabel.center.x, nextY + timesLabel.frame.size.height/2);
    [dialogBoxView addSubview:timesLabel];
    
    UILabel* dLabel = [self getTextLabel:CGRectMake(0, nextY, 0, textLabelHeight)];
    [dLabel setText:LOCALIZE(@"Live_Duration")];
    [dLabel sizeToFit];
    dLabel.center = CGPointMake(totalLikeLabel.center.x, nextY + dLabel.frame.size.height/2);
    [dialogBoxView addSubview:dLabel];
    
    nextY += timesLabel.frame.size.height+topMargin;
    
    /* Add Buttons */
    
    float boxHeight = 0.0f;
    
    boxHeight = nextY+btnHeight+topMargin;
    
    UIButton* deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteBtn.titleLabel.font =SYSTEM_FONT_WITH_SIZE(16);
    [deleteBtn setTitleColor:ALERT_TEXT_COLOR forState:UIControlStateNormal];
    deleteBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH/3, btnHeight);
    deleteBtn.layer.cornerRadius = btnHeight/2.0f;
    [deleteBtn.layer setBorderColor:[ALERT_TEXT_COLOR CGColor]];
    deleteBtn.layer.borderWidth=1.0f;
    deleteBtn.center = CGPointMake(timesLabel.center.x, nextY+btnHeight/2);

    [deleteBtn setTitle:LOCALIZE(@"Delete_Replay") forState:UIControlStateNormal];
//    [dialogBoxView addSubview:deleteBtn];
    
    UIButton* saveBtn = [ThemeManager getGreenCircleBtn];
//    saveBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH/3, btnHeight);
    saveBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH/2, btnHeight);
//    saveBtn.center = CGPointMake(dLabel.center.x, nextY+btnHeight/2);
    saveBtn.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2);
    saveBtn.layer.cornerRadius = btnHeight/2.0f;
//    [saveBtn setTitle:LOCALIZE(@"Save_Replay") forState:UIControlStateNormal];
    [saveBtn setTitle:LOCALIZE(@"OK") forState:UIControlStateNormal];
    [dialogBoxView addSubview:saveBtn];
    
    
    [deleteBtn bk_addEventHandler:^(id sender) {
        
        callback(@"response");
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    [saveBtn bk_addEventHandler:^(id sender) {
        callback(@"follow");
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);

    
    [UIView animateWithDuration:animationDuration animations:^{
        
        dialogBgView.alpha = 1.0f;
        dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT-boxHeight, SCREEN_WIDTH, boxHeight);
    }];
}




-(void) showActionSheetCamera:(int)tag  withCompletion: (void (^)(NSString* selectedAction)) callback
{
    if (_onlyOneTap) {
        return;
    }
    __block BOOL buttonFlying = false;
    if (buttonFlying) {
        return;
    }
    buttonFlying=YES;
    _onlyOneTap=YES;
    __block BOOL onlyOneTapWithDynamic;
    onlyOneTapWithDynamic=NO;
    [self removeDialogImmediately];
    dialogBgView = [self getTopBgView];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
    dialogBgView.alpha = 0.0f;
    if (_animator==nil) {
        //创建物理仿真器，设置仿真范围，ReferenceView为参照视图
        _animator=[[UIDynamicAnimator alloc]initWithReferenceView:self.dialogBgView];
    }
    int topMargin =7 ;
    int btnHeight =35 ;
    
    float animationDuration = kDialogFadeDuration;
    
//    UIImageView* dialogBoxView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"dialog"] resizableImageWithCapInsets:UIEdgeInsetsMake(24, 24, 24, 24)]];
//    
    dialogBgView.userInteractionEnabled = YES;
    
    int nextY = topMargin;
    /* Add Label */
    
    UIButton* photobtn = [[UIButton alloc]initWithFrame:CGRectMake(30, SCREEN_HEIGHT, 75, 75)];
    [photobtn setImage:[UIImage imageNamed:@"camera"] forState:UIControlStateNormal];
    [photobtn setImage:[UIImage imageNamed:@"camera_down"] forState:UIControlStateSelected];
    [dialogBgView addSubview:photobtn];
    
    UIButton* videobtn = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2, SCREEN_HEIGHT, 75, 75)];
    [videobtn setImage:[UIImage imageNamed:@"video"] forState:UIControlStateNormal];
    [videobtn setImage:[UIImage imageNamed:@"video_down"] forState:UIControlStateSelected];
    [dialogBgView addSubview:videobtn];

    
    nextY += videobtn.frame.size.height+3;
    
    
    UIButton* livebtn = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2-37, SCREEN_HEIGHT, 75, 75)];
    [livebtn setImage:[UIImage imageNamed:@"live"] forState:UIControlStateNormal];
    [livebtn setImage:[UIImage imageNamed:@"live_down"] forState:UIControlStateSelected];
    [dialogBgView addSubview:livebtn];
    
    
    UIButton* blabbtn = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2, SCREEN_HEIGHT, 75, 75)];
    blabbtn.hidden =  YES;
    [blabbtn setImage:[UIImage imageNamed:@"group"] forState:UIControlStateNormal];
    [blabbtn setImage:[UIImage imageNamed:@"group_down"] forState:UIControlStateSelected];
    [dialogBgView addSubview:blabbtn];

    nextY += blabbtn.frame.size.height+topMargin;
    
    /* Add Buttons */
    
    float boxHeight = 0.0f;
    
    boxHeight = nextY+btnHeight+topMargin;
    
 //    [dialogBoxView addSubview:deleteBtn];
    
//    UIButton* saveBtn = [ThemeManager getGreenCircleBtn];
//    //    saveBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH/3, btnHeight);
//    saveBtn.frame = CGRectMake(0, nextY, SCREEN_WIDTH/2, btnHeight);
//    //    saveBtn.center = CGPointMake(dLabel.center.x, nextY+btnHeight/2);
//    saveBtn.center = CGPointMake(SCREEN_WIDTH/2, nextY+btnHeight/2);
//    saveBtn.layer.cornerRadius = btnHeight/2.0f;
//    //    [saveBtn setTitle:LOCALIZE(@"Save_Replay") forState:UIControlStateNormal];
//    [saveBtn setTitle:LOCALIZE(@"OK") forState:UIControlStateNormal];
//    [dialogBgView addSubview:saveBtn];
    
    
    [photobtn bk_addEventHandler:^(id sender) {
        if (buttonFlying) {
            return;
        }
        callback(@"photo");
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBgView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.animator removeAllBehaviors];
            _animator=nil;
            [dialogBgView removeFromSuperview];
            _onlyOneTap=NO;

        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    [videobtn bk_addEventHandler:^(id sender) {
        if (buttonFlying) {
            return;
        }
        callback(@"video");
        [UIView animateWithDuration:animationDuration animations:^{
            [self.animator removeAllBehaviors];
            _animator=nil;
            dialogBgView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.animator removeAllBehaviors];
            _animator=nil;
            [dialogBgView removeFromSuperview];
            _onlyOneTap=NO;

        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    [livebtn bk_addEventHandler:^(id sender) {
        if (buttonFlying) {
            return;
        }
        callback(@"live");
        
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBgView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.animator removeAllBehaviors];
            _animator=nil;
            [dialogBgView removeFromSuperview];
            _onlyOneTap=NO;

        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    [blabbtn bk_addEventHandler:^(id sender) {
        if (buttonFlying) {
            return;
        }
        callback(@"blab");
            //            dialogBgView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
        [UIView animateWithDuration:animationDuration animations:^{
            dialogBgView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
            dialogBgView.alpha = 0.0f;
        }];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.animator removeAllBehaviors];
                    _animator=nil;
            });
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            _onlyOneTap=NO;

        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    dialogBgView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    
    UITapGestureRecognizer* tapRecog = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        if (buttonFlying) {
            return;
        }
        if (onlyOneTapWithDynamic) {
            return;
        }
        onlyOneTapWithDynamic=YES;
        DLog(@"%f",SCREEN_WIDTH);
        DLog(@"%f",SCREEN_HEIGHT);

        photobtn.center = CGPointMake(SCREEN_WIDTH*1/4+10, SCREEN_HEIGHT*2/5-30);
        videobtn.center = CGPointMake(SCREEN_WIDTH*3/4-10, SCREEN_HEIGHT*2/5-30);
        livebtn.center = CGPointMake(SCREEN_WIDTH*1/2,  SCREEN_HEIGHT*3/5);
        blabbtn.center = CGPointMake(SCREEN_WIDTH*3/4-10,  SCREEN_HEIGHT*3/5);

             [self.animator removeAllBehaviors];
        UIDynamicItemBehavior *dynamicItemBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[photobtn,videobtn]];
        
        
        UIDynamicItemBehavior *dynamicItemBehavior1 = [[UIDynamicItemBehavior alloc] initWithItems:@[livebtn,blabbtn]];

        
        [dynamicItemBehavior setElasticity:0.4];    //反彈係數Default value is 0.0. Valid range is from 0.0 for no bounce upon collision, to 1.0 for completely elastic collisions.
        [dynamicItemBehavior setAllowsRotation:YES];//旋轉
        [dynamicItemBehavior setFriction:0.0];      //磨擦力Default value is 0.0, which corresponds to no friction. Use a value of 1.0 to apply strong friction. To apply an even stronger friction, you can use higher numbers.
        [dynamicItemBehavior setResistance:0];    //Default value is 0.0. Valid range is from 0.0 for no velocity damping, to CGFLOAT_MAX for complete velocity damping. If you set this property to 1.0, a dynamic item’s motion stops as soon as there is no force applied to it.
        [dynamicItemBehavior setDensity:1];    //密度質量
        [dynamicItemBehavior setAngularResistance:50];    //旋轉係數Valid range is 0 through CGFLOAT_MAX. The greater the value, the greater the angular damping and the faster rotation slows to a stop.
        
        
        
        [dynamicItemBehavior1 setElasticity:0.4];    //反彈係數
        [dynamicItemBehavior1 setAllowsRotation:YES];//旋轉
        [dynamicItemBehavior1 setFriction:0.0];      //磨擦力
        [dynamicItemBehavior1 setResistance:0];    //阻力
        [dynamicItemBehavior1 setDensity:1];    //密度質量
        [dynamicItemBehavior1 setAngularResistance:50];    //旋轉係數
        
        UISnapBehavior *snapBehavior = [[UISnapBehavior alloc] initWithItem:photobtn snapToPoint:CGPointMake(SCREEN_WIDTH*1/4+10, SCREEN_HEIGHT+100)];
        UISnapBehavior *snapBehavior_1 = [[UISnapBehavior alloc] initWithItem:videobtn snapToPoint:CGPointMake(SCREEN_WIDTH*3/4-10, SCREEN_HEIGHT+100)];
        UISnapBehavior *snapBehavior_2 = [[UISnapBehavior alloc] initWithItem:livebtn snapToPoint:CGPointMake(SCREEN_WIDTH*1/2,  SCREEN_HEIGHT+100)];
        UISnapBehavior *snapBehavior_3 = [[UISnapBehavior alloc] initWithItem:blabbtn snapToPoint:CGPointMake(SCREEN_WIDTH*3/4-10,  SCREEN_HEIGHT+100)];
        snapBehavior.damping=0.31;
        snapBehavior_1.damping=0.4;
        snapBehavior_2.damping=0.3;
        snapBehavior_3.damping=0.3;

        [self.animator removeAllBehaviors];
        
        //button drop animation
        [self.animator addBehavior:dynamicItemBehavior];
        [self.animator addBehavior:dynamicItemBehavior1];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.animator addBehavior:snapBehavior];
        [self.animator addBehavior:snapBehavior_1];
        });
        [self.animator addBehavior:snapBehavior_2];
        [self.animator addBehavior:snapBehavior_3];


        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            [self.animator removeAllBehaviors];
            _animator=nil;
            
            [dialogBgView removeFromSuperview];
            _onlyOneTap=NO;
        });    }];
    [dialogBgView addGestureRecognizer:tapRecog];
    photobtn.center = CGPointMake(SCREEN_WIDTH*1/4+10, SCREEN_HEIGHT+36);
    videobtn.center = CGPointMake(SCREEN_WIDTH*3/4-10,  SCREEN_HEIGHT+36);
    livebtn.center = CGPointMake(SCREEN_WIDTH*1/2,  SCREEN_HEIGHT+400);
    blabbtn.center = CGPointMake(SCREEN_WIDTH*3/4-10,  SCREEN_HEIGHT+400);
    UIDynamicItemBehavior *dynamicItemBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[photobtn,videobtn]];
    
    UIDynamicItemBehavior *dynamicItemBehavior1 = [[UIDynamicItemBehavior alloc] initWithItems:@[livebtn,blabbtn]];

    
    [dynamicItemBehavior setElasticity:0.4];    //反彈係數Default value is 0.0. Valid range is from 0.0 for no bounce upon collision, to 1.0 for completely elastic collisions.
    [dynamicItemBehavior setAllowsRotation:NO];//旋轉
    [dynamicItemBehavior setFriction:0.0];      //磨擦力Default value is 0.0, which corresponds to no friction. Use a value of 1.0 to apply strong friction. To apply an even stronger friction, you can use higher numbers.
    [dynamicItemBehavior setResistance:0];    //Default value is 0.0. Valid range is from 0.0 for no velocity damping, to CGFLOAT_MAX for complete velocity damping. If you set this property to 1.0, a dynamic item’s motion stops as soon as there is no force applied to it.
    [dynamicItemBehavior setDensity:1];    //密度質量
    [dynamicItemBehavior setAngularResistance:50];    //旋轉係數Valid range is 0 through CGFLOAT_MAX. The greater the value, the greater the angular damping and the faster rotation slows to a stop.
    

    
    [dynamicItemBehavior1 setElasticity:0.4];    //反彈係數
    [dynamicItemBehavior1 setAllowsRotation:NO];//旋轉
    [dynamicItemBehavior1 setFriction:0.0];      //磨擦力
    [dynamicItemBehavior1 setResistance:0];    //阻力
    [dynamicItemBehavior1 setDensity:2];    //密度質量
    [dynamicItemBehavior1 setAngularResistance:50];    //旋轉係數


    DLog(@"%@",NSStringFromCGPoint(photobtn.center));


    
    UISnapBehavior *snapBehavior = [[UISnapBehavior alloc] initWithItem:photobtn snapToPoint:CGPointMake(SCREEN_WIDTH*1/4+10, SCREEN_HEIGHT*2/5-30)];
    UISnapBehavior *snapBehavior_1 = [[UISnapBehavior alloc] initWithItem:videobtn snapToPoint:CGPointMake(SCREEN_WIDTH*3/4-10, SCREEN_HEIGHT*2/5-30)];
    UISnapBehavior *snapBehavior_2 = [[UISnapBehavior alloc] initWithItem:livebtn snapToPoint:CGPointMake(SCREEN_WIDTH*1/2,  SCREEN_HEIGHT*3/5)];
    UISnapBehavior *snapBehavior_3 = [[UISnapBehavior alloc] initWithItem:blabbtn snapToPoint:CGPointMake(SCREEN_WIDTH*3/4-10,  SCREEN_HEIGHT*3/5)];
    snapBehavior.damping=0.31;
    snapBehavior_1.damping=0.4;
    snapBehavior_2.damping=0.3;
    snapBehavior_3.damping=0.3;
//button fly animation
    [self.animator removeAllBehaviors];
    [self.animator addBehavior:dynamicItemBehavior];
    [self.animator addBehavior:dynamicItemBehavior1];
    [self.animator addBehavior:snapBehavior];
    [self.animator addBehavior:snapBehavior_1];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    [self.animator addBehavior:snapBehavior_2];
    [self.animator addBehavior:snapBehavior_3];
        buttonFlying=NO;
    });
//    DLog(@"%@",NSStringFromCGPoint(photobtn.center));


//            [UIView animateWithDuration:0.2 animations:^{
//      
//                photobtn.center = CGPointMake(SCREEN_WIDTH*1/4+10, SCREEN_HEIGHT*2/5);
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//
//              [UIView animateWithDuration:0.2 animations:^{
//                  photobtn.center = CGPointMake(SCREEN_WIDTH*1/4+10, SCREEN_HEIGHT*2/5-30);
//                livebtn.center = CGPointMake(SCREEN_WIDTH*1/4+10,  SCREEN_HEIGHT*3/5);
//                  
//              }];});
//                
//                }];
//    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.04 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//
//    [UIView animateWithDuration:0.2 animations:^{
//        videobtn.center = CGPointMake(SCREEN_WIDTH*3/4-10, SCREEN_HEIGHT*2/5);
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            
//            [UIView animateWithDuration:0.2 animations:^{
//                videobtn.center = CGPointMake(SCREEN_WIDTH*3/4-10,  SCREEN_HEIGHT*2/5-30);
//                blabbtn.center = CGPointMake(SCREEN_WIDTH*3/4-10,  SCREEN_HEIGHT*3/5);
//                
//            }];});
//        
//    }];
//    });
    
    
            dialogBgView.alpha = 0.0f;

        dialogBgView.alpha = 1.0f;
        dialogBgView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
}

-(void) showPopOverViewFromPoint:(CGPoint)startPoint withMessage:(NSString*)message
{
    [self showPopOverViewFromPoint:startPoint withMessage:message withTextColor:[UIColor blackColor]];
}




-(void) showPopOverViewFromPoint:(CGPoint)startPoint withMessage:(NSString*)message withTextColor:(UIColor*)color
{
   [self removePopImmediately];
    
    if(_popImageView==nil){
        _popImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 80, 40)];
    }else{
        [_popImageView removeFromSuperview];
        [_popImageView.layer removeAllAnimations];
        for(UIView* view in [_popImageView subviews]){
            [view removeFromSuperview];
        }
    }

    [_popImageView.layer removeAllAnimations];
    [_popImageView setImage:IMAGE_FROM_BUNDLE(@"remind")];
    
    UILabel* messageLabel = [UILabel new];
    [messageLabel setTextColor:WHITE_COLOR];
    [messageLabel setText:message];
    messageLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    messageLabel.textColor = color;
    [messageLabel sizeToFit];
    messageLabel.frame = CGRectMake(5, 5, messageLabel.frame.size.width, messageLabel.frame.size.height);
    _popImageView.frame = CGRectMake(0, 0, messageLabel.frame.size.width+10, messageLabel.frame.size.height+17);
    [_popImageView addSubview:messageLabel];
    
    [[self getWindowView] addSubview:_popImageView];
    startPoint.y += NAVI_BAR_HEIGHT+5;
    _popImageView.center = startPoint;
    _popImageView.alpha =0.0f;
    
    float popImageViewY = _popImageView.frame.origin.y;
    
    [UIView animateWithDuration:0.2 animations:^{
        
        _popImageView.alpha =1.0f;
        
    } completion:^(BOOL finished) {
        
        [UIView animateKeyframesWithDuration:kDialogFadeDuration delay:0.0 options:UIViewKeyframeAnimationOptionAutoreverse |
         UIViewKeyframeAnimationOptionRepeat animations:^{
             
             [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.3 animations:^{
                 
                 _popImageView.frame = CGRectMake(_popImageView.frame.origin.x , popImageViewY-2, _popImageView.frame.size.width, _popImageView.frame.size.height);
             }]
             ;
             [UIView addKeyframeWithRelativeStartTime:0.5 relativeDuration:0.5 animations:^{
                 _popImageView.frame = CGRectMake(_popImageView.frame.origin.x , popImageViewY-4, _popImageView.frame.size.width, _popImageView.frame.size.height);
                 
             }];
             
         } completion:nil];
        
        [UIView animateWithDuration:0.5 delay:2.0 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            _popImageView.alpha =0.0f;
        } completion:nil];
        
    }];
}

- (void)showActionSheetLoginDialogWithCompletion:(void (^)(LoginAction loginAction))callback
{
    [self removeDialogImmediately];
    
    CGFloat elementMargin = 8.0f;
    CGFloat elementHeight = 40.0f;
    
    dialogBgView = [self getTopBgView];
    dialogBgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    dialogBgView.alpha = 0.0f;
    dialogBgView.userInteractionEnabled = YES;
    
    UIImageView *dialogBoxView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"dialog"] resizableImageWithCapInsets:UIEdgeInsetsMake(22, 22, 22, 2)]];
    
    dialogBoxView.userInteractionEnabled = YES;
    [dialogBgView addSubview:dialogBoxView];
    
    UITapGestureRecognizer *tapBackgroundGR = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [UIView animateWithDuration:kDialogFadeDuration animations:^{
            dialogBgView.alpha = 0.0f;
            dialogBoxView.transform = CGAffineTransformIdentity;
            
        } completion:^(BOOL finished) {
            if (finished) {
                [dialogBgView removeFromSuperview];
            }
        }];
    }];
    [dialogBgView addGestureRecognizer:tapBackgroundGR];
    
    CGFloat yPosition = elementMargin;
    
    // Title
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(elementMargin, yPosition, SCREEN_WIDTH - 2 * elementMargin, elementHeight)];
    titleLabel.text = LOCALIZE(@"17 is awesome. Get it now!");
    titleLabel.textColor = DIALOG_TITLE_TEXT_COLOR;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = SYSTEM_FONT_WITH_SIZE(18);
    titleLabel.numberOfLines = 0;
    titleLabel.minimumScaleFactor = 0.5;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.userInteractionEnabled = NO;
    [dialogBoxView addSubview:titleLabel];
    
    yPosition += CGRectGetHeight(titleLabel.frame) + elementMargin;
    
    CGFloat buttonWidth = (SCREEN_WIDTH - 3 * elementMargin) / 2;
    
    // Sign up button
    UIButton *signUpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    signUpButton.frame = CGRectMake(elementMargin, yPosition, buttonWidth, elementHeight);
    signUpButton.backgroundColor = [UIColor blackColor];
    [signUpButton setTitle:LOCALIZE(@"sign_up") forState:UIControlStateNormal];
    [signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    signUpButton.titleLabel.numberOfLines = 1;
    signUpButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    signUpButton.titleLabel.lineBreakMode = NSLineBreakByClipping;
    [signUpButton setExclusiveTouch:YES];
    [dialogBoxView addSubview:signUpButton];
    
    [signUpButton bk_addEventHandler:^(id sender) {
        [UIView animateWithDuration:kDialogFadeDuration animations:^{
            dialogBgView.alpha = 0.0f;
            dialogBoxView.transform = CGAffineTransformIdentity;
            
        } completion:^(BOOL finished) {
            if (finished) {
                [dialogBgView removeFromSuperview];
            }
        }];
        
        callback(LoginActionSignUp);
    } forControlEvents:UIControlEventTouchUpInside];
    
    // Sign in button
    UIButton *signInButton = [UIButton buttonWithType:UIButtonTypeCustom];
    signInButton.frame = CGRectMake(2 * elementMargin + buttonWidth, yPosition, buttonWidth, elementHeight);
    signInButton.backgroundColor = [UIColor whiteColor];
    [signInButton setTitle:LOCALIZE(@"log_in") forState:UIControlStateNormal];
    [signInButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    signInButton.layer.borderWidth = 1.0;
    signInButton.layer.borderColor = [[UIColor blackColor] CGColor];
    signInButton.titleLabel.numberOfLines = 1;
    signInButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    signInButton.titleLabel.lineBreakMode = NSLineBreakByClipping;
    [signInButton setExclusiveTouch:YES];
    [dialogBoxView addSubview:signInButton];
    
    [signInButton bk_addEventHandler:^(id sender) {
        [UIView animateWithDuration:kDialogFadeDuration animations:^{
            dialogBgView.alpha = 0.0f;
            dialogBoxView.transform = CGAffineTransformIdentity;
            
        } completion:^(BOOL finished) {
            if (finished) {
                [dialogBgView removeFromSuperview];
            }
        }];
        
        callback(LoginActionSignIn);
    } forControlEvents:UIControlEventTouchUpInside];
    
    yPosition += CGRectGetHeight(signInButton.frame) + elementMargin;
    
    CGFloat longButtonWidth = SCREEN_WIDTH - 2 * elementMargin;
    
    // Social sign in button
    UIButton *socialSignInButton = [UIButton buttonWithType:UIButtonTypeCustom];
    socialSignInButton.frame = CGRectMake(elementMargin, yPosition, longButtonWidth, elementHeight);
    socialSignInButton.backgroundColor = [UIColor whiteColor];
    [socialSignInButton setTitle:LOCALIZE(@"REGISTER_FB_LOGIN") forState:UIControlStateNormal];
    [socialSignInButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [socialSignInButton setImage:[UIImage imageNamed:@"lod_facebook"] forState:UIControlStateNormal];
    socialSignInButton.layer.borderWidth = 1.0;
    socialSignInButton.layer.borderColor = [[UIColor blackColor] CGColor];
    socialSignInButton.titleLabel.numberOfLines = 1;
    socialSignInButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    socialSignInButton.titleLabel.lineBreakMode = NSLineBreakByClipping;
    [socialSignInButton setExclusiveTouch:YES];
    #ifdef  INTERNATIONAL_VERSION
    [dialogBoxView addSubview:socialSignInButton];
    #endif
    
    [socialSignInButton bk_addEventHandler:^(id sender) {
        [UIView animateWithDuration:kDialogFadeDuration animations:^{
            dialogBgView.alpha = 0.0f;
            dialogBoxView.transform = CGAffineTransformIdentity;
            
        } completion:^(BOOL finished) {
            if (finished) {
                [dialogBgView removeFromSuperview];
            }
        }];
        
        callback(LoginActionScoialSignIn);
    } forControlEvents:UIControlEventTouchUpInside];
    
    #ifdef  INTERNATIONAL_VERSION
    yPosition += CGRectGetHeight(socialSignInButton.frame) + elementMargin;
    #endif
    
    dialogBoxView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, yPosition);
    [UIView animateWithDuration:kDialogFadeDuration animations:^{
        dialogBgView.alpha = 1.0f;
        dialogBoxView.transform = CGAffineTransformTranslate(dialogBoxView.transform, 0, -yPosition);
    } completion:nil];
}

-(void) showTextAlertView:(CGPoint)startPoint withMessage:(NSString*)message
{
    UIImageView* popImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 80, 40)];
    [popImageView setImage:IMAGE_FROM_BUNDLE(@"remind")];
    
    UILabel* messageLabel = [UILabel new];
    [messageLabel setTextColor:WHITE_COLOR];
    [messageLabel setText:message];
    messageLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [messageLabel sizeToFit];
    messageLabel.frame = CGRectMake(5, 5, messageLabel.frame.size.width, messageLabel.frame.size.height);
    popImageView.frame = CGRectMake(0, 0, messageLabel.frame.size.width+10, messageLabel.frame.size.height+17);
    [popImageView addSubview:messageLabel];
    
    [[self getStatusBarWindowView] addSubview:popImageView];
    startPoint.y += NAVI_BAR_HEIGHT+5;
    popImageView.center = startPoint;
    popImageView.alpha =0.0f;
    
    float popImageViewY = popImageView.frame.origin.y;
    
    [UIView animateWithDuration:0.2 animations:^{
        
        popImageView.alpha =1.0f;
        
    } completion:^(BOOL finished) {
        
        [UIView animateKeyframesWithDuration:kDialogFadeDuration delay:0.0 options:UIViewKeyframeAnimationOptionAutoreverse |
         UIViewKeyframeAnimationOptionRepeat animations:^{
             
             [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.3 animations:^{
                 
                 popImageView.frame = CGRectMake(popImageView.frame.origin.x , popImageViewY-2, popImageView.frame.size.width, popImageView.frame.size.height);
             }]
             ;
             [UIView addKeyframeWithRelativeStartTime:0.5 relativeDuration:0.5 animations:^{
                 popImageView.frame = CGRectMake(popImageView.frame.origin.x , popImageViewY-4, popImageView.frame.size.width, popImageView.frame.size.height);
                 
             }];
             
         } completion:nil];
        
        [UIView animateWithDuration:0.5 delay:4.0 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            popImageView.alpha =0.0f;
        } completion:^(BOOL finished) {
            [popImageView removeFromSuperview];
        }];
        
    }];
    
}

-(UIButton*)getNormalButton
{
    UIButton* optionButton = [UIButton new];
    [optionButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateNormal];
    [optionButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateHighlighted];
    [optionButton setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
    optionButton.titleLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    return optionButton;
}

-(UIButton*)getDestructiveButton
{
    UIButton* optionButton = [UIButton new];
    [optionButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateNormal];
    [optionButton setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateHighlighted];
    [optionButton setTitleColor:[UIColor colorWithRed:255.0/255.0 green:50.0/255.0 blue:20.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    optionButton.titleLabel.font = BOLD_FONT_WITH_SIZE(16);
    return optionButton;
}

-(UIButton*)getCancelButton
{
    UIButton* cancelButton = [self getNormalButton];
    [cancelButton setTitle:LOCALIZE(@"Cancel") forState:UIControlStateNormal];
    [cancelButton setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateNormal];
    [cancelButton setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateHighlighted];
    [cancelButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    cancelButton.titleLabel.font = BOLD_FONT_WITH_SIZE(16);
    return cancelButton;
}

-(UIButton*)getGMButton
{
    UIButton* gmButton = [UIButton new];
    [gmButton setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateNormal];
    [gmButton setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateHighlighted];
    [gmButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    gmButton.titleLabel.font = BOLD_FONT_WITH_SIZE(16);
    return gmButton;
}

-(UILabel*)getDataLabel:(CGRect)rect
{
    UILabel* dataLabel = [[UILabel alloc]initWithFrame:rect];
    [dataLabel setTextColor:MAIN_COLOR];
    dataLabel.textAlignment = NSTextAlignmentCenter;
    dataLabel.font = BOLD_FONT_WITH_SIZE(25);
    return dataLabel;
}

-(UILabel*)getTextLabel:(CGRect)rect
{
    UILabel* textLabel = [[UILabel alloc]initWithFrame:rect];
    [textLabel setTextColor:DARK_GRAY_COLOR];
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    return textLabel;
}

-(UIView*)getTopBgView
{
    UIView* topView = [self getWindowView];
    CGRect rect = topView.frame;
    dialogBgView = [[UIView alloc] initWithFrame:rect];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
    dialogBgView.userInteractionEnabled = YES;
    [topView addSubview:dialogBgView];
    
    return dialogBgView;
}

-(void)didClickShareButton:(UIButton*)sender  shareUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage withObj:(id)obj
{
    
    switch (sender.tag) {
        case 0:
        {
           //share17  callback do it
        }
            break;
        case 1:
        {
        #ifdef INTERNATIONAL_VERSION
            [_shareManager shareFacebookWithUrl:shareUrl shareText:shareText shareImage:shareImage];
        #else
            [_shareManager shareWeiboWithUrl:shareUrl shareText:shareText shareImage:shareImage];
        #endif
        }

            break;
        case 2:
        {
        #ifdef INTERNATIONAL_VERSION
            [_shareManager shareTwitterWithUrl:shareUrl shareText:shareText shareImage:shareImage];
        #else
            [_shareManager shareWechatWithUrl:shareUrl shareText:shareText shareImage:shareImage];
        #endif
        }
            break;
        case 3:
        {
        #ifdef INTERNATIONAL_VERSION
            [_shareManager shareWeiboWithUrl:shareUrl shareText:shareText shareImage:shareImage];
        #else
            [_shareManager shareMomentsWithUrl:shareUrl shareText:shareText shareImage:shareImage];
        #endif
            
        }
            break;
        case 4:
        {
        #ifdef INTERNATIONAL_VERSION
            [_shareManager shareWechatWithUrl:shareUrl shareText:shareText shareImage:shareImage];
        #else
            [_shareManager shareQQWithUrl:shareUrl shareText:shareText shareImage:shareImage];
        #endif
        }
            break;
        case 5:
        {
        #ifdef INTERNATIONAL_VERSION
            [_shareManager shareLineWithUrl:shareUrl shareText:shareText shareImage:shareImage];
        #else
            [_shareManager shareQzoneWithUrl:shareUrl shareText:shareText shareImage:shareImage withObj:obj];
        #endif
        }
            break;
        case 6:
        {
        #ifdef INTERNATIONAL_VERSION
            [_shareManager shareWhatsappWithUrl:shareUrl shareText:shareText shareImage:shareImage];
        #else
            [_shareManager copyUrl:shareUrl];
        #endif
            
        }
            break;
        case 7:
        {
            
            [_shareManager copyUrl:shareUrl];
        }
            break;
        case 8:
        {
            [_shareManager shareMailWithUrl:shareUrl];
        }
            break;
        case 9:
        {
            [_shareManager shareMessageWithUrl:shareUrl];
        }
            break;
        default:
            break;
    }
    
}


-(void)getShareLinkType:(ShareMediaType)type withObj:(id)obj completion:(void (^)(NSString *url, NSString *shareText, NSString *shareImgPath, NSError *error))callback
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:@"website" forKey:@"$og_type"];
    [params setObject:@"app" forKey:@"$twitter_card"];
    
    switch (type) {
        case ShareMediaTypeUser:
        {
            UserObject* user = (UserObject*)obj;
            [params setObject:@"u" forKey:@"page"];
            [params setObject:user.userID forKey:@"ID"];
            
            [params setObject:[NSString stringWithFormat:LOCALIZE(@"SHARE_USER"), user.openID, @""] forKey:@"$og_title"];
            [params setObject:[NSString stringWithFormat:LOCALIZE(@"SHARE_USER"), user.openID, @""] forKey:@"$twitter_title"];
            [params setObject:S3_FILE_URL(user.picture) forKey:@"$og_image_url"];
            [params setObject:S3_FILE_URL(user.picture) forKey:@"twitter_image"];
            [params setObject:[NSString stringWithFormat:@"http://%@/%@",SHARED_URL, user.openID] forKey:@"$fallback_url"];
            break;
        }
            
        case ShareMediaTypePost:
        {
            PostObject* post = (PostObject*)obj;
            if ([post.type isEqualToString:@"video"]) {
                if (post.caption) {
                    [params setObject:[NSString stringWithFormat:LOCALIZE(@"SHARE_VIDEO_POST"), post.user.openID, post.caption] forKey:@"$og_title"];
                    [params setObject:[NSString stringWithFormat:LOCALIZE(@"SHARE_VIDEO_POST"), post.user.openID, post.caption] forKey:@"$twitter_title"];
                } else {
                    [params setObject:[NSString stringWithFormat:LOCALIZE(@"SHARE_VIDEO_POST"), post.user.openID] forKey:@"$og_title"];
                    [params setObject:[NSString stringWithFormat:LOCALIZE(@"SHARE_VIDEO_POST"), post.user.openID] forKey:@"$twitter_title"];
                }
            } else {
                if (post.caption) {
                    [params setObject:[NSString stringWithFormat:LOCALIZE(@"SHARE_PHOTO_POST"), post.user.openID, post.caption] forKey:@"$og_title"];
                } else {
                    [params setObject:[NSString stringWithFormat:LOCALIZE(@"SHARE_PHOTO_POST"), post.user.openID] forKey:@"$og_title"];
                }
            }
            [params setObject:S3_FILE_URL(post.picture) forKey:@"$og_image_url"];
            [params setObject:S3_FILE_URL(post.picture) forKey:@"twitter_image"];
            [params setObject:[NSString stringWithFormat:@"http://%@/p/%@",SHARED_URL, post.postID] forKey:@"$fallback_url"];
            
            [params setObject:@"p" forKey:@"page"];
            [params setObject:post.postID forKey:@"ID"];
            
            break;
        }
            
        case ShareMediaTypeLive:
        {
            LiveStreamObject* livestream = (LiveStreamObject*)obj;
            
            if (livestream.caption) {
                [params setObject:[NSString stringWithFormat:LOCALIZE(@"SHARE_LIVE_STREAM"), livestream.user.openID, livestream.caption] forKey:@"$og_title"];
                [params setObject:[NSString stringWithFormat:LOCALIZE(@"SHARE_LIVE_STREAM"), livestream.user.openID, livestream.caption] forKey:@"$twitter_title"];
            } else {
                [params setObject:[NSString stringWithFormat:LOCALIZE(@"SHARE_LIVE_STREAM"), livestream.user.openID] forKey:@"$og_title"];
                [params setObject:[NSString stringWithFormat:LOCALIZE(@"SHARE_LIVE_STREAM"), livestream.user.openID] forKey:@"$twitter_title"];
            }
            [params setObject:S3_FILE_URL(livestream.user.picture) forKey:@"$og_image_url"];
            [params setObject:S3_FILE_URL(livestream.user.picture) forKey:@"twitter_image"];
            [params setObject:[NSString stringWithFormat:@"http://%@/live/%@",SHARED_URL, livestream.liveStreamID] forKey:@"$fallback_url"];
            
            [params setObject:@"live" forKey:@"page"];
            [params setObject:livestream.liveStreamID forKey:@"ID"];
            
            break;
        }
            
        default:
            break;
    }
    
    Branch *branch = [Branch getInstance];
    [branch getShortURLWithParams:params andCallback:^(NSString *url, NSError *error) {
        // show the link to the user or share it immediately
        DLog(@"branch url:%@", url);
        callback(url, [params objectForKey:@"$og_title"], [params objectForKey:@"$og_image"], error);
    }];
}


-(void)showProductList:(NSArray *)products remainingWarning:(BOOL)remainingWarning withCompletion:(void (^)(int))callback
{
    [self removeDialogImmediately];
    dialogBgView = [self getTopBgView];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    dialogBgView.alpha = 0.0f;

    float boxHeight = SCREEN_HEIGHT*2/3;
    float boxWidth = SCREEN_WIDTH-80;
    float btnHeight =40;
    __block float nextY= 20;
    _iapManager=[SINGLETON iapManager];
    _iapManager.delegate=self;
    UIImageView* dialogBoxView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"btn_bk_r"] resizableImageWithCapInsets:UIEdgeInsetsMake(22, 22, 22, 22)]];
    [dialogBgView addSubview:dialogBoxView];
    
    UILabel* pointNotEnough=[[UILabel alloc]initWithFrame:CGRectMake(0, nextY, boxWidth, 30)];
    [pointNotEnough setFont:SYSTEM_FONT_WITH_SIZE(18)];
    [pointNotEnough setTextColor:WHITE_COLOR];
    pointNotEnough.textAlignment=NSTextAlignmentCenter;
    [dialogBoxView addSubview:pointNotEnough];
    if (remainingWarning) {
        [pointNotEnough setText:LOCALIZE(@"point_not_enough")];
    }else{
        [pointNotEnough setText:LOCALIZE(@"purchase_point")];
    }
    nextY+=35;
    
     _nowPointLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, nextY, boxWidth, 30)];
    [_nowPointLabel setText:[NSString stringWithFormat:@"%@ : %@",LOCALIZE(@"now_point"),GET_DEFAULT(MY_POINT) ]];
    [_nowPointLabel setFont:SYSTEM_FONT_WITH_SIZE(18)];
    [_nowPointLabel setTextColor:WHITE_COLOR];
    _nowPointLabel.textAlignment=NSTextAlignmentCenter;
    [dialogBoxView addSubview:_nowPointLabel];
    nextY+=35;
    
    int imageMargin=10;
    UIActivityIndicatorView* progressView = [UIActivityIndicatorView new];
    [progressView setTintColor:MAIN_COLOR];
    [progressView setColor:MAIN_COLOR];
    [dialogBoxView addSubview:progressView];
    
    
        UIButton* free=[[UIButton alloc]initWithFrame:CGRectMake(100, 100, boxWidth-20, btnHeight)];
        free.center=CGPointMake(boxWidth/2, nextY+btnHeight/2);
        
        [free setBackgroundImage:[[UIImage imageNamed:@"btn_wb_circle"]resizableImageWithCapInsets:UIEdgeInsetsMake(22, 22, 22, 22)]  forState:UIControlStateNormal];
        [free setBackgroundImage:[[UIImage imageNamed:@"btn_wb_circle_down"]resizableImageWithCapInsets:UIEdgeInsetsMake(22, 22, 22, 22)]  forState:UIControlStateHighlighted];
        [free setTitle:@"看廣告獲得10點" forState:UIControlStateNormal];
        [free setTitle:@"看廣告獲得10點" forState:UIControlStateHighlighted];
        [free setTintColor:WHITE_COLOR];
        free.titleLabel.textAlignment=NSTextAlignmentCenter;
        free.titleLabel.font=SYSTEM_FONT_WITH_SIZE(18);
        
        free.tag=20;
        [free addTarget:self action:@selector(didClickGetFreePoint:) forControlEvents:UIControlEventTouchUpInside];
//        [dialogBoxView addSubview:free];
//        nextY+=btnHeight+imageMargin;
         int giftPicutre=0;
        for (int index=0; index<[products count]; index++) {
            ProductObject* product=[products objectAtIndex:index];
            UIButton* buyproduct=[[UIButton alloc]initWithFrame:CGRectMake(100, 100, boxWidth-20, btnHeight)];
            buyproduct.center=CGPointMake(boxWidth/2, nextY+btnHeight/2+(btnHeight+imageMargin)*index);
            giftPicutre++;
            if (giftPicutre>7) {
                giftPicutre=0;
            }
            UIImageView* a=[[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"giftpoint_%d",giftPicutre+1]]];
            a.frame=CGRectMake(20, 10, 13, 15);
            [buyproduct addSubview:a];
            [buyproduct bk_addEventHandler:^(id sender) {
                if ([GET_DEFAULT(IS_ON_LIVESTREAMING)isEqualToNumber:@1]) {
                    [EVENT_HANDLER addEventTracking:@"ClickBuyPointInLive" withDict:nil];
                }
                callback(index);
            }forControlEvents:UIControlEventTouchUpInside];

            UILabel* point=[[UILabel alloc]initWithFrame:CGRectMake(boxWidth/3, 10, 60, 30)];
            
            [point setText:[NSString stringWithFormat:@"%dP",product.point]];
            [point setFont:SYSTEM_FONT_WITH_SIZE(18)];
            [point sizeToFit];

            if (product.bonusAmount>0) {
                point.frame=CGRectMake(boxWidth*2/5-point.frame.size.width, 3, point.frame.size.width, point.frame.size.height);
                UILabel* bouns=[[UILabel alloc]initWithFrame:CGRectMake(0, point.frame.origin.y+point.frame.size.height+2, 100, 10)];
                [bouns setText:[NSString stringWithFormat:@"(%d+%d%%)",product.point-product.bonusAmount,product.bonusPercentage]];
                [bouns setFont:SYSTEM_FONT_WITH_SIZE(14)];
                [bouns sizeToFit];
                bouns.center=CGPointMake(point.center.x, bouns.frame.origin.y+3);
                [bouns setTextColor:GRAY_COLOR];
                [buyproduct addSubview:bouns];
            }else{
                point.frame=CGRectMake(boxWidth*2/5-point.frame.size.width, 10, point.frame.size.width, point.frame.size.height);

            }
            
            UILabel* price=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, 70, 20)];
            [price setText:product.localePrice];
            [price setFont:SYSTEM_FONT_WITH_SIZE(16)];
            [price sizeToFit];
            [price setTextColor:WHITE_COLOR];
            price.frame=CGRectMake(boxWidth-40-price.frame.size.width, 10, price.frame.size.width, 20);
            [buyproduct addSubview:price];
            
            
            [point setTextColor:WHITE_COLOR];
            [buyproduct addSubview:point];
            
            [buyproduct setBackgroundImage:[[UIImage imageNamed:@"btn_wb_circle"]resizableImageWithCapInsets:UIEdgeInsetsMake(22, 22, 22, 22)]  forState:UIControlStateNormal];
            [buyproduct setBackgroundImage:[[UIImage imageNamed:@"btn_wb_circle_down"]resizableImageWithCapInsets:UIEdgeInsetsMake(22, 22, 22, 22)]  forState:UIControlStateHighlighted];
            buyproduct.tag=index;
//            [buyproduct addTarget:self action:@selector(didClickBuyProduct:) forControlEvents:UIControlEventTouchUpInside];
            [dialogBoxView addSubview:buyproduct];
        }
    dialogBoxView.userInteractionEnabled = YES;

//    [UIView animateWithDuration:kDialogFadeDuration animations:^{
//        dialogBgView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
//        dialogBgView.alpha = 0.0f;
//    }];
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kDialogFadeDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [dialogBgView removeFromSuperview];
//    });
    
    UITapGestureRecognizer* tapRecog = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        
        [UIView animateWithDuration:kDialogFadeDuration animations:^{
            dialogBgView.alpha=0.0f;
            _iapManager.delegate=nil;
            if ([GET_DEFAULT(IS_ON_LIVESTREAMING)isEqualToNumber:@1]) {
                [EVENT_HANDLER addEventTracking:@"GiveupBuyPointInLive" withDict:nil];
            }
        } completion:^(BOOL finished) {
            [dialogBoxView removeFromSuperview];
            [self removeDialogImmediately];

        }];
    }];
    boxHeight=(imageMargin+btnHeight)*[products count]+70+20+10;
    dialogBoxView.frame = CGRectMake((SCREEN_WIDTH-boxWidth)/2, (SCREEN_HEIGHT-boxHeight)/2, boxWidth, boxHeight);

    [UIView animateWithDuration:kDialogFadeDuration animations:^{
        dialogBgView.alpha = 1.0f;
        
    }];
    [dialogBgView addGestureRecognizer:tapRecog];

}

-(void)showGiftConfirm:(GiftObject *)products withCompletion:(void (^)(BOOL))callback
{
    [self removeDialogImmediately];
    dialogBgView = [self getTopBgView];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    dialogBgView.alpha = 0.0f;
    
    float boxHeight = 140;
    float boxWidth = 240;
//    float btnHeight =40;
//    __block float nextY= 20;
    UIImageView* dialogBoxView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"dialog_white"] resizableImageWithCapInsets:UIEdgeInsetsMake(22, 22, 22, 22)]];
    [dialogBgView addSubview:dialogBoxView];
    UILabel* sendConfirmLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 20, 200, 20)];
    [sendConfirmLabel setText:[NSString stringWithFormat:LOCALIZE(@"ConfrimtoSendGift"),products.name]];
    [sendConfirmLabel setTextAlignment:NSTextAlignmentCenter];
    [sendConfirmLabel setTextColor:MAIN_COLOR];
    sendConfirmLabel.numberOfLines=2;
    [sendConfirmLabel setFont:SYSTEM_FONT_WITH_SIZE(18)];
    [sendConfirmLabel sizeToFit];
    sendConfirmLabel.center=CGPointMake(boxWidth/2, sendConfirmLabel.center.y);
    [dialogBoxView addSubview:sendConfirmLabel];
    UIView* consumeView=[[UIView alloc]initWithFrame:CGRectMake(60, 60, 160, 30)];
    [dialogBoxView addSubview:consumeView];
    consumeView.center=CGPointMake(sendConfirmLabel.center.x, sendConfirmLabel.center.y+sendConfirmLabel.frame.size.height/2+15);
    UILabel* pointConsumeLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, 150, 20)];
    [pointConsumeLabel setText:LOCALIZE(@"willcharge")];
    [pointConsumeLabel setTextColor:GRAY_COLOR];
    [pointConsumeLabel setFont:SYSTEM_FONT_WITH_SIZE(16)];
    [pointConsumeLabel sizeToFit];
    [consumeView addSubview:pointConsumeLabel];
    
    UIImageView* giftIcon=[[UIImageView alloc]initWithFrame:CGRectMake(30, 50, 13, 15)];
    [giftIcon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"giftpoint_%d",rand()%7+1]]];
    giftIcon.center=CGPointMake(pointConsumeLabel.frame.origin.x+pointConsumeLabel.frame.size.width+10, pointConsumeLabel.center.y);
       dialogBoxView.userInteractionEnabled = YES;
    [consumeView addSubview:giftIcon];
    
    
    UILabel* pointLabel=[[UILabel alloc]initWithFrame:CGRectMake(giftIcon.frame.origin.x+20, pointConsumeLabel.frame.origin.y, 100, 20)];
    [pointLabel setText:[NSString stringWithFormat:LOCALIZE(@"xxP"),products.point]];
    [pointLabel setTextColor:GRAY_COLOR];
    [pointLabel setFont:SYSTEM_FONT_WITH_SIZE(16)];
    [pointLabel sizeToFit];
    [consumeView addSubview:pointLabel];
    
    
    UIView* hLine=[[UIView alloc]initWithFrame:CGRectMake(0 , 90, boxWidth, 1)];
    [dialogBoxView addSubview:hLine];
    [hLine setBackgroundColor:MAIN_COLOR];
    
    
    UIView* vLine=[[UIView alloc]initWithFrame:CGRectMake(boxWidth/2-1 , 90, 1, boxHeight-90)];
    [dialogBoxView addSubview:vLine];
    [vLine setBackgroundColor:MAIN_COLOR];
    
    UIButton* cancelButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 100, boxWidth/2, 40)];
    [cancelButton setTitle:LOCALIZE(@"Cancel") forState:UIControlStateNormal];
    [cancelButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    [dialogBoxView addSubview:cancelButton];
    [cancelButton bk_addEventHandler:^(id sender) {
        
        callback(NO);
        [UIView animateWithDuration:kDialogFadeDuration animations:^{
            dialogBgView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kDialogFadeDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            [dialogBgView removeFromSuperview];
        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    UIButton* purchaseButton=[[UIButton alloc]initWithFrame:CGRectMake(boxWidth/2, 100, boxWidth/2, 40)];
    [purchaseButton setTitle:LOCALIZE(@"OK") forState:UIControlStateNormal];
    [purchaseButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    [dialogBoxView addSubview:purchaseButton];

    [purchaseButton bk_addEventHandler:^(id sender) {
        
        callback(YES);
        [UIView animateWithDuration:kDialogFadeDuration animations:^{
            dialogBgView.alpha = 0.0f;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kDialogFadeDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [dialogBgView removeFromSuperview];
        });
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    //    [UIView animateWithDuration:kDialogFadeDuration animations:^{
    //        dialogBgView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, boxHeight);
    //        dialogBgView.alpha = 0.0f;
    //    }];
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kDialogFadeDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //        [dialogBgView removeFromSuperview];
    //    });
    
    UITapGestureRecognizer* tapRecog = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        
        [UIView animateWithDuration:kDialogFadeDuration animations:^{
            dialogBgView.alpha=0.0f;
        } completion:^(BOOL finished) {
            [dialogBoxView removeFromSuperview];
            [self removeDialogImmediately];
            
        }];
    }];

        dialogBoxView.frame = CGRectMake(40,30 , boxWidth, boxHeight);
    dialogBoxView.center=CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
    [UIView animateWithDuration:kDialogFadeDuration animations:^{
        dialogBgView.alpha = 1.0f;
        
    }];
    [dialogBgView addGestureRecognizer:tapRecog];
    
}

-(void)didClickGetFreePoint:(UIButton*)button
{
    
}

-(void)pointRefresh
{
    [_nowPointLabel setText:[NSString stringWithFormat:@"%@ : %@",LOCALIZE(@"now_point"),GET_DEFAULT(MY_POINT) ]];
}



@end
