//
//  MyScene.h
//  story17
//
//  Created by POPO Chen on 7/24/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>


@protocol MySceneDelegate <NSObject>
-(void)playAnimate;
@end

@interface MyScene : SKScene
@property (nonatomic) BOOL animating;

-(void)showheartFrompout:(CGPoint)point selectedColor:(int)color;
-(void)showAnimate:(CGPoint)point imgarray:(NSArray*)imgarray perFramDuraiton:(float)perFramDuration startPoint:(int)imageStart withCompletion:(void (^)(BOOL Done))callback;


-(void)showAnimation1FromPoint:(CGPoint)point;
-(void)showAnimation2FromPoint;
-(void)showAnimation3FromPoint;

@end
