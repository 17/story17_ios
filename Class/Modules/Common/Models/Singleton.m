#import "Singleton.h"
#import "UIAlertView+NSCookbook.h"
#import "FeedViewController.h"
#import "NotificationViewController.h"
#import "SearchViewController.h"
#import "UserProfileViewController.h"
#import "UserSuggestionViewController.h"
#import "CameraViewController.h"
#import "LiveStreamViewController.h"

@implementation Singleton

@synthesize cdnBaseUrl;
@synthesize chunkcdnBaseUrl;

+(Singleton*) sharedInstance
{
    static dispatch_once_t once;
    
    static Singleton *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });

    return sharedInstance;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        self.imageCache = [[NSCache alloc] init];

        self.defaults = [NSUserDefaults standardUserDefaults];
        
        self.apiManager = [[ApiManager alloc] init];
        self.revenueDataManager = [[RevenueDataManager alloc] init];
        self.connectionManager = [[ConnectionManager alloc]init];
        
        self.minihourMinutedateFormatter = [[NSDateFormatter alloc]init];
        [self.minihourMinutedateFormatter setDateFormat:@"h:mm"];
        
        self.hourMinutedateFormatter = [[NSDateFormatter alloc] init];
        [self.hourMinutedateFormatter setDateFormat:@"H:mm"];
        
        self.dayOfMonthWithweekdateFormatter = [[NSDateFormatter alloc] init];
        [self.dayOfMonthWithweekdateFormatter setDateFormat:@"M/d"];
        
        self.dayOfWeekdateFormatter = [[NSDateFormatter alloc] init];
        [self.dayOfWeekdateFormatter setDateFormat:@"EEE"];
        
        self.dayofYearFormatter = [[NSDateFormatter alloc] init];
        [self.dayofYearFormatter setDateFormat:@"M/d (eee)"];
        
        #ifdef INTERNATIONAL_VERSION
            _currenctRateDict = @{@"TWD":@31,@"CNY":@6,@"MOP":@7.6,@"HKD":@7.4,@"SGD":@1.3,@"MYR":@3.9,@"USD":@1,@"JPY":@124};   // for currencyRate
        #else
            _currenctRateDict = @{@"CNY":@6,@"MOP":@7.6,@"HKD":@7.4,@"SGD":@1.3,@"MYR":@3.9,@"USD":@1,@"JPY":@124};   // for currencyRate
        #endif
    }
    
    self.countryData = CREATE_MUTABLE_ARRAY;
    self.country2File = CREATE_MUTABLE_DICTIONARY;
    
    self.countryCode2PhoneCode = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countryCode2PhoneCode" ofType:@"plist"]];
    self.country2File = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"country2file" ofType:@"plist"]];

    for(NSString* countryCode in [self.countryCode2PhoneCode allKeys]){
        
        NSString* phoneCode = [self.countryCode2PhoneCode objectForKey:countryCode];
        NSMutableDictionary* infoDict = CREATE_MUTABLE_DICTIONARY;
        infoDict[@"country"] = countryCode;
        
        // in 17app, countryCode means phoneCode
        infoDict[@"countryCode"] = phoneCode;

        [self.countryData addObject:infoDict];
        
    }

    _heartImage = [UIImage imageNamed:@"colorheart_1"];

    return self;
}

-(void) setupMainTabBarViewController
{
    NSDictionary * textAttributes =    [NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor colorWithRed:173.0/255.0 green:173.0/255.5 blue:173.0/255.0 alpha:1.0], UITextAttributeTextColor,
                                        [UIColor clearColor], UITextAttributeTextShadowColor,
                                        [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                        [UIFont fontWithName:@"Rok" size:0.0], UITextAttributeFont,nil];
    
    NSDictionary * highLightTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                              MAIN_COLOR, UITextAttributeTextColor,
                                              [UIColor clearColor], UITextAttributeTextShadowColor,
                                              [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                              BOLD_FONT_WITH_SIZE(10.0), UITextAttributeFont,nil];
    
    FeedViewController* feedVC = [[FeedViewController alloc] init];
    
//    _feedVC = feedVC;
    UINavigationController* feedNav = [[UINavigationController alloc]initWithRootViewController:feedVC];
    UITabBarItem *tab1 = [[UITabBarItem alloc] initWithTitle:@"" image:[[UIImage imageNamed:@""]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@""]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab1 setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
    [tab1 setTitleTextAttributes:highLightTextAttributes forState:UIControlStateSelected];
    tab1.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);

    [feedNav setTabBarItem:tab1];

    SearchViewController* searchVC = [[SearchViewController alloc] init];
    UINavigationController* searchNav = [[UINavigationController alloc]initWithRootViewController:searchVC];
    UITabBarItem *tab2 = [[UITabBarItem alloc] initWithTitle:@"" image:[[UIImage imageNamed:@""]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@""]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab2 setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
    [tab2 setTitleTextAttributes:highLightTextAttributes forState:UIControlStateSelected];
    [searchNav setTabBarItem:tab2];
    tab2.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    UIViewController* feed2VC = [[UIViewController alloc] init]; // for add a camera btn

    NotificationViewController* notifVC = [[NotificationViewController alloc] init];
    notifVC.enterMode=ENTER_MODE_SELF_FOLLOWING;
    UINavigationController* notifNav = [[UINavigationController alloc]initWithRootViewController:notifVC];
    UITabBarItem *tab4 = [[UITabBarItem alloc] initWithTitle:@"" image:[[UIImage imageNamed:@""]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@""]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab4 setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
    [tab4 setTitleTextAttributes:highLightTextAttributes forState:UIControlStateSelected];
    [notifNav setTabBarItem:tab4];
    tab4.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    UserProfileViewController* userVC = [[UserProfileViewController alloc] init];
    UINavigationController* userNav = [[UINavigationController alloc]initWithRootViewController:userVC];
    UITabBarItem *tab5 = [[UITabBarItem alloc] initWithTitle:@"" image:[[UIImage imageNamed:@""]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@""]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab5 setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
    [tab5 setTitleTextAttributes:highLightTextAttributes forState:UIControlStateSelected];
    [userNav setTabBarItem:tab5];
    tab5.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    userVC.user = [SINGLETON getSelfUserObject];

    /* Badge View */
    _badgeImageView = [[UIImageView alloc]initWithFrame:CGRectMake(4*SCREEN_WIDTH/5-27, 10, 8,8)];
    [_badgeImageView setImage:[UIImage imageNamed:@"badge_red"]];
    
    self.mainTabBarViewController = [[CustomTabbarController alloc] init];
    self.mainTabBarViewController.viewControllers = @[feedNav,searchNav,feed2VC,notifNav,userNav];
    [[[[self.mainTabBarViewController tabBar]items]objectAtIndex:2]setEnabled:FALSE];
    [self.mainTabBarViewController.tabBar addSubview:_badgeImageView];
    [self.mainTabBarViewController.tabBar setBackgroundColor:WHITE_COLOR];

    [Flurry logAllPageViews: self.mainTabBarViewController];
    
    [self synchronizeTokenToServer];
    [Flurry setUserID:MY_USER_ID];
    
    [SINGLETON customizeInterface];
    [self recuriveGetIpGeoLocation];

    if(REVENUE_MANAGER !=NULL){
        [REVENUE_MANAGER pullData:YES];
    }
    
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        if([SSKeychain passwordForService:BUNDLE_ID account:@"isFreeze" error:nil]!=nil) {
            
            [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"freeze_title") message:LOCALIZE(@"freeze_message") buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                exit(0);
            }];
        }
        [SINGLETON generateUDIDToKeychain];
    });
    [API_MANAGER getSelfInfo:^(BOOL success) {
    
    }];
    [API_MANAGER checkingSandBox:^(BOOL success) {}];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [UIApplication sharedApplication].keyWindow.rootViewController = self.mainTabBarViewController;
    
}

-(void)mainTabbarRelease
{

    // remove all observer in maintabbarcontrller (dealloc not called)
    
    for(UIViewController* vc in self.mainTabBarViewController.viewControllers) {
        if([vc isKindOfClass:[UINavigationController class]]) {
            [NOTIFICATION_CENTER removeObserver:((UINavigationController*)vc).viewControllers[0]];
        }
    }
    
    self.mainTabBarViewController.viewControllers = @[];
    self.mainTabBarViewController=nil;
}


-(void)mainTabbarSetSelectedIndex:(int)selectedIndex
{
    [self.mainTabBarViewController setSelectedIndex:selectedIndex];
}



# pragma mark - Date Formatter
-(NSString *)timestampToMessageTimeString:(int) timestamp
{
    return [self.hourMinutedateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:timestamp]];
}

-(NSString *)getDateAndDayForTimestamp:(int) timestamp
{
    NSString *dateString;
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    NSDate *today = [NSDate date];
    
    int timeInterval =  [today timeIntervalSince1970]-timestamp;
    
    if (timeInterval < 86400) {
        dateString = LOCALIZE(@"Today");
    }
    else if (timeInterval < 604800){
        dateString = [self.dayOfWeekdateFormatter stringFromDate:date];
    }
    else {
        dateString = [self.dayOfMonthWithweekdateFormatter stringFromDate:date];
    }
    
    return  [NSString stringWithFormat:@"%@  %@",dateString,[self.dayOfMonthWithweekdateFormatter stringFromDate:date]];
}

-(NSString *)getDayAndTimeForTimestamp:(int) timestamp
{
    NSString *dateString;

    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    NSDate *today = [NSDate date];
    
    int timeInterval =  [today timeIntervalSince1970]-timestamp;
    
    if (timeInterval < 86400) {
        dateString = LOCALIZE(@"Today");
    }
    else if (timeInterval < 604800){
        dateString = [self.dayOfWeekdateFormatter stringFromDate:date];
    }
    else {
        dateString = [self.dayOfMonthWithweekdateFormatter stringFromDate:date];
    }
    
    return  [NSString stringWithFormat:@"%@  %@",dateString,[self.hourMinutedateFormatter stringFromDate:date]];
}

-(NSString *)timestampToChatThreadTimeString:(int)timestamp
{
    int timeInterval =  CURRENT_TIMESTAMP-timestamp;
    
    NSString* today = [self.dayOfMonthWithweekdateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:CURRENT_TIMESTAMP]];
    NSString* date = [self.dayOfMonthWithweekdateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:timestamp]];
    
    // check if is on the same day!
    if([today isEqualToString:date]) {
        return [self.hourMinutedateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:timestamp]];
    }
    
    // check if is within one week
    if(timeInterval<604800) {
        return [self.dayOfWeekdateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:timestamp]];
    } else {
        return [self.dayOfMonthWithweekdateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:timestamp]];
    }
}

-(NSString*) getElaspsedTimeString:(int) timestamp
{
    int currentTime = (int) ([[NSDate date] timeIntervalSince1970]);
    int timeElapsed = currentTime - timestamp;
    
    if(timeElapsed<=60) {
        return LOCALIZE(@"just_now");
    }
    
    if(timeElapsed<=3600) {
        return [NSString stringWithFormat:LOCALIZE(@"mins_ago_placeholder"), timeElapsed/60];
    }
    
    if(timeElapsed<=86400) {
        return [NSString stringWithFormat:LOCALIZE(@"hrs_ago_placeholder"), timeElapsed/3600];
    }
    
    if(timeElapsed<=86400*30) {
        return [NSString stringWithFormat:LOCALIZE(@"days_ago_placeholder"), timeElapsed/86400];
    }
    
    if(timeElapsed<=86400*360) {
        return [NSString stringWithFormat:LOCALIZE(@"months_ago_placeholder"), timeElapsed/(86400*30)];
    }
    
    return [NSString stringWithFormat:LOCALIZE(@"yrs_ago_placeholder"), timeElapsed/(86400*360)];
}

# pragma mark - Image Helper
-(UIImage*) loadCachedImage: (NSString*) imageFileName withMethod:(int) method
{
    if(imageFileName==nil || [[NSString stringWithFormat:@"%@",imageFileName]length]<1){
        return nil;
    }
    NSObject* cachedImage = [self.imageCache objectForKey:imageFileName];
    
    if(cachedImage!=nil) {
        return (UIImage*) cachedImage;
    }
    
    UIImage* image = nil;
    
    if(method==LOAD_FROM_BUNDLE) {
        image = [UIImage imageNamed:imageFileName];
    } else if(method==LOAD_FROM_DOCUMENTS) {
        image = [UIImage imageWithContentsOfFile:GET_LOCAL_FILE_PATH(imageFileName)];
    }
    if(image!=nil){
        [self.imageCache setObject:image forKey:imageFileName];
    }
    return image;
}

-(UIImage*) loadCachedImage: (NSString*) imageFileName withEdgeInset:(UIEdgeInsets) edgeInsets withMethod:(int) method
{
    if(imageFileName==nil || [[NSString stringWithFormat:@"%@",imageFileName]length]<1){
        return nil;
    }

    
    NSString* key = [NSString stringWithFormat:@"%@_WITH_EDGE_INSETS", imageFileName];
    
    NSObject* cachedImage = [self.imageCache objectForKey:key];
    
    if(cachedImage!=nil) {
        return (UIImage*) cachedImage;
    }
    
    UIImage* image = nil;
    
    if(method==LOAD_FROM_BUNDLE) {
        image = [[UIImage imageNamed:imageFileName] resizableImageWithCapInsets:edgeInsets];
    } else if(method==LOAD_FROM_DOCUMENTS) {
        image = [[UIImage imageWithContentsOfFile:GET_LOCAL_FILE_PATH(imageFileName)] resizableImageWithCapInsets:edgeInsets];
    }
    
    if(image!=nil){
        [self.imageCache setObject:image forKey:key];
    }
    return image;
}


- (void)saveImageWithWhiteBackgroundWithoutCompleteMessage:(UIImage*)image
{
    UIImage* drawingWithWhiteBG = [UIImage imageWithData:UIImageJPEGRepresentation(image, 1.0)];
    UIImageWriteToSavedPhotosAlbum(drawingWithWhiteBG, self, nil, nil );
}

-(UIImage*) getCompressedImage:(UIImage*) originalImage withTargetSize:(float) targetSize
{
    double originalWidth = originalImage.size.width;
    double originalHeight = originalImage.size.height;
    double scaleFactor = sqrtf(targetSize/originalWidth/originalHeight);
    
    double compressedWidth = originalWidth * scaleFactor;
    double compressedHeight = originalHeight * scaleFactor;
    
    CGSize imageSize = CGSizeMake(compressedWidth, compressedHeight);
    UIGraphicsBeginImageContext(imageSize);
    [originalImage drawInRect:CGRectMake(0, 0, compressedWidth+1, compressedHeight+1)];
    UIImage* compressedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return compressedImage;
}

-(void) saveImageWithThumbnailImage:(UIImage *)sourceImage andFileName:filename
{
    NSString* filenameThumbnail = [NSString stringWithFormat:@"THUMBNAIL_%@",filename];
    
    UIImage* compressedImageLarge = [self getCompressedImage:sourceImage withTargetSize:NORMAL_IMAGE_COMPRESSION_TARGET_SIZE];
    UIImage*  compressedImageNormal = [self getCompressedImage:compressedImageLarge withTargetSize:THUMBNAIL_IMAGE_COMPRESSION_TARGET_SIZE];
    NSData* imageDataNormal = UIImageJPEGRepresentation(compressedImageNormal, 0.6f);
    NSData* imageDataLarge = UIImageJPEGRepresentation(compressedImageLarge, 0.6f);
    
    [imageDataLarge writeToFile:GET_LOCAL_FILE_PATH(filename) atomically:YES];
    [imageDataNormal writeToFile:GET_LOCAL_FILE_PATH(filenameThumbnail) atomically:YES];
}

- (UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    CGSize size = image.size;
    CGRect rect;
    if(size.height>size.width){
        rect = CGRectMake(0, (size.height-size.width)/2, size.width, size.width);
    }else if(size.height<size.width){
        rect = CGRectMake((size.width-size.height)/2, 0, size.height, size.height);
    }else{
        rect = CGRectMake(0, 0, size.width, size.width);
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], rect);
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    
    return img;
}

- (UIImage *)rotateImage:(UIImage*)image byDegree:(CGFloat)degrees
{
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,image.size.width, image.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(degrees * M_PI / 180);
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, degrees*M_PI/180);
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-image.size.width / 2, -image.size.height / 2, image.size.width, image.size.height), [image CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)generateUDIDToKeychain
{
    NSString *uuid = [SSKeychain passwordForService:BUNDLE_ID account:@"uuid" error:nil];

    if(uuid==nil){
        [SSKeychain setPassword:[self getUUID] forService:BUNDLE_ID account:@"uuid"];
    }
}

# pragma mark - SSKeychain Helper
-(void)saveFreezeFile
{
    NSString *isFreeze = [SSKeychain passwordForService:BUNDLE_ID account:@"isFreeze" error:nil];
    if(isFreeze==nil){
        [SSKeychain setPassword:IS_FREEZED forService:BUNDLE_ID account:@"isFreeze"];
    }
}

-(BOOL) hasRated
{
    NSString* key = [NSString stringWithFormat:@"Rated Version %@", [NSBundle mainBundle].infoDictionary[@"CFBundleVersion"]];
    
    return [SSKeychain passwordForService:BUNDLE_ID account:key error:nil]!=nil;
}

-(void) rateComplete
{
    NSString* key = [NSString stringWithFormat:@"Rated Version %@", [NSBundle mainBundle].infoDictionary[@"CFBundleVersion"]];

    [SSKeychain setPassword:@"1" forService:BUNDLE_ID account:key];
}

# pragma mark - Helper
-(void)playShortSound:(NSString*)filePath
{
    SystemSoundID sound;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:filePath],&sound);
    AudioServicesPlaySystemSound(sound);
}

-(void)vibrate
{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

-(void)reloadAllBadge
{
    if([GET_DEFAULT(NOTIF_BADGE) intValue]!=0  || [GET_DEFAULT(LIKE_BADGE) intValue]!=0 || [GET_DEFAULT(COMMENT_BADGE) intValue]!=0 || [GET_DEFAULT(MESSGAE_BADGE) intValue]!=0){
        self.badgeImageView.hidden = NO;
    }else{
        self.badgeImageView.hidden = YES;
    }
}

-(void) sizeToFitHeight:(UILabel*) label maxWidth:(float) maxWidth
{
    CGSize size =  [label.text boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName: label.font}
                                            context:nil].size;
    CGRect labelFrame = label.frame;
    labelFrame.size.height = size.height;
    label.frame = labelFrame;
}

-(void)synchronizeTokenToServer
{
    if([MY_USER_ID isEqualToString:@""] || [[DEFAULTS objectForKey:TOKEN] isEqualToString:@""]) {
        return;
    }

    NSDictionary *dict = @{@"pushToken":[DEFAULTS objectForKey:TOKEN]};
    [API_MANAGER updateUserInfo:dict  fetchSelfInfo:NO  completion:^(BOOL success) {
        if (success) {
        }
    }];
}

-(NSString*) getUUID
{
    return [NSString stringWithFormat:@"%@", [[[NSProcessInfo processInfo] globallyUniqueString] substringToIndex:36]];
}

-(NSString*) generateRandomFileNameWithExtension:(NSString*) fileExtension
{
    NSString* filename = [NSString stringWithFormat:@"%@.%@", [self getUUID], fileExtension];
    
    return filename;
}

-(void) goToAppStore
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat: @"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8", APP_ID]]];
}

-(void) clearUserDefaults
{
    NSDictionary * dict = [DEFAULTS dictionaryRepresentation];
    
    for (id key in dict) {
        if(![key isEqualToString:TOKEN]){
            [DEFAULTS removeObjectForKey:key];
        }
    }
    
    [DEFAULTS synchronize];
}

-(void) initializeDefaults
{
    [DEFAULTS registerDefaults:@{
                                 USER_ID: @"",
                                 DEVICE_ID: @"",
                                 APP_OPEN_TIMES:@0,
                                 SESSION_ID:@"",
                                 ACCESS_TOKEN: @"",
                                 TOKEN: @"",
                                 FACEBOOK_ID: @"",
                                 SAVE_PHOTO: @1,
                                 PICTURE:@"",
                                 CAMERA_FLASH_MODE: CAMERA_FLASH_MODE_OFF,
                                 CAMERA_GRID_MODE: CAMERA_GRID_MODE_OFF,
                                 UPDATE_FEED_TIME:@0,
                                 UPDATE_SEARCH_TIME:@0,
                                 UPDATE_NOTIF_TIME:@0,
                                 UPLOAD_FAILED_FILES:@"[]",
                                 DOWNLOAD_VIDEO_FILES:@"[]",
                                 GIFTMODULESTATE:@0,
                                 PAYPALEMAIL:@"",
                                 ALIPAYPHONE:@"",
                                 USER_OPEN_ID:@"",
                                 USER_NAME:@"",
                                 PRIVACY_MODE:@"",
                                 BIO:@"",
                                 PICTURE:@"",
                                 WEBSITE:@"",
                                 AGE:@18,
                                 GENDER:@"",
                                 IS_VERIFIED:@0,
                                 FOLLOWER_COUNT:@0,
                                 FOLLOWING_COUNT:@0,
                                 POST_COUNT:@0,
                                 REPOST_COUNT:@0,
                                 LIKED_POST_COUNT:@0,
                                 EMAIL:@"",
                                 COUNTRY_CODE:@"",
                                 PHONE_NUM:@"",
                                 IS_ADMIN:@0,
                                 IS_FREEZED:@0,
                                 PUSH_LIKE:@1,
                                 PUSH_COMMENT:@1,
                                 PUSH_TAG:@1,
                                 PUSH_FOLLOW:@1,
                                 PUSH_FRIEND_JOIN:@1,
                                 PUSH_FRIEND_REQUEST:@1,
                                 PUSH_FRIEND_FIRST_POST:@1,
                                 PUSH_LIVESTREAM_NOTIF:@1,
                                 PUSH_RESTREAM_NOTIF:@1,
                                 PUSH_FRIEND_REQUEST:@1,
                                 PUSH_SYSTEM_NOTIF:@1,
                                 PUSH_FOLLOW_REQUEST:@1,
                                 NOTIF_BADGE:@0,
                                 SYSTEM_NOTIF_BADGE:@0,
                                 LIKE_BADGE:@0,
                                 COMMENT_BADGE:@0,
                                 MESSGAE_BADGE:@0,
                                 BANK_NAME:@"",
                                 BANK_ADDRESS:@"",
                                 ACCOUNT_NUMBER:@"",
                                 BENEFICIARY_NAME:@"",
                                 SWIFT_CODE:@"",
                                 ROUNTING_NUMBER:@"",
                                 BANK_COUNTRY:@"",
                                 BRANCH_NAME:@"",
                                 ID_CARD_NUMBER:@"",
                                 ID_CARD_NAME:@"",
                                 BANK_CODE:@"",
                                 BANK_BRANCHCODE:@"",
                                 ID_CARD_FRONT_PICTURE:@"",
                                 ID_CAR_BACK_PICTURE:@"",
                                 PASSBOOK_PICTURE:@"",
                                 BANK_ACCOUNT_VERIFIED:@0,
                                 TOTAL_REVENUE_EARNED:@0,
                                 RECEIVED_LIKE_COUNT:@0,
                                 OPENID_MODIFYTIME:@0,
                                 IP_COUNTRY: @"",
                                 CURRENCY_RATE: @"USD",
                                 CURRENCY_RATE_SETTED: @0,
                                 MY_USER_IP:@"",
                                 IS_ON_LIVESTREAMING:@0,
                                 USER_IP:@"[]",
                                 VERIFIEDPHONE:@"",
                                 IN_SANDBOX:@"0",
                                 IS_FESTIVAL:@"0",
                                 MY_POINT:@"0",
                                 NOTLIVESTREAMING:@"1",
                                 GIFT_ZIP: @[],
                                 UPDATE_APPLINK: @"",
                                 REMOVE_CACHED_INFO:@0,
                                 LOGIN_MODE: @0,
                                 QQ_ID: @"",
                                 QQ_TOKEN: @"",
                                 WECHAT_TOKEN: @"",
                                 WECHAT_USER_ID: @"",
                                 WEIBO_USER_ID: @"",
                                 WEIBO_ACCESSTOKEN: @""
                                 }];
    [DEFAULTS synchronize];
}

-(UserObject*) getSelfUserObject
{
    static UserObject* selfUserObject = nil;
    if (selfUserObject == nil){
        selfUserObject = [[UserObject alloc] init];
    }

    selfUserObject.userID = GET_DEFAULT(USER_ID);
    selfUserObject.openID = GET_DEFAULT(USER_OPEN_ID);
    selfUserObject.name = GET_DEFAULT(USER_NAME);
    selfUserObject.picture = GET_DEFAULT(PICTURE);

    selfUserObject.website = GET_DEFAULT(WEBSITE);
    selfUserObject.gender = GET_DEFAULT(GENDER);
    selfUserObject.bio = GET_DEFAULT(BIO);
    selfUserObject.age = [GET_DEFAULT(AGE) intValue];
    selfUserObject.isVerified = [GET_DEFAULT(IS_VERIFIED) intValue];
    selfUserObject.followerCount = [GET_DEFAULT(FOLLOWER_COUNT) intValue];
    selfUserObject.followingCount = [GET_DEFAULT(FOLLOWING_COUNT) intValue];
    selfUserObject.postCount = [GET_DEFAULT(POST_COUNT) intValue];
    selfUserObject.repostCount = [GET_DEFAULT(REPOST_COUNT) intValue];
    selfUserObject.likePostCount = [GET_DEFAULT(LIKED_POST_COUNT) intValue];
    selfUserObject.privacyMode = GET_DEFAULT(PRIVACY_MODE);
    selfUserObject.receivedLikeCount = [GET_DEFAULT(RECEIVED_LIKE_COUNT) intValue];
    selfUserObject.phoneNumber = GET_DEFAULT(PHONE_NUM);

    return selfUserObject;
}

-(NSString*) dateToString:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    return [formatter stringFromDate:date];
}

-(void) customizeInterface
{
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               WHITE_COLOR,NSForegroundColorAttributeName,
                                               [UIFont systemFontOfSize:18.0], NSFontAttributeName, nil];
    [[UINavigationBar appearance] setBarTintColor: BLACK_COLOR];
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    [[UINavigationBar appearance] setTintColor:WHITE_COLOR];
    
//    [[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"title.png"]
//                                                      resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch] forBarMetrics:UIBarMetricsDefault];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:WHITE_COLOR];
    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:WHITE_COLOR];
}

-(UIColor*) getRandomColor
{
    CGFloat hue = ( arc4random() % 256 / 256.0 );
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];

    return color;

}

-(NSString*)getCountNumNormalization:(int)count
{
    
    if(count<1000)
        return [NSString stringWithFormat:@"%d",count];
    
    if(count>=1000 && count<10000){
        float nor = count/1000.;
        return [NSString stringWithFormat:@"%.2lfk",nor];
    }
    
    if(count>=10000 && count<pow(10,6)){
        float nor = count/pow(10,3);
        return [NSString stringWithFormat:@"%.1lfk",nor];
    }
    
    if(count>=pow(10,6) && count<pow(10,9)){
        float nor = count/pow(10,6);
        return [NSString stringWithFormat:@"%.2lfm",nor];
    }
    
    float nor = count/pow(10,9);
    return [NSString stringWithFormat:@"%.2lfb",nor];
}


-(NSArray*)getAgeRangeItem
{
    if(_ageArray!=NULL){
        return _ageArray;
    }
    
    NSMutableArray* ageArray = CREATE_MUTABLE_ARRAY;
    
    for(int i=AGE_MIN;i<=AGE_MAX;i++){
        [ageArray addObject:[NSString stringWithFormat:@"%d",i ]];
    }
    _ageArray = [ageArray copy];
    return _ageArray;
}

-(NSArray*)getTagedArray:(NSString*)inputStr
{
    if(_tagArray==NULL){
        _tagArray =  CREATE_MUTABLE_ARRAY;
    }

    [_tagArray removeAllObjects];
    
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"#[a-zA-Z0-9-_]+"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSArray* matches = [regex matchesInString:inputStr options:0 range: NSMakeRange(0, [inputStr length])];
    
//    DLog(@"%@",matches);
    for (NSTextCheckingResult* match in matches) {
        NSString* matchText = [inputStr substringWithRange:[match range]];
        [_tagArray addObject:matchText];
    }
    
    return [_tagArray copy];
}


-(void)gotoSystemSettings
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

-(int)getTimezone{
    
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    NSDate* sourceDate1 = [NSDate date];
    NSInteger localOffset = [timeZone secondsFromGMTForDate:sourceDate1];
    NSInteger utcOffset = [utcTimeZone secondsFromGMTForDate:sourceDate1];
    NSTimeInterval interval1 = localOffset-utcOffset;
    return (int)interval1;
}


-(void)reloadFollowStatus
{
    [DEFAULTS setObject:@0 forKey:UPDATE_FEED_TIME];
    [DEFAULTS setObject:@0 forKey:UPDATE_NOTIF_TIME];
    [DEFAULTS synchronize]; 
}

#pragma mark - VideoPlayerStatus
- (void)startVideoAVPlayerForCollectionView:(UICollectionView*)collectionView
{
    /* playing video
     assume at most 2 visible cells in the screen at a time
     */
    NSMutableArray* visibleVideoCells = CREATE_MUTABLE_ARRAY;
    NSMutableArray* visibleLiveCells = CREATE_MUTABLE_ARRAY;
    for (UICollectionViewCell *cell in [collectionView visibleCells]) {
        if ([cell isKindOfClass:[PostListCell class]]) {
            PostListCell* postcell = (PostListCell*)cell;
            if ([postcell.post.type isEqualToString:@"video"]) {
                [visibleVideoCells addObject:postcell];
            }
        } else if ([cell isKindOfClass:[LivestreamCell class]]) {
            LivestreamCell* liveCell = (LivestreamCell*)cell;
            [visibleLiveCells addObject:liveCell];
        }
    }
    
    // todo: live stream cell
    
    // decide if there is a cell that need to be played
    unsigned long int visibleVideoCellCount = [visibleVideoCells count];
    if (visibleVideoCellCount == 1) {
        PostListCell* pCell = [visibleVideoCells objectAtIndex:0];
        if ([pCell isCellVisible]) {
            [pCell startAVPlayer];
        } else {
            [pCell stopAVPlayer];
        }
    } else if (visibleVideoCellCount == 2) {
        PostListCell* pCellUp;
        PostListCell* pCellDown;
        
        // sort
        if ([[visibleVideoCells objectAtIndex:0] frame].origin.y > [[visibleVideoCells objectAtIndex:1] frame].origin.y) {
            pCellUp = [visibleVideoCells objectAtIndex:1];
            pCellDown = [visibleVideoCells objectAtIndex:0];
        } else {
            pCellUp = [visibleVideoCells objectAtIndex:0];
            pCellDown = [visibleVideoCells objectAtIndex:1];
        }
        
        // control cell play
        if ([pCellUp isCellVisible]) {
            [pCellDown stopAVPlayer];
            [pCellUp startAVPlayer];
        } else {
            [pCellUp stopAVPlayer];
            if ([pCellDown isCellVisible]) {
                [pCellDown startAVPlayer];
            } else {
                [pCellDown stopAVPlayer];
            }
        }
    }
}

-(void)stopAVPlayerForCollectionView:(UICollectionView*)collectionView
{
    for (UICollectionViewCell *cell in [collectionView visibleCells]) {
        if ([cell isKindOfClass:[PostListCell class]]) {
            PostListCell* postcell = (PostListCell*)cell;
            if ([postcell.post.type isEqualToString:@"video"]) {
                [postcell stopAVPlayer];
            }
        }
    }
}

-(NSString*)getTimeFormatBySecond:(int)second
{
    
    if(second<0)
        return @"00:00";
    
    NSTimeInterval theTimeInterval = second;
    
    // Get the system calendar
    NSCalendar *sysCalendar = [NSCalendar currentCalendar];
    
    // Create the NSDates
    NSDate *date1 = [[NSDate alloc] init];
    NSDate *date2 = [[NSDate alloc] initWithTimeInterval:theTimeInterval sinceDate:date1];
    
    // Get conversion to months, days, hours, minutes
    unsigned int unitFlags = NSSecondCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit;
    
    NSDateComponents *conversionInfo = [sysCalendar components:unitFlags fromDate:date1  toDate:date2  options:0];
   
    int year = (int)[conversionInfo year];
    int month = (int)[conversionInfo month];
    int day = (int)[conversionInfo day];
    int hour = (int)[conversionInfo hour];
    int minute = (int)[conversionInfo minute];
    int sec = (int)[conversionInfo second];

    if(year!=0){
        return [NSString stringWithFormat:@"%dY %dM %dD %dH %02d:%02d",year,month,day,hour,minute,sec];
    }else if(month!=0){
        return [NSString stringWithFormat:@"%dM %dD %dH %02d:%02d",month,day,hour,minute,sec];
    }else if(day!=0){
        return [NSString stringWithFormat:@"%dD %dH %02d:%02d",day,hour,minute,sec];
    }else if(hour!=0){
        return [NSString stringWithFormat:@"%dH:%02d:%02d",hour,minute,sec];
    }else if(minute!=0){
        return [NSString stringWithFormat:@"%02d:%02d",minute,sec];
    }else{
        return [NSString stringWithFormat:@"%02d:%02d",minute,sec];
    }
}

-(BOOL)shouldOpenLivestream
{
    return YES;
    
    //return ([GET_DEFAULT(IS_ADMIN) intValue]==1 || [GET_DEFAULT(IS_VERIFIED) intValue]==1 );
}

-(int)hexString2ColorCode:(NSString*)hexString
{
    unsigned result = 0;
    if (hexString && hexString.length) {
        NSScanner *scanner = [NSScanner scannerWithString:hexString];
        [scanner setScanLocation:1]; // bypass '#' character
        [scanner scanHexInt:&result];
    } else {
        result = (unsigned)(RANDOM_FLOAT(0, 28));
    }
    
    return result%28+1;
}

-(BOOL) isUsingFastGPU
{
    if([EAGLContext currentContext]==nil) {
        [EAGLContext setCurrentContext:[[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2]];
    }
    
    NSString *glVersion = [NSString stringWithUTF8String:(char *)glGetString(GL_VERSION)];
    
    if ([glVersion containsString:@"Apple A5"] || [glVersion containsString:@"S5L8"]) {
        return NO;
    }
    
    if ([glVersion containsString:@"Apple A6"] || [glVersion containsString:@"IMGSGX5"]) {
        return NO;
    }
    
    if ([glVersion containsString:@"Apple A7"] || [glVersion containsString:@"G6430"]) {
        return YES;
    }
    
    if ([glVersion containsString:@"Apple A8"] || [glVersion containsString:@"GXA6850"]) {
        return YES;
    }
    
    return YES;
}

-(BOOL)shouldPlaySoundForNotificationType:(NSString*)notifType
{
    if([notifType isEqualToString:NEW_FOLLOW] && [GET_DEFAULT(PUSH_FOLLOW) isEqual:@1] ) {
        return YES;
    }else if([notifType isEqualToString:NEW_LIKE]  && [GET_DEFAULT(PUSH_LIKE) isEqual:@1]){
        return YES;
    }else if([notifType isEqualToString:NEW_COMMENT] && [GET_DEFAULT(PUSH_COMMENT) isEqual:@1]){
        return YES;
    }else if([notifType isEqualToString:NEW_POST_TAG] && [GET_DEFAULT(PUSH_TAG) isEqual:@1]){
        return YES;
    }else if([notifType isEqualToString:NEW_COMMENT_TAG] && [GET_DEFAULT(PUSH_TAG) isEqual:@1]){
        return YES;
    }else if([notifType isEqualToString:NEW_FRIEND_JOIN_FROM_CONTACTS] && [GET_DEFAULT(PUSH_FRIEND_JOIN) isEqual:@1]){
        return YES;
    }else if([notifType isEqualToString:NEW_FRIEND_JOIN_FROM_FB] && [GET_DEFAULT(PUSH_FRIEND_JOIN) isEqual:@1]){
        return YES;
    }else if([notifType isEqualToString:NEW_SYSTEM_NOTIF] && [GET_DEFAULT(PUSH_SYSTEM_NOTIF) isEqual:@1]){
        return YES;
    }else if([notifType isEqualToString:NEW_LIVE_STREAM] && [GET_DEFAULT(PUSH_LIVESTREAM_NOTIF) isEqual:@1]){
        return YES;
    }else if([notifType isEqualToString:NEW_LIVE_RESTREAM] && [GET_DEFAULT(PUSH_RESTREAM_NOTIF) isEqual:@1]){
        return YES;
    }
    return NO;

}

-(float)getCurrencyRate
{
    return [_currenctRateDict[[self getCurrencyType]] floatValue];
}

-(NSString*)getCurrencyType 
{
    if([GET_DEFAULT(CURRENCY_RATE_SETTED) isEqual:@0]){
    
        NSString* countryCode = GET_DEFAULT(IP_COUNTRY);
        if([countryCode isEqualToString:@"TW"]){
            #ifdef INTERNATIONAL_VERSION
            return @"TWD";
            #else
            return @"¥";
            #endif
        }else if([countryCode isEqualToString:@"CN"]){
            return @"¥";
        }else if([countryCode isEqualToString:@"US"]){
            return @"USD";
        }else if([countryCode isEqualToString:@"MY"]){
            return @"MYR";
        }else if([countryCode isEqualToString:@"HK"]){
            return @"HKD";
        }else if([countryCode isEqualToString:@"MO"]){
            return @"MOP";
        }else if([countryCode isEqualToString:@"SG"]){
            return @"SGD";
        }else if([countryCode isEqualToString:@"JP"]){
            return @"JPY";
        }
        
        #ifdef INTERNATIONAL_VERSION
            return @"USD";
        #else
            return @"¥";
        #endif
    }else{
        
        if([GET_DEFAULT(CURRENCY_RATE) isEqualToString:@"CNY"]){
            return @"¥";
        }
        return GET_DEFAULT(CURRENCY_RATE);
    }
}

-(void)recuriveGetIpGeoLocation
{
    [self detectIpGeoLocation:^(BOOL success, NSDictionary *jsonDict) {
        if(!success || [GET_DEFAULT(IP_COUNTRY) isEqualToString:@""]){
            [self recuriveGetIpGeoLocation];
        }
    }];
}

-(void) detectIpGeoLocation:(void (^)(BOOL success, NSDictionary* jsonDict)) callback
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval=1;

    [manager GET:@"http://ip-api.com/json" parameters:@{}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             NSDictionary *jsonDict = (NSDictionary *) responseObject;
             NSString* userip = (NSString*)jsonDict[@"query"];
             
             
             [DEFAULTS setObject:userip forKey:MY_USER_IP];
             [DEFAULTS setObject:jsonDict[@"city"] forKey:CURRENT_CITY];

             if(jsonDict[@"countryCode"]) {
                 [DEFAULTS setObject:jsonDict[@"countryCode"] forKey:IP_COUNTRY];
             }
             
             [DEFAULTS synchronize];
             if(callback!=nil)
                 callback(YES,jsonDict);

         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
             if(callback!=nil)
                 callback(NO,nil);
         
         }
     ];
}



-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(NSString*)modelToJsonString:(id)arrayOrDict
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrayOrDict
                                                       options:0
                                                        error:&error];
    NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    return JSONString;
}

-(NSString*)queryDNS
{
    return @"";
}


-(NSString*) getCountryImageFileName:(NSString*)name
{
    if([name isEqualToString:@"TW"]&&( [GET_DEFAULT(IP_COUNTRY) isEqualToString:@"CN"] || [GET_DEFAULT(IP_COUNTRY) isEqualToString:@""]) ){
        return @"taiwan_2";
    }
    return (NSString*) self.country2File[name];
}

- (LiveStreamViewController *)liveStreamSharedManager
{
    static LiveStreamViewController *shaderManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shaderManager = [[LiveStreamViewController alloc] init];
    });
    return shaderManager;
}

-(IAPManager*)iapManager
{
    static IAPManager *shaderManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shaderManager = [[IAPManager alloc] init];
    });
    return shaderManager;
}

-(STTweetLabel*)stTitleLabel
{
    static STTweetLabel *shared=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[STTweetLabel alloc] init];
        [shared setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(14)}
                      hotWord:STTweetHandle];
        [shared setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(14)}
                      hotWord:STTweetHashtag];
        [shared setAttributes:@{NSForegroundColorAttributeName: GRAY_COLOR,
                                          NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(14)}];

    });
    return shared;
}

-(STTweetLabel*)msgTextLabel
{
    static STTweetLabel *shared=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[STTweetLabel alloc] init];
        [shared setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(14)}
                      hotWord:STTweetHandle];
        [shared setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(14)}
                      hotWord:STTweetHashtag];
        [shared setAttributes:@{NSForegroundColorAttributeName: DARK_GRAY_COLOR,
                                NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(14)}];
    });
    return shared;
}

-(void)removeCacheInfo
{
    /* remove cache data for version 1.12 to change dir */
    if([GET_DEFAULT(REMOVE_CACHED_INFO) intValue]==0 ){
        NSMutableArray *giftZiped = CREATE_MUTABLE_ARRAY;
        [DEFAULTS setObject:giftZiped forKey:GIFT_ZIP];
        [[DatabaseManager sharedInstance] removeCacheVideoInfo];
        [DEFAULTS setObject:@1 forKey:REMOVE_CACHED_INFO];
        [DEFAULTS synchronize];
        
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:@"./Docuemnts" error:nil];
        for (NSString *filename in fileArray)  {
            [fileMgr removeItemAtPath:[@"./Docuemnts" stringByAppendingPathComponent:filename] error:NULL];
        }
    }
}

-(BOOL)isSpecialMode
{
//    return YES;
//    return YES;
//    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"dd/MM/yyyy"];
//    NSDate* date1 = [[NSDate alloc] initWithTimeIntervalSince1970:CURRENT_TIMESTAMP];
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate *dt1 = [[NSDate alloc] init];
    NSDate* dt2 = [[NSDate alloc] initWithTimeIntervalSince1970:CURRENT_TIMESTAMP];
    dt1 = [df dateFromString:@"2016-2-16"];
    NSComparisonResult result = [dt1 compare:dt2];
    switch (result)
    {
        case NSOrderedAscending:
//            DLog(@"%@ is greater than %@", dt2, dt1);
            break;
        case NSOrderedDescending:
//            DLog(@"%@ is less %@", dt2, dt1);
            if([GET_DEFAULT(IS_FESTIVAL) intValue]==1){
                return YES;
            }else{
                return NO;
            }
            break;
        case NSOrderedSame:
//            DLog(@"%@ is equal to %@", dt2, dt1);
            break;
        default:
//            DLog(@"erorr dates %@, %@", dt2, dt1);
            break;
    }
    
    /* tempororily  */
//    if([GET_DEFAULT(IS_FESTIVAL) intValue]==1){
//        return YES;
////        if([[dateFormat stringFromDate:date1] isEqualToString:@"24/12/2015"]){
////            return YES;
////        }
//    }else{
//        return NO;
//    }
    
    return NO;
    
//    DLog(@"data:%@",[dateFormat stringFromDate:date1] );
//
//    return YES;
}


-(BOOL)openIDAvaliable:(NSString*)openID
{
    NSString *nameRegex = @"[A-Za-z0-9_.]+";
    NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    if(![nameTest evaluateWithObject:openID]){
        return NO;
    }else{
        return YES;
    }
    
}


@end
