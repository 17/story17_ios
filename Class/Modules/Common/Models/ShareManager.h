//
//  ShareManager.h
//  Story17
//
//  Created by York on 2016/1/8.
//  Copyright © 2016年 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"


#ifdef INTERNATIONAL_VERSION
typedef NS_ENUM(NSInteger, ShareMediaPlatform) {
    ShareMediaPlatform17 = 0,
    ShareMediaPlatformFacebook,
    ShareMediaPlatformTwitter,
    ShareMediaPlatformWeibo,
    ShareMediaPlatformWechat,
    ShareMediaPlatformLine,
    ShareMediaPlatformWhatsApp,
    ShareMediaPlatformSystemCopyLink,
    ShareMediaPlatformSystemMail,
    ShareMediaPlatformSystemMessage,
    ShareMediaPlatformMax
};
#else
typedef NS_ENUM(NSInteger, ShareMediaPlatform) {
    ShareMediaPlatform17 = 0,
    ShareMediaPlatformWeibo,
    ShareMediaPlatformWechat,
    ShareMediaPlatformMoments,
    ShareMediaPlatformQQ,
    ShareMediaPlatformQzone,
    ShareMediaPlatformSystemCopyLink,
    ShareMediaPlatformMax
};
#endif


@interface ShareManager : NSObject <MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,WXApiDelegate,UIDocumentInteractionControllerDelegate>

@property (nonatomic, strong) UIDocumentInteractionController *documentController;

-(void)shareFacebookWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage;
-(void)shareTwitterWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage;
-(void)shareInstagramWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage;
-(void)shareLineWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage;
-(void)shareMomentsWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage;
-(void)shareQzoneWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage withObj:(id)obj;
-(void)shareQQWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage;
-(void)shareWechatWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage;
-(void)shareWeiboWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage;
-(void)shareWhatsappWithUrl:(NSString*)shareUrl shareText:(NSString*)shareText shareImage:(NSString*)shareImage;
-(void)shareMessageWithUrl:(NSString*)shareUrl;
-(void)shareMailWithUrl:(NSString*)shareUrl;
-(void)copyUrl:(NSString*)shareUrl;

@end
