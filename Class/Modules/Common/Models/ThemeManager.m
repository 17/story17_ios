#import "ThemeManager.h"
#import "ImageTextField.h"

@implementation ThemeManager

#define alertViewHeight 30

+(UIImageView*)circleImageView:(float)imgSize
{    
    UIImageView* imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, imgSize, imgSize)];
//    imgView.layer.cornerRadius  = imgSize/2;
//    imgView.layer.masksToBounds = YES;
//    imgView.layer.borderWidth = 5.0;
    imgView.contentMode = UIViewContentModeScaleAspectFill;
    imgView.clipsToBounds = YES;
//    imgView.layer.borderColor = [WHITE_COLOR CGColor];
    return imgView;
}

+(UILabel*)bolderLabel:(CGRect)frame
{
//    UIImageView* imgView = [UIImageView new];

    UILabel* label = [UILabel new];
    [label setFont:SYSTEM_FONT_WITH_SIZE(12)];
    label.numberOfLines = 2;

//    CGRect stringRect = [label boundingRectWithSize:CGSizeMake(frame.size.width, frame.size.height)
//                                          options:NSStringDrawingUsesLineFragmentOrigin
//                                       attributes:@{ NSFontAttributeName : label.font }
//                                          context:nil];
    
    [label drawTextInRect:CGRectMake(5, 5, 5, 5)];
    label.frame = frame;
    label.textAlignment = NSTextAlignmentLeft;
    label.layer.cornerRadius = 10.0f;
    label.layer.masksToBounds = YES;
    label.layer.borderWidth = 2.0f;
    label.layer.borderColor = [MAIN_COLOR CGColor];
    return label;
}

+(UILabel*) normalLabel:(CGRect)frame;
{
    UILabel* label = [UILabel new];
    label.frame = frame;
    [label setTextColor:DARK_GRAY_COLOR];
    [label setFont:SYSTEM_FONT_WITH_SIZE(12)];
    return label;
}

+(UIButton*) likeButton:(CGRect)frame
{
    UIButton* btn = [[UIButton alloc]initWithFrame:frame];
    [btn setBackgroundImage:[[UIImage imageNamed:@"btn_pink_normal"] resizableImageWithCapInsets:UIEdgeInsetsMake(32, 32, 32, 32)] forState:UIControlStateNormal];
    [btn setBackgroundImage:[[UIImage imageNamed:@"btn_pink_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(32, 32, 32, 32)] forState:UIControlStateHighlighted];
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = 20.0f;
    return btn;
}

+(UIButton*) malelikeButton:(CGRect)frame
{
    UIButton* btn = [[UIButton alloc]initWithFrame:frame];
    
    [btn setBackgroundImage:[[UIImage imageNamed:@"btn_yellow_normal"] resizableImageWithCapInsets:UIEdgeInsetsMake(32, 32, 32, 32)] forState:UIControlStateNormal];
    [btn setBackgroundImage:[[UIImage imageNamed:@"btn_yellow_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(32, 32, 32, 32)] forState:UIControlStateHighlighted];
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = 20.0f;
    return btn;
}


+(UIButton*) moreViewRectButton:(NSString*)imgName hightLight:(NSString*)hightLight
{
    UIButton* btn = [UIButton new];
    
    [btn setBackgroundImage:[[UIImage imageNamed:imgName] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forState:UIControlStateNormal];
    [btn setBackgroundImage:[[UIImage imageNamed:imgName] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forState:UIControlStateSelected];
    
    return btn;
}


+(UIButton*)badgeButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:12];
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btn setBackgroundImage:[IMAGE_FROM_BUNDLE(@"number_tip") resizableImageWithCapInsets:UIEdgeInsetsMake(8, 9, 8, 9)] forState:UIControlStateNormal];
    btn.adjustsImageWhenHighlighted = NO;
    return btn;
}

#pragma mark disclosure icon
+(UIImageView*)disclosureImageView
{
    return [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"icon_arr_normal")];
}





+(UIView*) separaterLine
{
    UIView* lineView = [UIView new];
    [lineView setBackgroundColor:LINE_GRAY_COLOR];
    return lineView;
}

+(UIView*)iconTextFieldView:(CGRect)frame{
    
    UIView* view = [[UIView alloc]initWithFrame:frame];
    
    UIImageView* headImage = [[UIImageView alloc]initWithFrame:CGRectMake(12, frame.size.height/2-11, 22, 22)];
    [headImage setImage:[UIImage imageNamed:@"icon_write"]];
    
    
    UITextField* mainTextField = [[UITextField alloc]initWithFrame:CGRectMake(headImage.frame.origin.x+headImage.frame.size.width+10, 0, SCREEN_WIDTH-headImage.frame.origin.x+headImage.frame.size.width+10-20, frame.size.height)];
    [mainTextField setFont:SYSTEM_FONT_WITH_SIZE(15)];
    mainTextField.textAlignment = NSTextAlignmentLeft;
    
    [mainTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    UIView* lineView = [self separaterLine];
    lineView.frame =CGRectMake(5, frame.size.height, SCREEN_WIDTH-10, 0.5);
    
    
    [view addSubview:headImage];
    [view addSubview:mainTextField];
    [view addSubview:lineView];
    
    return view;
}


#pragma mark - Search
+(UIImageView *)maintabBackgroundImageView
{
    UIImageView *view = [[UIImageView alloc] init];
    view.userInteractionEnabled = YES;
    return view;
}


+(UIButton*)findFriendBtn
{
    UIButton* findFriendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [findFriendBtn setFrame:CGRectMake(0, 0, NAVI_BAR_HEIGHT, NAVI_BAR_HEIGHT)];
    [findFriendBtn setShowsTouchWhenHighlighted:YES];
    
    [findFriendBtn setImage:[UIImage imageNamed:@"addfriend"] forState:UIControlStateNormal];
    [findFriendBtn setImage:[UIImage imageNamed:@"addfriend"] forState:UIControlStateHighlighted];
    return findFriendBtn;
}


+(UIButton*)regionButton:(CGRect)frame
{
    UIButton* regionTypeBtn=[[UIButton alloc]initWithFrame:frame];
    [regionTypeBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
    return regionTypeBtn;
}


+(UIButton*)leaderBoardBtn
{
    UIButton* leaderBoardBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leaderBoardBtn setFrame:CGRectMake(0, 0, NAVI_BAR_HEIGHT, NAVI_BAR_HEIGHT)];
    [leaderBoardBtn setShowsTouchWhenHighlighted:YES];
    
    [leaderBoardBtn setImage:[UIImage imageNamed:@"leaderboard"] forState:UIControlStateNormal];
    [leaderBoardBtn setImage:[UIImage imageNamed:@"leaderboard"] forState:UIControlStateHighlighted];
    return leaderBoardBtn;
}

+(UIButton*) gridModeBtn
{
    UIButton* gridModeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [gridModeBtn setFrame:CGRectMake(0, 0, NAVI_BAR_HEIGHT, NAVI_BAR_HEIGHT)];
    [gridModeBtn setShowsTouchWhenHighlighted:YES];
    
    [gridModeBtn setImage:[UIImage imageNamed:@"profile_grid_white"] forState:UIControlStateNormal];
    [gridModeBtn setImage:[UIImage imageNamed:@"profile_list_white"] forState:UIControlStateSelected];

    return gridModeBtn;
}

+(UIButton*)moreBtn
{
    UIButton* moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [moreBtn setFrame:CGRectMake(0, 0, NAVI_BAR_HEIGHT, NAVI_BAR_HEIGHT)];
    [moreBtn setShowsTouchWhenHighlighted:YES];
    
    [moreBtn setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
    return moreBtn;
}

+(UIButton*)getGreenBtn
{
    UIButton* greenlineBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    greenlineBtn.titleLabel.font =SYSTEM_FONT_WITH_SIZE(12);
    [greenlineBtn setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    [greenlineBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
    [greenlineBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
    return greenlineBtn;
}

+(UIButton*)getGreenLineBtn
{
    UIButton* greenlineBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    greenlineBtn.titleLabel.font =SYSTEM_FONT_WITH_SIZE(12);
    [greenlineBtn setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    [greenlineBtn setBackgroundImage:[[UIImage imageNamed:@"btn_greenline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
    [greenlineBtn setBackgroundImage:[[UIImage imageNamed:@"btn_greenline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];

    return greenlineBtn;
}

+(UIButton*)getMainCircleBtn
{
    UIButton* mainCircleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    mainCircleButton.titleLabel.font =SYSTEM_FONT_WITH_SIZE(12);
    [mainCircleButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    [mainCircleButton setBackgroundImage:[[UIImage imageNamed:@"btn_black_cl"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 15, 10, 15)] forState:UIControlStateNormal];
    [mainCircleButton setBackgroundImage:[[UIImage imageNamed:@"btn_black_cl2_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 15, 5, 15)] forState:UIControlStateHighlighted];
    return mainCircleButton;
}

+(UIButton*)getGreenCircleBtn
{
    UIButton* greenCircleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
 
    [greenCircleBtn bk_addEventHandler:^(id sender) {
        [greenCircleBtn playBounceAnimation];
    } forControlEvents:UIControlEventTouchUpInside];
    
    greenCircleBtn.titleLabel.font =SYSTEM_FONT_WITH_SIZE(16);
    [greenCircleBtn setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    
    [greenCircleBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green_circle"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 12)] forState:UIControlStateNormal];
    [greenCircleBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green_circle_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 12)] forState:UIControlStateHighlighted];
    [greenCircleBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    greenCircleBtn.layer.cornerRadius = 25.0f;
    greenCircleBtn.layer.masksToBounds = YES;
    return greenCircleBtn;
}


+(UIButton*)getWhiteLineBtn
{
    UIButton* whiteLineBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    whiteLineBtn.titleLabel.font =SYSTEM_FONT_WITH_SIZE(20);
    [whiteLineBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    
    [whiteLineBtn setBackgroundImage:[[UIImage imageNamed:@"btn_whitebg"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [whiteLineBtn setBackgroundImage:[[UIImage imageNamed:@"btn_whitebg_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
    [whiteLineBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
    return whiteLineBtn;
}

+(UIButton*)getGraylineBtn
{
    UIButton* whiteLineBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    whiteLineBtn.titleLabel.font =SYSTEM_FONT_WITH_SIZE(12);
    [whiteLineBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    
    [whiteLineBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [whiteLineBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
    [whiteLineBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
    return whiteLineBtn;
}



+(UIButton*)getPinkLineBtn
{
    UIButton* greenlineBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    greenlineBtn.titleLabel.font =SYSTEM_FONT_WITH_SIZE(12);
    [greenlineBtn setTitleColor:ALERT_TEXT_COLOR forState:UIControlStateNormal];
    [greenlineBtn setBackgroundImage:[[UIImage imageNamed:@"btn_pink"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
    [greenlineBtn setBackgroundImage:[[UIImage imageNamed:@"btn_pink_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
    return greenlineBtn;
}

+(UILabel*) getNoDataLabel
{
    
    UILabel* noDataLabel = [UILabel new];
    noDataLabel.frame = CGRectMake(0, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, SCREEN_WIDTH, 50);
    [noDataLabel setText:LOCALIZE(@"no_data")];
    [noDataLabel setBackgroundColor:[UIColor clearColor]];
    [noDataLabel setFont:BOLD_FONT_WITH_SIZE(16)];
    noDataLabel.textAlignment = NSTextAlignmentCenter;
    [noDataLabel setTextColor:DARK_GRAY_COLOR];
    noDataLabel.hidden = YES;
    return noDataLabel;
}

+(UIImageView*) getNoDataImageView
{
    UIImageView* noDataImageView = [UIImageView new];
    noDataImageView.frame = CGRectMake(SCREEN_WIDTH/2-125, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-180, 250, 250);
    [noDataImageView setImage:[UIImage imageNamed:@"nodata"]];
    noDataImageView.hidden = YES;
    return noDataImageView;
}


+(UIImageView*)randomHeartImage
{
    UIImageView* heartImageView = [UIImageView new];
    heartImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"colorheart_%i", (int) arc4random_uniform(28)+1]];
    return heartImageView;
}

+(UIScrollView*)getBouncedScrollView
{
    UIScrollView* scrollView = [UIScrollView new];
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = YES;
    scrollView.delaysContentTouches = YES;
    scrollView.canCancelContentTouches = NO;
    scrollView.scrollsToTop = NO;
    return scrollView;
}

+(UIImageView*)getAlertImageView
{
    UIImageView* alertImageView = [[UIImageView alloc]initWithImage:IMAGE_FROM_BUNDLE(@"dialog")];
    alertImageView.frame = CGRectMake(0, SCREEN_HEIGHT-KEYBOARD_HEIGHT, SCREEN_WIDTH, alertViewHeight);
    return alertImageView;
}

+(UILabel*)getLivestreamlabel:(CGRect)rect
{
    UILabel* liveLabel = [[UILabel alloc]initWithFrame:rect];
    
    liveLabel.font = BOLD_FONT_WITH_SIZE(14);
    [liveLabel setTextColor:MAIN_COLOR];
    liveLabel.textAlignment = NSTextAlignmentCenter;
    
    return liveLabel;
}


/* Livestream Animation View */
+(UIImageView*) audioOnlyImageView
{
   UIImageView* audioOnlyImageView = [UIImageView new];
    [audioOnlyImageView setImage:IMAGE_FROM_BUNDLE(@"live_vonly_01")];
    NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity:88];
    
    for(int count = 1; count <= 20; count++)
    {
        //        NSString *fileName = [NSString stringWithFormat:@"live_vonly_%02d.png", count];
        NSString *fileName = [NSString stringWithFormat:@"live_vonly_%02d", count];
        UIImage  *frame    = [UIImage imageNamed:fileName];
        [images addObject:frame];
    }
    
    audioOnlyImageView.animationImages = images;
    audioOnlyImageView.animationDuration = 1;
    [audioOnlyImageView startAnimating];
    
    return audioOnlyImageView;
}

+(UIImageView*) loadingImageView
{
    UIImageView* loadingImageView = [UIImageView new];
    [loadingImageView setImage:IMAGE_FROM_BUNDLE(@"link_01")];
    NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity:88];
    
    for(int count = 1; count <= 40; count++)
    {
        NSString *fileName = [NSString stringWithFormat:@"link_%02d", count];
        UIImage  *frame    = [UIImage imageNamed:fileName];
        [images addObject:frame];
    }
    
    loadingImageView.animationImages = images;
    loadingImageView.animationDuration = 1;
    [loadingImageView startAnimating];
    
    return loadingImageView;
}

+(UIImageView *)busyImageView
{
    UIImageView* busyImageView = [UIImageView new];
    [busyImageView setImage:IMAGE_FROM_BUNDLE(@"lod_01")];
    NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity:88];
    
    for(int count = 1; count <= 30; count++)
    {
        NSString *fileName = [NSString stringWithFormat:@"lod_%02d", count];
        UIImage  *frame    = [UIImage imageNamed:fileName];
        [images addObject:frame];
    }
    
    busyImageView.animationImages = images;
    busyImageView.animationDuration = 1;
    [busyImageView startAnimating];
    
    return busyImageView;
}

/* Login & Register */
+ (UIButton*) getRegisterControlBtn
{
    UIButton* registerCtlBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    registerCtlBtn.frame = CGRectMake(10, 0, SCREEN_WIDTH-20, 40);
    registerCtlBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [registerCtlBtn setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    [registerCtlBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [registerCtlBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
    return registerCtlBtn;
}

+ (UIButton*) getRegisterCircleBtn
{
    UIButton* registerCtlBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    registerCtlBtn.frame = CGRectMake(10, 0, 50, 50);
    registerCtlBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [registerCtlBtn setTitleColor:MAIN_COLOR forState:UIControlStateNormal];

    return registerCtlBtn;
}

+ (UIButton*) getRegisterNextStepBtn
{
    UIButton* registerNextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    registerNextBtn.frame = CGRectMake(10, 0, SCREEN_WIDTH-20, 40);
    registerNextBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(14);
    [registerNextBtn setTitle:LOCALIZE(@"REGISTER_NEXT_STEP") forState:UIControlStateNormal];
    [registerNextBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    [registerNextBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
    [registerNextBtn setBackgroundImage:[[UIImage imageNamed:@"btn_green_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
    
    return registerNextBtn;
}

+ (UILabel*) getRegisterHeaderTitleLabel
{
    UILabel* registerHeaderTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 100)];
    registerHeaderTitleLabel.numberOfLines = 0;
    registerHeaderTitleLabel.font = BOLD_FONT_WITH_SIZE(14);
    registerHeaderTitleLabel.textAlignment = NSTextAlignmentCenter;
    registerHeaderTitleLabel.textColor = WHITE_COLOR;
    registerHeaderTitleLabel.backgroundColor = [UIColor clearColor];

    return registerHeaderTitleLabel;
}

+ (UIButton*) getRegisterSkipBtn
{
    UIButton* registerSkipBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [registerSkipBtn setTitle:LOCALIZE(@"REGISTER_SKIP_STEP") forState:UIControlStateNormal];

    registerSkipBtn.frame = CGRectMake(0, 0, 45, 45);
    registerSkipBtn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    [registerSkipBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    
    return registerSkipBtn;
}

+(UIButton*)notifyBtn
{
    UIButton* notifyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [notifyBtn setFrame:CGRectMake(0, 3, NAVI_BAR_HEIGHT, NAVI_BAR_HEIGHT)];
    [notifyBtn setShowsTouchWhenHighlighted:YES];
    
    [notifyBtn setImage:[UIImage imageNamed:@"news"] forState:UIControlStateNormal];
    [notifyBtn setImage:[UIImage imageNamed:@"news"] forState:UIControlStateHighlighted];
    
    return notifyBtn;
}

#pragma mark - Guest Mode

+ (UIButton *)getExploreUserButton
{
    UIButton *exploreUserButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [exploreUserButton setFrame:CGRectMake(0, 0, NAVI_BAR_HEIGHT, NAVI_BAR_HEIGHT)];
    
    [exploreUserButton setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    
    return exploreUserButton;
}

@end