//
//  DatabaseManager.m
//  story17
//
//  Created by POPO Chen on 6/23/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "DatabaseManager.h"
#import "Constant.h"

@interface DatabaseManager()

@property (nonatomic, strong) FMDatabase* database;
@property (nonatomic, strong) FMDatabaseQueue* FMQueue;
@property (nonatomic, strong) NSMutableArray* cacheVideoArray;  /* cache downloaded video to avoid db insertion delay */
@property (nonatomic, strong) NSMutableArray* downloadArray;    /* downloading video filename */
@property (nonatomic, strong) NSMutableArray* bufferArray;      /* buffer to cache file wating for downloading */
@property (copy) dispatch_queue_t writeQueue;

@property (nonatomic, strong) NSMutableArray* cacheReportUserArray;
@property (nonatomic, strong) NSMutableArray* cacheReportPostArray;

@end

@implementation DatabaseManager

#define max_fileNumber 20
#define max_downloading_file 1
#define buffer_size 2

+(DatabaseManager*) sharedInstance
{
    static dispatch_once_t once;
    
    static DatabaseManager *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(id)init
{
    self = [super init];
    
    if (self) {
        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *documentsDir = [documentPaths objectAtIndex:0];
        NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"cache.sqlite"];
        
        _downloadArray = CREATE_MUTABLE_ARRAY;
        _cacheVideoArray = CREATE_MUTABLE_ARRAY;
        _bufferArray = CREATE_MUTABLE_ARRAY;
        
        _cacheReportUserArray = CREATE_MUTABLE_ARRAY;
        _cacheReportPostArray = CREATE_MUTABLE_ARRAY;
        
        if(![GET_DEFAULT(DOWNLOAD_VIDEO_FILES) isEqualToString:@"[]"]){
            NSString* defaultString = GET_DEFAULT(DOWNLOAD_VIDEO_FILES);
            NSArray *tempArray = [defaultString mj_JSONObject];
            _cacheVideoArray = [tempArray mutableCopy];

        }
        
        _database = [FMDatabase databaseWithPath:dbPath];
        
        if(![self.database open]) {
//            DLog(@"Cannot open cache.sqlite");
        }
        
        _FMQueue = [FMDatabaseQueue databaseQueueWithPath:dbPath];
        _writeQueue = dispatch_queue_create("write_queue",NULL);
        
        [self createCatchVideoTable];
        [self createReportInfoTable];

    }
    
    return self;
}

-(void)createCatchVideoTable
{
    [self.database executeUpdate:@"CREATE TABLE IF NOT EXISTS videoCache (id INTEGER PRIMARY KEY AUTOINCREMENT,videoName TEXT UNIQUE DEFAULT '',cacheTimestamp INT DEFAULT 0);"];
    [self.database executeUpdate:@"CREATE UNIQUE INDEX indexVideoID ON videoCache(videoName);"];
}

-(void)createReportInfoTable
{
    [self.database executeUpdate:@"CREATE TABLE IF NOT EXISTS reportRecords (id INTEGER PRIMARY KEY AUTOINCREMENT,paramID TEXT UNIQUE DEFAULT '',type TEXT DEFAULT '');"];
    [self.database executeUpdate:@"CREATE UNIQUE INDEX indexParamsID ON reportRecords(paramID);"];
}

-(void)insertReportRecord:(NSString*)paramID type:(NSString*)type
{

    if([type isEqualToString:@"userID"]){
        if(![_cacheReportUserArray containsObject:paramID]){
            [_cacheReportUserArray addObject:paramID];
        }
    }else if([type isEqualToString:@"postID"]){
        if(![_cacheReportPostArray containsObject:paramID]){
            [_cacheReportPostArray addObject:paramID];
        }
    }
    
    dispatch_async(self.writeQueue, ^{
        
        [self.FMQueue inDatabase:^(FMDatabase *db) {
            [db executeUpdate:@"INSERT INTO reportRecords ('paramID','type') VALUES(?,?) ",paramID,type];
        }];
    });
}

-(NSArray*)getReportRecord:(NSString*)reportType
{
    if([reportType isEqualToString:@"userID"] && [_cacheReportUserArray count]!=0){
        return _cacheReportUserArray;
    }else if([reportType isEqualToString:@"postID"]  && [_cacheReportPostArray count]!=0){
        return _cacheReportPostArray;
    }

    NSString* query = @"SELECT * FROM reportRecords";
    FMResultSet *rs = [_database executeQuery:query];
  
    while ([rs next]) {
        
        NSString* type = [rs stringForColumn:@"type"];
        NSString* paramID = [rs stringForColumn:@"paramID"];
        
        if([type isEqualToString:@"userID"]){
            if(![_cacheReportUserArray containsObject:paramID]){
                [_cacheReportUserArray addObject:paramID];
            }
        }else if([type isEqualToString:@"postID"]){
            if(![_cacheReportPostArray containsObject:paramID]){
                [_cacheReportPostArray addObject:paramID];
            }
        }
    }
    
//    DLog(@"_cacheReportUserArray:%@",_cacheReportUserArray);
//    DLog(@"_cacheReportPostArray:%@",_cacheReportPostArray);
    
    if([reportType isEqualToString:@"userID"]){
        return _cacheReportUserArray;
    }else if([reportType isEqualToString:@"postID"]){
        return _cacheReportPostArray;
    }
    return _cacheReportPostArray;
}

-(void)insertCacheVideo:(NSString*)videoName
{
    /* sync db insertion */
    
    NSString *defaultString = GET_DEFAULT(DOWNLOAD_VIDEO_FILES);
    NSArray *tempArray = [defaultString mj_JSONObject];
    _cacheVideoArray = [tempArray mutableCopy];
    
    [_cacheVideoArray addObject:videoName];
    
    if([_cacheVideoArray count]!=0){
        
        [DEFAULTS setObject:TO_JSON([_cacheVideoArray copy]) forKey:DOWNLOAD_VIDEO_FILES];
        [DEFAULTS synchronize];
    }
    
    
    if([_downloadArray containsObject:videoName]){
        [_downloadArray removeObject:videoName];
        if([_bufferArray count]!=0){

//            DLog(@"download video from buffer");
            
            [self downloadVideo:_bufferArray[0] withCallback:^(BOOL success, NSString *status) {
                if(success){
                    /* send notification to refresh Feed */
//                    DLog(@"BUFFER_VIDEO_UPLADED");
                    [NOTIFICATION_CENTER postNotificationName:BUFFER_VIDEO_UPLADED object:self];
                }
            }];
        }
    }
    
    int timeStamp = CURRENT_TIMESTAMP;
    
    dispatch_async(self.writeQueue, ^{
       
        [self.FMQueue inDatabase:^(FMDatabase *db) {
            
            [db executeUpdate:@"INSERT INTO videoCache ('videoName','cacheTimestamp') VALUES(?,?) ",videoName, [NSNumber numberWithInt:timeStamp]];
            NSUInteger count = [db intForQuery:@"SELECT COUNT(id) FROM videoCache"];
            
            if(count>max_fileNumber){
                /* Refresh files */
                [self refreshVideoCacheData];
            }
        }];
        
        dispatch_async(MAIN_QUEUE, ^{
        });
        
    });
}

-(void)refreshVideoCacheData
{
    
    dispatch_async(self.writeQueue, ^{
        
        [self.FMQueue inDatabase:^(FMDatabase *db) {
            
            NSUInteger count = [db intForQuery:@"SELECT COUNT(id) FROM videoCache"];
            NSString* query = [NSString stringWithFormat:@"SELECT videoName FROM videoCache ORDER BY cacheTimestamp asc limit %@",[NSNumber numberWithInt:(int)count-max_fileNumber]];
            
            FMResultSet *rs = [db executeQuery:query];

            NSMutableArray *array = [NSMutableArray new];
            while ([rs next]) {

                NSString* videoName = [rs stringForColumn:@"videoName"];
                DELETE_FILE(GET_LOCAL_FILE_PATH(videoName));
                [array addObject:videoName];
                [db executeUpdate:@"DELETE FROM videoCache WHERE videoName=?",videoName];
            
                /* sync db deletion */
                NSArray *tempArray = [GET_DEFAULT(DOWNLOAD_VIDEO_FILES) mj_JSONObject];
                _cacheVideoArray = [tempArray mutableCopy];
                [_cacheVideoArray removeObject:videoName];
                [DEFAULTS setObject:TO_JSON([_cacheVideoArray copy]) forKey:DOWNLOAD_VIDEO_FILES];
                [DEFAULTS synchronize];
            }
        }];
    });
}

-(void)downloadVideo:(NSString*)video withCallback:(void(^)(BOOL success,NSString* status))callback
{
    
//    DLog(@"downloadVideo ");
    
    if([_bufferArray containsObject:video]){
        [_bufferArray removeObject:video];
    }
    
    [self checkDownloadStatus];
    
    if([_downloadArray count]>=max_downloading_file){
        [_bufferArray addObject:video];
        if([_bufferArray count]>buffer_size){
            _bufferArray = [[_bufferArray subarrayWithRange:NSMakeRange(1,buffer_size)] mutableCopy];
        }
        callback(NO,@"buffering");
        return;
    }
    
    NSString* fileName = video;
    
    if([self fileExist:video]){

        callback(YES,@"success");
        
    } else {
        
        if([self isDownloading:video]){
            callback(NO,@"downloading");
            return;
        }
        
        [_downloadArray addObject:video];

//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
//        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:video];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        
        NSURL *URL = S3_FILE_URL(video);
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        
        NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
            DLog(@"download complete");
            NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSCachesDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
            return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
        
        } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
            
            if(error){
                DLog(@"download fail");

                [_downloadArray removeObject:video];
                DELETE_FILE(GET_LOCAL_FILE_PATH(fileName));
//                DLog(@"download fail : %@",video);
                callback(NO,@"fail");
            
            }else{
                DLog(@"download success");

                [self insertCacheVideo:video];
//                DLog(@"download success : %@",video);
                callback(YES,@"success");
            
            }
        
            
        }];
        [downloadTask resume];
        
        
        
//        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//        manager.requestSerializer.timeoutInterval = 2.0;
////        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
//        manager.operationQueue.maxConcurrentOperationCount = 4;
//
//        AFHTTPRequestOperation *op = [manager GET:S3_FILE_URLSTRING(video) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            
//            [self insertCacheVideo:video];
//            DLog(@"download success : %@",video);
//            callback(YES,@"success");
//            
//        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            
//            [_downloadArray removeObject:video];
//            DELETE_FILE(GET_LOCAL_FILE_PATH(fileName));
//            DLog(@"download fail : %@",video);
//            callback(NO,@"fail");
//
//        }];
//        
//        op.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
//        [op start];
        
    }
}

-(BOOL)fileExist:(NSString*)videoName
{
    if([_cacheVideoArray containsObject:videoName]){
        return YES;
    }
    return NO;

}

-(BOOL)isDownloading:(NSString*)videoName
{
    if([_downloadArray containsObject:videoName]){
        return YES;
    }
    return NO;
    
}

-(void)removeCacheVideoInfo
{
    [_cacheVideoArray removeAllObjects];
    [DEFAULTS setObject:@"[]" forKey:DOWNLOAD_VIDEO_FILES];
    [DEFAULTS synchronize];
}

-(void)checkDownloadStatus
{
//    DLog(@"_downloadArray %@",_downloadArray);
//    DLog(@"_bufferArray %@",_bufferArray);
//    DLog(@"_cacheVideoArray %@",_cacheVideoArray);
    
}



@end
