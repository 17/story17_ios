#import <Foundation/Foundation.h>
#import "Constant.h"

@interface ThemeManager : NSObject

+(UIImageView*) circleImageView:(float)imgSize;
+(UILabel*) bolderLabel:(CGRect)frame;
+(UILabel*) normalLabel:(CGRect)frame;
+(UIButton*) likeButton:(CGRect)frame;
+(UIButton*) malelikeButton:(CGRect)frame;

// MoreViewController UIButton
+(UIButton*) moreViewRectButton:(NSString*)imgName hightLight:(NSString*)hightLight;
+(UIButton*) badgeButton;
+(UIImageView*) disclosureImageView;

+(UIView*) iconTextFieldView:(CGRect)frame;
+(UIView*) separaterLine;

+(UIImageView *) maintabBackgroundImageView;

+(UIButton*) moreBtn;
+(UIButton*) findFriendBtn;
+(UIButton*) leaderBoardBtn;
+(UIButton*) gridModeBtn;
+(UIButton*) getPinkLineBtn;
+(UIButton*) getGreenBtn;
+(UIButton*) getGreenLineBtn;
+(UIButton*) getMainCircleBtn;
+(UIButton*) getGreenCircleBtn;
+(UIButton*) getWhiteLineBtn;
+(UIButton*) getGraylineBtn;

+(UIButton*)regionButton:(CGRect)frame;

+(UIImageView*) randomHeartImage;

//+(UILabel*) getNoDataLabel;
+(UIImageView*) getNoDataImageView;
+(UIScrollView*) getBouncedScrollView;
+(UIImageView*) getAlertImageView;

+(UILabel*) getLivestreamlabel:(CGRect)rect;
+(UIImageView*) audioOnlyImageView;
+(UIImageView*) loadingImageView;
+(UIImageView*) busyImageView;

/* Register */
+ (UIButton*) getRegisterControlBtn;
+ (UIButton*) getRegisterNextStepBtn;
+ (UILabel*)  getRegisterHeaderTitleLabel;
+ (UIButton*) getRegisterSkipBtn;
+ (UIButton*) getRegisterCircleBtn;
+ (UIButton*) notifyBtn;

#pragma mark - Guest Mode

+ (UIButton *)getExploreUserButton;

@end