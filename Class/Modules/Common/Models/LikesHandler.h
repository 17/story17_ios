//
//  LikerHandler.h
//  story17
//
//  Created by POPO Chen on 7/23/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LikesHandler : NSObject


+(LikesHandler*) sharedInstance;

-(void)likePost:(NSString*)postID;
-(void)likeBatchUpload;

-(void)likeLivestream:(NSString*)livestreamID;
-(void)likeLivestreamBatchUpdate;

@end
