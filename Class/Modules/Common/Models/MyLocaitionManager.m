//
//  MyLocaitionManager.m
//  story17
//
//  Created by POPO Chen on 7/16/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "MyLocaitionManager.h"

@interface MyLocaitionManager()

@property (strong,nonatomic) CLLocationManager *locationManager;

/* Self Location for livestream */
@property (nonatomic, strong) NSString* myLocationName;
@property float myLongtitude;
@property float myLattitude;

@end

@implementation MyLocaitionManager

+(MyLocaitionManager*) sharedInstance
{
    static dispatch_once_t once;
    
    static MyLocaitionManager *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(id)init
{
    self = [super init];
    if(self){
        _locationObjArray = CREATE_MUTABLE_ARRAY;
        
        _eof = NO;
        _isUpdating = NO;
        _lastUpdateTime = 0;
        
    }
    return self;
}


# pragma mark - CLLocationManagerDelegate
-(void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    float nowLatitude=0.0;
    float nowLongitude=0.0;
    
    if(newLocation.coordinate.latitude!=0.0 && newLocation.coordinate.longitude!=0.0) {
        nowLatitude = newLocation.coordinate.latitude;
        nowLongitude = newLocation.coordinate.longitude;
    } else {
        [self.locationManager stopUpdatingLocation];
        self.locationManager = nil;
        return;
    }

    self.myLongtitude = nowLongitude;
    self.myLattitude = nowLatitude;

    [self.locationManager stopUpdatingLocation];
    self.locationManager = nil;
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:newLocation
       completionHandler:^(NSArray *placemarks, NSError *error) {
           if (error){
               
               if(_delegate!=nil){
                   [_delegate finishUpdateLocation:NO localName:nil geo:nil];
               }
               return;
           }
           CLPlacemark *placemark = [placemarks objectAtIndex:0];
           self.myLocationName = placemark.locality;
           
           if(_delegate!=nil){
               [_delegate finishUpdateLocation:YES localName:self.myLocationName geo:[self getNowLocation]];
           }
    }];
    
    [self queryGooglePlaces:nil];
}

-(void) updateLocation
{
    if(self.locationManager!=nil || _isUpdating) { /* updating */
        return;
    }
    
    if(![CLLocationManager locationServicesEnabled]) {
        return;
    }
    
    /* reset end of file to no */
    _eof = NO;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.distanceFilter = 10.0;
    self.locationManager.pausesLocationUpdatesAutomatically = NO;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    self.locationManager.delegate = self;
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
}


-(void) queryGooglePlaces:(NSString*)pageToken {
    
    float lattitude=self.myLattitude;
    float longitude=self.myLongtitude;
    
    __block NSString *pagetoken;

    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&radius=%@&key=%@", lattitude,longitude, [NSString stringWithFormat:@"%i", SearchRadius], kGOOGLE_API_KEY];
    
    if(pageToken!=nil){
        url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken=%@&key=%@", pageToken, kGOOGLE_API_KEY];
    }

    _isUpdating = YES;

    NSURL *googleRequestURL=[NSURL URLWithString:url];
 
    // Retrieve the results of the URL.
    NSURLRequest *request = [NSURLRequest requestWithURL:googleRequestURL];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
  operation.responseSerializer = [AFJSONResponseSerializer serializer];

    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *data = (NSDictionary *) responseObject;
        NSArray *resultsArr = [data objectForKey:@"results"];
        pagetoken= [data objectForKey:@"next_page_token"];

        for (NSDictionary * resultDetailDic in resultsArr)
        {
            LocationObject* locationObj = [LocationObject new];
            locationObj.name = resultDetailDic[@"name"];
            locationObj.locationID = resultDetailDic[@"place_id"];
            locationObj.latitude = [resultDetailDic[@"geometry"][@"location"][@"lat"] intValue];
            locationObj.longitude = [resultDetailDic[@"geometry"][@"location"][@"lng"] intValue];
            [_locationObjArray addObject:locationObj];
        }
        
//        DLog(@"token:%@ %lu",pagetoken, (unsigned long)[_locationObjArray count]);
        
        if((int)[_locationObjArray count]<60 && pagetoken !=nil){
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self queryGooglePlaces:pagetoken];
            });
            
        }else{
            _isUpdating = NO;
            _eof = YES;
            _lastUpdateTime = CURRENT_TIMESTAMP;
            if(_delegate!=nil)
                [_delegate googlePlaceCallback:YES];
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        _isUpdating = NO;
        if(_delegate!=nil)
            [_delegate googlePlaceCallback:NO];
        
    }];
        [operation start];
}


-(void) googleSearchPlace:(NSString *)name withCallback:(void (^)(BOOL succcess,NSArray* locObjArray))callback {
    
    float lattitude=self.myLattitude;
    float longitude=self.myLongtitude;
    
    __block NSString *pagetoken;
    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&radius=%@&name=%@&key=%@", lattitude,longitude, [NSString stringWithFormat:@"%i", SearchRadius],name, kGOOGLE_API_KEY];
    url = [url stringByAddingPercentEscapesUsingEncoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingUTF8)];
    
    //Formulate the string as a URL object.
    NSURL *googleRequestURL=[NSURL URLWithString:url];
   // Retrieve the results of the URL.
    NSURLRequest *request = [NSURLRequest requestWithURL:googleRequestURL];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *data = (NSDictionary *) responseObject;
        NSArray *resultsArr = [data objectForKey:@"results"];
        pagetoken= [data objectForKey:@"next_page_token"];
        
        NSMutableArray* locaObj = CREATE_MUTABLE_ARRAY;
        for (NSDictionary * resultDetailDic in resultsArr)
        {
            LocationObject* locationObj = [LocationObject new];
            locationObj.name = resultDetailDic[@"name"];
            locationObj.locationID = resultDetailDic[@"place_id"];
            locationObj.latitude = [resultDetailDic[@"geometry"][@"location"][@"lat"] intValue];
            locationObj.longitude = [resultDetailDic[@"geometry"][@"location"][@"lng"] intValue];
            [locaObj addObject:locationObj];

        }

        callback(YES,[locaObj copy]);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        callback(NO,nil);
    }];
    
    [operation start];
   
}


-(NSDictionary*)getNowLocation
{
    return @{@"latitude":[NSNumber numberWithFloat:self.myLattitude],@"longitude":[NSNumber numberWithFloat:self.myLongtitude]};
}

-(BOOL)locationEnable
{
    if(![CLLocationManager locationServicesEnabled]) {
        return NO;
    }
    return YES;
}


@end
