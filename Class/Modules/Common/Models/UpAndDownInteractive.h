//
//  UpAndDownInteractive.h
//  Story17
//
//  Created by Racing on 2015/11/30.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpAndDownInteractive : UIPercentDrivenInteractiveTransition
@property (nonatomic) BOOL shouldComplete;

@end
