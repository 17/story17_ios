//
//  SuggestionObject.m
//  Story17
//
//  Created by POPO on 5/12/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "SuggestionObject.h"

@implementation SuggestionObject

@synthesize user;
@synthesize post1;
@synthesize post2;
@synthesize post3;

+(SuggestionObject*) getSuggestionObjWithDict:(NSDictionary*)dict
{
    
    SuggestionObject* sugObj = [SuggestionObject new];
    sugObj.user =  [UserObject getUserWithDict:dict];
    
    int flag = 0;
    
    for(NSDictionary* post in dict[@"pictures"]){
        switch (flag) {
            case 0:
                sugObj.post1 = [PostObject getPostWithDict:post];
                break;
            case 1:
                sugObj.post2 = [PostObject getPostWithDict:post];
                break;
            case 2:
                sugObj.post3 = [PostObject getPostWithDict:post];
                break;
            default:
                break;
        };
        flag+=1;
    }
    return sugObj;
}


@end
