//
//  AdObject.m
//  story17
//
//  Created by POPO Chen on 4/30/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "AdObject.h"

@implementation AdObject

+(AdObject *)getAdWithDict:(NSDictionary *)dict
{
    AdObject *ad = [[AdObject alloc] init];
    
    ad.adID = dict[@"adID"];
    ad.caption = dict[@"caption"];
    ad.actionName = dict[@"actionName"];
    ad.androidAppPackageName = dict[@"androidAppPackageName"];
    ad.iosAppID = dict[@"iosAppID"];
    ad.adUrl = dict[@"adUrl"];
    ad.urlScheme = dict[@"urlScheme"];
    ad.video = dict[@"video"];
    ad.headline = dict[@"headline"];
    ad.bidType = dict[@"bidType"];
    ad.clickPrice = [dict[@"clickPrice"] intValue];
    ad.installPrice = [dict[@"installPrice"] intValue];
    ad.impressionPrice = [dict[@"impressionPrice"] intValue];
    
    return ad;
}

@end
