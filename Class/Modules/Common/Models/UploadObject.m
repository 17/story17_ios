#import "UploadObject.h"
#import "AFNetworking.h"

@implementation UploadObject

@synthesize filename;
@synthesize uploadCallback;

#define S3_MAX_TRANSFER_TIMEOUT 10
#define  _WEAKREF(obj)         __weak typeof(obj)ref=obj;
#define  _STRONGREF(name)      __strong typeof(ref)name=ref;\ if(!name)return ;\

-(id) initWithFileName:(NSString *)file
{
    if (self = [super init]) {
        filename = file;
        _manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        _progress = nil;
    }
    
    return self;
}

-(void) startUploadWithCallback:(void (^)(NSString* fileName, int code, float progress)) callback
{
    if(self.filename==nil) {
        return;
    }
    
    uploadCallback = callback;

    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@uploadFile",UPLOAD_IP] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
  
        NSData* fileData = [NSData dataWithContentsOfFile:GET_LOCAL_FILE_PATH(filename)];
        NSString* mimeType = @"image/jpeg";
        
        if([filename hasSuffix:@".jpg"]) {
            mimeType = @"image/jpeg";
        } else if([filename hasSuffix:@".m4a"]) {
            mimeType = @"audio/mp3";
        } else if([filename hasSuffix:@".png"]) {
            mimeType = @"image/png";
        } else if([filename hasSuffix:@".mp4"]) {
            mimeType = @"video/mp4";
        }
        [formData appendPartWithFileData:fileData name:@"myfile" fileName:filename mimeType:mimeType];
    } error:nil];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy = securityPolicy;
    
    AFHTTPRequestOperation *operation =
    [manager HTTPRequestOperationWithRequest:request
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             if(uploadCallback!=nil)
                 uploadCallback(filename, UPLOAD_SUCCESS, 1.0);

         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

             if(error){
                 DLog(@"%@",error);
             }
             
             if(uploadCallback!=nil){
//                 [DIALOG_MANAGER showNetworkFailToast];
                 uploadCallback(filename, UPLOAD_FAIL, 0.0);
             }
    }];
    
    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                        long long totalBytesWritten,
                                        long long totalBytesExpectedToWrite) {
        if(uploadCallback!=nil)
            uploadCallback(filename, PROGRESSING, (float)totalBytesWritten/totalBytesExpectedToWrite);
    }];
    
    [operation start];
    
}

@end
