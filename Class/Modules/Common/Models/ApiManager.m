#import "ApiManager.h"

@implementation ApiManager

@synthesize manager;

-(id)init
{
    self = [super init];

    if (self) {
        manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:SERVER_IP]];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.requestSerializer.timeoutInterval = 20.0;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        manager.securityPolicy.allowInvalidCertificates = YES;
        manager.securityPolicy.validatesDomainName = NO;

    }
    
    return self;
}

#pragma mark - Register Related
-(void) loginAction:(NSDictionary*)params completion: (void (^)(BOOL success, NSString* message)) callback
{
    NSString* deviceID = [SINGLETON getUUID];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    [dict addEntriesFromDictionary:params];
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"loginAction"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString *message = decodedDict[@"message"];
            if ([self resultSuccess:decodedDict]) {
                [DEFAULTS setObject:decodedDict[@"accessToken"] forKey:ACCESS_TOKEN];
                [DEFAULTS setObject:deviceID forKey:DEVICE_ID];
                [DEFAULTS synchronize];
                [self saveUserDefaultsWithDict:decodedDict[@"userInfo"]];
                callback(YES, message);
            } else {
                callback(NO, message);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR);
    }];
}

-(void) loginAction2:(NSDictionary*)params completion: (void (^)(BOOL success, NSString* message)) callback
{
    NSString* deviceID = [SINGLETON getUUID];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    [dict addEntriesFromDictionary:params];
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"loginAction2"];

    DLog(@"dict:%@",dict);
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            DLog(@"decodingMiddleware");
            NSString *message = decodedDict[@"message"];
            if ([self resultSuccess:decodedDict]) {
                [DEFAULTS setObject:decodedDict[@"accessToken"] forKey:ACCESS_TOKEN];
                [DEFAULTS setObject:deviceID forKey:DEVICE_ID];
                [DEFAULTS synchronize];
                [self saveUserDefaultsWithDict:decodedDict[@"userInfo"]];
                callback(YES, message);
            } else {
                callback(YES, message);
            }
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR);
    }];
}

-(void) recoverPassword:(NSString*)openID email:(NSString*)email completion: (void (^)(BOOL success, NSString* message)) callback
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"openID"] = openID;
    dict[@"email"] = email;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"recoverPassword"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString *message = decodedDict[@"message"];
            if ([self resultSuccess:decodedDict]) {
                callback(YES, message);
            } else {
                callback(NO, message);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR);
    }];
}

-(void) checkOpenIDAvailable:(NSString*) openID completion: (void (^)(BOOL success, NSString* message)) callback
{
    NSMutableDictionary *dict = CREATE_MUTABLE_DICTIONARY;
    dict[@"openID"] = openID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"checkOpenIDAvailable"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString *message = decodedDict[@"message"];
            if ([self resultSuccess:decodedDict]) {
                callback(YES, message);
            } else {
                callback(NO, message);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR);
    }];
}

-(void) checkFacebookIDAvailable:(NSString*)facebookID completion: (void (^)(BOOL success, NSString* message)) callback
{
    NSMutableDictionary *dict = CREATE_MUTABLE_DICTIONARY;
    dict[@"facebookID"] = facebookID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"checkFacebookIDAvailable"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            NSString *message = decodedDict[@"message"];
            
            if ([self resultSuccess:decodedDict]) {
                callback(YES, message);
            } else {
                callback(NO, message);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR);
    }];
}

-(void)checkQQIDAvailable:(NSString*)qqID completion: (void (^)(BOOL success, NSString* message)) callback
{
    NSMutableDictionary *dict = CREATE_MUTABLE_DICTIONARY;
    dict[@"qqID"] = qqID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"checkQQIDAvailable"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            NSString *message = decodedDict[@"message"];
            if ([self resultSuccess:decodedDict]) {
                callback(YES, message);
            } else {
                callback(NO, message);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR);
    }];
}

-(void) checkWeiboIDAvailable:(NSString*)weiboID completion: (void (^)(BOOL success, NSString* message)) callback
{
    NSMutableDictionary *dict = CREATE_MUTABLE_DICTIONARY;
    dict[@"weiboID"] = weiboID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"checkWeiboIDAvailable"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            NSString *message = decodedDict[@"message"];
            
            if ([self resultSuccess:decodedDict]) {
                callback(YES, message);
            } else {
                callback(NO, message);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR);
    }];
}

-(void) checkWechatIDAvailable:(NSString*)wechatID completion: (void (^)(BOOL success, NSString* message)) callback
{
    NSMutableDictionary *dict = CREATE_MUTABLE_DICTIONARY;
    dict[@"wechatID"] = wechatID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"checkWechatIDAvailable"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            NSString *message = decodedDict[@"message"];
            
            if ([self resultSuccess:decodedDict]) {
                callback(YES, message);
            } else {
                callback(NO, message);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR);
    }];
}

-(void) checkTwitterIDAvailable:(NSString*)twitterID completion: (void (^)(BOOL success, NSString* message)) callback
{
    NSMutableDictionary *dict = CREATE_MUTABLE_DICTIONARY;
    dict[@"twitterID"] = twitterID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"checkTwitterIDAvailable"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            NSString *message = decodedDict[@"message"];
            
            if ([self resultSuccess:decodedDict]) {
                callback(YES, message);
            } else {
                callback(NO, message);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR);
    }];
}

-(void) registerAction: (NSDictionary*) params completion: (void (^)(BOOL success, NSString* message)) callback
{
    NSString* deviceID = [SINGLETON getUUID];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    [dict addEntriesFromDictionary:params];
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"registerAction"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString *message = decodedDict[@"message"];
            
            if ([self resultSuccess:decodedDict]) {
                
                [DEFAULTS setObject:decodedDict[@"accessToken"] forKey:ACCESS_TOKEN];
                [DEFAULTS setObject:deviceID forKey:DEVICE_ID];
                [DEFAULTS synchronize];
                [self saveUserDefaultsWithDict:decodedDict[@"userInfo"]];
                
                callback(YES, message);
            } else {
                callback(YES, message);
            }

        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR);
    }];
}

-(void) registerAction2: (NSDictionary*) params completion: (void (^)(BOOL success, NSString* message)) callback
{
    
//    DLog(@"register2");
    NSString* deviceID = [SINGLETON getUUID];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    [dict addEntriesFromDictionary:params];
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"registerAction2"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString *message = decodedDict[@"message"];
            
            if ([self resultSuccess:decodedDict]) {
                
                [DEFAULTS setObject:decodedDict[@"accessToken"] forKey:ACCESS_TOKEN];
                [DEFAULTS setObject:deviceID forKey:DEVICE_ID];
                [DEFAULTS synchronize];
                
                [self saveUserDefaultsWithDict:decodedDict[@"userInfo"]];
                
                callback(YES, message);
            } else {
                callback(NO, message);
            }
            
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR);
    }];
}

-(void) sendVerificationCode:(NSString*)countryCode andPhoneNum:(NSString*)phoneNumber completion:(void(^)(BOOL success, NSString* message)) callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"countryCode"] = countryCode;
    dict[@"phoneNumber"] = phoneNumber;
    
    NSDictionary *packDict = [self dictPackageEncoding:dict withAction:@"sendVerificationCode"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString *message = decodedDict[@"message"];
            
            NSLog(@"傳送驗證碼 : %@ ",message);

            if ([self resultSuccess:decodedDict]) {
                callback(YES, message);
            } else {
                callback(NO, message);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR);
    }];
}

-(void) isPhoneNumberUsed:(NSString*)countryCallingCode andPhoneNum:(NSString*)localPhoneNumber completion:(void(^)(BOOL success, NSString* message,BOOL isUsed)) callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"countryCallingCode"] = countryCallingCode;
    dict[@"localPhoneNumber"] = localPhoneNumber;
    
    NSDictionary *packDict = [self dictPackageEncoding:dict withAction:@"isPhoneNumberUsed"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString *message = decodedDict[@"message"];
            BOOL isUsed = [decodedDict[@"isUsed"] isEqualToString:@"yes"]? true :false;
            
            if ([self resultSuccess:decodedDict]) {
                callback(YES, message,isUsed);
            } else {
                callback(NO, message,isUsed);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR,false);
    }];
}


-(void)sendPhoneVerificationCode:(NSString*)countryCallingCode phoneNumber:(NSString*)phoneNumber type:(NSString*)type completion:(void(^)(BOOL success, NSString *message , NSString *requestId)) callback{

    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    
    dict[@"countryCallingCode"] = countryCallingCode;
    dict[@"phoneNumber"] = phoneNumber;
    dict[@"openID"] = GET_DEFAULT(USER_OPEN_ID);
    dict[@"requestID"] = [SINGLETON getUUID];
    NSString *key = @"ce7c4cb6-ac5a-4b86-9798-26ce3016e599";
    NSString *signature = [NSString stringWithFormat:@"%@%@%@%@%@",dict[@"requestID"],dict[@"language"],countryCallingCode,phoneNumber,key];
    signature = [signature MD5];
    dict[@"signature"] = signature;
    dict[@"type"] = type;
    
    
    NSDictionary *packDict = [self dictPackageEncoding:dict withAction:@"sendPhoneVerificationCode"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString *message = decodedDict[@"message"];
            
            NSLog(@"發送簡訊 API :%@" ,decodedDict);
            if ([self resultSuccess:decodedDict]) {
                callback(YES, message, dict[@"requestID"]);
            } else {
                callback(NO, message, dict[@"requestID"]);
            }
        }];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,@"",@"");
        NSLog(@"傳送失敗:%@ ",error);
        
    }];
    
}

-(void) isPhoneNumberOpenIDPaired:(NSString*)countryCallingCode localPhoneNumber:(NSString*)localPhoneNumber openID:(NSString*)openID completion:(void(^)(BOOL success, NSString *message , BOOL isPaired))callback
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];

    
    dict[@"countryCallingCode"] = countryCallingCode;
    dict[@"localPhoneNumber"] = localPhoneNumber;
    dict[@"openID"] = openID;
    
    NSDictionary *packDict = [self dictPackageEncoding:dict withAction:@"isPhoneNumberOpenIDPaired"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            NSString* message = decodedDict[@"message"];
            BOOL isPaired  = [decodedDict[@"isPaired"] isEqualToString:@"yes"]? YES:NO;
            
            if ([self resultSuccess:decodedDict]) {
 
                callback(YES, message,isPaired);
            } else {
                callback(NO, message,isPaired);
                
            }
        }];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        callback(NO,nil,NO);
        
    }];
    
}

-(void)registerByPhoneAction:(NSString*)openID password:(NSString*)password requestID:(NSString*)requestID countryCode:(NSString*)countryCode phoneNumber:(NSString*)phoneNumber verificationCode:(NSString*)verificationCode phoneCountry:(NSString*)phoneCountry  optionalDict:(NSDictionary*)optionalDict completion:(void(^)(BOOL success, NSString *message , NSString *accesToken , NSDictionary *userInfo)) callback {
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
 
    for(NSString* key in [optionalDict allKeys]){
        dict[key] = [optionalDict objectForKey:key];
    }

    DLog(@"optionalDict:%@",optionalDict);
    DLog(@"dict:%@",dict);
    dict[@"password"] = password;
    dict[@"picture"] = GET_DEFAULT(PICTURE);
    dict[@"openID"] = openID;
    dict[@"countryCallingCode"] = countryCode;
    dict[@"localPhoneNumber"] = phoneNumber;
    dict[@"requestID"] = requestID;
    dict[@"verificationCode"] = verificationCode;
    dict[@"phoneTwoDigitISO"] = phoneCountry;
    
    NSDictionary *packDict = [self dictPackageEncoding:dict withAction:@"registerByPhoneAction"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString *message = decodedDict[@"message"];
            NSString *accessToken = decodedDict[@"accessToken"];
            NSDictionary *userInfoDic = decodedDict[@"userInfo"];
            
            DLog(@"registerByPhoneAction : %@",decodedDict);
            
            if ([self resultSuccess:decodedDict]) {
                
                [DEFAULTS setObject:decodedDict[@"accessToken"] forKey:ACCESS_TOKEN];
                [DEFAULTS synchronize];
                [self saveUserDefaultsWithDict:decodedDict[@"userInfo"]];
                
                callback(YES, message,accessToken,userInfoDic);
            } else {
                callback(NO, message,nil,nil);
                NSLog(@"簡訊驗證失敗，原因：%@" ,message);

               
            }
        }];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        callback(NO,nil,nil,nil);
        NSLog(@"簡訊驗證傳送 server 失敗：%@" ,error);

    }];
    
}

- (void)humanTestSeconderyValidateWithType:(NSString *)type result:(NSDictionary *)result completion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    [dict addEntriesFromDictionary:result];
    dict[@"type"] = type;
    
    NSDictionary* packDict = [self dictPackageEncoding:[result mutableCopy] withAction:@"validateHumanTestResponse"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            if ([self resultSuccess:decodedDict]) {
                callback(YES);
            } else {
                callback(NO);
            }
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"client captcha response error:%@", error.localizedDescription);
        
        callback(NO);
    }];
}

- (void)getResetPasswordToken:(NSString*)countryCode openID:(NSString*)openID requestID:(NSString*)requestID phoneNumber:(NSString*)phoneNumber verificationCode:(NSString*)verificationCode  withCompletion:(void(^)(BOOL success, NSString *message ,NSString *resetPasswordToken))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"countryCallingCode"] = countryCode;
    dict[@"localPhoneNumber"] = phoneNumber;
    dict[@"openID"] = openID;
    dict[@"requestID"] = requestID;
    dict[@"verificationCode"] = verificationCode;

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getResetPasswordToken"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {

            NSLog(@"拿重設密碼token API: %@",decodedDict);

            if ([self resultSuccess:decodedDict]) {
                callback(YES, decodedDict[@"message"] ,decodedDict[@"resetPasswordToken"]);
            } else {
                callback(NO, decodedDict[@"message"], nil);
                // verificationCode_error/verificationCode_timeout/ip_rate_limit_exceeds/old_password_error/new_password_confirm_error
            }
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, @"",nil);
        NSLog(@"拿重設密碼token 失敗 無法routing進去");

    }];
}


- (void)resetPassword:(NSString*)resetPasswordToken newPassword:(NSString*)newPassword confirmNewPassword:(NSString*)confirmNewPassword withCompletion:(void(^)(BOOL success, NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"resetPasswordToken"] = resetPasswordToken;
    dict[@"newPassword"] = [newPassword MD5];
    dict[@"confirmNewPassword"] = [confirmNewPassword MD5];
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"resetPassword"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            NSLog(@"重設密碼 API: %@",decodedDict);

            if ([self resultSuccess:decodedDict]) {
                callback(YES, decodedDict[@"message"]);
            } else {
                callback(NO, decodedDict[@"message"]);
                // resetPasswordToken_invalid/resetPasswordToken_timeout/old_password_error/new_password_confirm_error
            }
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"重設密碼失敗 無法routing進去");
        callback(NO, @"");
    }];
}

-(void)changePhoneNumber:(NSString*)openID  requestID:(NSString*)requestID countryCode:(NSString*)countryCode phoneNumber:(NSString*)phoneNumber verificationCode:(NSString*)verificationCode phoneCountry:(NSString*)phoneCountry completion:(void(^)(BOOL success, NSString *message )) callback {
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    
    dict[@"openID"] = openID;
    dict[@"countryCallingCode"] = countryCode;
    dict[@"localPhoneNumber"] = phoneNumber;
    dict[@"requestID"] = requestID;
    dict[@"verificationCode"] = verificationCode;
    dict[@"phoneTwoDigitISO"] = phoneCountry;
    
    NSLog(@"改變電話號碼參數: %@ ",dict);
    
    NSDictionary *packDict = [self dictPackageEncoding:dict withAction:@"changePhoneNumber"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
       
            NSString *message = decodedDict[@"message"];

            NSLog(@"綁約電話 API: %@",decodedDict);
            
            if ([self resultSuccess:decodedDict]) {
                
//                [self saveUserDefaultsWithDict:decodedDict[@"userInfo"]];
                
                callback(YES, message);
            } else {
                
                NSLog(@"綁約電話失敗，原因:%@ ",message);
                callback(NO, message);
                
            }
        }];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"綁約電話失敗 無法routing進去");

        callback(NO,nil);
        
    }];
}

- (void)checkHumanTestWithType:(NSString *)type completion:(void(^)(BOOL success, BOOL isNeeded))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"type"] = type;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"checkNeedHumanTest"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            if ([self resultSuccess:decodedDict]) {
                callback(YES, [decodedDict[@"isNeeded"] boolValue]);
            }
            else {
                callback(YES, [decodedDict[@"isNeeded"] boolValue]);
                
            }
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NO);
        NSLog(@"檢查 human test 連線失敗");
    }];
}

#pragma mark - User Related
-(void) getUserInfo:(NSString*)targetOpenID completion:(void (^)(BOOL success, UserObject* userObject)) callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetOpenID"] = targetOpenID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getUserInfo"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {

        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            callback(YES, [UserObject getUserWithDict:decodedDict]);

//            if ([self resultSuccess:decodedDict]) {
//                callback(YES, [UserObject getUserWithDict:decodedDict]);
//            }else{
//                callback(NO, nil);
//            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];

}

-(void) isUserOnLiveStream:(NSString*)targetUserID completion:(void (^)(BOOL success, int userIsOnLive, NSString* livestreamID)) callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"isUserOnLiveStream"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            callback(YES, [decodedDict[@"userIsOnLive"] intValue],decodedDict[@"liveStreamID"]);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, 0,@"");
    }];
    
}

-(void) getUserInfoByUserID:(NSString*)targetUserID completion:(void (^)(BOOL success, UserObject* userObject)) callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getUserInfoByUserID"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES, [UserObject getUserWithDict:decodedDict]);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
    
}


-(void) getSelfInfo:(void (^)(BOOL success)) callback{
        
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getSelfInfo"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            [self saveUserDefaultsWithDict:decodedDict];
            callback(YES);
        }];

    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) getCountry:(void (^)(BOOL success, NSString* country)) callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getCountry"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES, decodedDict[@"country"]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
    
}

-(void) getFollower:(NSString*)targetUserID beforeTime:(int)beforeTime andCount:(int)count withCompletion:(void(^)(BOOL success,NSArray* dict))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getFollower"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* users = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                UserObject* user = [UserObject getUserWithDict:dict];
                if(user!=nil)
                    [users addObject:user];
            }
            callback(YES,  [users copy]);
        }];
       
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void) getFollowing:(NSString*)targetUserID beforeTime:(int)beforeTime andCount:(int)count withCompletion:(void(^)(BOOL success,NSArray* dict))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getFollowing"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* users = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                UserObject* user = [UserObject getUserWithDict:dict];
                if(user!=nil)
                    [users addObject:user];
            }
            callback(YES,  [users copy]);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
    
}

-(void)getFollowRequests:(int)count beforeTime:(int)beforeTime withCompletion:(void(^)(BOOL success,NSArray* dict))callback{

    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"count"] = INT_TO_STRING(count);
    dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getFollowRequests"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* users = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                UserObject* user = [UserObject getUserWithDict:dict];
                if(user!=nil)
                    [users addObject:user];
            }
            callback(YES,  [users copy]);
        }];

    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       callback(NO,nil);
   }];
}

-(void)sendFollowRequest:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"sendFollowRequest"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)cancelFollowRequests:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"cancelFollowRequests"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)acceptFollowRequest:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"acceptFollowRequest"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)rejectFollowRequest:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"rejectFollowRequest"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) followUserAction:(NSString*)targetUserID withCompletion:(void (^)(BOOL success)) callback{
    if (targetUserID!=nil) {
        [EVENT_HANDLER addEventTracking:@"FollowUser" withDict:@{@"targetUserID":targetUserID}];
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"followUserAction"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }
   failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       callback(NO);
   }];
}

-(void) followAllAction:(NSString*)followedUserIDs withCompletion:(void (^)(BOOL success)) callback{

    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"followedUserIDs"] = followedUserIDs;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"followAllAction"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }
   failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       callback(NO);
   }];
}

-(void) unfollowUserAction:(NSString*)targetUserID withCompletion:(void (^)(BOOL success)) callback{
    [EVENT_HANDLER addEventTracking:@"UnfollowUser" withDict:@{@"followedUserID":targetUserID}];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"unfollowUserAction"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }
   failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       callback(NO);
   }];
}

-(void) unblockUserAction:(NSString*)unblockUserID withCompletion:(void (^)(BOOL success)) callback{
    if (unblockUserID!=nil) {
        [EVENT_HANDLER addEventTracking:@"UnblockUser" withDict:@{@"blockedUserID":unblockUserID}];
    }
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"unblockUserID"] = unblockUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"unblockUserAction"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}
-(void) blockUserAction:(NSString*)blockUserID withCompletion:(void (^)(BOOL success)) callback{
    [EVENT_HANDLER addEventTracking:@"BlockUser" withDict:@{@"blockedUserID":blockUserID}];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"blockedUserID"] = blockUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"blockUserAction"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          callback(NO);
    }];
}

-(void) reportLiveAction:(LiveStreamObject*)livestream withReason:(NSString*)reason withCompletion:(void (^)(BOOL success , NSString* message)) callback
{
    [EVENT_HANDLER addEventTracking:@"ReportLive" withDict:nil];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"reportedUserID"] = livestream.user.userID;
    dict[@"livestreamID"] = livestream.liveStreamID;
    dict[@"reason"] = reason;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"reportLiveAction"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            NSString *message = decodedDict[@"message"];
            callback(YES, message);
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,NETWORK_ERROR);
    }];

}

-(void) reportUserAction:(NSString*)reportedUserID withReason:(NSString*)reason andMessage:(NSString*)message withCompletion:(void (^)(BOOL success , NSString* message)) callback{
    [EVENT_HANDLER addEventTracking:@"ReportUser" withDict:nil];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"reportedUserID"] = reportedUserID;
    dict[@"reason"] = reason;
    dict[@"message"] = message;

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"reportUserAction"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            [DEFAULTS setObject:@0 forKey:UPDATE_FEED_TIME];
            [DEFAULTS setObject:@0 forKey:UPDATE_SEARCH_TIME];
            [DEFAULTS setObject:@0 forKey:UPDATE_USER_PROFILE];
            [DEFAULTS synchronize];
//            [[DatabaseManager sharedInstance] insertReportRecord:reportedUserID type:@"userID"];
            NSString *message = decodedDict[@"message"];
            callback(YES, message);
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,NETWORK_ERROR);
    }];
}

-(void) reportPostAction:(NSString*)reportedUserID withReason:(NSString*)reason andPostID:(NSString*)postID withCompletion:(void (^)(BOOL success , NSString* message)) callback{
    
    [EVENT_HANDLER addEventTracking:@"ReportPost" withDict:nil];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"reportedUserID"] = reportedUserID;
    dict[@"reason"] = reason;
    dict[@"message"] = @"";
    dict[@"postID"] = postID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"reportPostAction"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString *message = decodedDict[@"message"];
            if ([self resultSuccess:decodedDict]) {
                [DEFAULTS setObject:@0 forKey:UPDATE_FEED_TIME];
                [DEFAULTS setObject:@0 forKey:UPDATE_SEARCH_TIME];
                [DEFAULTS setObject:@0 forKey:UPDATE_USER_PROFILE];
                [DEFAULTS synchronize];
//                [[DatabaseManager sharedInstance] insertReportRecord:postID type:@"postID"];

                callback(YES, message);
            } else {
                callback(NO, message);
            }
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,NETWORK_ERROR);
    }];
}

-(void) reportCommentAction:(NSString*)reportedUserID withReason:(NSString*)reason andCommentID:(NSString*)commentID withCompletion:(void (^)(BOOL success , NSString* message)) callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"reportedUserID"] = reportedUserID;
    dict[@"reason"] = reason;
    dict[@"commentID"] = commentID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"reportCommentAction"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString *message = decodedDict[@"message"];
            callback(YES, message);
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,NETWORK_ERROR);
    }];
}

-(void) updateUserInfo:(NSDictionary*)dic fetchSelfInfo:(BOOL)fetch completion:(void(^)(BOOL success))callback
{
    
//    DLog(@"updateUserInfo");
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    
    for(NSString* key in [dic allKeys]){
        dict[key] = [dic objectForKey:key];
    }
    
//    DLog(@"dict:%@",dict);

    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"updateUserInfo"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if(fetch){
            [API_MANAGER getSelfInfo:^(BOOL success) {
                if(success){
                    callback(YES);
                }else{
                    callback(NO);
                }
            }];
        }else{
            callback(YES);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) updateOpenID:(NSString*)openID completion:(void(^)(BOOL success))callback
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"openID"] = openID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"updateOpenID"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [API_MANAGER getSelfInfo:^(BOOL success) {
            if(success){
                callback(YES);
            }else{
                callback(NO);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) checkLoginWithCompletion: (void(^) (BOOL success, NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"checkLogin"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            if(decodedDict==nil) {
                return;
            }
            if(![decodedDict isKindOfClass:[NSDictionary class]]) {
                return;
            }
            NSString *message = decodedDict[@"result"];
            callback(YES, message);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR);
    }];
}

-(void) getSearchUsers: (NSString*)query followingOnly:(int)followingOnly offset:(int)offset count:(int)count completion:(void (^)(BOOL success, NSArray* userObjects)) callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"query"] =query ;
    dict[@"followingOnly"] =INT_TO_STRING(followingOnly) ;
    dict[@"offset"] =INT_TO_STRING(offset) ;
    dict[@"count"] =INT_TO_STRING(count) ;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getSearchUsers"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* userObjects = CREATE_MUTABLE_ARRAY;
           
            for(NSDictionary* dict in decodedDict) {
                UserObject* user = [UserObject getUserWithDict:dict];
                if(user!=nil)
                    [userObjects addObject:user];
            }
            
            callback(YES, userObjects);

        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, @[]);
    }];
}

-(void) getSuggestedUsers:(int)offset count:(int)count completion:(void (^)(BOOL success, NSArray* suggestedUser)) callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"offset"] =INT_TO_STRING(offset) ;
    dict[@"count"] =INT_TO_STRING(count) ;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getSuggestedUsers"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* suggestedUser = CREATE_MUTABLE_ARRAY;
            
            for(NSDictionary* suggestion in decodedDict) {
                [suggestedUser addObject:[SuggestionObject getSuggestionObjWithDict:suggestion]];
            }
            callback(YES, suggestedUser);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, @[]);
    }];
}

-(void) getExploreSuggestedUsers:(int)beforeTime count:(int)count completion:(void (^)(BOOL success, NSArray* suggestedUser)) callback
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"beforeTime"] =INT_TO_STRING(beforeTime) ;
    dict[@"count"] =INT_TO_STRING(count) ;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getExploreSuggestedUsers"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* suggestedUser = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* suggestion in decodedDict) {
                [suggestedUser addObject:[SuggestionObject getSuggestionObjWithDict:suggestion]];
            }
            callback(YES, suggestedUser);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, @[]);
    }];
}

-(void) getFriendSuggestion:(NSString*)friendUserID offset:(int)offset count:(int)count completion:(void (^)(BOOL success, NSArray* userObjects)) callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"friendUserID"] = friendUserID;
    dict[@"offset"] =INT_TO_STRING(offset) ;
    dict[@"count"] =INT_TO_STRING(count) ;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getFriendSuggestion"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* suggestedUser = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* suggestion in decodedDict) {
                UserObject* user = [UserObject getUserWithDict:suggestion];
                if(user!=nil)
                    [suggestedUser addObject:user];
            }
            callback(YES, suggestedUser);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, @[]);
    }];
}


-(void) getHotUsers:(int)offset count:(int)count completion:(void (^)(BOOL success, NSArray* suggestedUser)) callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"offset"] =INT_TO_STRING(offset) ;
    dict[@"count"] =INT_TO_STRING(count) ;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getHotUsers"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* suggestedUser = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* suggestion in decodedDict) {
                [suggestedUser addObject:[SuggestionObject getSuggestionObjWithDict:suggestion]];
            }
            callback(YES, suggestedUser);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, @[]);
    }];
}

-(void) adminBlockDirtyWordAction:(NSString*)messageString withCompletion:(void(^)(BOOL success))callback
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"messageString"] = messageString;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"adminBlockDirtyWordAction"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) killLiveStreamAction:(NSString*)liveStreamID targetUserID:(NSString*)targetUserID withCompletion:(void (^)(BOOL success))callback
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    dict[@"liveStreamID"] = liveStreamID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"killLiveStreamAction"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];

}

-(void) adminBlockUserAction:(NSString*)userID publisherUserID:(NSString*)publisherUserID blockedUserID:(NSString*)blockedUserID inLivestreamID:(NSString*)inLivestreamID  withCompletion:(void(^)(BOOL success))callback
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"userID"] = userID;
    dict[@"publisherUserID"] = publisherUserID;
    dict[@"blockedUserID"] = blockedUserID;
    dict[@"inLivestreamID"] = inLivestreamID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"adminBlockUserAction"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }
   failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       callback(NO);
   }];
}


#pragma mark Notification Related 
-(void) getNotif:(int)beforeTime andCount:(int)count withCompletion:(void(^)(BOOL success,NSArray* dict))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getNotif"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
           
            if(decodedDict==nil){
            }
            
            NSMutableArray* notifications = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [notifications addObject:[NotificationObject getNotifWithDict:dict]];
            }
            dispatch_async(MAIN_QUEUE, ^{
                callback(YES,  [notifications copy]);
            });
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void) getFriendNotif:(int)beforeTime andCount:(int)count withCompletion:(void(^)(BOOL success,NSArray* dict))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getFriendNotif"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            dispatch_async(GLOBAL_QUEUE, ^{
                NSMutableArray* notifications = CREATE_MUTABLE_ARRAY;
                for(NSDictionary* dict in decodedDict) {
                    [notifications addObject:[NotificationObject getNotifWithDict:dict]];
                }
                dispatch_async(MAIN_QUEUE, ^{
                    callback(YES,  [notifications copy]); 
                });
            });
            
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

- (void) sendPaymentPushSystemNotif
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"sendPaymentPushSytemNotif"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"success");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"fail");
    }];
}

-(void) getSystemNotif:(int)beforeTime andCount:(int)count withCompletion:(void(^)(BOOL success,NSArray* dict))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getSystemNotif"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            NSMutableArray* notifications = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [notifications addObject:[NotificationObject getNotifWithDict:dict]];
            }
            callback(YES,  [notifications copy]);
            
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)readSystemNotifWithCompletion:(void(^)(BOOL success))callback{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"readSystemNotif"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)readNotif:(void(^)(BOOL success))callback{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"readNotif"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

#pragma mark - Post Related
-(void) getPostFeed:(int)beforeTime andCount:(int)count fetchLiveStreamL:(int)fetchLiveStream withCompletion:(void(^)(BOOL success,NSArray* posts))callback
{
    dispatch_async(GLOBAL_QUEUE, ^{
        NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
        dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
        dict[@"count"] = INT_TO_STRING(count);
        dict[@"apiVersion"] = @"v2";
        dict[@"fetchLiveStream"] = INT_TO_STRING(fetchLiveStream);
        
        NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getPostFeed"];
        
        [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self decodingMiddleware:responseObject withCompletion:^(NSDictionary* decodedDict) {
                if([decodedDict count]==0) {
                    callback(YES,@[]);
                } else {
                    NSMutableArray* feedObj = CREATE_MUTABLE_ARRAY;
                    
                    // v2 version
                    for(NSDictionary* dict in decodedDict[@"liveStreams"]) {
                        [feedObj addObject: [LiveStreamObject getLiveStreamWithDict:dict]];
                    }
                    for(NSDictionary* dict in decodedDict[@"posts"]) {
                        [feedObj addObject:[PostObject getPostWithDict:dict]];
                    }
                    
                    
                    callback(YES, [feedObj copy]);
                }
            }];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            callback(NO, nil);
        }];
    });
}

-(void) getUserPost:(NSString *)targetUserID beforeTime:(int)beforeTime andCount:(int)count withCompletion:(void(^)(BOOL success,NSArray* posts))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getUserPost"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
                NSMutableArray* posts = CREATE_MUTABLE_ARRAY;
                for(NSDictionary* dict in decodedDict) {
                    [posts addObject:[PostObject getPostWithDict:dict]];
                }
                callback(YES,  [posts copy]);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void) getPostInfo:(NSString*)postID withCompletion:(void(^)(BOOL success, PostObject* post))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postID"] = postID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getPostInfo"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {

        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            PostObject* post;
            post = [PostObject getPostWithDict:decodedDict];
            callback( YES, post);
        }];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void) publishPost:(NSDictionary*)params withCompletion:(void(^)(BOOL success,NSString* message, PostObject* post))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];

    dict[@"caption"] = params[@"caption"];
    dict[@"taggedUsers"] = params[@"taggedUsers"];  // jsonString
//    dict[@"videoSegments"] = INT_TO_STRING((int)params[@"videoSegments"]);
    dict[@"picture"] = params[@"picture"];
    dict[@"locationName"] = params[@"locationName"];
    dict[@"locationID"] = params[@"locationID"];
    dict[@"userTags"] = params[@"userTags"];
    dict[@"hashTags"] = params[@"hashTags"];
    dict[@"longitude"] = params[@"longitude"];
    dict[@"latitude"] = params[@"latitude"];
    dict[@"video"] = params[@"video"];
    dict[@"type"] = params[@"type"];
    dict[@"canComment"] = params[@"canComment"];
    dict[@"reachability"] = params[@"reachability"];

    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"publishPost"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString *message = decodedDict[@"message"];
            if ([self resultSuccess:decodedDict]) {
                callback(YES, message ,[PostObject getPostWithDict:[decodedDict[@"postInfo"] mj_JSONObject]]);
            } else {
                callback(NO, message,nil);
            }
        }];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,NETWORK_ERROR, nil);
    }];
}

-(void) updatePost:(NSDictionary*)params withCompletion:(void(^)(BOOL success,NSString* message, PostObject* post))callback
{
    
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];

    [dict addEntriesFromDictionary:params];

//    dict[@"postID"] = params[@"postID"];
//    dict[@"caption"] = params[@"caption"];
//    dict[@"taggedUsers"] = params[@"taggedUsers"];  // jsonString
//    dict[@"picture"] = params[@"picture"];
//    dict[@"locationName"] = params[@"locationName"];
//    dict[@"locationID"] = params[@"locationID"];
//    dict[@"userTags"] = params[@"userTags"];
//    dict[@"hashTags"] = params[@"hashTags"];
//    dict[@"longitude"] = params[@"longitude"];
//    dict[@"latitude"] = params[@"latitude"];
//    dict[@"video"] = params[@"video"];
//    dict[@"type"] = params[@"type"];
//    dict[@"canComment"] = params[@"canComment"];
//    dict[@"reachability"] = params[@"reachability"];
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"updatePost"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString *message = decodedDict[@"message"];
            if ([self resultSuccess:decodedDict]) {
                
                callback(YES, message ,[PostObject getPostWithDict:[decodedDict[@"postInfo"] mj_JSONObject]]);

//                callback(YES, message ,[PostObject getPostWithDict:[decodedDict[@"postInfo"] objectFromJSONString]]);
            } else {
                callback(NO, message,nil);
            }
        }];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,NETWORK_ERROR, nil);
    }];
}

-(void) getPostLikers:(NSString*)postID offset:(int)offset count:(int)count withCompletion:(void(^)(BOOL success, NSArray* userObjects))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postID"] = postID;
    dict[@"offset"] = INT_TO_STRING(offset);
    dict[@"count"] = INT_TO_STRING(count);

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getPostLikers"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* users = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict){
                UserObject* user = [UserObject getUserWithDict:dict];
                if(user!=nil)
                    [users addObject:user];
            }
            
            callback(YES,users);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void) getUserLikers:(NSString*)targetUserID offset:(int)offset count:(int)count withCompletion:(void(^)(BOOL success, NSArray* userObjects))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    dict[@"offset"] = INT_TO_STRING(offset);
    dict[@"count"] = INT_TO_STRING(count);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getUserLikers"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* users = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict){
                UserObject* user = [UserObject getUserWithDict:dict];
                if(user!=nil)
                    [users addObject:user];
            }
            
            callback(YES,users);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}


-(void) likePost:(NSString*)postID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postID"] = postID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"likePost"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) unlikePost:(NSString*)postID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postID"] = postID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"unlikePost"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) likePostBatchUpdate:(NSString*)postID likeCount:(int)likeCount withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postID"] = postID;
    dict[@"likeCount"] = INT_TO_STRING(likeCount);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"likePostBatchUpdate"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) commentPost:(NSString*)postID userTags:(NSString*)userTags comment:(NSString*)comment withCompletion:(void(^)(BOOL success,NSString* message, NSString* timestamp,NSString* commentID))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postID"] = postID;
    dict[@"userTags"] = userTags;
    dict[@"comment"] = comment;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"commentPost"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {

        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString* message = decodedDict[@"message"];
            NSString* commentID = decodedDict[@"commentID"];
            if ([self resultSuccess:decodedDict]) {
                callback(YES, message,decodedDict[@"timestamp"],commentID);
            } else {
                callback(NO, message,nil,nil);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,NETWORK_ERROR,nil,nil);
    }];
}

-(void) getComments:(NSString*)postID beforeTime:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* comments))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postID"] = postID;
    dict[@"beforeTime"] =INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getComments"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {

        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* comments = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict){
                [comments addObject:[CommentObject getCommentWithDict:dict]];
            }
            callback(YES,comments);
        }];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void) deleteComment:(NSString*)commentID postID:(NSString*)postID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"commentID"] = commentID;
    dict[@"postID"] = postID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"deleteComment"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) deletePost:(NSString*)postID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postID"] = postID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"deletePost"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) clearPostComment:(NSString*)postID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postID"] = postID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"clearPostComment"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) viewPost:(NSString*)postIDs withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postIDs"] = postIDs; // JSON
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"viewPost"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) getPostByHashTag:(NSString*)hashTag beforeTime:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* posts))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"hashTag"] = hashTag;
    dict[@"beforeTime"] =INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getPostByHashTag"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* posts = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict){
                [posts addObject:[PostObject getPostWithDict:dict]];
            }
            callback(YES,posts);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void) getPostByLocation:(NSString*)locationID beforeTime:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* posts))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"locationID"] = locationID;
    dict[@"beforeTime"] =INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getPostByLocation"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {

        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* posts = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict){
                [posts addObject:[PostObject getPostWithDict:dict]];
            }
            callback(YES,posts);
        }];
       
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void) getHotPost:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* posts))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"beforeTime"] =INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getHotPost"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
      
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {

            NSMutableArray* posts = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [posts addObject:[PostObject getPostWithDict:dict]];
            }
            callback(YES,  [posts copy]);

        }];
  
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void) getLikedPost:(NSString*)targetUserID beforeTime:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* posts))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    dict[@"beforeTime"] =INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLikedPost"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* posts = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict){
                [posts addObject:[PostObject getPostWithDict:dict]];
            }
            callback(YES,posts);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void) searchHashTag:(NSString*)query offset:(int)offset count:(int)count withCompletion:(void(^)(BOOL success, NSArray* tags))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"query"] = query;
    dict[@"offset"] =INT_TO_STRING(offset);
    dict[@"count"] = INT_TO_STRING(count);
    
    DLog(@"query:%@",query);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"searchHashTag"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* tags = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict){
                [tags addObject:[TagObject getTagWithDict:dict]];
            }
            callback(YES,tags);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}


-(void) createLocation:(NSString*)name latitude:(int)latitude longitude:(int)longitude withCompletion:(void(^)(BOOL success, NSString* locationID))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"name"] = name;
    dict[@"latitude"] =INT_TO_STRING(latitude);
    dict[@"longitude"] = INT_TO_STRING(longitude);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"createLocation"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString* locationID = decodedDict[@"locationID"];
            callback(YES,locationID);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,@"");
    }];
}

-(void) searchLocation:(NSString*)query offset:(int)offset count:(int)count withCompletion:(void(^)(BOOL success, NSArray* tags))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"query"] = query;
    dict[@"offset"] =INT_TO_STRING(offset);
    dict[@"count"] = INT_TO_STRING(count);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"searchLocation"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* locations = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict){
                [locations addObject: [LocationObject getLocationWithDict:dict]];
            }
            callback(YES,locations);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void) repostAction:(NSString*)postID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postID"] = postID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"repostAction"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) removeRepostAction:(NSString*)postID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postID"] = postID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"removeRepostAction"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}


-(void) getAds:(int)offset count:(int)count withCompletion:(void(^)(BOOL success,NSArray* adArray))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"offset"] =INT_TO_STRING(offset);
    dict[@"count"] = INT_TO_STRING(count);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getAds"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* ads = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict){
                [ads addObject:[AdObject getAdWithDict:dict]];
            }
            callback(YES,ads);
        }];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}


-(void) adTrack:(NSString*)data withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"data"] = data; // (json array: [{type: , adID: , bidType: , clickPrice, impressionPrice, installPrice}])
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"adTrack"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}


#pragma mark - Payment Related
-(void) sendSysNotif:(NSString*)targetUserID withMessage:(NSString*)msg withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    dict[@"message"] = msg;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"sendSysNotif"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"err");
        callback(NO);
    }];

}

-(void) balancePayment:(NSString*)targetOpenID amount:(NSString*)amount toBankName:(NSString*)bankName account:(NSString*)account inYear:(NSString*)y month:(NSString*)m day:(NSString*)d withMessage:(NSString*)msg withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetOpenID"] = targetOpenID;
    dict[@"year"] = y;
    dict[@"month"] = m;
    dict[@"day"] = d;
    dict[@"amount"] = amount;
    dict[@"bankName"] = bankName;
    dict[@"accountNumber"] = account;
    dict[@"message"] = msg;

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"balancePayment"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"err");
        callback(NO);
    }];
}
-(void) getPayments:(int)offset count:(int)count withCompletion:(void(^)(BOOL success,NSArray* paymentArray))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"offset"] =INT_TO_STRING(offset);
    dict[@"count"] = INT_TO_STRING(count);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getPayments"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            NSMutableArray* payments = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict){
                [payments addObject:[PaymentObject getPaymentWithDict:dict]];
            }
            callback(YES,payments);
        }];
        

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];

}

-(void) getRevenueReport:(int)minTimestamp maxTimestamp:(int)maxTimestamp withCompletion:(void(^)(BOOL success,NSArray* adArray))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"minTimestamp"] =INT_TO_STRING(minTimestamp);
    dict[@"maxTimestamp"] = INT_TO_STRING(maxTimestamp);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getRevenueReport"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* payments = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict){
                [payments addObject:[RevenueObject getObjectWithDict:dict]];
            }
            
            callback(YES,payments);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void)changePassword:(NSString*)oldPassword newPassword:(NSString*)newPassword withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"oldPassword"] =oldPassword;
    dict[@"newPassword"] =newPassword;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"changePassword"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            if ([self resultSuccess:decodedDict]) {
                callback(YES,decodedDict[@"message"]);
            } else {
                callback(NO,decodedDict[@"message"]);
            }
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void) getPostRevenueReport:(int)offset count:(int)count withCompletion:(void(^)(BOOL success,NSArray* postRevenueArray))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"offset"] =INT_TO_STRING(offset);
    dict[@"count"] = INT_TO_STRING(count);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getPostRevenueReport"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {

        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* payments = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict){
                [payments addObject:[RevenueObject getObjectWithDict:dict]];
            }
            callback(YES,payments);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

#pragma mark - Find Friend Related


-(void)uploadPhoneNumbers:(NSString*)myPhoneNumber phoneNumbers:(NSString*)phoneNumbersJsonString withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"myPhoneNumber"] =myPhoneNumber;
    dict[@"phoneNumberData"] =phoneNumbersJsonString;
    dict[@"countryCode"] =GET_DEFAULT(COUNTRY_CODE);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"uploadPhoneNumbers"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            if ([self resultSuccess:decodedDict]) {
                callback(YES,decodedDict[@"message"]);
            } else {
                callback(NO,decodedDict[@"message"]);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}


-(void)uploadFacebookFriends:(NSString*)myFacebookName facebookIDs:(NSString*)facebookIDsJson withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"myFacebookName"] = myFacebookName;
    dict[@"facebookData"] = facebookIDsJson;
    
    DLog(@"%@",facebookIDsJson);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"uploadFacebookFriends"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            if ([self resultSuccess:decodedDict]) {
                callback(YES,decodedDict[@"message"]);
            } else {
                callback(NO,decodedDict[@"message"]);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
        NSLog(@"Upload FACEBOOK Friends 失敗 :%@", error);
    }];
}



-(void)uploadTwitterFriends:(NSString*)myTwitterName twitterIDs:(NSString*) twitterIDsJson withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"myTwitterName"] = myTwitterName;
    dict[@"twitterData"] = twitterIDsJson;
    
//    DLog(@"%@",facebookIDsJson);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"uploadTwitterFriends"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            if ([self resultSuccess:decodedDict]) {
                callback(YES,decodedDict[@"message"]);
            } else {
                callback(NO,decodedDict[@"message"]);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
        NSLog(@"Upload Twitter Friends 失敗 : %@" ,error);
        
    }];
}

-(void)uploadInstagramFriends:(NSString*)myInstagramName instagramIDs:(NSString*)instagramIDsJson withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"myInstagramName"] = myInstagramName;
    dict[@"instagramData"] = instagramIDsJson;
    
    DLog(@"%@",instagramIDsJson);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"uploadInstagramFriends"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            if ([self resultSuccess:decodedDict]) {
                callback(YES,decodedDict[@"message"]);
            } else {
                callback(NO,decodedDict[@"message"]);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
        NSLog(@"Upload Instagram Followers 失敗 : %@" ,error);
        
    }];
    
}

-(void) findFriends: (NSString*)jsonData method:(NSString*)method completion:(void (^)(BOOL success, NSArray* userObjects)) callback
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"method"] =method ;
    dict[@"jsonData"] =jsonData ;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"findFriends"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* userObjects = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                UserObject* user = [UserObject getUserWithDict:dict];
                if(user!=nil)
                    [userObjects addObject:user];
            }
            callback(YES, userObjects);

        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, @[]);
    }];
}


#pragma mark - Admin Only

-(void)adminPromotePost:(NSString*)postID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postID"] =postID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"adminPromotePost"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)hidePostAction:(NSString*)postID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postID"] =postID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"hidePostAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)hideHotLivestreamAction:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] =liveStreamID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"hideHotLivestreamAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)adminDeletePostAction:(NSString*)postID message:(NSString*)message targetUserID:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"postID"] =postID;
    dict[@"message"] =message;
    dict[@"targetUserID"] = targetUserID;

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"adminDeletePostAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)sendOpenidAlert:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"sendOpenidAlert"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}


-(void)adminDeleteProfilePictureAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"adminDeleteProfilePictureAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)processProfilePictureReview:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"processProfilePictureReview"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) freezeUserAction:(NSString*)freezedUserID inLivestreamID:(NSString*)inLivestreamID absTime:(int)absTime withComletion:(void(^)(BOOL success, NSString* message)) callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"freezedUserID"] =freezedUserID;
    dict[@"inLivestreamID"] =inLivestreamID;
    dict[@"absTimestamp"] =INT_TO_STRING(absTime);

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"freezeUserAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,@"");
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void)banUserAction:(NSString*)bannedUserID inLivestreamID:(NSString*)inLivestreamID  absTime:(int)absTime withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"bannedUserID"] =bannedUserID;
    dict[@"inLivestreamID"] =inLivestreamID;
    dict[@"absTimestamp"] =INT_TO_STRING(absTime);

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"banUserAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,@"");
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void)adminUserAction:(NSString*)openID withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"openID"] =openID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"adminUserAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            if ([self resultSuccess:decodedDict]) {
                callback(YES,decodedDict[@"message"]);
            } else {
                callback(NO,decodedDict[@"message"]);
            }
        }];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void)verifyUserAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"verifyUserAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,@"");
        }];
       
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}


-(void)addCrossGreatWallFlag:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"addCrossGreatWallFlag"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,@"");
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void)removeCrossGreatWallFlag:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"removeCrossGreatWallFlag"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,@"");
        }];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void)celebrityAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"celebrityAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,@"");
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void)removeCelebrityAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"removeCelebrityAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,@"");
        }];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}


-(void)internationalTagAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"internationalTagAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,@"");
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void)removeInternationalTagAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"removeInternationalTagAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,@"");
        }];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void)adOnAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"adOnAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,@"");
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void)adOffAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"adOffAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,@"");
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}


-(void)removeVerifiedUserAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"removeVerifiedUserAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,@"");
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void)choiceUserAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"choiceUserAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            if ([self resultSuccess:decodedDict]) {
                callback(YES,decodedDict[@"message"]);
            } else {
                callback(NO,decodedDict[@"message"]);
            }
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void)removeChoiceUserAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] =targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"removeChoiceUserAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            if ([self resultSuccess:decodedDict]) {
                callback(YES,decodedDict[@"message"]);
            } else {
                callback(NO,decodedDict[@"message"]);
            }
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void) clearPostLikeFromUser:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"clearPostLikeFromUser"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) clearPostLikeByUserReceivedLikeTable:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"clearPostLikeByUserReceivedLikeTable"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) recalculateUserReceivedLikeCount:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"recalculateUserReceivedLikeCount"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) writeUserReceivedLikeCount:(NSString*)targetUserID toLeaderBoard:(int)leaderBoardType withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    dict[@"leaderBoardType"] = [NSString stringWithFormat:@"%d", leaderBoardType];
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"writeUserReceivedLikeCountToLeaderBoard"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

#pragma mark - LiveStream Related
-(void)publishLiveStream:(NSDictionary*)publishDict withCompletion:(void(^)(BOOL success, NSString* liveStreamID, int timestamp, NSString* colorCode,NSString* publicIP))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    [dict addEntriesFromDictionary:publishDict];
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"publishLiveStream"];

    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {

        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,decodedDict[@"liveStreamID"], [decodedDict[@"timestamp"] intValue],decodedDict[@"colorCode"],decodedDict[@"publicIP"]);
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, 0, @"000000",@"");  // blackColor
    }];
}

-(void)keepLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success, NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"keepLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            if ([self resultSuccess:decodedDict]) {
                callback(YES,decodedDict[@"message"]);
            } else {
                callback(NO,decodedDict[@"message"]);
            }
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,@"");
    }];
}

-(void)updateLiveStreamInfo:(NSDictionary*)updateDict withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    [dict addEntriesFromDictionary:updateDict];
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"updateLiveStreamInfo"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)endLiveStream:(NSString*)liveStreamID duration:(int)duration withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"duration"] = INT_TO_STRING(duration);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"endLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)getLiveStreamComments:(NSString*)liveStreamID afterTime:(int)afterTime commentCount:(int)count withCompletion:(void(^)(BOOL success, NSArray* liveStreamCommentObjects))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"afterTime"] = INT_TO_STRING(afterTime);
    dict[@"count"] = INT_TO_STRING(count);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLiveStreamComments"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            NSMutableArray* comments = CREATE_MUTABLE_ARRAY;
            
            for(NSDictionary* dict in decodedDict) {
                [comments addObject:[LiveStreamCommentObject getLiveStreamCommentWithDict:dict]];
            }
            
            callback(YES, [comments copy]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)getLiveStreamViewers:(NSString*)liveStreamID afterTime:(int)afterTime viewerCount:(int)count withCompletion:(void(^)(BOOL success, NSArray* dict))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"afterTime"] = INT_TO_STRING(afterTime);
    dict[@"count"] = INT_TO_STRING(count);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLiveStreamViewers"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* users = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [users addObject:[UserObject getUserWithDict:dict]];
            }
            callback(YES, [users copy]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)getLiveStreamReplayViewers:(NSString*)liveStreamID beforeTime:(int)beforeTime viewerCount:(int)count withCompletion:(void(^)(BOOL success, NSArray* dict))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLiveStreamReplayViewers"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* users = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [users addObject:[UserObject getUserWithDict:dict]];
            }
            callback(YES, [users copy]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)viewLiveStreamReplay:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"viewLiveStreamReplay"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)getLiveStreamInfo:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success, LiveStreamObject* liveStreamInfo))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLiveStreamInfo"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES, [LiveStreamObject getLiveStreamWithDict:decodedDict]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)getUserLiveStreams:(NSString*)targetUserID beforeTime:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* livestreams))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getUserLiveStreams"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
                
                NSMutableArray* livestreams = CREATE_MUTABLE_ARRAY;
                for(NSDictionary* dict in decodedDict) {
                    [livestreams addObject:[LiveStreamObject getLiveStreamWithDict:dict]];
                }
                callback(YES,  [livestreams copy]);
            }];
            
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)getLiveStreamFeed:(void(^)(BOOL success, NSArray* liveSteamObjects))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLiveStreamFeed"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* liveStreams = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [liveStreams addObject:[LiveStreamObject getLiveStreamWithDict:dict]];
            }
            callback(YES, [liveStreams copy]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)getLocalLiveStreams:(NSString*)sortMode withCompletion:(void(^)(BOOL success, NSArray* liveSteamObjects))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"sortMode"] = sortMode;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLocalLiveStreams"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* liveStreams = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [liveStreams addObject:[LiveStreamObject getLiveStreamWithDict:dict]];
            }
            callback(YES, [liveStreams copy]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)getHotLiveStreams:(void(^)(BOOL success, NSArray* liveSteamObjects))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getHotLiveStreams"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* liveStreams = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [liveStreams addObject:[LiveStreamObject getLiveStreamWithDict:dict]];
            }
            callback(YES, [liveStreams copy]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)getHotLiveStreams2:(NSString*)region count:(int)count withCompletion:(void(^)(BOOL success, NSArray* liveSteamObjects))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"region"] = region;
    dict[@"count"] = INT_TO_STRING(count);

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getHotLiveStreams2"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* liveStreams = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [liveStreams addObject:[LiveStreamObject getLiveStreamWithDict:dict]];
            }
            callback(YES, [liveStreams copy]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)getSuggestedLiveStreams:(NSString*)region count:(int)count withCompletion:(void(^)(BOOL success, NSArray* liveSteamObjects))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"region"] = region;
    dict[@"count"] = INT_TO_STRING(count);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getSuggestedLiveStreams"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* liveStreams = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [liveStreams addObject:[LiveStreamObject getLiveStreamWithDict:dict]];
            }
            callback(YES, [liveStreams copy]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)getLatestLiveStreams:(NSString*)region count:(int)count withCompletion:(void(^)(BOOL success, NSArray* liveSteamObjects))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"region"] = region;
    dict[@"count"] = INT_TO_STRING(count);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLatestLiveStreams"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* liveStreams = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [liveStreams addObject:[LiveStreamObject getLiveStreamWithDict:dict]];
            }
            callback(YES, [liveStreams copy]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)getLiveStreamHotCountryList:(void(^)(BOOL success, NSArray* regionList))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLiveStreamHotCountryList"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {

            NSMutableArray* regionArray = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                NSString* region = dict[@"region"];
                NSString* liveCount = dict[@"liveCount"];
                [regionArray addObject:@[region,liveCount]];
            }
            callback(YES, [regionArray copy]);

        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)getLatestLiveStreamReplays:(int)beforeTime liveStreamCount:(int)count withCompletion:(void(^)(BOOL success, NSArray* liveSteamObjects))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLatestLiveStreamReplays"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* liveStreams = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [liveStreams addObject:[LiveStreamObject getLiveStreamWithDict:dict]];
            }
            callback(YES, [liveStreams copy]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)enterLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success, NSString* message, int timestamp, int numberOfChunks, NSString* colorCode,NSString* publicIP))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"enterLiveStream"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString* message = decodedDict[@"message"];
            if ([self resultSuccess:decodedDict]) {
                callback(YES, message, [decodedDict[@"timestamp"] intValue], [decodedDict[@"numberOfChunks"] intValue], decodedDict[@"colorCode"],decodedDict[@"publicIP"]);
            } else {
                callback(NO, message, 0, 0, nil,@"");
            }
        }];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR, 0, 0, nil,@"");
    }];
}

-(void)previewLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"previewLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)keepViewLiveStream:(NSString*)liveStreamID isPreview:(int)isPreview withCompletion:(void(^)(BOOL success,BOOL blocked,NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"isPreview"] = INT_TO_STRING(isPreview);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"keepViewLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            BOOL blocked = [decodedDict[@"blocked"] intValue];
            
            if ([self resultSuccess:decodedDict]) {
                callback(YES,blocked,decodedDict[@"message"]);
            } else {
                callback(NO,blocked,decodedDict[@"message"]);
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,0,@"");
    }];
}

-(void)quitViewLiveStream:(NSString*)liveStreamID duration:(int)duration withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"duration"] = INT_TO_STRING(duration);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"quitViewLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)likeLiveStream:(NSString*)liveStreamID absTimestamp:(int) absTimestamp withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"absTimestamp"] = INT_TO_STRING(absTimestamp);

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"likeLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}


-(void)likeLivestreamBatchUpdate:(NSString*)liveStreamID likeCount:(int)likeCount absTimestamp:(int) absTimestamp withCompletion:(void(^)(BOOL success))callback
{
    DLog(@"likeLivestreamBatchUpdate count:%d",likeCount);
    
    
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"likeCount"] = INT_TO_STRING(likeCount);
    dict[@"absTimestamp"] = INT_TO_STRING(absTimestamp);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"likeLivestreamBatchUpdate"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)commentLiveStream:(NSString*)liveStreamID comment:(NSString*)comment colorCode:(NSString*)hexColorCode absTimestamp:(int) absTimestamp withCompletion:(void(^)(BOOL success, NSString* message))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"comment"] = comment;
    dict[@"colorCode"] = hexColorCode;
    dict[@"absTimestamp"] = INT_TO_STRING(absTimestamp);

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"commentLiveStream"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
    
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString* message = decodedDict[@"message"];
            if ([self resultSuccess:decodedDict]) {
                callback(YES, message);
            } else {
                callback(NO, message);
            }
        }];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR);
    }];
}


-(void)restreamLiveStream:(NSString*)liveStreamID liveStreamerOpenID:(NSString*)liveStreamerOpenID myOpenID:(NSString*)myOpenID  withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];

    dict[@"liveStreamID"] = liveStreamID;
    dict[@"liveStreamerOpenID"] = liveStreamerOpenID;
    dict[@"myOpenID"] = myOpenID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"restreamLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)hasRestreamedLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success,int hasRestreamed))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"hasRestreamedLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            int hasRestreamed = [decodedDict[@"hasRestreamed"] intValue];
            callback(YES,hasRestreamed);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,0);
    }];
}

#pragma mark - GroupLiveStream Related
-(void)publishGroupLiveStream:(NSDictionary*)publishDict withCompletion:(void(^)(BOOL success, NSString* liveStreamID, int timestamp, NSString* colorCode))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    [dict addEntriesFromDictionary:publishDict];
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"publishGroupLiveStream"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,decodedDict[@"groupLiveStreamID"], [decodedDict[@"timestamp"] intValue],decodedDict[@"colorCode"]);
        }];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, 0, @"000000");  // blackColor
    }];
}

-(void)updateGroupLiveStreamInfo:(NSDictionary*)updateDict withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    [dict addEntriesFromDictionary:updateDict];
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"updateGroupLiveStreamInfo"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)getGroupLiveStreamInfo:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success, LiveStreamObject* liveSteamInfo))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getGroupLiveStreamInfo"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES, [LiveStreamObject getLiveStreamWithDict:decodedDict]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)getUserGroupLiveStreams:(NSString*)targetUserID beforeTime:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* livestreams))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getUserGroupLiveStreams"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
                
                NSMutableArray* livestreams = CREATE_MUTABLE_ARRAY;
                for(NSDictionary* dict in decodedDict) {
                    [livestreams addObject:[LiveStreamObject getLiveStreamWithDict:dict]];
                }
                callback(YES,  [livestreams copy]);
            }];
            
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)getHotGroupLiveStreams:(void(^)(BOOL success, NSArray* liveSteamObjects))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getHotGroupLiveStreams"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* liveStreams = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [liveStreams addObject:[LiveStreamObject getLiveStreamWithDict:dict]];
            }
            callback(YES, [liveStreams copy]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void)enterGroupLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success, NSString* message, int timestamp, int numberOfChunks, NSString* colorCode))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"enterGroupLiveStream"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSString* message = decodedDict[@"message"];
            if ([self resultSuccess:decodedDict]) {
                callback(YES, message, [decodedDict[@"timestamp"] intValue], [decodedDict[@"numberOfChunks"] intValue], decodedDict[@"colorCode"]);
            } else {
                callback(NO, message, 0, 0, nil);
            }
        }];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NETWORK_ERROR, 0, 0, nil);
    }];
}

-(void)likeGroupLiveStreamBatchUpdate:(NSString*)liveStreamID likeCount:(int)likeCount absTimestamp:(int) absTimestamp withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"likeCount"] = INT_TO_STRING(likeCount);
    dict[@"absTimestamp"] = INT_TO_STRING(absTimestamp);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"likeGroupLiveStreamBatchUpdate"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)restreamGroupLiveStream:(NSString*)liveStreamID liveStreamerOpenID:(NSString*)liveStreamerOpenID myOpenID:(NSString*)myOpenID  withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"liveStreamerOpenID"] = liveStreamerOpenID;
    dict[@"myOpenID"] = myOpenID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"restreamGroupLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)hasRestreamedGroupLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success,int hasRestreamed))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"hasRestreamedGroupLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            int hasRestreamed = [decodedDict[@"hasRestreamed"] intValue];
            callback(YES,hasRestreamed);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,0);
    }];
}

-(void) getLikeLeaderboard:(NSString*)mode count:(int)count offset:(int)offset withCompletion:(void (^)(BOOL success,NSArray* userObjects)) callback
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"mode"] = mode;
    dict[@"count"] = INT_TO_STRING(count);
    dict[@"offset"] = INT_TO_STRING(offset);

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLikeLeaderboard"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* userObjects = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [userObjects addObject:[UserObject getUserWithDict:dict]];
            }
            callback(YES, [userObjects copy]);
        }];
    }
   failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       callback(NO,nil);
   }];
}

-(void)promoteLiveStream:(NSString*)liveStreamID countryName:(NSString*)countryName withCompletion:(void(^)(BOOL success,int hasRestreamed))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"countryName"] = countryName;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"promoteLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,0);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,0);
    }];
}

-(void)promoteTopLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"promoteTopLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}


-(void)cancelPromoteTopLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"cancelPromoteTopLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}


-(void)removePromotedLiveStream:(NSString*)liveStreamID countryName:(NSString*)countryName withCompletion:(void(^)(BOOL success,int hasRestreamed))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"countryName"] = countryName;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"removePromotedLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES,0);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,0);
    }];
}


/* For Review */
-(void) getLiveStreamsForReview:(NSString*)region beforeTime:(int)beforeTime count:(int)count fetchMode:(NSString*)fetchMode modulo:(int)modulo devisor:(int)devisor withCompletion:(void (^)(BOOL success,NSArray* liveStreams)) callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"region"] = region;
    dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    dict[@"modulo"] = INT_TO_STRING(modulo);
    dict[@"devisor"] = INT_TO_STRING(devisor);
    dict[@"fetchMode"] = fetchMode;

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLiveStreamsForReview"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* liveStreams = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [liveStreams addObject:[LiveStreamObject getLiveStreamWithDict:dict]];
            }
            callback(YES, [liveStreams copy]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void) passLiveStream:(NSString*)liveStreamID withCompletion:(void (^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"passLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) hideLiveStream:(NSString*)liveStreamID withCompletion:(void (^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"hideLiveStream"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) getExplorePicturesForReview:(NSString*)region beforeTime:(int)beforeTime count:(int)count fetchMode:(NSString*)fetchMode modulo:(int)modulo devisor:(int)devisor withCompletion:(void (^)(BOOL success,NSArray* liveStreams)) callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"region"] = region;
    dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    dict[@"modulo"] = INT_TO_STRING(modulo);
    dict[@"devisor"] = INT_TO_STRING(devisor);
    dict[@"fetchMode"] = fetchMode;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getExplorePicturesForReview"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* liveStreams = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [liveStreams addObject:[PostObject getPostWithDict:dict]];
            }
            callback(YES, [liveStreams copy]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

-(void) getReportPost:(int)count offset:(int)offset withCompletion:(void (^)(BOOL success , NSArray* posts))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"count"] = INT_TO_STRING(count);
    dict[@"offset"] = INT_TO_STRING(offset);

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getReportPost"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* posts = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [posts addObject:[PostObject getPostWithDict:dict]];
            }
            callback(YES,posts);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void) getReviewProfile:(int)count offset:(int)offset withCompletion:(void (^)(BOOL success , NSArray* users))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"count"] = INT_TO_STRING(count);
    dict[@"offset"] = INT_TO_STRING(offset);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getReviewProfile"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* users = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [users addObject:[ UserObject getUserWithDict:dict] ];
            }
            callback(YES,users);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,nil);
    }];
}

-(void)processReportedPostAction:(NSString*)postID withCompletion:(void (^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"postID"] = postID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"processReportedPostAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}



-(void) passExplorePicture:(NSString*)postID withCompletion:(void (^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"postID"] = postID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"passExplorePicture"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) hideExplorePicture:(NSString*)postID withCompletion:(void (^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"postID"] = postID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"hideExplorePicture"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void) checkingSandBox:(void (^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"checkingSandBox"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            if([decodedDict[@"isSandBox"] isEqual:@1]){
                [DEFAULTS setObject:@"1" forKey:IN_SANDBOX];
            }else{
                [DEFAULTS setObject:@"0" forKey:IN_SANDBOX];
            }
            if([decodedDict[@"isFestival"] isEqual:@1]){
                [DEFAULTS setObject:@"1" forKey:IS_FESTIVAL];
            }else{
                [DEFAULTS setObject:@"0" forKey:IS_FESTIVAL];
            }
            
            [[SINGLETON mainTabBarViewController] reloadCameraButton];

            [DEFAULTS synchronize];
            callback(YES);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}


-(void) hideUserFromLiveAction:(NSString*)targetUserID hideUserValue:(int)hideUserValue withCompletion:(void (^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    dict[@"hideUserValue"] = INT_TO_STRING(hideUserValue);
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"hideUserAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

-(void)recoverPostAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL sucess))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"targetUserID"] = targetUserID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"recoverPostAction"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];

}

-(void)getBlockUserList:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* userObjects))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"beforeTime"] = INT_TO_STRING(beforeTime);
    dict[@"count"] = INT_TO_STRING(count);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getBlockUserList"];
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {

//            callback(YES, nil);

            NSMutableArray* users = CREATE_MUTABLE_ARRAY;
            for(NSDictionary* dict in decodedDict) {
                [users addObject:[UserObject getUserWithDict:dict]];
            }
            callback(YES, [users copy]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

#pragma mark - Gift Related
- (void)getPurchaseCountWithCompletion:(void(^)(BOOL success, int32_t count))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getPurchaseCount"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            if ([decodedDict[@"result"] isEqual:@"success"]) {
                callback(YES, [decodedDict[@"count"] intValue]);
            } else {
                DLog(@"getPurchaseCount fail: err %@", decodedDict[@"message"]);
                callback(NO, 0);
            }
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, 0);
    }];
}

- (void)getPointProductListWithCompletion:(void(^)(BOOL success, NSArray *productObjs))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"platform"] = @"IOS";
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getPointProductList"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray *products = CREATE_MUTABLE_ARRAY;
            for (NSDictionary *productDict in decodedDict) {
                [products addObject:[ProductObject getProductWithDict:productDict]];
            }
            callback(YES, products);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

- (void)purchaseProductWithReceipt:(NSString*)receipt andPriceCountryCode:(NSString*)priceCountryCode withCompletion:(void(^)(BOOL success, int32_t purchasedPoint, int32_t errorCode))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"receipt"] = receipt;
    dict[@"platform"] = @"IOS";
    dict[@"priceCountryCode"] = priceCountryCode;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"purchaseProduct"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            if ([decodedDict[@"result"] isEqual:@"success"]) {
                callback(YES, [decodedDict[@"point"] intValue], [decodedDict[@"errorCode"] intValue]);
            } else {
                DLog(@"purchaseProduct fail: err %@", decodedDict[@"message"]);
                callback(NO, 0, [decodedDict[@"errorCode"] intValue]);
            }
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, 0, 2);
    }];
}

- (void)getPointUsageLogFromUser:(NSString*)userID beforeTime:(int32_t)timestamp withCount:(int32_t)count withCompletion:(void(^)(BOOL success, NSArray* pointUsageLogs))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"userID"] = userID;
    dict[@"beforeTime"] = INT_TO_STRING(timestamp);
    dict[@"count"] = INT_TO_STRING(count);
    dict[@"platform"] = @"IOS";
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getPointUsageLog"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray *pointUsageLogs = CREATE_MUTABLE_ARRAY;
            for (NSDictionary *pointUsageLogDict in decodedDict) {
                [pointUsageLogs addObject:[PointUsageLogObject getPointUsageLogWithDict:pointUsageLogDict]];
            }
            
            callback(YES, pointUsageLogs);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NULL);
    }];
}

- (void)getPointGainLogFromUser:(NSString*)userID beforeTime:(int32_t)timestamp withCount:(int32_t)count withCompletion:(void(^)(BOOL success, NSArray* pointGainLogs))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"userID"] = userID;
    dict[@"beforeTime"] = INT_TO_STRING(timestamp);
    dict[@"count"] = INT_TO_STRING(count);
    dict[@"platform"] = @"IOS";
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getPointGainLog"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray *pointGainLogs = CREATE_MUTABLE_ARRAY;
            for (NSDictionary *pointGainLogDict in decodedDict) {
                [pointGainLogs addObject:[PointGainObject getPointGainWithDict:pointGainLogDict]];
            }
            
            callback(YES, pointGainLogs);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NULL);
    }];
}

- (void)getReceivedGiftLog:(NSString*)userID beforeTime:(int32_t)timestamp withCount:(int32_t)count withCompletion:(void(^)(BOOL success, NSArray* receivedGiftLogs))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"userID"] = userID;
    dict[@"beforeTime"] = INT_TO_STRING(timestamp);
    dict[@"count"] = INT_TO_STRING(count);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getReceivedGiftLog"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray *receiveGiftLogs = CREATE_MUTABLE_ARRAY;
            for (NSDictionary *receivedGiftLogDict in decodedDict) {
                [receiveGiftLogs addObject:[UserGiftObject getUserGiftWitDict:receivedGiftLogDict]];
            }
            
            callback(YES, receiveGiftLogs);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NULL);
    }];
}

- (void)getSentGiftLog:(NSString*)userID beforeTime:(int32_t)timestamp withCount:(int32_t)count withCompletion:(void(^)(BOOL success, NSArray* sentGiftLogs))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"userID"] = userID;
    dict[@"beforeTime"] = INT_TO_STRING(timestamp);
    dict[@"count"] = INT_TO_STRING(count);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getSentGiftLog"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray *sentGiftLogs = CREATE_MUTABLE_ARRAY;
            for (NSDictionary *sentGiftLogDict in decodedDict) {
                [sentGiftLogs addObject:[UserGiftObject getUserGiftWitDict:sentGiftLogDict]];
            }
            
            callback(YES, sentGiftLogs);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NULL);
    }];
}

- (void)getGiftListWithCompletion:(void(^)(BOOL success, NSArray* gifts))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    if ([GET_DEFAULT(IS_ADMIN) intValue]>0) {
        dict[@"isGiftSandBox"] = @"1";
    }
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getGiftList"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* giftsarr = CREATE_MUTABLE_ARRAY;
            
            for (NSDictionary* giftDict in decodedDict) {
                [giftsarr addObject:[GiftObject getGiftWitDict:giftDict]];
            }
            NSArray *giftsback=[giftsarr copy];
            callback(YES, giftsback);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NULL);
    }];

}

- (void)getGiftInfoByGiftID:(NSString*)giftID withCompletion:(void(^)(BOOL success, GiftObject* gift))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"giftID"] = giftID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getGiftInfo"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            callback(YES, [GiftObject getGiftWitDict:decodedDict]);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NULL);
    }];
}

- (void)sendGift:(NSString*)giftID toLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"giftID"] = giftID;
    dict[@"liveStreamID"] = liveStreamID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"sendGift"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            if ([decodedDict[@"result"] isEqual:@"success"]) {
                callback(YES);
            } else {
                DLog(@"sendGift: err %@", decodedDict[@"message"]);
                callback(NO);
            }
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

- (void)readGift:(int32_t )giftToken toLiveStream:(NSString *)liveStreamID withCompletion:(void (^)(BOOL))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"giftToken"] = INT_TO_STRING(giftToken);
    dict[@"liveStreamID"] = liveStreamID;
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"readGift"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            if ([decodedDict[@"result"] isEqual:@"success"]) {
                callback(YES);
            } else {
                DLog(@"sendGift: err %@", decodedDict[@"message"]);
                callback(NO);
            }
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO);
    }];
}

- (void)getUnreadReceivedGift:(NSString *)liveStreamID beforeTime:(int32_t)time withCount:(int32_t)count withCompletion:(void (^)(BOOL, NSArray *))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"beforeTime"] = INT_TO_STRING(time);
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"count"] = INT_TO_STRING(count);

    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getUnreadReceivedGift"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
//                UserGiftObject* receivedUserGift = [UserGiftObject getUserGiftWitDict:decodedDict];
                NSMutableArray* unReadGifts = CREATE_MUTABLE_ARRAY;
            if ((unsigned long)[decodedDict count]==0) {
                callback(YES,NULL);
            }else{
          
                    for (NSDictionary* receivedGiftDict in decodedDict) {
                        
                        [unReadGifts addObject:[UserGiftObject getUserGiftWitDict:receivedGiftDict]];
                    }
                    callback(YES,unReadGifts);
                }
            
            DLog(@"%lu",(unsigned long)[decodedDict count]);

 
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO,NULL);
    }];
}



- (void)getLiveStream:(NSString*)liveStreamID giftToken:(int32_t)giftToken withCompletion:(void(^)(BOOL success, UserGiftObject* userGift))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"giftToken"] = INT_TO_STRING(giftToken);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLiveStreamGiftInfo"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            UserGiftObject* receivedUserGift = [UserGiftObject getUserGiftWitDict:decodedDict];
            
            callback(YES, receivedUserGift);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NULL);
    }];
}

- (void)getReceivedGiftsInLiveStream:(NSString*)liveStreamID afterTime:(int32_t)time withCount:(int32_t)count withCompletion:(void(^)(BOOL success, NSArray* gifts))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"afterTime_ms"] = INT_TO_STRING(time);
    dict[@"count"] = INT_TO_STRING(count);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLiveStreamReceivedGifts"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* receivedGifts = CREATE_MUTABLE_ARRAY;
            
            for (NSDictionary* receivedGiftDict in decodedDict) {
                [receivedGifts addObject:[UserGiftObject getUserGiftWitDict:receivedGiftDict]];
            }
            
            callback(YES, receivedGifts);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NULL);
    }];
}

- (void)getGiftLeaderboardType:(GiftLeaderboardRecordType)type fromUser:(NSString*)userID withCount:(int32_t)count andOffset:(int32_t)offset withCompletion:(void(^)(BOOL success, NSArray* giftLeaderboardRecords))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    switch (type) {
        case GiftLeaderboardRecordTypeTotal:
            dict[@"type"] = @"total";
            break;
        case GiftLeaderboardRecordTypeYear:
            dict[@"type"] = @"yyyy";
            break;
        case GiftLeaderboardRecordTypeMonth:
            dict[@"type"] = @"yyyymm";
            break;
        case GiftLeaderboardRecordTypeWeek:
            dict[@"type"] = @"yyyyww";
            break;
        case GiftLeaderboardRecordTypeDay:
            dict[@"type"] = @"yyyymmdd";
            break;
        
        default:
            dict[@"type"] = @"total";
            break;
    }
    dict[@"targetUserID"] = userID;
    dict[@"count"] = INT_TO_STRING(count);
    dict[@"offset"] = INT_TO_STRING(offset);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getGiftLeaderboard"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* giftLeaderboardRecords = CREATE_MUTABLE_ARRAY;
            
            for (NSDictionary* giftLeaderboardRecordDict in decodedDict) {
                [giftLeaderboardRecords addObject:[GiftLeaderboardRecordObject getGiftLeaderboardRecordWitDict:giftLeaderboardRecordDict]];
            }
            
            callback(YES, giftLeaderboardRecords);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NULL);
    }];
}

- (void)getLiveStreamGiftLeaderboardByUserId:(NSString *)userId liveStreamId:(NSString *)liveStreamId withCount:(int32_t)count andOffset:(int32_t)offset withCompletion:(void(^)(BOOL success, LiveStreamGiftLeaderboardObject* liveStreamGiftLeaderboard))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"userID"] = userId;
    dict[@"liveStreamID"] = liveStreamId;
    dict[@"count"] = INT_TO_STRING(count);
    dict[@"offset"] = INT_TO_STRING(offset);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLiveStreamGiftLeaderboard"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            
            LiveStreamGiftLeaderboardObject *liveStreamGiftLeaderboard = [LiveStreamGiftLeaderboardObject getLiveStreamLeaderboardWithResult:decodedDict];
            
            callback(YES, liveStreamGiftLeaderboard);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil);
    }];
}

- (void)getGiftHistoryInLiveStream:(NSString*)liveStreamID withCount:(int32_t)count andOffset:(int32_t)offset withCompletion:(void(^)(BOOL success, NSArray* gifts))callback
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
    dict[@"liveStreamID"] = liveStreamID;
    dict[@"count"] = INT_TO_STRING(count);
    dict[@"offset"] = INT_TO_STRING(offset);
    
    NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"getLiveStreamAllGifts"];
    
    [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
            NSMutableArray* receivedGifts = CREATE_MUTABLE_ARRAY;
            
            for (NSDictionary* receivedGiftDict in decodedDict) {
                [receivedGifts addObject:[UserGiftObject getUserGiftWitDict:receivedGiftDict]];
            }
            
            callback(YES, receivedGifts);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, NULL);
    }];
}




#pragma mark - Even tracking

-(void)eventTracking:eventArray withCompletion:(void (^)(BOOL))callback
{
//    NSMutableDictionary* a=[eventArray objectAtIndex:[eventArray count]-1];
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.baseDict];
        dict[@"eventArray"] = TO_JSON(eventArray);
        NSDictionary* packDict = [self dictPackageEncoding:dict withAction:@"eventTracking"];
        
        [self.manager POST:@"apiGateWay" parameters:packDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self decodingMiddleware:responseObject withCompletion:^(NSDictionary *decodedDict) {
//                NSString *message = decodedDict[@"message"];
                callback(YES);

            }];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            callback(NO);
        }];
    
    
    
}

# pragma mark - Helper Methods

-(BOOL)resultSuccess: (NSDictionary *)decodedDict
{
    return [decodedDict[@"result"] isEqualToString:@"success"];
}

-(void)saveUserDefaultsWithDict: (NSDictionary *)dict
{
    // invalid user
    if ([dict[@"userID"] isEqualToString:@""]) {
        return;
    }
    
    // Self info related
    if ([GET_DEFAULT(USER_ID) isEqualToString:@""] || [GET_DEFAULT(USER_ID) isEqualToString:dict[@"userID"]]) {
        [DEFAULTS setObject:dict[@"userID"] forKey:USER_ID];
    }
    
    [DEFAULTS setObject:dict[@"openID"] forKey:USER_OPEN_ID];
    [DEFAULTS setObject:dict[@"name"] forKey:USER_NAME];
    [DEFAULTS setObject:dict[@"privacyMode"] forKey:PRIVACY_MODE];
    [DEFAULTS setObject:dict[@"bio"] forKey:BIO];
    [DEFAULTS setObject:dict[@"picture"] forKey:PICTURE];
    [DEFAULTS setObject:dict[@"website"] forKey:WEBSITE];
    [DEFAULTS setObject:dict[@"age"] forKey:AGE];
    [DEFAULTS setObject:dict[@"gender"] forKey:GENDER];
    [DEFAULTS setObject:dict[@"isVerified"] forKey:IS_VERIFIED];
    [DEFAULTS setObject:dict[@"followerCount"] forKey:FOLLOWER_COUNT];
    [DEFAULTS setObject:dict[@"followingCount"] forKey:FOLLOWING_COUNT];
    [DEFAULTS setObject:dict[@"postCount"] forKey:POST_COUNT];
    [DEFAULTS setObject:dict[@"repostCount"] forKey:REPOST_COUNT];
    [DEFAULTS setObject:dict[@"likePostCount"] forKey:LIKED_POST_COUNT];
    [DEFAULTS setObject:dict[@"email"] forKey:EMAIL];
    
    [DEFAULTS setObject:dict[@"countryCode"] forKey:COUNTRY_CODE];
    [DEFAULTS setObject:dict[@"phoneNumber"] forKey:PHONE_NUM];
    
    if([dict objectForKey:@"countryCallingCode"]){
        if([dict[@"countryCallingCode"] length]!=0){
            [DEFAULTS setObject:dict[@"countryCallingCode"] forKey:COUNTRY_CODE];
        }
    }
    
    if([dict objectForKey:@"phoneTwoDigitISO"]){
        if([dict[@"phoneTwoDigitISO"] length]!=0){
            [DEFAULTS setObject:dict[@"phoneTwoDigitISO"] forKey:PHONE_TWO_DIGIT];
        }
    }
    
    if([dict objectForKey:@"localPhoneNumber"]){
        if([dict[@"localPhoneNumber"] length]!=0){
            [DEFAULTS setObject:dict[@"localPhoneNumber"] forKey:PHONE_NUM];
    
        }
    }
    
    [DEFAULTS setObject:dict[@"isAdmin"] forKey:IS_ADMIN];
    [DEFAULTS setObject:dict[@"isFreezed"] forKey:IS_FREEZED];
    
    // Notification related
    [DEFAULTS setObject:dict[@"pushLike"] forKey:PUSH_LIKE];
    [DEFAULTS setObject:dict[@"pushComment"] forKey:PUSH_COMMENT];
    [DEFAULTS setObject:dict[@"pushTag"] forKey:PUSH_TAG];
    [DEFAULTS setObject:dict[@"pushFollow"] forKey:PUSH_FOLLOW];
    [DEFAULTS setObject:dict[@"pushFriendJoin"] forKey:PUSH_FRIEND_JOIN];
    [DEFAULTS setObject:dict[@"pushFriendFirstPost"] forKey:PUSH_FRIEND_FIRST_POST];
    [DEFAULTS setObject:dict[@"pushLiveStream"] forKey:PUSH_LIVESTREAM_NOTIF];
    [DEFAULTS setObject:dict[@"pushLiveRestream"] forKey:PUSH_RESTREAM_NOTIF];
    [DEFAULTS setObject:dict[@"pushScheduleLiveStream"] forKey:PUSH_SCHEDULE_NOTIF];
    [DEFAULTS setObject:dict[@"pushFriendRequest"] forKey:PUSH_FRIEND_REQUEST];
    [DEFAULTS setObject:dict[@"pushSystemNotif"] forKey:PUSH_SYSTEM_NOTIF];
    [DEFAULTS setObject:dict[@"pushFollowRequest"] forKey:PUSH_FOLLOW_REQUEST];
    
    // Badge related
    [DEFAULTS setObject:dict[@"notifBadge"] forKey:NOTIF_BADGE];
    [DEFAULTS setObject:dict[@"systemNotifBadge"] forKey:SYSTEM_NOTIF_BADGE];
    [DEFAULTS setObject:dict[@"likeBadge"] forKey:LIKE_BADGE];
    [DEFAULTS setObject:dict[@"commentBadge"] forKey:COMMENT_BADGE];
    [DEFAULTS setObject:dict[@"messageBadge"] forKey:MESSGAE_BADGE];
    [DEFAULTS setObject:dict[@"requestBadge"] forKey:REQUEST_BADGE];
    
    // Bank & Payment related
    [DEFAULTS setObject:dict[@"bankName"] forKey:BANK_NAME];
    [DEFAULTS setObject:dict[@"bankAddress"] forKey:BANK_ADDRESS];
    [DEFAULTS setObject:dict[@"accountNumber"] forKey:ACCOUNT_NUMBER];
    [DEFAULTS setObject:dict[@"beneficiaryName"] forKey:BENEFICIARY_NAME];
    [DEFAULTS setObject:dict[@"swiftCode"] forKey:SWIFT_CODE];
    [DEFAULTS setObject:dict[@"rountingNumber"] forKey:ROUNTING_NUMBER];
    [DEFAULTS setObject:dict[@"bankCountry"] forKey:BANK_COUNTRY];
    [DEFAULTS setObject:dict[@"branchName"] forKey:BRANCH_NAME];
    [DEFAULTS setObject:dict[@"idCardNumber"] forKey:ID_CARD_NUMBER];
    [DEFAULTS setObject:dict[@"idCardName"] forKey:ID_CARD_NAME];
    [DEFAULTS setObject:dict[@"bankCode"] forKey:BANK_CODE];
    [DEFAULTS setObject:dict[@"bankBranchCode"] forKey:BANK_BRANCHCODE];
    [DEFAULTS setObject:dict[@"idCardFrontPicture"] forKey:ID_CARD_FRONT_PICTURE];
    [DEFAULTS setObject:dict[@"idCardBackPicture"] forKey:ID_CAR_BACK_PICTURE];
    [DEFAULTS setObject:dict[@"passbookPicture"] forKey:PASSBOOK_PICTURE];
    [DEFAULTS setObject:dict[@"bankAccountVerified"] forKey:BANK_ACCOUNT_VERIFIED];
    [DEFAULTS setObject:dict[@"totalRevenueEarned"] forKey:TOTAL_REVENUE_EARNED];
    [DEFAULTS setObject:dict[@"totalRevenuePayed"] forKey:TOTAL_REVENUE_PAYED];
    
    [DEFAULTS setObject:dict[@"receivedLikeCount"] forKey:RECEIVED_LIKE_COUNT];
    [DEFAULTS setObject:dict[@"openIDModifyTime"] forKey:OPENID_MODIFYTIME];
    [DEFAULTS setObject:dict[@"userIPs"] forKey:USER_IP];

    [DEFAULTS setObject:dict[@"alipayPhone"] forKey:ALIPAYPHONE];
    [DEFAULTS setObject:dict[@"paypalEmail"] forKey:PAYPALEMAIL];
    [DEFAULTS setObject:dict[@"point"] forKey:MY_POINT];

//    [DEFAULTS setObject:dict[@"verifiedPhoneNumber"] forKey:VERIFIEDPHONE];
    [DEFAULTS setObject:dict[@"openidRejected"] forKey:OPENID_REJECTED];
    [DEFAULTS setObject:dict[@"giftModuleState"] forKey:GIFTMODULESTATE];
    [DEFAULTS setObject:dict[@"appUpdateLink"] forKey:UPDATE_APPLINK];
    [SINGLETON reloadAllBadge];
    [DEFAULTS synchronize];

    DLog(@"openidRejected:%d",[GET_DEFAULT(OPENID_REJECTED) intValue]);
    
    
    if ([dict[@"isFreezed"] intValue] == 1) {
        [SINGLETON saveFreezeFile];
    }
    
    if ([dict[@"unLockUser"] intValue] == 1) {
        [SSKeychain deletePasswordForService:BUNDLE_ID account:@"isFreeze"];
    }
}

-(NSDictionary *)baseDict
{
    return @{@"userID":MY_USER_ID,
             @"deviceID":GET_DEFAULT(DEVICE_ID),
             @"deviceType": @"IOS",
             @"version":CURRENT_VERSION,
             @"accessToken":GET_DEFAULT(ACCESS_TOKEN),
             @"language": LOCALIZE(@"CURRENT_LANGUAGE"),
             @"packageName": BUNDLE_ID,
             @"nonce":[SINGLETON getUUID],
             @"ipCountry":GET_DEFAULT(IP_COUNTRY)
//             @"ipCountry":@"CN"
             };
}

-(NSDictionary*)dictPackageEncoding:(NSMutableDictionary*)dict withAction:(NSString*)action
{
    NSMutableDictionary* packageDict = CREATE_MUTABLE_DICTIONARY;
    dict[@"action"] = action;
    packageDict[@"data"] = TO_JSON(dict);
    packageDict[@"key"] = @"";
    packageDict[@"cypher"] = @"0_v2";
    return packageDict;
}



-(void) decodingMiddleware:(NSDictionary*)dict withCompletion:(void (^)(NSDictionary *))callback
{
    NSString* encodedString = dict[@"data"];
    
    /* To Handle Multi-layer json parser error */
    NSString* trimString = [encodedString stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
    NSDictionary *deserializedData = [trimString mj_JSONObject];
    callback(deserializedData);
}


@end
