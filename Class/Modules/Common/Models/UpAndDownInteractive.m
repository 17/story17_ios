//
//  UpAndDownInteractive.m
//  Story17
//
//  Created by Racing on 2015/11/30.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "UpAndDownInteractive.h"

@implementation UpAndDownInteractive

- (CGFloat)completionSpeed
{
    return 1.f - self.percentComplete;
}

@end
