//
//  LikerHandler.m
//  story17
//
//  Created by POPO Chen on 7/23/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "LikesHandler.h"
#import "Constant.h"

@interface LikesHandler()

/* Post Liker */
-(void)likeBatchUpload;

@property (nonatomic,strong) NSString* postID;
@property (nonatomic) int likeCount;
@property (nonatomic) int lastLikeTime;
@property (nonatomic) int likeCountHalfSecond;

@property (nonatomic,strong) NSTimer* likeTimer;
@property (nonatomic,strong) NSTimer* livelikeTimer;

@property (nonatomic,strong) NSString* livestreamID;
@property (nonatomic) int liveLikeCount;
@property (nonatomic) int absTimestamp;

@end

@implementation LikesHandler

#define StopBot YES
#define BotCountHalfSecond 9
#define MaxLikeHaldSecond 4
#define UPLOAD_INTERNAL 1

+(LikesHandler*) sharedInstance
{
    static dispatch_once_t once;
    
    static LikesHandler *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(id)init
{
    self = [super init];
    
    if (self) {
        [self resetPost];
        [self resetLive];

    }
    return self;
}

-(void)likePost:(NSString *)postID
{
    _lastLikeTime = CURRENT_TIMESTAMP;
    
    if([postID isEqualToString:_postID]){
        _likeCount += 1;
        _likeCountHalfSecond += 1;

//        if(_likeCount>=100){
//            [self likeBatchUpload];
//        }
        
    }else{
        
        /* Upload Like */
        [self likeBatchUpload];
        /* First Like or Change Post Like */
        _postID = postID;
        _likeCount = 1;
        _likeCountHalfSecond = 1;
        
        if(_likeTimer==nil){
            _likeTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(likeTimer:) userInfo:nil repeats:YES];
        }
    }
}

-(void)likeBatchUpload
{
    if([_postID isEqualToString:@""] || _likeCount==0 || _postID==nil){
        return;
    }
    
    [API_MANAGER likePostBatchUpdate:_postID likeCount:_likeCount withCompletion:^(BOOL success) {}];
    [self resetPost];
}

-(void)likeTimer:(id)sender
{

//    DLog(@"beforeLikeCount:%d",_likeCountHalfSecond);
    
    /* Avoid Bot Click */
    if(_likeCountHalfSecond>BotCountHalfSecond && StopBot){
        _likeCount = 0;
    }else if(_likeCountHalfSecond<BotCountHalfSecond && _likeCountHalfSecond>MaxLikeHaldSecond){
//        _likeCount -= _likeCountHalfSecond-BotCountHalfSecond;
    }
    
    if(_lastLikeTime<CURRENT_TIMESTAMP-UPLOAD_INTERNAL){
        [self likeBatchUpload];
    }

//    DLog(@"afterLikeCount:%d",_likeCountHalfSecond);
    
    _likeCountHalfSecond = 0.0;
}


-(void)resetPost
{
    _postID = @"";
    _likeCount = 0;
    _lastLikeTime = 0;
    _likeCountHalfSecond = 0;
    
    [_likeTimer invalidate];
    _likeTimer = nil;
    
    
}

/* Livestream Like  */
-(void)likeLivestreamBatchUpdate
{
    if(![self shouldUpdateLivestream])
        return;
    
   [API_MANAGER likeLivestreamBatchUpdate:_livestreamID likeCount:_liveLikeCount absTimestamp:_absTimestamp withCompletion:^(BOOL success) {}];
    [self resetLive];
}

-(void)likeLivestream:(NSString*)livestreamID
{
    if (!MY_USER_ID || [MY_USER_ID isEqualToString:@""]) {
        return;
    }
    
    _lastLikeTime = CURRENT_TIMESTAMP;
    
    if([[NSString stringWithFormat:@"%@",livestreamID] isEqualToString:[NSString stringWithFormat:@"%@",_livestreamID]]){
        
        _liveLikeCount += 1;
        _likeCountHalfSecond += 1;

//        if(_liveLikeCount>=100){
//            [self likeLivestreamBatchUpdate];
//        }
        
    }else{
        
        if([self shouldUpdateLivestream]){
            
            /* Upload Like */
            [self likeLivestreamBatchUpdate];
        }
        
        /* First Like or Change Post Like */
        _livestreamID = livestreamID;
        _liveLikeCount = 1;
        _likeCountHalfSecond += 1;

        if(_livelikeTimer==nil){
            _livelikeTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(livelikeTimer:) userInfo:nil repeats:YES];
        }
        
    }
}

-(void)livelikeTimer:(id)sender
{
    /* Avoid Bot Click */
    if(_likeCountHalfSecond>BotCountHalfSecond && StopBot){
        _liveLikeCount = 0;
    }else if(_likeCountHalfSecond<BotCountHalfSecond && _likeCountHalfSecond>MaxLikeHaldSecond){
        _liveLikeCount -= _likeCountHalfSecond-MaxLikeHaldSecond;
    }
    
    if(_lastLikeTime<CURRENT_TIMESTAMP-UPLOAD_INTERNAL){
        [self likeLivestreamBatchUpdate];
    }
    
    _likeCountHalfSecond = 0.0;
}

-(BOOL)shouldUpdateLivestream{
    
    if([[NSString stringWithFormat:@"%@",_livestreamID] isEqualToString:@""] || _liveLikeCount==0){
        return NO;
    }
    return YES;
}

-(void)resetLive
{
    _livestreamID = @"";
    _liveLikeCount = 0;
    _absTimestamp = 0;
    _likeCountHalfSecond = 0;

    [_livelikeTimer invalidate];
    _livelikeTimer = nil;
}




@end
