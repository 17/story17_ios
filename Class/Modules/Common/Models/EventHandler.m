//
//  EventHandler.m
//  story17
//
//  Created by POPO Chen on 7/23/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "EventHandler.h"
#import "Constant.h"
#import "LoginHandler.h"

@interface EventHandler()

/* Post Liker */

@property (nonatomic,strong) NSMutableArray* eventArray;
@property (nonatomic,strong) NSMutableArray* eventArrayForFlurry;

@property (nonatomic,strong) NSString* previousEventID;
@property (nonatomic,strong) NSString* previousEventName;
@property int audioOnlyCount;
@property int errorTimes;
@property int maxEvent;
@property (nonatomic,strong)  NSString* nowStatus;
@end

@implementation EventHandler



+(EventHandler*) sharedInstance
{
    static dispatch_once_t once;
    
    static EventHandler *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(id)init
{
    self = [super init];
    
    if (self) {
        _previousEventID=GET_DEFAULT(SESSION_ID);
        _previousEventName=@"LaunchAPP";
        _eventArray=CREATE_MUTABLE_ARRAY;
        _eventArrayForFlurry=CREATE_MUTABLE_ARRAY;
        _errorTimes=0;
        _maxEvent=30;
        _networkUnstableCount=0;
        _audioOnlyCount=0;
    }
    return self;
    
}

-(void)addEventTracking:(NSString *)eventName withDict:(NSDictionary *)params {
  
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:self.eventTrackingBaseDict];
    NSMutableDictionary* dictFlurry = [[NSMutableDictionary alloc] initWithDictionary:self.eventTrackingBaseDictForFlurryAndUmeng];
    
    if ([_eventArray count]>0&&[_eventArrayForFlurry count]>0) {//add info
        NSMutableDictionary* preDict=[_eventArray objectAtIndex:([_eventArray count]-1)];
        NSMutableDictionary* preDictForFlurry=[_eventArrayForFlurry objectAtIndex:([_eventArrayForFlurry count]-1)];
        
        preDict[@"to_event"]=eventName;
        preDict[@"to_event_id"]=dict[@"event_id"];
        [_eventArray replaceObjectAtIndex:[_eventArray count]-1 withObject:preDict];
        
        preDictForFlurry[@"to_event"]=eventName;
        
        [Flurry logEvent:eventName withParameters:preDictForFlurry];
        [MobClick event:eventName attributes:preDictForFlurry];
    }
    
    dict[@"from_event_id"] = _previousEventID;
    dict[@"from_event"] = _previousEventName;
    dict[@"event"]=eventName;
    
    
    dictFlurry[@"from_event"]=_previousEventName;
    dictFlurry[@"event"]=eventName;
    [dict addEntriesFromDictionary:params];

    if (params.count<5) {//flurry can't accept too many argument
        [dictFlurry addEntriesFromDictionary:params];

    }else{
        
    }
    [_eventArray addObject:dict];
    
    
    [_eventArrayForFlurry addObject:dictFlurry];
    _previousEventName=eventName;
    _previousEventID=dict[@"event_id"];
    
    
    if ([_eventArray count]>_maxEvent||[eventName isEqualToString:@"CloseApp"]) {//flush
        NSArray* eventArray=[_eventArray copy];
        NSArray* flurryarr=[_eventArrayForFlurry copy];
        [self eventBatchUpload:eventArray flurryarr:flurryarr];
        if ([eventName isEqualToString:@"CloseApp"]) {
            [Flurry logEvent:eventName withParameters:dictFlurry];
            [MobClick event:eventName attributes:dictFlurry];

        }
    }

}






-(void)eventBatchUpload:(NSArray *)eventArr flurryarr:(NSArray*)flurryarr
{

        [API_MANAGER eventTracking:eventArr withCompletion:^(BOOL success) {
            if (success) {
                [_eventArray removeObjectsInArray:eventArr];
                [_eventArrayForFlurry removeObjectsInArray:flurryarr];
                _maxEvent=30;
                _errorTimes=0;
            }else{
                _errorTimes++;
                if (_errorTimes>5) { //too many times fail
                    [_eventArray removeAllObjects];
                    [_eventArrayForFlurry removeAllObjects];
                }
                _maxEvent=_maxEvent+10;
            }
        }];
}

-(NSDictionary *)eventTrackingBaseDict
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict addEntriesFromDictionary:@{
                                     @"dn":[[UIDevice currentDevice] name],
                                     @"t":DOUBLE_TO_STRING([[NSDate date] timeIntervalSince1970]),
                                     @"ip": GET_DEFAULT(MY_USER_IP),
                                     @"longitude":@"0",
                                     @"latitude":@"0",
                                     @"sessionID": GET_DEFAULT(SESSION_ID),
                                     @"event_id":[SINGLETON getUUID],
                                     @"event":@"",
                                     @"from_event":@"",
                                     @"from_event_id":@"",
                                     @"to_event":@"",
                                     @"to_event_id":@"",
                                     @"app_open_times":GET_DEFAULT(APP_OPEN_TIMES),
                                     @"isGuest":[LoginHandler isGuest] ?@"1":@"0"
                                     //             @"ipCountry":@"CN"
                                     }];
    return dict;
}

-(NSDictionary *)eventTrackingBaseDictForFlurryAndUmeng
{
    return @{
             @"region":GET_DEFAULT(IP_COUNTRY),
             @"from_event":@"",
             @"to_event":@"",
             @"app_open_times":GET_DEFAULT(APP_OPEN_TIMES),
             @"isGuest":[LoginHandler isGuest] ?@"1":@"0"
             //             @"ipCountry":@"CN"
             };
}

-(NSString*)readNowStatus
{
    return _nowStatus;
}

-(void)setNowStatus:(NSString*)value
{
    _nowStatus=value;
}

-(void)addNetworkUnstableCount
{
    _networkUnstableCount++;
}

-(int)readNowNetworkUnstableCount
{
    return _networkUnstableCount;
}

-(void)setEnterPageTime
{
    _enterPageTimeStamp=CURRENT_TIMESTAMP;
}
-(int)readEnterPageTime
{
    return _enterPageTimeStamp;
}
-(void)addAudioOnlyCount
{
    _audioOnlyCount++;
}
-(int)readAudioOnlyCount
{
    return _audioOnlyCount;
}

@end
