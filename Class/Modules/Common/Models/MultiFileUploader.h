#import "Constant.h"

@interface MultiFileUploader : NSObject

@property (nonatomic, copy) void(^multiFileUploaderCallback)(NSArray* fileNameArray, int code ,float progress);
@property (nonatomic, retain) NSArray* fileNameArray;
@property (nonatomic, retain) NSMutableArray* uploadObjectArray;
@property int completeCount;

-(id) initWithFileNameArray:(NSArray *)fileArray;
-(void) startUploadWithCallback:(void (^)(NSArray* fileNameArray, int code,float progress)) callback;
-(void) stopUpload;

@end
