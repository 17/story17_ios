#//
//  MyScene.m
//  AnimatedBear
//
//  Created by Tony Dahbura on 7/16/13.
//  Copyright (c) 2013 Tony Dahbura. All rights reserved.
//


#import <AVFoundation/AVFoundation.h>
#import "MyScene.h"
#import "Constant.h"

@implementation MyScene {

    NSMutableArray* heartarray;
    NSMutableArray* beararray;
    NSMutableArray* rabbitarray;
    NSMutableArray* catarray;
    NSMutableArray* giftarray;
    NSMutableArray* sparray;
    NSMutableArray* sp2array;
}

-(id)initWithSize:(CGSize)size {
    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */

        self.backgroundColor = [SKColor clearColor];
        heartarray = CREATE_MUTABLE_ARRAY;
        beararray = CREATE_MUTABLE_ARRAY;
        rabbitarray = CREATE_MUTABLE_ARRAY;
        catarray = CREATE_MUTABLE_ARRAY;
        giftarray=CREATE_MUTABLE_ARRAY;
        sparray=CREATE_MUTABLE_ARRAY;
        sp2array = CREATE_MUTABLE_ARRAY;
        
        for(int i=1;i<=28;i++){
            SKTexture* textTure = [SKTexture textureWithImageNamed:[NSString stringWithFormat:@"colorheart_%i",i]];
            [heartarray addObject:textTure];
        }
        
        for(int i=1;i<=8;i++){
            SKTexture* textTure = [SKTexture textureWithImageNamed:[NSString stringWithFormat:@"bear_%i",i]];
            [beararray addObject:textTure];
        }
        
        for(int i=1;i<=8;i++){
            SKTexture* textTure = [SKTexture textureWithImageNamed:[NSString stringWithFormat:@"rabbit_%i",i]];
            [rabbitarray addObject:textTure];
        }
        
        for(int i=1;i<=8;i++){
            SKTexture* textTure = [SKTexture textureWithImageNamed:[NSString stringWithFormat:@"cat_%i",i]];
            [catarray addObject:textTure];
        }
        
        for(int i=1;i<=10;i++){
            SKTexture* textTure = [SKTexture textureWithImageNamed:[NSString stringWithFormat:@"money_%i",i]];
            [sparray addObject:textTure];
        }
        
        for(int i=1;i<=8;i++){
            SKTexture* textTure = [SKTexture textureWithImageNamed:[NSString stringWithFormat:@"new_year_%i",i]];
            [sp2array addObject:textTure];
        }


        
    }
    return self;
}


-(void)showAnimate:(CGPoint)point imgarray:(NSArray *)imgarray perFramDuraiton:(float)perFramDuration startPoint:(int)imageStart withCompletion:(void (^)(BOOL))callback
{
    if (_animating) {
        callback(NO);
        return;
    }


    giftarray=CREATE_MUTABLE_ARRAY;

    _animating=YES;
    
    
    int stopPoint=imageStart+10;
    int imageCount=0;
    for(int i=imageStart;i<[imgarray count]&&i<stopPoint;i++){
            SKTexture* textTure = [SKTexture textureWithImageNamed:imgarray[i]];
//        DLog(@"path==%@",imgarray[i]);
            [giftarray addObject:textTure];
        imageStart++;
        imageCount++;
    }
    if ([giftarray count]==0) {
        _animating=NO;
        callback(NO);
        return;
    }

   __block  SKTexture *temp = giftarray[0];
    
    __block  SKSpriteNode *gift;
    if (IS_IPHONE_4) {
        gift= [SKSpriteNode spriteNodeWithTexture:temp size:CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT*4/3)];
    }else{
        gift= [SKSpriteNode spriteNodeWithTexture:temp size:CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT)];
    }
    gift.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    [self addChild:gift];

    [gift runAction:[SKAction animateWithTextures:giftarray timePerFrame:perFramDuration resize:NO restore:NO] withKey:@"gift"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(perFramDuration*imageCount * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        DLog(@"REMOOOOOOOOOOOOOOOOOOVE");
        [gift removeFromParent];
        [gift removeActionForKey:@"gift"];
        [giftarray removeAllObjects];
        giftarray=nil;
        temp=nil;
        gift=nil;
        _animating=NO;
        [self showAnimate:CGPointMake(SCREEN_WIDTH, SCREEN_HEIGHT) imgarray:imgarray perFramDuraiton:perFramDuration startPoint:imageStart withCompletion:^(BOOL Done) {
            callback(YES);

        }];
      
    });
}



-(void)showheartFrompout:(CGPoint)point selectedColor:(int)color
{
    if(color<1)
        return;

    SKSpriteNode *heart;
    
    float totalAnimationTime = RANDOM_FLOAT(2.5, 3.5);
    float offsetX1 = RANDOM_FLOAT(-40.0, 40.0);
    float offsetX2 = RANDOM_FLOAT(-20.0, 20.0);
    float offsetY = RANDOM_FLOAT(-220.0, -180.0);

    SKAction* runAction = [SKAction moveByX:offsetX1 y:-offsetY duration:totalAnimationTime/2];
    SKAction* fadeInAction = [SKAction fadeAlphaTo:1.0 duration:totalAnimationTime/2];
    SKAction* scaleAnimation = [SKAction scaleBy:RANDOM_FLOAT(1.2, 1.5) duration:totalAnimationTime/2];
    SKAction* rotationAnimation = [SKAction rotateByAngle:RANDOM_FLOAT(-M_PI_4*0.75, M_PI_4*0.75) duration:totalAnimationTime/2];

    SKAction *inGroup = [SKAction group:@[runAction, fadeInAction,scaleAnimation,rotationAnimation]];

    SKAction* run2Action = [SKAction moveByX:offsetX2 y:-offsetY duration:totalAnimationTime/2];
    SKAction* fadeOutAction = [SKAction fadeAlphaTo:0.0 duration:totalAnimationTime/2];
    SKAction* scale2Animation = [SKAction scaleBy:RANDOM_FLOAT(0.9, 1.1) duration:totalAnimationTime/2];
    SKAction* rotation2Animation = [SKAction rotateByAngle:RANDOM_FLOAT(-M_PI_4*0.75, M_PI_4*0.75) duration:totalAnimationTime/2];
    
    SKAction *outGroup = [SKAction group:@[run2Action, fadeOutAction,scale2Animation,rotation2Animation]];

    SKAction* sequence = [SKAction sequence:@[inGroup,outGroup, [SKAction runBlock:^{
        [heart removeFromParent];
    }]]];
    
    if(color<0 || color > [heartarray count])
        return; 

    if([SINGLETON isSpecialMode]){
        if(RANDOM_FLOAT(0, 10)<7){
            heart = [SKSpriteNode spriteNodeWithTexture:sparray[RANDOM_INT(0,9)]];
        }else{
            heart = [SKSpriteNode spriteNodeWithTexture:sp2array[RANDOM_INT(0,7)]];
        }
    }else{
        if(RANDOM_FLOAT(0, 10)<9){
            
            heart = [SKSpriteNode spriteNodeWithTexture:heartarray[color-1]];
            
        }else{
            
            int random = (int)(arc4random_uniform(3));
            if(random==0){
                heart = [SKSpriteNode spriteNodeWithTexture:beararray[color%8]];
            }else if(random==1){
                heart = [SKSpriteNode spriteNodeWithTexture:rabbitarray[color%8]];
            }else{
                heart = [SKSpriteNode spriteNodeWithTexture:catarray[color%8]];
            }
        }
    }
    
    heart.alpha = 0.5f;
    heart.position = CGPointMake(point.x, SCREEN_HEIGHT-point.y);
    [heart runAction:sequence];
    [self addChild:heart];
}

-(void)showAnimation1FromPoint:(CGPoint)point
{
    int color = 5;
    
    if(color<1)
        return;
    SKEmitterNode *explosion = [SKEmitterNode new];
    
    [explosion setParticleTexture:heartarray[RANDOM_INT(0,11)]];
    [explosion setParticleSize:CGSizeMake(30,30)];
    [explosion setParticleColor:WHITE_COLOR];
    [explosion setNumParticlesToEmit:50];
    [explosion setParticleBirthRate:100];
    [explosion setParticleLifetime:4];
    [explosion setEmissionAngleRange:360];
    [explosion setParticleSpeed:250];
    [explosion setParticleSpeedRange:50];
    [explosion setXAcceleration:0];
    [explosion setYAcceleration:0];
    [explosion setParticleAlpha:0.8];
    [explosion setParticleAlphaRange:0.2];
    [explosion setParticleAlphaSpeed:-0.5];
    [explosion setParticleScale:1.5];
    [explosion setParticleScaleRange:0.4];
    [explosion setParticleScaleSpeed:-0.5];
    [explosion setParticleRotation:0];
    [explosion setParticleRotationRange:0];
    [explosion setParticleRotationSpeed:0];
    explosion.position = point;
    
    [explosion setParticleColorBlendFactor:1];
    [explosion setParticleColorBlendFactorRange:0];
    [explosion setParticleColorBlendFactorSpeed:0];
    [explosion setParticleBlendMode:SKBlendModeAdd];
    
    [self addChild:explosion];
}

-(void)showAnimation2FromPoint
{
    for(int j=0;j<7;j++){
        
        float totalAnimationTime = RANDOM_FLOAT(2.5, 3.5);

        for(int i=0;i<5;i++){
        
            int color = RANDOM_INT(1, 28);
            int textureSize = 45;
            
            SKSpriteNode *heart;
            
            SKAction* runAction = [SKAction moveByX:SCREEN_WIDTH*2.5 y:0 duration:totalAnimationTime];
            SKAction* fadeInAction = [SKAction fadeAlphaTo:1.0 duration:totalAnimationTime];
            
            SKAction *inGroup = [SKAction group:@[runAction, fadeInAction]];

            SKAction* sequence = [SKAction sequence:@[inGroup, [SKAction runBlock:^{
                [heart removeFromParent];
            }]]];
            
            if(RANDOM_FLOAT(0, 10)<9){
                
                heart = [SKSpriteNode spriteNodeWithTexture:heartarray[color-1] size:CGSizeMake(textureSize, textureSize)];
                
            }else{
                
                int random = (int)(arc4random_uniform(3));
                if(random==0){
                    heart = [SKSpriteNode spriteNodeWithTexture:beararray[color%8] size:CGSizeMake(textureSize, textureSize)];
                }else if(random==1){
                    heart = [SKSpriteNode spriteNodeWithTexture:rabbitarray[color%8] size:CGSizeMake(textureSize, textureSize)];
                }else{
                    heart = [SKSpriteNode spriteNodeWithTexture:catarray[color%8] size:CGSizeMake(textureSize, textureSize)];
                }
            }
            
            heart.alpha = 0.5f;
            heart.position = CGPointMake(-SCREEN_WIDTH+70*i, SCREEN_HEIGHT-180-50*j);
            [heart runAction:sequence];
            [self addChild:heart];
        }
    }
}

-(void)showAnimation3FromPoint
{
    for(int i=0;i<5;i++){
        
        float totalAnimationTime = RANDOM_FLOAT(2.5, 3.5);
        
        for(int j=0;j<7;j++){
            
            int color = RANDOM_INT(1, 28);
            int textureSize = 45;
            
            SKSpriteNode *heart;
            
            SKAction* runAction = [SKAction moveByX:0 y:SCREEN_HEIGHT*2 duration:totalAnimationTime];
            SKAction* fadeInAction = [SKAction fadeAlphaTo:1.0 duration:totalAnimationTime];
            
            SKAction *inGroup = [SKAction group:@[runAction, fadeInAction]];
            
            SKAction* sequence = [SKAction sequence:@[inGroup, [SKAction runBlock:^{
                [heart removeFromParent];
            }]]];
            
            if(RANDOM_FLOAT(0, 10)<9){
                
                heart = [SKSpriteNode spriteNodeWithTexture:heartarray[color-1] size:CGSizeMake(textureSize, textureSize)];
                
            }else{
                
                int random = (int)(arc4random_uniform(3));
                if(random==0){
                    heart = [SKSpriteNode spriteNodeWithTexture:beararray[color%8] size:CGSizeMake(textureSize, textureSize)];
                }else if(random==1){
                    heart = [SKSpriteNode spriteNodeWithTexture:rabbitarray[color%8] size:CGSizeMake(textureSize, textureSize)];
                }else{
                    heart = [SKSpriteNode spriteNodeWithTexture:catarray[color%8] size:CGSizeMake(textureSize, textureSize)];
                }
            }
            
            heart.alpha = 0.5f;
            heart.position = CGPointMake(45+70*i, -70*j);
            [heart runAction:sequence];
            [self addChild:heart];
        }
    }
}

-(void)dealloc
{
    
}


@end
