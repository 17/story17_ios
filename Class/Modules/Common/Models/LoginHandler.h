//
//  LoginHandler.h
//  Story17
//
//  Created by Racing on 2016/1/5.
//  Copyright © 2016年 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "WeiboSDK.h"
#import "WXApiManager.h"
#import "MultiFileUploader.h"

typedef NS_ENUM(NSInteger, loginMode){
    weiboLoginMode,
    wechatLoginMode,
    qqLoginMode
};

@protocol LoginHandlerDelegate <NSObject>

@optional
- (void) loginSuccess:(BOOL)success loginMode:(loginMode)loginMode userid:(NSString*)userid token:(NSString*)token;
- (void) requestUserInfo:(BOOL)success loginMode:(loginMode)loginMode;
@end

@interface LoginHandler : NSObject<TencentSessionDelegate,WXApiManagerDelegate,WeiboSDKDelegate,WBHttpRequestDelegate>
+ (BOOL)isGuest;
+ (void)handleUserLogin;
+ (LoginHandler*)sharedInstance;
-(void)getUserInfo:(loginMode)mode;
- (void) loginWithType:(loginMode)mode withViewController:(UIViewController*)vc;

@property (nonatomic, strong) TencentOAuth* tencentOAuth;
@property (nonatomic, assign) id<LoginHandlerDelegate> delegate;
@property (nonatomic, strong) MultiFileUploader* multiFileUploader;

@end
