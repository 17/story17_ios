//
//  IAPManager.h
//  Story17
//
//  Created by POPO on 11/11/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "ProductObject.h"
@protocol IAPManagerDelegate1 <NSObject>
-(void)pointRefresh;
@end


@protocol IAPManagerDelegate <NSObject>
-(void)pointRefresh;
@end

@interface IAPManager : NSObject<SKProductsRequestDelegate, SKPaymentTransactionObserver>

@property (nonatomic,strong)NSMutableArray* productArray;
@property (nonatomic)BOOL showUI;
@property (nonatomic)BOOL remainingWarning;
@property (nonatomic)BOOL buying;
- (void)displayStoreUI;
- (void)getProductList;

@property (nonatomic, weak) id<IAPManagerDelegate> delegate;
@property (nonatomic, weak) id<IAPManagerDelegate1> delegate1;

@end
