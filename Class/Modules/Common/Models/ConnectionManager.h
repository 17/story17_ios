//
//  FacebookManager.h
//  story17
//
//  Created by POPO Chen on 5/20/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectionManager : NSObject


/* Contact book related */
@property (nonatomic,strong) NSMutableArray* fbFriendIDArray;

/* Contact book related */
@property (nonatomic,strong) NSMutableArray* contactsArray;
@property (nonatomic,strong) NSMutableArray* fullNameArray;
@property (nonatomic,strong) NSMutableArray* phoneNumerArray;
@property (nonatomic,strong) NSMutableDictionary* numberMapNameDict;
@property (nonatomic,strong) NSMutableDictionary* nameMapNumberDict;
@property (nonatomic,strong) NSMutableDictionary* getImageFromFullName;

@property (nonatomic,strong) NSMutableArray* addedFriends;
@property (nonatomic,strong) NSMutableArray* unAddedFriends;
@property (nonatomic,strong) NSMutableArray* unPlayedFriends;

@property (nonatomic,strong) NSString* fbName;


-(void) showPermissionAlert:(void(^)(BOOL success,NSString* message))callback;
-(NSArray*)getPhoneNumerForSearch;
-(NSString*)getRealNameByPhone:(NSString*)phone;
//-(void) showFBPermissionAlert;

@end
