//
//  UpAndDownAnimator.m
//  Story17
//
//  Created by Racing on 2015/11/30.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "UpAndDownAnimator.h"
#import "Constant.h"

static const NSTimeInterval kAnimationDuration = 0.4f;

@implementation UpAndDownAnimator
- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return kAnimationDuration;
}

- (void)animatePresentingInContext:(id<UIViewControllerContextTransitioning>)transitionContext toVC:(UIViewController *)toVC fromVC:(UIViewController *)fromVC
{
    CGRect fromVCRect = [transitionContext initialFrameForViewController:fromVC];
    CGRect toVCRect = fromVCRect;
    toVCRect.origin.y = toVCRect.size.height;
    
    toVC.view.frame = toVCRect;
    UIView *container = [transitionContext containerView];
    [container addSubview:fromVC.view];
    [container addSubview:toVC.view];
    
    [UIView animateWithDuration:kAnimationDuration animations:^{
        toVC.view.frame = fromVCRect;
    } completion:^(BOOL finished) {
        if ([transitionContext transitionWasCancelled]) {
            [transitionContext completeTransition:NO];
        } else {
            [transitionContext completeTransition:YES];
        }
    }];
}

- (void)animateDismissingInContext:(id<UIViewControllerContextTransitioning>)transitionContext toVC:(UIViewController *)toVC fromVC:(UIViewController *)fromVC
{
    CGRect fromVCRect = [transitionContext initialFrameForViewController:fromVC];
    fromVCRect.origin.y = fromVCRect.size.height;
    
    UIView *container = [transitionContext containerView];
    [container addSubview:toVC.view];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    alphaView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    [container addSubview:alphaView];
    
    [container addSubview:fromVC.view];
    
    [UIView animateWithDuration:kAnimationDuration animations:^{
        fromVC.view.frame = fromVCRect;
        alphaView.alpha = 0;
    } completion:^(BOOL finished) {
        if ([transitionContext transitionWasCancelled]) {
            [transitionContext completeTransition:NO];
            [toVC.view removeFromSuperview];
        } else {
            [transitionContext completeTransition:YES];
        }
    }];
}

@end
