#import "MultiFileUploader.h"
#import "UploadObject.h"

@implementation MultiFileUploader

@synthesize fileNameArray;
@synthesize completeCount;
@synthesize uploadObjectArray;
@synthesize multiFileUploaderCallback;


-(id)init
{
    self = [super init];
    if(self){
        uploadObjectArray = CREATE_MUTABLE_ARRAY;
        completeCount = 0;
    }
    return self;
}

-(id) initWithFileNameArray:(NSArray *)fileArray
{
    if (self = [super init]) {
        uploadObjectArray = CREATE_MUTABLE_ARRAY;
        fileNameArray = fileArray;
        completeCount = 0;
    }
    
    return self;
}

-(void) startUploadWithCallback:(void (^)(NSArray* fileNameArray, int code,float progress)) callback
{
    multiFileUploaderCallback = callback;
    
    __block float totalProgress = 0.0f;
    int fileCount = (int)[fileNameArray count];
    
    for(NSString* fileName in fileNameArray) {
        
        UploadObject* uploadObject = [[UploadObject alloc] initWithFileName:fileName];

        [uploadObjectArray addObject:uploadObject];
        
        [uploadObject startUploadWithCallback:^(NSString *fileName, int code, float progress) {
            
            if(code==UPLOAD_SUCCESS) {
                
                completeCount++;
        
                totalProgress = (float)completeCount/(float)fileCount;
                if(completeCount==fileNameArray.count) {
        
                    if(multiFileUploaderCallback!=nil) {
                        multiFileUploaderCallback(fileNameArray, MULTI_UPLOAD_SUCCESS,totalProgress);
                    }
                    [self stopUpload];
                }
            } else if(code==UPLOAD_FAIL) {
                
                if(multiFileUploaderCallback!=nil) {
                    multiFileUploaderCallback(fileNameArray, MULTI_UPLOAD_FAIL,0.0);
                    [self stopUpload];
                }
            }else if(code==PROGRESSING){
                
                totalProgress = ((float)(completeCount+progress)/(float)fileCount);
                multiFileUploaderCallback(fileNameArray, PROGRESSING,totalProgress);
            
            }
        }];
    }
}

-(void) stopUpload
{
    multiFileUploaderCallback = nil;
    
    for(UploadObject* u in uploadObjectArray) {
        u.uploadCallback = nil;
    }
}

@end
