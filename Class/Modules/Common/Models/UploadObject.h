#import "Constant.h"

#define FILE_UPLOAD_MODE 0
#define MESSAGE_UPLOAD_MODE 1
#define PROFILE_PICTURE_UPLOAD_MODE 2
#define CHATROOM_PICTURE_UPLOAD_MODE 3

@class MessageObject;

@interface UploadObject : NSObject

@property (nonatomic, retain) NSString* filename;
@property (nonatomic, strong) AFURLSessionManager* manager;
@property (nonatomic, strong) NSProgress* progress;

@property (nonatomic, copy) void(^uploadCallback)(NSString* fileName, int code, float progress);

-(id) initWithFileName:(NSString *)file;
-(void) startUploadWithCallback:(void (^)(NSString* fileName, int code, float progress)) callback;;

@end
