//
//  SuggestionObject.h
//  Story17
//
//  Created by POPO on 5/12/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserObject.h"
#import "PostObject.h"

@interface SuggestionObject : NSObject

@property (nonatomic,strong) UserObject* user;
@property (nonatomic,strong) PostObject* post1;
@property (nonatomic,strong) PostObject* post2;
@property (nonatomic,strong) PostObject* post3;

+(SuggestionObject*) getSuggestionObjWithDict:(NSDictionary*)dict;

@end
