//
//  EventHandler.h
//  story17
//
//  Created by POPO Chen on 7/23/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventHandler : NSObject


+(EventHandler*) sharedInstance;

-(void)addEventTracking:(NSString *)eventName withDict:(NSDictionary *)params;
-(NSString*)readNowStatus;
-(void)setNowStatus:(NSString*)value;
-(void)addNetworkUnstableCount;
-(int)readNowNetworkUnstableCount;
-(void)setEnterPageTime;
-(int)readEnterPageTime;
-(void)addAudioOnlyCount;
-(int)readAudioOnlyCount;
@property (nonatomic) int enterPageTimeStamp;
@property (nonatomic) int networkUnstableCount;
@end
