//
//  AdObject.h
//  story17
//
//  Created by POPO Chen on 4/30/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdObject : NSObject



@property(nonatomic,strong) NSString* adID;
@property(nonatomic,strong) NSString* caption;
@property(nonatomic,strong) NSString* actionName;
@property(nonatomic,strong) NSString* androidAppPackageName;
@property(nonatomic,strong) NSString* iosAppID;
@property(nonatomic,strong) NSString* adUrl;
@property(nonatomic,strong) NSString* urlScheme;
@property(nonatomic,strong) NSString* picture;
@property(nonatomic,strong) NSString* video;
@property(nonatomic,strong) NSString* descrip;
@property(nonatomic,strong) NSString* headline;
@property(nonatomic,strong) NSString* bidType;

@property int clickPrice;
@property int impressionPrice;
@property int installPrice;

+(AdObject *)getAdWithDict:(NSDictionary *)dict;

@end
