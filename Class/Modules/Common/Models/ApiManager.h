#import <Foundation/Foundation.h>
#import "Constant.h"
#import "AFNetworking.h"
#import "UserObject.h"
#import "PostObject.h"
#import "MessageObject.h"
#import "NotificationObject.h"
#import "CommentObject.h"
#import "NSString+Helper.h"
#import "PaymentObject.h"
#import "TagObject.h"
#import "AdObject.h"
#import "LocationObject.h"
#import "SuggestionObject.h"
#import "RevenueObject.h"
#import "LiveStreamObject.h"
#import "LiveStreamCommentObject.h"
#import "PointUsageLogObject.h"
#import "PointGainObject.h"
#import "ProductObject.h"

#import "GiftObject.h"
#import "UserGiftObject.h"
#import "GiftLeaderboardRecordObject.h"
#import "LiveStreamGiftLeaderboardObject.h"

@class PostObject;
@class LiveStreamObject;
@class LiveStreamGiftLeaderboardObject;

@interface ApiManager : NSObject

@property (strong,nonatomic) AFHTTPRequestOperationManager *manager; // compose
#pragma mark - Register Related
-(void) loginAction:(NSDictionary*)params completion: (void (^)(BOOL success, NSString* message)) callback;
-(void) loginAction2:(NSDictionary*)params completion: (void (^)(BOOL success, NSString* message)) callback;
-(void) recoverPassword:(NSString*)openID email:(NSString*)email completion: (void (^)(BOOL success, NSString* message)) callback;
-(void) checkOpenIDAvailable:(NSString*) openID completion: (void (^)(BOOL success, NSString* message)) callback;
-(void) registerAction: (NSDictionary*) params completion: (void (^)(BOOL success, NSString* message)) callback;
-(void) registerAction2: (NSDictionary*) params completion: (void (^)(BOOL success, NSString* message)) callback;
-(void) sendVerificationCode:(NSString*)countryCode andPhoneNum:(NSString*)phoneNumber completion:(void(^)(BOOL success, NSString* message)) callback;
-(void) checkLoginWithCompletion: (void(^) (BOOL success, NSString* message))callback;
-(void)sendPhoneVerificationCode:(NSString*)countryCallingCode phoneNumber:(NSString*)phoneNumber type:(NSString*)type completion:(void(^)(BOOL success, NSString *message , NSString *requestId)) callback;
-(void)registerByPhoneAction:(NSString*)openID password:(NSString*)password requestID:(NSString*)requestID countryCode:(NSString*)countryCode phoneNumber:(NSString*)phoneNumber verificationCode:(NSString*)verificationCode phoneCountry:(NSString*)phoneCountry  optionalDict:(NSDictionary*)optionalDict completion:(void(^)(BOOL success, NSString *message , NSString *accesToken , NSDictionary *userInfo)) callback;
-(void) isPhoneNumberOpenIDPaired:(NSString*)countryCallingCode localPhoneNumber:(NSString*)localPhoneNumber openID:(NSString*)openID completion:(void(^)(BOOL success, NSString *message , BOOL isPaired))callback;
- (void)humanTestSeconderyValidateWithType:(NSString *)type result:(NSDictionary *)result completion:(void(^)(BOOL success))callback;
- (void)resetPassword:(NSString*)resetPasswordToken newPassword:(NSString*)newPassword confirmNewPassword:(NSString*)confirmNewPassword withCompletion:(void(^)(BOOL success, NSString* message))callback;
- (void)getResetPasswordToken:(NSString*)countryCode openID:(NSString*)openID requestID:(NSString*)requestID phoneNumber:(NSString*)phoneNumber verificationCode:(NSString*)verificationCode withCompletion:(void(^)(BOOL success, NSString *message ,NSString *resetPasswordToken))callback;
-(void) isPhoneNumberUsed:(NSString*)countryCallingCode andPhoneNum:(NSString*)localPhoneNumber completion:(void(^)(BOOL success, NSString* message,BOOL isUsed)) callback;

-(void)changePhoneNumber:(NSString*)openID  requestID:(NSString*)requestID countryCode:(NSString*)countryCode phoneNumber:(NSString*)phoneNumber verificationCode:(NSString*)verificationCode phoneCountry:(NSString*)phoneCountry completion:(void(^)(BOOL success, NSString *message )) callback ;
- (void)checkHumanTestWithType:(NSString *)type completion:(void(^)(BOOL success, BOOL isNeeded))callback;

#pragma mark - User Related
-(void) getUserInfo:(NSString*)targetOpenID completion:(void (^)(BOOL success, UserObject* userObject)) callback;
-(void) isUserOnLiveStream:(NSString*)targetUserID completion:(void (^)(BOOL success, int userIsOnLive, NSString* livestreamID)) callback;
-(void) getUserInfoByUserID:(NSString*)targetUserID completion:(void (^)(BOOL success, UserObject* userObject)) callback;
-(void) getSelfInfo:(void (^)(BOOL success)) callback;
-(void) unblockUserAction:(NSString*)unblockUserID withCompletion:(void (^)(BOOL success)) callback;
-(void) blockUserAction:(NSString*)blockUserID withCompletion:(void (^)(BOOL success)) callback;
-(void) followUserAction:(NSString*)targetUserID withCompletion:(void (^)(BOOL success)) callback;
-(void) unfollowUserAction:(NSString*)targetUserID withCompletion:(void (^)(BOOL success)) callback;

#pragma mark - Follow Request
-(void)getFollowRequests:(int)count beforeTime:(int)beforeTime withCompletion:(void(^)(BOOL success,NSArray* dict))callback;
-(void)sendFollowRequest:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback;
-(void)cancelFollowRequests:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback;
-(void)acceptFollowRequest:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback;
-(void)rejectFollowRequest:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback;

-(void) followAllAction:(NSString*)jsonData withCompletion:(void (^)(BOOL success)) callback;
-(void) reportUserAction:(NSString*)reportedUserID withReason:(NSString*)reason andMessage:(NSString*)message withCompletion:(void (^)(BOOL success , NSString* message)) callback;
-(void) reportPostAction:(NSString*)reportedUserID withReason:(NSString*)reason andPostID:(NSString*)postID withCompletion:(void (^)(BOOL success , NSString* message)) callback;
-(void) reportCommentAction:(NSString*)reportedUserID withReason:(NSString*)reason andCommentID:(NSString*)commentID withCompletion:(void (^)(BOOL success , NSString* message)) callback;
-(void) reportLiveAction:(LiveStreamObject*)livestream withReason:(NSString*)reason withCompletion:(void (^)(BOOL success , NSString* message)) callback;

-(void) updateUserInfo:(NSDictionary*)dic fetchSelfInfo:(BOOL)fetch completion:(void(^)(BOOL success))callback;
-(void) updateOpenID:(NSString*)openID completion:(void(^)(BOOL success))callback;
-(void) changePassword:(NSString*)oldPassword newPassword:(NSString*)newPassword withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void) getSearchUsers: (NSString*)query followingOnly:(int)followingOnly offset:(int)offset count:(int)count completion:(void (^)(BOOL success, NSArray* userObjects)) callback;
-(void) getSuggestedUsers:(int)offset count:(int)count completion:(void (^)(BOOL success, NSArray* suggestedUser)) callback;
-(void) getExploreSuggestedUsers:(int)beforeTime count:(int)count completion:(void (^)(BOOL success, NSArray* suggestedUser)) callback;

-(void) getFriendSuggestion:(NSString*)friendUserID offset:(int)offset count:(int)count completion:(void (^)(BOOL success, NSArray* userObjects)) callback;
-(void) getHotUsers:(int)offset count:(int)count completion:(void (^)(BOOL success, NSArray* suggestedUser)) callback;
//-(void) findFriendsByPhone: (NSString*)phoneNumbers completion:(void (^)(BOOL success, NSArray* userObjects)) callback;
-(void) freezeUserAction:(NSString*)freezedUserID inLivestreamID:(NSString*)inLivestreamID absTime:(int)absTime withComletion:(void(^)(BOOL success, NSString* message)) callback;
-(void) banUserAction:(NSString*)bannedUserID inLivestreamID:(NSString*)inLivestreamID  absTime:(int)absTime withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void) adminBlockUserAction:(NSString*)userID publisherUserID:(NSString*)publisherUserID blockedUserID:(NSString*)blockedUserID inLivestreamID:(NSString*)inLivestreamID  withCompletion:(void(^)(BOOL success))callback;
-(void) adminBlockDirtyWordAction:(NSString*)messageString withCompletion:(void(^)(BOOL success))callback;
-(void) processProfilePictureReview:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback;
-(void) internationalTagAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void) removeInternationalTagAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback;

-(void)celebrityAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void)removeCelebrityAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback;

-(void) getLiveStreamsForReview:(NSString*)region beforeTime:(int)beforeTime count:(int)count fetchMode:(NSString*)fetchMode modulo:(int)modulo devisor:(int)devisor withCompletion:(void (^)(BOOL success,NSArray* liveStreams)) callback;
-(void) passLiveStream:(NSString*)liveStreamID withCompletion:(void (^)(BOOL success))callback;
-(void) hideLiveStream:(NSString*)liveStreamID withCompletion:(void (^)(BOOL success))callback;
-(void) getExplorePicturesForReview:(NSString*)region beforeTime:(int)beforeTime count:(int)count fetchMode:(NSString*)fetchMode modulo:(int)modulo devisor:(int)devisor withCompletion:(void (^)(BOOL success,NSArray* liveStreams)) callback;
-(void) passExplorePicture:(NSString*)postID withCompletion:(void (^)(BOOL success))callback;
-(void) hideExplorePicture:(NSString*)postID withCompletion:(void (^)(BOOL success))callback;

-(void) hideUserFromLiveAction:(NSString*)targetUserID hideUserValue:(int)hideUserValue withCompletion:(void (^)(BOOL success))callback;

#pragma mark Notification Related
-(void) getNotif:(int)beforeTime andCount:(int)count withCompletion:(void(^)(BOOL success,NSArray* dict))callback;
-(void) getFriendNotif:(int)beforeTime andCount:(int)count withCompletion:(void(^)(BOOL success,NSArray* dict))callback;
- (void) sendPaymentPushSystemNotif;
-(void) getSystemNotif:(int)beforeTime andCount:(int)count withCompletion:(void(^)(BOOL success,NSArray* dict))callback;
-(void) readSystemNotifWithCompletion:(void(^)(BOOL success))callback;
-(void) readNotif:(void(^)(BOOL success))callback;

#pragma mark - Post Related
-(void) getPostFeed:(int)beforeTime andCount:(int)count fetchLiveStreamL:(int)fetchLiveStream withCompletion:(void(^)(BOOL success,NSArray* posts))callback;
-(void) getPostLikers:(NSString*)postID offset:(int)offset count:(int)count withCompletion:(void(^)(BOOL success, NSArray* userObjects))callback;
-(void) getUserLikers:(NSString*)targetUserID offset:(int)offset count:(int)count withCompletion:(void(^)(BOOL success, NSArray* userObjects))callback;
-(void) getUserPost:(NSString *)targetUserID beforeTime:(int)beforeTime andCount:(int)count withCompletion:(void(^)(BOOL success,NSArray* posts))callback;
-(void) getPostInfo:(NSString*)postID withCompletion:(void(^)(BOOL success,PostObject* post))callback;
-(void) publishPost:(NSDictionary*)params withCompletion:(void(^)(BOOL success,NSString* message, PostObject* post))callback;
-(void) likePost:(NSString*)postID withCompletion:(void(^)(BOOL success))callback;
-(void) unlikePost:(NSString*)postID withCompletion:(void(^)(BOOL success))callback;
-(void) commentPost:(NSString*)postID userTags:(NSString*)userTags comment:(NSString*)comment withCompletion:(void(^)(BOOL success,NSString* message, NSString* timestamp,NSString* commentID))callback;
-(void) getComments:(NSString*)postID beforeTime:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* comments))callback;
-(void) deleteComment:(NSString*)commentID postID:(NSString*)postID withCompletion:(void(^)(BOOL success))callback;
-(void) deletePost:(NSString*)postID withCompletion:(void(^)(BOOL success))callback;
-(void) clearPostComment:(NSString*)postID withCompletion:(void(^)(BOOL success))callback;
-(void) viewPost:(NSString*)postIDs withCompletion:(void(^)(BOOL success))callback;
-(void) getPostByHashTag:(NSString*)hashTag beforeTime:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* posts))callback;
-(void) getPostByLocation:(NSString*)locationID beforeTime:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* posts))callback;
-(void) getHotPost:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* posts))callback;
-(void) getLikedPost:(NSString*)targetUserID beforeTime:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* posts))callback;
-(void) searchHashTag:(NSString*)query offset:(int)offset count:(int)count withCompletion:(void(^)(BOOL success, NSArray* tags))callback;
-(void) createLocation:(NSString*)name latitude:(int)latitude longitude:(int)longitude withCompletion:(void(^)(BOOL success, NSString* locationID))callback;
-(void) searchLocation:(NSString*)query offset:(int)offset count:(int)count withCompletion:(void(^)(BOOL success, NSArray* tags))callback;
-(void) repostAction:(NSString*)postID withCompletion:(void(^)(BOOL success))callback;
-(void) removeRepostAction:(NSString*)postID withCompletion:(void(^)(BOOL success))callback;
-(void) updatePost:(NSDictionary*)params withCompletion:(void(^)(BOOL success,NSString* message, PostObject* post))callback;

-(void) likePostBatchUpdate:(NSString*)postID likeCount:(int)likeCount withCompletion:(void(^)(BOOL success))callback;


#pragma mark - Advertise Related
-(void) getAds:(int)offset count:(int)count withCompletion:(void(^)(BOOL success,NSArray* adArray))callback;
-(void) adTrack:(NSString*)data withCompletion:(void(^)(BOOL success))callback;

#pragma mark - Payment Related
-(void) sendSysNotif:(NSString*)targetUserID withMessage:(NSString*)msg withCompletion:(void(^)(BOOL success))callback;
-(void) balancePayment:(NSString*)targetOpenID amount:(NSString*)amount toBankName:(NSString*)bankName account:(NSString*)account inYear:(NSString*)y month:(NSString*)m day:(NSString*)d withMessage:(NSString*)msg withCompletion:(void(^)(BOOL success))callback;
-(void) getPayments:(int)offset count:(int)count withCompletion:(void(^)(BOOL success,NSArray* paymentArray))callback;
-(void) getRevenueReport:(int)minTimestamp maxTimestamp:(int)maxTimestamp withCompletion:(void(^)(BOOL success,NSArray* adArray))callback;
-(void) getPostRevenueReport:(int)offset count:(int)count withCompletion:(void(^)(BOOL success,NSArray* postRevenueArray))callback;

#pragma mark - Find Friend Related
-(void)uploadPhoneNumbers:(NSString*)myPhoneNumber phoneNumbers:(NSString*)phoneNumbersJsonString withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void)uploadFacebookFriends:(NSString*)myFacebookName facebookIDs:(NSString*)facebookIDsJson withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void)uploadTwitterFriends:(NSString*)myTwitterName twitterIDs:(NSString*) twitterIDsJson withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void)uploadInstagramFriends:(NSString*)myInstagramName instagramIDs:(NSString*)instagramIDsJson withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void) findFriends: (NSString*)jsonData method:(NSString*)method completion:(void (^)(BOOL success, NSArray* userObjects)) callback;

#pragma mark - Admin Only
-(void)adminPromotePost:(NSString*)postID withCompletion:(void(^)(BOOL success))callback;
//-(void)freezeUserAction:(NSString*)freezedUserID inLivestreamID:(NSString*)inLivestreamID withCompletion:(void(^)(BOOL success,NSString* message))callback;
//-(void)banUserAction:(NSString*)bannedUserID inLivestreamID:(NSString*)inLivestreamID withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void)verifyUserAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void)promoteLiveStream:(NSString*)liveStreamID countryName:(NSString*)countryName withCompletion:(void(^)(BOOL success,int hasRestreamed))callback;
-(void)removePromotedLiveStream:(NSString*)liveStreamID countryName:(NSString*)countryName withCompletion:(void(^)(BOOL success,int hasRestreamed))callback;
-(void)promoteTopLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success))callback;
-(void)cancelPromoteTopLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success))callback;
-(void)checkingSandBox:(void (^)(BOOL success))callback;

-(void)removeVerifiedUserAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void)hidePostAction:(NSString*)postID withCompletion:(void(^)(BOOL success))callback;
-(void)hideHotLivestreamAction:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success))callback;
-(void)adminDeletePostAction:(NSString*)postID message:(NSString*)message targetUserID:(NSString*)targerUserID withCompletion:(void(^)(BOOL success))callback;
-(void)adminDeleteProfilePictureAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback;
-(void)sendOpenidAlert:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback;

-(void)choiceUserAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void)removeChoiceUserAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void)clearPostLikeFromUser:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback;
-(void)clearPostLikeByUserReceivedLikeTable:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback;
-(void)recalculateUserReceivedLikeCount:(NSString*)targetUserID withCompletion:(void(^)(BOOL success))callback;
-(void)writeUserReceivedLikeCount:(NSString*)targetUserID toLeaderBoard:(int)leaderBoardType withCompletion:(void(^)(BOOL success))callback;
-(void)adOnAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void)adOffAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void)addCrossGreatWallFlag:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback;
-(void)removeCrossGreatWallFlag:(NSString*)targetUserID withCompletion:(void(^)(BOOL success,NSString* message))callback;

-(void) getCountry:(void (^)(BOOL success, NSString* country)) callback;
-(void) getFollowing:(NSString*)targetUserID beforeTime:(int)beforeTime andCount:(int)count withCompletion:(void(^)(BOOL success,NSArray* dict))callback;
-(void) getFollower:(NSString*)targetUserID beforeTime:(int)beforeTime andCount:(int)count withCompletion:(void(^)(BOOL success,NSArray* dict))callback;
-(void) getLikeLeaderboard:(NSString*)mode count:(int)count offset:(int)offset withCompletion:(void (^)(BOOL success,NSArray* userObjects)) callback;
-(void) getReportPost:(int)count offset:(int)offset withCompletion:(void (^)(BOOL success , NSArray* posts))callback;
-(void) getReviewProfile:(int)count offset:(int)offset withCompletion:(void (^)(BOOL success , NSArray* users))callback;
-(void) processReportedPostAction:(NSString*)postID withCompletion:(void (^)(BOOL success))callback;
-(void) killLiveStreamAction:(NSString*)liveStreamID targetUserID:(NSString*)targetUserID withCompletion:(void (^)(BOOL success))callback;

//-(NSDictionary*)dictPackageEncoding:(NSMutableDictionary*)dict withAction:(NSString*)action;
//-(void)dictPackageEncoding:(NSMutableDictionary*)dict withAction:(NSString*)action withCompletion:(void(^)(NSDictionary* dict))callback;

#pragma mark - LiveStream Related
-(void)publishLiveStream:(NSDictionary*)publishDict withCompletion:(void(^)(BOOL success, NSString* liveStreamID, int timestamp, NSString* colorCode,NSString* publicIP))callback;
-(void)keepLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success, NSString* message))callback;
-(void)updateLiveStreamInfo:(NSDictionary*)updateDict withCompletion:(void(^)(BOOL success))callback;
-(void)endLiveStream:(NSString*)liveStreamID duration:(int)duration withCompletion:(void(^)(BOOL success))callback;
-(void)getLiveStreamComments:(NSString*)liveStreamID afterTime:(int)afterTime commentCount:(int)count withCompletion:(void(^)(BOOL success, NSArray* liveStreamCommentObjects))callback;
-(void)getLiveStreamViewers:(NSString*)liveStreamID afterTime:(int)afterTime viewerCount:(int)count withCompletion:(void(^)(BOOL success, NSArray* dict))callback;
-(void)getLiveStreamReplayViewers:(NSString*)liveStreamID beforeTime:(int)beforeTime viewerCount:(int)count withCompletion:(void(^)(BOOL success, NSArray* dict))callback;
-(void)viewLiveStreamReplay:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success))callback;
-(void)getLiveStreamInfo:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success, LiveStreamObject* liveStreamInfo))callback;
-(void)getUserLiveStreams:(NSString*)targetUserID beforeTime:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* livestreams))callback;
-(void)getLiveStreamFeed:(void(^)(BOOL success, NSArray* liveSteamObjects))callback;
-(void)getHotLiveStreams:(void(^)(BOOL success, NSArray* liveSteamObjects))callback;
-(void)getLocalLiveStreams:(NSString*)sortMode withCompletion:(void(^)(BOOL success, NSArray* liveSteamObjects))callback;

-(void)getHotLiveStreams2:(NSString*)region count:(int)count withCompletion:(void(^)(BOOL success, NSArray* liveSteamObjects))callback;
-(void)getSuggestedLiveStreams:(NSString*)region count:(int)count withCompletion:(void(^)(BOOL success, NSArray* liveSteamObjects))callback;
-(void)getLatestLiveStreams:(NSString*)region count:(int)count withCompletion:(void(^)(BOOL success, NSArray* liveSteamObjects))callback;

-(void)getBlockUserList:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* userObjects))callback;

-(void)getLatestLiveStreamReplays:(int)beforeTime liveStreamCount:(int)count withCompletion:(void(^)(BOOL success, NSArray* liveSteamObjects))callback;
-(void)enterLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success, NSString* message, int timestamp, int numberOfChunks, NSString* colorCode,NSString* publicIP))callback;
-(void)previewLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success))callback;
-(void)keepViewLiveStream:(NSString*)liveStreamID isPreview:(int)isPreview withCompletion:(void(^)(BOOL success,BOOL blocked,NSString* message))callback;
-(void)quitViewLiveStream:(NSString*)liveStreamID duration:(int)duration withCompletion:(void(^)(BOOL success))callback;
-(void)likeLiveStream:(NSString*)liveStreamID absTimestamp:(int) absTimestamp withCompletion:(void(^)(BOOL success))callback;
-(void)likeLivestreamBatchUpdate:(NSString*)liveStreamID likeCount:(int)likeCount absTimestamp:(int) absTimestamp withCompletion:(void(^)(BOOL success))callback;
-(void)commentLiveStream:(NSString*)liveStreamID comment:(NSString*)comment colorCode:(NSString*)hexColorCode absTimestamp:(int)absTimestamp withCompletion:(void(^)(BOOL success, NSString* message))callback;
-(void)restreamLiveStream:(NSString*)liveStreamID liveStreamerOpenID:(NSString*)liveStreamerOpenID myOpenID:(NSString*)myOpenID  withCompletion:(void(^)(BOOL success))callback;
-(void)hasRestreamedLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success,int hasRestreamed))callback;
-(void)checkFacebookIDAvailable:(NSString*)facebookID completion: (void (^)(BOOL success, NSString* message)) callback;
-(void)checkWechatIDAvailable:(NSString*)wechatID completion: (void (^)(BOOL success, NSString* message)) callback;
-(void)checkWeiboIDAvailable:(NSString*)weiboID completion: (void (^)(BOOL success, NSString* message)) callback;
-(void)checkQQIDAvailable:(NSString*)qqID completion: (void (^)(BOOL success, NSString* message)) callback;
-(void)checkTwitterIDAvailable:(NSString*)twitterID completion: (void (^)(BOOL success, NSString* message)) callback;
//-(void)getRegionLivestream:(NSString*)ipCountry withCompletion:(void(^)(BOOL success, NSArray* liveSteamObjects))callback;
-(void)getLiveStreamHotCountryList:(void(^)(BOOL success, NSArray* regionList))callback;
-(void)recoverPostAction:(NSString*)targetUserID withCompletion:(void(^)(BOOL sucess))callback;

#pragma mark - GroupLiveStream Related
-(void)publishGroupLiveStream:(NSDictionary*)publishDict withCompletion:(void(^)(BOOL success, NSString* liveStreamID, int timestamp, NSString* colorCode))callback;
-(void)updateGroupLiveStreamInfo:(NSDictionary*)updateDict withCompletion:(void(^)(BOOL success))callback;
-(void)getGroupLiveStreamInfo:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success, LiveStreamObject* liveSteamInfo))callback;
-(void)getUserGroupLiveStreams:(NSString*)targetUserID beforeTime:(int)beforeTime count:(int)count withCompletion:(void(^)(BOOL success, NSArray* livestreams))callback;
-(void)getHotGroupLiveStreams:(void(^)(BOOL success, NSArray* liveSteamObjects))callback;
-(void)enterGroupLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success, NSString* message, int timestamp, int numberOfChunks, NSString* colorCode))callback;
-(void)likeGroupLiveStreamBatchUpdate:(NSString*)liveStreamID likeCount:(int)likeCount absTimestamp:(int) absTimestamp withCompletion:(void(^)(BOOL success))callback;
-(void)restreamGroupLiveStream:(NSString*)liveStreamID liveStreamerOpenID:(NSString*)liveStreamerOpenID myOpenID:(NSString*)myOpenID withCompletion:(void(^)(BOOL success))callback;
-(void)hasRestreamedGroupLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success,int hasRestreamed))callback;

#pragma mark - Gift Related

/* Purchase Point Api */
- (void)getPurchaseCountWithCompletion:(void(^)(BOOL success, int32_t count))callback;
- (void)getPointProductListWithCompletion:(void(^)(BOOL success, NSArray *productObjs))callback;
- (void)purchaseProductWithReceipt:(NSString*)receipt andPriceCountryCode:(NSString*)priceCountryCode withCompletion:(void(^)(BOOL success, int32_t purchasedPoint, int32_t errorCode))callback;
/* My Point */
- (void)getPointUsageLogFromUser:(NSString*)userID beforeTime:(int32_t)timestamp withCount:(int32_t)count withCompletion:(void(^)(BOOL success, NSArray* pointUsageLogs))callback;
- (void)getPointGainLogFromUser:(NSString*)userID beforeTime:(int32_t)timestamp withCount:(int32_t)count withCompletion:(void(^)(BOOL success, NSArray* pointGainLogs))callback;
/* Revenue Page */
- (void)getReceivedGiftLog:(NSString*)userID beforeTime:(int32_t)timestamp withCount:(int32_t)count withCompletion:(void(^)(BOOL success, NSArray* receivedGiftLogs))callback;
/* Gift Api */
- (void)getGiftListWithCompletion:(void(^)(BOOL success, NSArray* giftsback))callback;
- (void)sendGift:(NSString*)giftID toLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success))callback;
- (void)readGift:(int32_t)giftToken toLiveStream:(NSString*)liveStreamID withCompletion:(void(^)(BOOL success))callback;
/* get on gift by token */
- (void)getLiveStream:(NSString*)liveStreamID giftToken:(int32_t)giftToken withCompletion:(void(^)(BOOL success, UserGiftObject* userGift))callback;
/* get afterTime gift */
- (void)getReceivedGiftsInLiveStream:(NSString*)liveStreamID afterTime:(int32_t)time withCount:(int32_t)count withCompletion:(void(^)(BOOL success, NSArray* gifts))callback;
/* Gift LeaderBoard */
- (void)getGiftLeaderboardType:(GiftLeaderboardRecordType)type fromUser:(NSString*)userID withCount:(int32_t)count andOffset:(int32_t)offset withCompletion:(void(^)(BOOL success, NSArray* giftLeaderboardRecords))callback;

- (void)getUnreadReceivedGift:(NSString*)liveStreamID beforeTime:(int32_t)time withCount:(int32_t)count withCompletion:(void(^)(BOOL success, NSArray* gifts))callback;

//event tracking
-(void)eventTracking:(NSArray*)eventArray withCompletion:(void(^)(BOOL success))callback;

// deprecated
- (void)getGiftInfoByGiftID:(NSString*)giftID withCompletion:(void(^)(BOOL success, GiftObject* gift))callback;
- (void)getSentGiftLog:(NSString*)userID beforeTime:(int32_t)timestamp withCount:(int32_t)count withCompletion:(void(^)(BOOL success, NSArray* sentGiftLogs))callback;
- (void)getLiveStreamGiftLeaderboardByUserId:(NSString *)userId liveStreamId:(NSString *)liveStreamId withCount:(int32_t)count andOffset:(int32_t)offset withCompletion:(void(^)(BOOL success, LiveStreamGiftLeaderboardObject* liveStreamGiftLeaderboard))callback;
- (void)getGiftHistoryInLiveStream:(NSString*)liveStreamID withCount:(int32_t)count andOffset:(int32_t)offset withCompletion:(void(^)(BOOL success, NSArray* gifts))callback;

//Params
-(NSDictionary *)baseDict;


@end