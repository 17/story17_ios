#import "Constant.h"
#import "WXApi.h"
#import "IAPManager.h"
#import "GiftObject.h"
#import "ShareManager.h"

#define PICKER_MODE_ITEM 0
#define PICKER_MODE_RANGE 1

#define LINK_LIVE 0
#define LINK_OTHERS 1

typedef NS_ENUM(NSInteger, ShareMediaType) {
    ShareMediaTypeApp = 0,
    ShareMediaTypeUser,
    ShareMediaTypePost,
    ShareMediaTypeLive
};

typedef NS_ENUM(NSUInteger, UserDialogAction) {
    UserDialogActionFollow,
    UserDialogActionResponse,
    UserDialogActionUnfollow,
    UserDialogActionReport,
    UserDialogActionBlock,
    UserDialogActionUnblock,
    UserDialogActionCancelFollowRequest
};

typedef NS_ENUM(NSUInteger, LoginAction) {
    LoginActionSignUp,
    LoginActionSignIn,
    LoginActionScoialSignIn
};

@class LiveStreamObject;
@class ShareManager;

@interface DialogManager : NSObject <UIPickerViewDataSource,UIPickerViewDelegate,IAPManagerDelegate>

@property (nonatomic, strong) ShareManager* shareManager;
@property (nonatomic, strong) UIView* dialogBgView;
@property (nonatomic, strong) UIImageView* popImageView;

// Loading View
@property (nonatomic, strong) UIImageView* loadingBgView;
@property (nonatomic, strong) UIImageView* loadingView;
@property (nonatomic,strong) UIDynamicAnimator* animator;
// Pickers
@property (nonatomic) BOOL onlyOneTap;
@property (nonatomic, strong) UIPickerView* pickerView;
@property (nonatomic,strong) UISnapBehavior* snap;
@property (nonatomic,strong) UIImageView* shareLiveView;
@property int pickerMode;
@property (nonatomic, strong) NSArray* pickerItems;
@property (nonatomic, strong) NSArray* pickerRightItems;
@property (copy)void (^itemPickerCallback)(BOOL done, int selectedIndex);
@property (copy)void (^rangePickerCallback)(BOOL done, int selectedLowIndex, int selectedHighIndex);
@property (nonatomic,strong) IAPManager* iapManager;
@property float shareBoxHeight;
@property (nonatomic,strong)UILabel* nowPointLabel;

+ (id)sharedInstance;

-(void) showRangePickerDialogWithItems:(NSArray *)items rightItem:(NSArray*)rightItem lowOffset:(int)lowOffset highOffset:(int)highOffset withCompletion:(void (^)(BOOL done, int selectedLowIndex, int selectedHighIndex)) callback;

-(void) hidePickerDialog;

-(UIView*) getWindowView;
-(UIView*) getStatusBarWindowView;

-(void) showActionSheetDialogTitle:(NSString*) title options:(NSArray*) options destructiveIndexes:(NSArray*) destructiveIndexes cancelable:(BOOL) cancelable withCompletion: (void (^)(int selectedOption)) callback;
-(void) showActionSheetDialogTitle:(NSString*) title options:(NSArray*) options destructiveIndexes:(NSArray*) destructiveIndexes adminIndexes:(NSArray*)adminIndexes cancelable:(BOOL) cancelable withCompletion: (void (^)(int selectedOption)) callback;
-(void) showActionSheetDialogTitleV2:(NSString*) title options:(NSArray*) options destructiveIndexes:(NSArray*) destructiveIndexes adminIndexes:(NSArray*)adminIndexes cancelable:(BOOL) cancelable withCompletion: (void (^)(int selectedOption)) callback;

-(void) showActionSheetOKDialogTitle:(NSString*)title message:(NSString*)message buttonText:(NSString*)buttonText cancelable:(BOOL)cancelable withCompletion: (void (^)(BOOL okClicked)) callback;
-(void) showActionSheetUserDialog:(UserObject*)user isPublisher:(BOOL)isPublisher isLivestreamer:(BOOL)isLivestreamer  withCompletion: (void (^)(UserDialogAction selectedAction)) callback;
-(void) showActionSheetUserDialogcommentver:(UserObject*)user isPublisher:(BOOL)isPublisher isLivestreamer:(BOOL)isLivestreamer  withCompletion: (void (^)(NSString* selectedAction)) callback;
-(void) showActionSheetLoginDialogWithCompletion:(void (^)(LoginAction loginAction))callback;
-(void) showRegisterActionSheetWithCompletion:(void (^)(NSString* action))callback;

//-(void) showActionKickConfirm:(int)seat withCompletion: (void (^)(BOOL Confirm))callback;

-(void) showActionSheetLivestreamDetail:(LiveStreamObject*)livestream  withCompletion: (void (^)(NSString* selectedAction))callback;

-(void) showActionSheetLiveRemind:(NSString*)openID showedText:(NSString*)showedText  withCompletion: (void (^)(BOOL onClick))callback;

-(void) showHeartFromPoint:(CGPoint) startPoint;
-(void) showSameHeartFromPoint:(CGPoint) startPoint selectedColor:(int)color;
-(void) showEmitterHeartFromPoint:(CGPoint) startPoint selectedColor:(int)color;
-(void) showSingleEmitterHeartFromPoint:(CGPoint) startPoint selectedColor:(int)color;

-(void) showPickerDialogWithItems:(NSArray *)items offset:(int)offset withCompletion:(void (^)(BOOL done, int selectedIndex)) callback;
-(void) showActionSheetCamera:(int)tag withCompletion:(void (^)(NSString* selectedAction))callback;
-(void) showLoadingView;
-(void) hideLoadingView;
-(void) showCompleteToast;
-(void) showHappyToastWithMessage:(NSString*) message;
-(void) showSadToastWithMessage:(NSString*) message;
-(void) showNetworkFailToast;
-(void) showProductList:(NSArray*)products remainingWarning:(BOOL)remainingWarning   withCompletion:(void (^)(int selectAction))callback;
-(void) showGiftConfirm:(GiftObject*)products withCompletion:(void (^)(BOOL okclick))callback;

-(void) showPopOverViewFromPoint:(CGPoint)startPoint withMessage:(NSString*)message;
-(void) showPopOverViewFromPoint:(CGPoint)startPoint withMessage:(NSString*)message withTextColor:(UIColor*)color;
-(void) removePopImmediately;
-(void) showActionSheetShareLink:(ShareMediaType)type withObj:(id)obj hasRestream:(BOOL)hasRestream withCompletion:(void (^)(NSString* action))callback;
-(void) removeDialogImmediately;
-(void) showLoadingViewQicker;
@end
