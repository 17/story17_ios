//
//  ProductObject.h
//  Story17
//
//  Created by POPO on 11/17/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductObject : NSObject

@property (nonatomic) int32_t sequence;
@property (nonatomic, strong) NSString* productID;
@property (nonatomic) float price;
@property (nonatomic) int32_t point;
@property (nonatomic) int32_t bonusAmount;
@property (nonatomic) int32_t bonusPercentage;
@property (nonatomic, strong) NSString* name;
@property (nonatomic,strong)NSString* localePrice;
@property (nonatomic, strong) NSString* priceCountryCode;

+ (ProductObject*)getProductWithDict:(NSDictionary *)productInfoDict;

@end
