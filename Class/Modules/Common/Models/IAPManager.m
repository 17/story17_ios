//
//  IAPManager.m
//  Story17
//
//  Created by POPO on 11/11/15.
//  Copyright © 2015 POPO_INNOVATION. All rights reserved.
//

#import "IAPManager.h"
#import "Constant.h"

@interface IAPManager()

@property UIButton* purchaseButton;
@property SKProductsRequest* request;
@property NSArray* products;

@end

@implementation IAPManager

- (id)init
{
    self = [super init];
    
    if (self) {
        if (![SKPaymentQueue canMakePayments])
            return nil;
        _productArray=CREATE_MUTABLE_ARRAY;
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        _remainingWarning=NO;
        _showUI=NO;
        _buying=NO;
        [self validateProductIdentifiers];
    }
    
    return self;
}

- (void)validateProductIdentifiers
{
    /* Get Product Info */
    [API_MANAGER getPointProductListWithCompletion:^(BOOL success, NSArray *productObjs) {
        if (success) {
            [_productArray removeAllObjects];
            /* Validate Product ID */
            NSMutableArray *productIdentifiers = CREATE_MUTABLE_ARRAY;
            for (ProductObject *product in productObjs) {
                [productIdentifiers addObject:product.productID];
                [_productArray addObject:product];
            }
            
            SKProductsRequest *productsRequest = [[SKProductsRequest alloc]
                                                  initWithProductIdentifiers:[NSSet setWithArray:productIdentifiers]];
            
            // Keep a strong reference to the request.
            self.request = productsRequest;
            productsRequest.delegate = self;
            [productsRequest start];
        }else{
            [DIALOG_MANAGER showNetworkFailToast];
        }
        
    }];
}

// SKProductsRequestDelegate protocol method
- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response
{
    
    self.products = response.products;
    if (_showUI) {
        [self showProductList];
    }
//    for (NSString *invalidIdentifier in response.invalidProductIdentifiers) {
//        
//        DLog(@"!!! Invalid product ID: %@", invalidIdentifier);
//    }
}

-(void)getProductList
{
    _showUI=YES;
    [self validateProductIdentifiers];
}

- (void)displayStoreUI
{
    NSMutableArray *titles = CREATE_MUTABLE_ARRAY;
//    [titles addObject:@"獲得免費點數"];
    for (int i=0; i<[_products count]; i++) {
        SKProduct *product = _products[i];
        ProductObject* a=[[ProductObject alloc]init];
        a=[_productArray objectAtIndex:i];
        a.price=[product.price floatValue];
        a.name=product.localizedTitle;
        
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [numberFormatter setLocale:product.priceLocale];
        NSString *formattedPrice = [numberFormatter stringFromNumber:product.price];
        
        [titles addObject:[NSString stringWithFormat:@"%@ %@", product.localizedTitle, formattedPrice]];
    }
    
    
    [DIALOG_MANAGER showActionSheetDialogTitle:[NSString stringWithFormat:@"%@：%d",LOCALIZE(@"now_point"), 0] options:titles destructiveIndexes:@[@"1"] cancelable:YES withCompletion:^(int selectedOption) {
        if (selectedOption > 0) {
            SKPayment * payment = [SKPayment paymentWithProduct:_products[selectedOption-1]];
            [[SKPaymentQueue defaultQueue] addPayment:payment];
        }
    }];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                DLog(@"transactionIdentifier = %@", transaction.transactionIdentifier);
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction]; 
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
            case SKPaymentTransactionStatePurchasing:
                DLog(@"SKPaymentTransactionStatePurchasing");
                break;
            default: break;
        }
    }
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
//    NSString * productIdentifier = transaction.payment.productIdentifier;
    
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSString *receipt = [[NSData dataWithContentsOfURL:receiptURL] base64EncodedStringWithOptions:0];
//    NSString *receiptFromURL = [receiptData base64EncodedString];
    
    if (!receipt) {
        _buying=NO;

        UIAlertView *av = [[UIAlertView alloc] initWithTitle:LOCALIZE(@"Notice") message:LOCALIZE(@"buy_point_error") delegate:nil cancelButtonTitle:LOCALIZE(@"ok") otherButtonTitles:nil, nil];
        [av show];
        [[SKPaymentQueue defaultQueue] finishTransaction: transaction];

    } else {
        [API_MANAGER getPurchaseCountWithCompletion:^(BOOL success, int32_t count) {
            if (success) {
    
                if ([_productArray count]==0) {
                    return ;
                }
                [API_MANAGER purchaseProductWithReceipt:receipt andPriceCountryCode:[[_productArray objectAtIndex:0] priceCountryCode] withCompletion:^(BOOL success, int32_t purchasedPoint, int32_t errorCode) {
                    _buying=NO;
                    [DIALOG_MANAGER hideLoadingView];
                    if (success) {
                        if (count == 0) {
                            // log first purchase
                            [EVENT_HANDLER addEventTracking:@"FirstPurchasePoint" withDict:@{@"point":INT_TO_NUMBER(purchasedPoint)}];
                        }
                        DLog(@"%@",[NSNumber numberWithInt:purchasedPoint+[GET_DEFAULT(MY_POINT) intValue]]);
                        if (GET_DEFAULT(IS_ON_LIVESTREAMING)) {
                            [EVENT_HANDLER addEventTracking:@"DidBuyPointInLive" withDict:@{@"point":INT_TO_STRING(purchasedPoint)}];
                        }
                        [EVENT_HANDLER addEventTracking:@"PurchasePoint" withDict:@{@"point":INT_TO_STRING(purchasedPoint),@"BuyTimes":INT_TO_STRING(count)}];
                        // Remove the transaction from the payment queue.
                        [DEFAULTS setObject:[NSNumber numberWithInt:purchasedPoint+[GET_DEFAULT(MY_POINT) intValue]] forKey:MY_POINT];
                        [DEFAULTS synchronize];
                        if (_delegate!=nil) {
                            [_delegate pointRefresh];
                        }
                        if (_delegate1!=nil) {
                            [_delegate1 pointRefresh];
                        }
                        [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
                        
                    } else {
                        if (errorCode==1) {
                            [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
                        }else{
                            UIAlertView *av = [[UIAlertView alloc] initWithTitle:LOCALIZE(@"Notice") message:LOCALIZE(@"buy_point_error") delegate:nil cancelButtonTitle:LOCALIZE(@"ok") otherButtonTitles:nil, nil];
                            [av show];
                        }
                    }
                }];
            } else {
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:LOCALIZE(@"Notice") message:LOCALIZE(@"buy_point_error") delegate:nil cancelButtonTitle:LOCALIZE(@"ok") otherButtonTitles:nil, nil];
                [av show];
            }
        }];
    }
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction
{
    if(transaction.error.code != SKErrorPaymentCancelled) {
        DLog(@"SKError: %@", transaction.error);
//        [DIALOG_MANAGER showNetworkFailToast];
        _buying=NO;
        [DIALOG_MANAGER hideLoadingView];
 
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:LOCALIZE(@"Notice") message:LOCALIZE(@"buy_point_error") delegate:nil cancelButtonTitle:LOCALIZE(@"ok") otherButtonTitles:nil, nil];
        [av show];
    } else {
        DLog(@"Payment cancelled by user");
        _buying=NO;
        if (GET_DEFAULT(IS_ON_LIVESTREAMING)) {
            [EVENT_HANDLER addEventTracking:@"GiveupBuyPointInLive" withDict:@{@"point":transaction.payment.productIdentifier}];
        }
        [DIALOG_MANAGER hideLoadingView];
    }
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    _buying=NO;

}

-(void)showProductList
{
    NSMutableArray* productList=[[NSMutableArray alloc]init];
    NSMutableArray* appleProductList=[[NSMutableArray alloc]init];

    if ([_products count]==0) {
        return;
    }
    //    [titles addObject:@"獲得免費點數"];
    for (int i=0; i<[_productArray count]; i++) {
        ProductObject* a=[[ProductObject alloc]init];
        a=[_productArray objectAtIndex:i];
        int appleIndex=0;
        for (int index=0; index<[_products count]; index++) {
            SKProduct *product = _products[index];

            if ([a.productID isEqualToString:product.productIdentifier]) {
                appleIndex=index;
                break;
            }
        }
        
        SKProduct *product = _products[appleIndex];
        [appleProductList addObject:product];
        a.price = [product.price floatValue];
        a.name = product.localizedTitle;
        a.priceCountryCode = [product.priceLocale objectForKey: NSLocaleCountryCode];
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [numberFormatter setLocale:product.priceLocale];
        NSString *formattedPrice = [numberFormatter stringFromNumber:product.price];
        a.localePrice = formattedPrice;
        
        [productList addObject:a];
    }

    [DIALOG_MANAGER hideLoadingView];
    [DIALOG_MANAGER showProductList:productList remainingWarning:_remainingWarning withCompletion:^(int selectAction)  {
        if (selectAction>=0&&_buying==NO) {
            [DIALOG_MANAGER showLoadingView];
            _buying=YES;
                SKPayment * payment = [SKPayment paymentWithProduct:appleProductList[selectAction]];
                [[SKPaymentQueue defaultQueue] addPayment:payment];
        

        }
    } ];
        
    _showUI=NO;

}

-(void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    
}

-(void)requestDidFinish:(SKRequest *)request
{
    
}

@end
