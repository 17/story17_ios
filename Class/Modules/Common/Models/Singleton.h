#import "Constant.h"
#import "ApiManager.h"
#import "UserObject.h"
#import "RevenueDataManager.h"
#import "ConnectionManager.h"
#import "CustomTabbarController.h"
#import "STTweetLabel.h"
#import "IAPManager.h"


//#import "LiveStreamViewController.h"

@class LiveStreamViewController;

@class ApiManager;

@interface Singleton : NSObject

@property (nonatomic, strong) ApiManager* apiManager;
@property (nonatomic, strong) RevenueDataManager* revenueDataManager;
@property (nonatomic, strong) ConnectionManager* connectionManager;

@property (nonatomic, strong) NSUserDefaults* defaults;
@property (nonatomic, strong) CustomTabbarController* mainTabBarViewController;
@property (nonatomic, strong) NSString* cdnBaseUrl;
@property (nonatomic, strong) NSString* chunkcdnBaseUrl;

// Time Formatters
@property (nonatomic, strong) NSDateFormatter* minihourMinutedateFormatter;
@property (nonatomic, strong) NSDateFormatter* hourMinutedateFormatter;
@property (nonatomic, strong) NSDateFormatter* dayOfWeekdateFormatter;
@property (nonatomic, strong) NSDateFormatter* dayOfMonthWithweekdateFormatter;
@property (nonatomic, strong) NSDateFormatter* dayofYearFormatter;

@property (nonatomic, strong) NSCache* imageCache;
@property (nonatomic, strong) NSString* currentChatingUserID;

@property (nonatomic, strong) NSMutableArray* countryData;
//@property (nonatomic, strong) NSDictionary* dictCodes;
@property (nonatomic, strong) NSArray* ageArray;
@property (nonatomic, strong) NSMutableArray* tagArray;
@property (nonatomic, strong) UIImageView* badgeImageView;

@property (nonatomic, strong) NSDictionary* country2File;
@property (nonatomic, strong) NSDictionary* countryCode2PhoneCode;

@property (nonatomic, strong) NSDictionary* currenctRateDict;

@property (nonatomic, strong) UIImage* heartImage;

-(NSString*) getCountryImageFileName:(NSString*)name;

+ (id)sharedInstance;
-(void) setupMainTabBarViewController;
-(void)synchronizeTokenToServer;
-(NSString*) getUUID;
-(NSString*) generateRandomFileNameWithExtension:(NSString*) fileExtension;
- (void)saveImageWithWhiteBackgroundWithoutCompleteMessage:(UIImage*)image;
-(UIImage*) getCompressedImage:(UIImage*) originalImage withTargetSize:(float) targetSize;
-(void) saveImageWithThumbnailImage:(UIImage *)sourceImage andFileName:filename;
- (UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
-(NSString *)timestampToMessageTimeString:(int) timestamp;
-(NSString *)getDateAndDayForTimestamp:(int) timestamp;
-(NSString *)getDayAndTimeForTimestamp:(int) timestamp;
-(NSString *)timestampToChatThreadTimeString:(int)timestamp;
-(void)playShortSound:(NSString*)filePath;
-(void)vibrate;
- (UIImage *)rotateImage:(UIImage*)image byDegree:(CGFloat)degrees;
//-(BOOL)isVIP;
//-(BOOL)hasVipPrivilege;
-(void)reloadAllBadge;
-(void) sizeToFitHeight:(UILabel*) label maxWidth:(float) maxWidth;
//-(NSString *)getVipExpDateString:(int)timestamp;
-(UIImage*) loadCachedImage: (NSString*) imageFileName withMethod:(int) method;
-(UIImage*) loadCachedImage: (NSString*) imageFileName withEdgeInset:(UIEdgeInsets) edgeInsets withMethod:(int) method;
-(void)generateUDIDToKeychain;
-(void)saveFreezeFile;
-(void) goToAppStore;
-(BOOL) hasRated;
-(void) rateComplete;
-(void) clearUserDefaults;
-(void) initializeDefaults;
-(NSString*) getElaspsedTimeString:(int) timestamp;

-(UserObject*) getSelfUserObject;

-(NSString*) dateToString:(NSDate*)date;
-(void) customizeInterface;

-(UIColor*)getRandomColor;
-(NSString*)getCountNumNormalization:(int)count;

-(void)mainTabbarSetSelectedIndex:(int)selectedIndex;
-(void)mainTabbarRelease;

-(NSArray*)getAgeRangeItem;
-(NSArray*)getTagedArray:(NSString*)inputStr;
-(void)gotoSystemSettings;
-(NSString*)getTimeFormatBySecond:(int)second;
-(int)getTimezone;
-(void)reloadFollowStatus;

/* Encription */
-(NSData*) aesEncryptString:(NSString*)plaintext withKey:(NSString*)key;
-(NSString*) aesDecryptData:(NSData*)cipherdata withKey:(NSString*)key;
-(NSString*) rsaEncoding:(NSString*)inputStr;
-(NSString*) rsaDecoding:(NSString*)inputStr;

/* Video Status */
-(void)startVideoAVPlayerForCollectionView:(UICollectionView*)collectionView;
-(void)stopAVPlayerForCollectionView:(UICollectionView*)collectionView;
-(BOOL)shouldOpenLivestream;

-(int)hexString2ColorCode:(NSString*)hexString;
-(BOOL)isUsingFastGPU;
-(BOOL)isSpecialMode;

-(BOOL)shouldPlaySoundForNotificationType:(NSString*)notifType;
-(void)detectIpGeoLocation:(void (^)(BOOL success, NSDictionary* jsonDict)) callback;
-(void)recuriveGetIpGeoLocation;

-(NSString*)getCurrencyType;
-(float)getCurrencyRate;
-(BOOL) NSStringIsValidEmail:(NSString *)checkString;
-(NSString*)modelToJsonString:(id)arrayOrDict;
-(NSString*)queryDNS;
-(LiveStreamViewController*)liveStreamSharedManager;
-(STTweetLabel*)stTitleLabel;
-(STTweetLabel*)msgTextLabel;
-(IAPManager*)iapManager;
-(void)removeCacheInfo;
-(BOOL)openIDAvaliable:(NSString*)openID;


@end
