#import "FacebookManager.h"

@interface FacebookManager()
{
    struct {
        unsigned int didLoginComplete : 1;
    } _delegateFlags;
}
@end

@implementation FacebookManager

+ (FacebookManager*)sharedInstance
{
    static dispatch_once_t once;
    
    static FacebookManager *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        _login = [[FBSDKLoginManager alloc] init];
        _login.loginBehavior = FBSDKLoginBehaviorNative;
        [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    }
    return self;
}

- (void)loginAction:(UIViewController*)viewController
{
    if([self isLoginned]) {
        if (_delegateFlags.didLoginComplete) {
            [_delegate facebookManager:self didLoginComplete:YES];
        }        return;
    }
 
    [_login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"]
        fromViewController:viewController
        handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error) {
                if (_delegateFlags.didLoginComplete) {
                    [_delegate facebookManager:self didLoginComplete:NO];
                }
            }
            else if (result.isCancelled) {
                
            }
            else {
                if (_delegateFlags.didLoginComplete) {
                    [_delegate facebookManager:self didLoginComplete:YES];
                }
            }
    }];
}

- (void)logoutAction
{
    [_login logOut];
}

- (BOOL)isLoginned
{
    if ([FBSDKAccessToken currentAccessToken]) {
        return YES;
    }
    return NO;
}

#pragma mark - Override

- (void)setDelegate:(id<FacebookManagerDelegate>)delegate
{
    _delegate = delegate;
    
    _delegateFlags.didLoginComplete = [delegate respondsToSelector:@selector(facebookManager:didLoginComplete:)];
}

@end
