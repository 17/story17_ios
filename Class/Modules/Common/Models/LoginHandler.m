//
//  LoginHandler.m
//  Story17
//
//  Created by Racing on 2016/1/5.
//  Copyright © 2016年 POPO_INNOVATION. All rights reserved.
//

#import "LoginHandler.h"
#import "DialogManager.h"
#import "Constant.h"
#import "LoginV4ViewController.h"
#import "RegisterV4ViewController.h"

@implementation LoginHandler


+ (LoginHandler*)sharedInstance
{
    static dispatch_once_t once;
    
    static LoginHandler *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        _tencentOAuth =  [[TencentOAuth alloc] initWithAppId:QQ_APP_ID andDelegate:self];
    }
    return self;
}



+ (BOOL)isGuest
{
    return (!MY_USER_ID || [MY_USER_ID isEqualToString:@""]);
}

+ (void)handleUserLogin
{
    [DIALOG_MANAGER showActionSheetLoginDialogWithCompletion:^(LoginAction loginAction) {
        switch (loginAction) {
            case LoginActionScoialSignIn:
            {
                [NOTIFICATION_CENTER postNotificationName:SocialLoginNotification object:nil];
            }
                break;
                
            case LoginActionSignIn:
            {
                [NOTIFICATION_CENTER postNotificationName:LoginNotification object:nil];

            }
                break;
            case LoginActionSignUp:
            {
                [NOTIFICATION_CENTER postNotificationName:SignUpNotification object:nil];
            }
                break;
        }

    }];
}


- (void) loginWithType:(loginMode)mode withViewController:(UIViewController*)vc
{
    if( mode==qqLoginMode){

        [_tencentOAuth authorize:[NSArray arrayWithObjects:@"get_user_info", @"get_simple_userinfo", @"add_t", nil] inSafari:NO];

    }else if(mode==wechatLoginMode){
        
        NSString *scope = @"snsapi_userinfo";
        NSString *state = @"xxx";
        
        SendAuthReq* req = [[SendAuthReq alloc] init];
        req.scope = scope;
        req.state = state;
        
        [WXApiManager sharedManager].delegate = self;
        [WXApi sendAuthReq:req
            viewController:vc
                  delegate:[WXApiManager sharedManager]];
        
    }else if(mode==weiboLoginMode){
        
        WBAuthorizeRequest *request = [WBAuthorizeRequest request];
        request.redirectURI = WEIBO_REDIRECT_URL;
        request.scope = @"all";
        request.userInfo = @{@"SSO_From": @"SendMessageToWeiboViewController",
                             @"Other_Info_1": [NSNumber numberWithInt:123],
                             @"Other_Info_2": @[@"obj1", @"obj2"],
                             @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
        [WeiboSDK sendRequest:request];
    }
}


# pragma mark - TencentSessionDelegate
- (void)getUserInfoResponse:(APIResponse*) response
{
    // register process
//    DLog(@"nickname:%@",response.jsonResponse[@"nickname"]);
//    DLog(@"figureurl_qq_2:%@",response.jsonResponse[@"figureurl_qq_2"]);
//    DLog(@"response:%@",response.jsonResponse);
    
    NSString* name = response.jsonResponse[@"nickname"];
    NSString* pictureUrl = response.jsonResponse[@"figureurl_qq_2"];
    NSString* gender = response.jsonResponse[@"gender"];
    
    
    if([SINGLETON openIDAvaliable:name]){
        [DEFAULTS setObject:name forKey:USER_OPEN_ID];
    }
    
    [DEFAULTS setObject:name forKey:USER_NAME];
    [DEFAULTS setObject:[_tencentOAuth openId] forKey:QQ_ID];
    
    if([gender isEqualToString:@"男"]){
        [DEFAULTS setObject:@"male" forKey:GENDER];
    }else if([gender isEqualToString:@"女"]){
        [DEFAULTS setObject:@"female" forKey:GENDER];
    }
    
    NSString* profileImageFileName = [SINGLETON generateRandomFileNameWithExtension:@"png"];
    [DEFAULTS setObject:profileImageFileName forKey:CACHE_PICTURE];
    [DEFAULTS synchronize];

    
    [_delegate requestUserInfo:YES loginMode:qqLoginMode];

    // get profile photo
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"myQQPic.png"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    AFHTTPRequestOperation *op = [manager GET:pictureUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [SINGLETON saveImageWithThumbnailImage:[UIImage imageWithContentsOfFile:path] andFileName:profileImageFileName];
        self.multiFileUploader = [[MultiFileUploader alloc] initWithFileNameArray:@[profileImageFileName,[NSString stringWithFormat:@"THUMBNAIL_%@",profileImageFileName]]];
        
        [self.multiFileUploader startUploadWithCallback:^(NSArray *fileNameArray, int code, float progress) {
            if(code==MULTI_UPLOAD_SUCCESS) {
                DLog(@"upload success");

            } else if(code==MULTI_UPLOAD_FAIL) {
                DLog(@"upload fail");

            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

        DELETE_FILE(GET_LOCAL_FILE_PATH(path));

    }];
    
    op.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    [op start];
}

-(void)tencentDidLogin
{
    
    if (_tencentOAuth.accessToken && 0 != [_tencentOAuth.accessToken length])
    {
        
        DLog(@"登录完成:%@ %@",_tencentOAuth.accessToken,[_tencentOAuth openId]);
        NSString* accessToken = _tencentOAuth.accessToken;
        NSString* qqID = [_tencentOAuth openId];
        [DEFAULTS setObject:qqID forKey:QQ_ID];
        [DEFAULTS setObject:accessToken forKey:QQ_TOKEN];
        [DEFAULTS synchronize];
        
        [_delegate loginSuccess:YES loginMode:qqLoginMode userid:qqID token:accessToken];
        
    }
    else
    {
        [_delegate loginSuccess:NO loginMode:qqLoginMode userid:@"" token:@""];

        DLog(@"登录不成功 没有获取accesstoken");
    }
}

-(void)getUserInfo:(loginMode)mode
{
    if(mode==qqLoginMode){
        
        [_tencentOAuth getUserInfo];

    }else if(mode==wechatLoginMode){
    
        
        NSString* wechatID = GET_DEFAULT(WECHAT_USER_ID);
        NSString* accessToken = GET_DEFAULT(WECHAT_TOKEN);

//        DLog(@"request:%@",[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",accessToken,wechatID]);

        NSMutableURLRequest *userInfoRequest = [[NSMutableURLRequest alloc] init];
        [userInfoRequest setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",accessToken,wechatID]]];
        [userInfoRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [userInfoRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [userInfoRequest setHTTPMethod:@"GET"];
        
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [[session dataTaskWithRequest:userInfoRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(error){
                
            }else{
                NSString *requestReply2 = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                NSString* trimString = [requestReply2 stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
                NSDictionary *deserializedData2 = [trimString mj_JSONObject];
                NSString* picture = deserializedData2[@"headimgurl"];
                NSString* profileImageFileName = [SINGLETON generateRandomFileNameWithExtension:@"png"];
                NSString* name = deserializedData2[@"nickname"] ;
                name = [NSString stringWithCString:[name cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
                
                NSString* gender = @"female";
                if([deserializedData2[@"sex"] intValue]==1){
                    gender=@"male";
                }
                
                
                // wechat Register
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"myFacebookPic.png"];
                
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                
                AFHTTPRequestOperation *op = [manager GET:picture parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    //                        NSString* profileImageFileName = [SINGLETON generateRandomFileNameWithExtension:@"png"];
                    [SINGLETON saveImageWithThumbnailImage:[UIImage imageWithContentsOfFile:path] andFileName:profileImageFileName];
                    self.multiFileUploader = [[MultiFileUploader alloc] initWithFileNameArray:@[profileImageFileName, [NSString stringWithFormat:@"THUMBNAIL_%@", profileImageFileName]]];
                    
                    [self.multiFileUploader startUploadWithCallback:^(NSArray *fileNameArray, int code, float progress) {

//                        DLog(@"upload exc");

                        if(code==MULTI_UPLOAD_SUCCESS) {
                            
                            DLog(@"upload success");
                            
                        } else if(code==MULTI_UPLOAD_FAIL) {

                            DLog(@"upload fail");

                        }
                    }];
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    DELETE_FILE(GET_LOCAL_FILE_PATH(path));
                }];
                
                op.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
                [op start];
                
                if([SINGLETON openIDAvaliable:name]){
                    [DEFAULTS setObject:name forKey:USER_OPEN_ID];
                }
                
                
                DLog(@"profileImageFileName:%@",profileImageFileName);

                [DEFAULTS setObject:name forKey:USER_NAME];
                [DEFAULTS setObject:gender forKey:GENDER];
                [DEFAULTS setObject:profileImageFileName forKey:CACHE_PICTURE];
                [DEFAULTS setObject:wechatID forKey:WECHAT_USER_ID];
                [DEFAULTS setObject:accessToken forKey:WECHAT_TOKEN];
                [DEFAULTS synchronize];
                
                [_delegate requestUserInfo:YES loginMode:wechatLoginMode];
                
            }
        }] resume];
        
    }else if(mode==weiboLoginMode){
    
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://api.weibo.com/2/users/show.json?access_token=%@&uid=%@",GET_DEFAULT(WEIBO_ACCESSTOKEN),GET_DEFAULT(WEIBO_USER_ID)]]];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setHTTPMethod:@"GET"];
        
        
            DLog(@"%@",[NSString stringWithFormat:@"https://api.weibo.com/2/users/show.json?access_token=%@&uid=%@",GET_DEFAULT(WEIBO_ACCESSTOKEN),GET_DEFAULT(WEIBO_USER_ID)]);
        
            NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
                if(error){
                    return;
                }
        
                NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                NSDictionary *deserializedData = [requestReply mj_JSONObject];
        
                NSString* name = deserializedData[@"name"];
                NSString* picture = deserializedData[@"profile_image_url"];
                NSString* weiboID = deserializedData[@"id"];
                NSString* profileImageFileName = [SINGLETON generateRandomFileNameWithExtension:@"png"];
                NSString* gender = @"female";
                name = [NSString stringWithCString:[name cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
        
                if([deserializedData[@"gender"] isEqualToString:@"m"]){
                    gender = @"male";
                }
        
                DLog(@"weiboID %@",weiboID);
                DLog(@"gender %@",gender);
                DLog(@"name %@",name);
                DLog(@"picture %@",picture);
                
                // get profile photo
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"myWeibo.png"];
                
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            
                AFHTTPRequestOperation *op = [manager GET:picture parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    [SINGLETON saveImageWithThumbnailImage:[UIImage imageWithContentsOfFile:path] andFileName:profileImageFileName];
                    
                    self.multiFileUploader = [[MultiFileUploader alloc] initWithFileNameArray:@[profileImageFileName, [NSString stringWithFormat:@"THUMBNAIL_%@", profileImageFileName]]];
                    
                    [self.multiFileUploader startUploadWithCallback:^(NSArray *fileNameArray, int code, float progress) {
                        if(code==MULTI_UPLOAD_SUCCESS) {

                            DLog(@"upload success");

                        } else if(code==MULTI_UPLOAD_FAIL) {
                        
                            DLog(@"upload fail");

                        }
                    }];
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    DELETE_FILE(GET_LOCAL_FILE_PATH(path));
                }];
                
                op.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
                [op start];
                
                if([SINGLETON openIDAvaliable:name]){
                    [DEFAULTS setObject:name forKey:USER_OPEN_ID];
                }
                
                DLog(@"profileImageFileName:%@",profileImageFileName);
                
                [DEFAULTS setObject:name forKey:USER_NAME];
                [DEFAULTS setObject:gender forKey:GENDER];
                [DEFAULTS setObject:profileImageFileName forKey:CACHE_PICTURE];
                [DEFAULTS setObject:weiboID forKey:WEIBO_USER_ID];
                [DEFAULTS synchronize];

                [_delegate requestUserInfo:YES loginMode:weiboLoginMode];

            }] resume];
    }
    
}

-(void)tencentDidNotLogin:(BOOL)cancelled
{
    if (cancelled)
    {
        DLog(@"用户取消登录");
    }
    else
    {
        DLog(@"登录失败");
    }
}

-(void)tencentDidNotNetWork
{
    DLog(@"无网络连接，请设置网络");
    [DIALOG_MANAGER showNetworkFailToast];
}


# pragma WBHttpRequestDelegate
- (void)request:(WBHttpRequest *)request didFinishLoadingWithResult:(NSString *)result
{
    NSString *title = nil;
    UIAlertView *alert = nil;
    
    title = NSLocalizedString(@"收到网络回调", nil);
    alert = [[UIAlertView alloc] initWithTitle:title
                                       message:[NSString stringWithFormat:@"%@",result]
                                      delegate:nil
                             cancelButtonTitle:NSLocalizedString(@"确定", nil)
                             otherButtonTitles:nil];
    [alert show];
}

- (void)request:(WBHttpRequest *)request didFailWithError:(NSError *)error;
{
    NSString *title = nil;
    UIAlertView *alert = nil;
    
    title = NSLocalizedString(@"请求异常", nil);
    alert = [[UIAlertView alloc] initWithTitle:title
                                       message:[NSString stringWithFormat:@"%@",error]
                                      delegate:nil
                             cancelButtonTitle:NSLocalizedString(@"确定", nil)
                             otherButtonTitles:nil];
    [alert show];
}

#pragma mark - WXApiManagerDelegate
- (void)managerDidRecvAuthResponse:(SendAuthResp *)response {
    
    if(response.errCode==0){
        // Auth Success
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code",WECHAT_APP_ID,WECHAT_APP_KEY,response.code]]];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setHTTPMethod:@"GET"];
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(error){
                
            }else{
                
                NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                NSDictionary *deserializedData = [requestReply mj_JSONObject];
                
                NSString* accessToken = deserializedData[@"access_token"];
                NSString* wechatID = deserializedData[@"openid"];
                
                [DEFAULTS setObject:wechatID forKey:WECHAT_USER_ID];
                [DEFAULTS setObject:accessToken forKey:WECHAT_TOKEN];
                [DEFAULTS synchronize];
                
                [_delegate loginSuccess:YES loginMode:wechatLoginMode userid:wechatID token:accessToken];

                
            }
        }] resume];
        
    }
}

#pragma mark - Weibo delegate
- (void)didReceiveWeiboRequest:(WBBaseRequest *)request
{
}

- (void)didReceiveWeiboResponse:(WBBaseResponse *)response
{
    NSString* weiboToken = @"";
    NSString* weiboID = @"";
    
    if ([response isKindOfClass:WBSendMessageToWeiboResponse.class])
    {
        NSString *title = NSLocalizedString(@"发送结果", nil);
        NSString *message = [NSString stringWithFormat:@"%@: %d\n%@: %@\n%@: %@", NSLocalizedString(@"响应状态", nil), (int)response.statusCode, NSLocalizedString(@"响应UserInfo数据", nil), response.userInfo, NSLocalizedString(@"原请求UserInfo数据", nil),response.requestUserInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"确定", nil)
                                              otherButtonTitles:nil];
        WBSendMessageToWeiboResponse* sendMessageToWeiboResponse = (WBSendMessageToWeiboResponse*)response;
        NSString* accessToken = [sendMessageToWeiboResponse.authResponse accessToken];
        if (accessToken)
        {
            weiboToken = accessToken;
        }
        NSString* userID = [sendMessageToWeiboResponse.authResponse userID];
        if (userID) {
            weiboID = userID;
        }
        [alert show];
    }
    else if ([response isKindOfClass:WBAuthorizeResponse.class])
    {
        
        weiboToken = [(WBAuthorizeResponse *)response accessToken];
        weiboID = [(WBAuthorizeResponse *)response userID];
     
        if(weiboToken==nil || weiboID==nil){
            return;
        }
        
        
        [DEFAULTS setObject:weiboID forKey:WEIBO_USER_ID];
        [DEFAULTS setObject:weiboToken forKey:WEIBO_ACCESSTOKEN];
        [DEFAULTS synchronize];
        
        [_delegate loginSuccess:YES loginMode:weiboLoginMode userid:weiboID token:weiboToken];
        
    }
}


@end
