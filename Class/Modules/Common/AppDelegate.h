#import "WeiboSDK.h"



@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property UIBackgroundTaskIdentifier bgTaskIdentifier;
@property int enterPageTimeStamp;

@end
