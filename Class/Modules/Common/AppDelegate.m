//#import "Constant.h"
#import "AppDelegate.h"
#import "UIAlertView+NSCookbook.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "FilterViewController.h"
#import "CameraViewController.h"
#import "PublishPostHandler.h"
#import "SinglePostViewController.h"
#import "UserProfileViewController.h"
#import "LiveStreamViewController.h"
#import "MyLocaitionManager.h"
#import "ConnectionViewController.h"
#import "LikesHandler.h"
#import "EventHandler.h"
#import "FacebookManager.h"
#import "LoginHandler.h"
#import "WXApiManager.h"

#import "LivestreamRevenueViewController.h"
#import "PublishPostViewController.h"
#import "UserListViewController.h"

#import "PhoneVerificationViewController.h"
#import "BlabViewController.h"

#import "PaymentHistoryViewController.h"
#import "LiveNationViewController.h"
#import <TwitterKit/TwitterKit.h>

#import "GuestViewController.h"
#import "RegisterV4ViewController.h"

#import "Branch.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import <HelpShift/Helpshift.h>

@interface WBBaseRequest ()
- (void)debugPrint;
@end

@interface WBBaseResponse ()
- (void)debugPrint;
@end

@implementation AppDelegate

@synthesize bgTaskIdentifier;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    /* Detect ip for region */
    [SINGLETON recuriveGetIpGeoLocation];
    
    
    NSString* upgrageUser = [[NSUserDefaults standardUserDefaults] objectForKey:@"UPDATE_FEED_TIME"];
    NSString* firstOpen = [[NSUserDefaults standardUserDefaults] objectForKey:@"APP_OPEN_TIMES"];
    //for old version detect upgrage
    if (upgrageUser != nil&&firstOpen==nil) {
        [DEFAULTS setObject:@0 forKey:UPDATE_FEED_TIME];
        [DEFAULTS setObject:@1 forKey:APP_OPEN_TIMES];
    }
    [[Partytrack sharedInstance] setDisplayWindow:self.window];
    [[Partytrack sharedInstance] startWithAppID:PARTYTRACK_APP_ID AndKey:PARTYTRACK_API_KEY AndOptions:launchOptions];
    
    [WeiboSDK enableDebugMode:YES];
    [WeiboSDK registerApp:WEIBO_APP_ID];
    
    _enterPageTimeStamp=CURRENT_TIMESTAMP;
    int times=[GET_DEFAULT(APP_OPEN_TIMES) intValue]+1;

    [DEFAULTS setObject:@1 forKey:IS_FIRST_HELPSHIFT];
    [DEFAULTS setObject:[SINGLETON getUUID] forKey:SESSION_ID];
    [DEFAULTS setObject:INT_TO_STRING(times) forKey:APP_OPEN_TIMES];
    [DEFAULTS synchronize];

    bgTaskIdentifier = UIBackgroundTaskInvalid;

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }

    
    [SINGLETON initializeDefaults];
    
    [Singleton sharedInstance];
    [PublishPostHandler sharedInstance];
    [LikesHandler sharedInstance];
    [EventHandler sharedInstance];
    
    [DatabaseManager sharedInstance];
    [MyLocaitionManager sharedInstance];
    [FacebookManager sharedInstance];
    
    [LoginHandler sharedInstance];
    
    [SINGLETON customizeInterface];
    [SINGLETON removeCacheInfo];

    [EVENT_HANDLER addEventTracking:@"OpenApp" withDict:nil];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window makeKeyAndVisible];
    self.window.backgroundColor = [UIColor whiteColor];

    Digits *digits = [Digits sharedInstance];
    [digits logOut];
    
    if([MY_USER_ID isEqualToString:@""]) {
      
//        IntroductionViewController *startvc = [IntroductionViewController new];
//        UINavigationController* startNav = [[UINavigationController alloc]initWithRootViewController:startvc];
//        [startNav setNavigationBarHidden:YES];
//        self.window.rootViewController = startNav;
        
        

        GuestViewController *vc = [[GuestViewController alloc] init];
        self.window.rootViewController = NAV(vc);

    }
    else {

        
        PhoneVerificationViewController *vc = [[PhoneVerificationViewController alloc] init];
        self.window.rootViewController = NAV(vc);
        [SINGLETON setupMainTabBarViewController];
        
    }
    
    NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in tmpDirectory) {
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), file] error:NULL];
    }
    
    [[Twitter sharedInstance] startWithConsumerKey:@"3mLuAr8kBXMNSBozYL8s6lmkR" consumerSecret:@"hai0C115QqBHpLeVRbOElf40YDNDBzJxaCt3TRM0467MbuY7wU"];
    [Fabric with:@[[Crashlytics class], [Digits class],[Twitter sharedInstance]]];
    
    [MobClick startWithAppkey:UMENG_KEY reportPolicy:BATCH channelId:@""];
    
    [Flurry setSessionReportsOnPauseEnabled:YES];
    [Flurry startSession:FLURRY_API_KEY];
    
    [WXApiManager sharedManager];
    [WXApi registerApp:WECHAT_APP_ID withDescription:@"moyo 17app"];

    
    
    [Helpshift installForApiKey:@"754b7c20139a1dae4b6c15ebfeb2b851"
                     domainName:@"machipopo.helpshift.com"
                          appID:@"machipopo_platform_20150907024744191-622c56672eb135f"];
    
    
    /* branch for handling deep linking */
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        // params are the deep linked params associated with the link that the user clicked before showing up.
        DLog(@"deep link data: %@", [params description]);
        
        UINavigationController* nav = (UINavigationController*)[SINGLETON mainTabBarViewController].selectedViewController;
        
        NSString *page = [params objectForKey:@"page"];
        if ([page isEqualToString:@"u"]) {
            
            NSString* userID = [params objectForKey:@"ID"];
            
            UserProfileViewController* uVC = [UserProfileViewController new];
            [uVC getUserWithUserID:userID];
            uVC.hidesBottomBarWhenPushed = YES;
            
            [nav pushViewController:uVC animated:NO];
        } else if ([page isEqualToString:@"p"]) {
            
            NSString* postID = [params objectForKey:@"ID"];;
            
            SinglePostViewController* sVC = [SinglePostViewController new];
            sVC.enterMode=EnterFromShared;
            [sVC getPostByPostID:postID];
            
            [nav pushViewController:sVC animated:NO];
        } else if ([page isEqualToString:@"live"]) {
            
            NSString* lvID = [params objectForKey:@"ID"];
            
            LiveStreamViewController* vc = [SINGLETON liveStreamSharedManager];
            if (vc.canPresentView) {
                [API_MANAGER getLiveStreamInfo:lvID withCompletion:^(BOOL success, LiveStreamObject *liveStreamInfo) {
                    if (vc.canPresentView) {
                        if (success) {

                            CustomTabbarController *tabVC = [SINGLETON mainTabBarViewController];
                            
                            vc.liveStream = liveStreamInfo;
                            vc.livestreamEnterMode = LiveStreamEnterModeWebPage;
                            vc.liveStreamMode = LiveStreamModeWatch;
                            vc.delegate = tabVC;
                            vc.transitioningDelegate = tabVC;
                            vc.modalTransitionStyle = UIModalPresentationCustom;
                            vc.modalPresentationStyle = UIModalPresentationFullScreen;
                            if (tabVC.presentedViewController==nil) {
                                [tabVC presentViewController:vc animated:YES completion:nil];
                            }
                        } else {
                            [DIALOG_MANAGER showNetworkFailToast];
                        }
                    }
                }];
            }
        }
    }];
    
    [[Branch getInstance] handleDeepLink:[launchOptions objectForKey:UIApplicationLaunchOptionsURLKey]];
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
    
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler {
    BOOL handledByBranch = [[Branch getInstance] continueUserActivity:userActivity];
    
    return handledByBranch;
}

-(void)applicationWillTerminate:(UIApplication *)application
{
    [DEFAULTS setObject:INT_TO_STRING(1) forKey:NOTLIVESTREAMING];
    [DEFAULTS synchronize];
    [EVENT_HANDLER addEventTracking:@"CloseApp" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp),@"networkUnstableCount":INT_TO_STRING([EVENT_HANDLER readNowNetworkUnstableCount]),@"audioOnlyCount":INT_TO_STRING([EVENT_HANDLER readAudioOnlyCount])}];
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [SINGLETON detectIpGeoLocation:nil];

    [DEFAULTS setObject:INT_TO_NUMBER(CURRENT_TIMESTAMP) forKey:TIME_BACKGROUND];
    [DEFAULTS synchronize];
    
    if(bgTaskIdentifier==UIBackgroundTaskInvalid) {
        bgTaskIdentifier = [application beginBackgroundTaskWithName:@"BgTask" expirationHandler:^{
        }];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [SINGLETON detectIpGeoLocation:nil];
    [FBSDKAppEvents activateApp];

    if(bgTaskIdentifier!=UIBackgroundTaskInvalid) {
        [application endBackgroundTask:bgTaskIdentifier];
        bgTaskIdentifier = UIBackgroundTaskInvalid;
    }
    [OL113Tool appActiveWithKEY:OL113_APP_KEY andAppID:APP_ID];
    [DEFAULTS setObject:INT_TO_NUMBER(CURRENT_TIMESTAMP) forKey:TIME_FOREGROUND];
    [DEFAULTS synchronize];
    [NOTIFICATION_CENTER postNotificationName:ENTER_FOREGROUND_NOTIFICATION object:nil];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
      
    if([MY_USER_ID isEqualToString:@""]) {
        return;
    }
    
    NSString* deviceInfo = [NSString stringWithFormat:@"%@ - %@", [UIDevice currentDevice].systemVersion, [UIDevice currentDevice].platformString];
    
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict addEntriesFromDictionary:@{@"deviceModel": deviceInfo,@"timeZoneOffset":INT_TO_STRING([SINGLETON getTimezone]),@"lastLogin":INT_TO_STRING(CURRENT_TIMESTAMP)}];
    

    if(![[DEFAULTS objectForKey:MY_USER_IP] isEqualToString:@""]){   

        NSArray *ipArray = [GET_DEFAULT(USER_IP) mj_JSONObject];
        
        [dict addEntriesFromDictionary:@{@"userIPs":TO_JSON(ipArray)}];
        DLog(@"ips:%@",TO_JSON(ipArray));
    }
    
    [API_MANAGER updateUserInfo:dict fetchSelfInfo:YES completion:^(BOOL success) {
        [SINGLETON reloadAllBadge];
    }];
    

    /* SandBox Mode */
    [API_MANAGER checkingSandBox:^(BOOL success) {}];
    
    if(REVENUE_MANAGER !=NULL){
        [REVENUE_MANAGER pullData:YES];
    }
}

# pragma mark - Remote Notification Delegate
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSString *key = [[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"loc-key"];
    
    if(key==nil) {
        return;
    }

    NSArray* info = [[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"loc-args"];

    NSString* postID;
    NSString* livestreamID;
    NSString* type=@"user";
    NSString* openID;
    NSString* livestreamerID;
    
    if([key isEqualToString:NEW_FOLLOW]) {
        openID = info[0];
    }else if([key isEqualToString:NEW_LIKE]){
        openID = info[0];
    }else if([key isEqualToString:NEW_COMMENT]){
        openID = info[0];
    }else if([key isEqualToString:NEW_POST_TAG]){
        openID = info[0];
    }else if([key isEqualToString:NEW_COMMENT_TAG]){
        openID = info[0];
    }else if([key isEqualToString:NEW_FRIEND_JOIN_FROM_CONTACTS]){
        openID = info[1];
    }else if([key isEqualToString:NEW_FRIEND_JOIN_FROM_FB]){
        openID = info[1];
    }else if([key isEqualToString:NEW_SYSTEM_NOTIF]){
        
    }else if([key isEqualToString:NEW_LIVE_STREAM]){
        
    }else if([key isEqualToString:NEW_LIVE_RESTREAM]){
        openID = info[0];
        livestreamerID=info[1];
    }else if([key isEqualToString:NEW_FRIEND_REQUEST]){
        openID = info[0];
    }
    
    [NOTIFICATION_CENTER postNotificationName:key object:nil];
    
    if( [userInfo objectForKey:@"postID"]){
        postID = [userInfo objectForKey:@"postID"];
        type = @"post";
    }
    
    if( [userInfo objectForKey:@"postID"]){
        postID = [userInfo objectForKey:@"postID"];
        type = @"post";
    }
    
    if( [userInfo objectForKey:@"liveStreamID"]){
        livestreamID = [userInfo objectForKey:@"liveStreamID"];
        type = @"livestream";
    }
    
    if( [userInfo objectForKey:@"openID"]){
        openID = [userInfo objectForKey:@"openID"];
    }
    
    if(NOTIF_FROM_BACKGROUND){
        
        if([type isEqualToString:@"user"] && ![openID isEqualToString:GET_DEFAULT(openID)]){
            
            UINavigationController* nav = (UINavigationController*)[SINGLETON mainTabBarViewController].selectedViewController;
            nav.hidesBottomBarWhenPushed = YES;

            if([key isEqualToString:NEW_FRIEND_REQUEST]){
                UserListViewController* ulVC  = [UserListViewController new];
                ulVC.mode = REQUEST_MODE;
                ulVC.title = LOCALIZE(@"FriendRequest");
                ulVC.hidesBottomBarWhenPushed = YES;
                [nav pushViewController:ulVC animated:YES];
            }else{
                UserProfileViewController* uVC = [UserProfileViewController new];
                [uVC getUserWithOpenID:openID];
                [nav pushViewController:uVC animated:YES];
            }
            
        }else if([type isEqualToString:@"post"]){
        
            SinglePostViewController* sVC = [SinglePostViewController new];
            sVC.enterMode=EnterFromShared;
            [sVC getPostByPostID:postID];
            UINavigationController* nav = (UINavigationController*)[SINGLETON mainTabBarViewController].selectedViewController;
            nav.hidesBottomBarWhenPushed = YES;
            [nav pushViewController:sVC animated:YES];
        
        }else if([type isEqualToString:@"livestream"]){
        
            LiveStreamViewController* vc = [SINGLETON liveStreamSharedManager];
            if (vc.canPresentView) {
                [API_MANAGER getLiveStreamInfo:livestreamID withCompletion:^(BOOL success, LiveStreamObject *liveStreamInfo) {
                    if (vc.canPresentView) {
                        if (success) {
                            CustomTabbarController *tabVC = [SINGLETON mainTabBarViewController];

                            vc.liveStream = liveStreamInfo;
                            vc.liveStreamMode = LiveStreamModeWatch;
                            vc.delegate = tabVC;
                            vc.transitioningDelegate = tabVC;
                            vc.modalTransitionStyle = UIModalPresentationCustom;
                            vc.modalPresentationStyle = UIModalPresentationFullScreen;
                            if (tabVC.presentedViewController==nil) {
                                [tabVC presentViewController:vc animated:YES completion:nil];
                            }
                        }
                        else {
                            [DIALOG_MANAGER showNetworkFailToast];
                        }
                    }
                }];
            }
        }
    }else{
        
        /* In Forground */
        
        if([type isEqualToString:@"livestream"]){
            
            [NOTIFICATION_CENTER postNotificationName:REFRESH_FEEDS object:nil];
            
            NSString* showedText = @"";
            if([key isEqualToString:NEW_LIVE_RESTREAM]){
                
                NSString* liveStreamerOpenID = info[1];
                showedText =[NSString stringWithFormat:LOCALIZE(@"NEW_LIVE_RESTREAM"),openID,liveStreamerOpenID];

            }else{
                showedText =[NSString stringWithFormat:LOCALIZE(@"NEW_LIVE_STREAM"),openID];
            }
            
            
            [DIALOG_MANAGER showActionSheetLiveRemind:openID showedText:showedText withCompletion:^(BOOL onClick) {
                if(onClick){
                    
                    if([GET_DEFAULT(IS_ON_LIVESTREAMING) isEqual:@0]){
                        LiveStreamViewController* vc = [SINGLETON liveStreamSharedManager];
                        if (vc.canPresentView) {
                            [API_MANAGER getLiveStreamInfo:livestreamID withCompletion:^(BOOL success, LiveStreamObject *liveStreamInfo) {
                                if (vc.canPresentView) {

                                    if (success) {
                                        CustomTabbarController *tabVC = [SINGLETON mainTabBarViewController];
                                        
                                        vc.liveStream = liveStreamInfo;
                                        vc.liveStreamMode = LiveStreamModeWatch;
                                        vc.delegate = tabVC;
                                        vc.transitioningDelegate = tabVC;
                                        vc.modalTransitionStyle = UIModalPresentationCustom;
                                        vc.modalPresentationStyle = UIModalPresentationFullScreen;
                                        if (tabVC.presentedViewController==nil) {
                                            [tabVC presentViewController:vc animated:YES completion:nil];
                                        }
                                    }
                                    else {
                                        [DIALOG_MANAGER showNetworkFailToast];
                                    }
                                }
                            }];
                        }
                    }else{
                        
                    }
                }
            }];
            
        }
        
        UITabBarController *tvc = [SINGLETON mainTabBarViewController];
        
        if(((int)tvc.selectedIndex)!=3 && [SINGLETON shouldPlaySoundForNotificationType:key]){
            
            // just for have a badge
            [DEFAULTS setObject:@1 forKey:COMMENT_BADGE];
            [SINGLETON reloadAllBadge];
            
        }

    }
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL result = YES;
    
    if([WeiboSDK handleOpenURL:url  delegate:[LoginHandler sharedInstance]]){
        return YES;
    }else if([WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]]){
        return YES;
    }else if([TencentOAuth HandleOpenURL:url]){
        return [TencentOAuth HandleOpenURL:url];
    }else if([[Branch getInstance] handleDeepLink:url]){
        return YES;
    }else{
        result =  [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    }
    return NO;
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString* tokenString = [[[NSString stringWithFormat:@"%@",deviceToken] stringByReplacingOccurrencesOfString:@" " withString:@""]substringWithRange:NSMakeRange(1, 64)];
    [DEFAULTS setObject:tokenString forKey:TOKEN];
    [DEFAULTS synchronize];
    
    [SINGLETON synchronizeTokenToServer];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{

}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    
    
    if([WeiboSDK handleOpenURL:url delegate:[LoginHandler sharedInstance]]){
        return YES;
    }else if([WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]]){
        return YES;
    }else if([TencentOAuth HandleOpenURL:url]){
        return YES;
    }
    
    return NO;
}



-(void) clearLocalNotifications
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}




@end
