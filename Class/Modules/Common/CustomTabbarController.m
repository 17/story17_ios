//
//  CustomTabbarController.m
//  story17
//
//  Created by POPO Chen on 6/10/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "CustomTabbarController.h"
#import "Constant.h"
#import "FeedViewController.h"
#import "SearchViewController.h"
#import "CameraViewController.h"
#import "UserProfileViewController.h"
#import "NotificationViewController.h"
#import "BlabSettingViewController.h"
#import "BlabViewController.h"
#import <DigitsKit/DigitsKit.h>
#import "UpAndDownAnimator.h"
#import "UpAndDownInteractive.h"

@interface CustomTabbarController()
@property (nonatomic, strong) UpAndDownInteractive *dismissInteractor;
@property (nonatomic, assign) BOOL enableInteractivePlayerTransitioning;

@end

@implementation CustomTabbarController

-(id)init
{
    self = [super init];
    if(self){
        
        _isXmasCameraBtn = NO;
        
        _animationBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [_animationBgView setBackgroundColor:WHITE_COLOR];
        _animationImageView = [UIImageView new];
        
        float scale = [[UIScreen mainScreen] scale];
        
        if(SYSTEM_VERSION_GREATER_THAN(@"8.0")) {
            scale = [[UIScreen mainScreen] nativeScale];
        }
        
        float height = 300/scale;
        float imageWidth = 360/scale;
        
        if(IS_IPHONE_4){
            height = 150/scale;
            imageWidth = 90;
        }else if(IS_IPHONE_6){
            height = 300/scale;
            imageWidth = 180;
        }else if(IS_IPHONE_6_PLUS){
            height = 450/scale-STATUS_BAR_HEIGHT;
            imageWidth = 180;
        }
        
        _animationImageView.frame = CGRectMake(0, 0, imageWidth, imageWidth);
        _animationImageView.center = CGPointMake(SCREEN_WIDTH/2, height+imageWidth/2);
        
        NSMutableArray* animationArray = CREATE_MUTABLE_ARRAY;
        for(int i =1;i<=16;i++){
            UIImage* image = [UIImage imageNamed:[NSString stringWithFormat:@"17_%02d",i]];
            [animationArray addObject:image];
        }
        
        [_animationImageView setImage:IMAGE_FROM_BUNDLE(@"17_01")];
        _animationImageView.contentMode = UIViewContentModeScaleAspectFill;
        [_animationImageView setBackgroundColor:MAIN_COLOR];
        _animationImageView.animationImages = [animationArray copy];
        _animationImageView.animationDuration = 1.8;
        [_animationBgView addSubview:_animationImageView];
    
        
        _tab1Button = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/5, TAB_BAR_HEIGHT)];
        [_tab1Button setImage:IMAGE_FROM_BUNDLE(@"dock_home") forState:UIControlStateNormal];
        [_tab1Button setImage:IMAGE_FROM_BUNDLE(@"dock_home_active") forState:UIControlStateSelected];
        
        _tab2Button = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/5, 0, SCREEN_WIDTH/5, TAB_BAR_HEIGHT)];
        [_tab2Button setImage:IMAGE_FROM_BUNDLE(@"dock_explore") forState:UIControlStateNormal];
        [_tab2Button setImage:IMAGE_FROM_BUNDLE(@"dock_explore_active") forState:UIControlStateSelected];
        
        _tab3Button = [[UIButton alloc]initWithFrame:CGRectMake(3*SCREEN_WIDTH/5, 0, SCREEN_WIDTH/5, TAB_BAR_HEIGHT)];
        [_tab3Button setImage:IMAGE_FROM_BUNDLE(@"dock_news") forState:UIControlStateNormal];
        [_tab3Button setImage:IMAGE_FROM_BUNDLE(@"dock_news_active") forState:UIControlStateSelected];
        
        _tab4Button = [[UIButton alloc]initWithFrame:CGRectMake(4*SCREEN_WIDTH/5, 0, SCREEN_WIDTH/5, TAB_BAR_HEIGHT)];
        [_tab4Button setImage:IMAGE_FROM_BUNDLE(@"dock_profile") forState:UIControlStateNormal];
        [_tab4Button setImage:IMAGE_FROM_BUNDLE(@"dock_profile_active") forState:UIControlStateSelected];
        
        _tab1Button.selected = YES;
        _tab2Button.selected = NO;
        _tab3Button.selected = NO;
        _tab4Button.selected = NO;

        [self.tabBar addSubview:_tab1Button];
        [self.tabBar addSubview:_tab2Button];
        [self.tabBar addSubview:_tab3Button];
        [self.tabBar addSubview:_tab4Button];
        
        
//        [self reloadCameraButton];
//
        if([SINGLETON isSpecialMode]){
            _isXmasCameraBtn = YES;
            _cameraImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dock_camera_newyear"] ];
//            [_cameraBtn setImage:[UIImage imageNamed:@"dock_camera_xmas_down"] forState:UIControlStateSelected];
            _cameraImageView.frame = CGRectMake(2*SCREEN_WIDTH/5, -20, SCREEN_WIDTH/5, TAB_BAR_HEIGHT+20);
        }else{
            _isXmasCameraBtn = NO;
            _cameraImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dock_camera"] ];
            _cameraImageView.frame = CGRectMake(2*SCREEN_WIDTH/5, 0, SCREEN_WIDTH/5, TAB_BAR_HEIGHT);
        }

        _cameraImageView.userInteractionEnabled = YES;
//        [_cameraImageView setBackgroundColor:TIFFINY_BLUE_COLOR];
        [_cameraImageView setContentMode:UIViewContentModeScaleAspectFit];
        
        UITapGestureRecognizer* cameraTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickCameraAction:)];
        [_cameraImageView addGestureRecognizer:cameraTapGesture];
        
        _cameraBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _cameraBtn.frame = CGRectMake(2*SCREEN_WIDTH/5, -20, SCREEN_WIDTH/5, TAB_BAR_HEIGHT+20);
        _cameraBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        [_cameraBtn setBackgroundColor:TIFFINY_BLUE_COLOR];
        [_cameraBtn.imageView setBackgroundColor:RED_COLOR];
        
        if([SINGLETON isSpecialMode]){
            [_cameraBtn setImage:[UIImage imageNamed:@"dock_camera_newyear"] forState:UIControlStateNormal];
            [_cameraBtn setImage:[UIImage imageNamed:@"dock_camera_newyear_down"] forState:UIControlStateSelected];
        }else{
            [_cameraBtn setImage:[UIImage imageNamed:@"dock_camera"] forState:UIControlStateNormal];
            [_cameraBtn setImage:[UIImage imageNamed:@"dock_camera_down"] forState:UIControlStateSelected];
        }
        
        
//        [_cameraBtn.imageView setBackgroundColor:BLACK_COLOR];
//        [_cameraBtn setBackgroundColor:TIFFINY_BLUE_COLOR];
        
//        _cameraBtn.imageView.frame = CGRectMake(0, 0, SCREEN_WIDTH/5, TAB_BAR_HEIGHT);
        
        [_cameraBtn.imageView setContentMode:UIViewContentModeScaleAspectFill];
        CGSize imageSize=_cameraBtn.imageView.image.size;
        _cameraBtn.titleEdgeInsets = UIEdgeInsetsMake(10, -imageSize.width, 0, 0);
        
        __weak CustomTabbarController* weakself = self;
        
        [_cameraBtn bk_addEventHandler:^(id sender) {
            
            [weakself selectAtIndex:2];
            
            [DIALOG_MANAGER showActionSheetCamera:0 withCompletion:^(NSString *selectedAction) {
                
                if([selectedAction isEqualToString:@"photo"]) {
                   
                    CameraViewController* cVC = [CameraViewController new];
                    UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:cVC];
                    cVC.currentCaptureMode=1;
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [weakself presentViewController:nav animated:YES completion:nil];
                    });
                    
                }else if([selectedAction isEqualToString:@"video"]){
                    
                    CameraViewController* cVC = [CameraViewController new];
                    UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:cVC];
                    cVC.currentCaptureMode=2;
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [weakself presentViewController:nav animated:YES completion:nil];
                    });
                    
                }else if([selectedAction isEqualToString:@"live"]){
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        if(IS_NETWORK_AVAILABLE){
                            
                            if ([GET_DEFAULT(PICTURE)isEqualToString:@""]) {
                                [self uploadProfilePicture];
                                
                            }else{
                                LiveStreamViewController* vc = [SINGLETON liveStreamSharedManager];
                                if (vc.canPresentView) {
                                    CustomTabbarController *tabVC = [SINGLETON mainTabBarViewController];
                                    
                                    vc.liveStreamMode = LiveStreamModeBroadcast;
                                    vc.delegate = tabVC;
                                    vc.transitioningDelegate = tabVC;
                                    vc.modalTransitionStyle = UIModalPresentationCustom;
                                    vc.modalPresentationStyle = UIModalPresentationFullScreen;
                                    [tabVC presentViewController:vc animated:YES completion:nil];
                                }
                            }

                        }else{
                            [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"network_unstable") message:@"" buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                            }];
                        }
                        
                        
                    });
                }else if([selectedAction isEqualToString:@"blab"]){
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        if(IS_NETWORK_AVAILABLE){
                            
                            if ([GET_DEFAULT(PICTURE)isEqualToString:@""]) {
                                [self uploadProfilePicture];
                            }else{
                                
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                    
                                    //                        BlabViewController *blabvc=[[BlabViewController alloc]init];
                                    //                        blabvc.liveStreamMode=LIVE_SHOW_MODE_HOLDER;
                                    //                        blabvc.livestreamID = @"1";
                                    //                        [weakself presentViewController:blabvc animated:YES completion:nil];
                                    
                                    BlabSettingViewController* bVC = [BlabSettingViewController new];
                                    UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:bVC];
                                    [weakself presentViewController:nav animated:YES completion:nil];
                                });
                                
                            }
                            
                        }else{
                            [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"network_unstable") message:@"" buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                            }];
                        }
                        
                        
                    });
                }
            } ];
            
   
        } forControlEvents:UIControlEventTouchUpInside];
        
        [self.tabBar addSubview:_cameraImageView];
        [_cameraImageView bringSubviewToFront:self.tabBar];

//        [self.tabBar addSubview:_cameraBtn];
//        [_cameraBtn bringSubviewToFront:self.tabBar];
        [self.view addSubview:_animationBgView];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dismissInteractor = [[UpAndDownInteractive alloc] init];
    self.enableInteractivePlayerTransitioning = NO;
}

-(void)didClickCameraAction:(id)sender
{
    [self selectAtIndex:2];
    
    [DIALOG_MANAGER showActionSheetCamera:0 withCompletion:^(NSString *selectedAction) {
        if([selectedAction isEqualToString:@"photo"]) {
            if ([GET_DEFAULT(PICTURE)isEqualToString:@""]) {
                
                [self uploadProfilePicture];
                
            }else{
                CameraViewController* cVC = [CameraViewController new];
                UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:cVC];
                cVC.currentCaptureMode=1;
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    [self presentViewController:nav animated:YES completion:nil];
                });

            }
        }else if([selectedAction isEqualToString:@"video"]){
            if ([GET_DEFAULT(PICTURE)isEqualToString:@""]) {
                
                [self uploadProfilePicture];
                
            }else{
                CameraViewController* cVC = [CameraViewController new];
                UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:cVC];
                cVC.currentCaptureMode=2;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    [self presentViewController:nav animated:YES completion:nil];
                });
            }
        }else if([selectedAction isEqualToString:@"live"]){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                if(IS_NETWORK_AVAILABLE){
                    
                    if ([GET_DEFAULT(PICTURE)isEqualToString:@""]) {

                        [self uploadProfilePicture];
                        
                    }else{
                        LiveStreamViewController* vc = [SINGLETON liveStreamSharedManager];
                        if (vc.canPresentView) {
                            CustomTabbarController *tabVC = [SINGLETON mainTabBarViewController];
                            
                            vc.liveStreamMode = LiveStreamModeBroadcast;
                            vc.delegate = tabVC;
                            vc.transitioningDelegate = tabVC;
                            vc.modalTransitionStyle = UIModalPresentationCustom;
                            vc.modalPresentationStyle = UIModalPresentationFullScreen;
                            [tabVC presentViewController:vc animated:YES completion:nil];
                        }
                    }
                    
                }else{
                    [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"network_unstable") message:@"" buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                    }];
                }
                
                
            });
        }else if([selectedAction isEqualToString:@"blab"]){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                if(IS_NETWORK_AVAILABLE){
                    
                    if ([GET_DEFAULT(PICTURE)isEqualToString:@""]) {
                        [self uploadProfilePicture];

                    }else{
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            
                            //                        BlabViewController *blabvc=[[BlabViewController alloc]init];
                            //                        blabvc.liveStreamMode=LIVE_SHOW_MODE_HOLDER;
                            //                        blabvc.livestreamID = @"1";
                            //                        [self presentViewController:blabvc animated:YES completion:nil];
                            
                            BlabSettingViewController* bVC = [BlabSettingViewController new];
                            UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:bVC];
                            [self presentViewController:nav animated:YES completion:nil];
                        });
                        
                    }

                }else{
                    [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"network_unstable") message:@"" buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                    }];
                }
            });
        }
    } ];

}



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [_animationImageView startAnimating];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.5 animations:^{
            _animationBgView.alpha = 0.0f;
        }];
    });

}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    // tabbarItem animation

    
    [self selectAtIndex:(int)[tabBar.items indexOfObject:tabBar.selectedItem]];

    
    if([tabBar.items indexOfObject:tabBar.selectedItem]==2)
        return;
    
    // scroll to top action
    
    if(self.selectedIndex == [tabBar.items indexOfObject:tabBar.selectedItem]){
        
        UINavigationController* nav = (UINavigationController*)self.selectedViewController;
        
        if([nav.viewControllers[0] isKindOfClass:[FeedViewController class]]){
            
            FeedViewController* fVC = (FeedViewController*)nav.viewControllers[0];
            [fVC.feedCollectionView setContentOffset:CGPointZero animated:YES];

        }
        if([nav.viewControllers[0] isKindOfClass:[SearchViewController class]]){
            
            SearchViewController* sVC = (SearchViewController*)nav.viewControllers[0];
            [sVC.photoCollectionView setContentOffset:CGPointZero animated:YES];
        
        }
        if([nav.viewControllers[0] isKindOfClass:[UserProfileViewController class]]){
            [EVENT_HANDLER addEventTracking:@"EnterSelfPage" withDict:nil];
            UserProfileViewController* sVC = (UserProfileViewController*)nav.viewControllers[0];
            [sVC.collectionView setContentOffset:CGPointZero animated:YES];
            
        }
        if([nav.viewControllers[0] isKindOfClass:[NotificationViewController class]]){
            
            NotificationViewController* nVC = (NotificationViewController*)nav.viewControllers[0];
            [nVC.tableViewArray[nVC.viewMode] setContentOffset:CGPointZero animated:YES];
            
        }

    }
}

-(void)selectAtIndex:(int)index
{
    if(index==0){
        [_tab1Button setSelected:YES];
        [self imageViewAnimation:_tab1Button];
        [_tab2Button setSelected:NO];
        [_tab3Button setSelected:NO];
        [_tab4Button setSelected:NO];
    }else if(index==1){
        [_tab2Button setSelected:YES];
        [self imageViewAnimation:_tab2Button];
        [_tab1Button setSelected:NO];
        [_tab3Button setSelected:NO];
        [_tab4Button setSelected:NO];
    }else if(index==3){
        [_tab3Button setSelected:YES];
        [self imageViewAnimation:_tab3Button];
        [_tab1Button setSelected:NO];
        [_tab2Button setSelected:NO];
        [_tab4Button setSelected:NO];
    }else if(index==4){
        
        [EVENT_HANDLER addEventTracking:@"EnterSelfPage" withDict:nil];
        [_tab4Button setSelected:YES];
        [self imageViewAnimation:_tab4Button];
        [_tab1Button setSelected:NO];
        [_tab3Button setSelected:NO];
        [_tab2Button setSelected:NO];
    }else if(index==2){
        [self imageViewAnimation:_cameraBtn];
        
        CAKeyframeAnimation * animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
        [animation setValues:[NSArray arrayWithObjects:@1.0 ,@1.4, @0.9, @1.15, @0.95, @1.02, @1.0, nil]];
        //    [animation setKeyTimes:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:.7], [NSNumber numberWithFloat:1.0], nil]];
        
        animation.duration = 0.8;
        animation.calculationMode = kCAAnimationCubic;
        
        [_cameraImageView.layer addAnimation:animation forKey:@"bounceAnimation"];
        [_cameraImageView startAnimating];
        
    }
}


-(void)imageViewAnimation:(UIButton*)button
{
    
//    CAPropertyAnimation* bounceAnimation = CAKeyframeAnimation(keyPath: "transform.scale");
    CAKeyframeAnimation * animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    [animation setValues:[NSArray arrayWithObjects:@1.0 ,@1.4, @0.9, @1.15, @0.95, @1.02, @1.0, nil]];
//    [animation setKeyTimes:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:.7], [NSNumber numberWithFloat:1.0], nil]];
    
    animation.duration = 0.8;
    animation.calculationMode = kCAAnimationCubic;
    
    UIImageView* imageView = button.imageView;

    [imageView.layer addAnimation:animation forKey:@"bounceAnimation"];
    [imageView startAnimating];
    
    
//    [UIView animateWithDuration:0.4
//                          delay:0
//                        options:UIViewAnimationOptionBeginFromCurrentState
//                     animations:(void (^)(void)) ^{
//                         imageView.transform=CGAffineTransformMakeScale(1.4, 1.4);
//                     }
//                     completion:^(BOOL finished){
//                         imageView.transform=CGAffineTransformIdentity;
//                     }];
    
//    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//        
//                    imageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.7, 0.7);
//        
//    }
//        completion:^(BOOL finished){
//            if (finished){
//        
//            imageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
//
////            [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
////                
////                imageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
////                
////                } completion:NULL];
//            }
//        }
//    ];
    
}


# pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [SINGLETON customizeInterface];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        UIImage* imageTaken = [info valueForKey: UIImagePickerControllerEditedImage];
        
        if(picker.sourceType!=UIImagePickerControllerSourceTypePhotoLibrary){
            [SINGLETON saveImageWithWhiteBackgroundWithoutCompleteMessage:imageTaken];
        };
        
        [DIALOG_MANAGER showLoadingView];
        
        NSString* profilePictureFileName = [SINGLETON generateRandomFileNameWithExtension:@"jpg"];
        [SINGLETON saveImageWithThumbnailImage:imageTaken andFileName:profilePictureFileName];
        
        // Handle the result image here
        
        DLog(@"%@",profilePictureFileName);
        NSError *err = nil;
        NSData *data = [NSData dataWithContentsOfFile:GET_LOCAL_FILE_PATH(profilePictureFileName)
                                              options:NSDataReadingUncached
                                                error:&err];
        
        NSLog(@"File size is : %.2f MB",(float)data.length/1024.0f/1024.0f);
        
        self.multiFileUploader = [[MultiFileUploader alloc] initWithFileNameArray:@[profilePictureFileName, [NSString stringWithFormat:@"THUMBNAIL_%@", profilePictureFileName]]];
        
        [self.multiFileUploader startUploadWithCallback:^(NSArray *fileNameArray, int code , float progress) {
            
            if(code==MULTI_UPLOAD_SUCCESS) {
                // UPLOAD SUCCESS UPDATE USER INFO
                
                [API_MANAGER  updateUserInfo:@{@"picture":profilePictureFileName}  fetchSelfInfo:YES  completion:^(BOOL success) {
                    [DIALOG_MANAGER hideLoadingView];
                    if(success){
//                      [DIALOG_MANAGER SHOwACtion]
                    }
             
                }];
                
            } else if(code==MULTI_UPLOAD_FAIL) {
                [DIALOG_MANAGER hideLoadingView];
                [DIALOG_MANAGER showNetworkFailToast];
            } else if(code==PROGRESSING){
                
            }
        }];
    }];
}

-(void)reloadCameraButton
{
    if([SINGLETON isSpecialMode]){
        
        if(_isXmasCameraBtn) return;
        
        [UIView animateWithDuration:0.2 animations:^{
            _cameraImageView.alpha = 0;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.2 animations:^{
                [_cameraImageView setImage:[UIImage imageNamed:@"dock_camera_newyear"]];
                _cameraImageView.alpha = 1;
                _cameraImageView.frame = CGRectMake(2*SCREEN_WIDTH/5, -20, SCREEN_WIDTH/5, TAB_BAR_HEIGHT+20);
                _isXmasCameraBtn = YES;
            }];
        }];
        
    }else{
        if(!_isXmasCameraBtn) return;
        
        [UIView animateWithDuration:0.2 animations:^{
            _cameraImageView.alpha = 0;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.2 animations:^{
                _cameraImageView.alpha = 1;
                [_cameraImageView setImage:[UIImage imageNamed:@"dock_camera"]];
                _cameraImageView.frame = CGRectMake(2*SCREEN_WIDTH/5, 0, SCREEN_WIDTH/5, TAB_BAR_HEIGHT);
                _isXmasCameraBtn = NO;
            }];
        }];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [SINGLETON customizeInterface];
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    UpAndDownAnimator *animator = [[UpAndDownAnimator alloc] init];
    animator.transitionType = ModalAnimatedTransitioningTypeDismiss;
    return animator;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    UpAndDownAnimator *animator = [[UpAndDownAnimator alloc] init];
    animator.transitionType = ModalAnimatedTransitioningTypePresent;
    return animator;
}

- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForPresentation:(id<UIViewControllerAnimatedTransitioning>)animator
{
    return nil;
}

- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id<UIViewControllerAnimatedTransitioning>)animator
{
    if (_enableInteractivePlayerTransitioning) {
        return _dismissInteractor;
    }
    return nil;
}

#pragma mark - LiveStreamViewControllerDelegate

- (void)liveStreamViewControllerBeginInteractiveTransitioning:(LiveStreamViewController *)liveStreamViewController
{
    self.enableInteractivePlayerTransitioning = YES;
}

- (void)liveStreamViewController:(LiveStreamViewController *)liveStreamViewController updateInteractiveTransition:(CGFloat)percentComplete shouldComplete:(BOOL)shouldComplete
{
    [_dismissInteractor updateInteractiveTransition:percentComplete];
    _dismissInteractor.shouldComplete = shouldComplete;
}

- (BOOL)liveStreamViewControllerShouldCompleteInteractiveTransition:(LiveStreamViewController *)liveStreamViewController
{
    return _dismissInteractor.shouldComplete;
}

- (void)liveStreamViewController:(LiveStreamViewController *)liveStreamViewController completeInteractiveTransition:(BOOL)complete
{
    if (complete) {
        [_dismissInteractor finishInteractiveTransition];
    }
    else {
        [_dismissInteractor cancelInteractiveTransition];
    }
    
    self.enableInteractivePlayerTransitioning = NO;
}

- (void)uploadProfilePicture
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"Please_verify_your_photo_before_live") message:@"" buttonText:LOCALIZE(@"OK") cancelable:YES withCompletion:^(BOOL okClicked) {
            if (okClicked) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    [DIALOG_MANAGER showActionSheetDialogTitle:@"" options:@[LOCALIZE(@"take_picture"),LOCALIZE(@"albom")] destructiveIndexes:@[] cancelable:YES withCompletion:^(int selectedOption) {
                        if(selectedOption==1){
                            
                            // 從相機膠卷選擇
                            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                            [[UIBarButtonItem appearance] setBackgroundImage:nil  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
                            [[UIBarButtonItem appearance] setBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
                            
                            imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
                            imagePicker.delegate = (id) self;
                            imagePicker.allowsEditing = YES;
                            
                            
                            [self presentViewController:imagePicker animated:YES completion:^{
                                
                            }];
                        }else if (selectedOption==0){
                            
                            // 照相
                            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                            imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
                            imagePicker.cameraDevice=UIImagePickerControllerCameraDeviceFront;
                            imagePicker.delegate = (id) self;
                            imagePicker.allowsEditing = YES;
                            [self presentViewController:imagePicker animated:YES completion:^{
                                
                            }];
                        }
                    }];});
                
            }
        }];
        
    });

}

@end
