//
//  CustomTabbarController.h
//  story17
//
//  Created by POPO Chen on 6/10/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveStreamViewController.h"

@class MultiFileUploader;

@interface CustomTabbarController : UITabBarController <UIImagePickerControllerDelegate, UIViewControllerTransitioningDelegate, LiveStreamViewControllerDelegate>


@property (nonatomic,strong)UIButton* tab1Button;
@property (nonatomic,strong)UIButton* tab2Button;
@property (nonatomic,strong)UIButton* tab3Button;
@property (nonatomic,strong)UIButton* tab4Button;

@property (nonatomic,strong)UIButton* cameraBtn;
@property (nonatomic,strong)UIImageView* cameraImageView;

@property (nonatomic,strong) UIImageView* animationImageView;
@property (nonatomic,strong) UIView* animationBgView;
@property (nonatomic,strong) MultiFileUploader* multiFileUploader;

@property(nonatomic, weak) UINavigationController* navCtrl;

@property (nonatomic, assign) BOOL isXmasCameraBtn;

-(void)selectAtIndex:(int)index;
-(void)reloadCameraButton;

@end
