//
//  StatusBarColorApplyingActivityViewController.m
//  story17
//
//  Created by POPO Chen on 8/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "StatusBarColorApplyingActivityViewController.h"
#import "Constant.h"

@implementation StatusBarColorApplyingActivityViewController

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:WHITE_COLOR];
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               WHITE_COLOR,NSForegroundColorAttributeName,
                                               [UIFont systemFontOfSize:18.0], NSFontAttributeName, nil];
    [[UINavigationBar appearance] setBarTintColor: BLACK_COLOR];
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

}

- (void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {
    
    [super presentViewController:viewControllerToPresent animated:flag completion:^{
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        if (completion) {
            completion();
        }
    }];
}

@end
