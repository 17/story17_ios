//
//  PublishPostViewController.m
//  story17
//
//  Created by POPO Chen on 5/21/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "PublishPostViewController.h"
#import "Constant.h"
#import "PublishPostObject.h"
#import "Endian.h"
#import "MyLocaitionManager.h"
#import "ListTableViewController.h"
#import "LocationSearchController.h"
#import "SearchViewController.h"
#import "TagUserViewController.h"

@implementation PublishPostViewController

#define topMargin 10

#define imageWidth 65
#define inputViewHeight 85

#define userIdentity @"userIdentity"
#define tagIdentity @"tagIdentity"

#define userCellHeight 55
#define tagCellHeight 55

#define max_length 500

- (id)init
{
    self = [super init];
    if (self)
    {
        _searchArray = CREATE_MUTABLE_ARRAY;
        _taggedusers = CREATE_MUTABLE_ARRAY;
        _hashtags = CREATE_MUTABLE_ARRAY;
        _searchString = @"";
        
        _locationObj = [LocationObject new];
        _locationObj.name = @"";
        _locationObj.locationID = @"";
        _locationObj.latitude = 0;
        _locationObj.longitude = 0;
        
        _lastCharacterLength=  0;
    }
    
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self addCustomNavigationBackButton];
    [self configureViewForIOS7];
//    [self postlocation];

    self.title = LOCALIZE(@"ShareTo");


//    [LOCATION_MANAGER updateLocation];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:LOCALIZE(@"Done") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];

    float nextY = 0.0f;
    
    _postBGView = [[UIView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, inputViewHeight)];
    [_postBGView setBackgroundColor:WHITE_COLOR];
    
    _bottomLine = [ThemeManager separaterLine];
    _bottomLine.frame = CGRectMake(0, inputViewHeight, SCREEN_WIDTH, 0.3);
    
    _postImageView = [[UIImageView alloc]initWithFrame:CGRectMake(topMargin, topMargin, imageWidth, imageWidth)];

    _inputTextField = [[UITextView alloc]initWithFrame:CGRectMake(imageWidth+topMargin, topMargin/2, SCREEN_WIDTH-imageWidth-topMargin, imageWidth)];
    [_inputTextField setTintColor:MAIN_COLOR];
    _inputTextField.font = SYSTEM_FONT_WITH_SIZE(14);
    _inputTextField.delegate = self;
    _inputTextField.keyboardType = UIKeyboardTypeTwitter;

    [_postBGView addSubview:_postImageView];
    [_postBGView addSubview:_inputTextField];
    
    nextY += _postBGView.frame.size.height+30;
     /* Location */
    
    
    _tagUserView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_tagUserView styleSettingsWithMode:WITH_NONE];
    [_tagUserView.titleLabel setText:LOCALIZE(@"Photo_with_Location")];
    _tagUserView.userInteractionEnabled = YES;
    [_tagUserView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        TagUserViewController* tagVC = [TagUserViewController new];
        [self.navigationController presentViewController:tagVC animated:YES completion:nil];
        
    }]];
    
    nextY += _tagUserView.frame.size.height;
    
    _locationSwitchView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 45)];
    [_locationSwitchView styleSettingsWithMode:WITH_SWITCH];
    [_locationSwitchView.titleLabel setText:LOCALIZE(@"Photo_with_Location")];

    [_locationSwitchView.switchView addTarget:self action:@selector(switchIsChanged:)
              forControlEvents:UIControlEventValueChanged];
//    [self.view addSubview:_locationSwitchView];

    _locationSwitchView.userInteractionEnabled = YES;

    nextY += _locationSwitchView.frame.size.height;
    _cordy_locationview=nextY;
    _locationView = [[ArrowSwitchLabelView alloc]initWithFrame:CGRectMake(0, nextY-45, SCREEN_WIDTH, 45)];
    [_locationView styleSettingsWithMode:WITH_NONE];

    [_locationView.titleLabel setText:LOCALIZE(@"YourLocation")];
    
    [_locationView.switchView addTarget:self action:@selector(didSelectLocatin:) forControlEvents:UIControlEventTouchUpInside];
    [_locationView addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didSelectLocatin:)]];

    _charNumLabel = [[UILabel alloc]initWithFrame:CGRectMake(10,inputViewHeight, SCREEN_WIDTH-20, 25)];
    _charNumLabel.font = SYSTEM_FONT_WITH_SIZE(12);
    [_charNumLabel setTextColor:GRAY_COLOR];
    [_charNumLabel setText:[NSString stringWithFormat:@"%d",max_length]];
    _charNumLabel.textAlignment = NSTextAlignmentRight;
    
    _suggestTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, inputViewHeight, SCREEN_WIDTH, SCREEN_HEIGHT-KEYBOARD_HEIGHT-inputViewHeight-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-37)];
    [_suggestTableView setBackgroundColor:LIGHT_BG_COLOR];
    _suggestTableView.hidden = YES;
    _suggestTableView.delegate = self;
    _suggestTableView.dataSource = self;
    _suggestTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:_postBGView];
    [self.view addSubview:_suggestTableView];
    [self.view addSubview:_charNumLabel];
    [self.view addSubview:_bottomLine];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    [_inputTextField becomeFirstResponder];
    _enterPageTimeStamp=CURRENT_TIMESTAMP;
    _postSuccess=NO;
    if([_videoUUID isEqualToString:@""] || _videoUUID == nil){
        [EVENT_HANDLER addEventTracking:@"EnterTextTypePage" withDict:@{@"type":@"pic"}];
    }else{
        [EVENT_HANDLER addEventTracking:@"EnterTextTypePage" withDict:@{@"type":@"video"}];
    }
    [self uploadImage];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:NO];
    [_inputTextField resignFirstResponder];
    NSString * booleanString = (_postSuccess) ? @"True" : @"False";
    if([_videoUUID isEqualToString:@""] || _videoUUID == nil){
        [EVENT_HANDLER addEventTracking:@"LeaveTextTypePage" withDict:@{@"type":@"pic",@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp),@"caption":_inputTextField.text,@"post":booleanString}];
    }else{
        [EVENT_HANDLER addEventTracking:@"LeaveTextTypePage" withDict:@{@"type":@"video",@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp),@"caption":_inputTextField.text,@"post":booleanString}];
    }
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

-(void)uploadImage
{
    [_postImageView setImage:_postimg];

    if([_videoUUID isEqualToString:@""] || _videoUUID == nil){
        _imageName = [SINGLETON generateRandomFileNameWithExtension:@"jpg"];
        [SINGLETON saveImageWithThumbnailImage:_postimg andFileName:_imageName];
        [PUBLISH_POST_HANDLER startUploading:@[_imageName, [NSString stringWithFormat:@"THUMBNAIL_%@", _imageName]]];
    } else {
        _imageName = [NSString stringWithFormat:@"%@.jpeg",_videoUUID];
        _videoName = [NSString stringWithFormat:@"%@.mp4",_videoUUID];
        [SINGLETON saveImageWithThumbnailImage:_postimg andFileName:_imageName];
        [PUBLISH_POST_HANDLER encodeVideo:_audioDataCache videoTotalFrameCount:_totalVideoFrameCount videoUUID:_videoUUID];
    }
    
//    [LOCATION_MANAGER getNowLocation];

}


-(void)doneAction:(id)sender
{
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    [self refineInputText];
    [self getAllTagers];
    _postSuccess=YES;
    NSString* alertString = @"";
    if([_taggedusers count]>10){
        alertString = LOCALIZE(@"no_more_than_ten_people");
    }
    if([_hashtags count]>10){
        alertString = LOCALIZE(@"no_more_than_ten_hashtag");
    }
    
    if(![alertString isEqualToString:@""])
    {
        [DIALOG_MANAGER showActionSheetOKDialogTitle:@"" message:@"" buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {}];
        return;
    }

    [SINGLETON mainTabbarSetSelectedIndex:0];
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
    NSString* type = @"";
    NSArray* uploadFileName ;
    if([_videoUUID isEqualToString:@""] || _videoUUID==nil){
        _videoName = @"";
        type = @"image";
        uploadFileName = GET_IMAGES_ARRAY(_imageName);
    }else{
        type = @"video";
        uploadFileName = GET_VIDEO_ARRAY(_imageName, _videoName);
    }
    
    PublishPostObject* publishObj = [PublishPostObject new];
    publishObj.postImage = _postimg;

    if ([_locationSwitchView.switchView isOn]){
        
        publishObj.uploadDict = @{@"picture":_imageName,@"caption":_inputTextField.text,@"userTags":TO_JSON(_taggedusers),@"hashTags":TO_JSON(_hashtags),@"taggedUsers":TO_JSON(@[]),@"locationID":_locationObj.locationID,@"locationName":_locationObj.name,@"latitude":[NSNumber numberWithFloat:_locationObj.latitude],@"longitude":[NSNumber numberWithFloat:_locationObj.longitude],@"canComment":@"1",@"reachability":@"",@"type":type,@"video":_videoName};
    
    }else{
        
        publishObj.uploadDict = @{@"picture":_imageName,@"caption":_inputTextField.text,@"userTags":TO_JSON(_taggedusers),@"hashTags":TO_JSON(_hashtags),@"taggedUsers":TO_JSON(@[]),@"locationID":@"",@"locationName":@"",@"latitude":@"0.0",@"longitude":@"0.0",@"canComment":@"1",@"reachability":@"",@"type":type,@"video":_videoName};
    
    }
    
    publishObj.uploadImageName = uploadFileName;
    publishObj.code = [PUBLISH_POST_HANDLER getStatusFromFilenameArray:uploadFileName];  // must know the status before insert into
    
    [PUBLISH_POST_HANDLER addPublishObj:publishObj];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(_type!=NoneTagging){
        return [_searchArray count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_type==UserTagging){
        SearchUserCell *cell = [tableView dequeueReusableCellWithIdentifier:userIdentity];
        if(cell==nil){
            cell = [[SearchUserCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:userIdentity];
        }
        UserObject* user = _searchArray[indexPath.row];
        [cell reloadCell:user];
        cell.followBtn.hidden = YES;
        return cell;
    }else{
        TagViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tagIdentity];
        if(cell==nil){
            cell = [[TagViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:tagIdentity];
        }
        
        TagObject* tag = _searchArray[indexPath.row];
        [cell reloadCell:tag];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_type==UserTagging){
        return userCellHeight;
    }else if(_type==HashTagging){
        return tagCellHeight;
    }
    return 0;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* appendStr = @"";
    
    if(_type==UserTagging){
        
        UserObject* user = [_searchArray objectAtIndex:indexPath.row];
        appendStr = user.openID;
    
    }else if(_type==HashTagging){
        TagObject* tag = [_searchArray objectAtIndex:indexPath.row];
        appendStr = tag.hashTag;
    }
    
    _inputTextField.text = [self getNewstringFromSelectedstring:appendStr];
    
    _type = NoneTagging;
    [_searchArray removeAllObjects];
    _suggestTableView.hidden = YES;
    
}


#pragma mark - UITextFieldDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    /* Searching Query Update */
//    if (range.location>1) {
//        NSString * stringemoji = [textView.text substringWithRange:NSMakeRange(range.location-2,2)];
//        DLog(@"bbbbbb:%@",stringemoji);
//    }

    
    NSString * stringToRange = [textView.text substringWithRange:NSMakeRange(0,range.location)];
    DLog(@"aaaaa:%@ 長度=%lu location=%lu",stringToRange,(unsigned long)range.length,(unsigned long)range.location);
    _searchString = [self textViewQueryStringParser:text withRange:range];
    
    [self searchModeDetected];
    
    if(_type==NoneTagging){
        _suggestTableView.hidden = YES;
    }
    
    if(_type==HashTagging){
        DLog(@"search");
        [API_MANAGER searchHashTag: [_searchString substringFromIndex:1] offset:0 count:100 withCompletion:^(BOOL success, NSArray *tags) {
            if(success){

                [_searchArray removeAllObjects];
                for(TagObject* tag in tags){
                    [_searchArray addObject:tag];
                }
                [_suggestTableView reloadData];
                
                if([_searchArray count]!=0){
                    _suggestTableView.hidden = NO;
                }
            }
        }];
    }else if(_type==UserTagging){
        
        
        [API_MANAGER getSearchUsers:[_searchString substringFromIndex:1] followingOnly:1 offset:0 count:100 completion:^(BOOL success, NSArray *userObjects) {
            if(success){
                [_searchArray removeAllObjects];
                for(UserObject* user in userObjects){
                    [_searchArray addObject:user];
                }
                [_suggestTableView reloadData];
                if([_searchArray count]!=0){
                    _suggestTableView.hidden = NO;
                }

            }
        }];
    }

    if(_inputTextField.text.length+text.length > max_length && ![text isEqualToString:@""]){
        return NO;
    }
    
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    [_charNumLabel setText:[NSString stringWithFormat:@"%lu",max_length-_inputTextField.text.length]];
}


-(NSString*)textViewQueryStringParser:(NSString*)replacementText withRange:(NSRange)range
{
    
    NSString* newStr = @"";

    if(![replacementText isEqualToString:@""]){
        newStr = [NSString stringWithFormat:@"%@%@",_inputTextField.text,replacementText];
    }else{
        if([_inputTextField.text length]==0)
            return @"";
        newStr = [_inputTextField.text substringWithRange:NSMakeRange(0,range.location)];

    }
    
    NSArray* userTag = [newStr componentsSeparatedByString:@"@"];
    NSArray* hashTag = [newStr componentsSeparatedByString:@"#"];

    NSString* lastUserTag = @"";
    NSString* lastHashTag = @"";
    
    if([userTag count]!=0){
        if([userTag count]>=2){
            
            lastUserTag = [userTag objectAtIndex:[userTag count]-1];
        }else{
            lastUserTag = [userTag objectAtIndex:0];
        }
    }
    
    if([hashTag count]!=0){
        if([hashTag count]>=2){
            lastHashTag = [hashTag objectAtIndex:[hashTag count]-1];
        }else{
            lastHashTag = [hashTag objectAtIndex:0];
        }
    }
    
    if(![lastHashTag isEqualToString:@""] && [lastHashTag length] < [lastUserTag length] && [lastHashTag rangeOfString:@" "].location == NSNotFound)
        return [NSString stringWithFormat:@"#%@",lastHashTag];
    
    if(![lastUserTag isEqualToString:@""] && [lastUserTag length] < [lastHashTag length] && [lastUserTag rangeOfString:@" "].location == NSNotFound)
        return [NSString stringWithFormat:@"@%@",lastUserTag];
    
    return @"";
    
}

-(void)searchModeDetected
{
    
    /* Search Mode JudgeMent */
    if( [_searchString isEqualToString:@""]){
        [_searchArray removeAllObjects];
        _type = NoneTagging;
        return;
    }
    
    if( [[_searchString substringToIndex:1] isEqualToString:@"#"]){
        _type = HashTagging;
    }else if( [[_searchString substringToIndex:1] isEqualToString:@"@"]){
        _type = UserTagging;
    }
}

-(NSString*)getNewstringFromSelectedstring:(NSString*)selectedstring
{
    NSString* trimString = [_inputTextField.text substringToIndex:_inputTextField.text.length-_searchString.length];
 
    DLog(@"trimString:%@ selectedString:%@",trimString,selectedstring);
    
    if(_type==HashTagging){
//        return selectedstring;
        return [NSString stringWithFormat:@"%@#%@ ",trimString,selectedstring];
    }else if(_type==UserTagging){
        return [NSString stringWithFormat:@"%@@%@ ",trimString,selectedstring];
    }
    return @"";
}

-(void)getAllTagers
{
    [_taggedusers removeAllObjects];
    [_hashtags removeAllObjects];
    
    if([_inputTextField.text isEqualToString:@""])
        return;
    
    NSArray* userArray = [_inputTextField.text componentsSeparatedByString:@"@"];
    NSArray* tagArray = [_inputTextField.text componentsSeparatedByString:@"#"];

    if([userArray count]>=1){
        for(int i =1;i< [userArray count];i++){
            NSString* preString = userArray[i-1];
            if([preString hasSuffix:@" "] || [preString isEqualToString:@""]){
                [_taggedusers addObject:[[userArray[i] componentsSeparatedByString:@" "][0] lowercaseString]];
            }
        }
    }
    
    if([tagArray count]>=1){

        for(int i =1;i< [tagArray count];i++){
            NSString* preString = tagArray[i-1];
            if([preString hasSuffix:@" "] || [preString isEqualToString:@""]){
                [_hashtags addObject:[[tagArray[i] componentsSeparatedByString:@" "][0] lowercaseString]];
            }
        }
    }
}

-(void)refineInputText
{
    if([_inputTextField.text isEqualToString:@""])
        return;
    
    NSString* refinedString = _inputTextField.text;
    NSArray* userArray = [_inputTextField.text componentsSeparatedByString:@"@"];

    if([userArray count]>=1){
        for(int i =0;i< [userArray count];i++){
            if(i==0){
                refinedString = userArray[0];
            }else{
                NSString* preString = userArray[i-1];
                if(![preString hasSuffix:@" "]){
                    refinedString = [NSString stringWithFormat:@"%@ @%@",refinedString,userArray[i]];
                }else{
                    refinedString = [NSString stringWithFormat:@"%@@%@",refinedString,userArray[i]];
                }
            }
        }
    }
    
    NSArray* tagArray = [refinedString componentsSeparatedByString:@"#"];

    if([tagArray count]>=1){
        for(int i =0;i< [tagArray count];i++){
            if(i==0){
                refinedString = tagArray[0];
            }else{
                NSString* preString = tagArray[i-1];
                if(![preString hasSuffix:@" "]){
                    refinedString = [NSString stringWithFormat:@"%@ #%@",refinedString,tagArray[i]];
                }else{
                    refinedString = [NSString stringWithFormat:@"%@#%@",refinedString,tagArray[i]];
                }
            }
        }
    }
    
    _inputTextField.text = refinedString;
}


- (UIImage *)makeUIImage:(uint8_t *)inBaseAddress bufferInfo:(CVPlanarPixelBufferInfo_YCbCrBiPlanar *)inBufferInfo width:(size_t)inWidth height:(size_t)inHeight bytesPerRow:(size_t)inBytesPerRow {
    
    NSUInteger yPitch = EndianU32_BtoN(inBufferInfo->componentInfoY.rowBytes);
    
    uint8_t *rgbBuffer = (uint8_t *)malloc(inWidth * inHeight * 4);
    uint8_t *yBuffer = (uint8_t *)inBaseAddress;
    uint8_t val;
    int bytesPerPixel = 4;
    
    // for each byte in the input buffer, fill in the output buffer with four bytes
    // the first byte is the Alpha channel, then the next three contain the same
    // value of the input buffer
    for(int y = 0; y < inHeight*inWidth; y++)
    {
        val = yBuffer[y];
        // Alpha channel
        rgbBuffer[(y*bytesPerPixel)] = 0xff;
        
        // next three bytes same as input
        rgbBuffer[(y*bytesPerPixel)+1] = rgbBuffer[(y*bytesPerPixel)+2] =  rgbBuffer[y*bytesPerPixel+3] = val;
    }
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(rgbBuffer, yPitch, inHeight, 8,
                                                 yPitch*bytesPerPixel, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
    
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    UIImage *image = [UIImage imageWithCGImage:quartzImage];
    
    CGImageRelease(quartzImage);
    free(rgbBuffer);
    return  image;
}


- (void) switchIsChanged:(UISwitch *)paramSender{
    //NSLog(@"Sender is = %@", paramSender);
     CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
   if ([paramSender isOn]){
        [UIView animateWithDuration:0.3 animations:^{
            if (status == kCLAuthorizationStatusDenied)
            {
                NSString*title;
                
                title=(status == kCLAuthorizationStatusDenied) ? LOCALIZE(@"loc_off") : @"Background use is not enabled";
                
                NSString *message = LOCALIZE(@"goset");
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:LOCALIZE(@"Cancel") otherButtonTitles:LOCALIZE(@"setting"), nil];
                
                [alert show];
            }
        
            _locationView.alpha = 1.0;
            _locationView.frame = CGRectMake(0, _cordy_locationview, SCREEN_WIDTH, 45);
            
        }];
     //   NSLog(@"The switch is turned on.");
         [self.view addSubview:_locationView];
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            
            _locationView.alpha = 0;
            
            _locationView.frame = CGRectMake(0, _cordy_locationview-45, SCREEN_WIDTH, 45);
            
        }];
      //  NSLog(@"The switch is turned off.");
//            [_locationView removeFromSuperview];

    } }



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        
        NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        
        [[UIApplication sharedApplication]openURL:settingsURL];
        
    }
}

-(void)didSelectLocatin:(id)sender
{
    LocationSearchController* lsVC = [LocationSearchController new];
    
    if(![LOCATION_MANAGER eof]){
        lsVC.dataArray = nil;
        [LOCATION_MANAGER updateLocation];
    }else{
        lsVC.dataArray = [LOCATION_MANAGER locationObjArray];
    }
    
    [lsVC setSelectionCallback:^(LocationObject * locObj) {
        
        _locationView.titleLabel.text = locObj.name;
        _locationObj.name = locObj.name;
        _locationObj.longitude = locObj.longitude;
        _locationObj.latitude = locObj.latitude;
        _locationObj.locationID = locObj.locationID;
        
    }];
    
    [self.navigationController presentViewController:NAV(lsVC) animated:YES completion:nil];
}



@end
