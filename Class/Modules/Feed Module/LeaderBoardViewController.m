//
//  LeaderBoardViewController.m
//  story17
//
//  Created by POPO Chen on 8/14/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "LeaderBoardViewController.h"
#import "Constant.h"
#import "RankTableViewCell.h"
#import "UserProfileViewController.h"
#import "SVPullToRefresh.h"

@interface LeaderBoardViewController()

@property (nonatomic, strong) UIScrollView* mainScrollView;
@property (nonatomic, strong) UIScrollView *navScrollView;
@property (nonatomic, strong) UIView *beforeView;
@property (nonatomic, strong) UIView *afterView;

@property (nonatomic, strong) NSArray* rankArray;
@property (nonatomic, strong) NSArray* modeArray;
@property (nonatomic, strong) NSArray* strArray;

@property (nonatomic, strong) NSMutableArray* labelArray;

@property (nonatomic, strong) NSMutableArray* hourRankArray;
@property (nonatomic, strong) NSMutableArray* dayRankArray;
@property (nonatomic, strong) NSMutableArray* weekRankArray;
@property (nonatomic, strong) NSMutableArray* monthRankArray;
@property (nonatomic, strong) NSMutableArray* yearRankArray;
//@property (nonatomic, strong) NSMutableArray* lifeRankArray;

@property (nonatomic, strong) UILabel* hourRankLabel;
@property (nonatomic, strong) UILabel* dayRankLabel;
@property (nonatomic, strong) UILabel* weekRankLabel;
@property (nonatomic, strong) UILabel* monthRankLabel;
@property (nonatomic, strong) UILabel* yearRankLabel;
//@property (nonatomic, strong) UILabel* lifeRankLabel;
@property (nonatomic, strong) UILabel* titleView;

@property (nonatomic, strong) NSMutableArray *reusableTableViews;
@property (strong, nonatomic) NSMutableArray *visibleTableViews;
@property (nonatomic) NSInteger currentPage;
@property (nonatomic) BOOL decelerating;

@end


@implementation LeaderBoardViewController

#define RankCell @"RankCell"
#define TOTAL_PAGE 5
#define scrollViewHeight SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-NAVI_BAR_HEIGHT
#define LABEL_WIDTH SCREEN_WIDTH/4


typedef NS_ENUM(NSInteger, InfiniteScrollViewPosition) {
    InfiniteScrollViewPositionBegin,
    InfiniteScrollViewPositionMiddle,
    InfiniteScrollViewPositionEnd
};

- (id)init
{
    self = [super init];
    
    if(self){
        
        _decelerating = NO;
        _visibleTableViews = CREATE_MUTABLE_ARRAY;
        
        _labelArray = CREATE_MUTABLE_ARRAY;
        _hourRankArray = CREATE_MUTABLE_ARRAY;
        _dayRankArray = CREATE_MUTABLE_ARRAY;
        _weekRankArray = CREATE_MUTABLE_ARRAY;
        _monthRankArray = CREATE_MUTABLE_ARRAY;
        _yearRankArray = CREATE_MUTABLE_ARRAY;
//        _lifeRankArray = CREATE_MUTABLE_ARRAY;

        _modeArray = @[@"weekly",@"daily",@"hourly",@"yearly",@"monthly"];
        _rankArray = @[_weekRankArray,_dayRankArray,_hourRankArray,_yearRankArray,_monthRankArray];
        _strArray = @[LOCALIZE(@"leaderboard_week"),LOCALIZE(@"leaderboard_day"),LOCALIZE(@"leaderboard_hour"),LOCALIZE(@"leaderboard_year"),LOCALIZE(@"leaderboard_month")];
        
    }
    return self;
}

-(void)viewDidLoad
{
    
    self.title = LOCALIZE(@"leaderboard");
    
    [super viewDidLoad];
    [self addCustomNavigationBackButton];
    [self configureViewForIOS7];
    
    [self setupPages];
    [self loadPage:2];
    _currentPage = 2;
    
    _navScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2-(LABEL_WIDTH)/2, 0, LABEL_WIDTH, NAVI_BAR_HEIGHT)];
    _navScrollView.layer.masksToBounds = NO;
    
    _beforeView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, (SCREEN_WIDTH-_navScrollView.frame.size.width)/2, NAVI_BAR_HEIGHT)];
    _afterView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-_beforeView.frame.size.width, 0, (SCREEN_WIDTH-_navScrollView.frame.size.width)/2, NAVI_BAR_HEIGHT)];
    _beforeView.userInteractionEnabled = YES;
    _afterView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer* nextTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pageChange:)];
    UITapGestureRecognizer* beforeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pageChange:)];
    [_beforeView addGestureRecognizer:beforeTap];
    [_afterView addGestureRecognizer:nextTap];
    
    [_beforeView setBackgroundColor:[UIColor clearColor]];
    [_afterView setBackgroundColor:[UIColor clearColor]];
    
    
    NSArray* strArray = [self getNavArrayOfPage:(int)_currentPage];
    for(int i=0;i<[strArray count];i++){
        
        UILabel* nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*LABEL_WIDTH, 0, LABEL_WIDTH, NAVI_BAR_HEIGHT)];
        [nameLabel setText:strArray[i]];
        if(i!=4)
            [nameLabel setTextColor:GRAY_COLOR];
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [_labelArray addObject:nameLabel];
        [_navScrollView addSubview:nameLabel];
    }
    [self.view addSubview:_navScrollView];
    [self.view addSubview:_beforeView];
    [self.view addSubview:_afterView];
    
    UIImageView* badgeImageView = [[UIImageView alloc]initWithImage:IMAGE_FROM_BUNDLE(@"badge")];
    badgeImageView.frame = CGRectMake(SCREEN_WIDTH/2-3, NAVI_BAR_HEIGHT-9, 6, 6);
    [self.view addSubview:badgeImageView];
    
    UIView* lineView = [[UIView alloc]initWithFrame:CGRectMake(0, NAVI_BAR_HEIGHT-0.5, SCREEN_WIDTH, 0.5)];
    [self.view addSubview:lineView];


    [_navScrollView setContentOffset:CGPointMake((LABEL_WIDTH)*4, 0)];
    [self.view addSubview:_mainScrollView];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [UIView animateWithDuration:0.3 animations:^{
        _navScrollView.alpha = 0.0f;
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshNavbar];
    _mainScrollView.scrollEnabled = YES;
    [UIView animateWithDuration:0.3 animations:^{
        _navScrollView.alpha = 1.0f;
    }];
}

#pragma mark - UITableViewDelegagte
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [RankTableViewCell getRankCellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int tag = (int)tableView.tag;
    NSMutableArray* userArray  =_rankArray[tag];
    UserObject* user = userArray[indexPath.row];
    
    if([user.userID isEqualToString:MY_USER_ID])
        return;
    
    UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
    userCtrl.user = user;
    [userCtrl getUserWithUserID:userCtrl.user.userID];
    userCtrl.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:userCtrl animated:YES];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int tag = (int)tableView.tag;
    NSMutableArray* userArray  =_rankArray[tag];
    return [userArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RankTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:RankCell];
    int tag = (int)tableView.tag;
    NSMutableArray* userArray  =_rankArray[tag];
    UserObject* user = userArray[indexPath.row];
    
    if(cell==nil){
        cell = [[RankTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:RankCell];
    }
    
    [cell reloadCell:user withIndex:(int)indexPath.row];
    return cell;
}


#pragma mark - reuse table
- (NSMutableArray *)reusableTableViews
{
    if (!_reusableTableViews) {
        _reusableTableViews = [NSMutableArray array];
    }
    return _reusableTableViews;
}

- (NSMutableArray *)visibleTableViews
{
    if (!_visibleTableViews) {
        _visibleTableViews = [NSMutableArray array];
    }
    return _visibleTableViews;
}

- (void)enqueueReusableTableView:(UITableView *)tableView
{
    [self.reusableTableViews addObject:tableView];
}

- (UITableView *)dequeueReusableTableView
{
    
    UITableView *tableView = [self.reusableTableViews firstObject];
    if (tableView) {
        DLog(@"!!! reuse table %ld!!!", tableView.tag);
        [self.reusableTableViews removeObject:tableView];
    } else {
        // init table view
        DLog(@"!!! init table !!!");
        tableView = [UITableView new];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.delegate = self;
        tableView.dataSource = self;
        [tableView registerClass:[RankTableViewCell class] forCellReuseIdentifier:RankCell];
        
        __weak LeaderBoardViewController* weakself = self;
        __weak UITableView* weaktable = tableView;

        [tableView addPullToRefreshWithActionHandler:^{
            [weakself fetchDataAtPage:weaktable refresh:YES page:_currentPage position:InfiniteScrollViewPositionMiddle];
        }];
    }
    return tableView;
}

/* UI Settings */
-(void) tableViewSettings:(UITableView*)tableView
{
    tableView.delegate = self;
    tableView.dataSource = self;
//    [tableView setBackgroundColor:[SINGLETON getRandomColor]];
    [tableView registerClass:[RankTableViewCell class] forCellReuseIdentifier:RankCell];
}

#pragma mark - page control
- (void)addTableViewToPosition:(InfiniteScrollViewPosition)pos forPage:(NSInteger)page
{
    if (page < 0 || page >= TOTAL_PAGE) {
        return;
    }
    
    DLog(@"set page %ld to pos:%ld", page, pos);

    UITableView *tableView = [self dequeueReusableTableView];
    tableView.tag = page;
    tableView.frame = CGRectMake(self.mainScrollView.frame.size.width * page, 0.0, self.mainScrollView.frame.size.width, self.mainScrollView.frame.size.height);
    [self resetTableView:tableView toPosition:pos];
    
    [self.mainScrollView addSubview:tableView];
    [self.visibleTableViews addObject:tableView];
    [self fetchDataAtPage:tableView refresh:NO page:page position:pos];
}

- (void)resetTableView:(UITableView*)tv toPosition:(InfiniteScrollViewPosition)pos
{
    switch (pos) {
        case InfiniteScrollViewPositionBegin:
            tv.frame = CGRectMake(0, 0, self.mainScrollView.frame.size.width, self.mainScrollView.frame.size.height);
            break;
        case InfiniteScrollViewPositionMiddle:
            tv.frame = CGRectMake(self.mainScrollView.frame.size.width*1, 0, self.mainScrollView.frame.size.width, self.mainScrollView.frame.size.height);
            break;
        case InfiniteScrollViewPositionEnd:
            tv.frame = CGRectMake(self.mainScrollView.frame.size.width*2, 0, self.mainScrollView.frame.size.width, self.mainScrollView.frame.size.height);
            break;
    }
}

- (void)setupPages
{
    // set up data scroll view
    self.mainScrollView = [ThemeManager getBouncedScrollView];
    self.mainScrollView.frame = CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, scrollViewHeight);
    self.mainScrollView.delegate = self;
    [self.mainScrollView setContentSize:CGSizeMake(SCREEN_WIDTH*3, self.mainScrollView.frame.size.height)];
    
    [self.view addSubview:self.mainScrollView];
    
    // init current page
    self.currentPage = -1;
    
}

- (void)setCurrentPage:(NSInteger)currentPage
{
    if (_currentPage != currentPage) {
        _currentPage = currentPage;
    }
}

- (NSInteger)getNextPage:(NSInteger)page
{
    return (page==TOTAL_PAGE-1)?0:page+1;
}

- (NSInteger)getPreviousPage:(NSInteger)page
{
    return (page)?page-1:TOTAL_PAGE-1;
}

- (void)loadPage:(NSInteger)page
{
    // set page content
    if (self.currentPage!=-1 && page==self.currentPage) {
        return;
    }
    
    NSLog(@"This currentPage is %ld", self.currentPage);

    self.currentPage = page;
    NSMutableDictionary *pagesToLoad = CREATE_MUTABLE_DICTIONARY;
    [pagesToLoad setObject:@([self getPreviousPage:page]) forKey:@(InfiniteScrollViewPositionBegin)];
    [pagesToLoad setObject:@(page) forKey:@(InfiniteScrollViewPositionMiddle)];
    [pagesToLoad setObject:@([self getNextPage:page]) forKey:@(InfiniteScrollViewPositionEnd)];

    NSArray *tmpVisibleTableViews = [NSArray arrayWithArray:self.visibleTableViews];
    for (UITableView *tableView in tmpVisibleTableViews) {
        NSInteger tableInPage = tableView.tag;
        if (tableInPage == page) {
            [pagesToLoad removeObjectForKey:@(InfiniteScrollViewPositionMiddle)];
            [self resetTableView:tableView toPosition:InfiniteScrollViewPositionMiddle];
            
        } else if (tableInPage == page-1) {
            [self resetTableView:tableView toPosition:InfiniteScrollViewPositionBegin];
            [pagesToLoad removeObjectForKey:@(InfiniteScrollViewPositionBegin)];

        } else if (tableInPage == page+1) {
            [self resetTableView:tableView toPosition:InfiniteScrollViewPositionEnd];
            [pagesToLoad removeObjectForKey:@(InfiniteScrollViewPositionEnd)];

        } else {
            // the table view is out of vision, enqueue and remove it
            [self enqueueReusableTableView:tableView];
            [tableView removeFromSuperview];
            [self.visibleTableViews removeObject:tableView];
            DLog(@"table:%ld, remove from visibleTableViews, enQ", tableInPage);
        }
    }
    
    for (NSNumber *p in pagesToLoad) {
        [self addTableViewToPosition:[p integerValue] forPage:[[pagesToLoad objectForKey:p] integerValue]];
    }
    
    [self.mainScrollView setContentOffset:CGPointMake(self.mainScrollView.frame.size.width, 0)];
}

#pragma mark - scroll view
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.mainScrollView) {
       
        CGFloat navXOffset = ((scrollView.contentOffset.x-SCREEN_WIDTH) / SCREEN_WIDTH) * self.navScrollView.frame.size.width;

        DLog(@"scrollViewDidScroll");
        if(_decelerating == NO){
            self.navScrollView.contentOffset = CGPointMake(4*LABEL_WIDTH+navXOffset, 0.0);
        }
        
        if (scrollView.contentOffset.x >= (2*scrollView.frame.size.width)) {
            // next page
            _decelerating = YES;
            DLog(@"didScroll from page %ld to page %ld", self.currentPage, self.currentPage+1);
            [self loadPage:[self getNextPage:self.currentPage]];
            
        } else if (scrollView.contentOffset.x <= 0) {
            // prvious page
            DLog(@"didScroll from page %ld to page %ld", self.currentPage, self.currentPage-1);
            _decelerating = YES;
            [self loadPage:[self getPreviousPage:self.currentPage]];
            
        }
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if(scrollView == self.mainScrollView){
        [self refreshNavbar];
        scrollView.scrollEnabled = YES;
        _decelerating = NO;
        _beforeView.userInteractionEnabled = YES;
        _afterView.userInteractionEnabled = YES;
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    if(scrollView == self.mainScrollView){
        scrollView.scrollEnabled = NO;
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView == self.mainScrollView){
        
        DLog(@"scrollViewDidEndDecelerating");
        [self refreshNavbar];
        scrollView.scrollEnabled = YES;
        _decelerating = NO;
    }
}

-(void) refreshNavbar
{
    
    DLog(@"refreshNavbar");
    for(UIView* view in [_navScrollView subviews]){
        [view removeFromSuperview];
    }
    
    NSArray* strArray = [self getNavArrayOfPage:(int)_currentPage];
    
    for(int i=0 ;i<[strArray count];i++){
    
        UILabel* nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*LABEL_WIDTH, 0, LABEL_WIDTH, NAVI_BAR_HEIGHT)];
        [nameLabel setText:strArray[i]];
//        nameLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        
        if(i!=4){
            [nameLabel setTextColor:GRAY_COLOR];
        }
        
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [_labelArray addObject:nameLabel];
        [_navScrollView addSubview:nameLabel];
    }
    
    /* relocated navscrollView */
    self.navScrollView.contentOffset = CGPointMake(4*LABEL_WIDTH, 0.0);

}

-(void)fetchDataAtPage:(UITableView*)currentTableView refresh:(BOOL)refresh page:(NSInteger)page position:(InfiniteScrollViewPosition)pos
{
    DLog(@"fetch page:%ld",page);
    DLog(@"currentPage:%ld",_currentPage);

    if(page<0 || page>= [_modeArray count])
        return;
    
    NSString* mode = [_modeArray objectAtIndex:page];
    NSMutableArray* dataArray = [_rankArray objectAtIndex:page];
    
    for(UITableView* tableView in _visibleTableViews){
        if(tableView.tag != -1){
            if(!refresh)
                [tableView setContentOffset:CGPointZero animated:NO];
            [tableView reloadData];

        }
    }
    
    if([dataArray count]==0 || refresh){
        
        DLog(@"API getLikeLeaderboard");
        if(pos==InfiniteScrollViewPositionMiddle && !refresh)
           [DIALOG_MANAGER showLoadingView];
        
        [API_MANAGER getLikeLeaderboard:mode count:100 offset:0 withCompletion:^(BOOL success, NSArray *userObjects) {
            [currentTableView.pullToRefreshView stopAnimating];
            [DIALOG_MANAGER hideLoadingView];
            if(success){
                [dataArray removeAllObjects];
                [dataArray addObjectsFromArray:userObjects];
                for(UITableView* tableView in _visibleTableViews){
                    if(tableView.tag != -1)
                        [tableView reloadData];
                }
            }else{
                if(pos==InfiniteScrollViewPositionMiddle)
                    [DIALOG_MANAGER showNetworkFailToast];
            }
        }];
    }
    else{
        [currentTableView.pullToRefreshView stopAnimating];
    }
}

-(BOOL)currentTableView{
    return YES;
}

-(NSArray*)getNavArrayOfPage:(int)page
{
    NSMutableArray* array = CREATE_MUTABLE_ARRAY;
    
    for(int i=page-4;i<page;i++){
        
        if(i>=0){
            [array addObject:_strArray[i]];
        }else{
            [array addObject:_strArray[[_strArray count]+i]];
        }
    }
    
    for(int i=page;i<=page+4;i++){
        
        if(i < [_strArray count]){
            [array addObject:_strArray[i]];
        }else{
            [array addObject:_strArray[i-[_strArray count]]];
        }
    }
    return array;
}


-(void)pageChange:(UITapGestureRecognizer*)tapGesture
{
    if([tapGesture.view isEqual:_beforeView]){
        /* with scroll animation */
        
        _beforeView.userInteractionEnabled = NO;
        _afterView.userInteractionEnabled = NO;

        _mainScrollView.scrollEnabled = NO;
        [_mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];

    }else if([tapGesture.view isEqual:_afterView]){
        /* with scroll animation */
        _beforeView.userInteractionEnabled = NO;
        _afterView.userInteractionEnabled = NO;
        
        _mainScrollView.scrollEnabled = NO;
        [_mainScrollView setContentOffset:CGPointMake(SCREEN_WIDTH*2, 0) animated:YES];

    }
}

@end
