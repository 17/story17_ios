//
//  SinglePostViewController.m
//  Story17
//
//  Created by POPO on 2015/5/7.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "SinglePostViewController.h"
#import "ListTableViewController.h"
#import "UserProfileViewController.h"
#import "EditPostViewController.h"

@implementation SinglePostViewController

@synthesize post = _post;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    
    if (_collectionView == nil)
    {
        CGRect rect = self.view.bounds;
        rect.size.height -= NAVI_BAR_HEIGHT + STATUS_BAR_HEIGHT;
    
        _bgTapView = [[UIView alloc]initWithFrame:CGRectMake(0, -NAVI_BAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [self.view addSubview:_bgTapView];

        UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
        [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
        [layout setItemSize:CGSizeMake(self.view.bounds.size.width, [PostListCell getCellHeigth])];
        [layout setMinimumInteritemSpacing:5];
    
        _collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:layout];
        [_collectionView setDataSource:self];
        _collectionView.delegate = self;
        [_collectionView registerClass:[PostListCell class] forCellWithReuseIdentifier:@"PostCell"];
        _collectionView.scrollsToTop = YES;
        _collectionView.alwaysBounceVertical = YES;
        [_collectionView setBackgroundColor:LIGHT_BG_COLOR];
        [self.view addSubview:_collectionView];
        _collectionView.userInteractionEnabled = YES;
         
    }
}

- (void)setPost:(PostObject *)post
{
    if ([post.type isEqualToString:@"video"]) {
        self.title = LOCALIZE(@"SINGLE_POST_TITLE_VIDEO");
    } else {
        self.title = LOCALIZE(@"SINGLE_POST_TITLE_PHOTO");
    }
    
    _post = post;
//    DLog(@"%@",_post.postID);
//    DLog(@"%@",_post.picture);

    if(_post.user==NULL){
        
    }
    if (_post==nil) {
        return;
    }

    [_collectionView reloadData];
    
    [API_MANAGER viewPost:TO_JSON(@[_post.postID]) withCompletion:^(BOOL success){}];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
    _likeCount=0;
    _enterPageTimeStamp=CURRENT_TIMESTAMP;
    
    switch (_enterMode) {
        case EnterFromBrowser:
            _from=@"Browser";
            break;
        case EnterFromDiscover:
            _from=@"Discover";
            break;
        case EnterFromFollowerLike:
            _from=@"FollowerLike";
            break;
        case EnterFromRevenue:
            _from=@"Revenue";
            break;
        case EnterFromShared:
            _from=@"Shared";
            break;
        case EnterFromUserProfile:
            _from=@"UserProfile";
            break;
        default:
            break;
    }
    if (_post!=nil&&_from!=nil) {
        [EVENT_HANDLER addEventTracking:@"EnterPostPage" withDict:@{@"targetUserID":_post.user.userID,@"postID":_post.postID,@"from":_from}];

    }

    [NOTIFICATION_CENTER addObserver:self selector:@selector(setCellToPlay:) name:BUFFER_VIDEO_UPLADED object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [SINGLETON startVideoAVPlayerForCollectionView:_collectionView];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (_post.user.userID!=nil && _post.postID != nil && _from!=nil) {
        [EVENT_HANDLER addEventTracking:@"LeavePostPage" withDict:@{@"targetUserID":_post.user.userID,@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp),@"postID":_post.postID,@"likeCount":INT_TO_STRING(_likeCount),@"from":_from}];
    }
    
    [NOTIFICATION_CENTER removeObserver:self name:BUFFER_VIDEO_UPLADED object:nil];
    [SINGLETON stopAVPlayerForCollectionView:_collectionView];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [_collectionView setContentOffset:CGPointMake(0, 0)];
}

#pragma mark - data source
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    return CGSizeMake(SCREEN_WIDTH, [PostListCell getCellHeigthWithPost:_post]);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.post == nil ? 0 : 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    PostListCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PostCell" forIndexPath:indexPath];
    if (cell == nil){
        CGRect rect = CGRectMake(0, 0, self.view.bounds.size.width, [PostListCell getCellHeigth]);
        cell = [[PostListCell alloc] initWithFrame:rect];
    }
    
    /* TRICKY This order can not change */
    cell.navCtrl = self.navigationController;
    cell.delegate = self;
    cell.post = _post;
    
    [cell showFollowerButton];
    
    return cell;
    
}

#pragma mark - PostCellDelegate
-(void)didClickPostImage:(UITapGestureRecognizer *)tapGesture withPostID:(NSString*)postID
{
    _likeCount++;
    [EVENT_HANDLER addEventTracking:@"LikePostSinglePost" withDict:@{@"postID":postID,@"targetUserID":_post.user.userID,@"from":_from}];
    CGPoint touchPoint = [tapGesture locationInView:_bgTapView];
    [DIALOG_MANAGER showHeartFromPoint:touchPoint];
}

-(void)reloadCellPlayer:(PostListCell *)cell
{
    
    NSArray* viewControllers = self.navigationController.viewControllers;
    if (viewControllers.count > 1 && [viewControllers objectAtIndex:viewControllers.count-1] == self) {
        [cell startAVPlayer];
    }
    
}

-(void)didClickMore:(PostListCell*)cell
{
    [self didClickMoreDialog:cell collectionView:_collectionView dataArray:[@[_post] mutableCopy]];
}

-(void)getPostByPostID:(NSString*)postID
{
    
    [DIALOG_MANAGER showLoadingView];
    [API_MANAGER getPostInfo:postID withCompletion:^(BOOL success, PostObject *post) {
        [DIALOG_MANAGER hideLoadingView];
        if(success){
            [self setPost:post];
            [_collectionView reloadData];
            DLog(@"getPostByPostID");
        }else{
            [DIALOG_MANAGER showNetworkFailToast];
        }
    }];
}

/* handle buffer video callback */
-(void)setCellToPlay:(id)sender
{
    [SINGLETON startVideoAVPlayerForCollectionView:_collectionView];
}

# pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [LIKE_HANDLER likeBatchUpload];
}



@end
