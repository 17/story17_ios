//
//  PostCell.h
//  Story17
//
//  Created by POPO on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PostListCell;
@class PostObject;
@class AVPlayer;
@class AVPlayerLayer;
@class PostCellTitle;
@class STTweetLabel;

@protocol PostCellDelegate <NSObject>
- (void)didClickPostImage:(UITapGestureRecognizer *)tapGesture withPostID:(NSString *)postID;

@optional
- (void)didClickMore:(PostListCell *)cell;
- (void)reloadCellPlayer:(PostListCell *)cell;
@end

typedef void(^videoDoneBlock)();

@interface PostListCell : UICollectionViewCell

@property (weak, nonatomic) PostObject *post;
@property (strong, nonatomic) PostCellTitle *postTitleView;
@property (strong, nonatomic) UIImageView *postImageView;
@property (strong, nonatomic) UIImageView *videoImageView;
@property (strong, nonatomic) UIButton *conmmentBtn;
@property (strong, nonatomic) UIButton *likeBtn;
@property (strong, nonatomic) UIButton *moreBtn;

@property (strong, nonatomic) UIView *videoView;
@property (strong, nonatomic) AVPlayer *avPlayer;
@property (strong, nonatomic) AVPlayerLayer *videoLayer;

@property (strong, nonatomic) UILabel *likeLabel;
@property (strong, nonatomic) UILabel *commentLabel;
@property (strong, nonatomic) UIView *separatorLine;

@property (strong, nonatomic) STTweetLabel *commentTextView;
@property (weak, nonatomic) id<PostCellDelegate> delegate;

@property (weak, nonatomic) UINavigationController *navCtrl;

@property (strong, nonatomic) UIActivityIndicatorView *loadingView;

// for delete callback
+ (float)getCellHeigth;
+ (float)getCellHeigthWithPost:(PostObject*)post;
- (void)showFollowerButton;

- (void)startAVPlayer;
- (void)stopAVPlayer;
- (BOOL)isCellVisible;

@end
