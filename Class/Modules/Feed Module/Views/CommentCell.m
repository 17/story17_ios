//
//  CommentCell.m
//  Story17
//
//  Created by POPO on 2015/5/5.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "CommentCell.h"
#import "UserObject.h"
#import "UserProfileViewController.h"
#import "BrowsePostsViewController.h"
#import "ListTableViewController.h"

@implementation CommentCell

@synthesize comment = _comment;

#define messageFontSize 14
#define kCatchWidth 130.0f

+ (float)getHeight
{
    return [CommentTitleView getDefaultHeight];
}

+ (float)getHeightFromCommentObj:(CommentObject*)commentObj
{
    float resizeHeight = [CommentTitleView getHeightFromSubstring:commentObj.comment] ;
    DLog(@"%f", (resizeHeight < [CommentTitleView getDefaultHeight])?[CommentTitleView getDefaultHeight]:resizeHeight);
    return (resizeHeight < [CommentTitleView getDefaultHeight])?[CommentTitleView getDefaultHeight]:resizeHeight;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        
        int defaultHeight = [CommentTitleView getDefaultHeight];
        
        self.isShowingMenu = NO;
        
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.bounds), defaultHeight)];
        scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.bounds) + kCatchWidth, 55);
        scrollView.delegate = self;
        scrollView.showsHorizontalScrollIndicator = NO;
        
        [self.contentView addSubview:scrollView];
        self.scrollView = scrollView;
        
        UIView *scrollViewButtonView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.bounds) - kCatchWidth, 0.0f, kCatchWidth, defaultHeight)];
        self.scrollViewButtonView = scrollViewButtonView;
        [self.scrollView addSubview:scrollViewButtonView];

        _repostButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _repostButton.backgroundColor = GRAY_COLOR;
        _repostButton.frame = CGRectMake(0.0f, 0.0f, kCatchWidth / 2.0f, defaultHeight);
        [_repostButton setImage:IMAGE_FROM_BUNDLE(@"repost") forState:UIControlStateNormal];
        [_repostButton setImage:IMAGE_FROM_BUNDLE(@"repost") forState:UIControlStateHighlighted];
        [_repostButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_repostButton addTarget:self action:@selector(userPressedRepostButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollViewButtonView addSubview:_repostButton];

        _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _moreButton.backgroundColor =DARK_GRAY_COLOR;
        _moreButton.frame = CGRectMake(kCatchWidth / 2.0f, 0.0f, kCatchWidth / 2.0f, defaultHeight);
        [_moreButton setImage:IMAGE_FROM_BUNDLE(@"warn") forState:UIControlStateNormal];
        [_moreButton setImage:IMAGE_FROM_BUNDLE(@"warn") forState:UIControlStateHighlighted];
        [_moreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_moreButton addTarget:self action:@selector(userPressedReportButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollViewButtonView addSubview:_moreButton];

        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _deleteButton.backgroundColor =PINK_COLOR;
        _deleteButton.frame = CGRectMake(kCatchWidth / 2.0f, 0.0f, kCatchWidth / 2.0f, defaultHeight);
        [_deleteButton setImage:IMAGE_FROM_BUNDLE(@"delete") forState:UIControlStateNormal];
        [_deleteButton setImage:IMAGE_FROM_BUNDLE(@"delete") forState:UIControlStateHighlighted];
        [_deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_deleteButton addTarget:self action:@selector(userPressedDeleteButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollViewButtonView addSubview:_deleteButton];
        
        UIView *scrollViewContentView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.bounds), defaultHeight)];
        [scrollViewContentView setBackgroundColor:WHITE_COLOR];
        [self.scrollView addSubview:scrollViewContentView];
        self.scrollViewContentView = scrollViewContentView;

        _timestamp = [[UILabel alloc] init];
        [_timestamp setTextAlignment:NSTextAlignmentRight];
        _timestamp.font = SYSTEM_FONT_WITH_SIZE(10);
        [_timestamp setTextColor:DARK_GRAY_COLOR];
        [self.scrollViewContentView addSubview:_timestamp];
        
        _titleView = [[CommentTitleView alloc] init];
        _titleView.userInteractionEnabled = YES;
        [self.scrollViewContentView addSubview:_titleView];
     
    }
    
    return self;
}

/* Tricky! Failed if not added */
-(void)layoutSubviews {
    [super layoutSubviews];
    if(self.scrollView.contentOffset.x==0.0){
        self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.bounds) + kCatchWidth, CGRectGetHeight(self.bounds));
        self.scrollView.frame = CGRectMake(0, 0, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
        self.scrollViewButtonView.frame = CGRectMake(CGRectGetWidth(self.bounds) - kCatchWidth, 0, kCatchWidth, CGRectGetHeight(self.bounds));
        self.scrollViewContentView.frame = CGRectMake(0, 0, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    }
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if (frame.size.width <= 0 || frame.size.height <= 0 || _titleView == nil)
        return;
    
    [_titleView setFrame:self.bounds];

    [_timestamp sizeToFit];
    CGRect rect = self.bounds;
    rect.origin.y = 5;
    rect.origin.x = rect.size.width - _timestamp.frame.size.width-10;
    rect.size.width = _timestamp.frame.size.width;
    rect.size.height = _timestamp.frame.size.height;
    [_timestamp setFrame:rect];
}

- (void)setComment:(CommentObject *)comment
{
    _comment = comment;
    
    [self.scrollView setContentOffset:CGPointZero animated:NO];

    _titleView.image = comment.user.picture;
    _titleView.title = comment.user.openID;;
//    _titleView.navCtrl = _navCtrl;
    [_titleView setUserSubTitle:comment.comment];
    
    _timestamp.text = [SINGLETON getElaspsedTimeString:comment.timestamp];
    
    [self buttonMode];
    
//    UITapGestureRecognizer* tapGesture = [UITapGestureRecognizer new];
//    [tapGesture addTarget:self action:@selector(testingAction)];
//    [_titleView.userSubTitle addGestureRecognizer:tapGesture];
    
    __weak UINavigationController* nav = _navCtrl;
    __weak CommentCell* comCell = self;
    
    [_titleView.userSubTitle setDetectionBlock:^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range) {
        
        
        NSString* userOpenID = [string substringToIndex:1];
        if([[string substringFromIndex:1] isEqualToString:GET_DEFAULT(USER_OPEN_ID)]){
            [comCell.contentView shake:5 withDelta:4 speed:0.05 shakeDirection:ShakeDirectionHorizontal];
            return;
        }
        
        NSString* keyword = [string substringFromIndex:1];
        if([userOpenID isEqualToString:@"@"]){

            
            UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
            [userCtrl getUserWithOpenID:keyword];
            userCtrl.hidesBottomBarWhenPushed = YES;
            [nav pushViewController:userCtrl animated:YES];
            
        }else if([[string substringToIndex:1] isEqualToString:@"#"]){
            
            if([GET_DEFAULT(IN_SANDBOX) isEqualToString:@"1"]){
                return;
            }
            
            BrowsePostsViewController* broVC =[BrowsePostsViewController new];
            broVC.tagQuery = keyword;
            broVC.title = string;
            broVC.hidesBottomBarWhenPushed = YES;
            [nav pushViewController:broVC animated:YES];
        }
    }];
    
    
    [ _titleView.userImage addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
       
        if([comCell.comment.user.openID isEqualToString:GET_DEFAULT(USER_OPEN_ID)]){
            [comCell.contentView shake:5 withDelta:4 speed:0.05 shakeDirection:ShakeDirectionHorizontal];
            return;
        }

        UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
        userCtrl.user = comCell.comment.user;
        [userCtrl getUserWithUserID:userCtrl.user.userID];
        userCtrl.hidesBottomBarWhenPushed = YES;
        [comCell.navCtrl pushViewController:userCtrl animated:YES];
        
    }]];
    
    
}


-(void)buttonMode
{
    float cellHeight = [CommentCell getHeightFromCommentObj:_comment];

    _scrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH, cellHeight+0.5);
    
//    if(_comment.isActive){
//        [self.scrollView setContentOffset:CGPointMake(kCatchWidth, 0) animated:NO];
//    }else{
//        [self.scrollView setContentOffset:CGPointZero animated:NO];
//    }

    
    if([_comment.userID isEqualToString:MY_USER_ID]){
        
        _repostButton.frame = CGRectMake(0, 0, kCatchWidth / 2.0f, cellHeight);
        _deleteButton.frame = CGRectMake(kCatchWidth / 2.0f, 0.0f, kCatchWidth / 2.0f, cellHeight);
        _repostButton.hidden = NO;
        _deleteButton.hidden = NO;
        _moreButton.hidden = YES;
        
    }else if(self.isSelfPost || [GET_DEFAULT(IS_ADMIN) intValue] >=1){
        
        _repostButton.frame = CGRectMake(0, 0,kCatchWidth / 3.0f, cellHeight);
        _moreButton.frame = CGRectMake(kCatchWidth / 3.0f, 0, kCatchWidth / 3.0f,  cellHeight);
        _deleteButton.frame = CGRectMake(2*kCatchWidth / 3.0f, 0, kCatchWidth / 3.0f, cellHeight);
        _deleteButton.hidden = NO;
        _repostButton.hidden = NO;
        _moreButton.hidden = NO;
            
    }else{

        _repostButton.frame = CGRectMake(0, 0, kCatchWidth / 2.0f, cellHeight);
        _moreButton.frame = CGRectMake(kCatchWidth / 2.0f, 0, kCatchWidth / 2.0f, cellHeight);
        _repostButton.hidden = NO;
        _deleteButton.hidden = YES;
        _moreButton.hidden = NO;
    }
 
}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    if (scrollView.contentOffset.x > kCatchWidth-30) {
        targetContentOffset->x = kCatchWidth;
//        _comment.isActive = YES;
    }else {
//        _comment.isActive = NO;
        *targetContentOffset = CGPointZero;
        
        // Need to call this subsequently to remove flickering. Strange.
        dispatch_async(dispatch_get_main_queue(), ^{
            [scrollView setContentOffset:CGPointZero animated:YES];
        });
    }
    
//    DLog(@"%f",scrollView.contentOffset.x);

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    if (scrollView.contentOffset.x < 0) {
        scrollView.contentOffset = CGPointZero;
    }
    
    self.scrollViewButtonView.frame = CGRectMake(scrollView.contentOffset.x + (CGRectGetWidth(self.bounds) - kCatchWidth), 0.0f, kCatchWidth, CGRectGetHeight(self.bounds));
    
    if (scrollView.contentOffset.x >= kCatchWidth-30) {
        if (!self.isShowingMenu) {
            self.isShowingMenu = YES;
            [self.delegate cell:self didShowMenu:self.isShowingMenu];
        }
    } else if (scrollView.contentOffset.x == 0.0f) {
        if (self.isShowingMenu) {
            self.isShowingMenu = NO;
            [self.delegate cell:self didShowMenu:self.isShowingMenu];
        }
    }
}


#pragma mark - didClickButton

- (void)userPressedDeleteButton:(id)sender {
    
    [self.delegate cellDidSelectDelete:self];
}

- (void)userPressedRepostButton:(id)sender {
    
    [self.delegate cellDidSelectRepost:self];
}

- (void)userPressedReportButton:(id)sender {
    
    [self.delegate cellDidSelectReport:self];
    
    [DIALOG_MANAGER showActionSheetDialogTitle:@"" options:@[LOCALIZE(@"report_comment_spam"),LOCALIZE(@"report_comment_content")] destructiveIndexes:@[] cancelable:YES withCompletion:^(int selectedOption) {
        if(selectedOption==0){
            
            [API_MANAGER reportCommentAction:_comment.userID   withReason:LOCALIZE(@"report_comment_spam") andCommentID:_comment.commentID withCompletion:^(BOOL success, NSString *message) {
                if(success){
                    [DIALOG_MANAGER showCompleteToast];
                    [self dismissCell];
                }else{
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
            
        }else if(selectedOption==1){
            
            ListTableViewController* listTableView = [ListTableViewController new];
            listTableView.dataArray = REPORTED_COMMENT_REASON;
            listTableView.hidesBottomBarWhenPushed= YES;
            listTableView.title = LOCALIZE(@"Reason");
            [listTableView setSelectionCallback:^(NSString * callbackStr) {
                
                [API_MANAGER reportCommentAction:_comment.userID   withReason:callbackStr andCommentID:_comment.commentID withCompletion:^(BOOL success, NSString *message) {
                    if(success){
                        [DIALOG_MANAGER showCompleteToast];
                        [self dismissCell];
                    }else{
                        [DIALOG_MANAGER showNetworkFailToast];
                    }
                }];
            }];
            [self.navCtrl pushViewController:listTableView animated:YES];
        
        }
    }];    
}

- (void)dismissCell
{
    [_scrollView setContentOffset:CGPointZero animated:YES];
//    [UIView animateWithDuration:0.3 animations:^{
//
//        [_scrollView setContentOffset:CGPointZero];
//    }];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    return YES;
}

@end
