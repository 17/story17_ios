//
//  PublishCell.m
//  story17
//
//  Created by POPO Chen on 6/17/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "PublishCell.h"
#import "Constant.h"

@implementation PublishCell


#define PublishCellHeight 50
#define imageWidth 40
#define btnWidth 30
#define margin 3
#define progressHeight 2


+(float)getDefaultHeight
{
    return PublishCellHeight;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
        [self.contentView setBackgroundColor:WHITE_COLOR];
        
        
        _uploadImageView = [UIImageView new];
        _captionLabel = [UILabel new];
        _reuploadBtn = [UIButton new];
        _deleteBtn = [UIButton new];
        
        _progressBgView = [UIView new];
        _progressView = [UIView new];
        
        [_progressBgView setBackgroundColor:GRAY_COLOR];
        [_progressView setBackgroundColor:RED_COLOR];
        
        [_captionLabel setText:LOCALIZE(@"upload_failed")];
        _captionLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        [_captionLabel setTextColor:DARK_GRAY_COLOR];
        
        [self.contentView addSubview:_uploadImageView];
        [self.contentView addSubview:_captionLabel];
        [self.contentView addSubview:_reuploadBtn];
        [self.contentView addSubview:_deleteBtn];
     
        [self.contentView addSubview:_progressBgView];
        [self.contentView addSubview:_progressView];
        
        _progressView.frame = CGRectMake(0, PublishCellHeight-progressHeight, SCREEN_WIDTH, progressHeight);
        _progressBgView.frame = CGRectMake(0, PublishCellHeight-progressHeight, SCREEN_WIDTH, progressHeight);
    
    }
    return self;
}


-(void)reloadCell:(PublishPostObject*)obj
{
    _publishObj = obj;
    
    if(FILE_EXIST(_publishObj.uploadDict[@"picture"])) {
        NSData* fileData = [NSData dataWithContentsOfFile:GET_LOCAL_FILE_PATH(_publishObj.uploadDict[@"picture"])];
        [_uploadImageView setImage:[UIImage imageWithData:fileData]];
    }else{
        [_uploadImageView setImage:[UIImage imageNamed:@"placehold_s"]];
    }

    
    CGRect rect = CGRectMake(margin, margin, imageWidth, imageWidth);
    
    _uploadImageView.frame = rect;
    [_uploadImageView setBackgroundColor:MAIN_COLOR];

    rect.origin.x += _uploadImageView.frame.size.width+3*margin;
    rect.origin.y += _uploadImageView.frame.size.height/2+_uploadImageView.frame.origin.y-18;
    rect.size.width = 120;
    rect.size.height = 26;
    _captionLabel.frame = rect;
    
    _reuploadBtn.frame = CGRectMake(SCREEN_WIDTH-2*imageWidth, (PublishCellHeight-btnWidth)/2, btnWidth, btnWidth);
    [_reuploadBtn setBackgroundImage:IMAGE_FROM_BUNDLE(@"retry_post") forState:UIControlStateNormal];
    [_reuploadBtn setBackgroundImage:IMAGE_FROM_BUNDLE(@"retry_post_down") forState:UIControlStateHighlighted];
    [_reuploadBtn addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
    
    _deleteBtn.frame = CGRectMake(SCREEN_WIDTH-imageWidth, (PublishCellHeight-btnWidth)/2, btnWidth, btnWidth);
    [_deleteBtn setBackgroundImage:IMAGE_FROM_BUNDLE(@"delete_post") forState:UIControlStateNormal];
    [_deleteBtn setBackgroundImage:IMAGE_FROM_BUNDLE(@"delete_post_down") forState:UIControlStateHighlighted];
    [_deleteBtn addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];

    [self reloadProgress:_publishObj.progress andStatus:_publishObj.code];
    
}

-(void)reloadProgress:(float)progress andStatus:(int)code
{

    if(code==MULTI_UPLOAD_FAIL){

        [_captionLabel setText:LOCALIZE(@"upload_failed")];
        _progressView.frame = CGRectMake(0, PublishCellHeight-progressHeight, SCREEN_WIDTH, progressHeight);
        [_progressView setBackgroundColor:RED_COLOR];
    
    }else if(code==MULTI_UPLOAD_SUCCESS){
    
        [_captionLabel setText:LOCALIZE(@"publishing")];
        _progressView.frame = CGRectMake(0, PublishCellHeight-progressHeight, SCREEN_WIDTH*progress, progressHeight);
        [_progressView setBackgroundColor:MAIN_COLOR];
    
    }else if(code==PROGRESSING){
        
        DLog(@"%f",SCREEN_WIDTH*progress);
        DLog(@"%f",progress);
        
        [_captionLabel setText:LOCALIZE(@"progressing")];
        [_progressView setBackgroundColor:MAIN_COLOR];
        [_progressBgView.layer removeAllAnimations];
        _progressView.frame = CGRectMake(0, PublishCellHeight-progressHeight, SCREEN_WIDTH*progress, progressHeight);

    }
    
}

-(void)didClickButton:(UIButton*)btn
{
    DLog(@"didClickButotn");
    if([btn isEqual:_deleteBtn]){
        
        if(_delegate!=nil)
            [_delegate didClickDeletePost:self];
        
    }else if([btn isEqual:_reuploadBtn]){
        if(_delegate!=nil){
            [_delegate didClickReupload:self];
        }
    
    }
}

@end
