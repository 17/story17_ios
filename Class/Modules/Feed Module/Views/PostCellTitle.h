//
//  PostCellTitle.h
//  Story17
//
//  Created by POPO on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserTitleView.h"

@interface PostCellTitle : UserTitleView
@property (assign, nonatomic) int views;
@property (assign, nonatomic) float money;
@property (assign, nonatomic) int timestamp;
@property (strong, nonatomic) UILabel *moneyLabel;
@property (strong, nonatomic) UILabel *viewsLabel;
@property (strong, nonatomic, readonly) UIImageView *userImage;
@property (strong, nonatomic) UIView *followBgView;  // for SinglePostViewController
@property (strong, nonatomic) UIButton *followBtn;  // for SinglePostViewController

@end

