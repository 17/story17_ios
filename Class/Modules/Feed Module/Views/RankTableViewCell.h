//
//  RankTableViewCell.h
//  story17
//
//  Created by POPO Chen on 8/14/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserObject.h"

@interface RankTableViewCell : UITableViewCell

@property (nonatomic,weak) UserObject* user;

@property (nonatomic,strong) UIImageView* userImageView;
@property (nonatomic,strong) UIImageView* maskImageView;
@property (nonatomic,strong) UIImageView* topImageView;
@property (nonatomic,strong) UIImageView* verifyImageView;

@property (nonatomic,strong) UILabel* rankLabel;
@property (nonatomic,strong) UIImageView* heartImageView;
@property (nonatomic,strong) UIButton* followButton;
@property (nonatomic,strong) UIView* followBgView;
@property (nonatomic,strong) UILabel* openIDLabel;
@property (nonatomic,strong) UILabel* userNameLabel;
@property (nonatomic,strong) UILabel* totalCountLabel;

@property (nonatomic,strong) UIView* bottomline;

-(void)reloadCell:(UserObject*)user withIndex:(int)index;
+(float)getRankCellHeight;

@end

