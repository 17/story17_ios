//
//  GridWithTypeView.h
//  story17
//
//  Created by POPO Chen on 5/6/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GridWithTypeView : UIView
-(void)reloadCellWithType:(id)post;

@end
