#import "Constant.h"

#define TEXT_MODE 1
#define AUDIO_MODE 2
#define MEDIA_MODE 3
#define GAME_MODE 4

@class CommentViewController;
@class MediaSelectionView;


typedef enum {
    POST_COMMENT_MESSAGE,
    LIVE_COMMENT_MESSAGE
} MESSAGE_MODE;


@interface MessageInputBar : UIView <UITextViewDelegate>

@property MESSAGE_MODE messageMode;

//@property (assign) CommentViewController* chatVC;
@property (assign) id chatVC;
@property (nonatomic, retain) UITextView* messageTextView;
@property (nonatomic, retain) UIButton* sendMessageButton;
@property (nonatomic, retain) UIImageView* textViewBgImageView;
@property (nonatomic,retain) UIButton* sayhiButton;

@property (nonatomic, strong) NSArray* textViewGestureRecognizers;
@property (nonatomic, strong) UIGestureRecognizer* mediaToggleGestureRecognizer;
//@property (nonatomic, strong) NSString* searchString;


@property int messageInputBarHeight;
@property float keyboardHeight;
@property float lastKeyboardY;
@property int mode;

- (void)textViewDidChange:(UITextView *)textView;
-(void) addKeyboardNotification;
-(void) removeKeyboardNotification;
-(void) resizeView;

@end
