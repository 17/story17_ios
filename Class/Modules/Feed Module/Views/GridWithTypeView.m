//
//  GridWithTypeView.m
//  story17
//
//  Created by POPO Chen on 5/6/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "GridWithTypeView.h"
#import "Constant.h"
#import "UIImageView+AFNetworking.h"
#import "PostObject.h"
#import "LiveStreamObject.h"

static CGFloat const liveMarkWidth = 47.0f;
static CGFloat const liveMarkHeight = 17.0f;
static CGFloat const liveMarkGap = 8.0f;

@interface GridWithTypeView()
@property (nonatomic,strong) UIImageView *postImageView;
@property (nonatomic,strong) UIImageView *videoImageView;
@property (nonatomic,strong) UIImageView *liveMarkImageView;
@property (nonatomic,strong) NSString *currentImageUrl;

@end

@implementation GridWithTypeView

#define videoWidth  25

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _currentImageUrl = @"";

        _postImageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _postImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        _videoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetWidth(self.bounds)-videoWidth,
                                                                       0,
                                                                       videoWidth,
                                                                       videoWidth)];

        _videoImageView.contentMode = UIViewContentModeScaleAspectFit;
        _liveMarkImageView = [[UIImageView alloc]initWithFrame:CGRectMake(liveMarkGap,
                                                                          liveMarkGap,
                                                                          liveMarkWidth,
                                                                          liveMarkHeight)];
        _liveMarkImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [self.videoImageView setImage:IMAGE_FROM_BUNDLE(@"video_1")];
        _liveMarkImageView.image = [UIImage imageNamed:@"onlive"];

        [self addSubview:_postImageView];
        [self addSubview:_videoImageView];
        [self addSubview:_liveMarkImageView];
    }
    return self;
}

-(void)reloadCellWithType:(id)post
{
    if (!post) {
        [self.postImageView setImage:IMAGE_FROM_BUNDLE(@"placehold_s")];
    }
    else if ([post isKindOfClass:[PostObject class]]) {
        
        PostObject *postObj = (PostObject *)post;
        
        if ([postObj.user isPrivacyMode]&&postObj.user.isFollowing==0&&![postObj.user.userID isEqualToString:GET_DEFAULT(USER_ID)]) {
            [_postImageView setImage:[UIImage imageNamed:@"lock"]];
        }
        else {
            [_postImageView setImageWithURL:S3_THUMB_IMAGE_URL(postObj.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_s")];
        }
        
        _videoImageView.hidden = ![postObj.type isEqualToString:@"video"];
        _liveMarkImageView.hidden = YES;
        
    }
    else if ([post isKindOfClass:[LiveStreamObject class]]) {
        LiveStreamObject *liveStreamObj = (LiveStreamObject *)post;
        
        [_postImageView setImageWithURL:S3_THUMB_IMAGE_URL(liveStreamObj.user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_s")];
        
        _videoImageView.hidden = YES;
        _liveMarkImageView.hidden = NO;
    }
    
}


@end
