//
//  PostGridCell.m
//  Story17
//
//  Created by Racing on 2015/12/31.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "PostGridCell.h"
#import "PostObject.h"
#import "LiveStreamObject.h"

@interface PostGridCell()
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UIImageView *videoTagView;
@property (weak, nonatomic) IBOutlet UIImageView *liveTagView;

@end

@implementation PostGridCell

- (void)awakeFromNib {
    // Initialization code
}

#pragma mark - Actions

- (void)setPost:(id)post
{
    _post = post;
    
    if ([post isKindOfClass:[LiveStreamObject class]]) {
        LiveStreamObject *liveStreamObj = (LiveStreamObject *)post;
        
        [_coverImageView setImageWithURL:S3_FILE_URL(liveStreamObj.user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
        
        _videoTagView.hidden = YES;
        _liveTagView.hidden = NO;
    }
    else if ([post isKindOfClass:[PostObject class]]) {
        PostObject *postObj = (PostObject *)post;
        
        [_coverImageView setImageWithURL:S3_FILE_URL(postObj.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
        
        _videoTagView.hidden = ![postObj.type isEqualToString:@"video"];
        _liveTagView.hidden = YES;
    }
}

@end
