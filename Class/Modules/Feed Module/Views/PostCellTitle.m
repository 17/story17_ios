//
//  PostCellTitle.m
//  Story17
//
//  Created by POPO on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "PostCellTitle.h"
#import "Constant.h"

#define PostCellTitleLabelWidth 120

@implementation PostCellTitle

#pragma mark - Initialize

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _views = 0;
        _money = 0;
        _timestamp = 0;
        
        _viewsLabel = [[UILabel alloc] init];
        _viewsLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_viewsLabel setTextAlignment:NSTextAlignmentRight];
        [_viewsLabel setTextColor:GRAY_COLOR];
        [self addSubview:_viewsLabel];
        
        _moneyLabel = [[UILabel alloc] init];
        [_moneyLabel setTextAlignment:NSTextAlignmentRight];
        _moneyLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_moneyLabel setTextColor:GRAY_COLOR];
        [self addSubview:_moneyLabel];
        
        _followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _followBtn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
        [_followBtn setBackgroundImage:[[UIImage imageNamed:@"btn_grayline"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        [_followBtn setTitle:LOCALIZE(@"+Follow") forState:UIControlStateNormal];
        [_followBtn setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
        

        _followBgView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-FOLLOW_BTN_WIDTH-20,0, FOLLOW_BTN_WIDTH+20, 55)];
        [_followBgView addSubview:_followBtn];
        _followBgView.userInteractionEnabled = YES;
        [_followBgView addSubview:_followBtn];
        _followBgView.hidden = YES;

        [self addSubview:_followBgView];
        
        [self setFrame:frame];

    }
    
    return self;
}

#pragma mark - Override

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    if (!_viewsLabel) {
        return;
    }

    float labelHeight = (frame.size.height - 15) / 2;
    CGRect rect = CGRectMake(frame.size.width-(PostCellTitleLabelWidth+10), 5, PostCellTitleLabelWidth, labelHeight);
    [_viewsLabel setFrame:rect];
    
    rect.origin.y = rect.origin.y + rect.size.height;
    [_moneyLabel setFrame:rect];
    
    rect.size.width -= 20;
    rect.origin.y -= 10;
    rect.origin.x += 10;
    _followBtn.frame = CGRectMake(10, rect.origin.y, rect.size.width, rect.size.height);
    
    _followBgView.frame = CGRectMake(rect.origin.x, 0, SCREEN_WIDTH-rect.origin.x, 55);
    
    /* UserTitleView Settings  */
    _userTitle.font = BOLD_FONT_WITH_SIZE(14);
    _userSubTitle.frame = CGRectMake(_userSubTitle.frame.origin.x+5,
                                     _userSubTitle.frame.origin.y+3,
                                     _userSubTitle.frame.size.width,
                                     _userSubTitle.frame.size.height);
    
}

- (void)setTimestamp:(int)timestamp
{
    _timestamp = timestamp;
    self.subTitle = [SINGLETON getElaspsedTimeString:timestamp];
}

- (void)setViews:(int)views
{
    _views = views;
    _viewsLabel.text = [NSString stringWithFormat:@"%d %@", views, LOCALIZE(@"Views")];
}

- (void)setMoney:(float)money
{
    _money = money;
    _moneyLabel.text = [NSString stringWithFormat:LOCALIZE(@"revenues_with_type"), money*[SINGLETON getCurrencyRate], [SINGLETON getCurrencyType]];
    
}

- (UIImageView *)userImage
{
    return _userImage;
}

@end
