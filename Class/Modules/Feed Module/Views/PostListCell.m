//
//  PostCell.m
//  Story17
//
//  Created by POPO on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "PostListCell.h"
#import "CommentViewController.h"
#import "UIImageView+AFNetworking.h"
#import "UserProfileViewController.h"
#import "BrowsePostsViewController.h"
#import "UserListViewController.h"
#import "LoginHandler.h"
#import "PostCellTitle.h"

#define PostCellUIPadding 5

#define PostCellUserTitleHeight 60
#define PostCellCommentTextViewHeight 60
#define PostCellCommentTextViewMaxHeight 8000

#define PostCellBtnWidth 30
#define PostCellBtnHight 30

#define CountLabelWidth 40
#define CountLabelHeight 20

#define CommentFontSize 14

//static CommentViewController* commCtrl = nil;
const int PostCellDefaultHeight = PostCellUserTitleHeight + CountLabelHeight+  PostCellBtnHight + (4 * PostCellUIPadding);

@implementation PostListCell

@synthesize post = _post;

+ (float)getCellHeigth
{
    
    return PostCellDefaultHeight + SCREEN_WIDTH;
}

+ (float)getCellHeigthWithPost:(PostObject*)post
{

    CGSize rStringSize = [post.caption getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-20,PostCellCommentTextViewMaxHeight) withFont:SYSTEM_FONT_WITH_SIZE(CommentFontSize)];

    CGSize stringSize = rStringSize;
   
    return PostCellDefaultHeight + SCREEN_WIDTH +stringSize.height ;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _post = nil;
        
        self.postTitleView = [[PostCellTitle alloc] init];
        [self.contentView addSubview:self.postTitleView];

//        self.moviePlayer = [MPMoviePlayerController new];
//        self.moviePlayer.controlStyle = MPMovieControlStyleNone;
//        [self.moviePlayer setRepeatMode:MPMovieRepeatModeOne];
//        self.moviePlayer.view.userInteractionEnabled =NO;
//        
//        _videoView = self.moviePlayer.view;
        
        _videoView.hidden = YES;
        
        _postTitleView.followBtn .alpha = 1.0f;
        
        __weak PostListCell* weakself = self;
        [_postTitleView.followBtn bk_addEventHandler:^(id sender) {
            if ([LoginHandler isGuest]) {
                [LoginHandler handleUserLogin];
                return;
            }
          
            if([GET_DEFAULT(FOLLOWING_COUNT) intValue] >= MAX_FOLLOWER_COUNT){
                [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"cannot_follow_user") message:[NSString stringWithFormat:LOCALIZE(@"max_followcount"),MAX_FOLLOWER_COUNT] buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                    if(okClicked){
                        
                    }
                }];
            }else{
                [weakself.postTitleView.followBtn addBounceAnimation:1.05 andDuration:0.2 withCompletion:^(BOOL success) {
                }];
                [UIView animateWithDuration:0.2 animations:^{
                    weakself.postTitleView.followBtn .alpha = 0.0f;
                }];
                weakself.post.user.isFollowing = 1;
                [API_MANAGER followUserAction:weakself.post.user.userID withCompletion:^(BOOL success) {
                    if(success){

                    }
                }];
            }
            
        } forControlEvents:UIControlEventTouchUpInside];
        
        self.postImageView = [[UIImageView alloc] init];
        self.postImageView.userInteractionEnabled = YES;

        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickPostImage:)];
        [self.postImageView addGestureRecognizer:tapGesture];
        [self.contentView addSubview:self.postImageView];
        
        self.videoImageView  = [[UIImageView alloc]init];
        self.videoImageView.hidden = YES;
        [self.videoImageView setImage:[UIImage imageNamed:@"video_1"]];
        [self.contentView addSubview:self.videoImageView];
        _videoImageView.userInteractionEnabled = YES;

        self.conmmentBtn = [[UIButton alloc] init];
        [self.contentView addSubview:self.conmmentBtn];
        
        
        self.likeBtn = [[UIButton alloc] init];
        [self.contentView addSubview:self.likeBtn];
        
        self.moreBtn = [[UIButton alloc] init];
        [self.contentView addSubview:self.moreBtn];
        
        self.likeLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.likeLabel];
        
        self.commentLabel = [[UILabel alloc] init];
//        [self.commentLabel setBackgroundColor:TIFFINY_BLUE_COLOR];
        [self.contentView addSubview:self.commentLabel];

        self.likeLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        self.commentLabel.font = SYSTEM_FONT_WITH_SIZE(12);
        
        self.likeLabel.textAlignment = NSTextAlignmentLeft;
        self.commentLabel.textAlignment = NSTextAlignmentCenter;
        
        [self.likeLabel setTextColor:GRAY_COLOR];
        [self.commentLabel setTextColor:GRAY_COLOR];
        
        self.separatorLine = [ThemeManager separaterLine];
        [self.contentView addSubview:self.separatorLine];

        
        self.commentTextView = [[STTweetLabel alloc] init];
        self.commentTextView.textAlignment = NSTextAlignmentLeft;
        self.commentTextView.numberOfLines = 0;
        self.commentTextView.font = SYSTEM_FONT_WITH_SIZE(CommentFontSize);
        self.commentTextView.text = [NSString stringWithFormat:@"Test1\nTest2\nTest3 ..."];
        [_commentTextView setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                          NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(CommentFontSize)}
                                hotWord:STTweetHandle];
        [_commentTextView setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                          NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(CommentFontSize)}
                                hotWord:STTweetHashtag];
        [_commentTextView setAttributes:@{NSForegroundColorAttributeName: GRAY_COLOR,
                                          NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(CommentFontSize)}];
        
        [self addSubview:self.commentTextView];
        
        self.postTitleView.title = @"Test User";
        [self setBackgroundColor:[UIColor whiteColor]];
        
        CGRect rect = CGRectMake(0, 0, frame.size.width, [PostCellTitle getDefaultHeight]);
        [self.postTitleView setFrame:rect];
        
        rect.origin.x = 0;
        rect.origin.y =  PostCellUserTitleHeight;
        rect.size.width = frame.size.width;
        rect.size.height = frame.size.width;
        [self.postImageView setFrame:rect];

        [self.videoImageView setFrame:CGRectMake(SCREEN_WIDTH-45, rect.origin.y, 40, 40)];

        rect.origin.x = 10;
        rect.origin.y += rect.size.height+PostCellUIPadding;
        rect.size.width = frame.size.width-20;
        rect.size.height = PostCellCommentTextViewHeight;
        [self.commentTextView setFrame:rect];
        
        rect.origin.x = 10;
        rect.origin.y = self.commentTextView.frame.origin.y + self.commentTextView.frame.size.height + PostCellUIPadding;
        rect.size.width = SCREEN_WIDTH-20;
        rect.size.height = CountLabelWidth;
        [self.likeLabel setFrame:rect];
        
        rect.origin.x = 5+CountLabelWidth;
        [self.commentLabel setFrame:rect];
        
        rect.origin.x = PostCellUIPadding;
        rect.origin.y = self.commentLabel.frame.origin.y + self.commentLabel.frame.size.height;
        rect.size.width = SCREEN_WIDTH-2*PostCellUIPadding;
        rect.size.height = 0.5;
        [self.separatorLine setFrame:rect];
        
        
        rect.origin.x = 2*PostCellUIPadding;
        rect.origin.y = self.commentTextView.frame.origin.y + self.commentTextView.frame.size.height + PostCellUIPadding;
        rect.size.width = PostCellBtnWidth;
        rect.size.height = PostCellBtnHight;
        
        [self.likeBtn setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        [self.likeBtn setFrame:rect];
        UITapGestureRecognizer* likeTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickPostImage:)];
        [self.likeBtn addGestureRecognizer:likeTapGesture];

        rect.origin.x = PostCellBtnWidth + 4*PostCellUIPadding;
        [self.conmmentBtn setImage:[UIImage imageNamed:@"comment"] forState:UIControlStateNormal];
        [self.conmmentBtn setFrame:rect];
        [self.conmmentBtn addTarget:self action:@selector(clickCommentBtn) forControlEvents:UIControlEventTouchUpInside];
        
        rect.origin.x = frame.size.width - (2*PostCellUIPadding + PostCellBtnWidth);
        [self.moreBtn setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
        [self.moreBtn setFrame:rect];
        [self.moreBtn addTarget:self action:@selector(clickMoreBtn) forControlEvents:UIControlEventTouchUpInside];
        
        UITapGestureRecognizer* tag = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickTitleImage)];
        [self.postTitleView addGestureRecognizer:tag];
        [self.postTitleView setUserInteractionEnabled:YES];
    
        [self.contentView addSubview:_postImageView];

        _loadingView = [UIActivityIndicatorView new];
        _loadingView.center = _postImageView.center;
        _loadingView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        _loadingView.color = MAIN_COLOR;
        
        [self.contentView addSubview:_loadingView];
        [self.contentView addSubview:_videoView];
        [self.contentView addSubview:_videoImageView];
        
    }
    
    return self;
}

- (void)setPost:(PostObject *)post
{
    _post = post;
    
    
    self.postTitleView.timestamp = _post.timestamp;
    self.postTitleView.title = _post.user.openID;
    self.postTitleView.image = _post.user.picture;
    self.postTitleView.views = _post.viewCount;
    self.postTitleView.money = _post.totalRevenue;
    
    if([_post.type isEqualToString:@"video"]){
        
        _videoImageView.hidden = NO;
        self.loadingView.center = _postImageView.center;
        
        [self.postImageView setImageWithURL:S3_FILE_URL(_post.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
//        [self.postImageView my_setImageWithURL:_post.picture placeholderImage:[UIImage imageNamed:@"placehold_l"] isThumbnail:NO];
        
        if(![DB_MANAGER fileExist:_post.video]){
            DLog(@"startAnimating");
            [self.loadingView startAnimating];
        }

        [DB_MANAGER downloadVideo:_post.video withCallback:^(BOOL success, NSString *status) {
            if(success){

                // async
//                [self prepareToStartAVPlayer];
                if ([self isCellVisible]) {
                    [self startAVPlayer];
                }
                
//                [self.loadingView stopAnimating];
//                [self.delegate reloadCellPlayer:self];
            }else{
            
            }
        }];
        
        _videoImageView.hidden = NO;
        [_videoImageView setImage:IMAGE_FROM_BUNDLE(@"video_1")];
        
    }else if([_post.type isEqualToString:@"image"]){
        
        _videoImageView.hidden = YES;
        _videoView.hidden = YES;
        self.loadingView.hidden = YES;
        
        
//        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:S3_FILE_URL(_post.picture)
//                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
//                                                  timeoutInterval:60];
//        [self.postImageView setImageWithURLRequest:imageRequest placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
////            dispatch_async(dispatch_get_main_queue(), ^{
////                [self.postImageView setImage:image]; //set image in main thread , we got downloaded image as parameter in block
////            });
////            
//            
//        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//            
//            if (error)
//            {
//                NSLog(@"Error:   %@", error);
//                NSLog(@"Error response:     %@", response);
//            }
//            
//        }];
            
        [self.postImageView setImageWithURL:S3_FILE_URL(_post.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
        
//        [self.postImageView my_setImageWithURL:_post.picture placeholderImage:[UIImage imageNamed:@"placehold_l"] isThumbnail:NO];
    }

    if(_post.user.isVerified==1){
        [_postTitleView verified:YES];
    }else{
        [_postTitleView verified:NO];
    }
    
    if(_post.user.isChoice==1){
        [_postTitleView choiced:YES];
    }else{
        [_postTitleView choiced:NO];
    }
    
    if(_post.user.crossGreatWall==1){
        [_postTitleView chinaFlag:YES];
    }else{
        [_postTitleView chinaFlag:NO];
    }
    
    if(_post.user.isInternational==1){
        [_postTitleView internationalFlag:YES];
    }else{
        [_postTitleView internationalFlag:NO];
    }
    
    if([_post.user.userID isEqualToString:MY_USER_ID]){
        self.postTitleView.viewsLabel.hidden =NO;
        self.postTitleView.moneyLabel.hidden = NO;
        self.postTitleView.viewsLabel.hidden = NO;
    }else{
        self.postTitleView.moneyLabel.hidden = YES;
        self.postTitleView.viewsLabel.hidden = YES;
    }
    
    NSString* likeCountText = [NSString stringWithFormat:@"%d",_post.likeCount ];
    NSString* commentCountText = [NSString stringWithFormat:@"%d",_post.commentCount ];

    [self.likeLabel setText:[NSString stringWithFormat:@"%@ %@ ",likeCountText,LOCALIZE(@"post_likes")]];
    [self.commentLabel setText:[NSString stringWithFormat:@". %@ %@ ",commentCountText,LOCALIZE(@"post_comments")]];
    self.likeLabel.userInteractionEnabled = YES;
    [self.likeLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickPostLiker)]];
    self.commentLabel.userInteractionEnabled = YES;
    [self.commentLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickCommentBtn)]];

    
    if (_post.isLiked==1)
        [self.likeBtn setImage:[UIImage imageNamed:@"like_down"] forState:UIControlStateNormal];
    else
        [self.likeBtn setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    
    self.commentTextView.text = _post.caption;
    
    CGSize stringSize = VIEW_SIZE([_commentTextView suggestedFrameSizeToFitEntireStringConstrainedToWidth:(SCREEN_WIDTH-20)]);
    
    self.commentTextView.frame = CGRectMake(_commentTextView.frame.origin.x, _commentTextView.frame.origin.y, stringSize.width, stringSize.height);

    
    [self.likeLabel setFrame:CGRectMake(_likeLabel.frame.origin.x, _commentTextView.frame.origin.y+_commentTextView.frame.size.height+PostCellUIPadding, SCREEN_WIDTH-10, CountLabelHeight)];
    [self.commentLabel setFrame:CGRectMake(_commentLabel.frame.origin.x, _commentTextView.frame.origin.y+_commentTextView.frame.size.height+PostCellUIPadding, CountLabelWidth, CountLabelHeight)];
    [self.separatorLine setFrame:CGRectMake(2*PostCellUIPadding, self.commentLabel.frame.origin.y+self.commentLabel.frame.size.height, SCREEN_WIDTH-4*PostCellUIPadding, 0.5)];
    
    [self labelSizeToFit];

    
    [self.likeBtn setFrame:CGRectMake(_likeBtn.frame.origin.x, _separatorLine.frame.origin.y+_separatorLine.frame.size.height+PostCellUIPadding, PostCellBtnWidth, PostCellBtnHight)];
    [self.moreBtn setFrame:CGRectMake(_moreBtn.frame.origin.x, _separatorLine.frame.origin.y+_separatorLine.frame.size.height+PostCellUIPadding, PostCellBtnWidth, PostCellBtnHight)];
    [self.conmmentBtn setFrame:CGRectMake(_conmmentBtn.frame.origin.x, _separatorLine.frame.origin.y+_separatorLine.frame.size.height+PostCellUIPadding, PostCellBtnWidth, PostCellBtnHight)];

    if(_navCtrl==nil)
        return;
    
    __weak UINavigationController* nav = _navCtrl;
    [_commentTextView setDetectionBlock:^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range) {
        
        if([GET_DEFAULT(IN_SANDBOX) isEqualToString:@"1"]){
            return;
        }
        
        NSString* userOpenID = [string substringToIndex:1];
        if([[string substringFromIndex:1] isEqualToString:GET_DEFAULT(USER_OPEN_ID)])
            return;
        
        NSString* keyword = [string substringFromIndex:1];
        
        if([userOpenID isEqualToString:@"@"]){
            
            UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
            [userCtrl getUserWithOpenID:keyword];
            userCtrl.hidesBottomBarWhenPushed = YES;
            [nav pushViewController:userCtrl animated:YES];

        }else if([[string substringToIndex:1] isEqualToString:@"#"]){
            
            if([GET_DEFAULT(IN_SANDBOX) isEqualToString:@"1"]){
                return;
            }
            
            BrowsePostsViewController* broVC =[BrowsePostsViewController new];
            broVC.tagQuery = keyword;
            broVC.title = string;
            broVC.hidesBottomBarWhenPushed = YES;
            [nav pushViewController:broVC animated:YES];
        
        }else{
            
            [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"open_on_safari") message:string buttonText:LOCALIZE(@"OK") cancelable:YES withCompletion:^(BOOL okClicked) {
                if(okClicked){
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
                }
            }];
        }
    }];

}


-(void)labelSizeToFit
{
    [self.likeLabel sizeToFit];
    self.commentLabel.frame = CGRectMake(self.likeLabel.frame.origin.x+self.likeLabel.frame.size.width+5, self.likeLabel.frame.origin.y, self.commentLabel.frame.size.width, self.commentLabel.frame.size.height);
    [self.commentLabel sizeToFit];

}


#pragma mark - Button action
- (void)clickCommentBtn
{
    if ([LoginHandler isGuest]) {
        [LoginHandler handleUserLogin];
        return;
    }
    
    if (self.navCtrl == nil)
        return;
    
    CommentViewController* commCtrl = [CommentViewController new];
    commCtrl.title = @"Comments";
    commCtrl.hidesBottomBarWhenPushed = YES;
    commCtrl.post = _post;
    
    [self.navCtrl pushViewController:commCtrl animated:YES];
    
}

- (void)clickMoreBtn
{
    if ([LoginHandler isGuest]) {
        [LoginHandler handleUserLogin];
        return;
    }
    
    [_delegate didClickMore:self];
}

- (void)clickTitleImage
{
    
    if (self.navCtrl == nil || [_post.user.userID isEqualToString:MY_USER_ID]){
        [_postTitleView shake:5 withDelta:4 speed:0.05 shakeDirection:ShakeDirectionHorizontal];
        return;
    }
    
    UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
    userCtrl.user = _post.user;
    [userCtrl getUserWithOpenID:_post.user.openID];
    userCtrl.hidesBottomBarWhenPushed = YES;
    [self.navCtrl pushViewController:userCtrl animated:YES];
}

-(void)didClickPostImage:(UITapGestureRecognizer *)tapRecognizer
{
    if ([LoginHandler isGuest]) {
        [LoginHandler handleUserLogin];
        return;
    }
    
    [self.likeBtn setImage:[UIImage imageNamed:@"like_down"] forState:UIControlStateNormal];
    
    [_delegate didClickPostImage:tapRecognizer withPostID:_post.postID];
    
    
    if(![_post.user.userID isEqualToString:MY_USER_ID]){
        
        _post.isLiked = 1;
        _post.likeCount += 1;
   
        NSString* likeCountText = [NSString stringWithFormat:@"%d",_post.likeCount ];
        NSString* commentCountText = [NSString stringWithFormat:@"%d",_post.commentCount ];
        
        [LIKE_HANDLER likePost:_post.postID];
        
        [self.likeLabel setText:[NSString stringWithFormat:@"%@ %@ ",likeCountText,LOCALIZE(@"post_likes")]];
        [self.commentLabel setText:[NSString stringWithFormat:@" %@ %@ ",commentCountText,LOCALIZE(@"post_comments")]];
    
    }else if(_post.isLiked==0){
        
        _post.isLiked = 1;
        _post.likeCount += 1;

        NSString* likeCountText = [NSString stringWithFormat:@"%d",_post.likeCount ];
        NSString* commentCountText = [NSString stringWithFormat:@"%d",_post.commentCount ];
        
        [LIKE_HANDLER likePost:_post.postID];
        
        [self.likeLabel setText:[NSString stringWithFormat:@"%@ %@ ",likeCountText,LOCALIZE(@"post_likes")]];
        [self.commentLabel setText:[NSString stringWithFormat:@" %@ %@ ",commentCountText,LOCALIZE(@"post_comments")]];

    }
    
    [self labelSizeToFit];
}

-(void)clickPostLiker
{
    if(![_post.user.userID isEqualToString:MY_USER_ID ] && [GET_DEFAULT(IS_ADMIN) intValue]<1)
        return;
    
    UserListViewController* ulVC  = [UserListViewController new];
    ulVC.mode = LIKER_MODE;
    ulVC.likesPost = _post;
    ulVC.title = LOCALIZE(@"PostLiker");
    ulVC.hidesBottomBarWhenPushed = YES;
    [_navCtrl pushViewController:ulVC animated:YES];

}

-(void)showFollowerButton
{
    if([_post.user.userID isEqualToString:MY_USER_ID]){
        return;
    }
    
    [API_MANAGER getUserInfo:_post.user.openID completion:^(BOOL success, UserObject *userObject) {
        if(success){
            if(userObject.isFollowing==0 && userObject.followRequestTime==0){
                _postTitleView.followBgView.alpha = 0.0f;
                _postTitleView.followBgView.hidden = NO;
                [UIView animateWithDuration:0.3 animations:^{
                    _postTitleView.followBgView.alpha = 1.0f;
                }];
            }else{
                _postTitleView.followBgView.hidden = YES;
            }
        }else{
        }
    }];

}

#pragma mark - cell control
- (BOOL)isCellVisible
{
    CGPoint oriInWin = [self.superview convertPoint:self.frame.origin toView:nil];
    if (oriInWin.y < SCREEN_HEIGHT-TAB_BAR_HEIGHT-[PostCellTitle getDefaultHeight]
        &&
        oriInWin.y > 0-(self.postImageView.frame.size.height-STATUS_BAR_HEIGHT-NAVI_BAR_HEIGHT)-[PostCellTitle getDefaultHeight]) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - AV player
- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

/* bug when using async AVPlayer. Do not know why now */
- (void)asyncStartAVPlayer
{
    if(_post.video==nil || [_post.video isEqualToString:@""] || ![_post.type isEqualToString:@"video"])
        return;
    
    if (_avPlayer.rate != 0) {
        return;
    }
    
    if(![DB_MANAGER fileExist:_post.video])
        return;
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[[NSURL alloc] initFileURLWithPath:GET_LOCAL_FILE_PATH(_post.video)] options:nil];
    NSArray *keys = [NSArray arrayWithObject:@"playable"];
    [asset loadValuesAsynchronouslyForKeys:keys completionHandler:^{
        _avPlayer = [AVPlayer playerWithPlayerItem:[AVPlayerItem playerItemWithAsset:asset]];
        _videoLayer = [AVPlayerLayer playerLayerWithPlayer:_avPlayer];
        _avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        _videoLayer.frame = _postImageView.bounds;
        [_postImageView.layer addSublayer:_videoLayer];
        [NOTIFICATION_CENTER addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:[_avPlayer currentItem]];
        [self.loadingView stopAnimating];
        
        [_avPlayer play];
    }];
}

- (void)startAVPlayer
{
//    DLog(@"startAVPlayer for cell %f", self.frame.origin.y);
    if(_post.video==nil || [_post.video isEqualToString:@""] || ![_post.type isEqualToString:@"video"])
        return;

    if (_avPlayer.rate != 0) {
        return;
    }
    
    if(![DB_MANAGER fileExist:_post.video])
        return;
    
    DLog(@"starREALAVPl for cell %f at rate %f", self.frame.origin.y, _avPlayer.rate);
    NSURL *movieURL = [[NSURL alloc] initFileURLWithPath:GET_LOCAL_FILE_PATH(_post.video)];
    _avPlayer = [AVPlayer playerWithURL:movieURL];
    _videoLayer = [AVPlayerLayer playerLayerWithPlayer:_avPlayer];
    _videoLayer.frame = _postImageView.bounds;
    [_postImageView.layer addSublayer:_videoLayer];
    _avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    [NOTIFICATION_CENTER addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:[_avPlayer currentItem]];
    
    [self.loadingView stopAnimating];
    [_avPlayer play];
}

- (void)stopAVPlayer
{
//    DLog(@"stopAVPlayer for cell %f", self.frame.origin.y);
    self.videoView.hidden = YES;
    
    if (_avPlayer.rate == 0) {
        return;
    }
    
    DLog(@"stopREALAVPl for cell %f at rate %f", self.frame.origin.y, _avPlayer.rate);
    [_avPlayer pause];
    _avPlayer.rate = 0;
    [_avPlayer seekToTime:kCMTimeZero];
    [_videoLayer removeFromSuperlayer];
    [NOTIFICATION_CENTER removeObserver:self];
}

//-(void)startPlayer
//{
//    if(_post.video==nil || [_post.video isEqualToString:@""] || ![_post.type isEqualToString:@"video"])
//        return;
//    if(self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying)
//        return;
//    
//    if(![DB_MANAGER fileExist:_post.video])
//        return;
//    
//    NSURL *movieURL = [[NSURL alloc] initFileURLWithPath:GET_LOCAL_FILE_PATH(_post.video)];
//    
//    [self.moviePlayer setContentURL:movieURL];
//    [self.moviePlayer prepareToPlay];
//    [self.moviePlayer play];
//    self.moviePlayer.view.frame = _postImageView.frame;
//    
//    [NOTIFICATION_CENTER addObserver:self
//                            selector:@selector(MPMoviePlayerLoadStateDidChangeNotification)
//                                name:MPMoviePlayerLoadStateDidChangeNotification
//                              object:nil];
//}
//
//-(void)stopPlayer
//{
//    
//    self.videoView.hidden = YES;
//    if(self.moviePlayer.playbackState != MPMoviePlaybackStatePlaying){
//        return;
//    }
//    
//    /* avoid delay caused by stop action */
//    dispatch_async(GLOBAL_QUEUE, ^{
//        [self.moviePlayer stop];
//        [NOTIFICATION_CENTER removeObserver:self];
//    });
//}

//- (void)MPMoviePlayerLoadStateDidChangeNotification{
//
//    /* Avoid Player Black Flash */
//    if(self.moviePlayer.loadState & (MPMovieLoadStatePlayable | MPMovieLoadStatePlaythroughOK))
//    {
//        if(![_post.type isEqualToString:@"video"])
//            return;
//    
//        _videoView.hidden = NO;
//    
//    }
//}


@end
