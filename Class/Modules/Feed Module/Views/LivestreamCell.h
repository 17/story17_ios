#import <UIKit/UIKit.h>
#import "LiveStreamObject.h"
#import "PostCellTitle.h"
#import "STTweetLabel.h"
#import "VP8VideoDecoder.h"
#import "LiveStreamConnectionWatcher.h"

@class LivestreamCell;

@protocol LivestreamCellDelegate <NSObject>
@optional
-(void)didClickLiveMore:(LivestreamCell*)livestreamCell;
@end

@interface LivestreamCell : UICollectionViewCell

typedef NS_ENUM(NSInteger, HotLiveStreamMode){
    HotLiveStreamModeSingle,
    HotLiveStreamModeShow
};

typedef NS_ENUM(NSInteger, liveMode){
    HotLive,
    Followed,
    Restreamed,
    Feed
};
@property int8_t hotLiveStreamMode;
@property (nonatomic, weak) LiveStreamObject* livestream;
@property (nonatomic, weak) UINavigationController* navCtrl;
@property (nonatomic, weak) id<LivestreamCellDelegate>delegate;
@property (nonatomic, strong) UIImageView* livestreamImageView;
@property int8_t liveMode;//hotlive ; followed; restream;
// Live Preview
@property BOOL needGrabLiveData;

+ (float)getHeightFromLivestream:(LiveStreamObject*)livestream;

@end
