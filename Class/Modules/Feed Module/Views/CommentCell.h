//
//  CommentCell.h
//  Story17
//
//  Created by POPO on 2015/5/5.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentObject.h"
#import "CommentTitleView.h"
#import "STTweetLabel.h"
#import "PostObject.h"

@class CommentCell;

@protocol CommentCellDelegate <NSObject>

- (void)cell:(CommentCell *)cell didShowMenu:(BOOL)isShowingMenu;
- (void)cellDidSelectDelete:(CommentCell *)cell;
- (void)cellDidSelectRepost:(CommentCell *)cell;
- (void)cellDidSelectReport:(CommentCell *)cell;

@end

@interface CommentCell : UITableViewCell <UIScrollViewDelegate>
{
@private
    UILabel* _timestamp;
}

@property(nonatomic, weak)CommentObject* comment;
@property(nonatomic, strong)CommentTitleView* titleView;

+ (float)getHeight;
+ (float)getHeightFromCommentObj:(CommentObject*)commentObj;

@property(nonatomic, weak)UINavigationController* navCtrl;
@property (nonatomic, assign) BOOL isSelfPost;



@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *scrollViewContentView;
@property (nonatomic, strong) UIView *scrollViewButtonView;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) UIButton *moreButton;
@property (nonatomic, strong) UIButton *repostButton;
@property (nonatomic, assign) BOOL isShowingMenu;
@property (nonatomic, weak) id<CommentCellDelegate> delegate;

- (void)dismissCell;

@end
