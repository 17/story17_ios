//
//  RankTableViewCell.m
//  story17
//
//  Created by POPO Chen on 8/14/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "RankTableViewCell.h"
#import "UserObject.h"
#import "UIImageView+AFNetworking.h"

@implementation RankTableViewCell

#define RANK_CELL_HEIGHT 55
#define USER_IMAGE_WIDTH 45
#define LEFT_MARGIN 5
#define FOLLOW_BUTTON_WIDTH 35

+(float)getRankCellHeight
{
    return RANK_CELL_HEIGHT;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self awakeFromNib];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)awakeFromNib {
    
    _userImageView = [UIImageView new];
    _maskImageView = [UIImageView new];
    [_maskImageView setImage:[UIImage imageNamed:@"head_mask"]];

    _verifyImageView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"verified")];
    [self addSubview:_verifyImageView];
    _verifyImageView.hidden = YES;
    
    _heartImageView = [ThemeManager randomHeartImage];
    _topImageView = [UIImageView new];

    _rankLabel = [UILabel new];
    _openIDLabel = [UILabel new];
    
    _rankLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    _openIDLabel.font = SYSTEM_FONT_WITH_SIZE(14);
    
    _userNameLabel = [UILabel new];
    _totalCountLabel = [UILabel new];
    _followButton = [UIButton new];
    [_followButton addTarget:self action:@selector(followAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _followBgView = [UIView new];
    _followBgView.userInteractionEnabled = YES;
    [_followBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(followAction:)]];

    _userNameLabel.font = SYSTEM_FONT_WITH_SIZE(12);
    _totalCountLabel.font = SYSTEM_FONT_WITH_SIZE(12);
    _bottomline = [ThemeManager separaterLine];
    _bottomline.frame = CGRectMake(10, RANK_CELL_HEIGHT-0.5, SCREEN_WIDTH-20, 0.5);
    
    [self.contentView addSubview:_userImageView];
    [self.contentView addSubview:_maskImageView];
    [self.contentView addSubview:_heartImageView];
    [self.contentView addSubview:_topImageView];
    [self.contentView addSubview:_rankLabel];
    [self.contentView addSubview:_openIDLabel];
    [self.contentView addSubview:_userNameLabel];
    [self.contentView addSubview:_totalCountLabel];
    [_followBgView addSubview:_followButton];
    [self.contentView addSubview:_followBgView];
    [self.contentView addSubview:_bottomline];

}


-(void)reloadCell:(UserObject*)user withIndex:(int)index;
{
    _user = user;

    float nextX = LEFT_MARGIN;
    
    if([_user.userID isEqualToString:MY_USER_ID]){
        _followBgView.hidden = YES;
    }else{
        _followBgView.hidden = NO;
    }
    
    if(index<3){
        [_topImageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"no%d",index+1]]];
        _topImageView.hidden = NO;
        _rankLabel.hidden = YES;
    }else{
        _topImageView.hidden = YES;
        _rankLabel.hidden = NO;
    }
    
    if(_user.isVerified==1){
        _verifyImageView.hidden = NO;
    }else{
        _verifyImageView.hidden = YES;
    }
    
    _topImageView.frame = CGRectMake(0, 0, USER_IMAGE_WIDTH-10, USER_IMAGE_WIDTH-5);
    [_userImageView setImageWithURL:S3_THUMB_IMAGE_URL(user.picture) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s")];
//    [_userImageView my_setImageWithURL:user.picture placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_profile_s") isThumbnail:YES];
    _userImageView.frame = CGRectMake(0, 0, USER_IMAGE_WIDTH, USER_IMAGE_WIDTH);
    _maskImageView.frame = _userImageView.frame;

    [_rankLabel setText:[NSString stringWithFormat:@"%d",index+1]];
    [_rankLabel sizeToFit];
    _rankLabel.center = CGPointMake(nextX+20, RANK_CELL_HEIGHT/2);
    _topImageView.center = _rankLabel.center;
    
    nextX += _topImageView.frame.size.width+2*LEFT_MARGIN;
    
    _userImageView.center = CGPointMake(nextX+USER_IMAGE_WIDTH/2, RANK_CELL_HEIGHT/2);
    _maskImageView.center = _userImageView.center;
    
    nextX += _userImageView.frame.size.width+2*LEFT_MARGIN;
    
    [_openIDLabel setText:_user.openID];
    [_openIDLabel sizeToFit];
    [_userNameLabel setText:_user.name];
    [_userNameLabel sizeToFit];
    
    _openIDLabel.center = CGPointMake(nextX+_openIDLabel.frame.size.width/2,  (RANK_CELL_HEIGHT-USER_IMAGE_WIDTH)/2+USER_IMAGE_WIDTH/4);
    _verifyImageView.frame = CGRectMake(_openIDLabel.frame.origin.x+_openIDLabel.frame.size.width, _openIDLabel.frame.origin.y, 15, 15);
    
//    _userNameLabel.center = CGPointMake(nextX+_userNameLabel.frame.size.width/2, (RANK_CELL_HEIGHT-USER_IMAGE_WIDTH)/2+3*USER_IMAGE_WIDTH/4);
    
    _followBgView.frame = CGRectMake(SCREEN_WIDTH-RANK_CELL_HEIGHT, 0, RANK_CELL_HEIGHT, RANK_CELL_HEIGHT);
    
    _followButton.frame = CGRectMake(0, 0, FOLLOW_BUTTON_WIDTH, FOLLOW_BUTTON_WIDTH);
    _followButton.center = CGPointMake(RANK_CELL_HEIGHT/2, RANK_CELL_HEIGHT/2);
    
    [_totalCountLabel setText:[NSString stringWithFormat:@"%d",_user.likeCount]];
    [_totalCountLabel sizeToFit];
    
    _heartImageView.frame = CGRectMake(0, 0, 15, 15);
    _heartImageView.center = CGPointMake(_followBgView.frame.origin.x-LEFT_MARGIN-_heartImageView.frame.size.width/2, _openIDLabel.center.y);
    _totalCountLabel.center = CGPointMake(_followBgView.frame.origin.x-LEFT_MARGIN-_totalCountLabel.frame.size.width/2,(RANK_CELL_HEIGHT-USER_IMAGE_WIDTH)/2+3*USER_IMAGE_WIDTH/4);
    _userNameLabel.frame = CGRectMake(nextX,_totalCountLabel.frame.origin.y, _totalCountLabel.frame.origin.x-nextX-5, _userNameLabel.frame.size.height);
    
    if(user.isFollowing==1){
        [_followButton setImage:IMAGE_FROM_BUNDLE(@"follow_down") forState:UIControlStateNormal];
        [_followButton setImage:IMAGE_FROM_BUNDLE(@"follow_down") forState:UIControlStateSelected];
    }else{
        [_followButton setImage:IMAGE_FROM_BUNDLE(@"follow") forState:UIControlStateNormal];
        [_followButton setImage:IMAGE_FROM_BUNDLE(@"follow") forState:UIControlStateSelected];
    }
}

-(void)followAction:(id)sender
{
    [_followButton playBounceAnimation];
    
    if(_user.isFollowing==1){
        _user.isFollowing=0;
        [_followButton setImage:IMAGE_FROM_BUNDLE(@"follow") forState:UIControlStateNormal];
        [_followButton setImage:IMAGE_FROM_BUNDLE(@"follow") forState:UIControlStateSelected];
        [API_MANAGER unfollowUserAction:_user.userID withCompletion:^(BOOL success) {}];
        
    }else{
        
        if([GET_DEFAULT(FOLLOWING_COUNT) intValue] >= MAX_FOLLOWER_COUNT){
            [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"cannot_follow_user") message:[NSString stringWithFormat:LOCALIZE(@"max_followcount"),MAX_FOLLOWER_COUNT] buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                if(okClicked){
                    
                }
            }];
        }else{
            _user.isFollowing=1;
            [_followButton setImage:IMAGE_FROM_BUNDLE(@"follow_down") forState:UIControlStateNormal];
            [_followButton setImage:IMAGE_FROM_BUNDLE(@"follow_down") forState:UIControlStateSelected];
            [API_MANAGER followUserAction:_user.userID withCompletion:^(BOOL success) {
                
            }];
        }
    }
    
    [SINGLETON reloadFollowStatus];
    
}



@end
