#import "LivestreamCell.h"
#import "UserProfileViewController.h"
#import "LiveStreamViewController.h"
#import "Constant.h"
#import "UIImageView+AFNetworking.h"
#import "BrowsePostsViewController.h"
#import "BlabViewController.h"

static CGFloat const kCaptionFontSize = 14.0f;

@interface LivestreamCell()

#define PostCellUIPadding 5
#define PostCellBtnHight 30
#define PostCellUserTitleHeight 60
#define PostCellCommentTextViewMaxHeight 500
#define CountLabelHeight 20
#define RestreamLabelHeight 30
#define LiveCoverImageGap 3

@property (nonatomic, strong) UIView* coverUIView;
@property (nonatomic, strong) UIImageView* hostImageView;
@property (nonatomic, strong) UIImageView* publisher1ImageView;
@property (nonatomic, strong) UIImageView* publisher2ImageView;
@property (nonatomic, strong) UIImageView* publisher3ImageView;

@property (nonatomic, strong) UIView* bgView;
@property (nonatomic, strong) UIActivityIndicatorView* loadingView;
@property (nonatomic, strong) PostCellTitle* postTitleView;
@property (nonatomic, strong) STTweetLabel* captionLabel;
@property (nonatomic, strong) UIButton* restreamButton;
@property (nonatomic, strong)UIButton* moreBtn;

@property (nonatomic, strong)UILabel* livestreamIDLabel;

@end



const int PostCellDefaultHeight = PostCellUserTitleHeight + (4 * PostCellUIPadding);

@implementation LivestreamCell

@synthesize needGrabLiveData;
@synthesize bgView;

+ (float)getHeightFromLivestream:(LiveStreamObject*)livestream
{
    float cellheight =0 ;
    
    CGSize rStringSize = [livestream.caption getSizeWithConstrainSize:CGSizeMake(SCREEN_WIDTH-20,PostCellCommentTextViewMaxHeight) withFont:SYSTEM_FONT_WITH_SIZE(14)];
    
    CGSize stringSize = VIEW_SIZE(rStringSize);

    cellheight = PostCellDefaultHeight + SCREEN_WIDTH+stringSize.height;
    
    if(![livestream.restreamerOpenID isEqualToString:@""]){
        cellheight += RestreamLabelHeight;
    }
    
    return cellheight ;
}

- (UIImageView*)getLiveUIImageView
{
    UIImageView* imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.masksToBounds = YES;
    imageView.hidden = YES;
    [imageView setBackgroundColor:MAIN_COLOR];
    [imageView setImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
    [_coverUIView addSubview:imageView];
    
    return imageView;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        _livestream = nil;
        
        self.contentView.backgroundColor = WHITE_COLOR;
        
        /* UI */
        self.restreamButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.restreamButton setBackgroundImage:[[UIImage imageNamed:@"btn_greenline"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
        [self.contentView addSubview:self.restreamButton];
        
        self.postTitleView = [[PostCellTitle alloc] init];
        [self.contentView addSubview:self.postTitleView];
       
        _coverUIView = [[UIView alloc] init];
        _coverUIView.userInteractionEnabled = YES;
        _coverUIView.contentMode = UIViewContentModeScaleAspectFill;
        _coverUIView.layer.masksToBounds = YES;
        [_coverUIView setBackgroundColor:WHITE_COLOR];
        
        UITapGestureRecognizer* coverGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickImage:)];
        [_coverUIView addGestureRecognizer:coverGesture];
        
        _hostImageView = [self getLiveUIImageView];
        _publisher1ImageView = [self getLiveUIImageView];
        _publisher2ImageView = [self getLiveUIImageView];
        _publisher3ImageView = [self getLiveUIImageView];
        
        self.livestreamImageView  = [[UIImageView alloc]init];
        
        self.postTitleView.title = @"Test User";
        [self setBackgroundColor:[UIColor whiteColor]];

        self.bgView.backgroundColor = BLACK_COLOR;
        
        self.captionLabel = [[STTweetLabel alloc] init];
        self.captionLabel.textAlignment = NSTextAlignmentLeft;
        self.captionLabel.numberOfLines = 0;
        [_captionLabel setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                       NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(kCaptionFontSize)}
                             hotWord:STTweetHandle];
        [_captionLabel setAttributes:@{NSForegroundColorAttributeName: MAIN_COLOR,
                                       NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(kCaptionFontSize)}
                             hotWord:STTweetHashtag];
        [_captionLabel setAttributes:@{NSForegroundColorAttributeName: DARK_GRAY_COLOR,
                                       NSFontAttributeName: SYSTEM_FONT_WITH_SIZE(kCaptionFontSize)}];

        UITapGestureRecognizer* tag = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickTitleImage)];
        [self.postTitleView addGestureRecognizer:tag];
        [self.postTitleView setUserInteractionEnabled:YES];
        
        _loadingView = [UIActivityIndicatorView new];
        _loadingView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        _loadingView.color = MAIN_COLOR;
        [_loadingView startAnimating];
        _loadingView.alpha = 0.0;
        
        self.moreBtn =  [UIButton new];
        [self.moreBtn setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
        [self.moreBtn addTarget:self action:@selector(clickMoreBtn) forControlEvents:UIControlEventTouchUpInside];
        _moreBtn.hidden = YES;
        
        _livestreamIDLabel = [UILabel new];
        [_livestreamIDLabel setBackgroundColor:MAIN_COLOR];
        _livestreamIDLabel.font = SYSTEM_FONT_WITH_SIZE(14);
        _livestreamIDLabel.textAlignment = NSTextAlignmentCenter;
        [_livestreamIDLabel setTextColor:WHITE_COLOR];
        
        _livestreamIDLabel.hidden = YES;
        
        [self.contentView addSubview:bgView];
        [self.contentView addSubview:_coverUIView];
        [self.contentView addSubview:_loadingView];
        [self.contentView addSubview:_captionLabel];
        [self.contentView addSubview:_livestreamImageView];
        [self.contentView addSubview:_moreBtn];
        
        #ifdef DEV_MODE
        [self.contentView addSubview:_livestreamIDLabel];
        #endif
        
    }
    
    return self;
}

- (void) joinImages:(NSArray*)imgArray toView:(UIView*)view
{
    _hostImageView.hidden = YES;
    _publisher1ImageView.hidden = YES;
    _publisher2ImageView.hidden = YES;
    _publisher3ImageView.hidden = YES;
    
    switch ([imgArray count]) {
        case 1:
        {
            _hostImageView.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
            [_hostImageView setImageWithURL:S3_FILE_URL([imgArray objectAtIndex:0]) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
            _hostImageView.hidden = NO;
            
            break;
        }
        case 2:
        {
            _hostImageView.frame = CGRectMake(0, 0, (view.frame.size.width-LiveCoverImageGap)/2, view.frame.size.height);
            [_hostImageView setImageWithURL:S3_FILE_URL([imgArray objectAtIndex:0]) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
            _hostImageView.hidden = NO;
            
            _publisher1ImageView.frame = CGRectMake((view.frame.size.width+LiveCoverImageGap)/2, 0, (view.frame.size.width-LiveCoverImageGap)/2, view.frame.size.height);
            [_publisher1ImageView setImageWithURL:S3_FILE_URL([imgArray objectAtIndex:1]) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
            _publisher1ImageView.hidden = NO;
            
            break;
        }
        case 3:
        {
            _publisher1ImageView.frame = CGRectMake(0, 0, (view.frame.size.width-LiveCoverImageGap)/2, (view.frame.size.height-LiveCoverImageGap)/2);
            [_publisher1ImageView setImageWithURL:S3_FILE_URL([imgArray objectAtIndex:1]) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
            _publisher1ImageView.hidden = NO;
            
            _publisher2ImageView.frame = CGRectMake(0, (view.frame.size.height+LiveCoverImageGap)/2, (view.frame.size.width-LiveCoverImageGap)/2, (view.frame.size.height-LiveCoverImageGap)/2);
            [_publisher2ImageView setImageWithURL:S3_FILE_URL([imgArray objectAtIndex:2]) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
            _publisher2ImageView.hidden = NO;
            
            _hostImageView.frame = CGRectMake((view.frame.size.width+LiveCoverImageGap)/2, 0, (view.frame.size.width-LiveCoverImageGap)/2, view.frame.size.height);
            [_hostImageView setImageWithURL:S3_FILE_URL([imgArray objectAtIndex:0]) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
            _hostImageView.hidden = NO;
            
            break;
        }
        case 4:
        {
            _hostImageView.frame = CGRectMake(0, 0, (view.frame.size.width-LiveCoverImageGap)/2, (view.frame.size.height-LiveCoverImageGap)/2);
            [_hostImageView setImageWithURL:S3_FILE_URL([imgArray objectAtIndex:0]) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
            _hostImageView.hidden = NO;
            
            _publisher1ImageView.frame = CGRectMake(0, (view.frame.size.height+LiveCoverImageGap)/2, (view.frame.size.width-LiveCoverImageGap)/2, (view.frame.size.height-LiveCoverImageGap)/2);
            [_publisher1ImageView setImageWithURL:S3_FILE_URL([imgArray objectAtIndex:1]) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
            _publisher1ImageView.hidden = NO;
            
            _publisher2ImageView.frame = CGRectMake((view.frame.size.width+LiveCoverImageGap)/2, 0, (view.frame.size.width-LiveCoverImageGap)/2, (view.frame.size.height-LiveCoverImageGap)/2);
            [_publisher2ImageView setImageWithURL:S3_FILE_URL([imgArray objectAtIndex:2]) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
            _publisher2ImageView.hidden = NO;
            
            _publisher3ImageView.frame = CGRectMake((view.frame.size.width+LiveCoverImageGap)/2, (view.frame.size.height+LiveCoverImageGap)/2, (view.frame.size.width-LiveCoverImageGap)/2, (view.frame.size.height-LiveCoverImageGap)/2);
            [_publisher3ImageView setImageWithURL:S3_FILE_URL([imgArray objectAtIndex:3]) placeholderImage:IMAGE_FROM_BUNDLE(@"placehold_l")];
            _publisher3ImageView.hidden = NO;
            
            break;
        }
            
        default:
        {
            break;
        }
    }
}

-(void)setLivestream:(LiveStreamObject *)livestream
{
    _livestream = livestream;

    if(_livestream.liveStreamIDInt!=livestream.liveStreamIDInt) {
        _loadingView.alpha = 1.0;
    }
    
    int nextY = 0;
    
    if(![_livestream.restreamerOpenID isEqualToString:@""]){
        _liveMode=Restreamed;
        self.restreamButton.frame = CGRectMake(5, 0, SCREEN_WIDTH-10, RestreamLabelHeight);
        [self.restreamButton setTitle:[NSString stringWithFormat:@"%@ %@",_livestream.restreamerOpenID,LOCALIZE(@"Restream_message")] forState:UIControlStateNormal];
    }else{
        self.restreamButton.frame = CGRectMake(5, 0, SCREEN_WIDTH-10, 0);
        [self.restreamButton setTitle:@"" forState:UIControlStateNormal];
    }
    [self.restreamButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    [self.restreamButton.titleLabel setFont:SYSTEM_FONT_WITH_SIZE(14)];
    [self.restreamButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    nextY += self.restreamButton.frame.size.height;
    
    [self.postTitleView setFrame:CGRectMake(0, nextY, SCREEN_WIDTH, PostCellUserTitleHeight)];
    nextY += self.postTitleView.frame.size.height;
    self.coverUIView.frame = CGRectMake(0, nextY, SCREEN_WIDTH, SCREEN_WIDTH);

    [self.livestreamImageView setFrame:CGRectMake(5, nextY+5, 92*0.75, 34*0.75)];
    nextY  += _coverUIView.frame.size.height;
    [self.captionLabel setFrame:CGRectMake(10, nextY+7, SCREEN_WIDTH-20, 10)];
    [self.bgView setFrame:CGRectMake(0, nextY, SCREEN_WIDTH, SCREEN_WIDTH)];
    
    self.postTitleView.timestamp = _livestream.beginTime;
    self.postTitleView.title = _livestream.user.openID;
    self.postTitleView.image = _livestream.user.picture;
    self.postTitleView.views = _livestream.totalViewTime;
    [self.postTitleView.viewsLabel setText:[NSString stringWithFormat:LOCALIZE(@"watching_viewer"),_livestream.liveViewerCount]];
    self.coverUIView.alpha = 1.0f;
    
    NSMutableArray *coverPictures = CREATE_MUTABLE_ARRAY;
    [coverPictures addObject:_livestream.user.picture];
    [coverPictures addObjectsFromArray:_livestream.picturesArray];
    [self joinImages:coverPictures toView:self.coverUIView];

    _livestreamIDLabel.frame = CGRectMake(0, _coverUIView.frame.origin.y+_coverUIView.frame.size.height-30, SCREEN_WIDTH, 30);
    
    if(_livestream.user.isVerified==1) {
        [_postTitleView verified:YES];
    } else {
        [_postTitleView verified:NO];
    }

    if(_livestream.user.isChoice==1){
        [_postTitleView choiced:YES];
    }else{
        [_postTitleView choiced:NO];
    }
    
    if(_livestream.user.crossGreatWall==1){
        [_postTitleView chinaFlag:YES];
    }else{
        [_postTitleView chinaFlag:NO];
    }
    
    if(_livestream.user.isInternational==1){
        [_postTitleView internationalFlag:YES];
    }else{
        [_postTitleView internationalFlag:NO];
    }
    
    self.postTitleView.moneyLabel.hidden = YES;
    self.postTitleView.viewsLabel.hidden =NO;

    self.captionLabel.text = _livestream.caption;

    CGSize stringSize = VIEW_SIZE([_captionLabel suggestedFrameSizeToFitEntireStringConstrainedToWidth:(SCREEN_WIDTH-20)]);

    
    self.captionLabel.frame = CGRectMake(_captionLabel.frame.origin.x, _captionLabel.frame.origin.y, stringSize.width, stringSize.height);

    __weak UINavigationController* nav = _navCtrl;
    [_captionLabel setDetectionBlock:^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range) {
        
        NSString* userOpenID = [string substringToIndex:1];
        if([[string substringFromIndex:1] isEqualToString:GET_DEFAULT(USER_OPEN_ID)])
            return;
        
        NSString* keyword = [string substringFromIndex:1];
        
        if([userOpenID isEqualToString:@"@"]) {
            UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
            [userCtrl getUserWithOpenID:keyword];
            userCtrl.hidesBottomBarWhenPushed = YES;
            [nav pushViewController:userCtrl animated:YES];
            
        } else if([[string substringToIndex:1] isEqualToString:@"#"]) {
            
            if([GET_DEFAULT(IN_SANDBOX) isEqualToString:@"1"]){
                return;
            }
            
            BrowsePostsViewController* broVC =[BrowsePostsViewController new];
            broVC.tagQuery = keyword;
            broVC.title = string;
            broVC.hidesBottomBarWhenPushed = YES;
            [nav pushViewController:broVC animated:YES];
        } else {
            [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"open_on_safari") message:string buttonText:LOCALIZE(@"OK") cancelable:YES withCompletion:^(BOOL okClicked) {
                if(okClicked){
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
                }
            }];
        }
    }];

    
    _moreBtn.frame = CGRectMake(SCREEN_WIDTH-40, _captionLabel.frame.origin.y, 30, 30);

    if([GET_DEFAULT(IS_ADMIN) intValue]>=1){
        _moreBtn.hidden = NO;
        _livestreamIDLabel.hidden = NO;
    }else{
        _moreBtn.hidden = YES;
        _livestreamIDLabel.hidden = YES;
    }
    
    [_livestreamIDLabel setText:[NSString stringWithFormat:@"LiveStreamID: %@",livestream.liveStreamID]];
    
    
}

-(void)didClickImage:(id)sender
{
    switch (_hotLiveStreamMode) {
        case HotLiveStreamModeSingle:
        {
//            LiveStreamViewController* vc = [LiveStreamViewController new];
            
            LiveStreamViewController* vc = [SINGLETON liveStreamSharedManager];
            if (vc.canPresentView) {
                if (_livestream!=nil) {
                    if(_liveMode==Feed)
                    {
                        [EVENT_HANDLER addEventTracking:@"ClickInFeedPage" withDict:@{@"targetUserID":_livestream.userID,@"type":@"liveStream",@"liveStreamID":_livestream.liveStreamID,@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-[EVENT_HANDLER readEnterPageTime])}];
                        _liveMode=Followed;
                    }else if (_liveMode==Restreamed){
                        [EVENT_HANDLER addEventTracking:@"ClickInFeedPage" withDict:@{@"targetUserID":_livestream.userID,@"type":@"restream",@"liveStreamID":_livestream.liveStreamID,@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-[EVENT_HANDLER readEnterPageTime])}];
                        
                    }
                }

                [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
                
                CustomTabbarController *tabVC = [SINGLETON mainTabBarViewController];

                vc.livestreamEnterMode=_liveMode;
                vc.liveStreamMode = LiveStreamModeWatch;
                vc.liveStream = _livestream;
                vc.delegate = tabVC;
                vc.transitioningDelegate = tabVC;
                vc.modalTransitionStyle = UIModalPresentationCustom;
                vc.modalPresentationStyle = UIModalPresentationFullScreen;
                if (tabVC.presentedViewController==nil) {
                    [tabVC presentViewController:vc animated:YES completion:nil];
                }
            }
            break;
        }
        case HotLiveStreamModeShow:
        {
            BlabViewController* vc = [BlabViewController new];
            vc.liveStreamMode = GROUP_WATCHER;
            vc.livestreamID = [_livestream.liveStreamID copy];
            [self.navCtrl presentViewController:NAV(vc) animated:YES completion:nil];
            break;
        }
            
        default:
        {
            LiveStreamViewController* vc = [SINGLETON liveStreamSharedManager];
            if (vc.canPresentView) {
                CustomTabbarController *tabVC = [SINGLETON mainTabBarViewController];

                vc.liveStreamMode = LiveStreamModeWatch;
                vc.liveStream = _livestream;
                vc.delegate = tabVC;
                vc.transitioningDelegate = tabVC;
                vc.modalTransitionStyle = UIModalPresentationCustom;
                vc.modalPresentationStyle = UIModalPresentationFullScreen;
                if (tabVC.presentedViewController==nil) {
                    [tabVC presentViewController:vc animated:YES completion:nil];
                }
            }
            break;
        }
    }
}

- (void)clickTitleImage
{
    if (self.navCtrl == nil || [_livestream.user.userID isEqualToString:MY_USER_ID])
        return;
    
    UserProfileViewController* userCtrl = [[UserProfileViewController alloc] init];
    [userCtrl getUserWithUserID:_livestream.user.userID];
    userCtrl.hidesBottomBarWhenPushed = YES;
    [self.navCtrl pushViewController:userCtrl animated:YES];
}


-(void)clickMoreBtn
{
    if(_delegate!=nil){
        [_delegate didClickLiveMore:self];
    }
}

-(void)dealloc
{
//    [NOTIFICATION_CENTER removeObserver:self];
}

@end
