//
//  PublishCell.h
//  story17
//
//  Created by POPO Chen on 6/17/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PublishPostObject.h"

@class PublishCell;

@protocol PublishCellDelegate < NSObject >
-(void)didClickDeletePost:(PublishCell*)publishObj;
-(void)didClickReupload:(PublishCell*)publishObj;
@end


@interface PublishCell : UICollectionViewCell

@property (nonatomic,weak) PublishPostObject* publishObj;

@property (nonatomic,strong) UIImageView* uploadImageView;
@property (nonatomic,strong) UILabel* captionLabel;
@property (nonatomic,strong) UIButton* reuploadBtn;
@property (nonatomic,strong) UIButton* deleteBtn;

@property (nonatomic,strong) UIView* progressBgView;
@property (nonatomic,strong) UIView* progressView;

@property(nonatomic)BOOL isUpload;

@property (nonatomic,weak) id<PublishCellDelegate> delegate;

+(float)getDefaultHeight;
-(void)reloadCell:(PublishPostObject*)obj;
-(void)reloadProgress:(float)progress andStatus:(int)code;

@end
