#import "MessageInputBar.h"
#import "CommentViewController.h"
#import <DigitsKit/DigitsKit.h>

@implementation MessageInputBar

@synthesize chatVC;
@synthesize messageInputBarHeight;
@synthesize sendMessageButton;
@synthesize messageTextView;
@synthesize textViewBgImageView;
@synthesize keyboardHeight;
@synthesize lastKeyboardY;
@synthesize mode;
@synthesize textViewGestureRecognizers;


#define MESSAGE_INPUT_BAR_HEIGHT 44.0


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        self.userInteractionEnabled = YES;
        self.clipsToBounds = NO;
        self.backgroundColor = MAIN_COLOR;
    
        mode = TEXT_MODE;
        _messageMode = POST_COMMENT_MESSAGE;
        
        self.keyboardHeight = 0.0;
        messageInputBarHeight = MESSAGE_INPUT_BAR_HEIGHT;
        
        lastKeyboardY = 0.0;
        
        // message text view
        textViewBgImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"input_white"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)]];
        textViewBgImageView.layer.masksToBounds = YES;
        
        [self addSubview:textViewBgImageView];
        
        messageTextView = [[UITextView alloc] initWithFrame:CGRectMake(5, 5, SCREEN_WIDTH-44, 34)];
        [messageTextView setFont:[UIFont systemFontOfSize:16.0]];
        messageTextView.clipsToBounds = YES;
        messageTextView.delegate = self;
        messageTextView.showsHorizontalScrollIndicator = NO;
        messageTextView.showsVerticalScrollIndicator = NO;
        messageTextView.autocapitalizationType = UITextAutocapitalizationTypeNone;
        messageTextView.autocorrectionType = UITextAutocorrectionTypeNo;
        messageTextView.backgroundColor = [UIColor whiteColor];
        messageTextView.tintColor = MAIN_COLOR;

        [self addSubview:messageTextView];
        
        sendMessageButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-44+5, 5, 34, 34)];
        sendMessageButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [sendMessageButton setBackgroundImage:[UIImage imageNamed:@"send"] forState:UIControlStateNormal];
        [sendMessageButton setBackgroundImage:[UIImage imageNamed:@"send_down"] forState:UIControlStateHighlighted];
        [self addSubview:sendMessageButton];
        
        // top separator line
//        UIView* separatorLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.5)];
//        separatorLine.backgroundColor = [UIColor colorWithWhite:217.0/255.0 alpha:1.0];
//        separatorLine.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
//        [self addSubview:separatorLine];
        
        [self resizeView];
        
        [self addKeyboardNotification];
    }
    
    return self;
}

-(void) addKeyboardNotification
{
    [self removeKeyboardNotification];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(keyboardNotificationHandler:) name: UIKeyboardWillChangeFrameNotification object:nil];
}

-(void) removeKeyboardNotification
{
    [NOTIFICATION_CENTER removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}

-(void) resizeView
{
    static int lastMessageLines = 1;
    
    int messageLines = [self getLineCount];
    float lineHeight = messageTextView.font.lineHeight;
    
    if(messageLines==1) {
        messageInputBarHeight = 44;
    } else if(messageLines==2) {
        messageInputBarHeight = 44+lineHeight;
    } else if(messageLines>=3) {
        messageInputBarHeight = 44+lineHeight*2;
    }
    
    self.messageTextView.frame = CGRectMake(messageTextView.frame.origin.x, messageTextView.frame.origin.y, messageTextView.frame.size.width, messageInputBarHeight-5*2);
    
    self.textViewBgImageView.frame = self.messageTextView.frame;
    
    if(lastMessageLines!=messageLines) {
        lastMessageLines = messageLines;
        
        [chatVC repositionTableViewWithAnimation:NO];
        [messageTextView scrollRangeToVisible:NSMakeRange([messageTextView.text length], 0)]; // hack: iOS7 textview jitter fix
    }
    
}

#pragma mark - KeyBoard Notification
-(void)keyboardNotificationHandler: (NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    int oldKeyboardHeight = keyboardHeight;
    
    if(keyboardFrame.origin.y==lastKeyboardY){
        return;
    }else{
        lastKeyboardY=keyboardFrame.origin.y;
    }
    
    if(keyboardFrame.origin.y==SCREEN_HEIGHT) {
        // 即將hide 鍵盤
        keyboardHeight = 0.0;
    } else {
        // 即將show 鍵盤
        keyboardHeight = keyboardFrame.size.height;
    }
    
    // check whether need to scroll to last message, and resize chatTableView!
    if(oldKeyboardHeight>0&&keyboardHeight>0) {
        [self resizeView];//re
    } else {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.25];
        [self resizeView];
        [UIView commitAnimations];
    }
    
    // this means keyboard show=>hide OR hide=>show
    //    if(abs(oldKeyboardHeight-keyboardHeight)>44 && abs(oldKeyboardHeight-keyboardHeight)<100) {
    //    }
    
    if(abs(oldKeyboardHeight-keyboardHeight)>100) {
        [chatVC repositionTableViewWithAnimation:YES];
    } else {
        [chatVC repositionTableViewWithAnimation:NO];
    }
}

-(int) getLineCount
{
    return MIN(3, MAX(1, (messageTextView.contentSize.height - messageTextView.textContainerInset.top - messageTextView.textContainerInset.bottom) / messageTextView.font.lineHeight));
}

#pragma mark - UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;

}

- (void)textViewDidChange:(UITextView *)textView
{
    [self resizeView];
    sendMessageButton.hidden = NO;
}

-(void) dealloc
{
    [self removeKeyboardNotification];
}


#pragma mark - Tagging Handler
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
//    DLog(@"shouldChangeTextInRange %@",text);
    
    if(_messageMode == LIVE_COMMENT_MESSAGE && ( [text isEqualToString:@"\n" ] || textView.text.length >= MAX_LIVE_COMMENT) ){
        return NO;
    }
    
    
    /* Searching Query Update */
//    _searchString = [self textViewQueryStringParser:text];
    [chatVC searchModeDetected:[self textViewQueryStringParser:text withRange:range]];
    
//    DLog(@"%@",_searchString);
    
    return YES;
}

-(NSString*)textViewQueryStringParser:(NSString*)text withRange:(NSRange)range
{
    
    NSString* newStr = @"";
    
    // need to handle backspace
    if(![text isEqualToString:@""]){
        newStr = [NSString stringWithFormat:@"%@%@",messageTextView.text,text];
    }else{
        if([messageTextView.text length]==0)
            return @"";
        if((int)(range.location-range.length)>=0){
            newStr = [messageTextView.text substringWithRange:NSMakeRange(0,range.location-range.length)];
        }else{
            newStr = [messageTextView.text substringWithRange:NSMakeRange(0,0)];
        }
    }
    
    NSArray* userTag = [newStr componentsSeparatedByString:@"@"];
    NSArray* hashTag = [newStr componentsSeparatedByString:@"#"];
    
    DLog(@"%@",userTag);
    
    NSString* lastUserTag = @"";
    NSString* lastHashTag = @"";
    
    if([userTag count]!=0){
        if([userTag count]>=2){
            NSString* preString = [userTag objectAtIndex:[userTag count]-2];
            if([preString hasSuffix:@" "] || [preString isEqualToString:@""]){
                lastUserTag = [userTag objectAtIndex:[userTag count]-1];
            }
        }else{
            lastUserTag = [userTag objectAtIndex:0];
        }
    }
    
    if([hashTag count]!=0){
        if([hashTag count]>=2){
            NSString* preString = [hashTag objectAtIndex:[hashTag count]-2];
            if([preString hasSuffix:@" "] || [preString isEqualToString:@""]){
                lastHashTag = [hashTag objectAtIndex:[hashTag count]-1];
            }
        }else{
            lastHashTag = [hashTag objectAtIndex:0];
        }
    }
    
    if(![lastHashTag isEqualToString:@""] && [lastHashTag length] < [lastUserTag length] && [lastHashTag rangeOfString:@" "].location == NSNotFound)
        return [NSString stringWithFormat:@"#%@",lastHashTag];
    
    if(![lastUserTag isEqualToString:@""] && [lastUserTag length] < [lastHashTag length] && [lastUserTag rangeOfString:@" "].location == NSNotFound)
        return [NSString stringWithFormat:@"@%@",lastUserTag];
    
    return @"";
    
}




@end
