//
//  PublishPostViewController.h
//  story17
//
//  Created by POPO Chen on 5/21/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "MultiFileUploader.h"
#import "SearchUserCell.h"
#import "TagViewCell.h"
#import "ArrowSwitchLabelView.h"
#import "MyLocaitionManager.h"
typedef enum{
    NoneTagging = 0,
    UserTagging = 1,
    HashTagging
} TagType;

@interface PublishPostViewController : UIViewController <UITextViewDelegate,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate>

@property (nonatomic,strong) UIView* postBGView;
@property (nonatomic,strong) UIView* bottomLine;
@property (nonatomic,strong) UIImageView* postImageView;
@property (nonatomic,strong) UITextView* inputTextField;

/* location  */
@property (nonatomic,strong) ArrowSwitchLabelView* tagUserView;
@property (nonatomic,strong) ArrowSwitchLabelView* locationSwitchView;
@property (nonatomic,strong) ArrowSwitchLabelView* locationView;
@property (nonatomic,strong) UIScrollView* mainScrollView;
@property (nonatomic,strong) UITableView* suggestTableView;
@property (nonatomic,strong) MyLocaitionManager* postlocation;
@property (nonatomic) int cordy_locationview;

@property (nonatomic,strong) LocationObject* locationObj;

@property (nonatomic,strong) CLLocationManager* locationManager;
/* Data for uploading */
@property (nonatomic,strong) UIImage* postimg;
@property (nonatomic,strong) NSString* imageName;
@property (nonatomic,strong) NSString* videoName;

@property (nonatomic) int totalVideoFrameCount;
@property (nonatomic, strong) NSMutableArray* audioDataCache;
@property (nonatomic,strong) NSString* videoUUID;

@property (nonatomic) TagType type;
@property (nonatomic,strong) NSMutableArray* searchArray;
@property (nonatomic,strong) NSString* searchString;
@property (nonatomic) int lastCharacterLength;

@property (nonatomic,strong) UILabel* charNumLabel;

@property (nonatomic,strong) NSMutableArray* taggedusers;
@property (nonatomic,strong) NSMutableArray* hashtags;

@property int enterPageTimeStamp;
@property NSString* caption;
@property BOOL postSuccess;
@end
