//
//  LeaderBoardViewController.h
//  story17
//
//  Created by POPO Chen on 8/14/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeaderBoardViewController : UIViewController <UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

@end
