//
//  SinglePostViewController.h
//  Story17
//
//  Created by POPO on 2015/5/7.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostObject.h"
#import "PostListCell.h"

typedef NS_ENUM(NSInteger, EnterModeSingelPost) {
    EnterFromShared=0,
    EnterFromFollowerLike,
    EnterFromUserProfile,
    EnterFromDiscover,
    EnterFromBrowser,
    EnterFromRevenue
};


@interface SinglePostViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,PostCellDelegate,UIScrollViewDelegate>
{
@private
    UICollectionView* _collectionView;
}

@property(nonatomic, strong)PostObject* post;
@property(nonatomic, strong)UIView* bgTapView;
@property(nonatomic)EnterModeSingelPost enterMode;
@property int likeCount;
@property int enterPageTimeStamp;
@property NSString* from;
-(void)getPostByPostID:(NSString*)postID;

@end
