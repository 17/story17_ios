//
//  PublishPostHandler.m
//  story17
//
//  Created by POPO Chen on 6/15/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "PublishPostHandler.h"
#import "Constant.h"
#import "MultiFileUploader.h"
#import "PublishPostObject.h"
#import "PublishCell.h"
#import "Mp4VideoEncoder.h"

@interface PublishPostHandler()

@property(nonatomic,strong) NSMutableArray *uploadPublishObjs;  // PublishPostObject
@property(nonatomic,strong) NSMutableArray *uploadImageObj;     // JSONString format as key
@property(nonatomic,strong) NSMutableArray *failUploadImageObj;
@property(nonatomic,strong) NSMutableArray *uploadingObj;

@end

@implementation PublishPostHandler

#define MAX_SAVED_FAILED_FILE 5
#define MAX_UPLOADED_IMAGE 5

+ (PublishPostHandler *)sharedInstance
{
    static dispatch_once_t once;
    
    static PublishPostHandler *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}


- (id)init
{
    self = [super init];
    
    if (self) {
        _uploadPublishObjs = CREATE_MUTABLE_ARRAY;
        _uploadImageObj = CREATE_MUTABLE_ARRAY;
        _failUploadImageObj = CREATE_MUTABLE_ARRAY;

        _uploadingObj = CREATE_MUTABLE_ARRAY;

        NSString *defaultString = GET_DEFAULT(UPLOAD_FAILED_FILES);
        NSArray *failFile = [defaultString mj_JSONObject];

        for (int i = 0; i < [failFile count]; i++) {
            NSDictionary *dict = [failFile[i] mj_JSONObject];
            [_uploadPublishObjs addObject:[PublishPostObject dict2PublishPostObject:dict]];
        }
    }
    return self;
}

- (void)clearData
{
    [_uploadPublishObjs removeAllObjects];
    [_uploadImageObj removeAllObjects];
    [_failUploadImageObj removeAllObjects];
}

- (void)startUploading:(NSArray *)fileNameArrays
{
    
    if ([_uploadingObj containsObject:TO_JSON(fileNameArrays)] || !fileNameArrays) {
        return;
    }
    
    [_uploadingObj addObject:TO_JSON(fileNameArrays)];
    
    if ([fileNameArrays count] == 0) {
        return;
    }
    
    MultiFileUploader *uploaded = [MultiFileUploader new];
    uploaded.fileNameArray = fileNameArrays;
    
    DLog(@"START UPLOADING %@",fileNameArrays);
    
    [uploaded startUploadWithCallback:^(NSArray *fileNameArray, int code , float callbackProgress) {
        
        int uploadedRow = [self getIndexFromFileNameArray:fileNameArray];
        PublishPostObject *pubObj;
        
        /* Update Cell data */
        if (uploadedRow != -1) {
            pubObj = _uploadPublishObjs[uploadedRow];
            pubObj.code = code;
            pubObj.progress = callbackProgress;
        }
        
        if ([self.delegate respondsToSelector:@selector(progressingCallback:status:fileNameArray:)]) {
            [self.delegate progressingCallback:callbackProgress status:code fileNameArray:fileNameArray];
        }
        
        if (code == MULTI_UPLOAD_SUCCESS) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self addUploadImageObj:fileNameArray];
                [self publishPostBroadcast];
            });
            
        } else if (code == MULTI_UPLOAD_FAIL) {
            
            [self addFailedUploadImageObj:fileNameArray];
            
            pubObj.code = MULTI_UPLOAD_FAIL;
            [self.delegate publishPostCallback:pubObj success:NO newPost:nil];
            [_uploadingObj removeObject:TO_JSON(fileNameArrays)];

        }
        
    }];
}

- (void)encodeVideo:(NSArray *)audioDataCache videoTotalFrameCount:(int)totalVideoFrameCount videoUUID:(NSString *)videoUUID
{
    
    _audioDataCache = [audioDataCache copy];
    _totalVideoFrameCount = totalVideoFrameCount;
    _videoQueue = dispatch_queue_create("video_queue", DISPATCH_QUEUE_SERIAL);

    dispatch_async(_videoQueue, ^{
        _mp4VideoEncoder = [Mp4VideoEncoder new];
        [_mp4VideoEncoder start:videoUUID];
        
        for (int i = 1; i <= _totalVideoFrameCount; i++) {
            
            NSString *frameFileName = [NSString stringWithFormat:@"%@_frame_%d", videoUUID, i];
            NSData *videoFrameData = [NSData dataWithContentsOfFile:GET_LOCAL_FILE_PATH(frameFileName)];
            u_int8_t *yDataBaseAddress = (u_int8_t *)videoFrameData.bytes;
            u_int8_t *cbDataBaseAddress = yDataBaseAddress + 480 * 480;
            u_int8_t *crDataBaseAddress = cbDataBaseAddress + 480 * 480 / 4;
            
            /* Encode Video */
            [_mp4VideoEncoder encodeVideoWithYData:yDataBaseAddress andCbData:cbDataBaseAddress andCrData:crDataBaseAddress];
        }

        /* Encode Audio */
        for (NSData *audioData in _audioDataCache) {
            [_mp4VideoEncoder encodeAudioData:audioData];
        }
        
        NSString *movieFileName = [_mp4VideoEncoder finish];
        _mp4VideoEncoder = nil;
        
        /* Upload video and images */
        NSString *imageName = [NSString stringWithFormat:@"%@.jpeg", videoUUID];
        NSString *videoName = [NSString stringWithFormat:@"%@.mp4", videoUUID];
        NSArray *fileNameArray = @[imageName, [NSString stringWithFormat:@"THUMBNAIL_%@", imageName], videoName];
        [PUBLISH_POST_HANDLER startUploading:fileNameArray];
        /* Delete all frame images */
        for (int i = 1; i <= _totalVideoFrameCount; i++) {
            NSString *frameFileName = [NSString stringWithFormat:@"%@_frame_%d", videoUUID, i];
            DELETE_FILE(GET_LOCAL_FILE_PATH(frameFileName));
        }
        
        if ([GET_DEFAULT(SAVE_PHOTO) intValue] == 1) {
            
            dispatch_async(MAIN_QUEUE, ^{
                
                if (![[[ALAssetsLibrary alloc] init] videoAtPathIsCompatibleWithSavedPhotosAlbum:[NSURL fileURLWithPath:GET_LOCAL_FILE_PATH(movieFileName) isDirectory:NO]]) {
                    DLog(@"MOVIE FILE BROKEN!");
                    return ;
                }
                
                [[[ALAssetsLibrary alloc] init] writeVideoAtPathToSavedPhotosAlbum:[NSURL fileURLWithPath:GET_LOCAL_FILE_PATH(movieFileName) isDirectory:NO] completionBlock:^(NSURL *assetURL, NSError *error) {
                    DLog(@"ERROR: %@", error.localizedDescription);
                }];
                
                DLog(@"RECORD FINISH");
            });
        }
    });

}

/* Publish Post Broadcast */

- (void)publishPostBroadcast
{
    for (PublishPostObject *pubObj in self.uploadPublishObjs) {
        if (pubObj.canUpload) {
            [self startUploadPublishObj:pubObj];
        }
    }
}

- (void)startUploadPublishObj:(PublishPostObject *)pubObj{
    
    /* publish post action */
    pubObj.canUpload = NO;  // avoid publish twice 
    
    [API_MANAGER publishPost:pubObj.uploadDict withCompletion:^(BOOL success, NSString *message, PostObject *post) {

        if (success) {
            if ([pubObj.uploadDict[@"type"] isEqualToString:@"video"]) {
                [EVENT_HANDLER addEventTracking:@"PublishPost" withDict:@{@"type":@"video"}];
            }else{
                [EVENT_HANDLER addEventTracking:@"PublishPost" withDict:@{@"type":@"pic"}];

            }
                

            if ([post.type isEqualToString:@"image"]) {
                [self removeLocalFile:pubObj.uploadImageName];
                
            }
            
            [self.delegate publishPostCallback:pubObj success:YES newPost:post];
            [self removePublishObj:pubObj];
            
        } else {
            pubObj.code = MULTI_UPLOAD_FAIL;
            [self.delegate publishPostCallback:pubObj success:NO newPost:nil];
        }
        
        [_uploadingObj removeObject:TO_JSON(pubObj.uploadImageName)];

    }];
}

/* Setter Action  */

- (void)addPublishObj:(PublishPostObject *)ppObj
{
    // Insert into array
    
    if ([self.uploadPublishObjs containsObject:ppObj]) {
        return;
    }

    [self.uploadPublishObjs insertObject:ppObj atIndex:0];
    
    if ([self.uploadPublishObjs count] > MAX_SAVED_FAILED_FILE) {
        self.uploadPublishObjs = [[ self.uploadPublishObjs subarrayWithRange: NSMakeRange(0, MAX_SAVED_FAILED_FILE)] mutableCopy];
    }  
    
    [self reloadUploadable];
    [self syncDefault];
}

- (void)replacePublishObjAt:(int)index withObject:(PublishPostObject *)ppObj
{
    if (index > [self.uploadPublishObjs count] - 1) {
        return;
    }
    
    [self.uploadPublishObjs replaceObjectAtIndex:index withObject:ppObj];
    [self reloadUploadable];
    [self syncDefault];
}

- (void)removePublishObj:(PublishPostObject *)ppObj
{
    [self removeLocalFile:ppObj.uploadImageName];
    [self.uploadPublishObjs removeObject:ppObj];
    [self syncDefault];
}

- (void)addUploadImageObj:(NSArray *)fileNameArray
{
    [self.uploadImageObj insertObject:TO_JSON(fileNameArray) atIndex:0];
    if ([self.uploadImageObj count] > MAX_UPLOADED_IMAGE) {
        self.uploadImageObj = [[self.uploadImageObj subarrayWithRange:NSMakeRange(0, MAX_UPLOADED_IMAGE)] mutableCopy];
    }
    
    [self reloadUploadable];

}

- (void)addFailedUploadImageObj:(NSArray *)fileNameArray
{
    [self.failUploadImageObj insertObject:TO_JSON(fileNameArray) atIndex:0];
    if ([self.failUploadImageObj count] > MAX_UPLOADED_IMAGE) {
        self.failUploadImageObj = [[self.failUploadImageObj subarrayWithRange:NSMakeRange(0, MAX_UPLOADED_IMAGE)] mutableCopy];
    }
}

- (NSArray *)getPostObjs
{
    return [self.uploadPublishObjs copy];
}

- (void)syncDefault
{
    NSMutableArray *fileString = CREATE_MUTABLE_ARRAY;
    for (int i = 0; i < [self.uploadPublishObjs count]; i++) {
        PublishPostObject *obj = self.uploadPublishObjs[i];
        [fileString addObject:[obj toJson]];
    }
    
    [DEFAULTS setObject:TO_JSON(fileString) forKey:UPLOAD_FAILED_FILES];
    [DEFAULTS synchronize];
}

- (void)reloadUploadable
{
    for (PublishPostObject *pubObj in self.uploadPublishObjs) {
        if ([self.uploadImageObj containsObject:TO_JSON(pubObj.uploadImageName)]) {
            pubObj.canUpload = YES;
            pubObj.code = MULTI_UPLOAD_SUCCESS;
            pubObj.progress = 1.0f;
        }
    }
}

- (BOOL)haveWaitingUploader
{
    if ([GET_DEFAULT(UPLOAD_FAILED_FILES) isEqualToString:@"[]"]) {
        return NO;
    }
    return YES;
}

- (int)getIndexFromFileNameArray:(NSArray *)fileNameArray
{
    
    for (int i=0; i < [_uploadPublishObjs count]; i++) {
        
        PublishPostObject *pubObj = _uploadPublishObjs[i];
    
        if ([TO_JSON(pubObj.uploadImageName) isEqualToString:TO_JSON(fileNameArray)]) {
            return i;
        }
        
    }
    return -1;
}

- (int)getIndexFromPublishObject:(PublishPostObject *)pubObj
{
    return (int)[_uploadPublishObjs indexOfObject:pubObj];
}

- (int)getStatusFromFilenameArray:(NSArray *)filenameArray
{
    if ([_uploadImageObj containsObject:TO_JSON(filenameArray)]) {
        return MULTI_UPLOAD_SUCCESS;
        
    } else if ([_failUploadImageObj containsObject:TO_JSON(filenameArray)]) {
        return MULTI_UPLOAD_FAIL;
        
    } else {
        return PROGRESSING;
        
    }
}

- (void)removeLocalFile:(NSArray *)fileNameArray
{
    for (NSString *fileName in fileNameArray) {
        DELETE_FILE(GET_LOCAL_FILE_PATH(fileName));
    }
}

@end
