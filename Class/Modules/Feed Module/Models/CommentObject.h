//
//  CommentObject.h
//  story17
//
//  Created by POPO Chen on 4/30/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserObject;

@interface CommentObject : NSObject

@property (nonatomic,strong) NSString* userID;
@property (nonatomic,strong) NSString* commentID;
@property (nonatomic,strong) NSString* comment;
@property int timestamp;
@property (nonatomic,strong) UserObject* user;

+(CommentObject*) getCommentWithDict:(NSDictionary*)commentDict;

@end
