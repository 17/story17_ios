#import <Foundation/Foundation.h>
#import "UserObject.h"

@class UserObject;

@interface PostObject : NSObject

@property (nonatomic,strong) NSString* postID;
@property (nonatomic,strong) NSString* message;
@property (nonatomic,strong) NSString* picture;
@property (nonatomic,strong) NSString* video;
@property (nonatomic,strong) NSString* caption;
@property (nonatomic,strong) NSString* type;
@property (nonatomic,strong) NSString* channelID;
@property (nonatomic,strong) NSString* reachability;

@property (nonatomic,strong) UserObject *user;
@property (nonatomic,assign) int likeCount;
@property (nonatomic,assign) int viewCount;
@property (nonatomic,assign) int isLiked;
@property (nonatomic,assign) int timestamp;
@property (nonatomic,assign) int commentCount;
@property (nonatomic,assign) int repostCount;
@property (nonatomic,assign) int canCommet;
@property (nonatomic,assign) float totalRevenue;

@property (nonatomic,strong) NSMutableArray* videoSegments;
@property (nonatomic,strong) NSMutableArray* taggedUsers;


+(PostObject *)getPostWithDict:(NSDictionary *)dict;

@end
