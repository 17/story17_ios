//
//  PublishPostObject.h
//  story17
//
//  Created by POPO Chen on 6/15/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PublishPostObject : NSObject

@property (nonatomic,strong) NSDictionary* uploadDict;
@property (nonatomic,strong) NSArray* uploadImageName; // [@"...jpg",@"THUBNAIL....jpg"]

@property (nonatomic) BOOL canUpload;
@property (nonatomic,strong) UIImage* postImage;
@property (nonatomic) float progress;
@property (nonatomic) int code;

-(NSString*)toJson;
+(PublishPostObject*)dict2PublishPostObject:(NSDictionary*)dict;

@end
