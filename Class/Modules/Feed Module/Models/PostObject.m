   #import "PostObject.h"

@implementation PostObject

@synthesize picture;

+(PostObject *)getPostWithDict:(NSDictionary *)dict
{
    
    PostObject *post = [PostObject new];
    
    if([dict objectForKey:@"userInfo"]){
        post.user = [UserObject getUserWithDict:dict[@"userInfo"]];
    }
    
    post.postID = dict[@"postID"];
    post.message = dict[@"message"];
    post.picture = dict[@"picture"];
    post.video = dict[@"video"];
    post.likeCount = [dict[@"likeCount"] intValue];
    
    post.isLiked = [dict[@"liked"] intValue];
    post.timestamp = [dict[@"timestamp"] intValue];
    post.viewCount = [dict[@"viewCount"] intValue];
    post.commentCount = [dict[@"commentCount"] intValue];
    
    post.caption = dict[@"caption"];
    post.type = dict[@"type"];
    post.channelID = dict[@"channelID"];
    post.reachability = dict[@"reachability"];
    post.totalRevenue = [dict[@"totalRevenue"] floatValue];

    return post;
}

@end
