//
//  PublishPostObject.m
//  story17
//
//  Created by POPO Chen on 6/15/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "PublishPostObject.h"
#import "Constant.h"

@implementation PublishPostObject

-(id)init
{
    self = [super init];
    if(self){
        self.canUpload = NO;
        self.progress = 0.0f;
        self.code = MULTI_UPLOAD_FAIL;
    }
    return self;
}


-(NSString*)toJson
{
    NSMutableDictionary* dict = CREATE_MUTABLE_DICTIONARY;
    dict[@"uploadDict"] = TO_JSON(self.uploadDict);
    dict[@"imageName"] = TO_JSON(self.uploadImageName);
    return TO_JSON(dict);
}

+(PublishPostObject*)dict2PublishPostObject:(NSDictionary*)dict
{
    PublishPostObject* pubObj = [PublishPostObject new];
    pubObj.uploadDict = [dict[@"uploadDict"] mj_JSONObject];

    pubObj.uploadImageName = [dict[@"imageName"] mj_JSONObject];

    pubObj.code = MULTI_UPLOAD_FAIL;
    pubObj.progress = 0.0f;

    return pubObj;
}


@end
