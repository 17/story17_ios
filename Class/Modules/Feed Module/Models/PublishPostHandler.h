//
//  PublishPostHandler.h
//  story17
//
//  Created by POPO Chen on 6/15/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MultiFileUploader;
@class PostObject;
@class Mp4VideoEncoder;
@class PublishPostObject;

@protocol PublishProgressingDelegate <NSObject>
- (void)progressingCallback:(float)progress status:(int)code fileNameArray:(NSArray *)fileNameArray;
- (void)publishPostCallback:(PublishPostObject *)pubObj success:(BOOL)success newPost:(PostObject *)post;
@end

@interface PublishPostHandler : NSObject

@property(nonatomic) id <PublishProgressingDelegate> delegate;
@property (assign, nonatomic) int totalVideoFrameCount;
@property (strong, nonatomic) NSArray* audioDataCache;
@property Mp4VideoEncoder *mp4VideoEncoder;
@property dispatch_queue_t videoQueue;

+ (id)sharedInstance;
- (void)startUploading:fileNameArray;

- (void)publishPostBroadcast;

- (void)addPublishObj:(PublishPostObject *)ppObj;
- (void)removePublishObj:(PublishPostObject *)ppObj;
- (void)addUploadImageObj:(NSArray *)fileNameArray;
- (NSArray *)getPostObjs;

- (int)getIndexFromFileNameArray:(NSArray *)fileNameArray;
- (int)getIndexFromPublishObject:(PublishPostObject *)pubObj;

- (BOOL)haveWaitingUploader;
- (int)getStatusFromFilenameArray:(NSArray *)filenameArray;

-(void)encodeVideo:(NSArray *)audioDataCache videoTotalFrameCount:(int)totalVideoFrameCount videoUUID:(NSString *)videoUUID;
-(void)clearData;

@end
