//
//  CommentObject.m
//  story17
//
//  Created by POPO Chen on 4/30/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "CommentObject.h"
#import "UserObject.h"
@implementation CommentObject

-(id)init
{
    self = [super init];
    return self;
}

+(CommentObject*) getCommentWithDict:(NSDictionary*)commentDict
{
    CommentObject* comment = [CommentObject new];
    comment.userID = commentDict[@"userID"];
    comment.commentID = commentDict[@"commentID"];
    comment.comment = commentDict[@"comment"];
    comment.timestamp = [commentDict[@"timestamp"] intValue];
    comment.user = [UserObject getUserWithDict:commentDict[@"userInfo"]];

    return comment;
    
}


@end
