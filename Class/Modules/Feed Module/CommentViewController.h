//
//  CommentViewController.h
//  Story17
//
//  Created by POPO on 2015/5/5.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageInputBar.h"
#import "CommentCell.h"

typedef enum{
    NoneTagging = 0,
    UserTagging = 1,
    HashTagging
} TagType;

@interface CommentViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,CommentCellDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate>

@property(nonatomic, weak) PostObject* post;
@property(nonatomic, strong)UITableView* commentsTableView;
@property(nonatomic, strong)UITableView* tagSuggestionTableView;

@property(nonatomic, strong)NSMutableArray* commentsArray;
@property(nonatomic, strong)NSMutableArray* searchArray;
@property(nonatomic, strong)MessageInputBar* messageInputBar;
@property(nonatomic, strong)UIImageView* noDataImageView;
@property(nonatomic, strong)NSString* searchString;
@property (nonatomic,strong) MultiFileUploader* multiFileUploader;

@property BOOL needScrollToBottom;
@property BOOL isFirstOpen;
@property (assign, nonatomic) BOOL isAtButtom;
@property (assign,nonatomic) CGFloat lastCellHeight;
@property (nonatomic) TagType type;

@property(nonatomic, strong)NSMutableArray* taggedUsers;

@property(nonatomic, strong)CommentCell* showedCommentCell;

-(void) repositionTableViewWithAnimation:(BOOL) animated;

-(void)searchModeDetected:(NSString*)searchString;

@end
