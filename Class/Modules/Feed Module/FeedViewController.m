//
//  FeedViewController.m
//  story17
//
//  Created by POPO Chen on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "FeedViewController.h"
#import "FindFriendViewController.h"
#import "UserSuggestionViewController.h"
#import "Constant.h"
#import "SVPullToRefresh.h"
#import "ThemeManager.h"
#import "ListTableViewController.h"
#import "PublishPostObject.h"
#import "EditPostViewController.h"
#import "LiveStreamViewController.h"
#import "LeaderBoardViewController.h"
#import "PostGridCell.h"

#import "ConnectionViewController.h"

#define PostCellKey @"PostCell"
#define PostADKey @"PostAd"
#define PublishCellKey @"PublishCellKey"
#define LivestreamCellKey @"LivestreamCellKey"

static NSString * const kReuseCellIdentifier = @"PostGridCell";

@interface FeedViewController()
@property (strong, nonatomic) UIButton *gridModeButton;
@property (assign, nonatomic) BOOL isGridMode;

@end

@implementation FeedViewController

@synthesize isFetching;

#define fetchNum 12

-(id)init
{
    self = [super init];
    if(self){
        
        [PUBLISH_POST_HANDLER setDelegate:self];
        
        self.tabBarController.tabBar.autoresizingMask = UIViewAutoresizingNone;

        _originalTabBarY = [self.tabBarController tabBar].frame.origin.y;
        _originalNaviBarY = [self.navigationController navigationBar].frame.origin.y;
        _originalStatusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
        
        _originalNaviBarY = STATUS_BAR_HEIGHT;
        _originalTabBarY = SCREEN_HEIGHT-TAB_BAR_HEIGHT-_originalNaviBarY+20;
        _hotspotHeight = 0.0f;
        
        _isForground = NO;
//        DLog(@"navbar:%f",_originalNaviBarY);
//        DLog(@"tabbar:%f",_originalTabBarY);
//        DLog(@"status:%f",_originalStatusBarHeight);
//        
        [NOTIFICATION_CENTER addObserver:self selector:@selector(publishPost:) name:PUBLISH_POST_NOTIFICATION object:nil];
        [NOTIFICATION_CENTER addObserver:self selector:@selector(refresh:) name:REFRESH_FEEDS object:nil];
        [NOTIFICATION_CENTER addObserver:self selector:@selector(resetBarPositions) name:ENTER_FOREGROUND_NOTIFICATION object:nil];
        [NOTIFICATION_CENTER addObserver:self selector:@selector(statusBarHeightChangeHandler) name:UIApplicationWillChangeStatusBarFrameNotification object:nil];

    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self configureViewForIOS7];
    
    self.title = LOCALIZE(@"Feed");
    self.navigationController.title = @"";

    _postArray = CREATE_MUTABLE_ARRAY;
    
    self.isGridMode = NO;
    
    __weak FeedViewController* weakSelf = self;
    
    if (self.feedCollectionView == nil)
    {
        UIButton* findFriendBtn = [ThemeManager findFriendBtn];
        [findFriendBtn addTarget:self action:@selector(didClickFindFriend:) forControlEvents:UIControlEventTouchUpInside];
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:findFriendBtn];
        
        
        /* add for gm to see editors choose */
        UIButton* editorChoice = [ThemeManager leaderBoardBtn];
        
        [editorChoice bk_addEventHandler:^(id sender) {

            
//            UserSuggestionViewController* userSugVC = [UserSuggestionViewController new];
////            userSugVC.hidesBottomBarWhenPushed = YES;
//            [weakSelf.navigationController pushViewController:userSugVC animated:YES];

            LeaderBoardViewController* userSugVC = [LeaderBoardViewController new];
            userSugVC.hidesBottomBarWhenPushed = YES;
            [weakSelf.navigationController pushViewController:userSugVC animated:YES];
            
        } forControlEvents:UIControlEventTouchUpInside];
        
        _gridModeButton = [ThemeManager gridModeBtn];
        [_gridModeButton addTarget:self action:@selector(onClickGridModeButton:) forControlEvents:UIControlEventTouchUpInside];

        self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:editorChoice], [[UIBarButtonItem alloc] initWithCustomView:_gridModeButton]];
            
       
        /* BG View for tap location */

        _bgTapView = [[UIView alloc]initWithFrame:CGRectMake(0, -NAVI_BAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [self.view addSubview:_bgTapView];
        
        UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
        [layout setScrollDirection:UICollectionViewScrollDirectionVertical];

        self.feedCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT) collectionViewLayout:layout];
        self.feedCollectionView.delegate = self;
        self.feedCollectionView.dataSource = self;
        
        [self.feedCollectionView registerClass:[PostListCell class] forCellWithReuseIdentifier:PostCellKey];
        [self.feedCollectionView registerClass:[PublishCell class] forCellWithReuseIdentifier:PublishCellKey];
        [self.feedCollectionView registerClass:[LivestreamCell class] forCellWithReuseIdentifier:LivestreamCellKey];
        [self.feedCollectionView registerNib:[UINib nibWithNibName:kReuseCellIdentifier bundle:nil] forCellWithReuseIdentifier:kReuseCellIdentifier];
        [self.feedCollectionView setAlwaysBounceVertical:YES];

        
        __weak FeedViewController* weakself = self;
        [self.feedCollectionView addPaddingPullToRefreshWithActionHandler:^{
            [weakself fetchDataWithRefresh:YES];
        }];
        
        _noDataBtn = [ThemeManager getGreenLineBtn];
        _noDataBtn.frame = CGRectMake(SCREEN_WIDTH/8, SCREEN_HEIGHT/2-90, SCREEN_WIDTH*3/4, 45);
        _noDataBtn.hidden = YES;
        _noDataBtn.titleLabel.font = SYSTEM_FONT_WITH_SIZE(18);
        [_noDataBtn setTitle:LOCALIZE(@"find more friends") forState:UIControlStateNormal];
        
        [_noDataBtn bk_addEventHandler:^(id sender) {
            
            UserSuggestionViewController* usVC = [UserSuggestionViewController new];
            usVC.useCustomNavBar = NO;
            UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:usVC];
            [weakself.navigationController presentViewController:nav animated:YES completion:nil];
        
        } forControlEvents:UIControlEventTouchUpInside];
        
        
        _progressView = [UIActivityIndicatorView new];
        _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
        [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [_progressView setColor:MAIN_COLOR];
        
        [self.feedCollectionView setBackgroundColor:LIGHT_BG_COLOR];
        [self.view addSubview:self.feedCollectionView];
        [self.view addSubview:_noDataBtn];
        [self.view addSubview:_progressView];
        
        self.isFirstFetching = true;
        self.isFetching = false;
        self.noMoreData = false;
        self.showCellIndex = 0;
        
    }
    
    [self fetchDataWithRefresh:true];
    
//    [self.navigationController setNavigationBarHidden:YES];
    
}



-(void)viewWillAppear:(BOOL)animated
{
    
    DLog(@"viewWillAppear");
    
    [super viewWillAppear:animated];
    
    _feedEnterTimeStamp=CURRENT_TIMESTAMP;
    [EVENT_HANDLER setEnterPageTime];
    [EVENT_HANDLER addEventTracking:@"EnterFeedPage" withDict:nil];
    
    [NOTIFICATION_CENTER addObserver:self selector:@selector(enterBackground:) name:UIApplicationWillResignActiveNotification object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(setCellToPlay:) name:BUFFER_VIDEO_UPLADED object:nil];
    _isForground = YES;
    //eventTracking
    _feedRefreshCount=0;
    _feedVisibleCellCount=0;
    _gridModeCount=0;

    if(ON_HOTSPOT){
        _hotspotHeight =20.0f;
    }
    
    if([GET_DEFAULT(UPDATE_FEED_TIME) intValue] < CURRENT_TIMESTAMP-UPDATE_INTERVAL){
        [self fetchDataWithRefresh:true];
    }
    
    _disableBarScroll = NO;
    
    /* play livestream */
    [_feedCollectionView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _isForground = NO;
    _disableBarScroll = YES;
    
    [EVENT_HANDLER addEventTracking:@"LeaveFeedPage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_feedEnterTimeStamp),@"watchCount":INT_TO_STRING(_feedVisibleCellCount),@"refreshCount":INT_TO_STRING(_feedRefreshCount),@"gridModeCount":INT_TO_STRING(_gridModeCount)}];
    [SINGLETON iapManager];
    if((int)self.navigationController.navigationBar.frame.origin.y!=0)
        [self resetBarPositions];
    
    /* Remove player */
    [SINGLETON stopAVPlayerForCollectionView:_feedCollectionView];
    
    [NOTIFICATION_CENTER postNotificationName:END_LIVE_PREVIEW_NOTIFICATION object:nil];
    [NOTIFICATION_CENTER removeObserver:self name:BUFFER_VIDEO_UPLADED object:nil];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];

    _originalTabBarY = [self.tabBarController tabBar].frame.origin.y;
    _originalNaviBarY = [self.navigationController navigationBar].frame.origin.y;
    _originalStatusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    
    _originalNaviBarY = STATUS_BAR_HEIGHT;
    _originalTabBarY = SCREEN_HEIGHT-TAB_BAR_HEIGHT-_originalNaviBarY+20;
    _hotspotHeight = 0.0f;
    
    /* Level 5 crash fixed */
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;

    if([PUBLISH_POST_HANDLER haveWaitingUploader]){
    
        dispatch_async(MAIN_QUEUE, ^{
            [_feedCollectionView reloadData];            
        });
        [_feedCollectionView setContentOffset:CGPointZero animated:NO];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [PUBLISH_POST_HANDLER publishPostBroadcast];
        });
    }

    [SINGLETON startVideoAVPlayerForCollectionView:_feedCollectionView];
}

-(void) resetBarPositions
{
    [self moveNavAndTabBar:0.0 animated:NO];
}

-(void) statusBarHeightChangeHandler
{
    float currentStatusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    float delta = _originalStatusBarHeight - currentStatusBarHeight;
    
    if(delta==0 || currentStatusBarHeight==0) {
        return;
    }
    
    _originalTabBarY += delta;
    
    _originalStatusBarHeight = currentStatusBarHeight;
    
    [self moveNavAndTabBar:0 animated:YES];
    
}

-(void)refresh:(id)sender
{
    [SINGLETON stopAVPlayerForCollectionView:_feedCollectionView];

    [self fetchDataWithRefresh:YES];
    _feedRefreshCount++;
}


-(void)dealloc
{
    [NOTIFICATION_CENTER removeObserver:self];
}

- (void)fetchDataWithRefresh:(BOOL)refresh
{
    if(refresh)
        _noMoreData = false;
    
    if(_noMoreData || isFetching)
        return;
    
    int fetchLive = 1;
    
    if (self.isFirstFetching){
        [_progressView startAnimating];
    }
    
    int lastTime = INT32_MAX;
    if (!refresh && [self.postArray count] != 0)
    {
        PostObject* obj = [self.postArray lastObject];
        lastTime = obj.timestamp;
        fetchLive = 0;
    }
    
    if(refresh && !_isFirstFetching){
        [_feedCollectionView.pullToRefreshView startAnimating];
    }
 
    isFetching = YES;
    
    [API_MANAGER getPostFeed:lastTime andCount:fetchNum fetchLiveStreamL:fetchLive withCompletion:^(BOOL success, NSArray* posts) {
        isFetching = NO;
        
        [self.feedCollectionView.pullToRefreshView stopAnimating];
        [_progressView stopAnimating];
        self.isFirstFetching = false;
        
        if([posts count]==0 && [_postArray count]==0){
            _noDataBtn.hidden = NO;
        } else {
            _noDataBtn.hidden = YES;
        }
        
        if(success) {
            if(refresh){
                
//                [NOTIFICATION_CENTER postNotificationName:END_LIVE_PREVIEW_NOTIFICATION object:nil];
                
                [DEFAULTS setObject:INT_TO_NUMBER(CURRENT_TIMESTAMP) forKey:UPDATE_FEED_TIME];
                [DEFAULTS synchronize];
                [_postArray removeAllObjects];
            }
            
            /* Remove From Local Database */
            for(PostObject* post in posts){
                if(![[[DatabaseManager sharedInstance] getReportRecord:@"userID"] containsObject:post.user.userID]){
                    post.user.isFollowing=1;
                    [_postArray addObject:post];
                }
            }
            
            if([posts count]<12){
                _noMoreData = true;
            }
            
            [self.feedCollectionView reloadData];
            
            
            // scrollToTop after refresh
            if(refresh){
                [self.feedCollectionView scrollsToTop];
            }
            
            NSMutableArray* postIDs= [[NSMutableArray alloc] init];
            for (NSObject* post in posts){
                if([post isKindOfClass:[PostObject class]]){
                    if(![((PostObject*)post).user.userID isEqualToString:MY_USER_ID]){
                        [postIDs addObject:((PostObject*)post).postID];
                    }
                }
            }
            
            [API_MANAGER viewPost:TO_JSON(postIDs) withCompletion:^(BOOL success){}];
        }
        
    }];
}


#pragma mark - UICollectionView DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(section==0){
        return [[PUBLISH_POST_HANDLER getPostObjs] count];
    }else{
        return self.postArray != nil ? [self.postArray count] : 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        
        PublishCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:PublishCellKey forIndexPath:indexPath];
        
        if (cell == nil)
        {
            CGRect rect = CGRectMake(0, 0, self.view.bounds.size.width, [PublishCell getDefaultHeight]);
            cell = [[PublishCell alloc] initWithFrame:rect];
        }
        
        PublishPostObject* pubObj = [PUBLISH_POST_HANDLER getPostObjs][indexPath.row];
        [cell reloadCell:pubObj];
        cell.delegate = self;
        
        return cell;
        
    }else if(indexPath.section == 1){
        
        if(indexPath.row>=[_postArray count]-fetchNum/2){
            [self fetchDataWithRefresh:NO];
        }
        
        if (_isGridMode) {
            PostGridCell* cell = (PostGridCell*)[collectionView dequeueReusableCellWithReuseIdentifier:kReuseCellIdentifier forIndexPath:indexPath];
            cell.post = _postArray[indexPath.row];

            
            return cell;
        }
        
        if([[_postArray objectAtIndex:indexPath.row] isKindOfClass:[LiveStreamObject class]]){
            
            LiveStreamObject* liveObj = [_postArray objectAtIndex:indexPath.row];
            /* user coverphoto as identifier not reuse cell */
            
            //            NSString* identifier = [SINGLETON getUUID];
            //            NSString* identifier = @"livestreamCell";
            //            [collectionView registerClass:[LivestreamCell class] forCellWithReuseIdentifier:identifier];
            
            LivestreamCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:LivestreamCellKey forIndexPath:indexPath];
            cell.liveMode=Feed;
            if (cell == nil)
            {
                cell = [[LivestreamCell alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,100)];
            }
            
            [cell.livestreamImageView setImage:[UIImage imageNamed:@"onlive"]];
            cell.navCtrl = self.navigationController;
            cell.delegate = self;
            cell.livestream = liveObj;  // reloadUI Action
            _feedVisibleCellCount++;
            return cell;
        }
        else {
            
            PostObject* postObj  = [_postArray objectAtIndex:indexPath.row];
            NSString* identifier = PostCellKey;
            
            if([postObj.type isEqualToString:@"video"]){
                identifier = postObj.postID;
                [collectionView registerClass:[PostListCell class] forCellWithReuseIdentifier:identifier];
            }
            
            PostListCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            
            if (cell == nil){
                CGRect rect = CGRectMake(0, 0, self.view.bounds.size.width, [PostListCell getCellHeigth]);
                cell = [[PostListCell alloc] initWithFrame:rect];
            }
            
            cell.navCtrl = self.navigationController;
            
            cell.post = postObj;
            cell.delegate = self;
            _feedVisibleCellCount++;
            return cell;
        }
    }
    return nil;
}

#pragma mark - UICollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isGridMode) {
        _gridModeButton.selected = NO;
        self.isGridMode = NO;
        [_feedCollectionView reloadData];
        
        [_feedCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
        _feedCollectionView.contentOffset = CGPointMake(_feedCollectionView.contentOffset.x, _feedCollectionView.contentOffset.y-STATUS_BAR_HEIGHT-NAVI_BAR_HEIGHT);
        
        [self moveNavAndTabBar:0 animated:YES];
        //eventTracking

    }else{
        
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    

    if(section==0){
        return UIEdgeInsetsMake(3, 0, 0, 0);
    }else{
        if (_isGridMode) {
            return UIEdgeInsetsMake(2, 2, 2, 2);
        }
        return UIEdgeInsetsMake(3, 0, 0, 0);
    }

}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if(section==0){
        return  CGSizeMake(SCREEN_WIDTH,NAVI_BAR_HEIGHT+STATUS_BAR_HEIGHT);
    }else{
        return  CGSizeMake(SCREEN_WIDTH,0);
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    if(indexPath.section==0){
        return CGSizeMake(SCREEN_WIDTH, [PublishCell getDefaultHeight]);
    }else{
        if (_isGridMode) {
            return CGSizeMake((SCREEN_WIDTH)/3-2,(SCREEN_WIDTH)/3-2);
        }
        
        if([[_postArray objectAtIndex:indexPath.row] isKindOfClass:[LiveStreamObject class]]){
            return CGSizeMake(SCREEN_WIDTH, [LivestreamCell getHeightFromLivestream:_postArray[indexPath.row]]);
        }else{
            return CGSizeMake(SCREEN_WIDTH, [PostListCell getCellHeigthWithPost:_postArray[indexPath.row]]);
        }
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if(section==0){
        return 2.0f;
    }else{
        if (_isGridMode) {
            return 1.0f;
        }
        return 10.0f;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

#pragma mark - PostCellDelegate

-(void)didClickPostImage:(UITapGestureRecognizer *)tapGesture withPostID:(NSString*)postID
{
    CGPoint touchPoint = [tapGesture locationInView:_bgTapView];
    if (postID!=nil) {
        [EVENT_HANDLER addEventTracking:@"LikePostFeed" withDict:@{@"postID":postID}];
        [EVENT_HANDLER addEventTracking:@"ClickInFeedPage" withDict:@{@"type":@"post",@"postID":postID,@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-[EVENT_HANDLER readEnterPageTime])}];
    }


    
    touchPoint.y += NAVI_BAR_HEIGHT*_barMovePercentage;
    [DIALOG_MANAGER showHeartFromPoint:touchPoint];
}

-(void)didClickMore:(PostListCell*)cell
{
    [self didClickMoreDialog:cell collectionView:_feedCollectionView dataArray:_postArray];
}


# pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(!_isForground)
        return;
    
    /* Scroll Up direction detect  */
    BOOL scrooUp = true;
    if (self.lastContentOffset > scrollView.contentOffset.y){
        scrooUp = false;
    }
    
    /* to get hotspot status */
    if(ON_HOTSPOT){
        _hotspotHeight =20.0f;
    }
    
    // video play
    [SINGLETON startVideoAVPlayerForCollectionView:_feedCollectionView];
    
    if(_disableBarScroll || _hotspotHeight!=0) {
        return;
    }
    
    float currentOffset = scrollView.contentOffset.y;
    
    float delta = 0.0;
    
    delta = currentOffset-self.lastScrollOffset;
    self.lastScrollOffset = scrollView.contentOffset.y;
    
    if(currentOffset<0 && delta<0) {
        [self moveNavAndTabBar:0 animated:YES];
    }
    
    if(currentOffset<0.0 || scrollView.contentOffset.y>scrollView.contentSize.height-scrollView.frame.size.height) {
        return;
    }
    
    UINavigationBar* naviBar = [self.navigationController navigationBar];
    
    float targetNaviBarY = naviBar.frame.origin.y - delta;

    targetNaviBarY = MAX(targetNaviBarY, _originalNaviBarY-naviBar.frame.size.height-20.0);
    
    if(targetNaviBarY<_originalNaviBarY && (int)_feedCollectionView.contentOffset.y>40) {
        [self moveNavAndTabBar:-(targetNaviBarY-_originalNaviBarY)/(naviBar.frame.size.height+20) animated:NO];
    } else {
        [self moveNavAndTabBar:0 animated:NO];
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
    
    
    [LIKE_HANDLER likeBatchUpload];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if(_barMovePercentage>=0.5) {
        [self moveNavAndTabBar:1.0 animated:YES];
    } else {
        [self moveNavAndTabBar:0.0 animated:YES];
    }
}

-(void) moveNavAndTabBar:(float) percentage animated:(BOOL) animated
{
    self.barMovePercentage = percentage;
    
    float animationDuration = 0.0;
    
    if(animated) {
        animationDuration = 0.3;
    }
    
//    DLog(@"%f",percentage);
    
    [UIView animateWithDuration:animationDuration animations:^{
        
        float targetAlpha = MAX(0, (1.0-percentage*1.25));
        UIColor* targetColor =  WHITE_COLOR;
        [self.navigationController.navigationBar setTintColor:targetColor];
        
        self.tabBarController.tabBar.alpha = targetAlpha;
        self.navigationController.navigationBar.alpha = targetAlpha;
        
        NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   targetColor, NSForegroundColorAttributeName,
                                                   [UIFont systemFontOfSize:18.0], NSFontAttributeName, nil];
        [self.navigationController.navigationBar setTitleTextAttributes:navbarTitleTextAttributes];
        
        
        UITabBar* tabBar = [self.tabBarController tabBar];
        
        tabBar.frame = CGRectMake(0, _originalTabBarY+tabBar.frame.size.height*percentage, SCREEN_WIDTH, tabBar.frame.size.height);
        
        
        UINavigationBar* naviBar = [self.navigationController navigationBar];
        UIWindow *statusBarWindow = (UIWindow *)[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"];
        
        naviBar.frame = CGRectMake(0, _originalNaviBarY-_hotspotHeight-(NAVI_BAR_HEIGHT+STATUS_BAR_HEIGHT)*percentage, SCREEN_WIDTH, NAVI_BAR_HEIGHT);
        
//        DLog(@"%f %f %f %f",_originalNaviBarY,(NAVI_BAR_HEIGHT+STATUS_BAR_HEIGHT)*percentage,percentage,naviBar.frame.size.height);
        
        CGRect frame = statusBarWindow.frame;
        frame.origin.y = -STATUS_BAR_HEIGHT*percentage*1.6;
        statusBarWindow.frame = frame;
        
        //statusBarWindow.alpha = targetAlpha;
    }];
}

#pragma mark - PublishProgressingDelegate
-(void)progressingCallback:(float)progress status:(int)code fileNameArray:(NSArray*)fileNameArray
{
    
    DLog(@"progressingCallback progress");
    int uploadedRow = [PUBLISH_POST_HANDLER getIndexFromFileNameArray:fileNameArray];
    
    if(uploadedRow!=-1){
        
            dispatch_async(MAIN_QUEUE, ^{
                NSIndexPath* indexPath = [NSIndexPath indexPathForItem:uploadedRow inSection:0];
                UICollectionViewCell* cell = [_feedCollectionView cellForItemAtIndexPath:indexPath];
                if(![cell isEqual:[NSNull null]] && [cell isKindOfClass:[PublishCell class]] &&  ![cell isKindOfClass:[NSNull class]]){
                    [_feedCollectionView reloadItemsAtIndexPaths:@[indexPath]];
                }
            });
    }
    
}

-(void)publishPostCallback:(PublishPostObject*)pubObj success:(BOOL)success newPost:(PostObject*)post
{
    if(success){
        
        [_postArray insertObject:post atIndex:0];
        dispatch_async(MAIN_QUEUE, ^{
            
            [SINGLETON stopAVPlayerForCollectionView:_feedCollectionView];
            [_feedCollectionView reloadData];
            [_feedCollectionView setContentOffset:CGPointZero animated:NO];

            NSIndexPath* indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
            
            UICollectionViewCell* cell = [_feedCollectionView cellForItemAtIndexPath:indexPath];
            if(![cell isEqual:[NSNull null]] && [cell isKindOfClass:[PostListCell class]] &&  ![cell isKindOfClass:[NSNull class]]){
                [((PostListCell*)cell) startAVPlayer];
            }
            
        });
        
        [NOTIFICATION_CENTER postNotificationName:RELOAD_SELF_DATA object:self];
        
    }else{
        
        int uploadedRow = [PUBLISH_POST_HANDLER getIndexFromPublishObject:pubObj];
        
        if(uploadedRow!=-1){
            
            if([_postArray count] == 0 ){
                _noDataBtn.hidden = NO;
            }
            
            dispatch_async(MAIN_QUEUE, ^{
                NSIndexPath* indexPath = [NSIndexPath indexPathForItem:uploadedRow inSection:0];
                UICollectionViewCell* cell = [_feedCollectionView cellForItemAtIndexPath:indexPath];
                DLog(@"cell:%@",cell);
                if(![cell isEqual:[NSNull null]] && [cell isKindOfClass:[PublishCell class]] &&  ![cell isKindOfClass:[NSNull class]]){
                    [_feedCollectionView reloadItemsAtIndexPaths:@[indexPath]];
                }
            });
            
        }
    }
    
}

#pragma mark - PublishCellDelegate
-(void)didClickReupload:(PublishCell *)cell
{
    [PUBLISH_POST_HANDLER startUploading:cell.publishObj.uploadImageName];
}

-(void)reloadCellPlayer:(PostListCell *)cell
{
    DLog(@"reloadCell");
    [SINGLETON startVideoAVPlayerForCollectionView:_feedCollectionView];
}

-(void)didClickDeletePost:(PublishCell *)cell
{
    if (!cell.publishObj) {
        return;
    }
    
    [_feedCollectionView performBatchUpdates:^{
        NSIndexPath *indexPath = [_feedCollectionView indexPathForCell:cell];
        if (!indexPath) {
            return;
        }
        
        [PUBLISH_POST_HANDLER removePublishObj:cell.publishObj];
        [_feedCollectionView deleteItemsAtIndexPaths:@[indexPath]];
        
    } completion:nil];
}

-(void)enterBackground:(id)sender
{
    [SINGLETON stopAVPlayerForCollectionView:_feedCollectionView];

}

/* handle buffer video callback */
-(void)setCellToPlay:(id)sender
{
    [SINGLETON startVideoAVPlayerForCollectionView:_feedCollectionView];
}

#pragma mark - LivestreamCellDelegate
-(void)didClickLiveMore:(LivestreamCell*)cell
{
    NSMutableArray* array = [NSMutableArray arrayWithObjects:LOCALIZE(@"hideHotLiveStream_confirm"),  nil];
    
    if([GET_DEFAULT(IS_ADMIN) intValue]>=PRIMO_ADMIN){
        [array addObject:LOCALIZE(@"promot_to_country")];
        [array addObject:LOCALIZE(@"remove_promot_to_country")];
        [array addObject:LOCALIZE(@"promote_to_top")];
        [array addObject:LOCALIZE(@"cancel_promote_to_top")];
    }
    
    [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"live_stream_control") options:[array copy] destructiveIndexes:@[@0] cancelable:YES withCompletion:^(int selectedOption) {
        
        if([array[selectedOption] isEqualToString:LOCALIZE(@"hideHotLiveStream_confirm")]){
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"hideHotLiveStream_confirm") options:@[LOCALIZE(@"OK")] destructiveIndexes:@[@0] cancelable:YES withCompletion:^(int selectedOption) {
                    if(selectedOption==0){
                        [API_MANAGER hideHotLivestreamAction:cell.livestream.liveStreamID withCompletion:^(BOOL success) {
                            if(success){
                                [DIALOG_MANAGER showCompleteToast];
                            }else{
                                [DIALOG_MANAGER showNetworkFailToast];
                            }
                        }];
                    }
                }];
            });
        } else if([array[selectedOption] isEqualToString:LOCALIZE(@"promot_to_country")]){
            
            
            NSMutableArray* countryArray = CREATE_MUTABLE_ARRAY;
            
            [API_MANAGER getLiveStreamHotCountryList:^(BOOL success, NSArray *regionList) {
                if(success){
                    for(NSArray* arr in regionList){
                        [countryArray addObject:arr[0]];
                    }
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"promot_to_country") options:countryArray destructiveIndexes:@[] cancelable:YES withCompletion:^(int selectedOption) {
                            NSString* selectedCountry = countryArray[selectedOption];
                            if(selectedCountry.length!=0){
                                [API_MANAGER promoteLiveStream:cell.livestream.liveStreamID countryName:selectedCountry withCompletion:^(BOOL success, int hasRestreamed) {
                                    if(success){
                                        [DIALOG_MANAGER showCompleteToast];
                                    }else{
                                        [DIALOG_MANAGER showNetworkFailToast];
                                    }
                                }];
                            }
                        }];
                    });
                }
            }];
            
        }else if([array[selectedOption] isEqualToString:LOCALIZE(@"remove_promot_to_country")]){
            
            NSMutableArray* countryArray = CREATE_MUTABLE_ARRAY;
            
            [API_MANAGER getLiveStreamHotCountryList:^(BOOL success, NSArray *regionList) {
                if(success){
                    
                    for(NSArray* arr in regionList){
                        [countryArray addObject:arr[0]];
                    }
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [DIALOG_MANAGER showActionSheetDialogTitle:LOCALIZE(@"promot_to_country") options:countryArray destructiveIndexes:@[] cancelable:YES withCompletion:^(int selectedOption) {
                            NSString* selectedCountry = countryArray[selectedOption];
                            if(selectedCountry.length!=0){
                                [API_MANAGER removePromotedLiveStream:cell.livestream.liveStreamID countryName:selectedCountry withCompletion:^(BOOL success, int hasRestreamed) {
                                    if(success){
                                        [DIALOG_MANAGER showCompleteToast];
                                    }else{
                                        [DIALOG_MANAGER showNetworkFailToast];
                                    }
                                }];
                            }
                        }];
                    });
                }
            }];
            
        } else if([array[selectedOption] isEqualToString:LOCALIZE(@"promote_to_top")]){
            
            [API_MANAGER promoteTopLiveStream:cell.livestream.liveStreamID withCompletion:^(BOOL success) {
                if(success){
                    [DIALOG_MANAGER showCompleteToast];
                }else{
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
        } else if([array[selectedOption] isEqualToString:LOCALIZE(@"cancel_promote_to_top")]){
            
            [API_MANAGER cancelPromoteTopLiveStream:cell.livestream.liveStreamID withCompletion:^(BOOL success) {
                if(success){
                    [DIALOG_MANAGER showCompleteToast];
                }else{
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];
            
        }
    }];
}

#pragma mark - Actions

-(void)didClickFindFriend:(id)sender
{
//    [API_MANAGER getSuggestedUsers:0 count:100 completion:^(BOOL success, NSArray *suggestedUser) {
//        [_progressView stopAnimating];
//        if(success){
//            [DIALOG_MANAGER showCompleteToast];
//        }else{
//            [DIALOG_MANAGER showNetworkFailToast];
//        }
//    }];


//    ConnectionViewController* conVC = [ConnectionViewController new];
//    conVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:conVC animated:YES];
    
    FindFriendViewController* findFriendVC = [FindFriendViewController new];
    findFriendVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:findFriendVC animated:YES];
    
}

- (void)onClickGridModeButton:(UIButton *)sender
{
    sender.selected = !sender.selected;
    self.isGridMode = sender.selected;
    _gridModeCount++;
    NSIndexPath *indexPath = [[_feedCollectionView indexPathsForVisibleItems] firstObject];
    [_feedCollectionView reloadData];
    if (_isGridMode) {
        [_feedCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    }
    else {
        [_feedCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    }
    _feedCollectionView.contentOffset = CGPointMake(_feedCollectionView.contentOffset.x, _feedCollectionView.contentOffset.y-STATUS_BAR_HEIGHT-NAVI_BAR_HEIGHT);
    [self moveNavAndTabBar:0 animated:NO];
}

@end
