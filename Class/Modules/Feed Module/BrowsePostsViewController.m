//
//  BrowsePostsViewController.m
//  story17
//
//  Created by POPO Chen on 5/27/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "BrowsePostsViewController.h"
#import "Constant.h"
#import "PostGridCell.h"
#import "SVPullToRefresh.h"
#import "SinglePostViewController.h"

static NSString * const kReuseCellIdentifier = @"PostGridCell";

@implementation BrowsePostsViewController

#define Padding 2
#define listBtnHeight 40


- (id)init
{
    self = [super init];
    if (self)
    {
        _tagposts = CREATE_MUTABLE_ARRAY;
        _dismissNavbar = false;
    }
    
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self addCustomNavigationBackButton];
    [self configureViewForIOS7];
    
    _isFirstFetching = YES;
    
    _bgTapView = [[UIView alloc]initWithFrame:CGRectMake(0, -NAVI_BAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.view addSubview:_bgTapView];
    
    _btnView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, listBtnHeight)];
    
    _gridButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, listBtnHeight)];
    [_gridButton setImage:[UIImage imageNamed:@"profile_grid"] forState:UIControlStateNormal];
    [_gridButton setImage:[UIImage imageNamed:@"profile_grid_active"] forState:UIControlStateSelected];
    [_gridButton setSelected:YES];
    [_gridButton addTarget:self action:@selector(changeMode:) forControlEvents:UIControlEventTouchUpInside];
    
    _listButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2, listBtnHeight)];
    [_listButton addTarget:self action:@selector(changeMode:) forControlEvents:UIControlEventTouchUpInside];
    [_listButton setImage:[UIImage imageNamed:@"profile_list"] forState:UIControlStateNormal];
    [_listButton setImage:[UIImage imageNamed:@"profile_list_active"] forState:UIControlStateSelected];
    
    UIView* separaterLine = [ThemeManager separaterLine];
    separaterLine.frame = CGRectMake(0, listBtnHeight-0.5, SCREEN_WIDTH, 0.5);
    UIView* separaterLine2 = [ThemeManager separaterLine];
    separaterLine2.frame = CGRectMake(SCREEN_WIDTH/2, 3, 0.5, listBtnHeight-6);
    
    [_btnView addSubview:_listButton];
    [_btnView addSubview:_gridButton];
    [_btnView addSubview:separaterLine];
    [_btnView addSubview:separaterLine2];
    
    UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [layout setMinimumInteritemSpacing:Padding];
    [layout setMinimumLineSpacing:Padding];
    
    _mainColletionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, listBtnHeight, SCREEN_WIDTH, SCREEN_HEIGHT-STATUS_BAR_HEIGHT-NAVI_BAR_HEIGHT-listBtnHeight) collectionViewLayout:layout];
    [_mainColletionView setDataSource:self];
    [_mainColletionView setDelegate:self];
    [_mainColletionView registerNib:[UINib nibWithNibName:kReuseCellIdentifier bundle:nil] forCellWithReuseIdentifier:kReuseCellIdentifier];
    [_mainColletionView registerClass:[PostListCell class] forCellWithReuseIdentifier:@"postListCell"];

    [_mainColletionView setAlwaysBounceVertical:YES];
    [_mainColletionView setBackgroundColor:LIGHT_BG_COLOR];
    
    
    _progressView = [UIActivityIndicatorView new];
    _progressView.frame = CGRectMake(SCREEN_WIDTH/2-15, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-TAB_BAR_HEIGHT)/2-50, 30, 30);
    [_progressView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_progressView setColor:MAIN_COLOR];

    [self.view addSubview:_progressView];

//    __weak BrowsePostsViewController* weakSelf = self;
//    [_mainColletionView addPullToRefreshWithActionHandler:^{
//        [weakSelf fetchData:YES];
//    }];
    
    [self.view addSubview:_mainColletionView];
    [self.view addSubview:_btnView];

    _noDataImageView = [ThemeManager getNoDataImageView];
    [self.view addSubview:_noDataImageView];

    [self fetchData:true];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
//    if(self.isBeingDismissed==YES){
//        DLog(@"toparent");
//    }else
//    {
//        DLog(@"notmovie");
//    }
    
    NSArray *viewControllers = self.navigationController.viewControllers;

    if (viewControllers.count > 1 && [viewControllers objectAtIndex:viewControllers.count-2] == self) {
        
    } else if ([viewControllers indexOfObject:self] == NSNotFound) {
        
        if(_dismissNavbar){
            [self.navigationController setNavigationBarHidden:YES animated:YES];
        }
    }

}


-(void)changeMode:(UIButton*)sender
{
    if([sender isEqual:_gridButton]){
        [_gridButton setSelected:YES];
        [_listButton setSelected:NO];
    }else{
        [_gridButton setSelected:NO];
        [_listButton setSelected:YES];
    }
    [_mainColletionView reloadData];
}

-(void)fetchData:(BOOL)refresh
{
    if(refresh){
        _noMoreData = false;
    }
    if(_isFetching || _noMoreData || _tagQuery==NULL) return;
    
//    [_mainColletionView.pullToRefreshView startAnimating];
    
    
    PostObject* lastPostObject = NULL;
    int beforeTime = INT32_MAX;
    int offset = 0;
    
    if([_tagposts count]>0){
        lastPostObject = _tagposts[[_tagposts count]-1];
        if(!refresh){
            offset = (int)[_tagposts count];
        }
    }
    
    if(lastPostObject==NULL || refresh){
        beforeTime = INT32_MAX;
    }else{
        beforeTime = lastPostObject.timestamp;
    }
    
    
    _isFetching = TRUE;
    if(_isFirstFetching){
        [_progressView startAnimating];
        _isFirstFetching = false;
    }
    
    
    [API_MANAGER getPostByHashTag:_tagQuery beforeTime:beforeTime count:27 withCompletion:^(BOOL success, NSArray *posts) {
        [_progressView stopAnimating];
        [_mainColletionView.pullToRefreshView stopAnimating];
        _isFetching = false;
        
        if(refresh){
            [_tagposts removeAllObjects];
        }
        if([posts count]==0 && [_tagposts count]==0){
            _noDataImageView.hidden = NO;
        }else{
            _noDataImageView.hidden = YES;
        }
        
        for(PostObject* post in posts){
            [_tagposts addObject:post];
        }
        
        if([posts count]<10){
            _noMoreData = true;
        }
        
        [_mainColletionView reloadData];

    }];

}



#pragma mark - data source
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
//    if (scrollView.contentOffset.y > 0)
//        [self fetchPostDataWithRefresh:NO];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_gridButton isSelected]){
         return CGSizeMake((SCREEN_WIDTH)/3-4,(SCREEN_WIDTH)/3-4);
    }else{
        return CGSizeMake(SCREEN_WIDTH, [PostListCell getCellHeigthWithPost:_tagposts[indexPath.row]]);
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_tagposts count];
    
//    if(section==0){
//        return 1;
//    }else{
//        return [_tagposts count];
//    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row>=[_tagposts count]-14){
        [self fetchData:NO];
    }

    if([_gridButton isSelected]){
        PostGridCell* cell = (PostGridCell*)[collectionView dequeueReusableCellWithReuseIdentifier:kReuseCellIdentifier forIndexPath:indexPath];
        cell.post = _tagposts[indexPath.row];
        
        return cell;
    }else{
        
        PostListCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"postListCell" forIndexPath:indexPath];
        cell.navCtrl = self.navigationController;
        
        if (cell == nil)
        {
            CGRect rect = CGRectMake(0, 0, self.view.bounds.size.width, [PostListCell getCellHeigth]);
            cell = [[PostListCell alloc] initWithFrame:rect];
        }
        cell.post =(PostObject *)_tagposts[indexPath.row];
        return cell;

    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(3, 3, 3, 3);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    id cell = [_mainColletionView cellForItemAtIndexPath:indexPath];
    if (![cell isKindOfClass:[PostGridCell class]])
        return;
    
    SinglePostViewController* ctrl = nil;
    if (ctrl == nil)
        ctrl = [[SinglePostViewController alloc] init];
    
    ctrl.enterMode=EnterFromBrowser;
    ctrl.post = [_tagposts objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:ctrl animated:YES];
}

#pragma mark - chage post style
- (void)tabClickWithType:(UserProfileTabType)type
{
//    _type = type;
//    
//    if (_section == 3)
//        [_collectionView reloadSections:[[NSIndexSet alloc] initWithIndex:2]];
//    else
//        [_collectionView reloadSections:[[NSIndexSet alloc] initWithIndex:3]];
}

-(void)didClickPostImage:(UITapGestureRecognizer *)tapGesture withPostID:(NSString *)postID
{
    CGPoint touchPoint = [tapGesture locationInView:_bgTapView];
    DLog(@"%f %f",touchPoint.x,touchPoint.y);
    [DIALOG_MANAGER showHeartFromPoint:touchPoint];
}



@end
