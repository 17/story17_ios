//
//  CommentViewController.m
//  Story17
//
//  Created by POPO on 2015/5/5.
//  Copyright (c) 2015年 POPO_INNOVATION. All rights reserved.
//

#import "CommentViewController.h"
#import "Constant.h"
#import "CommentCell.h"
#import "SearchUserCell.h"
#import "TagViewCell.h"
#import "MultiFileUploader.h"

@interface CommentViewController()
@property float lastContentY;

@property BOOL isFetching;
@property BOOL isFirstFetching;
@property BOOL noMoreData;

@end


@implementation CommentViewController

#define userIdentity @"userIdentity"
#define tagIdentity @"tagIdentity"

#define userCellHeight 55;
#define tagCellHeight 55;


#define fetchNum 50

@synthesize commentsArray = _commentsArray;

-(id)init
{
    self = [super init];
    
    if(self){
        
        _isFetching = NO;
        _noMoreData = NO;
        _isFirstFetching = YES;
        _isFirstOpen = YES;
        
        _searchArray = CREATE_MUTABLE_ARRAY;
        _taggedUsers = CREATE_MUTABLE_ARRAY;
        _commentsArray = CREATE_MUTABLE_ARRAY;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    
    self.title = LOCALIZE(@"Comments");
    
    if (self.commentsTableView == nil)
    {
        
        _lastContentY = 0.0f;
        
        _showedCommentCell = nil;

        CGRect rect = self.view.bounds;
        rect.size.height -= NAVI_BAR_HEIGHT + STATUS_BAR_HEIGHT;
        
        self.commentsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-STATUS_BAR_HEIGHT-NAVI_BAR_HEIGHT-44.0) style:UITableViewStylePlain];
        [self.commentsTableView setDelegate:self];
        [self.commentsTableView setDataSource:self];
        [self.view addSubview:self.commentsTableView];
        [self.commentsTableView registerClass:[CommentCell class] forCellReuseIdentifier:@"CommentCell"];
        
        UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
        panGesture.cancelsTouchesInView = NO;

//        [self.commentsTableView addGestureRecognizer:panGesture];
        self.commentsTableView.separatorStyle = UITableViewCellSelectionStyleNone;
        [self.commentsTableView setBackgroundColor:LIGHT_BG_COLOR];
        
        self.tagSuggestionTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-STATUS_BAR_HEIGHT-NAVI_BAR_HEIGHT-44.0) style:UITableViewStylePlain];
        [self.tagSuggestionTableView setDelegate:self];
        [self.tagSuggestionTableView setDataSource:self];
        [self.view addSubview:self.tagSuggestionTableView];
        self.tagSuggestionTableView.separatorStyle = UITableViewCellSelectionStyleNone;
        [self.tagSuggestionTableView setBackgroundColor:LIGHT_BG_COLOR];
        self.tagSuggestionTableView.hidden = YES;
        
        int hotspotHeight = 0;
        
        if(ON_HOTSPOT){
            hotspotHeight  = 20;
            _messageInputBar = [[MessageInputBar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-hotspotHeight+3, SCREEN_WIDTH, 44.0)];
        }else{
            _messageInputBar = [[MessageInputBar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-NAVI_BAR_HEIGHT+3, SCREEN_WIDTH, 44.0)];
        }
        
        _messageInputBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        _messageInputBar.chatVC = self;
        _messageInputBar.hidden = NO;
        [self.view addSubview:_messageInputBar];
        
        [_messageInputBar.sendMessageButton addTarget:self
                                              action:@selector(sendTextMessage)
                                    forControlEvents:UIControlEventTouchUpInside];
        _messageInputBar.messageTextView.keyboardType = UIKeyboardTypeTwitter;

        _noDataImageView = [ThemeManager getNoDataImageView];
        CGRect tmpRect = _noDataImageView.frame;
        tmpRect.origin.y = 0;
        _noDataImageView.frame = tmpRect;
                
        _commentsTableView.hidden = YES;
        
        [self.view addSubview:_noDataImageView];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(keyboardNotificationHandler:) name: UIKeyboardDidChangeFrameNotification object:nil];
    
    if(_isFirstOpen){
        [self fetchComment:YES];
        _isFirstOpen = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [_messageInputBar.messageTextView resignFirstResponder];
    [NOTIFICATION_CENTER removeObserver:self name:UIKeyboardDidChangeFrameNotification object:nil];
}

- (void)setCommentsArray:(NSArray *)commentsArray
{
    _commentsArray = [commentsArray mutableCopy];
    [self.commentsTableView reloadData];
}

- (void)setPost:(PostObject*)post
{    
    _post = post;
    
//    DLog(@"Get Post ID %@ Comments", post);
    
    if (_post.postID == nil)
    {
        self.commentsArray = nil;
    }
    else
    {
//        [DIALOG_MANAGER showLoadingView];
//        [API_MANAGER getComments:_post.postID beforeTime:INT32_MAX count:1000 withCompletion:^(BOOL success, NSArray* comments){
//            
//            [DIALOG_MANAGER hideLoadingView];
//            if (!success)
//                return;
//            
//            self.commentsArray = [comments mutableCopy];
//            [_commentsTableView reloadData];
//            
//            [self scrollToBottomAnimated:NO];
//
//            
//            if([comments count]==0){
//                _noDataImageView.hidden = NO;
//            }else{
//                _noDataImageView.hidden = YES;
//            }
//            
//            [_messageInputBar resizeView];
//            
//            _messageInputBar.hidden = NO;
//            _commentsTableView.hidden = NO;
//            
//            
//        }];
    }
}

-(void)fetchComment:(BOOL)refresh
{
    if(_post==nil)
        return;
    
    if(refresh){
        _noMoreData = NO;
    }
    
    if(_noMoreData || _isFetching){
        return;
    }
    
    if(_isFirstFetching){
    }
    
    int beforeTime = INT32_MAX;
    if([_commentsArray count]!=0){
        CommentObject* commObj = _commentsArray[0];
        beforeTime = commObj.timestamp;
    }
    
    _isFetching = YES;
    [DIALOG_MANAGER showLoadingView];
    
    [API_MANAGER getComments:_post.postID beforeTime:beforeTime count:fetchNum withCompletion:^(BOOL success, NSArray *comments) {
        
        [DIALOG_MANAGER hideLoadingView];
        _isFetching = NO;
        if (!success)
            return;

        
        for(CommentObject* comm in comments){
            [_commentsArray insertObject:comm atIndex:0];
        }

        [_commentsTableView reloadData];

        if([comments count]<fetchNum){
            _noMoreData = YES;
        }
        
        if(refresh){
            [self scrollToBottomAnimated:NO];
        }else{
            
            
//            NSArray* previsibleCell = [[_commentsTableView visibleCells] copy];
//            int row = INT32_MAX;
//            for(CommentCell* cell in previsibleCell){
//                
//                if((int)[_commentsTableView indexPathForCell:cell].row<row){
//                    row =  (int)[_commentsTableView indexPathForCell:cell].row;
//                }
//            }
            
            NSIndexPath* indexPath = [NSIndexPath indexPathForRow:[comments count] inSection:0];
            [_commentsTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];

            
//            if(row< [_commentsArray count])
//                [_commentsTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        
        }


        if([_commentsArray count]==0){
            _noDataImageView.hidden = NO;
        }else{
            _noDataImageView.hidden = YES;
        }

        [_messageInputBar resizeView];
        
        _messageInputBar.hidden = NO;
        _commentsTableView.hidden = NO;
    }];
    
    
}

#pragma mark - Table View data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([tableView isEqual:_commentsTableView]){
        return self.commentsArray != nil ? [self.commentsArray count] : 0;
    }else if([tableView isEqual:_tagSuggestionTableView]){
        if(_type!=NoneTagging){
            return _searchArray != nil ? [_searchArray count] : 0;
        }else{
            return 0;
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView isEqual:_commentsTableView]){
        
        CommentCell* cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell"];
        if (cell == nil){
            cell = [[CommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CommentCell"];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.navCtrl = self.navigationController;
        cell.delegate = self;
        if([_post.user.userID isEqualToString:MY_USER_ID]){
            cell.isSelfPost = YES;
            [cell setBackgroundColor:LIGHT_MAIN_COLOR];
        }else{
            cell.isSelfPost = NO;
            [cell setBackgroundColor:WHITE_COLOR];
        }
        
        if(indexPath.row <=0 ){
            [self fetchComment:NO];
        }
        
        cell.comment = [self.commentsArray objectAtIndex:indexPath.row];
        
        return cell;
    
    }else if([tableView isEqual:_tagSuggestionTableView]){
        
        if(_type==UserTagging){
            SearchUserCell *cell = [tableView dequeueReusableCellWithIdentifier:userIdentity];
            if(cell==nil){
                cell = [[SearchUserCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:userIdentity];
            }
            UserObject* user = _searchArray[indexPath.row];
            [cell reloadCell:user];
            cell.followBtn.hidden = YES;
            return cell;
        }else if(_type==HashTagging){
            TagViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tagIdentity];
            if(cell==nil){
                cell = [[TagViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:tagIdentity];
            }
            
            TagObject* tag = _searchArray[indexPath.row];
            [cell reloadCell:tag];
            return cell;
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([tableView isEqual:_commentsTableView]){
        
        CGFloat cellHeight = [CommentCell getHeightFromCommentObj:_commentsArray[indexPath.row]];
        
        if (indexPath.row == self.commentsArray.count-1) {
            self.lastCellHeight = cellHeight;
        }
        return cellHeight;
        
    }else{
        if(_type==UserTagging){
            return userCellHeight;
        }else if(_type==HashTagging){
            return tagCellHeight;
        }
    }
    return 0;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

#pragma mark - message input box
- (void)messageInputBar:(MessageInputBar *)bar willEditWithKeybordHeight:(float)keybordHeight
{
    CGRect rect = self.commentsTableView.frame;
    rect.size.height -= keybordHeight+TAB_BAR_HEIGHT;
    [self.commentsTableView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-2*NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-keybordHeight-TAB_BAR_HEIGHT)];
    [self.tagSuggestionTableView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-2*NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-keybordHeight-TAB_BAR_HEIGHT)];
    
    rect = self.messageInputBar.frame;
    rect.origin.y -= keybordHeight + TAB_BAR_HEIGHT;
    [self.messageInputBar setFrame:rect];
}

- (void)sendTextMessage
{

    NSString *text = self.messageInputBar.messageTextView.text;
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if([text isEqualToString:@""]) {
        return;
    }
    
    BOOL success = [self sendMessageWithType:@"text" andContent:text];
    
    if(success) {
        self.messageInputBar.messageTextView.text = @"";
        [self.messageInputBar.messageTextView setText:nil];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.messageInputBar textViewDidChange:self.messageInputBar.messageTextView];
        });
    }
}

-(BOOL)sendMessageWithType:(NSString*)type andContent:(NSString*) content
{
    if ([GET_DEFAULT(PICTURE)isEqualToString:@""]) {
        [self uploadProfilePicture];
        return NO;
    }else{
        if(_post==nil)
        {
            return NO;
        }
        [self getAllTagers];
        NSString* alertString = @"";
        if([_taggedUsers count]>10){
            alertString = LOCALIZE(@"no_more_than_ten_people");
        }
        
        if(![alertString isEqualToString:@""])
        {
            //        [DIALOG_MANAGER showOkDialog:@"" message:@"" buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {}];
            return NO;
        }
        
        _tagSuggestionTableView.hidden = YES;
        
        
        if(_post.postID==nil)
            return NO;
        
        [API_MANAGER commentPost:_post.postID userTags:TO_JSON(_taggedUsers) comment:content withCompletion:^(BOOL success, NSString *message, NSString *timestamp, NSString *commentID) {
            if (_post.user.userID!=nil) {
                [EVENT_HANDLER addEventTracking:@"CommentPost" withDict:@{@"targetUserID":_post.user.userID,@"content":content}];
            }
            if (success){
                if(commentID !=nil){
                    _post.commentCount += 1;
                    CommentObject* comment = _commentsArray[[_commentsArray count]-1];
                    comment.commentID = commentID;   // assign id for handle delete comment
                }
            }
        }];
        
        
        CommentObject* comment = [CommentObject new];
        comment.userID = MY_USER_ID;
        comment.comment = content;
        comment.timestamp = CURRENT_TIMESTAMP;
        comment.user = [SINGLETON getSelfUserObject];
        [_commentsArray addObject:comment];
        [self.commentsTableView reloadData];
        
        _noDataImageView.hidden = YES;
        _commentsTableView.hidden =NO;
        
        [self scrollToBottomAnimated:YES];
        return YES;
  
    }
    
}

-(void) repositionTableViewWithAnimation:(BOOL) animated
{

    
    if(_messageInputBar.mode==GAME_MODE) {
        return;
    }
    
    static float lastExecutionTime = 0.0;
    float currentTime = CACurrentMediaTime();
    
    if(currentTime-lastExecutionTime<0.05) {
        animated = NO;
    }
    
    lastExecutionTime = currentTime;
    
    // check if need to scroll to bottom
    self.needScrollToBottom = [self isLastCellVisible];
    
    int messageInputBarHeight = _messageInputBar.messageInputBarHeight;
    int keyboardHeight = _messageInputBar.keyboardHeight;
    
    /* resize chatTableView => very tricky and complicated algorithm, do not modify! */
    int newHeight = SCREEN_HEIGHT-STATUS_BAR_HEIGHT-NAVI_BAR_HEIGHT-messageInputBarHeight-keyboardHeight;
    
    int originalHeight = self.commentsTableView.frame.size.height;
    
    if(self.needScrollToBottom==NO&&newHeight>originalHeight) {
        animated = NO;
    }
    
    if(animated==YES) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.25];
        [UIView setAnimationCurve:7]; // undocumented hack!
    }
    
    [_commentsTableView setFrame:CGRectMake(0, 0, _commentsTableView.frame.size.width, newHeight)];
    
    if([self isLastCellVisible]==YES) {
        // need to scroll to bottom!
        int viewOffset = newHeight-originalHeight;
        if(newHeight>originalHeight) {
            self.commentsTableView.contentOffset = CGPointMake(0, self.commentsTableView.contentOffset.y-viewOffset/2.0);
            [self.commentsTableView reloadData];
            [self.commentsTableView setFrame:CGRectMake(0, 0, self.commentsTableView.frame.size.width, newHeight)];
        }
        
        static BOOL isNowRepositioningView = NO;
        
        if(newHeight<originalHeight&&isNowRepositioningView==NO) {
            isNowRepositioningView = YES;
            
            BOOL needToUpdateContentOffset = YES;
            
            if(self.commentsTableView.contentSize.height<newHeight || self.commentsTableView.contentSize.height<originalHeight) {
                needToUpdateContentOffset = NO;
            }
            
            if(needToUpdateContentOffset==YES) {
                [self.commentsTableView setFrame:CGRectMake(0, viewOffset, self.commentsTableView.frame.size.width, originalHeight)];
                
                double delayInSeconds = 0.25;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [self.commentsTableView setFrame:CGRectMake(0, 0, self.commentsTableView.frame.size.width, newHeight)];
                    self.commentsTableView.contentOffset = CGPointMake(0, self.commentsTableView.contentOffset.y-viewOffset);
                    isNowRepositioningView = NO;
                    
                    // check if need to scroll tableview to bottom
                    if(self.needScrollToBottom==YES) {
                        [self scrollToBottomAnimated:NO];
                    }
                });
            } else {
                isNowRepositioningView = NO;
                [self.commentsTableView setFrame:CGRectMake(0, 0, self.commentsTableView.frame.size.width, newHeight)];
            }
        }
    } else {
        [self.commentsTableView setFrame:CGRectMake(0, 0, self.commentsTableView.frame.size.width, newHeight)];
    }
    
    [_messageInputBar setFrame:CGRectMake(0, SCREEN_HEIGHT-STATUS_BAR_HEIGHT-NAVI_BAR_HEIGHT-_messageInputBar.keyboardHeight-_messageInputBar.messageInputBarHeight, SCREEN_WIDTH, messageInputBarHeight)];
    
    if(animated==YES) {
        [UIView commitAnimations];
    }
    
    if(self.commentsTableView.contentOffset.y>0 && self.commentsTableView.contentSize.height<self.commentsTableView.frame.size.height){
        [self.commentsTableView setContentOffset:CGPointMake(0, 0)];
        return;
    }
    
    // check if need to scroll tableview to bottom
    if(self.needScrollToBottom==YES) {
        [self scrollToBottomAnimated:NO];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([tableView isEqual:_tagSuggestionTableView]){
        NSString* appendStr = @"";
        
        if(_type==UserTagging){
            
            UserObject* user = [_searchArray objectAtIndex:indexPath.row];
            appendStr = user.openID;
            
        }else if(_type==HashTagging){
            TagObject* tag = [_searchArray objectAtIndex:indexPath.row];
            appendStr = tag.hashTag;
        }
        
        _messageInputBar.messageTextView.text = [self getNewstringFromSelectedstring:appendStr];
        
        _type = NoneTagging;
        [_searchArray removeAllObjects];
        _tagSuggestionTableView.hidden = YES;

    }
}

-(BOOL) isLastCellVisible
{
    if(self.commentsTableView.contentSize.height-self.lastCellHeight<self.commentsTableView.contentOffset.y+self.commentsTableView.frame.size.height+10.0) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Helper Methods
- (void)scrollToBottomAnimated:(BOOL)animated
{
    NSInteger rows = [self.commentsTableView numberOfRowsInSection:0];
    
    if (rows > 0) {
        [self.commentsTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:rows - 1 inSection:0]
                                     atScrollPosition:UITableViewScrollPositionBottom
                                             animated:animated];
    }
    
    self.isAtButtom = YES;
}

-(void)hideKeyboard
{
    [_messageInputBar.messageTextView resignFirstResponder];
}


#pragma mark - Tagging Handler
-(void)searchModeDetected:(NSString*)searchString
{
    
    _searchString = searchString;
    
    if([_commentsArray count]==0 && [_searchString isEqualToString:@""]){
        _noDataImageView.hidden = NO;
    }else{
        _noDataImageView.hidden = YES;
    }
    
    if( [_searchString isEqualToString:@""]){
        _type = NoneTagging;
        [_searchArray removeAllObjects];
        _tagSuggestionTableView.hidden = YES;
        return;
    }

    if( [[_searchString substringToIndex:1] isEqualToString:@"#"]){
        _type = HashTagging;
    }else if( [[_searchString substringToIndex:1] isEqualToString:@"@"]){
        _type = UserTagging;
    }
    
    if(_searchString.length==1){
        _type = NoneTagging;
    }

    if(_type==NoneTagging){
        _tagSuggestionTableView.hidden = YES;
        return;
    }
    
    if(_type==HashTagging){
        
        [API_MANAGER searchHashTag: [_searchString substringFromIndex:1] offset:0 count:100 withCompletion:^(BOOL success, NSArray *tags) {
            _tagSuggestionTableView.hidden = YES;
            if(success){
                [_searchArray removeAllObjects];
                for(TagObject* tag in tags){
                    [_searchArray addObject:tag];
                }
                [_tagSuggestionTableView reloadData];
                
                if([_searchArray count]!=0){
                    if(_type != NoneTagging){  // current mode is diff
                        _tagSuggestionTableView.hidden = NO;
                    }
                }
            }
        }];
    }else if(_type==UserTagging){
        
        NSString* query = [_searchString substringFromIndex:1];
        
        [API_MANAGER getSearchUsers:query followingOnly:1 offset:0 count:100 completion:^(BOOL success, NSArray *userObjects) {
            _tagSuggestionTableView.hidden = YES;
            if(success){
                [_searchArray removeAllObjects];
                for(UserObject* user in userObjects){
                    [_searchArray addObject:user];
                }
                [_tagSuggestionTableView reloadData];
                if([_searchArray count]!=0){
                    if(_type != NoneTagging){  // current mode is diff
                        _tagSuggestionTableView.hidden = NO;
                    }
                }
            }
        }];
    }
}



-(NSString*)getNewstringFromSelectedstring:(NSString*)selectedstring
{
    NSString* trimString = [_messageInputBar.messageTextView.text substringToIndex:_messageInputBar.messageTextView.text.length-_searchString.length];
    
    if(_type==HashTagging){
        return [NSString stringWithFormat:@"%@#%@ ",trimString,selectedstring];
    }else if(_type==UserTagging){
        return [NSString stringWithFormat:@"%@@%@ ",trimString,selectedstring];
    }
    return @"";
}


-(void)getAllTagers
{
    [_taggedUsers removeAllObjects];
    
    if([_messageInputBar.messageTextView.text isEqualToString:@""])
        return;
    
    NSArray* userArray = [_messageInputBar.messageTextView.text componentsSeparatedByString:@"@"];
    if([userArray count]>=1){
        for(int i =1;i< [userArray count];i++){
            NSString* preString = userArray[i-1];
            if([preString hasSuffix:@" "] || [preString isEqualToString:@""]){
                [_taggedUsers addObject:[[userArray[i] componentsSeparatedByString:@" "][0] lowercaseString]];
            }
        }
    }
}


#pragma mark - CommentCellDelegate

- (void)cell:(CommentCell *)cell didShowMenu:(BOOL)isShowingMenu
{
    if(isShowingMenu){
        
        if(_showedCommentCell !=nil && _showedCommentCell!=cell){
            [_showedCommentCell dismissCell];
        }
        _showedCommentCell = cell;
        
    }else{
        
    
    }
}

- (void)cellDidSelectDelete:(CommentCell *)cell
{
    if(cell.comment.commentID==nil){
        return;
    }
    
    NSIndexPath *indexPath = [_commentsTableView indexPathForCell:cell];
    
    if(indexPath==nil){
        return;
    }
    
    [API_MANAGER deleteComment:cell.comment.commentID postID:_post.postID withCompletion:^(BOOL success) {
        if(success){

        }
    }];
    
    [_commentsTableView beginUpdates];
    [_commentsArray removeObjectAtIndex:indexPath.row];
    [_commentsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [_commentsTableView endUpdates];
    
    if([_commentsArray count]==0){
        _noDataImageView.hidden = NO;
    }else{
        _noDataImageView.hidden = YES;
    }
    
}

- (void)cellDidSelectRepost:(CommentCell *)cell
{
    NSString* userOpenID = cell.comment.user.openID;
    _messageInputBar.messageTextView.text = [NSString stringWithFormat:@"%@ @%@ ",_messageInputBar.messageTextView.text,userOpenID];
    [_messageInputBar.messageTextView becomeFirstResponder];
    
    if(_showedCommentCell !=nil){
        [_showedCommentCell dismissCell];
        _showedCommentCell = nil;
    }
}

-(void)cellDidSelectReport:(CommentCell *)cell
{
    DLog(@"cellDidSelectReport");
    [_messageInputBar.messageTextView resignFirstResponder];
}


#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(_showedCommentCell !=nil){
        [_showedCommentCell dismissCell];
        _showedCommentCell = nil;
    }
        
    if(scrollView==_commentsTableView){
        
        switch (scrollView.panGestureRecognizer.state) {
                
            case UIGestureRecognizerStateBegan:
                
                break;
                
            case UIGestureRecognizerStateChanged:
                if(_lastContentY>scrollView.contentOffset.y){
                    [self hideKeyboard];
                }
                break;
                
            case UIGestureRecognizerStatePossible:
                
                break;
                
            default:
                break;
        }
        _lastContentY = scrollView.contentOffset.y;

    }
    
}

-(void)keyboardNotificationHandler:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    _tagSuggestionTableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-keyboardFrame.size.height-_messageInputBar.frame.size.height);
}

- (void)uploadProfilePicture
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"Please_verify_your_photo_before_live") message:@"" buttonText:LOCALIZE(@"OK") cancelable:YES withCompletion:^(BOOL okClicked) {
            if (okClicked) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    [DIALOG_MANAGER showActionSheetDialogTitle:@"" options:@[LOCALIZE(@"take_picture"),LOCALIZE(@"albom")] destructiveIndexes:@[] cancelable:YES withCompletion:^(int selectedOption) {
                        if(selectedOption==1){
                            
                            // 從相機膠卷選擇
                            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                            [[UIBarButtonItem appearance] setBackgroundImage:nil  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
                            [[UIBarButtonItem appearance] setBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
                            
                            imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
                            imagePicker.delegate = (id) self;
                            imagePicker.allowsEditing = YES;
                            
                            
                            [self presentViewController:imagePicker animated:YES completion:^{
                                
                            }];
                        }else if (selectedOption==0){
                            
                            // 照相
                            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                            imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
                            imagePicker.cameraDevice=UIImagePickerControllerCameraDeviceFront;
                            imagePicker.delegate = (id) self;
                            imagePicker.allowsEditing = YES;
                            [self presentViewController:imagePicker animated:YES completion:^{
                                
                            }];
                        }
                    }];});
                
            }
        }];
        
    });
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [SINGLETON customizeInterface];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        UIImage* imageTaken = [info valueForKey: UIImagePickerControllerEditedImage];
        
        if(picker.sourceType!=UIImagePickerControllerSourceTypePhotoLibrary){
            [SINGLETON saveImageWithWhiteBackgroundWithoutCompleteMessage:imageTaken];
        };
        
        [DIALOG_MANAGER showLoadingView];
        
        NSString* profilePictureFileName = [SINGLETON generateRandomFileNameWithExtension:@"jpg"];
        [SINGLETON saveImageWithThumbnailImage:imageTaken andFileName:profilePictureFileName];
        
        // Handle the result image here
        
        DLog(@"%@",profilePictureFileName);
        NSError *err = nil;
        NSData *data = [NSData dataWithContentsOfFile:GET_LOCAL_FILE_PATH(profilePictureFileName)
                                              options:NSDataReadingUncached
                                                error:&err];
        
        NSLog(@"File size is : %.2f MB",(float)data.length/1024.0f/1024.0f);
        
        self.multiFileUploader = [[MultiFileUploader alloc] initWithFileNameArray:@[profilePictureFileName, [NSString stringWithFormat:@"THUMBNAIL_%@", profilePictureFileName]]];
        
        [self.multiFileUploader startUploadWithCallback:^(NSArray *fileNameArray, int code , float progress) {
            
            if(code==MULTI_UPLOAD_SUCCESS) {
                // UPLOAD SUCCESS UPDATE USER INFO
                
                [API_MANAGER  updateUserInfo:@{@"picture":profilePictureFileName}  fetchSelfInfo:YES  completion:^(BOOL success) {
                    [DIALOG_MANAGER hideLoadingView];
                    if(success){
                        //                      [DIALOG_MANAGER SHOwACtion]
                    }
                    
                }];
                
            } else if(code==MULTI_UPLOAD_FAIL) {
                [DIALOG_MANAGER hideLoadingView];
                [DIALOG_MANAGER showNetworkFailToast];
            } else if(code==PROGRESSING){
                
            }
        }];
    }];
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [SINGLETON customizeInterface];
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}


@end
