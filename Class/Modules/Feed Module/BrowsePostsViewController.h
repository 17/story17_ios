//
//  BrowsePostsViewController.h
//  story17
//
//  Created by POPO Chen on 5/27/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserProfileTabCell.h"
#import "PostListCell.h"

@interface BrowsePostsViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,UserProfileTabCellDelegate>

@property (nonatomic,strong) UICollectionView* mainColletionView;
@property (nonatomic,strong) NSMutableArray* tagposts;

@property (nonatomic,strong) UIImageView* noDataImageView;
@property (nonatomic,strong) NSString* tagQuery;
@property (nonatomic,strong) UIActivityIndicatorView* progressView;

@property (nonatomic,strong) UIView* btnView;
@property (nonatomic,strong) UIButton* listButton;
@property (nonatomic,strong) UIButton* gridButton;

@property (nonatomic,strong) UIView* bgTapView;


@property BOOL isFirstFetching;
@property BOOL isFetching;
@property BOOL noMoreData;
@property(nonatomic)BOOL dismissNavbar;

@end
