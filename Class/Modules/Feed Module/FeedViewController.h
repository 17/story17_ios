//
//  FeedViewController.h
//  story17
//
//  Created by POPO Chen on 5/4/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostListCell.h"
#import "PublishCell.h"
#import "PublishPostHandler.h"
#import "LivestreamCell.h"
#import "EventHandler.h"

@interface FeedViewController : UIViewController <PostCellDelegate,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UIScrollViewDelegate,PublishProgressingDelegate,PublishCellDelegate,LivestreamCellDelegate>
@property(nonatomic)int feedEnterTimeStamp;
@property(nonatomic)int feedRefreshCount;
@property(nonatomic)int feedVisibleCellCount;
@property(nonatomic)int gridModeCount;


@property(nonatomic, strong)UICollectionView* feedCollectionView;
@property(nonatomic, strong)UIView* bgTapView;
@property(nonatomic, strong)NSMutableArray* postArray;
@property(nonatomic, strong)NSArray* adArray;
@property (nonatomic) CGFloat lastContentOffset;

@property(nonatomic)BOOL isForground;

@property(nonatomic)BOOL isFirstFetching;
@property(nonatomic)BOOL noMoreData;
@property(nonatomic)BOOL isFetching;
@property(nonatomic, strong)UIButton* restreamButton;

@property(nonatomic)int showCellIndex;

@property(nonatomic, strong)UIButton* noDataBtn;
@property (nonatomic,strong) UIActivityIndicatorView* progressView;

// scrollViewAnimation
@property (assign) BOOL disableBarScroll;
@property (assign) float lastScrollOffset;
@property (assign) float originalTabBarY;
@property (assign) float originalNaviBarY;
@property (assign) float originalStatusBarHeight;
@property (assign) float hotspotHeight;
@property (assign) float barMovePercentage;

- (void)fetchDataWithRefresh:(BOOL)refresh;

@end
