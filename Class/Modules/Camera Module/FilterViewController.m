#import "FilterViewController.h"
#import "PublishPostViewController.h"
#import "MyFilter.h"
#import "TiltControlAreaView.h"

@implementation FilterViewController

@synthesize srcImage;
@synthesize filters;
@synthesize toolButton;
@synthesize filterButton;
@synthesize autoEnhanceButton;
@synthesize filterScrollView;
@synthesize toolScrollView;
@synthesize currentToolIndicatorImageView;
@synthesize sliderControlBox;
@synthesize sliderMode;
@synthesize sliderValueLabel;
@synthesize minSliderValue;
@synthesize maxSliderValue;
@synthesize cancelTuningButton;
@synthesize confirmTuningButton;
@synthesize originSliderBall;
@synthesize currentSliderBall;
@synthesize currentEdittingTool;
@synthesize brightnessValue;
@synthesize currentFilter;
@synthesize currentFilterCode;
@synthesize filterMixPercentageDict;
@synthesize tuningConfirmButtonContainer;
@synthesize sliderValue;
@synthesize emptySliderBar;
@synthesize fullSliderBar;
@synthesize contrastValue;
@synthesize originalSliderValue;
@synthesize srcImageView;
@synthesize prevFilterCode;
@synthesize filterImageCacheDict;
@synthesize srcThumbnailImage;
@synthesize prevClickedFilterCode;
@synthesize autoEnhanceValue;
@synthesize isFirstTimeAutoEnhance;
@synthesize warmthValue;
@synthesize saturationValue;
@synthesize highlightsValue;
@synthesize shadowsValue;
@synthesize vignetteValue;
@synthesize sharpenValue;
@synthesize colorHighlightsValue;
@synthesize colorShadowsValue;
@synthesize shadowsColorMapIndex;
@synthesize highlightsColorMapIndex;
@synthesize colorToolControlBox;
@synthesize colorToolMode;
@synthesize originalShadowsColorMapIndex;
@synthesize originalHighlightsColorMapIndex;
@synthesize originalColorHighlightsValue;
@synthesize originalColorShadowsValue;
@synthesize tiltShiftLinearCenterX;
@synthesize tiltShiftLinearCenterY;
@synthesize tiltShiftLinearRadius;
@synthesize tiltShiftMode;
@synthesize tiltShiftRadialCenterX;
@synthesize tiltShiftRadialCenterY;
@synthesize tiltShiftRadialRadius;
@synthesize tiltShiftLinearAngle;
@synthesize tiltShiftControlBox;
@synthesize tiltShiftOffBtn;
@synthesize tiltShiftLinearBtn;
@synthesize tiltShiftRadialBtn;
@synthesize originalTiltShiftMode;
@synthesize tiltControlArea;
@synthesize panRecognizerInAction;
@synthesize rotateRecognizerInAction;
@synthesize pinchRecognizerInAction;
@synthesize unsharpMaskValue;
@synthesize loadingView;
@synthesize fadeValue;
@synthesize skinSmoothValue;
@synthesize isAdjust;
@synthesize navBarHeight;

- (void)viewDidLoad
{
    [super viewDidLoad];
    DLog(@"viewDidload Retain count is %ld", RETAIN_COUNT(self) );
    
//    isFirstTimeLoading = YES;
  //  self.view.backgroundColor = [UIColor redColor];
    self.navigationItem.hidesBackButton=YES;
    self.navigationController.navigationBarHidden = NO;
    _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if(self.navigationController!=nil && self.navigationController.viewControllers.count>1) {
        // pop
        [_backButton addTarget:self action:@selector(myPopHandler) forControlEvents:UIControlEventTouchUpInside];
    } else {
        // dismiss
        [_backButton addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [_backButton setImage:[SINGLETON loadCachedImage:@"nav_arrow_back" withMethod:LOAD_FROM_BUNDLE] forState:UIControlStateNormal];
    [_backButton setImage:[SINGLETON loadCachedImage:@"nav_arrow_back" withMethod:LOAD_FROM_BUNDLE] forState:UIControlStateHighlighted];
    [_backButton setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    
    _backButton.frame= CGRectMake(0.0, 0.0, 40, 40);
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_backButton];

    filterMixPercentageDict = CREATE_MUTABLE_DICTIONARY;
    filterImageCacheDict = CREATE_MUTABLE_DICTIONARY;
    
    // tilt shift params initialization
    tiltShiftMode = 0.0;
    tiltShiftRadialCenterX = 0.5;
    tiltShiftRadialCenterY = 0.5;
    tiltShiftRadialRadius = 0.3;
    tiltShiftLinearCenterX = 0.5;
    tiltShiftLinearCenterY = 0.5;
    tiltShiftLinearRadius = 0.3;
    tiltShiftLinearAngle = 0.0;
    
    currentFilterCode = NORMAL_FILTER;
    prevClickedFilterCode = NORMAL_FILTER;
    isFirstTimeAutoEnhance = 1;
    colorShadowsValue = 50.0;
    colorHighlightsValue = 50.0;
    originalColorHighlightsValue = 50.0;
    originalColorShadowsValue = 50.0;
    
    filters = [@[NORMAL_FILTER, RICH_FILTER, WARM_FILTER, SOFT_FILTER, ROSE_FILTER, MORNING_FILTER, SUNSHINE_FILTER, SUNSET_FILTER, COOL_FILTER, FREEZE_FILTER, OCEAN_FILTER, DREAM_FILTER, VIOLET_FILTER, MELLOW_FILTER, BLEAK_FILTER, MEMORY_FILTER, PURE_FILTER, CALM_FILTER, AUTUMN_FILTER, FANTASY_FILTER, FREEDOM_FILTER, MILD_FILTER, PRAIRIE_FILTER, DEEP_FILTER, GLOW_FILTER, MEMOIR_FILTER, MIST_FILTER, VIVID_FILTER, CHILL_FILTER, PINKY_FILTER, ADVENTURE_FILTER] mutableCopy];

    for(NSString* filterKey in filters) {
        [filterMixPercentageDict setObject:[NSNumber numberWithFloat:100.0] forKey:filterKey];
    }

    self.title = LOCALIZE(@"Filter");
    
    [self configureViewForIOS7];

    self.view.backgroundColor = [UIColor blackColor];

    navBarHeight = 0.0;
//
//    /*   Navigationbar for Iphone4   */
    if(IS_IPHONE_4) {
        _navBarIp4 = [FilterTopIp4Container new];
        _navBarIp4.frame = CGRectMake(0, 0, SCREEN_WIDTH, NAVI_BAR_HEIGHT);
        [self.view addSubview:_navBarIp4];
        _navBarIp4.hidden = NO;
        [_navBarIp4 setBackgroundColor:MAIN_COLOR];

        navBarHeight = _navBarIp4.frame.size.height;

        // delay event handler attach to speed up viewDidLoad
        DLog(@"21 Retain count is %ld", RETAIN_COUNT(self) );
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [_navBarIp4.backBtn addTarget:self action:@selector(Ip4_backBtn:) forControlEvents:UIControlEventTouchUpInside];
            [_navBarIp4.nextBtn addTarget:self action:@selector(Ip4_nextBtn:) forControlEvents:UIControlEventTouchUpInside];
            [_navBarIp4.toolButton addTarget:self action:@selector(Ip4_toolButton:) forControlEvents:UIControlEventTouchUpInside];
            [_navBarIp4.autoEnhanceButton addTarget:self action:@selector(Ip4_autoEnhanceButton:) forControlEvents:UIControlEventTouchUpInside];
            [_navBarIp4.filterButton addTarget:self action:@selector(Ip4_filterButton:) forControlEvents:UIControlEventTouchUpInside];
        });
       
    }
////
    srcImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, navBarHeight, SCREEN_WIDTH, SCREEN_WIDTH)];
    [self.view addSubview:srcImageView];
    srcImageView.image = srcImage;
    srcImageView.contentMode = UIViewContentModeScaleAspectFill;
    srcImageView.clipsToBounds = YES;
//
//    // delay setup tiltControlArea to speed up viewDidLoad
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        tiltControlArea = [[TiltControlAreaView alloc] initWithFrame:CGRectMake(0, navBarHeight, SCREEN_WIDTH, SCREEN_WIDTH)];
        [self.view addSubview:tiltControlArea];
        tiltControlArea.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.0];
        tiltControlArea.userInteractionEnabled = YES;
        tiltControlArea.filterVC = self;
        tiltControlArea.hidden = YES;
        UIPanGestureRecognizer* tiltPanRecog = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(tiltPanRecog:)];
        
        tiltPanRecog.delegate = self;
        tiltPanRecog.maximumNumberOfTouches = 2;
        
        UIRotationGestureRecognizer* tiltRotateRecog = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(tiltRotateRecog:)];
        tiltRotateRecog.delegate = self;
        
        UIPinchGestureRecognizer* tiltPinchRecog = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(tiltPinchRecog:)];
        tiltPinchRecog.delegate = self;
        
        [self.tiltControlArea addGestureRecognizer:tiltPanRecog];
        [self.tiltControlArea addGestureRecognizer:tiltRotateRecog];
        [self.tiltControlArea addGestureRecognizer:tiltPinchRecog];
    });
    
    filterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    filterButton.frame = CGRectMake(0, navBarHeight, 44, 44);
    filterButton.center = CGPointMake(SCREEN_WIDTH/5, SCREEN_WIDTH+32);
    [filterButton setSelected:YES];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"tab_filters"] forState:UIControlStateNormal];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"tab_filters_active"] forState:UIControlStateSelected];

    
    [filterButton addTarget:self action:@selector(filterButton:) forControlEvents:UIControlEventTouchUpInside];

   
    [self.view addSubview:filterButton];

    autoEnhanceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    autoEnhanceButton.frame = CGRectMake(0, navBarHeight, 44, 44);
    autoEnhanceButton.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_WIDTH+32);
    [autoEnhanceButton setBackgroundImage:[UIImage imageNamed:@"tab_lux"] forState:UIControlStateNormal];
    [autoEnhanceButton setBackgroundImage:[UIImage imageNamed:@"tab_lux_active"] forState:UIControlStateSelected];

    [autoEnhanceButton addTarget:self action:@selector(autoEnhance) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:autoEnhanceButton];
    
    toolButton = [UIButton buttonWithType:UIButtonTypeCustom];
    toolButton.frame = CGRectMake(0, navBarHeight, 44, 44);
    toolButton.center = CGPointMake(SCREEN_WIDTH/5*4, SCREEN_WIDTH+32);
    [toolButton setBackgroundImage:[UIImage imageNamed:@"tab_tool"] forState:UIControlStateNormal];
    
    [toolButton setBackgroundImage:[UIImage imageNamed:@"tab_tool_active"] forState:UIControlStateSelected];
    [toolButton addTarget:self action:@selector(toolopen) forControlEvents:UIControlEventTouchUpInside];


    
    
    [self.view addSubview:toolButton];

    float scrollViewHeight = 0.0f;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:LOCALIZE(@"Next") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction)];

    if(IS_IPHONE_4) {
        toolButton.hidden = YES;
        filterButton.hidden = YES;
        autoEnhanceButton.hidden = YES;
        scrollViewHeight = SCREEN_HEIGHT-SCREEN_WIDTH-NAVI_BAR_HEIGHT;
        self.navigationController.navigationBarHidden = YES;
        _navBarIp4.hidden = NO;
        
    } else {
        
        if(IS_IPHONE_6_PLUS) {
            scrollViewHeight = SCREEN_HEIGHT-SCREEN_WIDTH-NAVI_BAR_HEIGHT-80;
            toolButton.center = CGPointMake(SCREEN_WIDTH/5*4, SCREEN_WIDTH+40);
            autoEnhanceButton.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_WIDTH+40);
            filterButton.center = CGPointMake(SCREEN_WIDTH/5, SCREEN_WIDTH+40);
        } else {
            scrollViewHeight = SCREEN_HEIGHT-SCREEN_WIDTH-NAVI_BAR_HEIGHT-64;
        }
    }
//
    
    
    
    
    filterScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-scrollViewHeight+navBarHeight, SCREEN_WIDTH, scrollViewHeight)];
    filterScrollView.contentSize = CGSizeMake(SCREEN_WIDTH*2, scrollViewHeight);
    filterScrollView.backgroundColor = DARK_BG_COLOR;
    filterScrollView.showsHorizontalScrollIndicator = NO;
    filterScrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:filterScrollView];

    _selectedView = [UIView new];
    [_selectedView setBackgroundColor:MAIN_COLOR];
    
    // loading view
    loadingView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    loadingView.center = CGPointMake(SCREEN_WIDTH/2, filterScrollView.frame.size.height*0.45);
    loadingView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [loadingView setColor:[UIColor colorWithWhite:0.5 alpha:1.0]];
    [filterScrollView addSubview:self.loadingView];
    [loadingView startAnimating];
    loadingView.alpha = 0.8;
////
    // delay setup of tuning to speed up viewDidLoad
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        toolScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-scrollViewHeight+navBarHeight, SCREEN_WIDTH, scrollViewHeight)];
        toolScrollView.contentSize = CGSizeMake(SCREEN_WIDTH*2, scrollViewHeight);
        toolScrollView.backgroundColor = DARK_BG_COLOR;
        toolScrollView.showsHorizontalScrollIndicator = NO;
        toolScrollView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:toolScrollView];
        toolScrollView.alpha = 0.0;
        
        // slider
        sliderControlBox = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_WIDTH+navBarHeight, SCREEN_WIDTH, SCREEN_HEIGHT-SCREEN_WIDTH-44.0)];
        sliderControlBox.backgroundColor = DARK_BG_COLOR;
        [self.view addSubview:sliderControlBox];
        sliderControlBox.hidden = YES;
        UIPanGestureRecognizer* panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panRecognizer:)];
        
    
        [sliderControlBox addGestureRecognizer:panRecognizer];
        
        UITapGestureRecognizer* sliderBoxTapRecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sliderBoxTapRecog:)];
        [sliderControlBox addGestureRecognizer:sliderBoxTapRecog];
        
        emptySliderBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 3)];
        emptySliderBar.backgroundColor = GRAY_COLOR;
        [sliderControlBox addSubview:emptySliderBar];
        
        fullSliderBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 3)];
        fullSliderBar.backgroundColor = LIGHT_MAIN_COLOR;
        [sliderControlBox addSubview:fullSliderBar];
        
        originSliderBall = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"camera_pot"]];
        originSliderBall.frame = CGRectMake(0, 0, 20, 20);
        [sliderControlBox addSubview:originSliderBall];
        
        currentSliderBall = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"camera_center"]];
        currentSliderBall.frame = CGRectMake(0, 0, 20, 20);
        [sliderControlBox addSubview:currentSliderBall];
        
        sliderValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 20)];
        sliderValueLabel.textColor = WHITE_COLOR;
        sliderValueLabel.font = BOLD_FONT_WITH_SIZE(12.0);
        sliderValueLabel.textAlignment = NSTextAlignmentCenter;
        [sliderControlBox addSubview:sliderValueLabel];
        
        cancelTuningButton = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelTuningButton.frame = CGRectMake(0, sliderControlBox.frame.size.height-50, SCREEN_WIDTH/2, 50);
        
        [cancelTuningButton addTarget:self action:@selector(cancelTuningButton:) forControlEvents:UIControlEventTouchUpInside];
      
    
        [cancelTuningButton setImage:[UIImage imageNamed:@"nav_cancel"] forState:UIControlStateNormal];
        [cancelTuningButton setImage:[UIImage imageNamed:@"nav_cancel_down"] forState:UIControlStateHighlighted];
        [cancelTuningButton setBackgroundColor:MAIN_COLOR];

        [sliderControlBox addSubview:cancelTuningButton];
        
        confirmTuningButton = [UIButton buttonWithType:UIButtonTypeCustom];
        confirmTuningButton.frame = CGRectMake(SCREEN_WIDTH/2, sliderControlBox.frame.size.height-50, SCREEN_WIDTH/2, 50);
        
        [confirmTuningButton addTarget:self action:@selector(confirmTuningButton:) forControlEvents:UIControlEventTouchUpInside];
        
      
        
        [confirmTuningButton setImage:[UIImage imageNamed:@"nav_check"] forState:UIControlStateNormal];
        [confirmTuningButton setImage:[UIImage imageNamed:@"nav_check_down"] forState:UIControlStateHighlighted];
        [confirmTuningButton setBackgroundColor:MAIN_COLOR];
        
        [sliderControlBox addSubview:confirmTuningButton];
        
        colorToolControlBox = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_WIDTH+navBarHeight, SCREEN_WIDTH, sliderControlBox.frame.size.height-50)];
        colorToolControlBox.backgroundColor = DARK_BG_COLOR;
        colorToolControlBox.hidden = YES;
        [self.view addSubview:colorToolControlBox];
        
        // tilt shift tool
        tiltShiftControlBox = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_WIDTH+navBarHeight, SCREEN_WIDTH, sliderControlBox.frame.size.height-50)];
        tiltShiftControlBox.backgroundColor = DARK_BG_COLOR;
        tiltShiftControlBox.hidden = YES;
        [self.view addSubview:tiltShiftControlBox];
        
        tiltShiftOffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        tiltShiftOffBtn.frame = CGRectMake(0, 0, 60, 60);
        tiltShiftOffBtn.center = CGPointMake(SCREEN_WIDTH/4, tiltShiftControlBox.frame.size.height/2);
        [tiltShiftControlBox addSubview:tiltShiftOffBtn];
        [tiltShiftOffBtn addTarget:self action:@selector(tiltShiftOffBtn:) forControlEvents:UIControlEventTouchUpInside];

        tiltShiftRadialBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        tiltShiftRadialBtn.frame = CGRectMake(0, 0, 60, 60);
        tiltShiftRadialBtn.center = CGPointMake(SCREEN_WIDTH/4*2, tiltShiftControlBox.frame.size.height/2);
        [tiltShiftControlBox addSubview:tiltShiftRadialBtn];
        
        [tiltShiftRadialBtn addTarget:self action:@selector(tiltShiftRadialBtn:) forControlEvents:UIControlEventTouchUpInside];

        
        tiltShiftLinearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        tiltShiftLinearBtn.frame = CGRectMake(0, 0, 60, 60);
        tiltShiftLinearBtn.center = CGPointMake(SCREEN_WIDTH/4*3, tiltShiftControlBox.frame.size.height/2);
        [tiltShiftControlBox addSubview:tiltShiftLinearBtn];
        
        [tiltShiftLinearBtn addTarget:self action:@selector(tiltShiftLinearBtn:) forControlEvents:UIControlEventTouchUpInside];

        
        [self reloadTiltShiftBtn];
    });
    
    [self reloadInputImage];
}

-(void) showTiltControlArea
{
    tiltControlArea.alpha = 0.0;
    tiltControlArea.showTiltControl = YES;
    [tiltControlArea setNeedsDisplay];

    [UIView animateWithDuration:0.1 animations:^{
         tiltControlArea.alpha = 1.0;
    } completion:^(BOOL finished) {

    }];
}

-(void) hideTiltControlArea
{
    [UIView animateWithDuration:0.1 animations:^{
        tiltControlArea.alpha = 0.0;
    } completion:^(BOOL finished) {
        tiltControlArea.alpha = 1.0;
        tiltControlArea.showTiltControl = NO;
        [tiltControlArea setNeedsDisplay];
    }];
}

-(void) blinkTiltControlArea
{
    [self showTiltControlArea];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self hideTiltControlArea];
    });
}

# pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if(gestureRecognizer==self.navigationController.interactivePopGestureRecognizer) {
        return NO;
    }
    
    return YES;
}

-(void) reloadInputImage
{

    if(srcImage.size.width!=1080.0 || srcImage.size.height!=1080.0) {
        srcImage = [UIImage scaleImage:srcImage toSize:CGSizeMake(1080, 1080)];
    }
    
    srcThumbnailImage = [UIImage scaleImage:srcImage toSize:CGSizeMake(100, 100)];
    
    // TRICKY: need delay to ensure fast speed!
    srcImageView.image = srcImage;
    
    float delayTime = 0.15;
    
    if([SINGLETON isUsingFastGPU]) {
        delayTime = 0.35;
    }
    
    loadingView.alpha = 0.8;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(currentFilter==nil) {
            currentFilter = [[MyFilter alloc] initWithImageSize:CGSizeMake(1080, 1080) filterProgram:FILTER_PROGRAM_COMPLETE];
            [self matchViewPortSizeWithScreen];
            
            _glOutputView = [[GLKView alloc] initWithFrame:CGRectMake(0, navBarHeight, SCREEN_WIDTH, SCREEN_WIDTH) context:currentFilter.glContext];
            [self.view addSubview:_glOutputView];
            _glOutputView.delegate = self;
            _glOutputView.transform = CGAffineTransformConcat(CGAffineTransformMakeRotation(-M_PI_2), CGAffineTransformMakeScale(-1.0, 1.0)); // TRICKY: dont know why glkview has wrong orientation
            _glOutputView.userInteractionEnabled = YES;
            UILongPressGestureRecognizer* tapRecog = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecog:)];

            
            tapRecog.minimumPressDuration = 0.01;
            [_glOutputView addGestureRecognizer:tapRecog];
        }
        
        [currentFilter loadFilter:currentFilterCode];
        [currentFilter loadInputImage:srcImage textureName:@"inputImageTexture"];

        [self reloadOutputImage];

        // load gaussian blur textures in thread
        dispatch_async(GLOBAL_QUEUE, ^{
            MyFilter* sharedGaussianBlurFilter = [[MyFilter alloc] initWithImageSize:CGSizeMake(1080, 1080) filterProgram:FILTER_PROGRAM_GAUSSIAN_BLUR];
            [sharedGaussianBlurFilter loadInputImage:srcImage textureName:@"inputImageTexture"];

            NSArray* blurImgs = [sharedGaussianBlurFilter computeGaussianBlur];
            
            [currentFilter loadInputImage:blurImgs[1] textureName:@"blurTexture2"];
            [currentFilter loadInputImage:blurImgs[2] textureName:@"blurTexture3"];
            [currentFilter loadInputImage:blurImgs[3] textureName:@"blurTexture4"];
        });
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        MyFilter* thumbFilter = [[MyFilter alloc] initWithImageSize:CGSizeMake(100.0, 100.0) filterProgram:FILTER_PROGRAM_SIMPLE];
        [thumbFilter loadInputImage:srcThumbnailImage textureName:@"inputImageTexture"];
        [thumbFilter loadFilter:NORMAL_FILTER];

        // setup filter list
        for(UIView* v in filterScrollView.subviews) {
            if([v isKindOfClass:[UIImageView class]] || [v isKindOfClass:[UILabel class]]) {
                [v removeFromSuperview];
            }
        }
        
        __block float xPos = 20;
        __block float btnWidth = 60.0f;
        __block float yTopMatgin = 20.0f;
        
        if(IS_IPHONE_6_PLUS){
            btnWidth = 90.0f;
            yTopMatgin = 28.0f;
        }
        
        if(IS_IPHONE_6) {
            btnWidth = 90.0f;
            yTopMatgin = 28.0f;
        }
        
        if(IS_IPHONE_4) {
            yTopMatgin = 15.0f;
        }
        
        filterScrollView.contentSize = CGSizeMake(xPos + (yTopMatgin+btnWidth)*filters.count, filterScrollView.frame.size.height);
        
        int filterIndex = 0;
        
        for(NSString* currentFilterKey in filters) {
        
            [thumbFilter loadFilter:currentFilterKey];
            
            UIImageView* currentFilterImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yTopMatgin, btnWidth, btnWidth)];
            currentFilterImageView.image = [thumbFilter outputImage];
            currentFilterImageView.userInteractionEnabled = YES;
            [filterScrollView addSubview:currentFilterImageView];
            currentFilterImageView.alpha = 0.0;
            currentFilterImageView.transform = CGAffineTransformMakeScale(0, 0);

            UILabel* currentFilterLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos-5, yTopMatgin+btnWidth+5, btnWidth+10, 30)];
            currentFilterLabel.text = LOCALIZE(currentFilterKey);
            currentFilterLabel.textColor = WHITE_COLOR;
            currentFilterLabel.textAlignment = NSTextAlignmentCenter;
            currentFilterLabel.alpha = 0.0;
            currentFilterLabel.transform = CGAffineTransformMakeScale(0, 0);
            
            UIFont* filterLabelFont = BOLD_FONT_WITH_SIZE(14.0);
            currentFilterLabel.font = filterLabelFont;
            [filterScrollView addSubview:currentFilterLabel];
            currentFilterLabel.numberOfLines = 1;
            currentFilterLabel.minimumScaleFactor = 0.5;
            currentFilterLabel.adjustsFontSizeToFitWidth = YES;
            
            [UIView animateWithDuration:0.05 animations:^{
                currentFilterImageView.alpha = 1.0;
                currentFilterLabel.alpha = 1.0;
                
                currentFilterImageView.transform = CGAffineTransformMakeScale(1, 1);
                currentFilterLabel.transform = CGAffineTransformMakeScale(1, 1);
            } completion:^(BOOL finished) {
                
            }];
            UITapGestureRecognizer* tapRecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecog_select:)];
        
            [currentFilterImageView addGestureRecognizer:tapRecog];
            
            xPos += yTopMatgin+btnWidth;
            currentFilterImageView.tag=filterIndex;
            filterIndex++;
        }
        
        loadingView.alpha = 0.0;
    });
}

-(void) reloadToolsAndFilters
{

    // setup tool list
    for(UIView* v in toolScrollView.subviews) {
        [v removeFromSuperview];
    }
    
     _toolNames = @[LOCALIZE(@"ADJUST"), LOCALIZE(@"BRIGHTNESS"), LOCALIZE(@"CONTRAST"), LOCALIZE(@"BEAUTY"), LOCALIZE(@"STRUCTURE"), LOCALIZE(@"WARMTH"), LOCALIZE(@"SATURATION"), LOCALIZE(@"COLOR"), LOCALIZE(@"FADE"), LOCALIZE(@"HIGHLIGHTS"), LOCALIZE(@"SHADOWS"), LOCALIZE(@"VIGNETTE"), LOCALIZE(@"TILT SHIFT"), LOCALIZE(@"SHARPEN")];
    NSArray* toolIcons = @[@"tool_adjust", @"tool_brightness", @"tool_contrast", @"tool_beauty", @"tool_sharpen_2", @"tool_warmth", @"tool_saturation", @"tool_color", @"tool_fade", @"tool_highlights", @"tool_shadows", @"tool_vignette", @"tool_blur", @"tool_sharpen"];
    NSArray* toolDownIcons = @[@"tool_adjust_down", @"tool_brightness_down", @"tool_contrast_down", @"tool_beauty_down", @"tool_sharpen_2_down", @"tool_warmth_down", @"tool_saturation_down", @"tool_fade_down", @"tool_highlights_down", @"tool_highlights_down", @"tool_shadows_down", @"tool_vignette_down", @"tool_blur_down", @"tool_sharpen_down"];
    
    float xPos = 20;
    
    for(int i=0;i<_toolNames.count;i++) {
        _tempToolButton.tag=i-1;
        float btnWidth = 60.0f;
        float yTopMatgin = 20.0f;
        
        if(IS_IPHONE_6_PLUS){
            btnWidth = 90.0f;
            yTopMatgin = 28.0f;
        }
        
        if(IS_IPHONE_6) {
            btnWidth = 90.0f;
            yTopMatgin = 28.0f;
        }
        
        self.toolName= _toolNames[i];
        _tempToolButton = [UIButton buttonWithType:UIButtonTypeCustom];

        _tempToolButton.frame = CGRectMake(xPos, yTopMatgin, btnWidth, btnWidth);
        [_tempToolButton setBackgroundImage:[UIImage imageNamed:toolIcons[i]] forState:UIControlStateNormal];
        [_tempToolButton setBackgroundImage:[UIImage imageNamed:toolDownIcons[i]] forState:UIControlStateHighlighted];
        [toolScrollView addSubview:_tempToolButton];
        
        UIFont* toolLabelFont = BOLD_FONT_WITH_SIZE(12.0);
        UILabel* currentToolLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos-5, yTopMatgin+btnWidth+10, btnWidth+10, 30)];
        currentToolLabel.text = _toolNames[i];
        currentToolLabel.textColor = WHITE_COLOR;
        currentToolLabel.textAlignment = NSTextAlignmentCenter;
        currentToolLabel.font = toolLabelFont;
        [toolScrollView addSubview:currentToolLabel];
        currentToolLabel.numberOfLines = 1;
        currentToolLabel.minimumScaleFactor = 0.5;
        currentToolLabel.adjustsFontSizeToFitWidth = YES;

        // setup click actions
        if([self.toolName isEqualToString:LOCALIZE(@"BRIGHTNESS")]) {
            if (brightnessValue) {
                [self addEditLineOn:toolScrollView atCenter:_tempToolButton.center];
            }
            [_tempToolButton addTarget:self action:@selector(tempToolButtonBRI:) forControlEvents:UIControlEventTouchUpInside];
            
        } else if([self.toolName isEqualToString:LOCALIZE(@"CONTRAST")]) {
            if (self.contrastValue) {
                [self addEditLineOn:toolScrollView atCenter:_tempToolButton.center];
            }
            [_tempToolButton addTarget:self action:@selector(tempToolButtonCON:) forControlEvents:UIControlEventTouchUpInside];

        } else if([self.toolName isEqualToString:LOCALIZE(@"WARMTH")]) {
            if (self.warmthValue) {
                [self addEditLineOn:self.toolScrollView atCenter:_tempToolButton.center];
            }
            
            [_tempToolButton addTarget:self action:@selector(tempToolButtonWARM:) forControlEvents:UIControlEventTouchUpInside];

        } else if([self.toolName isEqualToString:LOCALIZE(@"SATURATION")]) {
            if (self.saturationValue) {
                [self addEditLineOn:self.toolScrollView atCenter:_tempToolButton.center];
            }
            [_tempToolButton addTarget:self action:@selector(tempToolButtonSATU:) forControlEvents:UIControlEventTouchUpInside];

        } else if([self.toolName isEqualToString:LOCALIZE(@"FADE")]) {
            if (self.fadeValue) {
                [self addEditLineOn:self.toolScrollView atCenter:_tempToolButton.center];
            }
            [_tempToolButton addTarget:self action:@selector(tempToolButtonFADE:) forControlEvents:UIControlEventTouchUpInside];
         
        } else if([self.toolName isEqualToString:LOCALIZE(@"BEAUTY")]) {
            if (self.skinSmoothValue) {
                [self addEditLineOn:self.toolScrollView atCenter:_tempToolButton.center];
            }
            
            [_tempToolButton addTarget:self action:@selector(tempToolButtonBEAU:) forControlEvents:UIControlEventTouchUpInside];
           
        } else if([self.toolName isEqualToString:LOCALIZE(@"HIGHLIGHTS")]) {
            if (self.highlightsValue) {
                [self addEditLineOn:self.toolScrollView atCenter:_tempToolButton.center];
            }
            [_tempToolButton addTarget:self action:@selector(tempToolButtonHIGH:) forControlEvents:UIControlEventTouchUpInside];
            
        } else if([self.toolName isEqualToString:LOCALIZE(@"SHADOWS")]) {
            if (self.shadowsValue) {
                [self addEditLineOn:self.toolScrollView atCenter:_tempToolButton.center];
            }
            [_tempToolButton addTarget:self action:@selector(tempToolButtonSHAD:) forControlEvents:UIControlEventTouchUpInside];

           
        } else if([self.toolName isEqualToString:LOCALIZE(@"VIGNETTE")]) {
            if (self.vignetteValue) {
                [self addEditLineOn:toolScrollView atCenter:_tempToolButton.center];
            }
            
            [_tempToolButton addTarget:self action:@selector(tempToolButtonVIGN:) forControlEvents:UIControlEventTouchUpInside];


        } else if([self.toolName isEqualToString:LOCALIZE(@"SHARPEN")]) {
            if (self.sharpenValue) {
                [self addEditLineOn:self.toolScrollView atCenter:_tempToolButton.center];
            }

            [_tempToolButton addTarget:self action:@selector(tempToolButtonSHAR:) forControlEvents:UIControlEventTouchUpInside];
                  } else if([self.toolName isEqualToString:LOCALIZE(@"STRUCTURE")]) {
            if (unsharpMaskValue) {
                [self addEditLineOn:self.toolScrollView atCenter:_tempToolButton.center];
            }
                      
                      [_tempToolButton addTarget:self action:@selector(tempToolButtonSTRU:) forControlEvents:UIControlEventTouchUpInside];
         
        } else if([self.toolName isEqualToString:LOCALIZE(@"COLOR")]) {
            if ( (self.highlightsColorMapIndex && self.colorHighlightsValue) || (self.shadowsColorMapIndex && self.colorShadowsValue)) {
                [self addEditLineOn:toolScrollView atCenter:_tempToolButton.center];
            }
            [_tempToolButton addTarget:self action:@selector(tempToolButtonCOLO:) forControlEvents:UIControlEventTouchUpInside];


        } else if([self.toolName isEqualToString:LOCALIZE(@"ADJUST")]) {
            if (self.isAdjust) {
                [self addEditLineOn:self.toolScrollView atCenter:_tempToolButton.center];
            }
            
            [_tempToolButton addTarget:self action:@selector(tempToolButtonADJU:) forControlEvents:UIControlEventTouchUpInside];
        } else if([self.toolName isEqualToString:LOCALIZE(@"TILT SHIFT")]) {
            
            if (self.tiltShiftMode != TILT_SHIFT_MODE_OFF) {
                [self addEditLineOn:self.toolScrollView atCenter:_tempToolButton.center];
            }
            
            [_tempToolButton addTarget:self action:@selector(tempToolButtonTILT:) forControlEvents:UIControlEventTouchUpInside];

        }
        
        xPos += btnWidth+20;
    }
    
    self.toolScrollView.contentSize = CGSizeMake(xPos, toolScrollView.frame.size.height);
    
    xPos = 20.0;
 
    
//    // setup filter list
//    for(UIView* v in filterScrollView.subviews) {
//        [v removeFromSuperview];
//    }
//    int filterIndex = 0;
//    for(NSString* currentFilterKey in filters) {
//        float btnWidth = 60.0f;
//        float yTopMatgin = 20.0f;
//        
//        if(IS_IPHONE_6_PLUS){
//            btnWidth = 90.0f;
//            yTopMatgin = 28.0f;
//        }
//        
//        if(IS_IPHONE_6) {
//            btnWidth = 90.0f;
//            yTopMatgin = 28.0f;
//        }
//        
//        UIImageView* currentFilterImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yTopMatgin, btnWidth, btnWidth)];
//        currentFilterImageView.image = [self.filterImageCacheDict objectForKey:currentFilterKey];
//        currentFilterImageView.userInteractionEnabled = YES;
//        [self.filterScrollView addSubview:currentFilterImageView];
//        
//        UILabel* currentFilterLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos-5, yTopMatgin+btnWidth+10, btnWidth+10, 30)];
//        currentFilterLabel.text = LOCALIZE(currentFilterKey);
//        currentFilterLabel.textColor = DARK_GRAY_COLOR;
//        currentFilterLabel.textAlignment = NSTextAlignmentCenter;
//        
//        UIFont* filterLabelFont = BOLD_FONT_WITH_SIZE(12.0);
//        
//        if(IS_IPHONE_6) {
//            filterLabelFont = BOLD_FONT_WITH_SIZE(16.0);
//        } else if(IS_IPHONE_6_PLUS) {
//            filterLabelFont = BOLD_FONT_WITH_SIZE(16.0);
//        }
//        
//        currentFilterLabel.font = filterLabelFont;
//        [self.filterScrollView addSubview:currentFilterLabel];
//        UITapGestureRecognizer* tapRecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecog_filter:)];
//        
//        [currentFilterImageView addGestureRecognizer:tapRecog];
//        
//        xPos += yTopMatgin+btnWidth;
//        currentFilterImageView.tag=filterIndex;
//        filterIndex++;
//    }
//    
//    filterScrollView.contentSize = CGSizeMake(xPos,self.filterScrollView.frame.size.height);
}

-(void) reloadTiltShiftBtn
{
    if(tiltShiftMode==TILT_SHIFT_MODE_OFF) {
        [tiltShiftOffBtn setBackgroundImage:[UIImage imageNamed:@"tool_blur_off_down"] forState:UIControlStateNormal];
        [tiltShiftOffBtn setBackgroundImage:[UIImage imageNamed:@"tool_blur_off_down"] forState:UIControlStateHighlighted];
    } else {
        [tiltShiftOffBtn setBackgroundImage:[UIImage imageNamed:@"tool_blur_off"] forState:UIControlStateNormal];
        [tiltShiftOffBtn setBackgroundImage:[UIImage imageNamed:@"tool_blur_off"] forState:UIControlStateHighlighted];
    }
    
    if(tiltShiftMode==TILT_SHIFT_MODE_RADIAL) {
        [tiltShiftRadialBtn setBackgroundImage:[UIImage imageNamed:@"mode_blur_radial_down"] forState:UIControlStateNormal];
        [tiltShiftRadialBtn setBackgroundImage:[UIImage imageNamed:@"mode_blur_radial_down"] forState:UIControlStateHighlighted];
    } else {
        [tiltShiftRadialBtn setBackgroundImage:[UIImage imageNamed:@"mode_blur_radial"] forState:UIControlStateNormal];
        [tiltShiftRadialBtn setBackgroundImage:[UIImage imageNamed:@"mode_blur_radial"] forState:UIControlStateHighlighted];
    }
    
    if(tiltShiftMode==TILT_SHIFT_MODE_LINEAR) {
        [tiltShiftLinearBtn setBackgroundImage:[UIImage imageNamed:@"mode_blur_linear_down"] forState:UIControlStateNormal];
        [tiltShiftLinearBtn setBackgroundImage:[UIImage imageNamed:@"mode_blur_linear_down"] forState:UIControlStateHighlighted];
    } else {
        [tiltShiftLinearBtn setBackgroundImage:[UIImage imageNamed:@"mode_blur_linear"] forState:UIControlStateNormal];
        [tiltShiftLinearBtn setBackgroundImage:[UIImage imageNamed:@"mode_blur_linear"] forState:UIControlStateHighlighted];
    }
}

-(void) reloadSlider
{
    float leftmostPointX = 30;
    float rightmostPointX = SCREEN_WIDTH-leftmostPointX;
    float centerY = (sliderControlBox.frame.size.height-50)/2;
    float currentPointX = SCREEN_WIDTH/2;

    sliderValueLabel.text = [NSString stringWithFormat:@"%d", (int) sliderValue];
    emptySliderBar.frame = CGRectMake(leftmostPointX, centerY-0.5, rightmostPointX-leftmostPointX, 3.0);

    if(sliderMode==SLIDER_MODE_ORIGIN_CENTER) {
        currentPointX = SCREEN_WIDTH/2 + (rightmostPointX-leftmostPointX)/2*sliderValue/maxSliderValue;
        originSliderBall.center = CGPointMake(SCREEN_WIDTH/2, centerY);
        
        if(sliderValue>=0) {
            fullSliderBar.frame = CGRectMake(SCREEN_WIDTH/2, centerY-0.5, currentPointX-SCREEN_WIDTH/2, 3.0);
        } else {
            fullSliderBar.frame = CGRectMake(currentPointX, centerY-0.5, SCREEN_WIDTH/2-currentPointX, 3.0);
        }
    } else if(sliderMode==SLIDER_MODE_ORIGIN_LEFT) {
        currentPointX = leftmostPointX + (rightmostPointX-leftmostPointX)*(sliderValue-minSliderValue)/(maxSliderValue-minSliderValue);
        originSliderBall.center = CGPointMake(leftmostPointX, centerY);
        fullSliderBar.frame = CGRectMake(leftmostPointX, centerY-0.5, currentPointX-leftmostPointX, 3.0);
    }
    
    currentSliderBall.center = CGPointMake(currentPointX, centerY);
    sliderValueLabel.center = CGPointMake(currentPointX, centerY-20);
}

-(void) enterColorMapEditMode
{

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.rightBarButtonItem = nil;
    
    colorToolControlBox.hidden = NO;
    sliderControlBox.hidden = NO;
    tiltShiftControlBox.hidden = YES;

    for(UIView* v in colorToolControlBox.subviews) {
        [v removeFromSuperview];
    }
    
    UIButton* shadowButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [shadowButton setTitle:LOCALIZE(@"SHADOWS") forState:UIControlStateNormal];
    [shadowButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    shadowButton.frame = CGRectMake(0, 0, SCREEN_WIDTH/2, 20);
    [colorToolControlBox addSubview:shadowButton];
    
    [shadowButton addTarget:self action:@selector(shadowButton:) forControlEvents:UIControlEventTouchUpInside];

    UIButton* highlightsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [highlightsButton setTitle:LOCALIZE(@"HIGHLIGHTS") forState:UIControlStateNormal];
    [highlightsButton setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    highlightsButton.frame = CGRectMake(SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2, 20);
    [colorToolControlBox addSubview:highlightsButton];
    
    [shadowButton addTarget:self action:@selector(highlightsButton:) forControlEvents:UIControlEventTouchUpInside];
  
    float marginX = 5.0;
    NSString* colorMode = @"";
    
    if(colorToolMode==COLOR_TOOL_MODE_SHADOWS) {
        colorMode = @"dark";
    } else if(colorToolMode==COLOR_TOOL_MODE_HIGHLIGHTS) {
        colorMode = @"light";
    }
    
    UIButton* colorBtn;
    
    // no color
    for(int i=1;i<=9;i++) {
        colorBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        colorBtn.frame = CGRectMake(0, 0, 30, 30);
        colorBtn.center = CGPointMake(marginX+(SCREEN_WIDTH-marginX*2)/10*i, 30+(colorToolControlBox.frame.size.height-30)/2);
        colorBtn.tag=i;
        int currentColorMapIndex = 0;
        
        if(colorToolMode==COLOR_TOOL_MODE_SHADOWS) {
            currentColorMapIndex = shadowsColorMapIndex;
        } else if(colorToolMode==COLOR_TOOL_MODE_HIGHLIGHTS) {
            currentColorMapIndex = highlightsColorMapIndex;
        }
        
        if(i==1) {
            [colorBtn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"color_%@_01", colorMode]] forState:UIControlStateNormal];
            [colorBtn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"color_%@_01", colorMode]] forState:UIControlStateHighlighted];
        } else {
            if(currentColorMapIndex==i-1) {
                [colorBtn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"color_%@_%02d_active", colorMode, i]] forState:UIControlStateNormal];
                [colorBtn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"color_%@_%02d_active", colorMode, i]] forState:UIControlStateHighlighted];
            } else {
                [colorBtn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"color_%@_%02d", colorMode, i]] forState:UIControlStateNormal];
                [colorBtn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"color_%@_%02d", colorMode, i]] forState:UIControlStateHighlighted];
            }
            
        }
        
        [colorToolControlBox addSubview:colorBtn];
        [colorBtn addTarget:self action:@selector(colorBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void) enterTiltEditMode
{
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.rightBarButtonItem = nil;
    
    sliderControlBox.hidden = NO;
    _backButton.hidden = YES;
    colorToolControlBox.hidden = YES;
    tiltShiftControlBox.hidden = NO;
    tiltControlArea.hidden = NO;
    
    if(tiltShiftMode!=TILT_SHIFT_MODE_OFF) {
        [self blinkTiltControlArea];
    }
}

-(void) enterToolEditMode
{
//    if(IS_IPHONE_4){
//        [_navBarIp4 autoEnhanceModeChange:_currentToolButton.titleLabel.text];
//    }
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.rightBarButtonItem = nil;
    
    sliderControlBox.hidden = NO;
    _backButton.hidden = YES;
    colorToolControlBox.hidden = YES;
    tiltShiftControlBox.hidden = YES;

    [self reloadSlider];
}

-(void) exitToolEditMode
{
//    if(IS_IPHONE_4){
//        [_navBarIp4 autoEnhanceModeChange:nil];
//    }
    
    self.title = LOCALIZE(@"Filter");
    _backButton.hidden = NO;
    self.navigationItem.hidesBackButton = NO;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:LOCALIZE(@"Next") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction)];
    colorToolControlBox.hidden = YES;
    sliderControlBox.hidden = YES;
    tiltShiftControlBox.hidden = YES;
    tiltControlArea.hidden = YES;
    
    
}

-(void) reloadOutputImage
{
    [currentFilter setParameter:@"filterMixPercentage" floatValue:[filterMixPercentageDict[currentFilterCode] floatValue]/100.0]; // filter percentage
    [currentFilter setParameter:@"autoEnhance" floatValue:autoEnhanceValue]; // auto enhance
    [currentFilter setParameter:@"brightness" floatValue:brightnessValue]; // brightness
    [currentFilter setParameter:@"contrast" floatValue:contrastValue]; // contrast
    [currentFilter setParameter:@"warmth" floatValue:warmthValue]; // warmth
    [currentFilter setParameter:@"saturation" floatValue:saturationValue]; // saturation
    [currentFilter setParameter:@"highlights" floatValue:highlightsValue]; // highlights
    [currentFilter setParameter:@"shadows" floatValue:shadowsValue]; // shadows
    [currentFilter setParameter:@"vignette" floatValue:vignetteValue]; // vignette
    [currentFilter setParameter:@"sharpen" floatValue:sharpenValue]; // sharpen
    [currentFilter setParameter:@"unsharpMask" floatValue:unsharpMaskValue]; // unsharp mask
    [currentFilter setParameter:@"skinSmooth" floatValue:skinSmoothValue]; // skin smooth
    [currentFilter setParameter:@"fade" floatValue:fadeValue]; // fade

    // shadowsColorMapIndex
    if(shadowsColorMapIndex==0.0) {
        [currentFilter setParameter:@"shadowsColorMapIndex" floatValue:0.0];
    } else {
        [currentFilter setParameter:@"shadowsColorMapIndex" floatValue:shadowsColorMapIndex+8.0];
    }
    
    // highlightsColorMapIndex
    if(highlightsColorMapIndex==0.0) {
        [currentFilter setParameter:@"highlightsColorMapIndex" floatValue:0.0];
    } else {
        [currentFilter setParameter:@"highlightsColorMapIndex" floatValue:highlightsColorMapIndex];
    }
    
    [currentFilter setParameter:@"colorShadows" floatValue:colorShadowsValue]; // colorShadows
    [currentFilter setParameter:@"colorHighlights" floatValue:colorHighlightsValue]; // colorHighlights
    
    // tilt shift
    [currentFilter setParameter:@"tiltShiftMode" floatValue:tiltShiftMode];
    [currentFilter setParameter:@"tiltShiftRadialCenterX" floatValue:tiltShiftRadialCenterX];
    [currentFilter setParameter:@"tiltShiftRadialCenterY" floatValue:tiltShiftRadialCenterY];
    [currentFilter setParameter:@"tiltShiftRadialRadius" floatValue:tiltShiftRadialRadius];
    [currentFilter setParameter:@"tiltShiftLinearCenterX" floatValue:tiltShiftLinearCenterX];
    [currentFilter setParameter:@"tiltShiftLinearCenterY" floatValue:tiltShiftLinearCenterY];
    [currentFilter setParameter:@"tiltShiftLinearRadius" floatValue:tiltShiftLinearRadius];
    [currentFilter setParameter:@"tiltShiftLinearAngle" floatValue:tiltShiftLinearAngle];
    
    [_glOutputView setNeedsDisplay];
}

// avoid slide to pop
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Disable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [EVENT_HANDLER addEventTracking:@"EnterFilterPage" withDict:nil];
    _enterPageTimeStamp=CURRENT_TIMESTAMP;
    _anyChange=NO;
    if(IS_IPHONE_4){
        self.navigationController.navigationBarHidden = YES;
    }
    
    // must match viewport size with screen
    [self matchViewPortSizeWithScreen];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    NSString * booleanString = (_anyChange) ? @"True" : @"False";
    

    [EVENT_HANDLER addEventTracking:@"LeaveFilterPage" withDict:@{@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp),@"anyChange":booleanString}];
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    
        DLog(@"viewWillDisappear Retain count is %ld", RETAIN_COUNT(self) );
}

-(void) matchViewPortSizeWithScreen
{
    // TRICKY: must determine viewPort Size
    float scale;
    
    if(SYSTEM_VERSION_GREATER_THAN(@"8.0")) {
        scale = [[UIScreen mainScreen] nativeScale];
    } else {
        scale = [[UIScreen mainScreen] scale];
    }
    
    currentFilter.targetViewPortSize = CGSizeMake(SCREEN_WIDTH*scale, SCREEN_WIDTH*scale);
}

# pragma mark - CropImageViewControllerDelegate
-(void)didCompleteImageCrop:(UIImage *)croppedImage isChanged:(BOOL)isChanged
{
    if (isChanged) {
        isAdjust = YES;
        filterImageCacheDict = CREATE_MUTABLE_DICTIONARY;
        
        srcImage = croppedImage;
        
        [self addEditLineOn:toolScrollView atCenter:_currentToolButton.center];
        [self reloadInputImage];
        
    }
}

# pragma mark - GLKViewDelegate
- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    [currentFilter render:RENDER_MODE_RGBA rebindFrameBuffer:0];
}

-(void) doneAction
{
    [currentFilter setParameter:@"autoEnhance" floatValue:20.0 + autoEnhanceValue * 0.8]; // auto enhance default 20.0
    [currentFilter setParameter:@"sharpen" floatValue:10.0 + sharpenValue * 0.9]; // sharpen
    [currentFilter setParameter:@"unsharpMask" floatValue:10.0 + unsharpMaskValue * 0.9]; // unsharp mask
    [currentFilter setParameter:@"skinSmoothValue" floatValue:20.0 + skinSmoothValue * 0.8]; // unsharp mask
    
    PublishPostViewController* pubVC = [PublishPostViewController new];
    currentFilter.targetViewPortSize = CGSizeMake(1080, 1080);
    pubVC.postimg = [currentFilter outputImage];
    
    if([GET_DEFAULT(SAVE_PHOTO) isEqual:@1]){
        dispatch_async(GLOBAL_QUEUE, ^{
            UIImageWriteToSavedPhotosAlbum(pubVC.postimg, nil, nil, nil);
        });
    };
    
    [self.navigationController pushViewController:pubVC animated:YES];
}


-(void)dealloc
{

    DLog(@"filter dealloc");
    

    
    //    [NOTIFICATION_CENTER removeObserver:self];
}



-(void)addEditLineOn:(UIScrollView*)scrollView atCenter:(CGPoint)center
{
    DLog(@"addEditLineOn %@ at: x=%f,y=%f", _currentToolButton, center.x, center.y);
    UIView* lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _currentToolButton.frame.size.width,3)];
    [lineView setBackgroundColor:MAIN_COLOR];
    [scrollView addSubview:lineView];
    center.y = scrollView.frame.size.height-5;
    lineView.center = center;
}

-(BOOL)valueChanged
{
    if(currentEdittingTool==TOOL_FILTER && [filterMixPercentageDict count]!=0) {
        return YES;
    } else if(currentEdittingTool==TOOL_AUTO_ENHANCE && (int)autoEnhanceValue!=0) {
        return YES;
    } else if(currentEdittingTool==TOOL_ADJUST && isAdjust) {
        return YES;
    } else if(currentEdittingTool==TOOL_BRIGHTNESS && (int)brightnessValue!=0) {
        return YES;
    } else if(currentEdittingTool==TOOL_CONTRAST && (int)contrastValue!=0) {
        return YES;
    } else if(currentEdittingTool==TOOL_WARMTH && (int)warmthValue!=0) {
        return YES;
    } else if(currentEdittingTool==TOOL_SATURATION && (int)saturationValue!=0) {
        return YES;
    } else if(currentEdittingTool==TOOL_HIGHLIGHTS && (int)highlightsValue!=0) {
        return YES;
    } else if(currentEdittingTool==TOOL_SHADOWS && (int)shadowsValue!=0) {
        return YES;
    } else if(currentEdittingTool==TOOL_VIGNETTE && (int)vignetteValue!=0) {
        return YES;
    } else if(currentEdittingTool==TOOL_SHARPEN && (int)sharpenValue!=0) {
        return YES;
    } else if(currentEdittingTool==TOOL_UNSHAR_MASK && (int)unsharpMaskValue!=0) {
        return YES;
    } else if(currentEdittingTool==TOOL_COLOR) {
        if(colorToolMode==COLOR_TOOL_MODE_SHADOWS && (int)colorShadowsValue!=0) {
            return YES;
        } else if(colorToolMode==COLOR_TOOL_MODE_HIGHLIGHTS && (int)colorHighlightsValue!=0) {
            return YES;
        }
    } else if(currentEdittingTool == TOOL_FADE && (int)fadeValue != 0)
    {
        return YES;
    } else if(currentEdittingTool==TOOL_SKIN && (int)skinSmoothValue!=0) {
        return YES;
    } else if(currentEdittingTool == TOOL_TILT_SHIFT && tiltShiftMode != TILT_SHIFT_MODE_OFF) {
        return YES;
    }
    
    return NO;
}


-(void)autoEnhance
{
    
    [filterButton setSelected:NO];
    [autoEnhanceButton setSelected:YES];
    [toolButton setSelected:NO];
    
    sliderMode = SLIDER_MODE_ORIGIN_LEFT;
    minSliderValue = 0.0;
    maxSliderValue = 100.0;
    currentEdittingTool = TOOL_AUTO_ENHANCE;
    self.title = LOCALIZE(@"CLARITY");
    
            originalSliderValue = autoEnhanceValue;
    
            if(isFirstTimeAutoEnhance) {
                sliderValue = 50.0;
                autoEnhanceValue = 50.0;
            } else {
                sliderValue = autoEnhanceValue;
            }
    
            [self enterToolEditMode];
            [self reloadOutputImage];
}

-(void)toolopen
{
    [filterButton setSelected:NO];
    [autoEnhanceButton setSelected:NO];
    [toolButton setSelected:YES];

    [self reloadToolsAndFilters];
    
    toolScrollView.alpha = 1.0;
    filterScrollView.alpha = 0.0;
}


-(void)tiltPanRecog:(UIPanGestureRecognizer*) sender

{
    static float lastXLocations[] = {0.0, 0.0};
    static float lastYLocations[] = {0.0, 0.0};
    static int oldNumberOfTouches = 0;
    if(self.tiltShiftMode==TILT_SHIFT_MODE_RADIAL) {
        
        if([(UIPanGestureRecognizer *)sender state] ==UIGestureRecognizerStateBegan) {
            self.panRecognizerInAction = YES;
            // save the initial position of each touch when total touch number changes!
            for(int i=0;i<sender.numberOfTouches;i++) {
                lastXLocations[i] = [((UIPanGestureRecognizer*)sender) locationOfTouch:i inView:self.tiltControlArea].x;
                lastYLocations[i] = [((UIPanGestureRecognizer*)sender) locationOfTouch:i inView:self.tiltControlArea].y;
            }
            [self showTiltControlArea];
        }
        if([(UIPanGestureRecognizer *)sender state]==UIGestureRecognizerStateChanged) {
            
            //                    // save the initial position of each touch when total touch number changes!
            
            if(oldNumberOfTouches!=sender.numberOfTouches) {
                for(int i=0;i<sender.numberOfTouches;i++) {
                    lastXLocations[i] = [((UIPanGestureRecognizer*)sender) locationOfTouch:i inView:self.tiltControlArea].x;
                    lastYLocations[i] = [((UIPanGestureRecognizer*)sender) locationOfTouch:i inView:self.tiltControlArea].y;
                }
            }
            oldNumberOfTouches = (int) sender.numberOfTouches;
            float offsetXAccumulated = 0.0;
            float offsetYAccumulated = 0.0;
            for(int i=0;i<sender.numberOfTouches;i++) {
                float currentLocationX = [((UIPanGestureRecognizer*)sender) locationOfTouch:i inView:self.tiltControlArea].x;
                float currentLocationY = [((UIPanGestureRecognizer*)sender) locationOfTouch:i inView:self.tiltControlArea].y;
                float offsetX = currentLocationX - lastXLocations[i];
                float offsetY = currentLocationY - lastYLocations[i];
                offsetXAccumulated += offsetX;
                offsetYAccumulated += offsetY;
                lastXLocations[i] = currentLocationX;
                lastYLocations[i] = currentLocationY;
            }
            self.tiltShiftRadialCenterX += offsetXAccumulated/SCREEN_WIDTH/sender.numberOfTouches;
            self.tiltShiftRadialCenterY += offsetYAccumulated/SCREEN_WIDTH/sender.numberOfTouches;
            if(self.tiltShiftRadialCenterX<0.0) {
                self.tiltShiftRadialCenterX = 0.0;
            }
            if(self.tiltShiftRadialCenterX>1.0) {
                self.tiltShiftRadialCenterX = 1.0;
            }
            if(self.tiltShiftRadialCenterY<0.0) {
                self.tiltShiftRadialCenterY = 0.0;
            }
            if(self.tiltShiftRadialCenterY>1.0) {
                self.tiltShiftRadialCenterY = 1.0;
            }
        }
        if([(UIPanGestureRecognizer *)sender state]==UIGestureRecognizerStateEnded) {
            
            if(sender.numberOfTouches==0) {
                
                self.panRecognizerInAction = NO;
                
                
                
                // must all recognizer not in action!
                
                if(self.panRecognizerInAction==NO && self.pinchRecognizerInAction==NO && self.rotateRecognizerInAction==NO) {
                    
                    [self hideTiltControlArea];
                    
                }
                
            }
            
        }
    } else if(self.tiltShiftMode==TILT_SHIFT_MODE_LINEAR) {
        if([(UIPanGestureRecognizer *)sender state]==UIGestureRecognizerStateBegan) {
            
            self.panRecognizerInAction = YES;
            
            
            
            // save the initial position of each touch when total touch number changes!
            
            for(int i=0;i<sender.numberOfTouches;i++) {
                
                lastXLocations[i] = [((UIPanGestureRecognizer*)sender) locationOfTouch:i inView:self.tiltControlArea].x;
                lastYLocations[i] = [((UIPanGestureRecognizer*)sender) locationOfTouch:i inView:self.tiltControlArea].y;
            }
            [self showTiltControlArea];
        }
        if([(UIPanGestureRecognizer *)sender state]==UIGestureRecognizerStateChanged) {
            //                    // save the initial position of each touch when total touch number changes!
            if(oldNumberOfTouches!=sender.numberOfTouches) {
                for(int i=0;i<sender.numberOfTouches;i++) {
                    lastXLocations[i] = [((UIPanGestureRecognizer*)sender) locationOfTouch:i inView:self.tiltControlArea].x;
                    lastYLocations[i] = [((UIPanGestureRecognizer*)sender) locationOfTouch:i inView:self.tiltControlArea].y;
                }
            }
            oldNumberOfTouches = (int) sender.numberOfTouches;
           float offsetXAccumulated = 0.0;
           float offsetYAccumulated = 0.0;
           for(int i=0;i<sender.numberOfTouches;i++) {
                float currentLocationX = [((UIPanGestureRecognizer*)sender) locationOfTouch:i inView:self.tiltControlArea].x;
                float currentLocationY = [((UIPanGestureRecognizer*)sender) locationOfTouch:i inView:self.tiltControlArea].y;
                float offsetX = currentLocationX - lastXLocations[i];
                float offsetY = currentLocationY - lastYLocations[i];
                offsetXAccumulated += offsetX;
                offsetYAccumulated += offsetY;
                lastXLocations[i] = currentLocationX;
                lastYLocations[i] = currentLocationY;
            }
            tiltShiftLinearCenterX += offsetXAccumulated/SCREEN_WIDTH/sender.numberOfTouches;
            tiltShiftLinearCenterY += offsetYAccumulated/SCREEN_WIDTH/sender.numberOfTouches;
            if(tiltShiftLinearCenterX<0.0) {                
                tiltShiftLinearCenterX = 0.0;
            }
            if(tiltShiftLinearCenterX>1.0) {
                tiltShiftLinearCenterX = 1.0;
            }
            if(tiltShiftLinearCenterY<0.0) {
                tiltShiftLinearCenterY = 0.0;
            }
            if(tiltShiftLinearCenterY>1.0) {
                tiltShiftLinearCenterY = 1.0;
            }
        }
        if([(UIPanGestureRecognizer *)sender state]==UIGestureRecognizerStateEnded) {
            if(sender.numberOfTouches==0) {
                panRecognizerInAction = NO;
                // must all recognizer not in action!
                if(panRecognizerInAction==NO && pinchRecognizerInAction==NO && rotateRecognizerInAction==NO) {
                    [self hideTiltControlArea];
                }
            }
        }
    }
    [self.tiltControlArea setNeedsDisplay];
    [self reloadOutputImage];
}





-(void)tiltRotateRecog:(UIGestureRecognizer*) sender

{    
    if(tiltShiftMode==TILT_SHIFT_MODE_RADIAL) {      
    }
    else if(tiltShiftMode==TILT_SHIFT_MODE_LINEAR) {
        static float originalRotation = 0.0; // in radians
        if([sender state]==UIGestureRecognizerStateBegan) {
            rotateRecognizerInAction = YES;
            originalRotation = tiltShiftLinearAngle;
            [self showTiltControlArea];
        }
        
        if([sender state]==UIGestureRecognizerStateChanged) {
            tiltShiftLinearAngle = originalRotation - ((UIRotationGestureRecognizer*)sender).rotation;
        }
        if([sender state]==UIGestureRecognizerStateEnded) {
            rotateRecognizerInAction = NO;
            // must all recognizer not in action!
            if(self.panRecognizerInAction==NO && self.pinchRecognizerInAction==NO && self.rotateRecognizerInAction==NO) {
                [self hideTiltControlArea];
            }
        }
    }
    [self.tiltControlArea setNeedsDisplay];    
    [self reloadOutputImage];
}







-(void) tiltPinchRecog:(UIGestureRecognizer*) sender

{
    static float originalTiltShiftRadialRadius = 0.0;
    static float maxTiltShiftRadialRadius = 0.8;
    static float minTiltShiftRadialRadius = 0.1;
    static float originalTiltShiftLinearRadius = 0.0;
    static float maxTiltShiftLinearRadius = 0.4;
    static float minTiltShiftLinearRadius = 0.1;
    if(self.tiltShiftMode==TILT_SHIFT_MODE_RADIAL) {        
        if([(UIPanGestureRecognizer *)sender state]==UIGestureRecognizerStateBegan) {
            self.pinchRecognizerInAction = YES;
            originalTiltShiftRadialRadius = tiltShiftRadialRadius;
            [self showTiltControlArea];
        }
        if([(UIPanGestureRecognizer *)sender state]==UIGestureRecognizerStateChanged) {
            self.tiltShiftRadialRadius = originalTiltShiftRadialRadius * [((UIPinchGestureRecognizer*)sender) scale];
            if(self.tiltShiftRadialRadius>maxTiltShiftRadialRadius) {
                self.tiltShiftRadialRadius = maxTiltShiftRadialRadius;
            }
            if(self.tiltShiftRadialRadius<minTiltShiftRadialRadius) {
                self.tiltShiftRadialRadius = minTiltShiftRadialRadius;
            }
        }
        if([(UIPanGestureRecognizer *)sender state]==UIGestureRecognizerStateEnded) {
            self.pinchRecognizerInAction = NO;
            // must all recognizer not in action!
            if(self.panRecognizerInAction==NO && self.pinchRecognizerInAction==NO && self.rotateRecognizerInAction==NO) {
                [self hideTiltControlArea];
            }
        }
    } else if(self.tiltShiftMode==TILT_SHIFT_MODE_LINEAR) {
        if([(UIPanGestureRecognizer *)sender state]==UIGestureRecognizerStateBegan) {
            self.pinchRecognizerInAction = YES;
            originalTiltShiftLinearRadius = self.tiltShiftLinearRadius;
            [self showTiltControlArea];
        }       
        if([(UIPanGestureRecognizer *)sender state]==UIGestureRecognizerStateChanged) {            
            self.tiltShiftLinearRadius = originalTiltShiftLinearRadius * [((UIPinchGestureRecognizer*)sender) scale];
            if(self.tiltShiftLinearRadius>maxTiltShiftLinearRadius) {
                self.tiltShiftLinearRadius = maxTiltShiftLinearRadius;
            }
            if(self.tiltShiftLinearRadius<minTiltShiftLinearRadius) {
                self.tiltShiftLinearRadius = minTiltShiftLinearRadius;
            }
        }
        if([(UIPanGestureRecognizer *)sender state]==UIGestureRecognizerStateEnded) {            
            self.pinchRecognizerInAction = NO;
            // must all recognizer not in action!
            if(self.panRecognizerInAction==NO && self.pinchRecognizerInAction==NO && self.rotateRecognizerInAction==NO) {
                [self hideTiltControlArea];
            }
        }
    }
    [self.tiltControlArea setNeedsDisplay];
    [self reloadOutputImage];
}

-(void)panRecognizer:(UIPanGestureRecognizer*) sender
{
      static float lastLocationX = 0.0;
        
        if([(UIPanGestureRecognizer *)sender state]==UIGestureRecognizerStateBegan) {
            lastLocationX = [(sender) locationInView:self.sliderControlBox].x;
        }
        
        if([sender state]==UIGestureRecognizerStateChanged) {
            float currentLocationX = [(sender) locationInView:self.sliderControlBox].x;
            float offsetX = currentLocationX - lastLocationX;
            
            float offsetSliderValue = offsetX/emptySliderBar.frame.size.width*(self.maxSliderValue-self.minSliderValue);
            self.sliderValue += offsetSliderValue;
            
            if(self.sliderValue>self.maxSliderValue) {
                self.sliderValue = self.maxSliderValue;
            }
            
            if(self.sliderValue<self.minSliderValue) {
                self.sliderValue = self.minSliderValue;
            }
            
            if(self.currentEdittingTool==TOOL_BRIGHTNESS) {
                self.brightnessValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_CONTRAST) {
                self.contrastValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_FILTER) {
                [self.filterMixPercentageDict setObject:[NSNumber numberWithFloat:self.sliderValue] forKey:self.currentFilterCode];
            } else if(self.currentEdittingTool==TOOL_AUTO_ENHANCE) {
                self.autoEnhanceValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_WARMTH) {
                self.warmthValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_SATURATION) {
                self.saturationValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_HIGHLIGHTS) {
                self.highlightsValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_SHADOWS) {
                self.shadowsValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_VIGNETTE) {
                self.vignetteValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_UNSHAR_MASK) {
                self.unsharpMaskValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_SHARPEN) {
                self.sharpenValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_FADE) {
                self.fadeValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_SKIN) {
                self.skinSmoothValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_COLOR) {
                if(self.colorToolMode==COLOR_TOOL_MODE_SHADOWS) {
                    self.colorShadowsValue = self.sliderValue;
                } else if(self.colorToolMode==COLOR_TOOL_MODE_HIGHLIGHTS) {
                    self.colorHighlightsValue = self.sliderValue;
                }
            }
            
            [self reloadSlider];
            [self reloadOutputImage];
            
            lastLocationX = currentLocationX;
        }
}

-(void)sliderBoxTapRecog:(UIGestureRecognizer*) sender
{
        static float increment = 0.0;
        
        if([sender state]==UIGestureRecognizerStateEnded) {
            if([sender locationInView:sender.view].x>self.currentSliderBall.center.x) {
                increment = 1.0;
            } else if([sender locationInView:sender.view].x<self.currentSliderBall.center.x) {
                increment = -1.0;
            }
            
            self.sliderValue += increment;
            
            if(self.sliderValue>self.maxSliderValue) {
                self.sliderValue = self.maxSliderValue;
            }
            
            if(self.sliderValue<self.minSliderValue) {
                self.sliderValue = self.minSliderValue;
            }
            
            if(self.currentEdittingTool==TOOL_BRIGHTNESS) {
                self.brightnessValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_CONTRAST) {
                self.contrastValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_FILTER) {
                [self.filterMixPercentageDict setObject:[NSNumber numberWithFloat:self.sliderValue] forKey:self.currentFilterCode];
            } else if(self.currentEdittingTool==TOOL_AUTO_ENHANCE) {
                self.autoEnhanceValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_WARMTH) {
                self.warmthValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_SATURATION) {
                self.saturationValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_HIGHLIGHTS) {
                self.highlightsValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_SHADOWS) {
                self.shadowsValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_VIGNETTE) {
                self.vignetteValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_UNSHAR_MASK) {
                self.unsharpMaskValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_SHARPEN) {
                self.sharpenValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_FADE) {
                self.fadeValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_SKIN) {
                self.skinSmoothValue = self.sliderValue;
            } else if(self.currentEdittingTool==TOOL_COLOR) {
                if(self.colorToolMode==COLOR_TOOL_MODE_SHADOWS) {
                    self.colorShadowsValue = self.sliderValue;
                } else if(self.colorToolMode==COLOR_TOOL_MODE_HIGHLIGHTS) {
                    self.colorHighlightsValue = self.sliderValue;
                }
            }
            
            [self reloadSlider];
            [self reloadOutputImage];
        }
}

-(void)Ip4_backBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)Ip4_nextBtn:(id)sender
{
    [self doneAction];
}

-(void)Ip4_toolButton:(id)sender
{
    self.sliderControlBox.hidden = YES;
    [self.navBarIp4 changeMode:self.navBarIp4.toolButton];
    [self reloadToolsAndFilters];
    self.toolScrollView.alpha = 1.0;
    self.filterScrollView.alpha = 0.0;
}



-(void)Ip4_autoEnhanceButton:(id)sender
{
[self.navBarIp4 changeMode:self.navBarIp4.autoEnhanceButton];
self.sliderMode = SLIDER_MODE_ORIGIN_LEFT;
self.minSliderValue = 0.0;
self.maxSliderValue = 100.0;
self.currentEdittingTool = TOOL_AUTO_ENHANCE;
self.title = LOCALIZE(@"CLARITY");
self.originalSliderValue = self.autoEnhanceValue;
if(self.isFirstTimeAutoEnhance) {
    self.sliderValue = 50.0;
    self.autoEnhanceValue = 50.0;
} else {
    self.sliderValue = self.autoEnhanceValue;
}
[self enterToolEditMode];
[self reloadOutputImage];

}

-(void)Ip4_filterButton:(id)sender
{
    self.sliderControlBox.hidden = YES;
    [self.navBarIp4 changeMode:self.navBarIp4.filterButton];
    [self reloadToolsAndFilters];
    self.filterScrollView.alpha = 1.0;
    self.toolScrollView.alpha = 0.0;
}


-(void)filterButton:(id)sender
{
    
    [self.filterButton setSelected:YES];
    [self.autoEnhanceButton setSelected:NO];
    [self.toolButton setSelected:NO];
    
    [self reloadToolsAndFilters];
    self.filterScrollView.alpha = 1.0;
    self.toolScrollView.alpha = 0.0;
}

-(void)cancelTuningButton:(id)sender
{
if(self.currentEdittingTool==TOOL_BRIGHTNESS) {
        self.brightnessValue = self.originalSliderValue;
        [self exitToolEditMode];
    } else if(self.currentEdittingTool==TOOL_AUTO_ENHANCE) {
        self.autoEnhanceValue = self.originalSliderValue;
        [self exitToolEditMode];
    } else if(self.currentEdittingTool==TOOL_CONTRAST) {
        self.contrastValue = self.originalSliderValue;
        [self exitToolEditMode];
    } else if(self.currentEdittingTool==TOOL_FILTER) {
        [self.filterMixPercentageDict setObject:[NSNumber numberWithFloat:self.originalSliderValue] forKey:self.currentFilterCode];
        self.currentFilterCode = self.prevFilterCode;
        [self.currentFilter loadFilter:self.currentFilterCode];
        [self exitToolEditMode];
    } else if(self.currentEdittingTool==TOOL_WARMTH) {
        self.warmthValue = self.originalSliderValue;
        [self exitToolEditMode];
    } else if(self.currentEdittingTool==TOOL_SATURATION) {
        self.saturationValue = self.originalSliderValue;
        [self exitToolEditMode];
    } else if(self.currentEdittingTool==TOOL_FADE) {
        self.fadeValue = self.originalSliderValue;
        [self exitToolEditMode];
    } else if(self.currentEdittingTool==TOOL_SKIN) {
        self.skinSmoothValue = self.originalSliderValue;
        [self exitToolEditMode];
    } else if(self.currentEdittingTool==TOOL_HIGHLIGHTS) {
        self.highlightsValue = self.originalSliderValue;
        [self exitToolEditMode];
    } else if(self.currentEdittingTool==TOOL_SHADOWS) {
        self.shadowsValue = self.originalSliderValue;
        [self exitToolEditMode];
    } else if(self.currentEdittingTool==TOOL_VIGNETTE) {
        self.vignetteValue = self.originalSliderValue;
        [self exitToolEditMode];
    } else if(self.currentEdittingTool==TOOL_SHARPEN) {
        self.sharpenValue = self.originalSliderValue;
        [self exitToolEditMode];
    } else if(self.currentEdittingTool==TOOL_UNSHAR_MASK) {
        self.unsharpMaskValue = self.originalSliderValue;
        [self exitToolEditMode];
    } else if(self.currentEdittingTool==TOOL_TILT_SHIFT) {
        [self exitToolEditMode];
        
        if(self.originalTiltShiftMode!=TILT_SHIFT_MODE_OFF) {
            self.tiltControlArea.hidden = NO;
            
            [self blinkTiltControlArea];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.tiltControlArea.hidden = YES;
            });
        }
        
        self.tiltShiftMode = self.originalTiltShiftMode;
        [self reloadTiltShiftBtn];
    } else if(self.currentEdittingTool==TOOL_COLOR) {
        if(self.colorToolControlBox.isHidden==NO)  {
            // revert all color tool settings
            self.shadowsColorMapIndex = self.originalShadowsColorMapIndex;
            self.highlightsColorMapIndex = self.originalHighlightsColorMapIndex;
            self.colorShadowsValue = self.originalColorShadowsValue;
            self.colorHighlightsValue = self.originalColorHighlightsValue;
            
            [self exitToolEditMode];
        } else {
            // only revert color slider settings
            if(self.colorToolMode==COLOR_TOOL_MODE_SHADOWS) {
                self.colorShadowsValue = self.originalSliderValue;
            } else if(self.colorToolMode==COLOR_TOOL_MODE_HIGHLIGHTS) {
                self.colorHighlightsValue = self.originalSliderValue;
            }
            
            [self exitToolEditMode];
            [self enterColorMapEditMode];
        }
    }
    
    [self reloadOutputImage];
    //            lastLocationX = currentLocationX;
    
}


-(void)confirmTuningButton:(id)sender
{
    if([self valueChanged]){
        _anyChange=YES;
        [self addEditLineOn:self.toolScrollView atCenter:self.currentToolButton.center];
    }
    
    if(self.currentEdittingTool==TOOL_AUTO_ENHANCE) {
        self.isFirstTimeAutoEnhance = 0;
    }
    
    if(self.currentEdittingTool==TOOL_COLOR) {
        if(self.colorToolControlBox.isHidden==NO)  {
            [self exitToolEditMode];
        } else {
            [self exitToolEditMode];
            [self enterColorMapEditMode];
        }
    } else {
        [self exitToolEditMode];
    }
    
}

-(void)tiltShiftOffBtn:(id)sender
{
    self.tiltShiftMode = TILT_SHIFT_MODE_OFF;
    [self reloadOutputImage];
    [self.tiltControlArea setNeedsDisplay];
    [self reloadTiltShiftBtn];
}

-(void)tiltShiftRadialBtn:(id)sender
{    if(self.tiltShiftMode!=TILT_SHIFT_MODE_RADIAL) {
        [self blinkTiltControlArea];
    }
    
    self.tiltShiftMode = TILT_SHIFT_MODE_RADIAL;
    [self reloadOutputImage];
    [self.tiltControlArea setNeedsDisplay];
    [self reloadTiltShiftBtn];
}

-(void)tiltShiftLinearBtn:(id)sender
{
    if(self.tiltShiftMode!=TILT_SHIFT_MODE_LINEAR) {
        [self blinkTiltControlArea];
    }
    
    self.tiltShiftMode = TILT_SHIFT_MODE_LINEAR;
    [self reloadOutputImage];
    [self.tiltControlArea setNeedsDisplay];
    [self reloadTiltShiftBtn];
}

-(void)tapRecog:(UILongPressGestureRecognizer *)aGer
{
    

        if(aGer.state==UIGestureRecognizerStateBegan) {
            self.glOutputView.alpha = 0.0;
        }
        
        if(aGer.state==UIGestureRecognizerStateEnded) {
            self.glOutputView.alpha = 1.0;
        }

}


-(void)tapRecog_select:(UIGestureRecognizer *)sender
{
    
    UIView* selectedImage =  sender.view;
    NSString* currentFilterKey = filters[selectedImage.tag];
    
    /* Selected bar */
    self.selectedView.hidden = NO;
    UIView* filterView = sender.view;
    self.selectedView.center = filterView.center;
    self.selectedView.frame = CGRectMake(filterView.frame.origin.x, self.filterScrollView.frame.size.height-5, filterView.frame.size.width, 3);
    [self.filterScrollView addSubview:_selectedView];
    
    self.prevFilterCode = self.currentFilterCode;
    self.currentFilterCode = currentFilterKey;
    [self.currentFilter loadFilter:self.currentFilterCode];
    
    if(![self.currentFilterCode isEqualToString:NORMAL_FILTER]) {
        
        if([self.prevClickedFilterCode isEqualToString:currentFilterKey]) {
            self.sliderMode = SLIDER_MODE_ORIGIN_LEFT;
            self.minSliderValue = 0.0;
            self.maxSliderValue = 100.0;
            self.currentEdittingTool = TOOL_FILTER;
            self.sliderValue = [self.filterMixPercentageDict[currentFilterKey] floatValue];
            self.originalSliderValue = [self.filterMixPercentageDict[currentFilterKey] floatValue];
            self.title = LOCALIZE(currentFilterKey);
            [self enterToolEditMode];
        }
        
        self.prevClickedFilterCode = currentFilterKey;
    }
    
    [self reloadOutputImage];
}

-(void)tempToolButtonBRI:(id)sender
{
    UIButton *clicked = (UIButton *) sender;
        self.title = _toolNames[clicked.tag];
    self.sliderMode = SLIDER_MODE_ORIGIN_CENTER;
    self.minSliderValue = -100.0;
    self.maxSliderValue = 100.0;
    self.currentEdittingTool = TOOL_BRIGHTNESS;
    self.sliderValue = self.brightnessValue;
    self.originalSliderValue = self.brightnessValue;
    self.currentToolButton = clicked;

    [self enterToolEditMode];
}

-(void)tempToolButtonCON:(id)sender
{
    self.sliderMode = SLIDER_MODE_ORIGIN_CENTER;
    self.minSliderValue = -100.0;
    self.maxSliderValue = 100.0;
    self.currentEdittingTool = TOOL_CONTRAST;
    UIButton *clicked = (UIButton *) sender;
    self.title = _toolNames[clicked.tag];
    self.sliderValue = self.contrastValue;
    self.originalSliderValue = self.contrastValue;
    self.currentToolButton = clicked;
    [self enterToolEditMode];
}

-(void)tempToolButtonWARM:(id)sender
{
    self.sliderMode = SLIDER_MODE_ORIGIN_CENTER;
    self.minSliderValue = -100.0;
    self.maxSliderValue = 100.0;
    self.currentEdittingTool = TOOL_WARMTH;
    UIButton *clicked = (UIButton *) sender;
    self.title = _toolNames[clicked.tag];    self.sliderValue = self.warmthValue;
    self.originalSliderValue = self.warmthValue;
    self.currentToolButton = clicked;
    [self enterToolEditMode];
}

-(void)tempToolButtonSATU:(id)sender
{
    self.sliderMode = SLIDER_MODE_ORIGIN_CENTER;
    self.minSliderValue = -100.0;
    self.maxSliderValue = 100.0;
    self.currentEdittingTool = TOOL_SATURATION;
    UIButton *clicked = (UIButton *) sender;
    self.title = _toolNames[clicked.tag];    self.sliderValue = self.saturationValue;
    self.originalSliderValue = self.saturationValue;
    self.currentToolButton = clicked;
    
    [self enterToolEditMode];
}
-(void)tempToolButtonFADE:(id)sender
{
    self.sliderMode = SLIDER_MODE_ORIGIN_LEFT;
    self.minSliderValue = 0.0;
    self.maxSliderValue = 100.0;
    self.currentEdittingTool = TOOL_FADE;
    UIButton *clicked = (UIButton *) sender;
    NSLog(@"%ld",(long)clicked.tag);
    self.title = _toolNames[clicked.tag];    self.sliderValue = self.fadeValue;
    self.originalSliderValue = self.fadeValue;
    self.currentToolButton = clicked;
    
    [self enterToolEditMode];
}

-(void)tempToolButtonBEAU:(id)sender
{
    self.sliderMode = SLIDER_MODE_ORIGIN_LEFT;
    self.minSliderValue = 0.0;
    self.maxSliderValue = 100.0;
    self.currentEdittingTool = TOOL_SKIN;
    UIButton *clicked = (UIButton *) sender;
    self.title = _toolNames[clicked.tag];    self.sliderValue = self.skinSmoothValue;
    self.originalSliderValue = self.skinSmoothValue;
    self.currentToolButton = clicked;
    
    [self enterToolEditMode];
}

-(void)tempToolButtonHIGH:(id)sender
{
    self.sliderMode = SLIDER_MODE_ORIGIN_CENTER;
    self.minSliderValue = -100.0;
    self.maxSliderValue = 100.0;
    self.currentEdittingTool = TOOL_HIGHLIGHTS;
    UIButton *clicked = (UIButton *) sender;
    self.title = _toolNames[clicked.tag];
    self.sliderValue = self.highlightsValue;
    self.originalSliderValue = self.highlightsValue;
    self.currentToolButton = clicked;
    
    [self enterToolEditMode];
}


-(void)tempToolButtonSHAD:(id)sender
{
    self.sliderMode = SLIDER_MODE_ORIGIN_CENTER;
    self.minSliderValue = -100.0;
    self.maxSliderValue = 100.0;
    self.currentEdittingTool = TOOL_SHADOWS;
    UIButton *clicked = (UIButton *) sender;
    self.title = _toolNames[clicked.tag];
    self.sliderValue = self.shadowsValue;
    self.originalSliderValue = self.shadowsValue;
    self.currentToolButton = clicked;
    
    [self enterToolEditMode];
}

-(void)tempToolButtonVIGN:(id)sender
{
    self.sliderMode = SLIDER_MODE_ORIGIN_LEFT;
    self.minSliderValue = 0.0;
    self.maxSliderValue = 100.0;
    self.currentEdittingTool = TOOL_VIGNETTE;
    UIButton *clicked = (UIButton *) sender;
    self.title = _toolNames[clicked.tag];
    self.sliderValue = self.vignetteValue;
    self.originalSliderValue = self.vignetteValue;
    self.currentToolButton = clicked;
    
    [self enterToolEditMode];
}

-(void)tempToolButtonSHAR:(id)sender
{
    self.sliderMode = SLIDER_MODE_ORIGIN_LEFT;
    self.minSliderValue = 0.0;
    self.maxSliderValue = 100.0;
    self.currentEdittingTool = TOOL_SHARPEN;
    UIButton *clicked = (UIButton *) sender;
//    NSLog(@"%ld",(long)clicked.tag);
    self.title = _toolNames[clicked.tag];
    self.sliderValue = self.sharpenValue;
    self.originalSliderValue = self.sharpenValue;
    self.currentToolButton = clicked;
    
    [self enterToolEditMode];
}

-(void)tempToolButtonSTRU:(id)sender
{
    
    self.sliderMode = SLIDER_MODE_ORIGIN_LEFT;
    self.minSliderValue = 0.0;
    self.maxSliderValue = 100.0;
    self.currentEdittingTool = TOOL_UNSHAR_MASK;
    UIButton *clicked = (UIButton *) sender;
    self.title = _toolNames[clicked.tag];
    self.sliderValue = self.unsharpMaskValue;
    self.originalSliderValue = self.unsharpMaskValue;
    self.currentToolButton = clicked;
    
    [self enterToolEditMode];
}

-(void)tempToolButtonCOLO:(id)sender
{
    self.sliderMode = SLIDER_MODE_ORIGIN_LEFT;
    self.minSliderValue = 0.0;
    self.maxSliderValue = 100.0;
    self.currentEdittingTool = TOOL_COLOR;
    UIButton *clicked = (UIButton *) sender;
    self.title = _toolNames[clicked.tag];
    self.currentToolButton = clicked;
    
    self.colorToolMode = COLOR_TOOL_MODE_SHADOWS;
    
    self.originalHighlightsColorMapIndex = self.highlightsColorMapIndex;
    self.originalShadowsColorMapIndex = self.shadowsColorMapIndex;
    self.originalColorShadowsValue = self.colorShadowsValue;
    self.originalColorHighlightsValue = self.colorHighlightsValue;
    
    [self enterColorMapEditMode];
}


-(void)tempToolButtonADJU:(id)sender
{
    // show CropImageVC
    CropImageViewController* cropImageVC = [CropImageViewController new];
    cropImageVC.delegate = self;
    cropImageVC.srcImage = self.srcImage;
       UIButton *clicked = (UIButton *) sender;
    self.currentToolButton = clicked;
    [self presentViewController:NAV(cropImageVC) animated:NO completion:^{
        
    }];
}

-(void)tempToolButtonTILT:(id)sender
{
    
    self.currentEdittingTool = TOOL_TILT_SHIFT;
    UIButton *clicked = (UIButton *) sender;
    self.title = _toolNames[clicked.tag];
    self.originalTiltShiftMode = self.tiltShiftMode;
    self.currentToolButton = clicked;
    
    [self enterTiltEditMode];
}

-(void)tapRecog_filter:(UIGestureRecognizer *)sender
{
    UIView* selectedImage =  sender.view;
    NSString* currentFilterKey = filters[selectedImage.tag];
    
    /* Selected bar */
    self.selectedView.hidden = NO;
    UIView* filterView = sender.view;
    self.selectedView.center = filterView.center;
    self.selectedView.frame = CGRectMake(filterView.frame.origin.x,self.filterScrollView.frame.size.height-5, filterView.frame.size.width, 3);
    [self.filterScrollView addSubview:_selectedView];
    
    
    self.prevFilterCode = self.currentFilterCode;
    self.currentFilterCode = currentFilterKey;
    [self.currentFilter loadFilter:self.currentFilterCode];
    
    if(![self.currentFilterCode isEqualToString:NORMAL_FILTER]) {
        
        if([self.prevClickedFilterCode isEqualToString:currentFilterKey]) {
            self.sliderMode = SLIDER_MODE_ORIGIN_LEFT;
            self.minSliderValue = 0.0;
            self.maxSliderValue = 100.0;
            self.currentEdittingTool = TOOL_FILTER;
            self.sliderValue = [self.filterMixPercentageDict[currentFilterKey] floatValue];
            self.originalSliderValue = [self.filterMixPercentageDict[currentFilterKey] floatValue];
            self.title = LOCALIZE(currentFilterKey);
            [self enterToolEditMode];
        }
        
        self.prevClickedFilterCode = currentFilterKey;
    }
    
    [self reloadOutputImage];
}

-(void)shadowButton:(id)sender
{
    self.colorToolMode = COLOR_TOOL_MODE_SHADOWS;
    [self enterColorMapEditMode];
}

-(void)highlightsButton:(id)sender
{
    self.colorToolMode = COLOR_TOOL_MODE_HIGHLIGHTS;
    [self enterColorMapEditMode];
}

-(void)colorBtn:(id)sender
{
    UIButton *clicked = (UIButton *) sender;
//    NSLog(@"%ld",(long)clicked.tag);
    BOOL showSlider = NO;
    
    if( clicked.tag==0) {
        if(self.colorToolMode==COLOR_TOOL_MODE_SHADOWS) {
            self.colorShadowsValue = 50.0;
            self.shadowsColorMapIndex = 0.0;
        } else if(self.colorToolMode==COLOR_TOOL_MODE_HIGHLIGHTS) {
            self.colorHighlightsValue = 50.0;
            self.highlightsColorMapIndex = 0.0;
        }
    } else {
        // if already operating on this color: show slider tool
        if(self.colorToolMode==COLOR_TOOL_MODE_SHADOWS) {
            if(self.shadowsColorMapIndex==(float)(clicked.tag-1)) {
                showSlider = YES;
            }
            
            self.shadowsColorMapIndex = (float) (clicked.tag-1);
        } else if(self.colorToolMode==COLOR_TOOL_MODE_HIGHLIGHTS) {
            if(self.highlightsColorMapIndex==(float)(clicked.tag-1)) {
                showSlider = YES;
            }
            
            self.highlightsColorMapIndex = (float) (clicked.tag-1);
        }
    }
    
    [self reloadOutputImage];
    [self enterColorMapEditMode];
    
    if(showSlider) {
        if(self.colorToolMode==COLOR_TOOL_MODE_SHADOWS) {
            self.sliderValue = self.colorShadowsValue;
            self.originalSliderValue = self.colorShadowsValue;
        } else if(self.colorToolMode==COLOR_TOOL_MODE_HIGHLIGHTS) {
            self.sliderValue = self.colorHighlightsValue;
            self.originalSliderValue = self.colorHighlightsValue;
        }
        
        [self enterToolEditMode];
    }
}
 @end
