#import "RotateControlAreaView.h"

@implementation RotateControlAreaView

@synthesize cropImageVC;

- (void)drawRect:(CGRect)rect
{
    // draw a line at center!
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, MAIN_COLOR.CGColor);
    CGContextSetLineWidth(context, 2.0f);
    
    CGContextMoveToPoint(context, rect.size.width/2, 10);
    CGContextAddLineToPoint(context, rect.size.width/2, rect.size.height-10);
    CGContextStrokePath(context);
    
    // draw scale lines!
    float stepSize = 25.0;
    int numberOfSteps = 45; // 45 is enough for program to draw all scale lines, too small cause scale line missing problem!
    float startX = rect.size.width/2 + cropImageVC.currentRotation*stepSize;
    
    for(int i=0;i<numberOfSteps;i++) {
        float yMargin = 20.0;
        
        if(i%5==0) {
            yMargin = 10.0;
        }
        
        CGContextSetLineWidth(context, 1.0f);
        
        // left line
        float xPos = 0.0;
        
        xPos = startX-stepSize*i;
        
        if(xPos>0.0) {
            CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:1.0 alpha:1.0-0.8*fabs(xPos-rect.size.width/2.0)/rect.size.width*2.0].CGColor);

            CGContextMoveToPoint(context, xPos, yMargin);
            CGContextAddLineToPoint(context, xPos, rect.size.height-yMargin);
            CGContextStrokePath(context);
        }

        // right line
        xPos = startX+stepSize*i;

        if(xPos<rect.size.width) {
            CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:1.0 alpha:1.0-0.8*fabs(xPos-rect.size.width/2.0)/rect.size.width*2.0].CGColor);

            CGContextMoveToPoint(context, xPos, yMargin);
            CGContextAddLineToPoint(context, xPos, rect.size.height-yMargin);
            CGContextStrokePath(context);
        }
    }
}

@end
