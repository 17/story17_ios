#import <UIKit/UIKit.h>
#import "FilterViewController.h"

@interface TiltControlAreaView : UIView

@property (nonatomic, assign) FilterViewController* filterVC;
@property BOOL showTiltControl;

@end
