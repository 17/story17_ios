#import "TiltControlAreaView.h"

@implementation TiltControlAreaView

@synthesize filterVC;
@synthesize showTiltControl;

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    if(!showTiltControl) {
        return;
    }
    
    if(filterVC.tiltShiftMode==TILT_SHIFT_MODE_RADIAL) {
        // draw circle
        float radiusInView = SCREEN_WIDTH*filterVC.tiltShiftRadialRadius;
        
        radiusInView = radiusInView*0.75;
        
        CGRect smallBorderRect = CGRectMake(SCREEN_WIDTH*filterVC.tiltShiftRadialCenterX-radiusInView/2, SCREEN_WIDTH*filterVC.tiltShiftRadialCenterY-radiusInView/2, radiusInView, radiusInView);
        
        radiusInView = SCREEN_WIDTH*filterVC.tiltShiftRadialRadius;
        radiusInView = radiusInView*1.25;
        
        CGRect largeBorderRect = CGRectMake(SCREEN_WIDTH*filterVC.tiltShiftRadialCenterX-radiusInView/2, SCREEN_WIDTH*filterVC.tiltShiftRadialCenterY-radiusInView/2, radiusInView, radiusInView);

        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetRGBStrokeColor(context, 1.0, 1.0, 1.0, 1.0);
        CGContextSetLineWidth(context, 2.0);
        CGContextStrokeEllipseInRect(context, smallBorderRect);
        CGContextStrokeEllipseInRect(context, largeBorderRect);

        CGContextFillPath(context);
    } else if(filterVC.tiltShiftMode==TILT_SHIFT_MODE_LINEAR) {
        // draw line
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
        
        // Draw them with a 2.0 stroke width so they are a bit more visible.
        CGContextSetLineWidth(context, 2.0f);
        
        float centerX = filterVC.tiltShiftLinearCenterX*SCREEN_WIDTH;
        float centerY = filterVC.tiltShiftLinearCenterY*SCREEN_WIDTH;
        float normalDeltaX = sin(filterVC.tiltShiftLinearAngle)*filterVC.tiltShiftLinearRadius*SCREEN_WIDTH;
        float normalDeltaY = cos(filterVC.tiltShiftLinearAngle)*filterVC.tiltShiftLinearRadius*SCREEN_WIDTH;
        float deltaX = SCREEN_WIDTH*2*cos(filterVC.tiltShiftLinearAngle);
        float deltaY = SCREEN_WIDTH*2*sin(filterVC.tiltShiftLinearAngle);
        
        // print lines
        CGContextMoveToPoint(context, centerX+normalDeltaX, centerY+normalDeltaY);
        CGContextAddLineToPoint(context, centerX+normalDeltaX-deltaX, centerY+normalDeltaY+deltaY);
        CGContextStrokePath(context);
        
        CGContextMoveToPoint(context, centerX-normalDeltaX, centerY-normalDeltaY);
        CGContextAddLineToPoint(context, centerX-normalDeltaX-deltaX, centerY-normalDeltaY+deltaY);
        CGContextStrokePath(context);
        
        CGContextMoveToPoint(context, centerX+normalDeltaX, centerY+normalDeltaY);
        CGContextAddLineToPoint(context, centerX+normalDeltaX+deltaX, centerY+normalDeltaY-deltaY);
        CGContextStrokePath(context);
        
        CGContextMoveToPoint(context, centerX-normalDeltaX, centerY-normalDeltaY);
        CGContextAddLineToPoint(context, centerX-normalDeltaX+deltaX, centerY-normalDeltaY-deltaY);
        CGContextStrokePath(context);

        // print normal vector
//        CGContextMoveToPoint(context, centerX+normalDeltaX, centerY+normalDeltaY);
//        CGContextAddLineToPoint(context, centerX-normalDeltaX, centerY-normalDeltaY);
//        CGContextStrokePath(context);
    }
}

@end
