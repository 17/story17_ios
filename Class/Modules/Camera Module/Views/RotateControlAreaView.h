#import <UIKit/UIKit.h>
#import "Constant.h"
#import "CropImageViewController.h"

@interface RotateControlAreaView : UIView

@property (nonatomic, weak) CropImageViewController* cropImageVC;

@end
