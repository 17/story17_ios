#import <UIKit/UIKit.h>
#import "Constant.h"
#import "MyFilter.h"
#import "CropImageViewController.h"
#import "FilterTopIp4Container.h"
#import "UIImage+Helper.h"

#define SLIDER_MODE_ORIGIN_LEFT 0
#define SLIDER_MODE_ORIGIN_CENTER 1

#define COLOR_TOOL_MODE_SHADOWS 0
#define COLOR_TOOL_MODE_HIGHLIGHTS 1

#define TILT_SHIFT_MODE_OFF 0.0
#define TILT_SHIFT_MODE_RADIAL 1.0
#define TILT_SHIFT_MODE_LINEAR 2.0

// filter tool type constants
typedef NS_ENUM(NSInteger, FILTER_TOOL_TYPE) {
    TOOL_FILTER = 0,
    
    TOOL_AUTO_ENHANCE,
    
    // tools
    TOOL_ADJUST,
    TOOL_BRIGHTNESS,
    TOOL_CONTRAST,
    TOOL_SKIN,
    TOOL_UNSHAR_MASK,
    TOOL_WARMTH,
    TOOL_SATURATION,
    TOOL_COLOR,
    TOOL_FADE,
    TOOL_HIGHLIGHTS,
    TOOL_SHADOWS,
    TOOL_VIGNETTE,
    TOOL_TILT_SHIFT,
    TOOL_SHARPEN,
    
    TOOL_MAX
};

// filter tool type constants, change to this next time refactor
//typedef NS_ENUM(NSInteger, FilterTool) {
//    FilterToolFilter = 0,
//    FilterToolAutoEnhance,

//    // tools
//    FilterToolClarity,
//    FilterToolAdjust,
//    FilterToolBrightness,
//    FilterToolContrast,
//    FilterToolBeauty,
//    FilterToolStructure,
//    FilterToolWarmth,
//    FilterToolSaturation,
//    FilterToolColor,
//    FilterToolFade,
//    FilterToolHighlights,
//    FilterToolShadows,
//    FilterToolVignette,
//    FilterToolTiltShift,
//    FilterToolSharpen,
//
//    FilterToolMax
//};

@class TiltControlAreaView;

@interface FilterViewController : UIViewController <UIGestureRecognizerDelegate, CropImageViewControllerDelegate, GLKViewDelegate>

@property (nonatomic, strong) MyFilter* currentFilter;
@property (nonatomic, strong) NSString* currentFilterCode;
@property (nonatomic, strong) NSString* prevFilterCode;
@property (nonatomic, strong) NSString* prevClickedFilterCode;

@property (nonatomic, strong) FilterTopIp4Container* navBarIp4;
@property (nonatomic, strong) NSMutableArray* filters;

@property (nonatomic, strong) UIImage* srcImage;
@property (nonatomic, strong) UIImage* srcThumbnailImage;
@property (nonatomic, strong) GLKView* glOutputView;
@property float navBarHeight;

@property (nonatomic, strong) UIImageView* srcImageView;
@property (nonatomic, strong) UIButton* toolButton;
@property (nonatomic, strong) UIButton* filterButton;
@property (nonatomic, strong) UIButton* autoEnhanceButton;
@property (nonatomic, strong) UIImageView* currentToolIndicatorImageView;

@property (nonatomic, strong) UIScrollView* filterScrollView;
@property (nonatomic, strong) UIScrollView* toolScrollView;

@property (nonatomic, strong) UIActivityIndicatorView* loadingView;
@property (nonatomic, strong) UIView* selectedView;
@property (nonatomic, strong) UIButton* currentToolButton;
@property (nonatomic,strong) UIButton* tempToolButton;
@property (nonatomic,strong) NSString* toolName;


@property int currentEdittingTool;
@property int enterPageTimeStamp;
@property BOOL anyChange;
// tilt control
@property (nonatomic, strong) TiltControlAreaView* tiltControlArea;

// slider
@property (nonatomic, strong) UIView* colorToolControlBox;
@property (nonatomic, strong) UIView* tiltShiftControlBox;
@property (nonatomic, strong) UIView* sliderControlBox;

@property (nonatomic, strong) UILabel* sliderValueLabel;
@property (nonatomic, strong) UIImageView* originSliderBall;
@property (nonatomic, strong) UIImageView* currentSliderBall;
@property (nonatomic, strong) UIView* emptySliderBar;
@property (nonatomic, strong) UIView* fullSliderBar;
@property float originalSliderValue;
@property float sliderValue;
@property float minSliderValue;
@property float maxSliderValue;
@property int sliderMode;
@property (nonatomic, strong) UIView* tuningConfirmButtonContainer;
@property (nonatomic, strong) UIButton* cancelTuningButton;
@property (nonatomic, strong) UIButton* backButton;
@property (nonatomic, strong) UIButton* confirmTuningButton;
@property (nonatomic, strong) UIButton* tiltShiftOffBtn;
@property (nonatomic, strong) UIButton* tiltShiftRadialBtn;
@property (nonatomic, strong) UIButton* tiltShiftLinearBtn;

// tool settings
@property (nonatomic, strong) NSMutableDictionary* filterMixPercentageDict;
@property (nonatomic, strong) NSMutableDictionary* filterImageCacheDict;
@property int isFirstTimeAutoEnhance;
@property float brightnessValue;
@property float contrastValue;
@property float autoEnhanceValue;
@property float warmthValue;
@property float saturationValue;
@property float fadeValue;
@property float skinSmoothValue;
@property float highlightsValue;
@property float shadowsValue;
@property float vignetteValue;
@property float sharpenValue;
@property float unsharpMaskValue;
@property float colorShadowsValue;
@property float colorHighlightsValue;
@property float shadowsColorMapIndex;
@property float highlightsColorMapIndex;
@property float originalShadowsColorMapIndex;
@property float originalHighlightsColorMapIndex;
@property float originalColorShadowsValue;
@property float originalColorHighlightsValue;
@property int colorToolMode;
@property BOOL isAdjust;
@property (nonatomic,strong) NSArray* toolNames;


@property float originalTiltShiftMode; // 0.0: Off, 1.0: radial, 2.0: linear
@property float tiltShiftMode; // 0.0: Off, 1.0: radial, 2.0: linear
@property float tiltShiftRadialCenterX; // 0.0 ~ 1.0
@property float tiltShiftRadialCenterY; // 0.0 ~ 1.0
@property float tiltShiftRadialRadius; // 0.1 ~ 0.9
@property float tiltShiftLinearCenterX; // 0.0 ~ 1.0
@property float tiltShiftLinearCenterY; // 0.0 ~ 1.0
@property float tiltShiftLinearRadius; // 0.05 ~ 0.9
@property float tiltShiftLinearAngle; // 0.0 ~ M_PI*2
@property BOOL panRecognizerInAction;
@property BOOL rotateRecognizerInAction;
@property BOOL pinchRecognizerInAction;

@end
