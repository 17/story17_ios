#import <UIKit/UIKit.h>
#import "Constant.h"
#import <math.h>

@class RotateControlAreaView;

@protocol CropImageViewControllerDelegate <NSObject>
-(void) didCompleteImageCrop:(UIImage*) croppedImage isChanged:(BOOL)isChanged;
@end

@interface CropImageViewController : UIViewController <UIGestureRecognizerDelegate>


//@property (copy)void (^cropImageCallback)(BOOL cropChanged);

@property (nonatomic, strong) UIImage* srcImage;
@property (nonatomic, strong) UIButton* cancelBtn;
@property (nonatomic, strong) UIButton* doneBtn;
@property (nonatomic, strong) UILabel* rotationLabel;
//@property (nonatomic, strong) UIScrollView* imageScrollView;
@property (nonatomic, strong) UIImageView* previewImageView;
@property (nonatomic, strong) UIImageView* gridImageView;
@property (nonatomic, strong) RotateControlAreaView* rotateControlAreaView;
@property (nonatomic, strong) UIPanGestureRecognizer* imageRotationPanRecog;
@property float currentRotation;
@property float currentScale;
@property float currentCenterX;
@property float currentCenterY;
@property BOOL isScaling;
@property BOOL isTranslating;
@property int rotateActionCount;

@property (nonatomic, weak) id<CropImageViewControllerDelegate> delegate;

@end
