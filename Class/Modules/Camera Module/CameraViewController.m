#import "CameraViewController.h"
#import "FilterViewController.h"
#import "TWPhotoPickerController.h"
#import "PublishPostViewController.h"
#import "LiveStreamViewController.h"
#import "UIImage+Helper.h"

#define TOP_CONTROL_PANEL_HEIGHT 44.0

#define CAPTURE_MODE_PHOTO 1
#define CAPTURE_MODE_VIDEO 2
#define CAPTURE_MODE_LIVESTREAM 3

#define PLANE_Y 0
#define PLANE_CBCR 1

#define MOVIE_PROGRESS_BAR_HEIGHT 4.0

@interface CameraViewController()

// capture session
@property (nonatomic, strong) AVCaptureSession* captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *previewLayer;
@property (nonatomic, strong) AVCaptureDeviceInput *inputDevice;
@property (nonatomic, strong) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic, strong) AVCaptureVideoDataOutput *videoDataOutput;
@property (nonatomic, strong) AVCaptureDevice *captureDevice;
@property (nonatomic, strong) UIImage *stillImage;
@property (nonatomic, strong) UIImage *coverImage;
@property BOOL isFrontCamera;
@property (nonatomic, strong) CIContext* ciContext;

// control btns
@property (nonatomic, strong) UIButton* albumPickerButton;
@property (nonatomic, strong) UIButton* takePictureButton;
@property (nonatomic, strong) UIButton* recordVideoButton;
@property (nonatomic, strong) UIButton* gridToggleButton;
@property (nonatomic, strong) UIButton* cameraSwitchButton;
@property (nonatomic, strong) UIButton* flashModeSwitchButton;
@property (nonatomic, strong) UIView* topControlButtonContainer;

@property (nonatomic, strong) UIView* bottomControlButtonContainer;
@property (nonatomic, strong) UIView* upperShutterImage;
@property (nonatomic, strong) UIView* lowerShutterImage;

@property (nonatomic, strong) UIImageView *focusImageView;
@property (nonatomic, strong) UIImageView *gridImageView;
@property (nonatomic, strong) UIView* cameraPreviewView;
@property (nonatomic, strong) UIView* cameraShtterView;
@property (nonatomic, strong) UIImageView* focusIndicatorView;

@property (nonatomic, strong) UIView* modeSlideView;
@property (nonatomic, strong) UIButton* photoModeBtn;
@property (nonatomic, strong) UIButton* videoModeBtn;
@property (nonatomic, strong) UIButton* liveModeBtn;

@property (nonatomic, strong) UIButton* exitButton;
@property dispatch_queue_t videoQueue;
@property (nonatomic, strong) NSString* videoUUID;

@property PCMAudioRecorder* pcmAudioRecorder;
@property unsigned char* movieFrameDataBuffer;

@property int reloadUICount;
@property int audioSampleLaggingCount;
@property int videoSampleLaggingCount;
@property (nonatomic, strong) UIView* videoSegmentContainer;
@property (nonatomic, strong) UIButton* deleteVideoSegmentBtn;
@property (nonatomic, strong) UIButton* videoDoneBtn;
@property (nonatomic, strong) NSMutableArray* audioDataCache; // array of NSData
@property (nonatomic, strong) NSMutableArray* audioDataSampleCountArray; // array of NSNumber of int
@property (nonatomic, strong) NSMutableArray* videoDataSampleCountArray; // array of NSNumber of int
@property (nonatomic, strong) NSMutableArray* movieSegmentDurationArray; // array of NSNumber of float

@property (nonatomic, strong) NSTimer* movieRecordUIUpdateTimer;
@property BOOL isRecordingVideo;
@property int totalVideoFrameCount;
@property int totalAudioFrameCount;
@property int currentSegmentVideoFrameCount;
@property int currentSegmentAudioFrameCount;
@property int movieSegmentCount;
@property float currentMovieDuration;
@property float recordBeginTime;
@property float recordEndTime; // TRICKY: this is pseudo end time, because we record 0.5 more seconds of data after long press end
@property BOOL isDeletingMovieSegment;

@property (nonatomic, strong) EAGLContext* glContext;
@property (nonatomic, strong) CIContext* videoOutputContext;
@property (nonatomic, strong) GLKView* glVideoOutputView;

@property (nonatomic, strong) UIView* topView;
@property (nonatomic, strong) UILabel* warningLabel;
@property (nonatomic, strong) UILabel* descriptLabel;
@property (nonatomic, strong) UILabel* linkLabel;
@property (nonatomic) BOOL isChanging;
@property (nonatomic) BOOL isFirstPresent;
@property int enterPageTimeStamp;
@end

@implementation CameraViewController

#define modeBtnWidth 60.0f
#define numOfMode 3
#define cameraHeight SCREEN_WIDTH*4.0/3.0
#define modeBtnHeight 40

-(id)init
{
    self = [super init];
    if(self){
        
        _isFirstPresent = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _isChanging = false;
    
    self.view.backgroundColor = MAIN_COLOR;
    
    __weak CameraViewController* weakself = self;
    
    _videoUUID = [SINGLETON getUUID];

    _cameraPreviewView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.view addSubview:_cameraPreviewView];

    float previewHeight = SCREEN_WIDTH*4.0/3.0;
    _cameraShtterView = [[UIView alloc] initWithFrame:CGRectMake(0, TOP_CONTROL_PANEL_HEIGHT-(previewHeight-SCREEN_WIDTH)/2, SCREEN_WIDTH, previewHeight)];
    [self.view addSubview:_cameraShtterView];
    
    [_cameraShtterView addGestureRecognizer:[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panGesture:)]];
    _cameraShtterView.userInteractionEnabled = YES;
    
//    [_cameraShtterView setBackgroundColor:RED_COLOR];
    
    /* queue */
    _videoQueue = dispatch_queue_create("video_queue", DISPATCH_QUEUE_SERIAL);
    
    _movieFrameDataBuffer = malloc(FRAME_WIDTH*FRAME_HEIGHT*3/2);

//    _currentCaptureMode = CAPTURE_MODE_PHOTO;

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _ciContext = [CIContext contextWithOptions:nil];
    });
    
    /* setup top controls */
    _topControlButtonContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, TOP_CONTROL_PANEL_HEIGHT)];
    _topControlButtonContainer.backgroundColor = BLACK_COLOR;
//    [_topControlButtonContainer addGestureRecognizer:[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panGesture:)]];
    _topControlButtonContainer.userInteractionEnabled = YES;
    [self.view addSubview:_topControlButtonContainer];
    
    // grid toggle
    _gridToggleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _gridToggleButton.showsTouchWhenHighlighted = YES;
    _gridToggleButton.frame = CGRectMake(0, 0, 40, 40);
    _gridToggleButton.center = CGPointMake(2*SCREEN_WIDTH/3+10, 22);
    
    [_gridToggleButton bk_addEventHandler:^(id sender) {
        
        if([[DEFAULTS objectForKey:CAMERA_GRID_MODE] isEqualToString:CAMERA_GRID_MODE_ON]) {
            [DEFAULTS setObject:CAMERA_GRID_MODE_OFF forKey:CAMERA_GRID_MODE];
        } else {
            [DEFAULTS setObject:CAMERA_GRID_MODE_ON forKey:CAMERA_GRID_MODE];
        }
        
        [DEFAULTS synchronize];
        
        [weakself reloadUI];
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    [_topControlButtonContainer addSubview:_gridToggleButton];
    
    _gridImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, SCREEN_WIDTH)];
    _gridImageView.image = [UIImage imageNamed:@"camera_grid"];
    [self.view addSubview:_gridImageView];
    
    // camera switch
    _cameraSwitchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _cameraSwitchButton.showsTouchWhenHighlighted = YES;
    _cameraSwitchButton.frame = CGRectMake(0, 0, 40, 40);
    _cameraSwitchButton.center = CGPointMake(5*SCREEN_WIDTH/6+20, 22);
    [_cameraSwitchButton setBackgroundImage:[UIImage imageNamed:@"camera_cameratoggle"] forState:UIControlStateNormal];
    [_cameraSwitchButton setBackgroundImage:[UIImage imageNamed:@"camera_cameratoggle_down"] forState:UIControlStateHighlighted];
    
    [_cameraSwitchButton bk_addEventHandler:^(id sender) {
        weakself.isFrontCamera = !weakself.isFrontCamera;
        [weakself setupCamera];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [_topControlButtonContainer addSubview:_cameraSwitchButton];
    
    // flash switch
    _flashModeSwitchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _flashModeSwitchButton.showsTouchWhenHighlighted = YES;
    _flashModeSwitchButton.frame = CGRectMake(0, 0, 40, 40);
    _flashModeSwitchButton.center = CGPointMake(SCREEN_WIDTH/2, 22);
    
    [_flashModeSwitchButton bk_addEventHandler:^(id sender) {
        if([[DEFAULTS objectForKey:CAMERA_FLASH_MODE] isEqualToString:CAMERA_FLASH_MODE_AUTO]) {
            [DEFAULTS setObject:CAMERA_FLASH_MODE_OFF forKey:CAMERA_FLASH_MODE];
        } else if([[DEFAULTS objectForKey:CAMERA_FLASH_MODE] isEqualToString:CAMERA_FLASH_MODE_OFF]) {
            [DEFAULTS setObject:CAMERA_FLASH_MODE_ON forKey:CAMERA_FLASH_MODE];
        } else if([[DEFAULTS objectForKey:CAMERA_FLASH_MODE] isEqualToString:CAMERA_FLASH_MODE_ON]) {
            [DEFAULTS setObject:CAMERA_FLASH_MODE_AUTO forKey:CAMERA_FLASH_MODE];
        }
        
        [DEFAULTS synchronize];
        
        [weakself reloadUI];
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    [_topControlButtonContainer addSubview:_flashModeSwitchButton];
    
    /* setup bottom controls */
    float bottomControlButtonContainerCenterY = (SCREEN_HEIGHT-SCREEN_WIDTH-TOP_CONTROL_PANEL_HEIGHT+1)/2;
    
    _bottomControlButtonContainer = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_WIDTH+TOP_CONTROL_PANEL_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT-SCREEN_WIDTH-TOP_CONTROL_PANEL_HEIGHT+1)];
    [self.view addSubview:_bottomControlButtonContainer];
    _bottomControlButtonContainer.backgroundColor = BLACK_COLOR;
//    UIPanGestureRecognizer* pan =[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panGesture:)];
//    pan.delegate = self;
//    [_bottomControlButtonContainer addGestureRecognizer:pan];
    _bottomControlButtonContainer.userInteractionEnabled = YES;
    
    // exit button
    _exitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _exitButton.frame = CGRectMake(8, 6, 32, 32);
    [_exitButton setBackgroundImage:[UIImage imageNamed:@"camera_close"] forState:UIControlStateNormal];
    [_exitButton setBackgroundImage:[UIImage imageNamed:@"camera_close_down"] forState:UIControlStateHighlighted];
    
    [_exitButton bk_addEventHandler:^(id sender) {
        [weakself stopPcmRecorder];
        if (_currentCaptureMode==CAPTURE_MODE_PHOTO) {
            [EVENT_HANDLER addEventTracking:@"LeavePublishPageWithoutPub" withDict:@{@"type":@"photo",@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp)}];
        }else{
            [EVENT_HANDLER addEventTracking:@"LeavePublishPageWithoutPub" withDict:@{@"type":@"video",@"duration":INT_TO_STRING(CURRENT_TIMESTAMP-_enterPageTimeStamp)}];

        }
        [weakself dismissViewControllerAnimated:YES completion:^{
    }];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [_topControlButtonContainer addSubview:_exitButton];
    
    // album picker
    _albumPickerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _albumPickerButton.frame = CGRectMake(0, 0, 50, 50);
    _albumPickerButton.center = CGPointMake(SCREEN_WIDTH/5, bottomControlButtonContainerCenterY);
    [_albumPickerButton setBackgroundImage:[UIImage imageNamed:@"camera_gally"] forState:UIControlStateNormal];
    [_albumPickerButton setBackgroundImage:[UIImage imageNamed:@"camera_gally_down"] forState:UIControlStateHighlighted];
    
    [_albumPickerButton bk_addEventHandler:^(id sender) {
        
        TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
        photoPicker.cropBlock = ^(UIImage *croppedImage) {
            
            FilterViewController* filterVC = [[FilterViewController alloc] init];
            filterVC.srcImage = croppedImage;
            [weakself.navigationController pushViewController:filterVC animated:YES];
            filterVC=nil;
        };
        
        UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
        [navCon setNavigationBarHidden:YES];
        
        [weakself presentViewController:navCon animated:YES completion:NULL];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [_bottomControlButtonContainer addSubview:_albumPickerButton];
    
    // take picture
    
    float takePicBtnWidth = 120.0f;
    float centerOffset =  0.0f;
    if(IS_IPHONE_4){
        takePicBtnWidth = 60.0f;
        centerOffset =10.0f;
    }
    
    _takePictureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _takePictureButton.frame = CGRectMake(0, 0, takePicBtnWidth, takePicBtnWidth);
    _takePictureButton.center = CGPointMake(SCREEN_WIDTH/2, bottomControlButtonContainerCenterY+centerOffset);
    
    [_takePictureButton setImage:[UIImage imageNamed:@"camera_btn"]  forState:UIControlStateNormal];
    [_takePictureButton setImage:[UIImage imageNamed:@"camera_btn_down"]  forState:UIControlStateHighlighted];
    [[_takePictureButton imageView] setContentMode: UIViewContentModeScaleAspectFill];
    [_takePictureButton imageView].layer.masksToBounds  = NO;
    _takePictureButton.contentHorizontalAlignment = UIControlContentVerticalAlignmentFill;
    
    [_takePictureButton bk_addEventHandler:^(id sender) {
       
        if(weakself.currentCaptureMode==CAPTURE_MODE_VIDEO || weakself.isChanging) {
            weakself.isDeletingMovieSegment = NO;
            return ;
        }
       
        
        
        
        [weakself.takePictureButton setEnabled:NO];
        // show capture image animation
        [weakself showSutterAnimation];
        [weakself removeShutterAnimation];
        
        AVCaptureConnection *videoConnection = [weakself findVideoConnection];

        [weakself.stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
            
            NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
            UIImage *image = [[UIImage alloc] initWithData:imageData];

            if([GET_DEFAULT(SAVE_PHOTO) isEqual:@1]){
                dispatch_async(GLOBAL_QUEUE, ^{
                    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
                });
            };
            
            // Crop Image to Square
            image = [UIImage getCenterMaxSquareImageByCroppingImage:image withOrientation:image.imageOrientation];

            // Scale Image to 1080*1080
            image = [UIImage scaleImage:image toSize:CGSizeMake(1080, 1080)];
           // image = [UIImage scaleImage:image toSize:CGSizeMake(480, 480)];

            FilterViewController* filterVC = [[FilterViewController alloc] init];
            filterVC.srcImage = image;
            [weakself.navigationController pushViewController:filterVC animated:YES];
            filterVC=nil;
            image=nil;
            imageData=nil;
        }];
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    UILongPressGestureRecognizer* longPressRecog = [[UILongPressGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        
        if(state==UIGestureRecognizerStateBegan) {
            if(weakself.currentMovieDuration>=MOVIE_MAX_RECORD_TIME) {
                return ;
            }
            
            weakself.isDeletingMovieSegment = NO;
            weakself.recordBeginTime = CACurrentMediaTime();
            weakself.recordEndTime = 0.0;
            weakself.isRecordingVideo = YES;
            weakself.currentSegmentVideoFrameCount = 0;
            weakself.currentSegmentAudioFrameCount = 0;
            weakself.audioSampleLaggingCount = 0;
            weakself.videoSampleLaggingCount = 0;
            
            [weakself reloadUI];
            
        } else if(state==UIGestureRecognizerStateEnded) {
           
            weakself.recordEndTime = CACurrentMediaTime();
            
            [weakself stopMovieRecording];
        }
    }];
    
    [_takePictureButton addGestureRecognizer:longPressRecog];
    
    [_bottomControlButtonContainer addSubview:_takePictureButton];

    // capture mode switch btns
    _photoModeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _photoModeBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(16);
    [_photoModeBtn setSelected:YES];
    [_photoModeBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    [_photoModeBtn setTitleColor:WHITE_COLOR forState:UIControlStateSelected];
    [_photoModeBtn setTitle:LOCALIZE(@"PHOTO") forState:UIControlStateNormal];
    _photoModeBtn.frame = CGRectMake(0, 0, modeBtnWidth, modeBtnHeight);
    
    
    [_photoModeBtn bk_addEventHandler:^(id sender) {
       
        if(weakself.currentCaptureMode==CAPTURE_MODE_PHOTO || weakself.isChanging) {
            return ;
        }
        weakself.currentCaptureMode = CAPTURE_MODE_PHOTO;
        [weakself reloadUIAnimated:YES];
        [weakself reloadCaptureSession];
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    _videoModeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _videoModeBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(16);
    [_videoModeBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    [_videoModeBtn setTitleColor:WHITE_COLOR forState:UIControlStateSelected];
    [_videoModeBtn setTitle:LOCALIZE(@"VIDEO") forState:UIControlStateNormal];
    _videoModeBtn.frame = CGRectMake(modeBtnWidth, 0, modeBtnWidth, modeBtnHeight);
    [_videoModeBtn bk_addEventHandler:^(id sender) {
        
        if(weakself.currentCaptureMode==CAPTURE_MODE_VIDEO || weakself.isChanging) {
            return ;
        }
        weakself.currentCaptureMode = CAPTURE_MODE_VIDEO;
        
        [weakself reloadUIAnimated:YES];
        [weakself reloadCaptureSession];
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    _liveModeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _liveModeBtn.titleLabel.font = BOLD_FONT_WITH_SIZE(16);
    [_liveModeBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    [_liveModeBtn setTitleColor:WHITE_COLOR forState:UIControlStateSelected];
    [_liveModeBtn setTitle:LOCALIZE(@"LIVE_STREAM") forState:UIControlStateNormal];
    _liveModeBtn.frame = CGRectMake(2*modeBtnWidth, 0, modeBtnWidth, modeBtnHeight);
    [_liveModeBtn bk_addEventHandler:^(id sender) {
        
        if(weakself.currentCaptureMode==CAPTURE_MODE_LIVESTREAM || weakself.isChanging) {
            return ;
        }
        weakself.currentCaptureMode = CAPTURE_MODE_LIVESTREAM;
        
        [weakself reloadUIAnimated:YES];

    } forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView* spotImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_WIDTH+44+25+2, 8, 8)];
    spotImageView.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_WIDTH+44+30+5);
    [spotImageView setImage:[UIImage imageNamed:@"badge_white"]];
//    [self.view addSubview:spotImageView];
    
    int modeNum = [SINGLETON shouldOpenLivestream]?numOfMode:numOfMode-1;
    
    _modeSlideView = [[UIView alloc]initWithFrame: CGRectMake(0, SCREEN_WIDTH+44, modeBtnWidth*modeNum, modeBtnHeight)];
//    [_modeSlideView addSubview:_photoModeBtn];
//    [_modeSlideView addSubview:_videoModeBtn];
    if([SINGLETON shouldOpenLivestream]){
//        [_modeSlideView addSubview:_liveModeBtn];
    }
    _modeSlideView.center = CGPointMake(SCREEN_WIDTH/2+modeBtnWidth/2*(modeNum-1), _modeSlideView.center.y);
//    [self.view addSubview:_modeSlideView];
    
    float videoButtonWidth = 80.0f;
    if(IS_IPHONE_4){
        videoButtonWidth = 60.0f;
    }
    
    // delete video segment
    _deleteVideoSegmentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _deleteVideoSegmentBtn.frame = CGRectMake(0, 0, videoButtonWidth, videoButtonWidth);
    _deleteVideoSegmentBtn.center = CGPointMake(SCREEN_WIDTH/5-4, bottomControlButtonContainerCenterY);
    [_deleteVideoSegmentBtn setBackgroundImage:[UIImage imageNamed:@"recording_delete"] forState:UIControlStateNormal];
    [_deleteVideoSegmentBtn setBackgroundImage:[UIImage imageNamed:@"recording_delete_down"] forState:UIControlStateHighlighted];

    [_deleteVideoSegmentBtn bk_addEventHandler:^(id sender) {
        if(weakself.movieSegmentCount==0) {
            return ;
        }
        
        if(weakself.isDeletingMovieSegment==NO) {
            weakself.isDeletingMovieSegment = YES;
            [weakself reloadUI];
            return;
        }

        weakself.movieSegmentCount--;

        int audioSampleCountToRemove = [[weakself.audioDataSampleCountArray lastObject] intValue];
        
        [weakself.audioDataCache removeObjectsInRange:NSMakeRange(weakself.audioDataCache.count-audioSampleCountToRemove, audioSampleCountToRemove)];
        weakself.totalAudioFrameCount -= audioSampleCountToRemove;
        
        [weakself.audioDataCache removeLastObject];
        
        int videoSampleCountToRemove = [[weakself.videoDataSampleCountArray lastObject] intValue];
        
        weakself.totalVideoFrameCount -= videoSampleCountToRemove;
        weakself.currentMovieDuration -= [[weakself.movieSegmentDurationArray lastObject] floatValue];
        
        [weakself.videoDataSampleCountArray removeLastObject];
        [weakself.movieSegmentDurationArray removeLastObject];
        
        weakself.isDeletingMovieSegment = NO;
        [weakself reloadUI];

    } forControlEvents:UIControlEventTouchUpInside];
    [_bottomControlButtonContainer addSubview:_deleteVideoSegmentBtn];
    
    // done video segment
    
    _videoDoneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _videoDoneBtn.frame = CGRectMake(0, 0, videoButtonWidth, videoButtonWidth);
    _videoDoneBtn.center = CGPointMake(SCREEN_WIDTH/5*4+4, bottomControlButtonContainerCenterY);
    [_videoDoneBtn setBackgroundImage:[UIImage imageNamed:@"recording_ok"] forState:UIControlStateNormal];
    [_videoDoneBtn setBackgroundImage:[UIImage imageNamed:@"recording_ok_down"] forState:UIControlStateHighlighted];
    
    [_videoDoneBtn bk_addEventHandler:^(id sender) {
    
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];

        PublishPostViewController* publishVC = [PublishPostViewController new];
        publishVC.postimg = weakself.coverImage;
        publishVC.audioDataCache = weakself.audioDataCache ;
        publishVC.totalVideoFrameCount = weakself.totalVideoFrameCount ;
        publishVC.videoUUID = weakself.videoUUID;
        [weakself.navigationController pushViewController:publishVC animated:YES];
    
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    [_bottomControlButtonContainer addSubview:_videoDoneBtn];

    // movie progress bar
    _videoSegmentContainer = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_WIDTH+44.0-MOVIE_PROGRESS_BAR_HEIGHT, SCREEN_WIDTH, MOVIE_PROGRESS_BAR_HEIGHT)];
    [self.view addSubview:_videoSegmentContainer];
    
    _upperShutterImage = [[UIView alloc] initWithFrame:CGRectMake(0, -cameraHeight/2, SCREEN_WIDTH, (cameraHeight)/2)];
    _upperShutterImage.backgroundColor = BLACK_COLOR;

    _lowerShutterImage = [[UIView alloc] initWithFrame:CGRectMake(0, cameraHeight+cameraHeight/2, SCREEN_WIDTH, cameraHeight/2)];
    _lowerShutterImage.backgroundColor = BLACK_COLOR;
    
    [self reloadUI];
}

-(void)showSutterAnimation
{
    [_cameraShtterView addSubview:_upperShutterImage];
    [_cameraShtterView addSubview:_lowerShutterImage];
    
    [UIView animateWithDuration:0.3 animations:^{
        _upperShutterImage.center = CGPointMake(SCREEN_WIDTH/2, cameraHeight/4);
        _lowerShutterImage.center = CGPointMake(SCREEN_WIDTH/2, cameraHeight/4*3);
    }];
}

-(void)removeShutterAnimation
{
    
    [UIView animateWithDuration:0.3 delay:0.3 options:UIViewAnimationOptionCurveLinear animations:^{
        _upperShutterImage.center = CGPointMake(SCREEN_WIDTH/2, -cameraHeight/2);
        _lowerShutterImage.center = CGPointMake(SCREEN_WIDTH/2, cameraHeight+cameraHeight/2);
    } completion:^(BOOL finished) {
        _upperShutterImage.frame = CGRectMake(0, -cameraHeight/2, SCREEN_WIDTH, (cameraHeight)/2);
        _lowerShutterImage.frame = CGRectMake(0, cameraHeight+cameraHeight/2, SCREEN_WIDTH, cameraHeight/2);
        [_upperShutterImage removeFromSuperview];
        [_lowerShutterImage removeFromSuperview];
    }];
    
}

-(void) checkMovieRecordingLength
{
    float recordEndTime = CACurrentMediaTime();
    float newMovieSegmentDuration = recordEndTime - _recordBeginTime;
    float totalDuration = _currentMovieDuration+newMovieSegmentDuration;
    
    if(totalDuration>=MOVIE_MAX_RECORD_TIME) {
        [self stopMovieRecording];
    }
}

-(void) stopMovieRecording
{
    if(_isRecordingVideo==NO) {
        return;
    }
    
    /* TRICKY: Sync Audio to Video */
//    float videoFrameTime = 1.0/24.0;
//    float audioFrameTime = 1.0/(44100.0/1024.0);
    
//    float videoTime = _totalVideoFrameCount*videoFrameTime;
//    float audioTime = _totalAudioFrameCount*audioFrameTime;
//    float timeDiff = fabsf(videoTime-audioTime);
    
    /* Update Stats */
    float newMovieSegmentDuration;

    if(_recordEndTime!=0.0) {
        newMovieSegmentDuration = _recordEndTime - _recordBeginTime;
    } else {
        float recordEndTime = CACurrentMediaTime();
        newMovieSegmentDuration = recordEndTime - _recordBeginTime;
    }
    
    _recordEndTime = 0.0;
    
    [_movieSegmentDurationArray addObject:[NSNumber numberWithFloat:newMovieSegmentDuration]];
    [_videoDataSampleCountArray addObject:[NSNumber numberWithInt:_currentSegmentVideoFrameCount+_videoSampleLaggingCount]];
    [_audioDataSampleCountArray addObject:[NSNumber numberWithInt:_currentSegmentAudioFrameCount+_audioSampleLaggingCount]];
    
    
    _totalVideoFrameCount += (_currentSegmentVideoFrameCount+_videoSampleLaggingCount);
    _totalAudioFrameCount += (_currentSegmentAudioFrameCount+_audioSampleLaggingCount);
    _currentMovieDuration += newMovieSegmentDuration;
    _movieSegmentCount++;
    _isRecordingVideo = NO;
    
}

-(void) resetVideoRecording
{
    
    /* Retain circle this part */

    dispatch_async(GLOBAL_QUEUE, ^{
        for (int i=1;i<=30*15;i++) {
            NSString* movieFrameFileName = [NSString stringWithFormat:@"%@_frame_%d",_videoUUID, i];
            [FILE_MANAGER removeItemAtPath:GET_LOCAL_FILE_PATH(movieFrameFileName) error:nil];
        }
    });
    
    _currentMovieDuration = 0.0;
    _movieSegmentCount = 0;
    _isRecordingVideo = NO;
    _totalVideoFrameCount = 0;
    _totalAudioFrameCount = 0;
    _currentSegmentVideoFrameCount = 0;
    _currentSegmentAudioFrameCount = 0;
    _recordEndTime = 0.0;
    _recordBeginTime = 0.0;
    _audioSampleLaggingCount = 0;
    _videoSampleLaggingCount = 0;
    _isDeletingMovieSegment = NO;
    
    _audioDataCache = CREATE_MUTABLE_ARRAY;
    _audioDataSampleCountArray = CREATE_MUTABLE_ARRAY;
    _videoDataSampleCountArray = CREATE_MUTABLE_ARRAY;
    _movieSegmentDurationArray = CREATE_MUTABLE_ARRAY;
    
    
    /* NSTimer not release cause vc not dealloc */
    dispatch_async(MAIN_QUEUE, ^{
        
        if(_movieRecordUIUpdateTimer!=nil) {
            [_movieRecordUIUpdateTimer invalidate];
            _movieRecordUIUpdateTimer = nil;
        }
        
        _movieRecordUIUpdateTimer = [NSTimer timerWithTimeInterval:0.05 target:self selector:@selector(reloadUI) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_movieRecordUIUpdateTimer forMode:NSRunLoopCommonModes];
        
        [self startPcmRecorder];
    });
    
}

-(void) startPcmRecorder
{
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryRecord error:nil];
    [self stopPcmRecorder];
    
    _pcmAudioRecorder = [[PCMAudioRecorder alloc] initWithMode:PCM_RECORDER_MODE_MP4];
    _pcmAudioRecorder.delegate = self;
    [_pcmAudioRecorder startRecording];
}

-(void) stopPcmRecorder
{
    if(_pcmAudioRecorder!=nil) {
        [_pcmAudioRecorder stopRecording];
        [_pcmAudioRecorder cleanUp];
        _pcmAudioRecorder = nil;
    }
}

-(void) reloadCaptureSession
{
    // close shutter
    _isChanging = true;
    
    [_cameraShtterView addSubview:_upperShutterImage];
    [_cameraShtterView addSubview:_lowerShutterImage];
    
    _upperShutterImage.center = CGPointMake(SCREEN_WIDTH/2, cameraHeight/4);
    _lowerShutterImage.center = CGPointMake(SCREEN_WIDTH/2, cameraHeight/4*3);
    
    [UIView animateWithDuration:0.15 animations:^{
        _upperShutterImage.center = CGPointMake(SCREEN_WIDTH/2, cameraHeight/4);
        _lowerShutterImage.center = CGPointMake(SCREEN_WIDTH/2, cameraHeight/4*3);
    } completion:^(BOOL finished) {
        if(_cameraPreviewView!=nil) {
            [_cameraPreviewView removeFromSuperview];
            _cameraPreviewView = nil;
        }
    }];
    
    // session setup
    dispatch_async(GLOBAL_QUEUE, ^{
        // clean up old capture session
        for(AVCaptureInput* input in _captureSession.inputs) {
            [_captureSession removeInput:input];
        }
        
        for(AVCaptureOutput* output in _captureSession.outputs) {
            [_captureSession removeOutput:output];
        }

        if([_captureSession isRunning]) {
            [_captureSession stopRunning];
        }
        
        _captureSession = [[AVCaptureSession alloc] init];
        
        if(_currentCaptureMode==CAPTURE_MODE_PHOTO) {
            
            [_captureSession setSessionPreset:AVCaptureSessionPresetPhoto];

            [self setupCamera];

            _stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
            NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
            _stillImageOutput.outputSettings = outputSettings;
            [_captureSession addOutput:_stillImageOutput];
            [_captureSession startRunning];

            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                // MUST STOP PCM RECORDER!
                [self stopPcmRecorder];

                [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
            });
            
        } else if(_currentCaptureMode==CAPTURE_MODE_VIDEO) {
            
            [_captureSession setSessionPreset:AVCaptureSessionPreset640x480];
            
            [self setupCamera];
            
            _videoDataOutput = [[AVCaptureVideoDataOutput alloc] init];
            _videoDataOutput.videoSettings=[NSDictionary dictionaryWithObject: [NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange] forKey: (id)kCVPixelBufferPixelFormatTypeKey];
            [_videoDataOutput setSampleBufferDelegate:self queue:_videoQueue];
            [_captureSession addOutput:_videoDataOutput];
            [_captureSession startRunning];

//            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryRecord error:nil];
//             //MUST RESET VIDEO RECORDING: new mp4 recorder and pcmRecorder
//            [self resetVideoRecording];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryRecord error:nil];
                // MUST RESET VIDEO RECORDING: new mp4 recorder and pcmRecorder
                [self resetVideoRecording];
            });
        }
        
        
        // set frame rate
        //    [_captureDevice lockForConfiguration:NULL];
        //    _captureDevice.activeVideoMaxFrameDuration = CMTimeMake(1, MOVIE_FRAME_RATE);
        //    _captureDevice.activeVideoMinFrameDuration = CMTimeMake(1, MOVIE_FRAME_RATE);
        //    [_captureDevice unlockForConfiguration];
        
        dispatch_async(MAIN_QUEUE, ^{
            
            // setup camera preview layer
            
            _cameraPreviewView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
            [self.view addSubview:_cameraPreviewView];
            
            _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
            _previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            
            float previewHeight = SCREEN_WIDTH*4.0/3.0;
            _previewLayer.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            _cameraPreviewView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            
            _cameraPreviewView.frame = CGRectMake(0, TOP_CONTROL_PANEL_HEIGHT-(previewHeight-SCREEN_WIDTH)/2, SCREEN_WIDTH, previewHeight);
            _previewLayer.frame = CGRectMake(0, 0, SCREEN_WIDTH, previewHeight);
            [_cameraPreviewView.layer addSublayer:_previewLayer];
            [self.view sendSubviewToBack:_cameraPreviewView];
            
            // focus
            _focusIndicatorView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"camera_focuse"]];
            _focusIndicatorView.frame = CGRectMake(0, 0, 100, 100);
            [_cameraPreviewView addSubview:_focusIndicatorView];
            
            _focusIndicatorView.alpha = 0.0;
            
            __weak CameraViewController* weakself = self;
            
            UITapGestureRecognizer* tapRecog = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
                weakself.focusIndicatorView.center = location;
                weakself.focusIndicatorView.alpha = 1.0;
                weakself.focusIndicatorView.transform = CGAffineTransformMakeScale(2.0, 2.0);
                
                [UIView animateWithDuration:0.3 animations:^{
                    weakself.focusIndicatorView.alpha = 0.0;
                    weakself.focusIndicatorView.transform = CGAffineTransformMakeScale(1.0, 1.0);
                }];
                
                // camera focus adjust
                AVCaptureDevice *device = [weakself.inputDevice device];
                if ([device lockForConfiguration:nil]) {
                    if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus])
                    {
                        [device setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
                        [device setFocusPointOfInterest:CGPointMake(location.x/weakself.cameraPreviewView.frame.size.width, location.y/weakself.cameraPreviewView.frame.size.height)];
                    }
                    if ([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure])
                    {
                        [device setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
                        [device setExposurePointOfInterest:CGPointMake(location.x/weakself.cameraPreviewView.frame.size.width, location.y/weakself.cameraPreviewView.frame.size.height)];
                    }
                    [device setSubjectAreaChangeMonitoringEnabled:YES];
                    [device unlockForConfiguration];
                }
            }];
            
            for (UIGestureRecognizer *recognizer in _cameraShtterView.gestureRecognizers) {
                [_cameraShtterView removeGestureRecognizer:recognizer];
            }
            [_cameraShtterView addGestureRecognizer:tapRecog];
//            [_cameraShtterView addGestureRecognizer:[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panGesture:)]];

            
            /* Top View */
            _topView = [[UIView alloc]initWithFrame:CGRectMake(0, NAVI_BAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-_bottomControlButtonContainer.frame.size.height+10)];
            _topView.hidden = YES;
            [_topView setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.3]];
            _topView.layer.masksToBounds = YES;
 

            UIView* blackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT)];
            [blackView setBackgroundColor:BLACK_COLOR];
            
            int nextY = 100;
            
            _warningLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 44)];
            _warningLabel.font = BOLD_FONT_WITH_SIZE(20);
            _warningLabel.textAlignment = NSTextAlignmentCenter;
            _warningLabel.text = LOCALIZE(@"take_photo_with_17");
            [_warningLabel setTextColor:WHITE_COLOR];
            
            nextY += _warningLabel.frame.size.height+3;
            
            _descriptLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, nextY, SCREEN_WIDTH-60, 40)];
            _descriptLabel.font = SYSTEM_FONT_WITH_SIZE(14);
            _descriptLabel.text = LOCALIZE(@"camera_not_access_description");
            [_descriptLabel sizeToFit];
            _descriptLabel.center=CGPointMake(SCREEN_WIDTH/2, nextY+44);
            _descriptLabel.textAlignment = NSTextAlignmentCenter;
            _descriptLabel.numberOfLines = 0;
            [_descriptLabel setTextColor:LIGHT_GRAY_COLOR];
            
            _linkLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 30)];
            _linkLabel.textAlignment = NSTextAlignmentCenter;
            _linkLabel.font = SYSTEM_FONT_WITH_SIZE(16);
            _linkLabel.text = LOCALIZE(@"link_to_camera");
            [_linkLabel setTextColor:[[UIColor alloc] initWithRed:129.0/255.0 green:171.0/255.0 blue:193.0/255.0 alpha:1.0]];
            _linkLabel.userInteractionEnabled = YES;
            
            [_linkLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender,
                                                                                                  UIGestureRecognizerState state, CGPoint location) {
                [weakself.navigationController dismissViewControllerAnimated:NO completion:nil];
                [SINGLETON gotoSystemSettings];
                
            }]];
            
            [blackView addSubview:_warningLabel];
            [blackView addSubview:_descriptLabel];
            [blackView addSubview:_linkLabel];
            [_topView addSubview:blackView];
            
            [self.view addSubview:_topView];
            
            
            __block  BOOL micGrant;
            micGrant=YES;
            if([[AVAudioSession sharedInstance] respondsToSelector:@selector(requestRecordPermission:)]) {
                [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
                    if (!granted) {
                        _linkLabel.text = LOCALIZE(@"link_to_mic");
                        _descriptLabel.text = LOCALIZE(@"mic_not_access_description");
                        _warningLabel.text = LOCALIZE(@"take_video_with_17");
                        [_descriptLabel sizeToFit];
                        _topView.hidden = NO;
                        [_topView bringSubviewToFront:self.view];
                        [_takePictureButton setEnabled:NO];
                        [_captureSession stopRunning];
                        micGrant=NO;
                    }else{
                        micGrant=YES;
                    }
                }];
            }
            
            if (micGrant) {
                
                if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                        
                        if (granted) {
                            // We are on iOS <= 7,8. Just do what we need to do.
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                _topView.hidden = YES;
                                [_takePictureButton setEnabled:YES];
                            });
                        } else {
                            // Deny
                            _topView.hidden = NO;
                            [_topView bringSubviewToFront:self.view];
                            [_takePictureButton setEnabled:NO];
                        }
                    }];
                } else {
                    // We are on iOS <= 6. Just do what we need to do.
                    _topView.hidden = YES;
                    [_takePictureButton setEnabled:YES];
                }
            }
            
            
            
            // close shutter
            _upperShutterImage.center = CGPointMake(SCREEN_WIDTH/2, cameraHeight/4);
            _lowerShutterImage.center = CGPointMake(SCREEN_WIDTH/2, cameraHeight/4*3);
            
            // open shutter
            [UIView animateWithDuration:0.3 animations:^{
                _upperShutterImage.center = CGPointMake(SCREEN_WIDTH/2, -cameraHeight/2);
                _lowerShutterImage.center = CGPointMake(SCREEN_WIDTH/2, cameraHeight+cameraHeight/2);
            } completion:^(BOOL finished) {
                _upperShutterImage.frame = CGRectMake(0, -cameraHeight/2, SCREEN_WIDTH, (cameraHeight)/2);
                _lowerShutterImage.frame = CGRectMake(0, cameraHeight+cameraHeight/2, SCREEN_WIDTH, cameraHeight/2);
                [_upperShutterImage removeFromSuperview];
                [_lowerShutterImage removeFromSuperview];
            }];
        });
        
        
        
        
        
        
        _isChanging = false;
    });
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _enterPageTimeStamp=CURRENT_TIMESTAMP;
    [NOTIFICATION_CENTER addObserver:self selector:@selector(enterBackground:) name:UIApplicationWillResignActiveNotification object:nil];
    [NOTIFICATION_CENTER addObserver:self selector:@selector(enterForeground:) name:UIApplicationDidBecomeActiveNotification object:nil];
    if (_currentCaptureMode==CAPTURE_MODE_PHOTO) {
        [EVENT_HANDLER addEventTracking:@"EnterPublishPostPage" withDict:@{@"type":@"photo"}];
    }else{
        [EVENT_HANDLER addEventTracking:@"EnterPublishPostPage" withDict:@{@"type":@"video"}];
    }
    [_takePictureButton setEnabled:YES];

    if(_isFirstPresent){
        _isFirstPresent = NO;
        /* Thesee two add retain count to 15 */
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
    }
    

    
//    /* These line add retain count 2 */
    [self reloadCaptureSession];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];

    [NOTIFICATION_CENTER removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [NOTIFICATION_CENTER removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    
    if([_captureSession isRunning]) {
        [_captureSession stopRunning];
    }

    [self stopPcmRecorder];
    
    if(_cameraPreviewView!=nil) {
        [_cameraPreviewView removeFromSuperview];
        _cameraPreviewView = nil;
    }
    
    /* Must release timer  */
    if(_movieRecordUIUpdateTimer!=nil) {
        [_movieRecordUIUpdateTimer invalidate];
        _movieRecordUIUpdateTimer = nil;
    }
    
    // show shutter
    _upperShutterImage.frame = CGRectMake(0, -cameraHeight/2, SCREEN_WIDTH, (cameraHeight)/2);
    _lowerShutterImage.frame = CGRectMake(0, cameraHeight+cameraHeight/2, SCREEN_WIDTH, cameraHeight/2);
    
    DLog(@"viewWillDisappear Retain count is %ld", RETAIN_COUNT(self) );
    
}

-(void) reloadUI
{
//    DLog(@"Camera reloadUI");
    
    _reloadUICount++;
    
    if([[DEFAULTS objectForKey:CAMERA_GRID_MODE] isEqualToString:CAMERA_GRID_MODE_ON]) {
        
        [_gridToggleButton setBackgroundImage:[UIImage imageNamed:@"camera_gridtoggle_down"] forState:UIControlStateNormal];
        [_gridToggleButton setBackgroundImage:[UIImage imageNamed:@"camera_gridtoggle_down"] forState:UIControlStateHighlighted];
        _gridImageView.hidden = NO;
        
    } else {
        
        [_gridToggleButton setBackgroundImage:[UIImage imageNamed:@"camera_gridtoggle"] forState:UIControlStateNormal];
        [_gridToggleButton setBackgroundImage:[UIImage imageNamed:@"camera_gridtoggle"] forState:UIControlStateHighlighted];
        _gridImageView.hidden = YES;
        
    }
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    if ([device hasFlash]) {
        _flashModeSwitchButton.hidden = NO;
        
        if([[DEFAULTS objectForKey:CAMERA_FLASH_MODE] isEqualToString:CAMERA_FLASH_MODE_AUTO]) {
            [_flashModeSwitchButton setBackgroundImage:[UIImage imageNamed:@"camera_flash_auto_active"] forState:UIControlStateNormal];
            [_flashModeSwitchButton setBackgroundImage:[UIImage imageNamed:@"camera_flash_auto_active"] forState:UIControlStateHighlighted];
        } else if([[DEFAULTS objectForKey:CAMERA_FLASH_MODE] isEqualToString:CAMERA_FLASH_MODE_OFF]) {
            [_flashModeSwitchButton setBackgroundImage:[UIImage imageNamed:@"camera_flash_off"] forState:UIControlStateNormal];
            [_flashModeSwitchButton setBackgroundImage:[UIImage imageNamed:@"camera_flash_off"] forState:UIControlStateHighlighted];
        } else if([[DEFAULTS objectForKey:CAMERA_FLASH_MODE] isEqualToString:CAMERA_FLASH_MODE_ON]) {
            [_flashModeSwitchButton setBackgroundImage:[UIImage imageNamed:@"camera_flash_on_active"] forState:UIControlStateNormal];
            [_flashModeSwitchButton setBackgroundImage:[UIImage imageNamed:@"camera_flash_on_active"] forState:UIControlStateHighlighted];
        }
    } else {
        _flashModeSwitchButton.hidden = YES;
    }

    if (_currentCaptureMode==CAPTURE_MODE_VIDEO) {
        _flashModeSwitchButton.hidden=YES;
    }
    dispatch_async(GLOBAL_QUEUE, ^{
        [device lockForConfiguration:nil];
        
        if ([device hasFlash]) {
            if([[DEFAULTS objectForKey:CAMERA_FLASH_MODE] isEqualToString:CAMERA_FLASH_MODE_AUTO]) {
                device.flashMode = AVCaptureFlashModeAuto;
            } else if([[DEFAULTS objectForKey:CAMERA_FLASH_MODE] isEqualToString:CAMERA_FLASH_MODE_OFF]) {
                device.flashMode = AVCaptureFlashModeOff;
            } else if([[DEFAULTS objectForKey:CAMERA_FLASH_MODE] isEqualToString:CAMERA_FLASH_MODE_ON]) {
                device.flashMode = AVCaptureFlashModeOn;
            }
        }
        
        [device unlockForConfiguration];
    });
    
    _deleteVideoSegmentBtn.hidden = YES;
    _videoDoneBtn.hidden = YES;
    _albumPickerButton.hidden = YES;
    
    if(_currentCaptureMode==CAPTURE_MODE_PHOTO) {
        _albumPickerButton.hidden = NO;
    } else if(_currentCaptureMode==CAPTURE_MODE_VIDEO) {
        if(_movieSegmentCount>0 || _recordEndTime>0.0) {
            _deleteVideoSegmentBtn.hidden = NO;
        }
        
        if(_recordEndTime>0.0) {
            if(_currentMovieDuration+(_recordEndTime-_recordBeginTime)>=MOVIE_MIN_RECORD_TIME) {
                _videoDoneBtn.hidden = NO;
            }
        } else {
            if(_currentMovieDuration>=MOVIE_MIN_RECORD_TIME) {
                _videoDoneBtn.hidden = NO;
            }
        }
        
        if(_isDeletingMovieSegment) {
            [_deleteVideoSegmentBtn setBackgroundImage:[UIImage imageNamed:@"recording_delete_down"] forState:UIControlStateNormal];
            [_deleteVideoSegmentBtn setBackgroundImage:[UIImage imageNamed:@"recording_delete_down"] forState:UIControlStateHighlighted];
        } else {
            [_deleteVideoSegmentBtn setBackgroundImage:[UIImage imageNamed:@"recording_delete"] forState:UIControlStateNormal];
            [_deleteVideoSegmentBtn setBackgroundImage:[UIImage imageNamed:@"recording_delete"] forState:UIControlStateHighlighted];
        }
        
        /* Movie Recording Segment Bar */
        NSMutableArray* videoLengthSegments = [NSMutableArray arrayWithArray:_movieSegmentDurationArray];
        
        if(_isRecordingVideo) {
            if(_recordEndTime==0.0) {
                [videoLengthSegments addObject:[NSNumber numberWithFloat:CACurrentMediaTime()-_recordBeginTime]];
            } else {
                [videoLengthSegments addObject:[NSNumber numberWithFloat:_recordEndTime-_recordBeginTime]];
            }
        }
        
        for (UIView* v in _videoSegmentContainer.subviews) {
            [v removeFromSuperview];
        }
        
        _videoSegmentContainer.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.2];
        
        UIView* minDurationBar = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*MOVIE_MIN_RECORD_TIME/MOVIE_MAX_RECORD_TIME, 0, 2.0, MOVIE_PROGRESS_BAR_HEIGHT)];
        minDurationBar.backgroundColor = RED_COLOR;
        [_videoSegmentContainer addSubview:minDurationBar];
        
        float accuUILength = 0.0;
        int lastSegmentIndex = (int) videoLengthSegments.count-1;
        int currentSegmentIndex = 0;
        
        for(NSNumber* len in videoLengthSegments) {
            float length = [len floatValue];
            float uiLength = SCREEN_WIDTH*length/MOVIE_MAX_RECORD_TIME;
            
            UIView* bar = [[UIView alloc] initWithFrame:CGRectMake(accuUILength, 0, uiLength, MOVIE_PROGRESS_BAR_HEIGHT)];
            if(currentSegmentIndex==lastSegmentIndex && _isDeletingMovieSegment==YES) {
                bar.backgroundColor = DARK_GRAY_COLOR;
            } else {
                bar.backgroundColor = WHITE_COLOR;
            }
            
            UIView* whiteBar = [[UIView alloc] initWithFrame:CGRectMake(accuUILength+uiLength-1.0, 0, 1.0, MOVIE_PROGRESS_BAR_HEIGHT)];
            whiteBar.backgroundColor = GRAY_COLOR;
            
            [_videoSegmentContainer addSubview:bar];
            [_videoSegmentContainer addSubview:whiteBar];
            
            accuUILength += uiLength;
            currentSegmentIndex++;
        }
        
        if(_currentMovieDuration<MOVIE_MAX_RECORD_TIME && _reloadUICount%20<10 && _isDeletingMovieSegment==NO) {
            UIView* cursorBar = [[UIView alloc] initWithFrame:CGRectMake(accuUILength, 0, 3.0, MOVIE_PROGRESS_BAR_HEIGHT)];
            cursorBar.backgroundColor = BLACK_COLOR;
            [_videoSegmentContainer addSubview:cursorBar];
        }

    }
}

// capture session management
-(void) setupCamera
{
    if(self.inputDevice) {
        [_captureSession removeInput:self.inputDevice];
    }
    
    NSArray *devices = [AVCaptureDevice devices];
    AVCaptureDevice *frontCamera;
    AVCaptureDevice *backCamera;
    
    for (AVCaptureDevice *device in devices) {
        if ([device hasMediaType:AVMediaTypeVideo]) {
            if ([device position] == AVCaptureDevicePositionBack) {
                backCamera = device;
            }  else {
                frontCamera = device;
            }
        }
    }
    
    NSError *error = nil;
    
    if (_isFrontCamera) {
        AVCaptureDeviceInput *frontFacingCameraDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:frontCamera error:&error];
        if (!error) {
            if ([_captureSession canAddInput:frontFacingCameraDeviceInput]) {
                [_captureSession addInput:frontFacingCameraDeviceInput];
                self.inputDevice = frontFacingCameraDeviceInput;
                _captureDevice = frontCamera;
            }
        }
    } else {
        AVCaptureDeviceInput *backFacingCameraDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:backCamera error:&error];
        if (!error) {
            if ([_captureSession canAddInput:backFacingCameraDeviceInput]) {
                [_captureSession addInput:backFacingCameraDeviceInput];
                self.inputDevice = backFacingCameraDeviceInput;
                _captureDevice = backCamera;
            }
        }
    }
}

- (AVCaptureConnection*)findVideoConnection
{
    AVCaptureConnection *videoConnection = nil;
    
    for (AVCaptureConnection *connection in _stillImageOutput.connections) {
        for (AVCaptureInputPort *port in connection.inputPorts) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo]) {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) {
            break;
        }
    }

    return videoConnection;
}

# pragma mark - AVCaptureDataOutputSampleBufferDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    if(_currentCaptureMode==CAPTURE_MODE_VIDEO) {
        
        
        if(_isRecordingVideo==NO && _videoSampleLaggingCount==0) {
            return;
        }
        
        CVImageBufferRef pixelBuffer = nil;
        unsigned char *planeBaseAddress = nil;
        
        pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
        CVPixelBufferLockBaseAddress(pixelBuffer, 0);
        int planeCount = (int) CVPixelBufferGetPlaneCount(pixelBuffer);
        
        unsigned char * yPlaneBaseAddress;
        unsigned char * uvPlaneBaseAddress;
        
        for (int planeIndex = 0; planeIndex < planeCount; planeIndex++) {
            planeBaseAddress = (unsigned char *)CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, planeIndex);

            if (planeIndex == PLANE_Y) {
                yPlaneBaseAddress = planeBaseAddress;
            } else if (planeIndex == PLANE_CBCR) {
                uvPlaneBaseAddress = planeBaseAddress;
            }
        }
        
        /* Pixel Format Conversion */
        
        // fill y data
        unsigned char* destPtr = _movieFrameDataBuffer;
        unsigned char* srcPtr = yPlaneBaseAddress+80;
        
        for(int y=0;y<480;y++) {
            memcpy(destPtr, srcPtr, 480);
            srcPtr += 640;
            destPtr += 480;
        }
        
        // fill uv data
        unsigned char* cbDestPtr = _movieFrameDataBuffer+MOVIE_WIDTH*MOVIE_HEIGHT;
        unsigned char* uvSrcPtr = uvPlaneBaseAddress+80;
        unsigned char* crDestPtr = cbDestPtr+MOVIE_WIDTH*MOVIE_HEIGHT/4;
        
        for(int y=0;y<240;y++) {
            
            for(int x=0;x<240;x++) {
                
                memcpy(cbDestPtr, uvSrcPtr, 1);
                memcpy(crDestPtr, uvSrcPtr+1, 1);
                
                uvSrcPtr += 2;
                cbDestPtr += 1;
                crDestPtr += 1;
            }
            
            uvSrcPtr += 160;
        }

        CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
        
        if(_isRecordingVideo) {

            dispatch_async(MAIN_QUEUE, ^{
                [self checkMovieRecordingLength];
            });

            dispatch_async(GLOBAL_QUEUE, ^{
                _currentSegmentVideoFrameCount++;
                
                NSString* movieFrameDiskCacheFileName = [NSString stringWithFormat:@"%@_frame_%d",_videoUUID, _totalVideoFrameCount+_currentSegmentVideoFrameCount];

                // save video frame to disk cache
                [[NSData dataWithBytes:_movieFrameDataBuffer length:MOVIE_WIDTH*MOVIE_HEIGHT*3/2] writeToFile:GET_LOCAL_FILE_PATH(movieFrameDiskCacheFileName) atomically:YES];
        
            });
        } else if(_videoSampleLaggingCount>0) {
//            NSString* movieFrameDiskCacheFileName = [NSString stringWithFormat:@"%@_frame_%d",_videoUUID, _totalVideoFrameCount-_videoSampleLaggingCount]; // TRICKY: minus back
//            
//            dispatch_async(GLOBAL_QUEUE, ^{
//                // save video frame to disk cache
//                [[NSData dataWithBytes:_movieFrameDataBuffer length:MOVIE_WIDTH*MOVIE_HEIGHT*3/2] writeToFile:GET_LOCAL_FILE_PATH(movieFrameDiskCacheFileName) atomically:YES];
//            });
            
            _videoSampleLaggingCount--;
            DLog(@"RECORDING LAGGING VIDEO FRAME!");
        }
        
        /* Save Coverphoto  */
        if(_totalVideoFrameCount+_currentSegmentVideoFrameCount==1){
            
            CIImage* ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer options:nil];
            ciImage = [ciImage imageByApplyingTransform:CGAffineTransformMakeRotation(-M_PI_2)];
            ciImage = [ciImage imageByCroppingToRect:CGRectMake(ciImage.extent.origin.x, ciImage.extent.origin.y+(ciImage.extent.size.height-ciImage.extent.size.width)/2.0, ciImage.extent.size.width, ciImage.extent.size.width)];
            CIContext *context = [CIContext contextWithOptions:nil];
            _coverImage = [UIImage imageWithCGImage:[context createCGImage:ciImage fromRect:ciImage.extent]];

          ciImage=nil;
        }
        
    }
}

# pragma mark - AudioRecorderDelegate
-(void)didOutputAudioData:(NSData *)audioData
{
    if(_isRecordingVideo) {
        _currentSegmentAudioFrameCount++;
        [_audioDataCache addObject:audioData];
        [self checkMovieRecordingLength];
    } else if(_audioSampleLaggingCount>0) {
//        [_audioDataCache addObject:audioData];
        _audioSampleLaggingCount--;
        
        DLog(@"RECORDING LAGGING AUDIO FRAME!");
    }
}

// TRICKY: to maintain audio recorder active and working
-(void) enterForegroundHandler
{
    if(_currentCaptureMode==CAPTURE_MODE_VIDEO) {
        if(_pcmAudioRecorder!=nil) {
            // restart pcm recorder
            [self startPcmRecorder];
        }
    }
}

-(void)dealloc
{
    free(_movieFrameDataBuffer);
    [self stopPcmRecorder];
    
    _inputDevice = nil;
    _captureDevice = nil;
    _previewLayer = nil;
    _stillImageOutput = nil;
    _captureSession = nil;
    _previewLayer = nil;
    
    [NOTIFICATION_CENTER removeObserver:self];

}


-(void) reloadUIAnimated:(BOOL)animated
{
    float animationDuration = animated? 0.2f:0.0f;
    
    CGPoint point;

    float modeNum = 2;
    if([SINGLETON shouldOpenLivestream]){
        modeNum =3;
    }
    
    if(_currentCaptureMode==CAPTURE_MODE_PHOTO) {
        
        point = CGPointMake(SCREEN_WIDTH/2+modeBtnWidth/2*(modeNum-1), _modeSlideView.center.y);
        [_photoModeBtn setSelected:YES];
        [_videoModeBtn setSelected:NO];
        [_liveModeBtn setSelected:NO];
        _videoSegmentContainer.hidden = YES;
        
        [_takePictureButton setImage:[UIImage imageNamed:@"camera_btn"]  forState:UIControlStateNormal];
        [_takePictureButton setImage:[UIImage imageNamed:@"camera_btn_down"]  forState:UIControlStateHighlighted];
    } else if(_currentCaptureMode==CAPTURE_MODE_VIDEO) {
        
        if(modeNum==3){
            point = CGPointMake(SCREEN_WIDTH/2, _modeSlideView.center.y);
        }else{
            point = CGPointMake(SCREEN_WIDTH/2-modeBtnWidth/2*(modeNum-1), _modeSlideView.center.y);
        }
        [_photoModeBtn setSelected:NO];
        [_videoModeBtn setSelected:YES];
        [_liveModeBtn setSelected:NO];
        _videoSegmentContainer.hidden = NO;
        
        [_takePictureButton setImage:[UIImage imageNamed:@"recording_btn"]  forState:UIControlStateNormal];
        [_takePictureButton setImage:[UIImage imageNamed:@"recording_btn_down"]  forState:UIControlStateHighlighted];
    } else if(_currentCaptureMode==CAPTURE_MODE_LIVESTREAM) {
        
        point = CGPointMake(SCREEN_WIDTH/2-modeBtnWidth/2*(modeNum-1), _modeSlideView.center.y);
        
        [_photoModeBtn setSelected:NO];
        [_videoModeBtn setSelected:NO];
        [_liveModeBtn setSelected:YES];
        _videoSegmentContainer.hidden = YES;

    }
    
    [UIView animateWithDuration:animationDuration animations:^{
        
        _modeSlideView.center = point;
    
    } completion:^(BOOL finished) {
        if(_currentCaptureMode!=CAPTURE_MODE_LIVESTREAM)
            return;
        
        dispatch_async(GLOBAL_QUEUE, ^{
            [_captureSession stopRunning];

            for(AVCaptureInput* input in _captureSession.inputs) {
                [_captureSession removeInput:input];
            }
            for(AVCaptureOutput* output in _captureSession.outputs) {
                [_captureSession removeOutput:output];
            }
            _captureSession = nil;
            
            dispatch_async(MAIN_QUEUE, ^{

                if(IS_NETWORK_AVAILABLE){
                    
                    [self dismissViewControllerAnimated:YES completion:^{
                        
                        LiveStreamViewController* vc = [SINGLETON liveStreamSharedManager];
                        if (vc.canPresentView) {
                            CustomTabbarController *tabVC = [SINGLETON mainTabBarViewController];
                            
                            vc.liveStreamMode = LiveStreamModeBroadcast;
                            vc.delegate = tabVC;
                            vc.transitioningDelegate = tabVC;
                            vc.modalTransitionStyle = UIModalPresentationCustom;
                            vc.modalPresentationStyle = UIModalPresentationFullScreen;
                            if (tabVC.presentedViewController==nil) {
                                [tabVC presentViewController:vc animated:YES completion:nil];
                            }
                        }
                    }];

                }else{
                    [DIALOG_MANAGER showActionSheetOKDialogTitle:LOCALIZE(@"network_unstable") message:@"" buttonText:LOCALIZE(@"OK") cancelable:NO withCompletion:^(BOOL okClicked) {
                        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                    }];
                }
                

            });
        });
    }];
}


-(void)panGesture:(UIPanGestureRecognizer*)gestureRecognizer
{
    if(_isChanging ) {
        return;
    }
    
    CGPoint velocity = [gestureRecognizer velocityInView:self.view];
    
    
    DLog(@"abs_velocity:%f",ABS(velocity.x));
    DLog(@"transX:%f",velocity.x);
    DLog(@"transY:%f",velocity.y);

    /* block pangesture of people not verified */
    if(_currentCaptureMode == CAPTURE_MODE_VIDEO &&  velocity.x <= 0 && ![SINGLETON shouldOpenLivestream] ) {
        return;
    }
    
    if(ABS(velocity.y)>ABS(velocity.x))
        return;
    
    if(velocity.x > 0) {
        if(_currentCaptureMode==CAPTURE_MODE_PHOTO)
            return;
        if(_currentCaptureMode==CAPTURE_MODE_VIDEO){
            _currentCaptureMode = CAPTURE_MODE_PHOTO;
        }else if(_currentCaptureMode==CAPTURE_MODE_LIVESTREAM){
            _currentCaptureMode = CAPTURE_MODE_VIDEO;
        }
    } else {
        if(_currentCaptureMode==CAPTURE_MODE_LIVESTREAM)
            return;
        if(_currentCaptureMode==CAPTURE_MODE_PHOTO) {
            _currentCaptureMode = CAPTURE_MODE_VIDEO;
        } else if(_currentCaptureMode==CAPTURE_MODE_VIDEO) {
            _currentCaptureMode = CAPTURE_MODE_LIVESTREAM;
        }
    }
    
    [self reloadUIAnimated:YES];
    [self reloadCaptureSession];
}

-(void) enterBackground:(id)sender
{
    if(_currentCaptureMode==CAPTURE_MODE_VIDEO) {
        [self stopPcmRecorder];
    }
}

-(void) enterForeground:(id)sender
{
    if(_currentCaptureMode==CAPTURE_MODE_VIDEO) {
        [self startPcmRecorder];
    }
}

@end
