#import "MyFilter.h"

@implementation MyFilter

@synthesize vertexShader;
@synthesize fragmentShader;
@synthesize glProgram;
@synthesize glProgramsquare;
@synthesize outputTexture;
@synthesize positionAttribute;
@synthesize inputTextureCoordinateAttribute;
@synthesize framebuffer;
@synthesize imageSize;
@synthesize textureMapping;
@synthesize textureCache;
@synthesize glContext;
@synthesize imageOutputDataBuffer;
@synthesize glTextureCacheRef;
@synthesize outputPixelBufferRef;
@synthesize glOutputTextureRef;
@synthesize glViewFrameBuffer;
@synthesize luminanceTextureRef;
@synthesize chrominanceTextureRef;
@synthesize yTexture;
@synthesize uvTexture;
@synthesize targetViewPortSize;
@synthesize renderBuffer;
@synthesize imageOrientation;
@synthesize blurTexture;
@synthesize currentProgramID;

-(instancetype)initWithImageSize:(CGSize) size filterProgram:(int) programID
{
    self = [super init];
    
    if(self) {
    
        textureCache = CREATE_MUTABLE_DICTIONARY;
        textureMapping = CREATE_MUTABLE_DICTIONARY;
        imageSize = size;
        targetViewPortSize = imageSize;
        
        imageOutputDataBuffer = malloc(imageSize.width * imageSize.height * 4);
        
        glContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        [EAGLContext setCurrentContext:glContext];
        
        // generate output texture
        BOOL useFastTextureUpload = YES;

        if(useFastTextureUpload) {
            NSDictionary* attrs = @{(__bridge NSString*) kCVPixelBufferIOSurfacePropertiesKey: @{}};
            
            CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, glContext, NULL, &glTextureCacheRef);
            
            CVPixelBufferCreate(kCFAllocatorDefault, imageSize.width, imageSize.height, kCVPixelFormatType_32BGRA, (__bridge CFDictionaryRef) attrs, &outputPixelBufferRef);
            
            CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault, glTextureCacheRef, outputPixelBufferRef, NULL, GL_TEXTURE_2D, GL_RGBA, imageSize.width, imageSize.height, GL_BGRA, GL_UNSIGNED_BYTE, 0, &glOutputTextureRef);
            outputTexture = CVOpenGLESTextureGetName(glOutputTextureRef);
            
            glBindTexture(GL_TEXTURE_2D, outputTexture);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        } else {
            glGenTextures(1, &outputTexture);
            glBindTexture(GL_TEXTURE_2D, outputTexture);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (int)imageSize.width, (int)imageSize.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        }
        
        // create output frame buffer
        glGenFramebuffers(1, &framebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, outputTexture, 0);
        
        // create render buffer
//        glGenRenderbuffers(1, &renderBuffer);
//        glBindRenderbuffer(GL_RENDERBUFFER, renderBuffer);
//        glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8_OES, (int)imageSize.width, (int)imageSize.height);
//        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, renderBuffer);
        
        // GAUSSIAN BLUR ONLY: need to generate blur
        if(programID==FILTER_PROGRAM_GAUSSIAN_BLUR) {
            glGenTextures(1, &blurTexture);
            glBindTexture(GL_TEXTURE_2D, blurTexture);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (int)imageSize.width, (int)imageSize.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
            
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        }
        
        [self loadProgram:programID];
    }
    
    return self;
}

-(void) loadInputImage:(UIImage*) image textureName:(NSString*) textureName
{
    [EAGLContext setCurrentContext:glContext];

    if(image==nil) {
        return;
    }
    
    GLuint texture = 0;
    
    if([textureMapping objectForKey:textureName]==nil) {
        // generate new texture
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    } else {
        texture = (GLuint) [[textureMapping objectForKey:textureName] intValue];
    }
    
    GLubyte *imageData = (GLubyte *) malloc((int)image.size.width * (int)image.size.height * 4);
    CGColorSpaceRef genericRGBColorspace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef imageContext = CGBitmapContextCreate(imageData, (int)image.size.width, (int)image.size.height, 8, (int)image.size.width * 4, genericRGBColorspace,  kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGContextDrawImage(imageContext, CGRectMake(0.0, 0.0, image.size.width, image.size.height), [image CGImage]);
    CGColorSpaceRelease(genericRGBColorspace);
    CGContextRelease(imageContext);

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (int)image.size.width, (int)image.size.height, 0, GL_BGRA, GL_UNSIGNED_BYTE, imageData);
    
    free(imageData);
    
    [textureMapping setObject:[NSNumber numberWithInt:(int)texture] forKey:textureName];
    
    if([textureName isEqualToString:@"inputImageTexture"]) {
        imageOrientation = image.imageOrientation;
        
        [self resetRotation];
    }
}

-(void) loadYUVData:(CVPixelBufferRef) pixelBuffer
{
    int pixelBufferWidth = (int) CVPixelBufferGetWidth(pixelBuffer);
    int pixelBufferHeight = (int) CVPixelBufferGetHeight(pixelBuffer);
    
    [EAGLContext setCurrentContext:glContext];
    
    CVPixelBufferLockBaseAddress(pixelBuffer, 0);

    glActiveTexture(GL_TEXTURE2);
    CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault, glTextureCacheRef, pixelBuffer, NULL, GL_TEXTURE_2D, GL_LUMINANCE, pixelBufferWidth, pixelBufferHeight, GL_LUMINANCE, GL_UNSIGNED_BYTE, 0, &luminanceTextureRef);
    
    glBindTexture(GL_TEXTURE_2D, CVOpenGLESTextureGetName(luminanceTextureRef));
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    [textureMapping setObject:[NSNumber numberWithInt:CVOpenGLESTextureGetName(luminanceTextureRef)] forKey:@"yTexture"];

    CFRelease(luminanceTextureRef);
    
    glActiveTexture(GL_TEXTURE3);
    CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault, glTextureCacheRef, pixelBuffer, NULL, GL_TEXTURE_2D, GL_LUMINANCE_ALPHA, pixelBufferWidth/2, pixelBufferHeight/2, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, 1, &chrominanceTextureRef);
    
    glBindTexture(GL_TEXTURE_2D, CVOpenGLESTextureGetName(chrominanceTextureRef));
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    [textureMapping setObject:[NSNumber numberWithInt:CVOpenGLESTextureGetName(chrominanceTextureRef)] forKey:@"uvTexture"];

    CFRelease(chrominanceTextureRef);

    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
}

-(void) loadLuminanceTextureData:(NSData*) textureData textureName:(NSString*) textureName textureSize:(CGSize) textureSize
{
    [EAGLContext setCurrentContext:glContext];
    
    GLuint texture = 0;
    
    if([textureMapping objectForKey:textureName]==nil) {
        // generate new texture
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    } else {
        texture = (GLuint) [[textureMapping objectForKey:textureName] intValue];
    }
    
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, (int)textureSize.width, (int)textureSize.height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, textureData.bytes);
    
    [textureMapping setObject:[NSNumber numberWithInt:(int)texture] forKey:textureName];
}

-(void) loadProgram:(int) programID
{
    currentProgramID = programID;
    
    // compile shaders and load glProgram
    glProgram = glCreateProgram();
    glProgramsquare=glCreateProgram();
    if(programID==FILTER_PROGRAM_COMPLETE) {
        vertexShader = glCreateShader(GL_VERTEX_SHADER);
        int vertexShaderStringLength = (int) strlen(vertexShaderString);
        glShaderSource(vertexShader, 1, (const char **)&vertexShaderString, &vertexShaderStringLength);
        glCompileShader(vertexShader);
        
        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        int fragmentShaderStringLength = (int) strlen(fragmentShaderString);
        glShaderSource(fragmentShader, 1, (const char **)&fragmentShaderString, &fragmentShaderStringLength);
        glCompileShader(fragmentShader);
    } else if(programID==FILTER_PROGRAM_GAUSSIAN_BLUR) {
        vertexShader = glCreateShader(GL_VERTEX_SHADER);
        int vertexShaderStringLength = (int) strlen(gaussianBlurVertexShaderString);
        glShaderSource(vertexShader, 1, (const char **)&gaussianBlurVertexShaderString, &vertexShaderStringLength);
        glCompileShader(vertexShader);
        
        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        int fragmentShaderStringLength = (int) strlen(gaussianBlurFragmentShaderString);
        glShaderSource(fragmentShader, 1, (const char **)&gaussianBlurFragmentShaderString, &fragmentShaderStringLength);
        glCompileShader(fragmentShader);
    } else if(programID==FILTER_PROGRAM_SIMPLE||programID==FILTER_PROGRAM_SIMPLE_SQUARE) {
        vertexShader = glCreateShader(GL_VERTEX_SHADER);
        int vertexShaderStringLength = (int) strlen(simpleFilterVertexShaderString);
        glShaderSource(vertexShader, 1, (const char **)&simpleFilterVertexShaderString, &vertexShaderStringLength);
        glCompileShader(vertexShader);
        
        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        int fragmentShaderStringLength = (int) strlen(simpleFilterFragmentShaderString);
        glShaderSource(fragmentShader, 1, (const char **)&simpleFilterFragmentShaderString, &fragmentShaderStringLength);
        glCompileShader(fragmentShader);
    }
    
    glAttachShader(glProgram, vertexShader);
    glAttachShader(glProgram, fragmentShader);
    
    glLinkProgram(glProgram);
    glLinkProgram(glProgramsquare);

    glUseProgram(glProgram);
    
    // enable vertex attribute array
    positionAttribute = glGetAttribLocation(glProgram, "position");
    inputTextureCoordinateAttribute = glGetAttribLocation(glProgram, "inputTextureCoordinate");
    
    glEnableVertexAttribArray(positionAttribute);
    glEnableVertexAttribArray(inputTextureCoordinateAttribute);

    // load shared color map
    if(programID!=FILTER_PROGRAM_GAUSSIAN_BLUR) {
        UIImage* sharedColorMapImage = [UIImage imageNamed:@"shared_color_map"];
        [self loadInputImage:sharedColorMapImage textureName:@"sharedColorMapTexture"];
    }
    
    
//     setup drawing vertex
    const GLfloat squareVertices[] = {
        -1.0f, -1.0f,
        1.0f, -1.0f,
        -1.0f,  1.0f,
        1.0f,  1.0f,
    };
//
//    const GLfloat squareTextureCoordinates[] = {
//        0.0f, 1.0f,
//        0.0f, 0.0f,
//        1.0f, 1.0f,
//        1.0f, 0.0f,
//    };
//    
//    glVertexAttribPointer(positionAttribute, 2, GL_FLOAT, 0, 0, squareVertices);
//    glVertexAttribPointer(inputTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, squareTextureCoordinates);
    
    
    if (programID==FILTER_PROGRAM_SIMPLE_SQUARE) {
        
        if (IS_IPHONE_4) {

            const GLfloat squareTextureCoordinates[] = {
                0.166666672f , 1.0f,
                0.166666672f, 0.0f,
                0.833333313f, 1.0f,
                0.833333313f, 0.0f,
            };
            

            glVertexAttribPointer(positionAttribute, 2, GL_FLOAT, 0, 0, squareVertices);
            glVertexAttribPointer(inputTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, squareTextureCoordinates);

            
        }else {


            const GLfloat squareTextureCoordinates[] = {
             0.218890548f , 1.0f,
             0.218890548f, 0.0f,
             0.781109452f, 1.0f,
             0.781109452f, 0.0f,
        };
        glVertexAttribPointer(positionAttribute, 2, GL_FLOAT, 0, 0, squareVertices);
        glVertexAttribPointer(inputTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, squareTextureCoordinates);
        }
    }else{
        const GLfloat squareTextureCoordinates[] = {
            0.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
        };
        
        glVertexAttribPointer(positionAttribute, 2, GL_FLOAT, 0, 0, squareVertices);
        glVertexAttribPointer(inputTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, squareTextureCoordinates);
    }
//
    

    
    // setup initial params
    [self setRotation:0.0 needHorizontalFlip:0];
    [self resetToolParams];
    [self setParameter:@"pixelStepSize" floatValue:1.0/imageSize.width];
}

-(void) setParameter:(NSString*) parameterName floatValue:(float) value
{
    [EAGLContext setCurrentContext:glContext];

    int floatUniform = glGetUniformLocation(glProgram, [parameterName cStringUsingEncoding:NSUTF8StringEncoding]);
    glUniform1f(floatUniform, value);
}

-(void) setParameter:(NSString*) parameterName intValue:(int) value
{
    [EAGLContext setCurrentContext:glContext];

    int intUniform = glGetUniformLocation(glProgram, [parameterName cStringUsingEncoding:NSUTF8StringEncoding]);
    glUniform1i(intUniform, value);
}

-(void) render:(int) renderMode rebindFrameBuffer:(int) rebindFrameBuffer
{
    

    const GLfloat squareTextureCoordinates[] = {
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
    };
    glVertexAttribPointer(inputTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, squareTextureCoordinates);

    
    [EAGLContext setCurrentContext:glContext];

    [self setParameter:@"renderOutputType" intValue:renderMode];
    
    if(rebindFrameBuffer) {
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer); // TRICKY: force bind to our frame buffer render target!
    }
    
    // bind input textures
    int textureIndex = 0;
    GLenum activeTexture = GL_TEXTURE0;
    
//    DLog(@"textureMapping: %@", textureMapping);
    
    for(NSString* key in textureMapping) {
        GLuint textureLocation = glGetUniformLocation(glProgram, [key UTF8String]);
        
        GLuint texture = (GLuint) [[textureMapping objectForKey:key] intValue];
        
        glActiveTexture(activeTexture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glUniform1i(textureLocation, textureIndex);
        
        textureIndex++;
        
        if(textureIndex==0) {
            activeTexture = GL_TEXTURE0;
        } else if(textureIndex==1) {
            activeTexture = GL_TEXTURE1;
        } else if(textureIndex==2) {
            activeTexture = GL_TEXTURE2;
        } else if(textureIndex==3) {
            activeTexture = GL_TEXTURE3;
        } else if(textureIndex==4) {
            activeTexture = GL_TEXTURE4;
        } else if(textureIndex==5) {
            activeTexture = GL_TEXTURE5;
        } else if(textureIndex==6) {
            activeTexture = GL_TEXTURE6;
        } else if(textureIndex==7) {
            activeTexture = GL_TEXTURE7;
        }
    }

    // render image
    glActiveTexture(GL_TEXTURE0);

    glViewport(0, 0, targetViewPortSize.width, targetViewPortSize.height);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

//    DLog(@"START DARWING");
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
//    DLog(@"STOP DAWING");
}


-(void) rendernew:(int) renderMode rebindFrameBuffer:(int) rebindFrameBuffer
{
    

    const GLfloat squareTextureCoordinates[] = {
        0.218890548f , 1.0f,
        0.218890548f, 0.0f,
        0.781109452f, 1.0f,
        0.781109452f, 0.0f,
    };
    glVertexAttribPointer(inputTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, squareTextureCoordinates);
    
    [EAGLContext setCurrentContext:glContext];

    [self setParameter:@"renderOutputType" intValue:renderMode];
    
    if(rebindFrameBuffer) {
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer); // TRICKY: force bind to our frame buffer render target!
    }
    
    // bind input textures
    int textureIndex = 0;
    GLenum activeTexture = GL_TEXTURE0;
    
    //    DLog(@"textureMapping: %@", textureMapping);
    
    for(NSString* key in textureMapping) {
        GLuint textureLocation = glGetUniformLocation(glProgram, [key UTF8String]);
        
        GLuint texture = (GLuint) [[textureMapping objectForKey:key] intValue];
        
        glActiveTexture(activeTexture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glUniform1i(textureLocation, textureIndex);
        
        textureIndex++;
        
        if(textureIndex==0) {
            activeTexture = GL_TEXTURE0;
        } else if(textureIndex==1) {
            activeTexture = GL_TEXTURE1;
        } else if(textureIndex==2) {
            activeTexture = GL_TEXTURE2;
        } else if(textureIndex==3) {
            activeTexture = GL_TEXTURE3;
        } else if(textureIndex==4) {
            activeTexture = GL_TEXTURE4;
        } else if(textureIndex==5) {
            activeTexture = GL_TEXTURE5;
        } else if(textureIndex==6) {
            activeTexture = GL_TEXTURE6;
        } else if(textureIndex==7) {
            activeTexture = GL_TEXTURE7;
        }
    }
    
    // render image
    glActiveTexture(GL_TEXTURE0);
    
    glViewport(0, 0, targetViewPortSize.width, targetViewPortSize.height);
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    //    DLog(@"START DARWING");
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    //    DLog(@"STOP DAWING");
}

-(UIImage*) outputImage
{
    [self render:RENDER_MODE_RGBA rebindFrameBuffer:1];
    
    // get output image
    NSUInteger totalBytesForImage = (int)imageSize.width * (int)imageSize.height * 4;
    glReadPixels(0, 0, (int)imageSize.width, (int)imageSize.height, GL_RGBA, GL_UNSIGNED_BYTE, imageOutputDataBuffer);
    NSData *imageData = [NSData dataWithBytes:imageOutputDataBuffer length:totalBytesForImage];
    
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((CFDataRef)imageData);
    CGColorSpaceRef defaultRGBColorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGImageRef cgImageFromBytes = CGImageCreate((int)imageSize.width, (int)imageSize.height, 8, 32, 4 * (int)imageSize.width, defaultRGBColorSpace, kCGBitmapByteOrderDefault, dataProvider, NULL, NO, kCGRenderingIntentDefault);
    
    UIImage *finalImage;
    
    if(currentProgramID==FILTER_PROGRAM_GAUSSIAN_BLUR) {
        finalImage = [UIImage imageWithCGImage:cgImageFromBytes scale:1.0 orientation:UIImageOrientationUp];
    } else {
        finalImage = [UIImage imageWithCGImage:cgImageFromBytes scale:1.0 orientation:UIImageOrientationLeft];
    }
    
    CGColorSpaceRelease(defaultRGBColorSpace);
    CGDataProviderRelease(dataProvider);
    CGImageRelease(cgImageFromBytes);
    
    return finalImage;
}

-(NSArray*) computeGaussianBlur
{
    GLuint inputImageTexture = [[textureMapping objectForKey:@"inputImageTexture"] intValue];
    
    NSMutableArray* blurImgs = CREATE_MUTABLE_ARRAY;
    
    for(int i=0;i<4;i++) {
        [self setRotation:90 needHorizontalFlip:0];

        [self render:RENDER_MODE_RGBA rebindFrameBuffer:0];
        [blurImgs addObject:[self outputImage]];
        glBindTexture(GL_TEXTURE_2D, inputImageTexture);
        glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 0, 0, imageSize.width, imageSize.height, 0);
    }
    
    return blurImgs;
}

-(void) loadYData:(void*) yDataBuffer cbData:(void*) cbDataBuffer crData:(void*) crDataBuffer isSquare:(int) isSquare
{
    // NOTE: outputPixelBufferRef is in BGRA form, and Y data is in B channel, U data in G channel, V data in R channel
    CVPixelBufferLockBaseAddress(outputPixelBufferRef, 0);
    void* baseAddress = CVPixelBufferGetBaseAddress(outputPixelBufferRef);
    
    
    
    int bytesPerRow = imageSize.width * 4;
    int imageWidth = imageSize.width;
    int imageHeight = imageSize.height;
    int halfFrameWidth = imageWidth/2;
    int halfFrameHeight = imageHeight/2;
    
    if(isSquare==1) {
        // fill Y data
        int initialSrcPtrOffset = (imageWidth-imageHeight)/2 * 4;
        
        unsigned char * srcPtr = baseAddress + initialSrcPtrOffset;
        unsigned char * destPtr = yDataBuffer;
        
        for(int y=0;y<imageHeight;y++) {
            memcpy(destPtr, srcPtr, imageHeight);
            srcPtr += bytesPerRow;
            destPtr += imageHeight;
        }
        
        // fill UV data
        unsigned char* uvSrcPtr = baseAddress + initialSrcPtrOffset + 1;
        unsigned char* cbDestPtr = cbDataBuffer;
        unsigned char* crDestPtr = crDataBuffer;
        
        for(int y=0;y<halfFrameHeight;y++) {
            for(int x=0;x<halfFrameHeight;x++) {
                memcpy(cbDestPtr, uvSrcPtr, 1);
                memcpy(crDestPtr, uvSrcPtr+1, 1);
                
                uvSrcPtr += 4;
                cbDestPtr += 1;
                crDestPtr += 1;
            }
            
            uvSrcPtr += (initialSrcPtrOffset * 2);
        }
    } else {
        // fill Y data
        unsigned char * srcPtr = baseAddress;
        unsigned char * destPtr = yDataBuffer;
        
        for(int y=0;y<imageHeight;y++) {
            for(int x=0;x<imageWidth;x++) {
                memcpy(destPtr, srcPtr, 1);
                
                destPtr += 1;
                srcPtr += 4;
            }
        }
        
        // fill UV data
        srcPtr = baseAddress + 1;
        unsigned char * cbDestPtr = cbDataBuffer;
        unsigned char * crDestPtr = crDataBuffer;

        for(int y=0;y<halfFrameHeight;y++) {
            for(int x=0;x<halfFrameWidth;x++) {
                memcpy(cbDestPtr, srcPtr, 1);
                memcpy(crDestPtr, srcPtr+1, 1);

                srcPtr += 8;
                cbDestPtr += 1;
                crDestPtr += 1;
            }
            
            srcPtr += bytesPerRow; // skip entire row
        }
    }

    CVPixelBufferUnlockBaseAddress(outputPixelBufferRef, 0);
}

-(void) loadLuminanceData:(const void*) luminanceData textureSize:(CGSize) textureSize textureName:(NSString*) textureName
{
    [EAGLContext setCurrentContext:glContext];
    
    GLuint texture = 0;
    
    if([textureMapping objectForKey:textureName]==nil) {
        // generate new texture
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    } else {
        texture = (GLuint) [[textureMapping objectForKey:textureName] intValue];
    }
    
    glBindTexture(GL_TEXTURE_2D, texture);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, (int)textureSize.width, (int)textureSize.height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, luminanceData);
    
    [textureMapping setObject:[NSNumber numberWithInt:(int)texture] forKey:textureName];
}

-(void) setRotation:(float) degrees needHorizontalFlip:(int) needHorizontalFlip
{
    GLKMatrix4 matrix = GLKMatrix4MakeZRotation(GLKMathDegreesToRadians(degrees));
    
    if(needHorizontalFlip) {
        matrix = GLKMatrix4Multiply(matrix, GLKMatrix4MakeScale(-1.0, 1.0, 1.0));
    }
    
    glUniformMatrix4fv(glGetUniformLocation(glProgram, "rotationMatrix"), 1, false, matrix.m);
}

-(void) resetRotation
{
    if(imageOrientation==UIImageOrientationUp) {
        [self setRotation:0.0 needHorizontalFlip:0];
    } else if(imageOrientation==UIImageOrientationLeft) {
        [self setRotation:-90.0 needHorizontalFlip:0];
    }
}

-(void) loadFilter:(NSString*) filterName
{
    [EAGLContext setCurrentContext:glContext];

    // this is identity filter setting
    NSDictionary* filterParams = @{
                                   @"COLOR_MAP1": @"", // color map 1 pic name
                                   @"OVERLAY1": @"", // overlay 1 pic name
                                   @"OVERLAY2": @"" // overlay 2 pic name
                                   };
    
    if([filterName isEqualToString:NORMAL_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"", // color map 1 pic name
                         @"OVERLAY1": @"", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:RICH_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"rich_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight6", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:WARM_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"warm_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight2", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:SOFT_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"soft_map", // color map 1 pic name
                         @"OVERLAY1": @"", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:ROSE_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"rose_map", // color map 1 pic name
                         @"OVERLAY1": @"", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:MORNING_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"morning_map", // color map 1 pic name
                         @"OVERLAY1": @"", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:SUNSHINE_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"sunshine_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight2", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:SUNSET_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"sunset_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight1", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    
    if([filterName isEqualToString:COOL_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"cool_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight2", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:FREEZE_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"freeze_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight1", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:OCEAN_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"ocean_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight1", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:DREAM_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"dream_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight3", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:VIOLET_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"violet_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight1", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:MELLOW_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"mellow_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight6", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:BLEAK_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"bleak_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight1", // overlay 1 pic name
                         @"OVERLAY2": @"overlay_softlight2" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:MEMORY_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"memory_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight1", // overlay 1 pic name
                         @"OVERLAY2": @"overlay_softlight3" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:PURE_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"pure_map", // color map 1 pic name
                         @"OVERLAY1": @"", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:CALM_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"calm_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight2", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:AUTUMN_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"autumn_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight1", // overlay 1 pic name
                         @"OVERLAY2": @"overlay_softlight3" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:FANTASY_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"fantasy_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight4", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:FREEDOM_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"freedom_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight2", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:MILD_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"mild_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight5", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:PRAIRIE_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"prairie_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight5", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:DEEP_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"deep_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight2", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:GLOW_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"glow_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight5", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:MEMOIR_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"memoir_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight6", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:MIST_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"mist_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight5", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:VIVID_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"vivid_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight1", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:CHILL_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"chill_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight1", // overlay 1 pic name
                         @"OVERLAY2": @"overlay_softlight5" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:PINKY_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"pinky_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight5", // overlay 1 pic name
                         @"OVERLAY2": @"" // overlay 2 pic name
                         };
    }
    
    if([filterName isEqualToString:ADVENTURE_FILTER]) {
        filterParams = @{
                         @"COLOR_MAP1": @"adventure_map", // color map 1 pic name
                         @"OVERLAY1": @"overlay_softlight2", // overlay 1 pic name
                         @"OVERLAY2": @"overlay_softlight3" // overlay 2 pic name
                         };
    }
    
    // load color map 1
    if(![filterParams[@"COLOR_MAP1"] isEqualToString:@""]) {
        [self setParameter:@"colorMap1Enabled" intValue:1];
        
        UIImage* colorMapImage1 = textureCache[filterParams[@"COLOR_MAP1"]];
        
        if(colorMapImage1==nil) {
            colorMapImage1 = [UIImage imageNamed:filterParams[@"COLOR_MAP1"]];
            textureCache[filterParams[@"COLOR_MAP1"]] = colorMapImage1;
        }
        
        [self loadInputImage:colorMapImage1 textureName:@"colorMapTexture1"];
    } else {
        [self setParameter:@"colorMap1Enabled" intValue:0];
    }

    // load overlay 1
    if(![filterParams[@"OVERLAY1"] isEqualToString:@""]) {
        [self setParameter:@"overlay1Enabled" intValue:1];

        UIImage* overlayImage1 = textureCache[filterParams[@"OVERLAY1"]];
        
        if(overlayImage1==nil) {
            overlayImage1 = [UIImage imageNamed:filterParams[@"OVERLAY1"]];
            textureCache[filterParams[@"OVERLAY1"]] = overlayImage1;
        }
        
        [self loadInputImage:overlayImage1 textureName:@"overlayTexture1"];
    } else {
        [self setParameter:@"overlay1Enabled" intValue:0];
    }
    
    // load overlay 2
    if(![filterParams[@"OVERLAY2"] isEqualToString:@""]) {
        [self setParameter:@"overlay2Enabled" intValue:1];
        
        UIImage* overlayImage2 = textureCache[filterParams[@"OVERLAY2"]];
        
        if(overlayImage2==nil) {
            overlayImage2 = [UIImage imageNamed:filterParams[@"OVERLAY2"]];
            textureCache[filterParams[@"OVERLAY2"]] = overlayImage2;
        }
        
        [self loadInputImage:overlayImage2 textureName:@"overlayTexture2"];
    } else {
        [self setParameter:@"overlay2Enabled" intValue:0];
    }
}

-(void) resetToolParams
{
    [EAGLContext setCurrentContext:glContext];
    
    [self setParameter:@"filterMixPercentage" floatValue:1.0];
    [self setParameter:@"brightness" floatValue:0.0]; // brightness
    [self setParameter:@"contrast" floatValue:0.0]; // contrast
    [self setParameter:@"unsharpMask" floatValue:0.0]; // unsharpMask
    [self setParameter:@"skinSmooth" floatValue:0.0]; // skinSmooth
    [self setParameter:@"autoEnhance" floatValue:0.0]; // auto enhance
    [self setParameter:@"warmth" floatValue:0.0]; // warmth
    [self setParameter:@"saturation" floatValue:0.0]; // saturation
    [self setParameter:@"fade" floatValue:0.0]; // fade
    [self setParameter:@"highlights" floatValue:0.0]; // highlights
    [self setParameter:@"shadows" floatValue:0.0]; // shadows
    [self setParameter:@"vignette" floatValue:0.0]; // vignette
    [self setParameter:@"sharpen" floatValue:0.0]; // sharpen
    [self setParameter:@"shadowsColorMapIndex" floatValue:0.0]; // shadowsColorMapIndex
    [self setParameter:@"highlightsColorMapIndex" floatValue:0.0]; // highlightsColorMapIndex
    [self setParameter:@"colorShadows" floatValue:0.0]; // colorShadows
    [self setParameter:@"colorHighlights" floatValue:0.0]; // colorHighlights
    
    // tilt shift
    [self setParameter:@"tiltShiftMode" floatValue:0.0];
    [self setParameter:@"tiltShiftRadialCenterX" floatValue:0.0];
    [self setParameter:@"tiltShiftRadialCenterY" floatValue:0.0];
    [self setParameter:@"tiltShiftRadialRadius" floatValue:0.0];
    [self setParameter:@"tiltShiftLinearCenterX" floatValue:0.0];
    [self setParameter:@"tiltShiftLinearCenterY" floatValue:0.0];
    [self setParameter:@"tiltShiftLinearRadius" floatValue:0.0];
    [self setParameter:@"tiltShiftLinearAngle" floatValue:0.0];
}

-(void) clearProgram
{
    [EAGLContext setCurrentContext:glContext];

    if(glProgram) {
        glDeleteProgram(glProgram);
    }
    
    if(vertexShader) {
        glDeleteShader(vertexShader);
    }
    
    if(fragmentShader) {
        glDeleteShader(fragmentShader);
    }
}

-(void)dealloc
{
    [EAGLContext setCurrentContext:glContext];

    for(NSString* key in textureMapping) {
        GLuint texture = (GLuint) [[textureMapping objectForKey:key] intValue];
        glDeleteTextures(1, &texture);
    }
    
    if(outputTexture) {
        glDeleteTextures(1, &outputTexture);
    }
    
    if(renderBuffer) {
        glDeleteRenderbuffers(1, &renderBuffer);
    }
    
    glDeleteFramebuffers(1, &framebuffer);
    
    [self clearProgram];
    
    free(imageOutputDataBuffer);
    
    if(glTextureCacheRef) {
        CFRelease(glTextureCacheRef);
    }
    
    if(outputPixelBufferRef) {
        CFRelease(outputPixelBufferRef);
    }
    
    if(glOutputTextureRef) {
        CFRelease(glOutputTextureRef);
    }
}

char *const vertexShaderString = STRINGIZE
(
 attribute vec4 position;
 attribute vec4 inputTextureCoordinate;
 
 uniform mat4 rotationMatrix;
 
 uniform float pixelStepSize;
 
 varying vec2 textureCoordinate;
 varying vec2 sharpCoordinates[4];
 varying vec2 tap9BlurCoordinates[9];

 void main()
 {
     gl_Position = position * rotationMatrix;
     
     textureCoordinate = inputTextureCoordinate.xy;
     
     sharpCoordinates[0] = inputTextureCoordinate.xy + vec2(0.0, -pixelStepSize);
     sharpCoordinates[1] = inputTextureCoordinate.xy + vec2(-pixelStepSize, 0.0);
     sharpCoordinates[2] = inputTextureCoordinate.xy + vec2(0.0, pixelStepSize);
     sharpCoordinates[3] = inputTextureCoordinate.xy + vec2(pixelStepSize, 0.0);
     
     tap9BlurCoordinates[0] = inputTextureCoordinate.xy;
     tap9BlurCoordinates[1] = inputTextureCoordinate.xy + vec2(0.0, -pixelStepSize) * 1.3846153846 * 2.0;
     tap9BlurCoordinates[2] = inputTextureCoordinate.xy + vec2(-pixelStepSize, 0.0) * 1.3846153846 * 2.0;
     tap9BlurCoordinates[3] = inputTextureCoordinate.xy + vec2(0.0, pixelStepSize) * 1.3846153846 * 2.0;
     tap9BlurCoordinates[4] = inputTextureCoordinate.xy + vec2(pixelStepSize, 0.0) * 1.3846153846 * 2.0;
     tap9BlurCoordinates[5] = inputTextureCoordinate.xy + vec2(0.0, -pixelStepSize) * 3.2307692308 * 2.0;
     tap9BlurCoordinates[6] = inputTextureCoordinate.xy + vec2(-pixelStepSize, 0.0) * 3.2307692308 * 2.0;
     tap9BlurCoordinates[7] = inputTextureCoordinate.xy + vec2(0.0, pixelStepSize) * 3.2307692308 * 2.0;
     tap9BlurCoordinates[8] = inputTextureCoordinate.xy + vec2(pixelStepSize, 0.0) * 3.2307692308 * 2.0;
 }
);

 char *const fragmentShaderString = STRINGIZE
 (
  precision highp float;

  varying vec2 textureCoordinate;
  varying vec2 sharpCoordinates[4];
  varying vec2 tap9BlurCoordinates[9];

  uniform int imageSourceInputType; // 0: RGBA, 1: Y + U + V, 2: Y + UV (NV21)
  uniform int renderOutputType; // 0: RGBA, 1: YUV
  
  uniform int colorMap1Enabled;
  uniform int overlay1Enabled;
  uniform int overlay2Enabled;
  
  uniform sampler2D inputImageTexture; // mandatory
  
  uniform sampler2D blurTexture1;
  uniform sampler2D blurTexture2;
  uniform sampler2D blurTexture3;
  uniform sampler2D blurTexture4;

  uniform sampler2D yTexture;
  uniform sampler2D uTexture;
  uniform sampler2D vTexture;
  
  uniform sampler2D uvTexture;
  
  uniform sampler2D sharedColorMapTexture; // mandatory => this texture contains 20 1X256 color maps, for highlights, shadows color transform
  
  uniform sampler2D colorMapTexture1; // optional
  uniform sampler2D overlayTexture1; // optional
  uniform sampler2D overlayTexture2; // optional
  
  uniform float filterMixPercentage; // mandatory
  
  // tools
  uniform float brightness; // -100 ~ 0 ~ 100, default 0
  uniform float contrast; // -100 ~ 0 ~ 100, default 0
  uniform float unsharpMask; // 0 ~ 100, default 0
  uniform float skinSmooth; // 0 ~ 100, default 0
  uniform float warmth; // -100 ~ 0 ~ 100, default 0
  uniform float saturation; // -100 ~ 0 ~ 100, default 0
  uniform float fade; // 0 ~ 100, default 0
  uniform float shadows; // -100 ~ 0 ~ 100, default 0
  uniform float highlights; // -100 ~ 0 ~ 100, default 0
  uniform float sharpen; // 0 ~ 100, default 0
  uniform float vignette; // 0 ~ 100, default 0
  uniform float autoEnhance; // 0 ~ 100, default 0
  
  // color map: 0.0 means no color map, 1.0~8.0: highlight colors, 9.0~16.0 shadow colors, 17.0: highlight minus, 18.0: highlight plus, 19.0: shadow minus, 20.0: shadow highlight
  uniform float shadowsColorMapIndex;
  uniform float highlightsColorMapIndex;
  uniform float colorShadows; // 0 ~ 100, default 0
  uniform float colorHighlights; // 0 ~ 100, default 0
  
  // tilt shift
  uniform float tiltShiftMode;
  uniform float tiltShiftRadialCenterX;
  uniform float tiltShiftRadialCenterY;
  uniform float tiltShiftRadialRadius;
  uniform float tiltShiftLinearCenterX;
  uniform float tiltShiftLinearCenterY;
  uniform float tiltShiftLinearRadius;
  uniform float tiltShiftLinearAngle; // in radians: 0 as horizontal
  
  const mat3 yuv2rgbMatrix = mat3( 1,       1,         1,
                                  0,       -0.39465,  2.03211,
                                  1.13983, -0.58060,  0);
  
  const mat3 rgb2yuvMatrix = mat3( 0.2990020091240603,       -0.14713869284835024,         0.6150022291709639,
                                  0.5869987255595551,       -0.28886168837294957,  -0.5149879592215989,
                                  0.11399926531638466, 0.4360003812212998,  -0.10001426994936496);
  
  // overlay
  float overlayCal(float a, float b){
      if(a<.5)
          return 2.*a*b;
      else
          return 1.-2.*(1.-a)*(1.-b);
      
      return 0.;
  }
  
  // softLight
  float softLightCal(float a, float b){
      if(b<.5)
          return 2.*a*b+a*a*(1.-2.*b);
      else
          return 2.*a*(1.-b)+sqrt(a)*(2.*b-1.);
      
      return 0.;
  }
  
  // soft overlay
  vec3 softOverlayBlend(vec3 a, float mag) {
      return pow(a, vec3(1.0 / (1.0 - mag)));
  }
  
  // hsv rgb color space transformation
  vec3 rgb2hsv(vec3 c)
  {
      vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
      vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
      vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);
      
      float d = q.x - min(q.w, q.y);
      float e = 1.0e-10;
      return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
  }
  
  vec3 hsv2rgb(vec3 c)
  {
      vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
      vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
      return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
  }
  
  float getLuma(vec3 inputColor) {
      return  (0.299 * inputColor.r) +
      (0.587 * inputColor.g) +
      (0.114 * inputColor.b);
  }
  
  vec3 rgbToYuv(vec3 inP) {
      vec3 outP;
      outP.r = getLuma(inP);
      outP.g = (1.0/1.772)*(inP.b - outP.r);
      outP.b = (1.0/1.402)*(inP.r - outP.r);
      return outP;
  }
  
  vec3 yuvToRgb(vec3 inP) {
      float y = inP.r;
      float u = inP.g;
      float v = inP.b;
      vec3 outP;
      outP.r = 1.402 * v + y;
      outP.g = (y - (0.299 * 1.402 / 0.587) * v -
                (0.114 * 1.772 / 0.587) * u);
      outP.b = 1.772 * u + y;
      return outP;
  }
  
  float easeInOutSigmoid(float value, float strength) {
      float t = 1.0 / (1.0 - strength);
      if (value > 0.5) {
          return 1.0 - pow(2.0 - 2.0 * value, t) * 0.5;
      } else {
          return pow(2.0 * value, t) * 0.5;
      }
  }
  
  vec3 bowRgbChannels(vec3 inVal, float mag) {
      
      vec3 outVal;
      float power = 1.0 + abs(mag);
      
      if (mag < 0.0) {
          power = 1.0 / power;
      }
      
      outVal.r = 1.0 - pow((1.0 - inVal.r), power);
      outVal.g = 1.0 - pow((1.0 - inVal.g), power);
      outVal.b = 1.0 - pow((1.0 - inVal.b), power);
      
      return outVal;
  }
  
  vec3 fadeFunc(vec3 color) {
      vec3 co1 = vec3(-0.9772);
      vec3 co2 = vec3(1.708);
      vec3 co3 = vec3(-0.1603);
      vec3 co4 = vec3(0.2878);
      
      vec3 comp1 = co1 * pow(color, vec3(3.0));
      vec3 comp2 = co2 * pow(color, vec3(2.0));
      vec3 comp3 = co3 * color;
      vec3 comp4 = co4;
      
      return comp1 + comp2 + comp3 + comp4;
  }

  vec3 adjustTemperature(float tempDelta, vec3 inRgb) {
      vec3 yuvVec;
      
      if (tempDelta > 0.0 ) {
          // "warm" midtone change
          yuvVec =  vec3(0.1765, -0.1255, 0.0902);
      } else {
          // "cool" midtone change
          yuvVec = -vec3(0.0588,  0.1569, -0.1255);
      }
      
      vec3 yuvColor = rgbToYuv(inRgb);
      
      float luma = yuvColor.r;
      
      float curveScale = sin(luma * 3.14159); // a hump
      
      yuvColor += 0.375 * tempDelta * curveScale * yuvVec;
      inRgb = yuvToRgb(yuvColor);
      return inRgb;
  }
  
  float computeBilateralWeight(float originalWeight, vec3 centerColor, vec3 targetColor, float strength) {
      float colorDiff = length(centerColor-targetColor) / 1.73205080757; // divide by sqrt(3.0) to normalize
      float threshold = strength / 20.0;
      
      if(colorDiff>threshold) {
          return 0.0;
      }
      
      return originalWeight * (1.0 - colorDiff) * strength * 2.0;
  }
  
  vec3 computeBilateral(sampler2D inputImageTexture, sampler2D blurTexture, vec2 textureCoordinate, float strength) {
      vec3 gaussianResult = vec3(0.0);

      vec3 centerColor = texture2D(inputImageTexture, textureCoordinate).rgb;
      vec3 targetColor = vec3(0.0);
      float weight = 0.0;
      float weightSum = 0.0;
      
      gaussianResult += texture2D(inputImageTexture, textureCoordinate).rgb * 0.2270270270;
      weightSum += 0.2270270270;
      
      targetColor = texture2D(blurTexture, tap9BlurCoordinates[1]).rgb;
      weight = computeBilateralWeight(0.1581081081, centerColor, targetColor, strength);
      gaussianResult += targetColor * weight;
      weightSum += weight;
      
      targetColor = texture2D(blurTexture, tap9BlurCoordinates[2]).rgb;
      weight = computeBilateralWeight(0.1581081081, centerColor, targetColor, strength);
      gaussianResult += targetColor * weight;
      weightSum += weight;
      
      targetColor = texture2D(blurTexture, tap9BlurCoordinates[3]).rgb;
      weight = computeBilateralWeight(0.1581081081, centerColor, targetColor, strength);
      gaussianResult += targetColor * weight;
      weightSum += weight;
      
      targetColor = texture2D(blurTexture, tap9BlurCoordinates[4]).rgb;
      weight = computeBilateralWeight(0.1581081081, centerColor, targetColor, strength);
      gaussianResult += targetColor * weight;
      weightSum += weight;
      
      targetColor = texture2D(blurTexture, tap9BlurCoordinates[5]).rgb;
      weight = computeBilateralWeight(0.03513513515, centerColor, targetColor, strength);
      gaussianResult += targetColor * weight;
      weightSum += weight;
      
      targetColor = texture2D(blurTexture, tap9BlurCoordinates[6]).rgb;
      weight = computeBilateralWeight(0.03513513515, centerColor, targetColor, strength);
      gaussianResult += targetColor * weight;
      weightSum += weight;
      
      targetColor = texture2D(blurTexture, tap9BlurCoordinates[7]).rgb;
      weight = computeBilateralWeight(0.03513513515, centerColor, targetColor, strength);
      gaussianResult += targetColor * weight;
      weightSum += weight;
      
      targetColor = texture2D(blurTexture, tap9BlurCoordinates[8]).rgb;
      weight = computeBilateralWeight(0.03513513515, centerColor, targetColor, strength);
      gaussianResult += targetColor * weight;
      weightSum += weight;

      return gaussianResult/weightSum;
  }
  
  vec3 applyColorMap(vec3 inputTexture, sampler2D colorMap) {
      float size = 33.0;
      
      float sliceSize = 1.0 / size;
      float slicePixelSize = sliceSize / size;
      float sliceInnerSize = slicePixelSize * (size - 1.0);
      float xOffset = 0.5 * sliceSize + inputTexture.x * (1.0 - sliceSize);
      float yOffset = 0.5 * slicePixelSize + inputTexture.y * sliceInnerSize;
      float zOffset = inputTexture.z * (size - 1.0);
      float zSlice0 = floor(zOffset);
      float zSlice1 = zSlice0 + 1.0;
      float s0 = yOffset + (zSlice0 * sliceSize);
      float s1 = yOffset + (zSlice1 * sliceSize);
      vec4 sliceColor0 = texture2D(colorMap, vec2(xOffset, s0));
      vec4 sliceColor1 = texture2D(colorMap, vec2(xOffset, s1));

      return mix(sliceColor0, sliceColor1, zOffset - zSlice0).rgb;
  }
  
  void main()
  {
      vec3 output_result = texture2D(inputImageTexture, textureCoordinate).rgb;
      
      /* tool part */
      
      // sharpen
      if(sharpen!=0.0) {
          vec3 sharpen_result = output_result.rgb * 5.0;
          
          sharpen_result -= texture2D(inputImageTexture, sharpCoordinates[0]).rgb;
          sharpen_result -= texture2D(inputImageTexture, sharpCoordinates[1]).rgb;
          sharpen_result -= texture2D(inputImageTexture, sharpCoordinates[2]).rgb;
          sharpen_result -= texture2D(inputImageTexture, sharpCoordinates[3]).rgb;
          
          output_result = mix(output_result, sharpen_result, sharpen/100.0);
          
          output_result = clamp(output_result, 0.0, 1.0);
      }
      
      // tilt shift
      if(tiltShiftMode==1.0) { // radial blur: determine radius
          float dist = length(textureCoordinate-vec2(tiltShiftRadialCenterX, tiltShiftRadialCenterY));
          float normalizedDist = dist/tiltShiftRadialRadius;
          
          if(normalizedDist<0.5) {

          } else if(normalizedDist<0.75) { // original -> blur 2: 0.5 ~ 0.75
              output_result = mix(output_result, texture2D(blurTexture2, textureCoordinate).rgb, (normalizedDist-0.5)/0.25);
          } else if(normalizedDist<1.0) { // blur 2 -> blur 3: 0.75 ~ 1.0
              output_result = mix(texture2D(blurTexture2, textureCoordinate).rgb, texture2D(blurTexture3, textureCoordinate).rgb, (normalizedDist-0.75)/0.25);
          } else if(normalizedDist<1.25) { // blur 3 -> blur 4: 1.0 ~ 1.25
              output_result = mix(texture2D(blurTexture3, textureCoordinate).rgb, texture2D(blurTexture4, textureCoordinate).rgb, (normalizedDist-1.0)/0.25);
          } else { // blur 4
              output_result = texture2D(blurTexture4, textureCoordinate).rgb;
          }
      } else if(tiltShiftMode==2.0) { // linear blur: determine radius
          float dist = abs(dot(textureCoordinate-vec2(tiltShiftLinearCenterX, tiltShiftLinearCenterY), vec2(sin(tiltShiftLinearAngle), cos(tiltShiftLinearAngle))));
          float normalizedDist = dist/tiltShiftLinearRadius;
          
          if(normalizedDist<0.5) {
              
          } else if(normalizedDist<0.75) { // original -> blur 2: 0.5 ~ 0.75
              output_result = mix(output_result, texture2D(blurTexture2, textureCoordinate).rgb, (normalizedDist-0.5)/0.25);
          } else if(normalizedDist<1.0) { // blur 2 -> blur 3: 0.75 ~ 1.0
              output_result = mix(texture2D(blurTexture2, textureCoordinate).rgb, texture2D(blurTexture3, textureCoordinate).rgb, (normalizedDist-0.75)/0.25);
          } else if(normalizedDist<1.25) { // blur 3 -> blur 4: 1.0 ~ 1.25
              output_result = mix(texture2D(blurTexture3, textureCoordinate).rgb, texture2D(blurTexture4, textureCoordinate).rgb, (normalizedDist-1.0)/0.25);
          } else { // blur 4
              output_result = texture2D(blurTexture4, textureCoordinate).rgb;
          }
      }
      
      output_result = clamp(output_result, 0.0, 1.0);
      
      // skin smoothing: bilateral filtering
      if(skinSmooth!=0.0) {
          output_result = computeBilateral(inputImageTexture, blurTexture4, textureCoordinate, skinSmooth/100.0);
          
          output_result = clamp(output_result, 0.0, 1.0);
      }
      
      // unsharp mask
      if(unsharpMask!=0.0) {
          vec3 gaussinan_blur_result = texture2D(blurTexture2, textureCoordinate).rgb;
          
          output_result = output_result*(1.0+unsharpMask/100.0) - gaussinan_blur_result*(unsharpMask/100.0);
          
          output_result = clamp(output_result, 0.0, 1.0);
      }
      
      // auto enhance
      if(autoEnhance!=0.0) {
          vec3 gaussinan_blur_result = texture2D(blurTexture4, textureCoordinate).rgb;

          output_result = output_result*(1.0+autoEnhance/100.0) - gaussinan_blur_result*(autoEnhance/100.0);
          
          output_result = clamp(output_result, 0.0, 1.0);
      }
      
      // fade
      if (fade!=0.0) {
          vec3 faded = fadeFunc(output_result);
          output_result = (output_result * (1.0 - fade/100.0)) + (faded * fade/100.0);
      }

      // highlights and shadows
      if(highlights>0.0) {
          vec3 highlight_result = vec3(
                                       texture2D(sharedColorMapTexture, vec2(output_result.r, 0.875)).r,
                                       texture2D(sharedColorMapTexture, vec2(output_result.g, 0.875)).g,
                                       texture2D(sharedColorMapTexture, vec2(output_result.b, 0.875)).b);
          output_result = mix(output_result, highlight_result, highlights/100.0);
      } else if(highlights<0.0) {
          vec3 highlight_result = vec3(
                                       texture2D(sharedColorMapTexture, vec2(output_result.r, 0.825)).r,
                                       texture2D(sharedColorMapTexture, vec2(output_result.g, 0.825)).g,
                                       texture2D(sharedColorMapTexture, vec2(output_result.b, 0.825)).b);
          
          output_result = mix(output_result, highlight_result, -highlights/100.0);
      }
      
      if(shadows>0.0) {
          vec3 shadow_result = vec3(
                                    texture2D(sharedColorMapTexture, vec2(output_result.r, 0.975)).r,
                                    texture2D(sharedColorMapTexture, vec2(output_result.g, 0.975)).g,
                                    texture2D(sharedColorMapTexture, vec2(output_result.b, 0.975)).b);
          output_result = mix(output_result, shadow_result, shadows/100.0);
      } else if(shadows<=0.0) {
          vec3 shadow_result = vec3(
                                    texture2D(sharedColorMapTexture, vec2(output_result.r, 0.925)).r,
                                    texture2D(sharedColorMapTexture, vec2(output_result.g, 0.925)).g,
                                    texture2D(sharedColorMapTexture, vec2(output_result.b, 0.925)).b);
          output_result = mix(output_result, shadow_result, -shadows/100.0);
      }
      
      // saturation
      if(saturation!=0.0) {
          vec3 hsv = rgb2hsv(output_result);
          
          float saturationFactor = 1.0 + saturation/100.0;
          hsv.y = hsv.y * saturationFactor;
          hsv.y = clamp(hsv.y, 0.0, 1.0);
          
          output_result = hsv2rgb(hsv);
          
          output_result = clamp(output_result, 0.0, 1.0);
      }
      
      // contrast
      if(contrast!=0.0) {
          output_result = clamp(output_result, 0.0, 1.0);
          
          float strength = contrast/100.0 * 0.5; // adjust range to useful values
          
          vec3 yuv = rgbToYuv(output_result.rgb);
          yuv.x = easeInOutSigmoid(yuv.x, strength);
          yuv.y = easeInOutSigmoid(yuv.y + 0.5, strength * 0.65) - 0.5;
          yuv.z = easeInOutSigmoid(yuv.z + 0.5, strength * 0.65) - 0.5;
          output_result.rgb = yuvToRgb(yuv);
          
          output_result = clamp(output_result, 0.0, 1.0);
      }
      
      // brightness
      if(brightness!=0.0) {
          output_result.rgb = clamp(output_result.rgb, 0.0, 1.0);
          output_result.rgb = bowRgbChannels(output_result.rgb, brightness/100.0 * 1.1);
          
          output_result = clamp(output_result, 0.0, 1.0);
      }
      
      // warmth
      if(warmth!=0.0) {
          output_result.rgb = adjustTemperature(warmth/100.0, output_result.rgb);
      }
      
      // color map shadows and highlights
      if(shadowsColorMapIndex>0.0) {
          float colorMapPosY = shadowsColorMapIndex/20.0-0.025;
          
          vec3 color_map_result = vec3(
                                       texture2D(sharedColorMapTexture, vec2(output_result.r, colorMapPosY)).r,
                                       texture2D(sharedColorMapTexture, vec2(output_result.g, colorMapPosY)).g,
                                       texture2D(sharedColorMapTexture, vec2(output_result.b, colorMapPosY)).b);
          output_result = mix(output_result, color_map_result, colorShadows/100.0);
      }
      
      if(highlightsColorMapIndex>0.0) {
          float colorMapPosY = highlightsColorMapIndex/20.0-0.025;
          
          vec3 color_map_result = vec3(
                                       texture2D(sharedColorMapTexture, vec2(output_result.r, colorMapPosY)).r,
                                       texture2D(sharedColorMapTexture, vec2(output_result.g, colorMapPosY)).g,
                                       texture2D(sharedColorMapTexture, vec2(output_result.b, colorMapPosY)).b);
          output_result = mix(output_result, color_map_result, colorHighlights/100.0);
      }
      
      // vignette
      if(vignette!=0.0) {
          const float midpoint = 0.7;
          const float fuzziness = 0.62;
          
          float radDist = length(textureCoordinate - 0.5) / sqrt(0.5);
          float mag = easeInOutSigmoid(radDist * midpoint, fuzziness) * vignette/100.0 * 0.645;

          output_result.rgb = mix(softOverlayBlend(output_result.rgb, mag), vec3(0.0), mag * mag);
      }
      
      /* filter part */
      vec3 filter_result = output_result;
      
      // color curve 1: optional
      if(colorMap1Enabled==1) {
          filter_result = applyColorMap(filter_result, colorMapTexture1);
      }
      
      // overlay 1: optional
      if(overlay1Enabled==1) {
          vec3 overlay_image1 = texture2D(overlayTexture1, textureCoordinate).rgb;
          
          filter_result = vec3(softLightCal(filter_result.r, overlay_image1.r),
                               softLightCal(filter_result.g, overlay_image1.g),
                               softLightCal(filter_result.b, overlay_image1.b));
          
          filter_result = clamp(filter_result, 0.0, 1.0);
      }
      
      // overlay 2: optional
      if(overlay2Enabled==1) {
          vec3 overlay_image2 = texture2D(overlayTexture2, textureCoordinate).rgb;
          
          filter_result = vec3(overlayCal(filter_result.r, overlay_image2.r),
                               overlayCal(filter_result.g, overlay_image2.g),
                               overlayCal(filter_result.b, overlay_image2.b));
          
          filter_result = clamp(filter_result, 0.0, 1.0);
      }
      
      // filter mix percentage
      vec3 final_result = mix(output_result, filter_result, filterMixPercentage);

      gl_FragColor = vec4(final_result, 1.);
  }
  );

char *const gaussianBlurVertexShaderString = STRINGIZE
(
 attribute vec4 position;
 attribute vec4 inputTextureCoordinate;
 
 uniform mat4 rotationMatrix;
 
 uniform float pixelStepSize;

 varying vec2 tap9BlurCoordinates[9];
 
 void main()
 {
     gl_Position = position * rotationMatrix;

     tap9BlurCoordinates[0] = inputTextureCoordinate.xy;
     tap9BlurCoordinates[1] = inputTextureCoordinate.xy + vec2(0.0, -pixelStepSize) * 1.3846153846 * 2.0;
     tap9BlurCoordinates[2] = inputTextureCoordinate.xy + vec2(-pixelStepSize, 0.0) * 1.3846153846 * 2.0;
     tap9BlurCoordinates[3] = inputTextureCoordinate.xy + vec2(0.0, pixelStepSize) * 1.3846153846 * 2.0;
     tap9BlurCoordinates[4] = inputTextureCoordinate.xy + vec2(pixelStepSize, 0.0) * 1.3846153846 * 2.0;
     tap9BlurCoordinates[5] = inputTextureCoordinate.xy + vec2(0.0, -pixelStepSize) * 3.2307692308 * 2.0;
     tap9BlurCoordinates[6] = inputTextureCoordinate.xy + vec2(-pixelStepSize, 0.0) * 3.2307692308 * 2.0;
     tap9BlurCoordinates[7] = inputTextureCoordinate.xy + vec2(0.0, pixelStepSize) * 3.2307692308 * 2.0;
     tap9BlurCoordinates[8] = inputTextureCoordinate.xy + vec2(pixelStepSize, 0.0) * 3.2307692308 * 2.0;
 }
 );

char *const gaussianBlurFragmentShaderString = STRINGIZE
(
 precision highp float;
 
 varying vec2 tap9BlurCoordinates[9];
 uniform sampler2D inputImageTexture;
 
 void main()
 {
     vec4 gaussianResult = texture2D(inputImageTexture, tap9BlurCoordinates[0]) * 0.2270270270;
     gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[1]) * 0.1581081081;
     gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[2]) * 0.1581081081;
     gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[3]) * 0.1581081081;
     gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[4]) * 0.1581081081;
     gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[5]) * 0.03513513515;
     gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[6]) * 0.03513513515;
     gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[7]) * 0.03513513515;
     gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[8]) * 0.03513513515;
     
     gl_FragColor = gaussianResult;
 }
 );

char *const simpleFilterVertexShaderString = STRINGIZE
(
 attribute vec4 position;
 attribute vec4 inputTextureCoordinate;
 
 uniform mat4 rotationMatrix;
 
 varying vec2 textureCoordinate;

 void main()
 {
     gl_Position = position * rotationMatrix;
     
     
     textureCoordinate = inputTextureCoordinate.xy;
 }
 );

char *const simpleFilterFragmentShaderString = STRINGIZE
(
 precision highp float;
 
 varying vec2 textureCoordinate;
 
 uniform int colorMap1Enabled;
 uniform int overlay1Enabled;
 uniform int overlay2Enabled;
 
 uniform sampler2D inputImageTexture; // mandatory
 
 uniform sampler2D yTexture;
 uniform sampler2D uTexture;
 uniform sampler2D vTexture;
 
 uniform sampler2D uvTexture;
 
 uniform sampler2D sharedColorMapTexture; // mandatory => this texture contains 20 1X256 color maps, for highlights, shadows color transform
 
 uniform sampler2D colorMapTexture1; // optional
 uniform sampler2D overlayTexture1; // optional
 uniform sampler2D overlayTexture2; // optional
 
 uniform float filterMixPercentage; // mandatory
 
 uniform int imageSourceInputType; // 0: RGBA, 1: Y + U + V, 2: Y + UV (NV21)
 uniform int renderOutputType; // 0: RGBA, 1: YUV
 
 // overlay
 float overlayCal(float a, float b){
     if(a<.5)
         return 2.*a*b;
     else
         return 1.-2.*(1.-a)*(1.-b);
     
     return 0.;
 }
 
 // softLight
 float softLightCal(float a, float b){
     if(b<.5)
         return 2.*a*b+a*a*(1.-2.*b);
     else
         return 2.*a*(1.-b)+sqrt(a)*(2.*b-1.);
     
     return 0.;
 }
 
 vec3 applyColorMap(vec3 inputTexture, sampler2D colorMap) {
     float size = 33.0;
     
     float sliceSize = 1.0 / size;
     float slicePixelSize = sliceSize / size;
     float sliceInnerSize = slicePixelSize * (size - 1.0);
     float xOffset = 0.5 * sliceSize + inputTexture.x * (1.0 - sliceSize);
     float yOffset = 0.5 * slicePixelSize + inputTexture.y * sliceInnerSize;
     float zOffset = inputTexture.z * (size - 1.0);
     float zSlice0 = floor(zOffset);
     float zSlice1 = zSlice0 + 1.0;
     float s0 = yOffset + (zSlice0 * sliceSize);
     float s1 = yOffset + (zSlice1 * sliceSize);
     vec4 sliceColor0 = texture2D(colorMap, vec2(xOffset, s0));
     vec4 sliceColor1 = texture2D(colorMap, vec2(xOffset, s1));
     
     return mix(sliceColor0, sliceColor1, zOffset - zSlice0).rgb;
 }
 
 const mat3 yuv2rgbMatrix = mat3(1.0, 1.0, 1.0,
                                 0.0, -0.343, 1.765,
                                 1.4, -0.711, 0.0);
 
 const mat3 rgb2yuvMatrix = mat3( 0.29835489519425873,       -0.16903960067663437,         0.5011750748612437,
                                 0.5874779933501582,       -0.33284872144484856,  -0.41962713810725577,
                                 0.11416711145558314, 0.5018883221214827,  -0.08154793675398796);

 void main()
 {
     vec3 output_result;
     
     if(imageSourceInputType==0) { // RGBA
         output_result = texture2D(inputImageTexture, textureCoordinate).rgb;
     } else if(imageSourceInputType==1) { // y + u + v
         float y = texture2D(yTexture, textureCoordinate).r;
         float u = texture2D(uTexture, textureCoordinate).r - 0.5;
         float v = texture2D(vTexture, textureCoordinate).r - 0.5;
         
         output_result = yuv2rgbMatrix * vec3(y, u, v);
     } else if(imageSourceInputType==2) { // y + uv
         float y = texture2D(yTexture, textureCoordinate).r;
         float u = texture2D(uvTexture, textureCoordinate).r - 0.5;
         float v = texture2D(uvTexture, textureCoordinate).a - 0.5;
         
         output_result = yuv2rgbMatrix * vec3(y, u, v);
         
         output_result = clamp(output_result, 0.0, 1.0);
     }
     
     vec3 filter_result = output_result;
     
     // color curve 1: optional
     if(colorMap1Enabled==1) {
         filter_result = applyColorMap(filter_result, colorMapTexture1);
         
         filter_result = clamp(filter_result, 0.0, 1.0);
     }
     
     // overlay 1: optional
     if(overlay1Enabled==1) {
         vec3 overlay_image1 = texture2D(overlayTexture1, textureCoordinate).rgb;
         
         filter_result = vec3(softLightCal(filter_result.r, overlay_image1.r),
                              softLightCal(filter_result.g, overlay_image1.g),
                              softLightCal(filter_result.b, overlay_image1.b));
         
         filter_result = clamp(filter_result, 0.0, 1.0);
     }
     
     // overlay 2: optional
     if(overlay2Enabled==1) {
         vec3 overlay_image2 = texture2D(overlayTexture2, textureCoordinate).rgb;
         
         filter_result = vec3(overlayCal(filter_result.r, overlay_image2.r),
                              overlayCal(filter_result.g, overlay_image2.g),
                              overlayCal(filter_result.b, overlay_image2.b));
         
         filter_result = clamp(filter_result, 0.0, 1.0);
     }
     
     // filter mix percentage
     filter_result = mix(output_result, filter_result, filterMixPercentage);

     // output type
     if(renderOutputType==0) { // output RGBA
         gl_FragColor = vec4(filter_result, 1.);
     } else if(renderOutputType==1) { // RGB to YUV
         vec3 yuv = rgb2yuvMatrix * filter_result + vec3(0.0, 0.5, 0.5);

         yuv = clamp(yuv, 0.0, 1.0);

         gl_FragColor = vec4(vec3(yuv.g, yuv.b, yuv.r), 1.0);
     }
 }
 );

@end
