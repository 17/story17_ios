//
//  FilterTopIp4Container.m
//  story17
//
//  Created by POPO Chen on 6/5/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "FilterTopIp4Container.h"
#import "Constant.h"

@implementation FilterTopIp4Container

-(id)init
{
    self = [super init];
    if(self){
    }
    [self setUp];
    return self;
}

-(void)setUp{
    
    _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backBtn setImage:[SINGLETON loadCachedImage:@"nav_arrow_back" withMethod:LOAD_FROM_BUNDLE] forState:UIControlStateNormal];
    [_backBtn setImage:[SINGLETON loadCachedImage:@"nav_arrow_back" withMethod:LOAD_FROM_BUNDLE] forState:UIControlStateHighlighted];
    [_backBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    _backBtn.frame= CGRectMake(5.0, 0.0, 40, 40);
    
    _nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_nextBtn setImage:[SINGLETON loadCachedImage:@"nav_arrow_next" withMethod:LOAD_FROM_BUNDLE] forState:UIControlStateNormal];
    [_nextBtn setImage:[SINGLETON loadCachedImage:@"nav_arrow_next" withMethod:LOAD_FROM_BUNDLE] forState:UIControlStateHighlighted];
    [_nextBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    _nextBtn.frame= CGRectMake(SCREEN_WIDTH-45, 0.0, 40, 40);
    
    [self addSubview:_backBtn];
    [self addSubview:_nextBtn];
    
    _filterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _filterButton.showsTouchWhenHighlighted = YES;
    _filterButton.frame = CGRectMake(10, 0, 35, 35);
    _filterButton.center = CGPointMake((SCREEN_WIDTH-100)/6+50, 22);
    [_filterButton setBackgroundImage:[UIImage imageNamed:@"tab_filters"] forState:UIControlStateNormal];
    [_filterButton setBackgroundImage:[UIImage imageNamed:@"tab_filters_active"] forState:UIControlStateSelected];
    [_filterButton setSelected:YES];
    [self addSubview:_filterButton];
    
    _autoEnhanceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _autoEnhanceButton.showsTouchWhenHighlighted = YES;
    _autoEnhanceButton.frame = CGRectMake(0, 0, 35, 35);
    _autoEnhanceButton.center = CGPointMake(3*(SCREEN_WIDTH-100)/6+50, 22);
    [_autoEnhanceButton setBackgroundImage:[UIImage imageNamed:@"tab_lux"] forState:UIControlStateNormal];
    [_autoEnhanceButton setBackgroundImage:[UIImage imageNamed:@"tab_lux_active"] forState:UIControlStateSelected];
    
    [self addSubview:_autoEnhanceButton];
    
    
    _toolButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _toolButton.showsTouchWhenHighlighted = YES;
    _toolButton.frame = CGRectMake(0, 0, 35, 35);
    _toolButton.center = CGPointMake(5*(SCREEN_WIDTH-100)/6+50, 22);
    [_toolButton setBackgroundImage:[UIImage imageNamed:@"tab_tool"] forState:UIControlStateNormal];
    [_toolButton setBackgroundImage:[UIImage imageNamed:@"tab_tool_active"] forState:UIControlStateSelected];
   
    [self addSubview:_toolButton];
    
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(50, 0, SCREEN_WIDTH-100, NAVI_BAR_HEIGHT)];
    _titleLabel.font = SYSTEM_FONT_WITH_SIZE(16);
    _titleLabel.text = LOCALIZE(@"AUTO ENHANCE");
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.hidden = YES;
    [self addSubview:_titleLabel];
   
}

-(void)changeMode:(UIButton*)selectedBtn
{
    if([_filterButton isEqual:selectedBtn]){
        
        [_filterButton setSelected:YES];
        [_autoEnhanceButton setSelected:NO];
        [_toolButton setSelected:NO];

    }else if([_autoEnhanceButton isEqual:selectedBtn]){
    
        [_filterButton setSelected:NO];
        [_autoEnhanceButton setSelected:YES];
        [_toolButton setSelected:NO];

    }else if([_toolButton isEqual:selectedBtn]){
        
        [_filterButton setSelected:NO];
        [_autoEnhanceButton setSelected:NO];
        [_toolButton setSelected:YES];
    
    }
}


-(void)autoEnhanceModeChange:(NSString*)title
{
    if(title!=nil){
        [_autoEnhanceButton setTitle:title forState:UIControlStateNormal];
    }
    
    if(_titleLabel.isHidden){
        _titleLabel.hidden = NO;
        _nextBtn.hidden = YES;
        _filterButton.hidden = YES;
        _autoEnhanceButton.hidden = YES;
        _toolButton.hidden = YES;
    }else{
        _titleLabel.hidden = YES;
        _nextBtn.hidden = NO;
        _filterButton.hidden = NO;
        _autoEnhanceButton.hidden = NO;
        _toolButton.hidden = NO;
    }

}

@end
