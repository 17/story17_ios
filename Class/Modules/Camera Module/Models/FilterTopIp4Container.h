//
//  FilterTopIp4Container.h
//  story17
//
//  Created by POPO Chen on 6/5/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterTopIp4Container : UIView

@property (nonatomic,strong) UIButton* backBtn;
@property (nonatomic,strong) UIButton* nextBtn;
@property (nonatomic,strong) UIButton* toolButton;
@property (nonatomic,strong) UIButton* filterButton;
@property (nonatomic,strong) UIButton* autoEnhanceButton;

@property (nonatomic,strong) UILabel* titleLabel;


-(void)changeMode:(UIButton*)selectedBtn;
-(void)autoEnhanceModeChange:(NSString*)title;

@end
