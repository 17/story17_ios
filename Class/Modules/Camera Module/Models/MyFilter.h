#import "Constant.h"

#define RENDER_MODE_RGBA 0
#define RENDER_MODE_YUV 1

#define IMAGE_FILTER_SIZE 640.0

#define FILTER_PROGRAM_SIMPLE 0
#define FILTER_PROGRAM_COMPLETE 1
#define FILTER_PROGRAM_GAUSSIAN_BLUR 2
#define FILTER_PROGRAM_SIMPLE_SQUARE 3

@interface MyFilter : NSObject

@property (nonatomic, strong) EAGLContext* glContext;

@property (nonatomic, strong) NSMutableDictionary* textureMapping; // textureName (NSString) to inputTexture(GLuint, as NSNumber) mapping
@property (nonatomic, strong) NSMutableDictionary* textureCache;

@property CGSize imageSize;
@property void* imageOutputDataBuffer;
@property int currentProgramID;

@property GLuint vertexShader;
@property GLuint fragmentShader;
@property GLuint glProgram;
@property GLuint glProgramsquare;
@property GLuint outputTexture;
@property GLint positionAttribute;
@property GLint inputTextureCoordinateAttribute;
@property GLuint framebuffer;
@property GLuint glViewFrameBuffer;
@property GLuint renderBuffer;

@property CVOpenGLESTextureCacheRef glTextureCacheRef;
@property CVPixelBufferRef outputPixelBufferRef;
@property CVOpenGLESTextureRef glOutputTextureRef;
@property CVOpenGLESTextureRef luminanceTextureRef;
@property CVOpenGLESTextureRef chrominanceTextureRef;
@property GLuint yTexture;
@property GLuint uvTexture;

@property GLuint blurTexture;

@property CGSize targetViewPortSize;
@property UIImageOrientation imageOrientation;

-(instancetype)initWithImageSize:(CGSize) size filterProgram:(int) programID;
-(void) loadInputImage:(UIImage*) image textureName:(NSString*) textureName;
-(void) loadYUVData:(CVPixelBufferRef) pixelBuffer;
-(void) setParameter:(NSString*) parameterName floatValue:(float) value;
-(void) setParameter:(NSString*) parameterName intValue:(int) value;
-(void) loadFilter:(NSString*) filterName;
-(UIImage*) outputImage;
-(NSArray*) computeGaussianBlur;
-(void) resetToolParams;
-(void) render:(int) renderMode rebindFrameBuffer:(int) rebindFrameBuffer;
-(void) rendernew:(int) renderMode rebindFrameBuffer:(int) rebindFrameBuffer;
-(void) setRotation:(float) degrees needHorizontalFlip:(int) needHorizontalFlip;
-(void) loadYData:(void*) yData cbData:(void*) cbData crData:(void*) crData isSquare:(int) isSquare;
//-(void) loadTextureData:(NSData*) textureData textureName:(NSString*) textureName;
-(void) loadLuminanceData:(const void*) luminanceData textureSize:(CGSize) textureSize textureName:(NSString*) textureName;

@end
