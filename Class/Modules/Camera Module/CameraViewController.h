#import "Constant.h"
#import "VP8VideoDecoder.h"
#import "VP8VideoEncoder.h"
#import "Mp4VideoEncoder.h"
#import "PCMAudioRecorder.h"
#import "UIImage+Helper.h"

@interface CameraViewController : UIViewController <AVCaptureVideoDataOutputSampleBufferDelegate, AudioRecorderDelegate,UIGestureRecognizerDelegate>
@property int currentCaptureMode;

@end
