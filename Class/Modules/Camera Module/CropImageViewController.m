#import "CropImageViewController.h"
#import "RotateControlAreaView.h"

@implementation CropImageViewController

@synthesize srcImage;
@synthesize cancelBtn;
@synthesize doneBtn;
@synthesize rotationLabel;
//@synthesize imageScrollView;
@synthesize previewImageView;
@synthesize rotateControlAreaView;
@synthesize delegate;
@synthesize currentRotation;
@synthesize gridImageView;
@synthesize currentCenterX;
@synthesize currentCenterY;
@synthesize currentScale;
@synthesize imageRotationPanRecog;
@synthesize isTranslating;
@synthesize isScaling;
@synthesize rotateActionCount;

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    rotateActionCount = 0;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureViewForIOS7];
    
    currentScale = 1.0;
    currentCenterX = 0;
    currentCenterY = 0;
    
    self.title = LOCALIZE(@"ADJUST");
    
    
    UIButton* rotateBtn = [ThemeManager findFriendBtn];
    [rotateBtn setImage:[UIImage imageNamed:@"adjust_rota"] forState:UIControlStateNormal];
    [rotateBtn setImage:[UIImage imageNamed:@"adjust_rota"] forState:UIControlStateHighlighted];
    [rotateBtn addTarget:self action:@selector(rotateAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rotateBtn];
    
    UIButton* gridBtn = [ThemeManager findFriendBtn];
    [gridBtn setImage:[UIImage imageNamed:@"camera_gridtoggle"] forState:UIControlStateNormal];
    [gridBtn setImage:[UIImage imageNamed:@"camera_gridtoggle"] forState:UIControlStateHighlighted];
    [gridBtn addTarget:self action:@selector(gridToggleAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:gridBtn];

    
    previewImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH)];
    previewImageView.contentMode = UIViewContentModeScaleAspectFill;
    previewImageView.image = srcImage;
    previewImageView.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_WIDTH/2);
    [self.view addSubview:previewImageView];
    previewImageView.userInteractionEnabled = YES;
    
    gridImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH)];
    gridImageView.contentMode = UIViewContentModeScaleAspectFill;
    gridImageView.image = [UIImage imageNamed:@"camera_grid"];
    [self.view addSubview:gridImageView];
    gridImageView.userInteractionEnabled = NO;
    
    rotationLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, SCREEN_WIDTH, SCREEN_WIDTH, 40)];
    rotationLabel.textColor = MAIN_COLOR;
    rotationLabel.backgroundColor = [UIColor colorWithWhite:255.0/255.0 alpha:0.9];
    rotationLabel.font = BOLD_FONT_WITH_SIZE(11.0);
    rotationLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:rotationLabel];
    [self reloadRotationLabel];
    
    rotateControlAreaView = [[RotateControlAreaView alloc] initWithFrame:CGRectMake(0, SCREEN_WIDTH+40, SCREEN_WIDTH, SCREEN_HEIGHT-SCREEN_WIDTH-NAVI_BAR_HEIGHT-50-40)];
    rotateControlAreaView.cropImageVC = self;
    rotateControlAreaView.backgroundColor = [UIColor colorWithWhite:227.0/255.0 alpha:0.9];
    [self.view addSubview:rotateControlAreaView];
    [rotateControlAreaView setNeedsDisplay];
    imageRotationPanRecog = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(imageRotationPanRecog:)];

    
    imageRotationPanRecog.delegate = self;
    [rotateControlAreaView addGestureRecognizer:imageRotationPanRecog];
    
    
  UIPanGestureRecognizer* imagePanRecog = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(imagePanRecog:)];
    
    imagePanRecog.delegate = self;
    imagePanRecog.maximumNumberOfTouches = 2;
    [self.view addGestureRecognizer:imagePanRecog];
    UIPinchGestureRecognizer* imagePinchRecog = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(imagePinchRecog:)];
    imagePinchRecog.delegate = self;
    [self.view addGestureRecognizer:imagePinchRecog];
    
    cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(0, SCREEN_HEIGHT-50-NAVI_BAR_HEIGHT, SCREEN_WIDTH/2, 50);
    [cancelBtn setBackgroundColor:MAIN_COLOR];

    [cancelBtn addTarget:self action:@selector(cancelBtn:) forControlEvents:UIControlEventTouchUpInside];

    [cancelBtn setImage:[UIImage imageNamed:@"nav_cancel"] forState:UIControlStateNormal];
    [cancelBtn setImage:[UIImage imageNamed:@"nav_cancel_down"] forState:UIControlStateHighlighted];

    
    [self.view addSubview:cancelBtn];
    
    doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(SCREEN_WIDTH/2, SCREEN_HEIGHT-50-NAVI_BAR_HEIGHT, SCREEN_WIDTH/2, 50);
    [doneBtn setBackgroundColor:MAIN_COLOR];

    [doneBtn addTarget:self action:@selector(doneBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    [doneBtn setImage:[UIImage imageNamed:@"nav_check"] forState:UIControlStateNormal];
    [doneBtn setImage:[UIImage imageNamed:@"nav_check_down"] forState:UIControlStateHighlighted];

    [self.view addSubview:doneBtn];
}

-(void) repositionImageView
{
    if(isScaling || isTranslating) {
        return;
    }
    
    // numerical method: newton method, recursively find solution
    float scaleFactor = 1.0;

    for(int i=1;i<=10;i++) {
        float tmpCenterX = currentCenterX*scaleFactor;
        float tmpCenterY = currentCenterY*scaleFactor;
        float scaleStepSize = 1.0/powf(2.0, (float)i);
        
        // check if tmpCenterX, tmpCenterY is OK, does not let white area show
        CGPoint tmpCenterPoint = CGPointMake(tmpCenterX, tmpCenterY);
        CGPoint originPoint = CGPointMake(0.0, 0.0);
        CGPoint upperRightPoint = CGPointMake(SCREEN_WIDTH/2, SCREEN_WIDTH/2);
        CGPoint lowerRightPoint = CGPointMake(SCREEN_WIDTH/2, -SCREEN_WIDTH/2);
        CGPoint upperLeftPoint = CGPointMake(-SCREEN_WIDTH/2, SCREEN_WIDTH/2);
        CGPoint lowerLeftPoint = CGPointMake(-SCREEN_WIDTH/2, -SCREEN_WIDTH/2);

        BOOL eigenValueTestPass = YES;
        
        for(int j=1;j<=4;j++) {
            float originPointTestResult = [self calculateEigenValueAtPoint:originPoint centerOffset:tmpCenterPoint boundary:j];
            float upperRightPointTestResult = [self calculateEigenValueAtPoint:upperRightPoint centerOffset:tmpCenterPoint boundary:j];
            float lowerRightPointTestResult = [self calculateEigenValueAtPoint:lowerRightPoint centerOffset:tmpCenterPoint boundary:j];
            float upperLeftPointTestResult = [self calculateEigenValueAtPoint:upperLeftPoint centerOffset:tmpCenterPoint boundary:j];
            float lowerLeftPointTestResult = [self calculateEigenValueAtPoint:lowerLeftPoint centerOffset:tmpCenterPoint boundary:j];

            if(originPointTestResult*upperRightPointTestResult<0) {
                eigenValueTestPass = NO;
                break;
            }
            
            if(originPointTestResult*upperLeftPointTestResult<0) {
                eigenValueTestPass = NO;
                break;
            }
            
            if(originPointTestResult*lowerRightPointTestResult<0) {
                eigenValueTestPass = NO;
                break;
            }
            
            if(originPointTestResult*lowerLeftPointTestResult<0) {
                eigenValueTestPass = NO;
                break;
            }
        }
        
        if(eigenValueTestPass) {
            if(i==1) { // already ok!
                break;
            }
            
            // expand scale by step size
            scaleFactor = scaleFactor + scaleStepSize;
        } else {
            // shrink scale by step size
            scaleFactor = scaleFactor - scaleStepSize;
        }
    }
    
    currentCenterX = currentCenterX*scaleFactor;
    currentCenterY = currentCenterY*scaleFactor;
}

-(float) calculateEigenValueAtPoint:(CGPoint) point centerOffset:(CGPoint) centerOffset boundary:(int) boundaryIndex
{
    float rotationInRadian = currentRotation/180.0*M_PI;
    
    if(boundaryIndex==1) {
        return point.y-centerOffset.y-1.0/sqrt(2.0)*SCREEN_WIDTH*currentScale*sin(M_PI_4+rotationInRadian)-tan(rotationInRadian)*(point.x-centerOffset.x-1.0/sqrt(2.0)*SCREEN_WIDTH*currentScale*cos(M_PI_4+rotationInRadian));
    } else if(boundaryIndex==2) {
        return point.y-centerOffset.y-1.0/sqrt(2.0)*SCREEN_WIDTH*currentScale*sin(M_PI_4+rotationInRadian)+1.0/tan(rotationInRadian)*(point.x-centerOffset.x-1.0/sqrt(2.0)*SCREEN_WIDTH*currentScale*cos(M_PI_4+rotationInRadian));
    } else if(boundaryIndex==3) {
        return point.y-centerOffset.y+1.0/sqrt(2.0)*SCREEN_WIDTH*currentScale*sin(M_PI_4+rotationInRadian)-tan(rotationInRadian)*(point.x-centerOffset.x+1.0/sqrt(2.0)*SCREEN_WIDTH*currentScale*cos(M_PI_4+rotationInRadian));
    } else if(boundaryIndex==4) {
        return point.y-centerOffset.y+1.0/sqrt(2.0)*SCREEN_WIDTH*currentScale*sin(M_PI_4+rotationInRadian)+1.0/tan(rotationInRadian)*(point.x-centerOffset.x+1.0/sqrt(2.0)*SCREEN_WIDTH*currentScale*cos(M_PI_4+rotationInRadian));
    }
    
    return 0.0;
}

-(float) currentMinScale
{
    return fabs(sin(currentRotation/180*M_PI))+fabs(cos(currentRotation/180*M_PI));
}

-(void) reloadImageViewPosition
{
    
    previewImageView.center = CGPointMake(currentCenterX+SCREEN_WIDTH/2, currentCenterY+SCREEN_WIDTH/2);
    previewImageView.transform = CGAffineTransformConcat(CGAffineTransformMakeRotation(currentRotation/180*M_PI), CGAffineTransformMakeScale(currentScale, currentScale));
}

-(void) reloadRotationLabel
{
    rotationLabel.text = [NSString stringWithFormat:LOCALIZE(@"Rotation_degrees"), currentRotation];
}

-(void) rotateAction
{
    rotateActionCount++;
    srcImage = [self imageRotatedByDegrees:srcImage deg:90.0];
    previewImageView.image = srcImage;
}

-(void) gridToggleAction
{
    gridImageView.hidden = !gridImageView.hidden;
}

- (UIImage *)imageRotatedByDegrees:(UIImage*)oldImage deg:(CGFloat)degrees{
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,oldImage.size.width, oldImage.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(degrees * M_PI / 180);
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, (degrees * M_PI / 180));
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-oldImage.size.width / 2, -oldImage.size.height / 2, oldImage.size.width, oldImage.size.height), [oldImage CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

# pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if(gestureRecognizer==imageRotationPanRecog || otherGestureRecognizer==imageRotationPanRecog) {
        return NO;
    }
    
    return YES;
}

-(void)cancelBtn:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}
-(void)doneBtn:(id)sender
{    // get cropped image
    DLog(@"Crop Done");
    
    CIContext* ciContext = [CIContext contextWithOptions:nil];
    CIImage* ciImage = [CIImage imageWithCGImage:srcImage.CGImage];
    
    // translation center of srcImage to (0, 0)
    DLog(@"translation:%f %f",ciImage.extent.size.width/2,ciImage.extent.size.height/2);
    
    ciImage = [ciImage imageByApplyingTransform:CGAffineTransformMakeTranslation(-ciImage.extent.size.width/2, -ciImage.extent.size.height/2)];
    
    // scale
    ciImage = [ciImage imageByApplyingTransform:CGAffineTransformMakeScale(currentScale, currentScale)];
    
    // rotation
    float rotation = -currentRotation/180.0*M_PI;
    
    ciImage = [ciImage imageByApplyingTransform:CGAffineTransformMakeRotation(rotation)];
    
    // translation
    
    DLog(@"%f,%f",currentCenterX,currentCenterY);
    
    float scaleFactor = srcImage.size.width/SCREEN_WIDTH;
    ciImage = [ciImage imageByApplyingTransform:CGAffineTransformMakeTranslation(currentCenterX*scaleFactor, -currentCenterY*scaleFactor)];
    // output
    
    DLog(@"size widtf %f, size height %f",srcImage.size.width,srcImage.size.height);
    
    
    CGImageRef processedCGImage = [ciContext createCGImage:ciImage
                                                  fromRect:CGRectMake(-srcImage.size.width/2, -srcImage.size.width/2, srcImage.size.width, srcImage.size.width)];
    
    srcImage = [UIImage imageWithCGImage:processedCGImage];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    [delegate didCompleteImageCrop:srcImage isChanged:YES];
    
    //        (rotateActionCount%4 == 0 && currentRotation == 0.0) ?([delegate didCompleteImageCrop:srcImage isChanged:NO]) : ([delegate didCompleteImageCrop:srcImage isChanged:YES]);
    
    
}


-(void)dealloc
{
    
    DLog(@"crop dealloc");
    
    
    
    //    [NOTIFICATION_CENTER removeObserver:self];
}

-(void)imageRotationPanRecog:(UIPanGestureRecognizer*)sender
{
    
   static float originalRotation = 0.0;
    static float oldScale = 1.0;
    static float oldX = 0.0;
    static float oldY = 0.0;
    
    if(sender.state==UIGestureRecognizerStateBegan) {
        
        originalRotation = currentRotation;
        oldX = currentCenterX;
        oldY = currentCenterY;
        oldScale = currentScale;
        
    } else if(sender.state==UIGestureRecognizerStateChanged) {
        
        currentRotation = originalRotation - [(sender) translationInView:rotateControlAreaView].x/10.0;
        
        // round to nearest 0.05
        float roundingValue = 0.05;
        int mulitpler = floor(currentRotation / roundingValue);
        currentRotation = mulitpler * roundingValue;
        
        if(currentRotation<=-25.0) {
            currentRotation = -25.0;
        }
        
        if(currentRotation>=25.0) {
            currentRotation = 25.0;
        }
        
        [self reloadRotationLabel];
        float minScale = [self currentMinScale];
        
        if(originalRotation!=currentRotation) {
            currentScale = oldScale*(fabs(sin(currentRotation/180*M_PI))+fabs(cos(currentRotation/180*M_PI)))/(fabs(sin(originalRotation/180*M_PI))+fabs(cos(originalRotation/180*M_PI)));
        } else {
            if(currentScale<minScale) {
                currentScale = minScale;
            }
        }
        
        // preserve the center point
        float deltaX = -oldX-(currentScale/oldScale)*(-oldX*cos((currentRotation-originalRotation)/180.0*M_PI)+oldY*sin((currentRotation-originalRotation)/180.0*M_PI));
        float deltaY = -oldY-(currentScale/oldScale)*(-oldX*sin((currentRotation-originalRotation)/180.0*M_PI)-oldY*cos((currentRotation-originalRotation)/180.0*M_PI));
        currentCenterX = oldX + deltaX;
        currentCenterY = oldY + deltaY;
        
        [self reloadImageViewPosition];
        [rotateControlAreaView setNeedsDisplay];
    }
}

-(void)imagePanRecog:(UIPanGestureRecognizer*)sender
{
    static float lastXLocations[] = {0.0, 0.0};
    static float lastYLocations[] = {0.0, 0.0};
    static int oldNumberOfTouches = 0;
    
    if(sender.state==UIGestureRecognizerStateBegan) {
        
        isTranslating = YES;
        
        // save the initial position of each touch when total touch number changes!
        for(int i=0;i<sender.numberOfTouches;i++) {
            lastXLocations[i] = [(sender) locationOfTouch:i inView:sender.view].x;
            lastYLocations[i] = [(sender) locationOfTouch:i inView:sender.view].y;
        }
    } else if(sender.state==UIGestureRecognizerStateChanged) {
        // save the initial position of each touch when total touch number changes!
        if(oldNumberOfTouches!=sender.numberOfTouches) {
            for(int i=0;i<sender.numberOfTouches;i++) {
                lastXLocations[i] = [((UIPanGestureRecognizer*)sender) locationOfTouch:i inView:sender.view].x;
                lastYLocations[i] = [((UIPanGestureRecognizer*)sender) locationOfTouch:i inView:sender.view].y;
            }
        }
        
        oldNumberOfTouches = (int) sender.numberOfTouches;
        
        float offsetXAccumulated = 0.0;
        float offsetYAccumulated = 0.0;
        
        for(int i=0;i<sender.numberOfTouches;i++) {
            float currentLocationX = [(sender) locationOfTouch:i inView:sender.view].x;
            float currentLocationY = [(sender) locationOfTouch:i inView:sender.view].y;
            float offsetX = currentLocationX - lastXLocations[i];
            float offsetY = currentLocationY - lastYLocations[i];
            
            offsetXAccumulated += offsetX;
            offsetYAccumulated += offsetY;
            
            lastXLocations[i] = currentLocationX;
            lastYLocations[i] = currentLocationY;
        }
        
        currentCenterX += offsetXAccumulated/sender.numberOfTouches;
        currentCenterY += offsetYAccumulated/sender.numberOfTouches;
        
        [self reloadImageViewPosition];
        
    } else if(sender.state==UIGestureRecognizerStateEnded) {
        
        isTranslating = NO;
        
        // reposition the image view
        [self repositionImageView];
        
        [UIView animateWithDuration:0.15 animations:^{
            [self reloadImageViewPosition];
        }];
    }
}

-(void)imagePinchRecog:(UIPinchGestureRecognizer*)sender
 {
    static float originalScale = 0.0;
    static float minScale = 1.0;
    static float maxScale = 3.0;
    
    if(sender.state==UIGestureRecognizerStateBegan) {
        
        isScaling = YES;
        originalScale = currentScale;
        minScale = 1.0;
        maxScale = 3.0;
        
    } else if(sender.state==UIGestureRecognizerStateChanged) {
        
        currentScale = originalScale * [(sender) scale];
        
        if(currentScale<minScale) {
            currentScale = minScale;
        }
        
        if(currentScale>maxScale) {
            currentScale = maxScale;
        }
        
        [rotateControlAreaView setNeedsDisplay];
        [self reloadImageViewPosition];
        
    } else if(sender.state==UIGestureRecognizerStateEnded) {
        
        isScaling = NO;
        
        minScale = [self currentMinScale];
        
        if(currentScale<minScale) {
            currentScale = minScale;
        }
        
        // reposition the image view
        [self repositionImageView];
        
        [UIView animateWithDuration:0.15 animations:^{
            [self reloadImageViewPosition];
        }];
    }
}



@end
