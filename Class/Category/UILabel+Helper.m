//
//  UILabel+Helper.m
//  Story17
//
//  Created by York on 2015/12/23.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import "UILabel+Helper.h"

@implementation UILabel (Helper)

-(CGSize)getSizeWithConstrainSize:(CGSize)constrainSize
{
    CGRect textRect = [self.text boundingRectWithSize:(constrainSize)
                                                         options:NSStringDrawingUsesLineFragmentOrigin
                                                      attributes:@{NSFontAttributeName:self.font}
                                                         context:nil];
    return textRect.size;
}

@end
