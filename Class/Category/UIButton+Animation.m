//
//  UIView+Animation.m
//  story17
//
//  Created by POPO Chen on 6/25/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "UIButton+Animation.h"
#import "SKBounceAnimation.h"

@implementation UIButton (Animation)

- (void) addBounceAnimation:(float)scaleSize andDuration:(float)duration withCompletion:(void(^)(BOOL success))callback
{
    NSString *keyPath = @"transform";
    CATransform3D transform = self.layer.transform;
    id finalValue = [NSValue valueWithCATransform3D:
                     CATransform3DScale(transform, scaleSize, scaleSize, scaleSize)
                     ];
    
    SKBounceAnimation *bounceAnimation = [SKBounceAnimation animationWithKeyPath:keyPath];
    bounceAnimation.fromValue = [NSValue valueWithCATransform3D:transform];
    bounceAnimation.toValue = finalValue;
    bounceAnimation.duration = duration;
    bounceAnimation.numberOfBounces = 1;
    bounceAnimation.shouldOvershoot = YES;
    
    [self.layer addAnimation:bounceAnimation forKey:@"someKey"];
    [self.layer setValue:finalValue forKeyPath:keyPath];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self removeAnimation];
        callback(YES);
    });
}


-(void)playBounceAnimation
{
    __block UIButton* blockBtn = self;
    [self addBounceAnimation:1.05 andDuration:0.1 withCompletion:^(BOOL success) {
        [blockBtn addBounceAnimation:1.0/1.05 andDuration:0.1 withCompletion:^(BOOL success) {
        }];
    }];
}

- (void) removeAnimation {
    [self.layer removeAllAnimations];
}

-(void)bounceBackAnimation
{
    CAKeyframeAnimation * animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    [animation setValues:[NSArray arrayWithObjects:@1.0 ,@1.4, @0.9, @1.15, @0.95, @1.02, @1.0, nil]];
    
    animation.duration = 0.8;
    animation.calculationMode = kCAAnimationCubic;
    
    [self.layer addAnimation:animation forKey:@"bounceAnimation"];
//    [self.layer startAnimating];

}



@end
