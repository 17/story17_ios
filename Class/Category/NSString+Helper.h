#import <Foundation/Foundation.h>

@interface NSString (Helper)

- (NSString *)stringByDecodingURLFormat;

- (NSMutableDictionary *)dictionaryFromQueryStringComponents;

- (NSString*)substringFrom:(NSInteger)a to:(NSInteger)b;

- (NSInteger)indexOf:(NSString*)substring from:(NSInteger)starts;

- (NSString*)trim;

- (BOOL)startsWith:(NSString*)s;

- (BOOL)containsString:(NSString*)aString;

- (NSString*)MD5;

+(NSString*) getElaspsedTimeString:(int) timestamp;

- (NSString *)urlEncode;

-(CGSize)getSizeWithConstrainSize:(CGSize)constrainSize withFont:(UIFont*)font;

@end