//
//  UITextView+Helper.h
//  Gossiping
//
//  Created by POPO on 4/7/14.
//  Copyright (c) 2014 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (Helper)

-(int) getLineCount:(NSString*) text;
-(void) iOS7BugFix;
-(void) resizeFontToMaxHeight:(float) maxHeight;

@end
