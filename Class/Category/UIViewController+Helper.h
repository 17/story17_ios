#import "PostListCell.h"

@class PostListCell;

@interface UIViewController (Helper)

-(void) configureViewForIOS7;
-(void) addCustomNavigationBackButton;
-(UIButton*) addCustomNavigationBackButtonCallback;

-(NSString*)arrayToNSString:(NSArray*)array;
-(void) showCompletedMessageWithDelay:(double) delayInSeconds completion: (void (^)(BOOL success)) callback;
-(void)didClickMoreDialog:(PostListCell*)cell collectionView:(UICollectionView*)collectionView dataArray:(NSMutableArray*)postArray;

@end
