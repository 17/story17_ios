//
//  UIView+Animation.h
//  story17
//
//  Created by POPO Chen on 6/25/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Animation)

- (void) addBounceAnimation:(float)scaleSize andDuration:(float)duration withCompletion:(void(^)(BOOL success))callback;
- (void) removeAnimation;
-(void)playBounceAnimation;


@end
