//
//  UIView+Animator.m
//  story17
//
//  Created by POPO Chen on 7/7/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import "UIView+Animator.h"
#import "Constant.h"

@implementation UIView (Animator)

- (void)shake:(int)times withDelta:(CGFloat)delta speed:(NSTimeInterval)interval shakeDirection:(ShakeDirection)shakeDirection {
    [self shake:times withDelta:delta speed:interval shakeDirection:shakeDirection completion:nil];
}

- (void)shake:(int)times withDelta:(CGFloat)delta speed:(NSTimeInterval)interval shakeDirection:(ShakeDirection)shakeDirection completion:(void(^)())handler {
    [self _shake:times direction:1 currentTimes:0 withDelta:delta speed:interval shakeDirection:shakeDirection completion:handler];
}

- (void)_shake:(int)times direction:(int)direction currentTimes:(int)current withDelta:(CGFloat)delta speed:(NSTimeInterval)interval shakeDirection:(ShakeDirection)shakeDirection completion:(void(^)())handler {
    
    [UIView animateWithDuration:interval animations:^{
        self.transform = (shakeDirection == ShakeDirectionHorizontal) ? CGAffineTransformMakeTranslation(delta * direction, 0) : CGAffineTransformMakeTranslation(0, delta * direction);
    } completion:^(BOOL finished) {
        if(current >= times) {
            [UIView animateWithDuration:interval animations:^{
                self.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished) {
                if (handler) {
                    handler();
                }
            }];
            return;
        }
        [self _shake:(times - 1)
           direction:direction * -1
        currentTimes:current + 1
           withDelta:delta
               speed:interval
      shakeDirection:shakeDirection
          completion:handler];
    }];
}

-(void) emitterAnimation:(CGPoint)pt
{
    float multiplier = 0.25f;
    
    CAEmitterLayer* emitter = [CAEmitterLayer layer];
    emitter.emitterPosition = pt;
    emitter.emitterMode = kCAEmitterLayerPoints;
    emitter.emitterShape = kCAEmitterLayerSphere;
    emitter.renderMode = kCAEmitterLayerAdditive;
    emitter.emitterSize = CGSizeMake(100 * multiplier, 0);
    
    NSMutableArray* cellArray = CREATE_MUTABLE_ARRAY;
    for(int i =1;i<=28;i++){
        CAEmitterCell* particle = [self getEmiiterCell:[NSString stringWithFormat:@"colorheart_%d",i] multiplier:multiplier];
        [cellArray addObject:particle];
    }

    emitter.emitterCells = [cellArray copy];

    [self.layer addSublayer:emitter];
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    [CATransaction commit];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [emitter removeFromSuperlayer];
    });
}

-(CAEmitterCell*)getEmiiterCell:(NSString*)imageName multiplier:(float)multiplier
{
    
    CAEmitterCell* particle = [CAEmitterCell emitterCell];
    
//    
//    particle.scale=0.05;
//    particle.emissionLongitude = M_PI;
//    particle.birthRate = multiplier * 40.0;
//    particle.lifetime = multiplier*30;
//    particle.lifetimeRange = multiplier * 4.0f;
//    particle.velocity = 300;
//    particle.velocityRange = 400;
//    particle.emissionRange = 5.5;
//    particle.scaleSpeed = 0.05; // was 0.3
//    particle.alphaRange = 0.02;
//    particle.alphaSpeed = 0.5;
    
    
    particle.scale= 0.1;
    particle.emissionLongitude = M_PI;
//    particle.emissionLatitude = M_PI;

//    particle.xAcceleration = -2;
//    particle.yAcceleration = -2;
//    
    particle.birthRate = multiplier * 20;  // number of particle
    particle.lifetime = 1.0;
    particle.lifetimeRange = multiplier * 4.0f;
    particle.velocity = RANDOM_FLOAT(600, 800);
    particle.velocityRange = RANDOM_FLOAT(400, 450);
    particle.emissionRange = 10;
    particle.scaleSpeed = RANDOM_FLOAT(0.05, 0.08); // was 0.3
    particle.alphaRange = 1.0;
    particle.alphaSpeed = 1.0;
    
    particle.contents = (__bridge id)(IMAGE_FROM_BUNDLE(imageName).CGImage);
    particle.name = imageName;
    
    return particle;
}


@end
