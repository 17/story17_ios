//
//  UILabel+Helper.h
//  Story17
//
//  Created by York on 2015/12/23.
//  Copyright © 2015年 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Helper)

-(CGSize)getSizeWithConstrainSize:(CGSize)constrainSize;

@end
