#import "Constant.h"
#import "PostListCell.h"
#import "ListTableViewController.h"
#import "EditPostViewController.h"
#import "SinglePostViewController.h"
#import "StatusBarColorApplyingActivityViewController.h"

#import "Branch.h"

@implementation UIViewController (Helper)

-(void)configureViewForIOS7
{
    
    self.view.backgroundColor = LIGHT_BG_COLOR;
    
    [self setNeedsStatusBarAppearanceUpdate];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;

    if(self.navigationController) {
        self.navigationController.navigationBar.translucent = NO;
    }
}

/* Don't know what's this */
//- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
//{
//    return YES;
//}

-(void) addCustomNavigationBackButton
{

    UIButton* backButton = [self addCustomNavigationBackButtonCallback];
    
    if(self.navigationController!=nil && self.navigationController.viewControllers.count>1) {
        // pop
        [backButton addTarget:self action:@selector(myPopHandler) forControlEvents:UIControlEventTouchUpInside];
    } else {
        // dismiss
        [backButton addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchUpInside];
    }
}


-(UIButton*) addCustomNavigationBackButtonCallback   // in order to handle scrollview did end drag before pop
{
//    self.navigationItem.hidesBackButton=YES;
    self.navigationController.navigationBarHidden = NO;
    
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[SINGLETON loadCachedImage:@"nav_arrow_back" withMethod:LOAD_FROM_BUNDLE] forState:UIControlStateNormal];
    [backButton setImage:[SINGLETON loadCachedImage:@"nav_arrow_back" withMethod:LOAD_FROM_BUNDLE] forState:UIControlStateHighlighted];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    backButton.frame= CGRectMake(0.0, 0.0, 40, 40);
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    return backButton;
}

-(void)dismissAction
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void) myPopHandler
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(NSString*)arrayToNSString:(NSArray*)array
{
    NSString* str = @"";
    for(int i=0;i<[array count];i++){
        str = [str stringByAppendingString:LOCALIZE(array[i])];
        if(i!=[array count]-1){
            str = [str stringByAppendingString:@", "];
        }
    }
    return str;
}

-(void) showCompletedMessageWithDelay:(double) delayInSeconds completion: (void (^)(BOOL success)) callback
{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[self getHudTargetView] animated:YES];
        hud.userInteractionEnabled=NO;
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"happy")];
        hud.animationType = MBProgressHUDAnimationFade;
        hud.labelText = LOCALIZE(@"Completed!");
        [hud hide:YES afterDelay:1.0];
        callback(YES);
    });
}

-(UIView*) getHudTargetView
{
    return [SINGLETON getStatusBarWindowView];
}


/* PostCell Dialog for didClickMore */
-(void)didClickMoreDialog:(PostListCell*)cell collectionView:(UICollectionView*)collectionView dataArray:(NSMutableArray*)postArray 
{
    NSIndexPath *indexPath = [collectionView indexPathForCell:cell];
    
    NSString* postID = cell.post.postID;
    NSMutableArray* arr;
    
    if(![cell.post.user.userID isEqualToString:MY_USER_ID]){
        arr = [NSMutableArray arrayWithObjects:LOCALIZE(@"ShareTo"),LOCALIZE(@"Unlike"),LOCALIZE(@"report_inappropriate"),  nil];
    }else{
        if([cell.post.type isEqual:@"image"]){
            arr = [NSMutableArray arrayWithObjects:LOCALIZE(@"ShareTo"),LOCALIZE(@"delete_post"),LOCALIZE(@"edit_post"),  nil];
        }else{
            arr = [NSMutableArray arrayWithObjects:LOCALIZE(@"ShareTo"),LOCALIZE(@"delete_video"),LOCALIZE(@"edit_post"),  nil];
        }
    }
    
    if([GET_DEFAULT(IS_ADMIN) intValue]>=NORMAL_ADMIN){
        [arr addObject:LOCALIZE(@"hideFromTimeline")];
        [arr addObject:LOCALIZE(@"promote_post")]; 
        [arr addObject:LOCALIZE(@"admin_delete_post")];
    }
    
    NSArray* dest = @[];

    if(![cell.post.user.userID isEqualToString:MY_USER_ID])
    {
        dest = @[@2];
    }else
    {
        dest = @[];
    }

    [DIALOG_MANAGER showActionSheetDialogTitle:@"" options:arr destructiveIndexes:dest cancelable:YES withCompletion:^(int selectedOption){

        NSString* selectedStr = arr[selectedOption];
        
     
            if([selectedStr isEqualToString:LOCALIZE(@"ShareTo")]){
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [DIALOG_MANAGER showActionSheetShareLink:ShareMediaTypePost withObj:cell.post hasRestream:NO withCompletion:^(NSString *action){
                        
                        if (![action isEqualToString:@"cancel"]) {
                            [EVENT_HANDLER addEventTracking:@"Share_Post" withDict:@{@"channel":action,@"targetUserID":cell.post.user.userID}];
                        }
                    }];
                    
                });
            }else if([selectedStr isEqualToString:LOCALIZE(@"delete_post")]||[selectedStr isEqualToString:LOCALIZE(@"delete_video")]){
                
                /* delete action */
                
                if([self isKindOfClass:[SinglePostViewController class]]){
//                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                        [self.navigationController popViewControllerAnimated:YES];
//                    });
                }else{
                    [DIALOG_MANAGER showCompleteToast];
                    if([postArray count]==1){
                        [cell stopAVPlayer];
                        [postArray removeObjectAtIndex:indexPath.row];
                        [collectionView reloadData];
                    }else{
                        [collectionView performBatchUpdates:^{
                            
                            [cell stopAVPlayer];
                            [postArray removeObjectAtIndex:indexPath.row];
                            [collectionView deleteItemsAtIndexPaths:@[indexPath]];
                            
                        } completion:^(BOOL finished) {
                            
                            [collectionView reloadData];
                            
                        }];
                    }
                }
                
                
                [API_MANAGER deletePost:postID withCompletion:^(BOOL success) {
                    if(success){
                        [NOTIFICATION_CENTER postNotificationName:RELOAD_SELF_DATA object:self];
                        [DIALOG_MANAGER showCompleteToast];
                        if([self isKindOfClass:[SinglePostViewController class]]){
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                [self.navigationController popViewControllerAnimated:YES];
                            });
                        }
                    }else{
                        [DIALOG_MANAGER showNetworkFailToast];
                    }
                }];
                
                
            
        }else if([selectedStr isEqualToString:LOCALIZE(@"report_inappropriate")]){
            
            /* report action */
            ListTableViewController* listTableView = [ListTableViewController new];
            listTableView.dataArray = REPORTED_REASON;
            listTableView.hidesBottomBarWhenPushed= YES;
            listTableView.title = LOCALIZE(@"report_reason");
            
            
            [listTableView setSelectionCallback:^(NSString * callbackStr) {
                
                [API_MANAGER reportPostAction:cell.post.user.userID withReason:callbackStr andPostID:cell.post.postID withCompletion:^(BOOL success, NSString *message) {
                    if(success){
                        [DIALOG_MANAGER showCompleteToast];
                    }else{
                        if([message isEqualToString:@"request_too_quick"]){
                            
                        }else{
                            [DIALOG_MANAGER showNetworkFailToast];
                        }
                    }
                }];
            }];
            [self.navigationController pushViewController:listTableView animated:YES];
            
        } else if([selectedStr isEqualToString:LOCALIZE(@"Unlike")]){
            
            [API_MANAGER unlikePost:postID withCompletion:^(BOOL success) {
                if(success){
                    cell.post.isLiked=0;
                    [collectionView reloadData];
                    [NOTIFICATION_CENTER postNotificationName:RELOAD_SELF_DATA object:self];
                    [DIALOG_MANAGER showCompleteToast];
                    if([self isKindOfClass:[SinglePostViewController class]]){
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [self.navigationController popViewControllerAnimated:YES];
                        });
                    }
                }else{
                    [DIALOG_MANAGER showNetworkFailToast];
                }
            }];

        }else{
            
            if([selectedStr isEqualToString:LOCALIZE(@"edit_post")]){
                
                EditPostViewController* editVC = [EditPostViewController new];
                [editVC setPost:cell.post];
                editVC.hidesBottomBarWhenPushed = YES;
                [editVC setUpdateCompleteCallback:^(PostObject * newPost) {
                    /* get index of cell */
                    NSIndexPath* indexPath = [collectionView indexPathForCell:cell];
                    postArray[indexPath.row] = newPost;
                    cell.post = newPost;
                }];
                [self.navigationController pushViewController:editVC animated:YES];
                
            }else if([selectedStr isEqualToString:LOCALIZE(@"hideFromTimeline")]){
                [API_MANAGER hidePostAction:cell.post.postID withCompletion:^(BOOL success) {
                    if(success){
                        [DIALOG_MANAGER showCompleteToast];
                    }
                }];
            }else if([selectedStr isEqualToString:LOCALIZE(@"promote_post")]){
                
                [API_MANAGER adminPromotePost:cell.post.postID withCompletion:^(BOOL success) {
                    if(success){
                        [DIALOG_MANAGER showCompleteToast];
                    }
                }];
            }else if([selectedStr isEqualToString:LOCALIZE(@"admin_delete_post")]){
                
                /* Admin Delete Post Action */
                ListTableViewController* listTableView = [ListTableViewController new];
                listTableView.dataArray = ADMIN_DELETE_POST_REASON;
                listTableView.hidesBottomBarWhenPushed= YES;
                listTableView.title = LOCALIZE(@"delete_post_reason");
                
                
                [listTableView setSelectionCallback:^(NSString * callbackStr) {
                    
                    /* Admin Delete Post Action  */
                    
                    DLog(@"callbackStr:%@",callbackStr);
                    [API_MANAGER adminDeletePostAction:cell.post.postID message:callbackStr targetUserID:cell.post.user.userID withCompletion:^(BOOL success) {
                        if(success){
                            [DIALOG_MANAGER showCompleteToast];
                        }else{
                            [DIALOG_MANAGER showNetworkFailToast];
                        }
                    }];

                }];
                [self.navigationController pushViewController:listTableView animated:YES];
                
            }
            
        }
    }];

}

@end
