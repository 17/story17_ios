//
//  UIColor+Helper.h
//  story17
//
//  Created by POPO Chen on 8/3/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Helper)

+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end
