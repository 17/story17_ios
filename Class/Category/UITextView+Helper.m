//
//  UITextView+Helper.m
//  Gossiping
//
//  Created by POPO on 4/7/14.
//  Copyright (c) 2014 POPO_INNOVATION. All rights reserved.
//

#import "UITextView+Helper.h"

@implementation UITextView (Helper)

-(int) getLineCount:(NSString*) text
{
    if(text==nil) {
        text = self.text;
    }
    
    // check for word wrapping onto newline.
    __block NSUInteger numberOfLines = 0;
    
    NSAttributedString *t2 = [[NSAttributedString alloc]
                              initWithString:[NSMutableString stringWithString:text] attributes:@{NSFontAttributeName:self.font}];
    
    CGFloat maxWidth = self.frame.size.width;
    
    NSTextContainer *tc = [[NSTextContainer alloc] initWithSize:CGSizeMake(maxWidth, CGFLOAT_MAX)];
    NSLayoutManager *lm = [[NSLayoutManager alloc] init];
    NSTextStorage   *ts = [[NSTextStorage alloc] initWithAttributedString:t2];
    [ts addLayoutManager:lm];
    [lm addTextContainer:tc];
    [lm enumerateLineFragmentsForGlyphRange:NSMakeRange(0,lm.numberOfGlyphs)
                                 usingBlock:^(CGRect rect,
                                              CGRect usedRect,
                                              NSTextContainer *textContainer,
                                              NSRange glyphRange,
                                              BOOL *stop)
     {
         numberOfLines++;
     }];
    
    if(text.length>0 && [[text substringFromIndex:text.length-1] isEqualToString:@"\n"]) {
        numberOfLines++;
    }
    
    return (int)numberOfLines;
}

-(void) resizeFontToMaxHeight:(float) maxHeight
{
    while (YES) {
        CGSize textViewContentSize = [self sizeThatFits:CGSizeMake(self.frame.size.width, FLT_MAX)];
        
        if(textViewContentSize.height>maxHeight) {
            self.font = [UIFont boldSystemFontOfSize:(self.font.pointSize-1.0f)];
        } else {
            break;
        }
    }
}

-(void) iOS7BugFix
{
    // ios7 bug fix
    CGRect line = [self caretRectForPosition:
                   self.selectedTextRange.start];
    CGFloat overflow = line.origin.y + line.size.height
    - ( self.contentOffset.y + self.bounds.size.height
       - self.contentInset.bottom - self.contentInset.top );
    if ( overflow > 0 ) {
        // We are at the bottom of the visible text and introduced a line feed, scroll down (iOS 7 does not do it)
        // Scroll caret to visible area
        CGPoint offset = self.contentOffset;
        offset.y += overflow + 7; // leave 7 pixels margin
        // Cannot animate with setContentOffset:animated: or caret will not appear
        [UIView animateWithDuration:.2 animations:^{
            [self setContentOffset:offset];
        }];
    }
}
@end
