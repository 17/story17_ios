//
//  UIView+Animator.h
//  story17
//
//  Created by POPO Chen on 7/7/15.
//  Copyright (c) 2015 POPO_INNOVATION. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, ShakeDirection) {
    ShakeDirectionHorizontal,
    ShakeDirectionVertical
};


@interface UIView (Animator)
- (void)shake:(int)times withDelta:(CGFloat)delta speed:(NSTimeInterval)interval shakeDirection:(ShakeDirection)shakeDirection;
-(void) emitterAnimation:(CGPoint)pt;

@end
