//
//  OL113Tool_iOS.h
//  OL113Tool_iOS
//
//  Created by ankey on 14-10-15.
//  Copyright (c) 2014年 online.net.tw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OL113Tool : NSObject

/*
 等级任务模式接口
 
 API for Level Mission_Model
 */
+ (void)callFineshWithACT:(NSString *)ACT andKEY:(NSString *)KEY handler:(void(^)(BOOL isSuccess, NSDictionary *dictionary))handler;

/*
 其他任务模式接口
 
 API for other Mission_Model
 */
+ (void)appActiveWithKEY:(NSString *)KEY andAppID:(NSString *)AppID;

@end
