//
//  TWPhotoPickerController.h
//  InstagramPhotoPicker
//
//  Created by Emar on 12/4/14.
//  Copyright (c) 2014 wenzhaot. All rights reserved.
//

#import <UIKit/UIKit.h>
#ifndef ELCImagePickerDemo_ELCImagePickerHeader_h
#define ELCImagePickerDemo_ELCImagePickerHeader_h

#import "ELCAlbumPickerController.h"
#import "ELCImagePickerController.h"
#import "ELCAssetTablePicker.h"

#endif
//#import "ELCImagePickerHeader.h"
#import "BlocksKit+UIKit.h"
#import "Constant.h"

@interface TWPhotoPickerController : UIViewController <ELCAlbumPickerControllerDelegate>

@property (nonatomic, copy) void(^cropBlock)(UIImage *image);
@property (nonatomic, strong) ALAssetsGroup* selectedAlbum;

@end
