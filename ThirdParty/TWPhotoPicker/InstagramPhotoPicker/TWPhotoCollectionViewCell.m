//
//  TWPhotoCollectionViewCell.m
//  InstagramPhotoPicker
//
//  Created by Emar on 12/4/14.
//  Copyright (c) 2014 wenzhaot. All rights reserved.
//

#import "TWPhotoCollectionViewCell.h"

@implementation TWPhotoCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:self.imageView];
        
        self.selectedMask = [[UIView alloc] initWithFrame:self.bounds];
        self.selectedMask.hidden = YES;
        self.selectedMask.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.2];
        self.selectedMask.layer.borderColor = [UIColor colorWithRed:132.0/255.0 green:217.0/255.0 blue:213.0/255.0 alpha:1.0].CGColor;
        [self.contentView addSubview:self.selectedMask];
    }
    return self;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    self.selectedMask.layer.borderWidth = selected ? 1 : 0;
    self.selectedMask.hidden = !selected;
}

@end
